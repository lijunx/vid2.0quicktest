cls
REM ------------------------------------------------------------------------------------
REM - Regression.bat
REM - by Grass Guan
REM ------------------------------------------------------------------------------------

REM ------------------------------------------------------------------------------------
REM Step 0: Environment Set
REM ------------------------------------------------------------------------------------
REM IM501 Project
set XTENSA_XPLORER=c:\usr\xtensa\Xplorer-6.0.2\xplorer.exe
set XTENSA_WORKSPACE=c:\usr\xtensa\Xplorer-6.0.2-workspaces\workspace
set XTDTOOL=C:\usr\xtensa\XtDevTools\install\tools\RF-2015.2-win32\XtensaTools\bin\xt-dumpelf.exe
set PROJ_NAME=vid_ss0_unifiedfw

set CODE_HOME=%XTENSA_WORKSPACE%\%PROJ_NAME%
set BINOUT_PATH=%CODE_HOME%\bin\hifi_mini_prod_RF2015_2\Debug\%PROJ_NAME%

REM Regression Tool
set Regression_HOME=C:\work\VID\Regression_Test_Release_V0.6

set NOAH_HOME=%Regression_HOME%\AudioTestBanch
set Regression_BUILD_HOME=%NOAH_HOME%\build
set HEADLESS_BUILD_CONTROL=%Regression_BUILD_HOME%\vid_build.xml

REM head file for Mode select
set MODE_CONFIG_HOME=%CODE_HOME%\inc
set MODE_CONFIG_FILE=iM501_cnfgInit_mtk_wearable.h
set FILE_NB=iM501_cnfgInit_mtk_wearable_NB.h
set FILE_WB=iM501_cnfgInit_mtk_wearable_WB.h
REM ------------------------------------------------------------------------------------
REM Step 1: git pull and checkout latest tag
REM ------------------------------------------------------------------------------------
cd %CODE_HOME%
git pull
git tag > 1.txt
set /p LATEST_TAG=<"1.txt"
del 1.txt
git checkout %LATEST_TAG%
set LATEST_TAG=

REM ------------------------------------------------------------------------------------
REM Step 2: generate NB and WB mode head file
REM ------------------------------------------------------------------------------------
copy %MODE_CONFIG_HOME%\%MODE_CONFIG_FILE% %Regression_BUILD_HOME%\%MODE_CONFIG_FILE% /y
cd %NOAH_HOME%
call Noah libs\regression\im501_config_modify.lua

REM ------------------------------------------------------------------------------------
REM Step 3: NB code build
REM ------------------------------------------------------------------------------------
set HEADLESS_BUILD_LOG=%Regression_BUILD_HOME%\vid_build_NB.log

copy %Regression_BUILD_HOME%\%FILE_NB% %MODE_CONFIG_HOME%\%MODE_CONFIG_FILE% /y
%XTENSA_XPLORER% --headless %HEADLESS_BUILD_CONTROL%  --cleanbuild --log %HEADLESS_BUILD_LOG% -- -data %XTENSA_WORKSPACE%

set HEADLESS_BUILD_LOG=

REM ------------------------------------------------------------------------------------
REM Step 4: NB hex to bin
REM ------------------------------------------------------------------------------------
set FMNOAH_PATH=%Regression_HOME%\iM501_HS_WB\DSP\iM501\default\NB_BT_Wearable
cd %Regression_BUILD_HOME%

%XTDTOOL% --width 64 --base 0x0FFC0000 --size 0x20000  --full %BINOUT_PATH% > dram0_NB.txt
%XTDTOOL% --width 64 --base 0x0FFE0000 --size 0x1C000  --full %BINOUT_PATH% > dram1_NB.txt
%XTDTOOL% --width 64 --base 0x10000000 --size 0x20000  --full %BINOUT_PATH% > iram0_NB.txt

txt2bin_dspout.py  iram0_NB.txt   %FMNOAH_PATH%\iram0.bin
txt2bin_dspout.py  dram0_NB.txt   %FMNOAH_PATH%\dram0.bin
txt2bin_dspout.py  dram1_NB.txt   %FMNOAH_PATH%\dram1.bin

erase dram0_NB.txt
erase dram1_NB.txt
erase iram0_NB.txt

set FMNOAH_PATH=

REM ------------------------------------------------------------------------------------
REM Step 5: WB code build
REM ------------------------------------------------------------------------------------
set HEADLESS_BUILD_LOG=%Regression_BUILD_HOME%\vid_build_WB.log

copy %Regression_BUILD_HOME%\%FILE_WB% %MODE_CONFIG_HOME%\%MODE_CONFIG_FILE% /y
%XTENSA_XPLORER% --headless %HEADLESS_BUILD_CONTROL%  --cleanbuild --log %HEADLESS_BUILD_LOG% -- -data %XTENSA_WORKSPACE%

set HEADLESS_BUILD_LOG=

REM ------------------------------------------------------------------------------------
REM Step 6: WB hex to bin
REM ------------------------------------------------------------------------------------
set FMNOAH_PATH=%Regression_HOME%\iM501_HS_WB\DSP\iM501\default\WB_BT_Wearable
cd %Regression_BUILD_HOME%

%XTDTOOL% --width 64 --base 0x0FFC0000 --size 0x20000  --full %BINOUT_PATH% > dram0_WB.txt
%XTDTOOL% --width 64 --base 0x0FFE0000 --size 0x1C000  --full %BINOUT_PATH% > dram1_WB.txt
%XTDTOOL% --width 64 --base 0x10000000 --size 0x20000  --full %BINOUT_PATH% > iram0_WB.txt

txt2bin_dspout.py  iram0_WB.txt   %FMNOAH_PATH%\iram0.bin
txt2bin_dspout.py  dram0_WB.txt   %FMNOAH_PATH%\dram0.bin
txt2bin_dspout.py  dram1_WB.txt   %FMNOAH_PATH%\dram1.bin

erase dram0_WB.txt
erase dram1_WB.txt
erase iram0_WB.txt

set FMNOAH_PATH=

REM ------------------------------------------------------------------------------------
REM Step 7: Clear Build
REM ------------------------------------------------------------------------------------
copy %Regression_BUILD_HOME%\%MODE_CONFIG_FILE% %MODE_CONFIG_HOME%\%MODE_CONFIG_FILE% /y
cd %CODE_HOME%
git checkout master

REM IM501 Project
set XTENSA_XPLORER=
set XTENSA_WORKSPACE=
set XTDTOOL=
set PROJ_NAME=
set CODE_HOME=
set BINOUT_PATH=
set HEADLESS_BUILD_CONTROL=

REM Regression Tool
set Regression_BUILD_HOME=

REM head file for Mode select
set MODE_CONFIG_HOME=
set MODE_CONFIG_FILE=
set FILE_NB=
set FILE_WB=

cd %NOAH_HOME%
call Noah libs\regressionTest_NB_Wearable.lua
cd %Regression_HOME%\iM501_HS_WB
rename ACQUA NB_ACQUA
rename Unprocessed NB_Unprocessed
rename Processed NB_Processed
rename Record NB_Record
rename Report NB_Report

cd %NOAH_HOME%
call Noah libs\regressionTest_WB_Wearable.lua
cd %Regression_HOME%\iM501_HS_WB
rename ACQUA WB_ACQUA
rename Unprocessed WB_Unprocessed
rename Processed WB_Processed
rename Record WB_Record
rename Report WB_Report
cd %Regression_HOME%
REM ------------------------------------------------------------------------------------

set NOAH_HOME=
set Regression_HOME=
REM ------------------------------------------------------------------------------------