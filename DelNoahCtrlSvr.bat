@echo off
set service_name=NoahCtrlSvr
set service_displayname=Automaticall Run Noah When System Power On

echo                    Delete NoahCtrlSvr 
echo =========================================================== 

echo try to stop old service
::delet old service if it exists
net stop %service_name% 

echo try to delete old service
sc delete %service_name%

echo backup startup.lua to startup.bak
copy /-Y startup.lua startup.bak

echo clean startup.lua
echo "" >startup.lua
del /P startup.lua

echo ==========================Uninstall Complete============================
pause