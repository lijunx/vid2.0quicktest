-- extend math functions

math = math or {}
math.__help = math.__help or {}

function math.round(num)
	local tmp = math.abs(num)
	local bfloor = false
	if (tmp - math.floor(tmp)) * 10 < 5 then bfloor = true end
	if tmp ~= num then bfloor = not bfloor end
	if bfloor then return math.floor(num) else return math.ceil(num) end
end

function math.logn(x, base)
	return math.log(x) / math.log(base)
end

function math.__help.mag2db()
    str = [==[Usage: mag2db(magnitude)
Convert magnitude to decibels(dB), which value is 20*log10(magnitude).
It support converting one number and an array of numbers
return:
    the corresponding db value.
Parameter:
    magnitude    --A number or number array representing the magnitude
                   value(s) to be converted to db value
]==]
    print(str)
end

function math.mag2db(mag)
	if type(mag)=="table" then
		local val={}
		for k=1, #mag do val[k]= 20*math.log10(mag[k]) end
		return val
	end
	return 20*math.log10(mag)
end

function math.__help.db2mag()
    str = [==[Usage: db2mag(db)
Convert decibels(dB) to magnitude, which value is 10^(db/20).
It support converting one number and an array of numbers
return:
    the corresponding magnitude value.
Parameter:
    db           --A number or number array representing the db value(s)
                   to be converted to magnitude value
]==]
    print(str)
end

function math.db2mag(db)
	if type(db)=="table" then
		local val={}
		for k=1, #db do val[k]= 10^(db[k]/20) end
		return val
	end
	return 10^(db/20)
end

function math.__help.pow2db()
    str = [==[Usage: pow2db(power)
Convert power to decibels(dB), which value is 10*log10(power).
It support converting one number and an array of numbers
return:
    the corresponding db value.
Parameter:
    power    --A number or number array representing the power
                   value(s) to be converted to db value
]==]
    print(str)
end

function math.pow2db(mag)
	if type(mag)=="table" then
		local val={}
		for k=1, #mag do val[k]= 10*math.log10(mag[k]) end
		return val
	end
	return 10*math.log10(mag)
end

function math.__help.db2pow()
    str = [==[Usage: db2pow(db)
Convert decibels(dB) to power, which value is 10^(db/10).
It support converting one number and an array of numbers
return:
    the corresponding power value.
Parameter:
    db           --A number or number array representing the db value(s)
                   to be converted to power value
]==]
    print(str)
end

function math.db2pow(db)
	if type(db)=="table" then
		local val={}
		for k=1, #db do val[k]= 10^(db[k]/10) end
		return val
	end
	return 10^(db/10)
end

function math.sum(tabIn)
	local sum = 0
	for i=1,#tabIn do
		sum = sum + tabIn[i]
	end
	return sum
end

function math.mean(tabIn)
	if #tabIn > 0 then
		return math.sum(tabIn)/(#tabIn)
	end

	return nil
end

function math.abs_sum(tabIn)
	local sum = 0
	for i=1,#tabIn do
		sum = sum + math.abs(tabIn[i])
	end
	return sum
end

function math.abs_mean(tabIn)
	if #tabIn > 0 then
		return math.abs_sum(tabIn)/(#tabIn)
	end

	return nil
end

function math.get_ypos_online(x1, y1, x2, y2, _x)
	assert(x2 ~= x1)
	local tmp = x2 - x1
    local a = (y2 - y1) / tmp
    local b = (y1*x2 - y2*x1) / tmp
    local y = a * _x + b
    return y
end

function math.print_number(numIn, tailLen)
	local numStr = tostring(math.round(numIn*(10^tailLen)))
	if numIn >= 0 then numStr = " " .. numStr end
	
	local len = string.len(numStr)
	if len < tailLen+2 then
		local tmpStr = string.sub(numStr,1,1)
		for i=1,tailLen+2-len do tmpStr = tmpStr .. "0" end
		numStr = tmpStr .. string.sub(numStr,2)
	end
	len = string.len(numStr)
	numStr = string.sub(numStr, 1, len-tailLen) .. "." .. string.sub(numStr, 1+len-tailLen)

	return numStr
end
