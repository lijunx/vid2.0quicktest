
require "libs.regressionV2"

----Debug Information
PRINT_FILE = DEBUG_PATH.."screen_info.txt"  --Save test process
os.execute("c: > "..PRINT_FILE)
ERROR_FILE = DEBUG_PATH.."error_info.txt"
os.execute("c: > "..ERROR_FILE)
TIME_COST_FILE = DEBUG_PATH.."time_cost.txt"
os.execute("c: > "..TIME_COST_FILE)

if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then dofile(SCRIPT_PATH.."Init.lua") end

for i=1,#regression.config.application do
    if regression.config.application[i].enable then
        regression.test(regression.config.application[i], regression.config.moduleSwitch)
    else
        print("skip Application: "..regression.config.application[i].name)
    end
end


if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then AB_disconnect() end
collectgarbage("collect")

print("All work done ...")

