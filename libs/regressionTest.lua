require "libs.regression"

regression.PROJECT_SAMPLE = {
    name = [==[Regression Test]==],
    path = [==[iM501_HS_WB\]==],
    flagFast = false,
    flagClearOldResult = true,
    stressTestTimes = 20,
    offline = {
        enable = false,
        cleanVer = "V001",
        timeIdx = "20170516_1947"
    },
    platform = {
        name = [==[UIF]==],
        version = [==[V2]==],
        module = [==[UIFV2]==],
        ABengine = "ABEngine\\ABDaemonAsync.exe",
        delay = 0.2,
    },
    dut = {
        name = [==[iM501]==],
        path = [==[DSP\]==],
        module = [==[iM501]==],
        version = [==[default]==],
        bandwidth = "WB", --"WB", "NB" ?? can get from vec?
        mode = "BT_Wearable",   --"BT_Wearable", "BT_Automotive", "VR_Automotive"
        reloadEachItem = false,
        delay = 0,
        gain = {
            SND = 0, -- dB
            RCV = 0,
        },
    },
    meter = "fluke289",   -- "fluke287",  "fluke189"
    tester = "Timmy",
}

-- param is follow regression.PROJECT_SAMPLE
regression.config = regression.init(regression.PROJECT_SAMPLE)

--*******************************************************************
----Basic Information

----Path Define
SCRIPT_PATH = "script\\source\\"
TEMPLATE_PATH = NOAH_PATH.."Template\\"
CONFIG_PATH = NOAH_PATH.."Configuration\\"

-- From Old config
RECORD_PATH = regression.config.PATH.record
RECORD_FILE    = "test.wav"            --default setting
RECORD_TIME  = 0     --**   --The time of one cycle recording. equel 0 means stop recording until press any key.
DSP_NAME = regression.config.dut.name
GUI_EN = false                  --**
DEBUG = 1                       --**

----Debug Information
PRINT_FILE = DEBUG_PATH.."screen_info.txt"  --Save test process
os.execute("c: > "..PRINT_FILE)
ERROR_FILE = DEBUG_PATH.."error_info.txt"
os.execute("c: > "..ERROR_FILE)
TIME_COST_FILE = DEBUG_PATH.."time_cost.txt"
os.execute("c: > "..TIME_COST_FILE)

--***************
----load function
require(SCRIPT_PATH.."Init")
if not regression.config.offline.enable then
    intial_platform(regression.config.dut.name, regression.config.dut.sample_rate, regression.config.platform.module)
end
GLOBAL.RESULT_FILE = regression.config.PATH.report.."report_"..regression.TEST_TIME_IDX..".txt"
os.execute("c: > "..GLOBAL.RESULT_FILE)

regression.test()
collectgarbage("collect")

regression.backup(regression.config, regression.TEST_TIME_IDX)
collectgarbage("collect")

--**************************************************
AB_disconnect()
collectgarbage("collect")
--os.execute("pause")

print("All work done ...")
--a = io.read("*l")
