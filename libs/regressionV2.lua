regression = regression or {}

require "libs.regression2.config"
require "libs.regression2.im501_config"
require "libs.regression2.base"
require "libs.regression2.audio"
require "libs.regression2.vos"
require "libs.regression2.electrical"
require "libs.regression2.report"
require "libs.regression2.VEN_Function"
require "libs.regression2.xAASimulation"
require "libs.regression2.AcousticAwareness"
require "libs.regression2.SPIrecording"
require "libs.regression2.pointerRead"

function regression.test(appConfig, moduleSwitch)
    print(notice.const.str.functionDivider.."Start Application: " .. appConfig.name .. " Test\n"..notice.const.str.functionDivider)

    if appConfig.VEN_Function then
        print(notice.const.str.functionBlockDivider.."Start VEN test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider)
        regression.test_VEN_Function(appConfig, moduleSwitch)
        print(notice.const.str.functionBlockDivider.."End VEN test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider.."\n")
    end

    if appConfig.xAASimulation then
        print(notice.const.str.functionBlockDivider.."Start xAASimulation test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider)
        regression.test_xAASimulation(appConfig, moduleSwitch)
        print(notice.const.str.functionBlockDivider.."End xAASimulation test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider.."\n")
    end

    if appConfig.AcousticAwareness then
        print(notice.const.str.functionBlockDivider.."Start AcousticAwareness test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider)
        regression.test_AcousticAwareness(appConfig, moduleSwitch)
        print(notice.const.str.functionBlockDivider.."End AcousticAwareness test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider.."\n")
    end

    if appConfig.SPIreocrding then
        print(notice.const.str.functionBlockDivider.."Start SPIrecording test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider)
        regression.test_SPIrecording(appConfig, moduleSwitch)
        print(notice.const.str.functionBlockDivider.."End SPIrecording test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider.."\n")
    end
    if appConfig.poniterRead then
        print(notice.const.str.functionBlockDivider.."Start pointerRead test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider)
        regression.test_pointerRead(appConfig, moduleSwitch)
        print(notice.const.str.functionBlockDivider.."End pointerRead test of " .. appConfig.name .. " \n"..notice.const.str.functionBlockDivider.."\n")
    end


    
    print(notice.const.str.functionDivider.."End Application: " .. appConfig.name .. " Test\n"..notice.const.str.functionDivider.."\n")
end