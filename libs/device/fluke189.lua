device = device or {}

local stop_str = "\13"  -- each comment must has this byte in the end.
--rs232_connect = "?R"..stop_str --3F 52 0D
--rs232_stop = "S0"..stop_str    -- 53 30 0D
--rs232_reset = "HX"..stop_str   -- 48 58 0D
--rs232_check_X = "?X"..stop_str -- 3F 58 0D

local fluke189 = {
    command = {
        default_setup = "DS"..stop_str;
        identification = "ID"..stop_str;
        reset_instrument = "RI"..stop_str; --use carefully.
        reset_setup = "RMP"..stop_str;
        query_measurement = "QM"..stop_str;
    };
    bps = rs232.RS232_BAUD_9600;
}

function fluke189.find_COM()
    local rs232_connect = fluke189.command.identification
    for port_no = 1, 50 do
        local port_name = [==[\\.\COM]==]..tostring(port_no)
        --printc(port_name)
        local e, p = rs232.open(port_name)
        --print(e, p)
        if e == rs232.RS232_ERR_NOERROR then
            assert(p:set_baud_rate(fluke189.bps) == rs232.RS232_ERR_NOERROR)
            assert(p:set_data_bits(rs232.RS232_DATA_8) == rs232.RS232_ERR_NOERROR)
            assert(p:set_parity(rs232.RS232_PARITY_NONE) == rs232.RS232_ERR_NOERROR)
            assert(p:set_stop_bits(rs232.RS232_STOP_1) == rs232.RS232_ERR_NOERROR)
            assert(p:set_flow_control(rs232.RS232_FLOW_OFF)  == rs232.RS232_ERR_NOERROR)
            --print(p)
            --p:close()
            --e, p = rs232.open(port_name)
            --print(e,p)
            local err, len_written = p:write(rs232_connect)
            --print(err)
            local read_len = 25 -- read one byte
            local timeout = 200 -- in miliseconds
            local err, data_read, size = p:read(read_len, timeout)
            print(err, data_read, size)
            if err == rs232.RS232_ERR_NOERROR and data_read ~= nil and string.find(data_read, "FLUKE 189")  then
                print("\nFluke189 use COM "..port_no)
                --out:write(string.format("OK, port open with values '%s'\n", tostring(p)))
                print("Fluke189 connect successful")
                assert(p:close() == rs232.RS232_ERR_NOERROR)
                return p, port_name
            end

            assert(p:close() == rs232.RS232_ERR_NOERROR)
        end
    end
    print("\nCan't find Fluke189 instrument, please check HW connect and instrument")
    return false
end

function fluke189.connect(port_name)
    print("\nConnect Fluke 189.")
    if port_name then
        local e, p = rs232.open(port_name)
        print(e,p)
        if e ~= rs232.RS232_ERR_NOERROR then
            print("\nCan't connect Fluke189, please check HW connect and instrument")
            return false
        else
            assert(p:set_baud_rate(fluke189.bps) == rs232.RS232_ERR_NOERROR)
        end
        --print(e,p)
        return p
    end
    return p
end

function fluke189.disconnect(p)
    print("\nDisconect Fluke 189.")
    --assert(p:close() == rs232.RS232_ERR_NOERROR)
    if p then
        return p:close()
    end
    return false
end

function fluke189.query_measurement(p)
    local value, unit = "Out of Range", "N/A"
    if p then
        local err, len_written = p:write(fluke189.command.query_measurement)
        local read_len = 25 -- read one byte
        local timeout = 150 -- in miliseconds
        local err, data_read, size = p:read(read_len, timeout)
        --print(err, data_read, size)
        if (err  == rs232.RS232_ERR_NOERROR) and data_read then
            local pos = string.find(data_read, "\13")
            if pos == nil then return value, unit end
            data_read = string.sub(data_read, pos+1, -1)
            pos = string.find(data_read, "\13")
            if pos then data_read = string.sub(data_read, 1, pos-1) end
            --print(data_read)
            pos = string.find(data_read, ",")

            if pos then
                data_read = string.sub(data_read, pos+1, -1)
                pos = string.find(data_read, "Out of range")
                if pos then return value, unit end
                pos = string.find(data_read, " ")
                if pos == nil then return value, unit end
                value = string.sub(data_read, 1, pos-1)
                unit = string.sub(data_read, pos+1, -1)
                unit = string.gsub(unit, " ", "")
                return tonumber(value), unit
            end
        end
    end
    
    return value
end

fluke189.default_set = function(p)
    local err, len_written = p:write(fluke189.command.default_setup)
end

device.fluke189 = device.fluke189 or fluke189