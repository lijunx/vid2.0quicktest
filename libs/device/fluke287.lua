device = device or {}

local stop_str = "\13"  -- each comment must has this byte in the end.
--rs232_connect = "?R"..stop_str --3F 52 0D
--rs232_stop = "S0"..stop_str    -- 53 30 0D
--rs232_reset = "HX"..stop_str   -- 48 58 0D
--rs232_check_X = "?X"..stop_str -- 3F 58 0D

local fluke287 = {
    command = {
        default_setup = "DS"..stop_str;
        identification = "ID"..stop_str;
        reset_instrument = "RI"..stop_str; --use carefully.
        reset_setup = "RMP"..stop_str;
        query_measurement = "QM"..stop_str;
    };
    bps = rs232.RS232_BAUD_115200;
}

function fluke287.find_COM()
    local rs232_connect = fluke287.command.identification
    for port_no = 1, 50 do
        local port_name = [==[\\.\COM]==]..tostring(port_no)
        --printc(port_name)
        local e, p = rs232.open(port_name)
        print(e, p)
        if e == rs232.RS232_ERR_NOERROR then

            assert(p:set_baud_rate(fluke287.bps) == rs232.RS232_ERR_NOERROR)
            assert(p:set_data_bits(rs232.RS232_DATA_8) == rs232.RS232_ERR_NOERROR)
            assert(p:set_parity(rs232.RS232_PARITY_NONE) == rs232.RS232_ERR_NOERROR)
            assert(p:set_stop_bits(rs232.RS232_STOP_1) == rs232.RS232_ERR_NOERROR)
            assert(p:set_flow_control(rs232.RS232_FLOW_OFF)  == rs232.RS232_ERR_NOERROR)

            local err, len_written = p:write(rs232_connect)

            local read_len = 25 -- read one byte
            local timeout = 150 -- in miliseconds
            local err, data_read, size = p:read(read_len, timeout)
            print(err, data_read, size)
            if err == rs232.RS232_ERR_NOERROR and data_read ~= nil and string.find(data_read, "FLUKE 287")  then
                print("\nFluke287 use COM "..port_no)
                --out:write(string.format("OK, port open with values '%s'\n", tostring(p)))
                print("Fluke287 connect successful")
                assert(p:close() == rs232.RS232_ERR_NOERROR)
                return p, port_name
            end

            assert(p:close() == rs232.RS232_ERR_NOERROR)
        end
    end
    print("\nCan't find Fluke287 instrument, please check HW connect and instrument")
    return false
end

function fluke287.connect(port_name)
    print("\nConnect Fluke 287.")
    if port_name then
        local e, p = rs232.open(port_name)
        print(e,p)
        if e ~= rs232.RS232_ERR_NOERROR then
            print("\nCan't connect Fluke287, please check HW connect and instrument")
            return false
        end
        return p
    end
    return false
end

function fluke287.disconnect(p)
    print("\nDisconect Fluke 287.")
    --assert(p:close() == rs232.RS232_ERR_NOERROR)
    if p then
        return p:close()
    end
    return false
end

function fluke287.query_measurement(p)
    local value, unit = "Out of Range", "N/A"
    if p then
        local err, len_written = p:write(fluke287.command.query_measurement)
        local read_len = 25 -- read one byte
        local timeout = 150 -- in miliseconds
        local err, data_read, size = p:read(read_len, timeout)
        print(err, data_read, size)
        if (err  == rs232.RS232_ERR_NOERROR) and data_read then
            local pos = string.find(data_read, "\13")
            if pos == nil then return value, unit end
            data_read = string.sub(data_read, pos+1, -1)
            pos = string.find(data_read, ",")
            if pos == nil then return value, unit end
            value = string.sub(data_read, 1, pos-1)
            local value1 = string.sub(data_read, pos+1, -1)
            pos = string.find(value1, ",")
            if pos then value1 = string.sub(value1, 1, pos-1) end
            
            pos = string.find(value, "E")
            if pos then
                unit = tonumber(string.sub(value, pos+1, -1))
                value = string.sub(value, 1, pos-1)
                --print(value, unit)
                if unit == 0 then
                    unit = ""..value1
                elseif unit == -3 then
                    unit = "m"..value1
                elseif unit == -6 then
                    unit = "u"..value1
                else
                    unit = "N/A"
                end
            end
            
            --print(value, unit)
            --print(string.len(value))
            if string.len(value) > 8 then value = "Out of Range" end
        end
    end
    
    return value, unit
end

fluke287.default_set = function(p)
    local err, len_written = p:write(fluke287.command.default_setup)
end

device.fluke287 = device.fluke287 or fluke287
