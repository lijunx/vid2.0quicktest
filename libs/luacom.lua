require "luacom"
require "libs.luacom.excel"
require "libs.luacom.word"

function luacom.RGB(r, g, b)
    return r+256*g+256*256*b
end