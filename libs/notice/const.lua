notice.const = notice.const or {}

local str = {}

str.functionDivider      = [==[*************************************************************************************************]==] .. "\n"
str.functionBlockDivider = [==[=================================================================================================]==] .. "\n"
str.functionLineDivider  = [==[-------------------------------------------------------------------------------------------------]==] .. "\n"
str.functionDividerStart = "\n" .. str.functionDivider
str.functionDividerEnd = str.functionDivider .. "\n"

notice.const.str = str