local log = {}

lfs.create_folder(NOAH_PATH .. [==[debug_info\]==])
log.FILE = NOAH_PATH .. [==[debug_info\screen_info.txt]==]
--log.FILE = NOAH_PATH .. [==[debug_info\log.txt]==]
messageStrInitial = ""
messageStrInitial = messageStrInitial .. "****************                          Log File                       ****************\n"
messageStrInitial = messageStrInitial .. "****************        For Auto Simulation Tool script CMD record.      ****************\n"
messageStrInitial = messageStrInitial .. "****************         A product of SQA Fortemedia Co., Ltd.           ****************\n\n"

local fp = assert( io.open(log.FILE,"w+") )
assert(fp:write( messageStrInitial ) )
assert(fp:close())

local function writeLog(messageStr)
    fp = assert( io.open(log.FILE,"a+") )
    assert(fp:write( messageStr.."\n" ) )
    assert(fp:close())
end

function log.message(functionStr, messageStr, messageType)
    local str = notice.base.message(functionStr, messageStr, messageType, 3)
    writeLog(str)
end

notice.log = notice.log or log