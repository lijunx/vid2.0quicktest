local base = {}

-- outputType
--    1: cmd message
--    2: ui  message
--    3: log message
function base.message(functionStr, messageStr, messageType, outputType)
    if 1 == outputType then return (os.date() .. " [[" .. messageType .. "]] [" .. functionStr .. "]:\t" .. messageStr) end -- cmd
    if 2 == outputType then return (os.date() .. " [" .. functionStr .. "]:\n" .. messageStr) end -- ui
    if 3 == outputType then return (os.date() .. " [[" .. messageType .. "]] [" .. functionStr .. "]:\t" .. messageStr) end -- log
end

notice.base = notice.base or base