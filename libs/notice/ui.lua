local ui = {}

local function dialogType(messageType)
    messageType = string.upper(messageType) or "MESSAGE"
    if messageType == "INFO " then
        messageType = "INFORMATION"
    elseif messageType == "WARN " then
        messageType = "WARNING"
    elseif messageType == "ERROR" then
        messageType = "ERROR"
    else
        messageType = "MESSAGE"
    end
    
    return messageType
end
    
function ui.message(functionStr, messageStr, messageType)
    local str = notice.base.message(functionStr, messageStr, messageType, 2)
    messageType = dialogType(messageType)
    local dlg = iup.messagedlg{DIALOGTYPE = messageType, value = str, title = messageType}
    iup.Popup(dlg)
    iup.Destroy(dlg)
end

notice.ui = notice.ui or ui