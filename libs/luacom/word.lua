luacom.word = {}

luacom.word.const = 
{
    row_heigh = 12,
    column_width = 45.755 ;--(7); --51.755(8);   --63.755 (10);   --45.755 (7);
    range_space = 2;
    null = "no data";
}


function luacom.word.create(visible)
    visible = visible or false
    local word = luacom.GetObject("Word.Application")
    if not word then
        word = luacom.CreateObject("Word.Application")
    end
    
    if not word then
        return false, "Can't instantiate Word"
    end
    
    if visible then word.Visible = visible end
    word.DisplayAlerts = false
    
    return word
end

function luacom.word.quit(word)
    word:Quit(0)
end

function luacom.word.open_file(word, file)
    return word.Documents.Open(0, file, 0, 0)
end

function luacom.word.close_file(mydoc)
    mydoc:Close(0)
end

function luacom.word.saveas(mydoc, name)
    mydoc:SaveAs(name)
end


