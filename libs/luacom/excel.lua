luacom.excel = {}

luacom.excel.error = {}
luacom.excel.fill_color = {}
luacom.excel.const = 
{
    row_heigh = 12,
    column_width = 45.755 ;--(7); --51.755(8);   --63.755 (10);   --45.755 (7);
    range_space = 2;
    null = "no data";
}
luacom.excel.__help = {}

function luacom.excel.get_column_index(column) 
    local column_index = "A"
    if column < 27 then
        column_index = string.char(tostring(column+64))
    else
        local d = math.floor(column/26)+64
        local d1 = column%26 + 64
        --print(d, d1)
        if column%26 == 0 then 
            d = d - 1 
            d1 = d1 + 26
        end
        d = string.char(tostring(d))
        d1 = string.char(tostring(d1))
        --print(d, d1)
        column_index = d..d1
    end
    --print(column_index)
    return column_index
end

function luacom.excel.check_error(value)
    local str = ""
    if value == luacom.excel.error.xlErrDiv0 then
        str = ("#DIV/0! Error")
    elseif value == luacom.excel.error.xlErrNA then
        str = ("#N/A Error")
    elseif value == luacom.excel.error.xlErrName then
        str = ("#NAME? Error") 
    elseif value == luacom.excel.error.xlErrNull then
        str = ("#NULL! Error")
    elseif value == luacom.excel.error.xlErrNum then
        str = ("#NUM! Error")
    elseif value == luacom.excel.error.xlErrRef then
        str = ("#REF! Error")
    elseif value == luacom.excel.error.xlErrValue then
        str = ("#VALUE! Error")
    end
    --printc(str)
    return st
end

function luacom.excel.create(visible)
    visible = visible or false
    local xl = luacom.GetObject("Excel.Application")
    if not xl then
        xl = luacom.CreateObject("Excel.Application")
    end

    if not xl then
        return false, "Can't instantiate Excel"
    end
    
    local typeinfo = luacom.GetTypeInfo(xl)
    local typelib = typeinfo:GetTypeLib()
    local enums = typelib:ExportEnumerations()
    luacom.excel.error = enums.XlCVError
    luacom.excel.fill_color = enums.XlRgbColor

    if visible then xl.Visible = visible end
    xl.DisplayAlerts = false
    
    return xl, enums
end

function luacom.excel.quit(excel)
    excel:Quit(0)
end

function luacom.excel.new_workbook(excel)
    local mybook = excel.Workbooks:Add()

    return mybook
end

function luacom.excel.new_sheet(mybook, sheetName)
    mysheet = mybook.WorkSheets:Add()
    mysheet.Name = sheetName or "1"

    return mysheet
end

function luacom.excel.open_file(excel, file)
    luacom.excel.quit(excel)
    return excel.Workbooks.Open(0, file, 0, 0)
end

function luacom.excel.close_file(myexcel)
    myexcel:Close(0)
end

function luacom.excel.saveas(myexcel, name)
    myexcel:SaveAs(name)
end

function luacom.excel.open_sheet(myexcel, sheet_name)
    local mysheet = myexcel.WorkSheets(sheet_name)
    --mysheet.Activate()
    luacom.excel.const.column_width = mysheet.Cells(1,1).ColumnWidth
    luacom.excel.const.row_heigh = mysheet.Cells(1,1).RowHeight

    return mysheet
end

function luacom.excel.get_range(sheet, start_row, start_column, end_row, end_column)
    start_column, start_row, end_column, end_row = start_column or "A", start_row or 1, end_column or "A", end_row or 1

    if (end_column < start_column) or end_row < start_row then
         return sheet.Range("A1:A1")
    end
    
    if type(start_column) == "number" then
        start_column = luacom.excel.get_column_index(start_column)
    end
    if type(end_column) == "number" then
        end_column = luacom.excel.get_column_index(end_column)
    end

    start_row, end_row = tostring(start_row), tostring(end_row)
    local range = start_column .. start_row .. ":" .. end_column .. end_row
    
--    return range
    return sheet.Range(nil, range)
end

function luacom.excel.range_duplicate(org_range, dest_range)
    org_range:Activate()
    org_range:Select()
    org_range:Copy()
    dest_range:PasteSpecial(xlPasteAll)
end

function luacom.excel.write_cell(range, row, column, value)
    range.Cells(row, column).Value2 = value
end

function luacom.excel.read_cell(range, row, column)
    return range.Cells(row, column).Value2
end

function luacom.excel.get_cell_value(mybook, sheet_name, row, column)
    local mysheet = mybook.Sheets(sheet_name)
    local cell_value = mysheet.Cells(row, column).Value2
    print ("the value read from excel cell is: "..cell_value)
    return cell_value
end

function luacom.excel.get_onecolumn_cells_value(mybook, sheet_name, start_row, end_row, column)
     local cell_value_array = {}
     local i
     for i = start_row, end_row do 
             print("current cell is row "..i.." column "..column)
             cell_value_array[i-start_row+1] = luacom.excel.get_cell_value(mybook, sheet_name, i, column)
             print(cell_value_array[i-start_row+1])
     end
     print("~~~"..#cell_value_array)
     return cell_value_array
 end

 function luacom.excel.write_cells_value(mybook, sheet_name, row, column, cell_value)
    --local mybook = Yichao_excel_process.open_excel_file(path, filename, excel)
    local mysheet = mybook.Sheets(sheet_name)
    mysheet.Cells(row, column).Value2 = cell_value
    print ("the value write to excel cell is: "..cell_value)

    return cell_value
 end

function luacom.excel.get_chart(sheet, chart_count)
    return sheet:ChartObjects(chart_count)
end

function luacom.excel.chart_duplicate(org_chart, dest_chart)
    org_chart:Activate()
    org_chart:Select()
    dest_chart = org_chart:Duplicate()
end

function luacom.excel.copy_chart(mybook, sheet_name_org, sheet_name_dst, chart_number, dst_start_row, dst_start_column)
    local sheet_org = mybook.Sheets(sheet_name_org)
    local sheet_dst = mybook.Sheets(sheet_name_dst)
    local org_chart = sheet_org:ChartObjects(chart_number)
    org_chart:Activate()
    org_chart:Select()
    org_chart:Copy()
    sheet_dst:Activate()
    local startcolumn = luacom.excel.get_column_index(dst_start_column)----select a start cell to copy the chart
    local chart_start_range = startcolumn..dst_start_row..":"..startcolumn..dst_start_row
    sheet_dst:Range(chart_start_range):Select()
    sheet_dst:Paste()
end

luacom.excel.__help.set_chart_property =
[==[
    property:
    .title : chart title
    .height : chart height
    .width : chart width
    .left : start point, column index
    .top: : start point, row index
]==]
function luacom.excel.set_chart_property(chart, property)
    chart.Chart.ChartTitle.Text = property.title or chart.Chart.ChartTitle.Text 
    chart.Height = property.height or chart.Height
    chart.Width = property.width or chart.Height
    chart.Left = property.left * luacom.excel.const.column_width or chart.Left
    chart.Top = property.top  * luacom.excel.const.row_heigh or chart.Top
end

function luacom.excel.delete_sheet(myexcel, sheetname)
    if myexcel.Sheets.Count == 1 then
        if myexcel.Sheets(1).Name == sheetname then
             myexcel.WorkSheets:Add()
             myexcel.Sheets(sheetname).Delete(true)
        end
        return myexcel
    end
    for i = 1, myexcel.Sheets.Count do
        if myexcel.Sheets(i).Name == sheetname then
            myexcel.Sheets(i).Delete(true)
            break
        end
    end

    return myexcel
end
function luacom.excel.new_sheet_after(mybook, sheetName)
    local newsheet = mybook.WorkSheets:Add(nil,mybook.Sheets(2),1)
    newsheet.Name = sheetName or "1"
    return newsheet
end

luacom.excel.__help.help = 
[==[
creat excel application: myexecl = luacom.CreateObject("Excel.Application");
serch excel: myexcel = luacom.GetObject("Excel.Application");

Work Book:
    operation: .Add; .Save; .SaveAs(filename); .Open(filename)
      creat workbook: newbook = excel.Workbooks.Add
      open workbook: mybook = excel.Workbooks.Open("myexcel.xls")
      save workbook: mybook.Save()
      saveas workbook: mybook.SaveAs("c:\\mynewbook.xls")
      
    attribution:  .Title; .Subject;
--print(xl.Application.RecentFiles(i).Name)
Work Sheet:
    activate work sheet: mybook.Worksheets(1).Activate; mybook.Worksheets("Sheet1").Activate
      mysheet = mybook.Worksheets(1)
      mysheet.Name = "mysheet"

Other object: Charts; DialogSheets; ChartObjects
    chart = excel.Charts:Add()
    chart.ChartType = 4 -- xlLine
    range = mysheet:Range("A1:A30")
    chart:SetSourceData(range)
    
Operate cell:
    mysheet.Cells.ClearContents; mysheet.[A1:B5].ClearContents
    mysheet.Cells(6,1).Value = 10; mysheet.Cells(counter, 1).Value = counter
    myrange = mysheet.Range("A1"); .Range("A1:B5"); .Range("C5:D9, G9:H16"); .Range("A:A"); .Range("1:1")
    myrange.Formula = "=RAND()"
    myrange.Font.Bold = True
    myrange.Value2 = "Hello world"
    myrange1 = mysheet.Range("A1:B2"), myrange2 = mysheet.Range("C3:D4")
    myMultipleRange = Union(myrange1, myrange2)
    Range("myrange").ClearContents
    Range.Select
    myrow = mysheet.Rows(1); myrow.Font.Bold = True
    mycolumns = mysheet.Columns("A")
   
Copy:
   myrange.Copy(destination); myrange.PasteSpeical()

Send mail:
    mybook.SendMail("junw@fortemedia.com", "Test")

PrintOut:
    mysheet.PrintOut(page_start, page_end, number, reveiw(true/false), printer_name, ...)
    
CVErr = {xlErrDiv0, xlErrNA, xlErrName, xlErrNull, xlErrNum, xlErrRef, xlErrValue}
]==]