batchcalctool  = batchcalctool or {}
batchcalctool.__help  = batchcalctool.__help or {}
batchcalctool.VERSION = 0.01

require "ACQUA"
require "libs.batchcalctool.const"
require "libs.batchcalctool.param_gen"
require "libs.batchcalctool.batchxml"
require "libs.batchcalctool.report_analysis"
require "libs.batchcalctool.base"
require "libs.batchcalctool.dongle"

function batchcalctool.get_param(calc_mode)
    return batchcalctool.const[calc_mode]
end

function batchcalctool.called_check(auto_kill)
    auto_kill = auto_kill or false
    notice.message_lib("batchcalctool.batchbat_gen", "Since BatchCalcTool server is conflict with ACQUA test application, need to check ACQUA application is running or not.")
    local state = lfs.check_task("Acqualyzer.exe")
    if state then 
        local input = iup.Alarm("Warning", "ACQUA application is running, if you want to run Batch calculator, need to close ACQUA application. Close it?", "OK", "Cancel")
        if input == 1 or auto_kill then
            os.execute("taskkill /f /im ".."Acqualyzer.exe")
            os.execute("taskkill /f /im ".."Acqua3.exe")
            os.execute("taskkill /f /im ".."HaspLicenseServer.exe")
            wait(2)
            return true
        else
            input = iup.Alarm("Warning", "Calculator can not run if ACQUA application is running, you have to close ACQUA application at first.\n  Does ACQUA close?", "Yes", "No")
            if input == 1 then return true
            else return false end
        end
    end
end

local function batchtxt_gen(fileList, file)
    local acquaFileList = {"clean", "unprocessed", "processed"}
    local str = ""
    if "table" == type(fileList.processed) then
        for j=1,#fileList.processed do
            for i=1,#acquaFileList do
                if fileList[acquaFileList[i]] then
                    str = str .. fileList[acquaFileList[i]][j] .. "\n"
                end
            end
            str = str .. "###\n\n"
        end
    else
        for i=1,#acquaFileList do
            if fileList[acquaFileList[i]] then
                str = str .. fileList[acquaFileList[i]] .. "\n"
            end
        end
        str = str .. "###"
    end
    local fp = assert( io.open(file,"w+") )
    assert(fp:write( str.."\n" ) )
    assert(fp:close())
end

function batchcalctool.str2num(str, numTailLen)
    local s,e = string.find(str, ",")
    if s then
        str = string.sub(str, 1, s-1) .. "." .. string.sub(str, e+1)
    end

    local result = tonumber(str)
    if "number" ~= type(result) then return str end

    if nil ~= numTailLen then
        local multi = 10^numTailLen
        result = math.round(multi*result)/multi
    end

    return result
end

function batchcalctool.check()
    local batchTool = [==[C:\Program Files\HEAD Analyzer ACQUA\BatchProcessor.exe]==]
    local flag = lfs.check_file_exist(batchTool)
    if not flag then
        batchTool = [==[C:\Program Files (x86)\HEAD Analyzer ACQUA\BatchProcessor.exe]==]
        flag = lfs.check_file_exist(batchTool)
    end
    if flag then
        notice.message_lib("batchcalctool.run", "batch tool exist", "Info")
    else
        notice.message_lib("batchcalctool.run", "batch tool not exist", "Error")
        return flag
    end

    flag = lfs.check_task(batchTool)
    if not flag then
        notice.message_lib("batchcalctool.run", "batch tool is avilable", "Info")
    else
        notice.message_lib("batchcalctool.run", "batch tool is running", "Error")
        return false
    end

    return true, batchTool
end

batchcalctool.create_bat = function(BAT_FILE, batch_tool, config_file, batch_file, result_file, keepOpen, flagExist)
    os.execute("c: > "..BAT_FILE)
    local FILE = assert( io.open(BAT_FILE,"a+") )
    assert(FILE:write("set BATCH_TOOL=\""..batch_tool.."\"\n"))
    assert(FILE:write("set CONFIG_FILE=\""..config_file.."\"\n"))
    assert(FILE:write("set BATCH_FILE=\""..batch_file.."\"\n"))
    assert(FILE:write("set RESULT_FILE=\""..result_file.."\"\n"))

    if flagExist then
        if keepOpen then
            assert(FILE:write([==[%BATCH_TOOL% -c %CONFIG_FILE% -b %BATCH_FILE% -r %RESULT_FILE%]==]))
        else
            assert(FILE:write([==[%BATCH_TOOL% -c %CONFIG_FILE% -b %BATCH_FILE% -r %RESULT_FILE% -e]==]))
        end
    else
        if keepOpen then
            assert(FILE:write([==[%BATCH_TOOL% -c %CONFIG_FILE% -b %BATCH_FILE% -r %RESULT_FILE% -s]==]))
        else
            assert(FILE:write([==[%BATCH_TOOL% -c %CONFIG_FILE% -b %BATCH_FILE% -r %RESULT_FILE% -s -e]==]))
        end
    end
    --assert(FILE:write([==[%BATCH_TOOL% -c %CONFIG_FILE% -b %BATCH_FILE% -r %RESULT_FILE% -s -e]==]))

    assert(FILE:write("\n exit"))
    assert(FILE:close())
end

function batchcalctool.run(param, files, path, keepOpen, clear)
    keepOpen = keepOpen or false
    path = lfs.create_folder(path, clear) -- temp folder clear
    --path = NOAH_PATH .. path

    -- generate file list txt
    local batchTXT = path .. "\\batch.txt"
    batchtxt_gen(files, batchTXT)

    -- generate xml
    batchXML = path .. "\\batch.xml"
    batchcalctool.batchxml_gen(param, batchXML)

    -- run tool
    -- batch tool check
    local flag, batchTool = batchcalctool.check()
    if not flag then return false end

    --local flagExist = lfs.check_task("BatchProcessor.exe")
    -- batch run
    local batchResult = path .. "\\result.txt"
    local commandStr = [==[call "]==] .. batchTool .. [==[" -b "]==] .. batchTXT .. [==[" -r "]==] .. batchResult .. [==[" -c "]==] .. batchXML .. [==[" -s -e]==]
    os.execute(commandStr)
    
    flag = lfs.check_file_exist(batchResult)
    if flag then
        local result = batchcalctool.report_analysis(param, files, batchResult)
        return result
    else
        return false
    end
end
