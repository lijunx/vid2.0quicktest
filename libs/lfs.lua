require"lfs"

lfs = lfs or {}

function lfs.get_full_path(path)
    if path then
        -- transfer relative path to absolute path
        local tmpStrHead = string.sub(path, 1, 4) -- get first 4 char
        local tmpS, tmpE = string.find(tmpStrHead, ":")
        if tmpS then return path end
        tmpS, tmpE = string.find(tmpStrHead, "%.%.")
        if tmpS then return path end
        
        -- relative path
        tmpStrHead = string.sub(path, 1, 1)
        while "\\" == tmpStrHead or "/" == tmpStrHead or "." == tmpStrHead do
            path = string.sub(path, 2)
            tmpStrHead = string.sub(path, 1, 1)
        end
        path = NOAH_PATH .. path

        tmpS, tmpE = string.find(path, "/")
        while tmpS do
            path = string.sub(path, 1, tmpS-1) .. "\\" .. string.sub(path, tmpE+1)
            tmpS, tmpE = string.find(path, "/")
        end
    end
    
    return path
end

function lfs.empty_dir(rootpath)
    pathes = pathes or {}
    for entry in lfs.dir(rootpath) do
        if entry ~= '.' and entry ~= '..' then
            local path = rootpath .. '\\' .. entry
            local attr = lfs.attributes(path)

            assert(type(attr) == 'table')
            
            if attr.mode == 'directory' then
                lfs.empty_dir(path)
                lfs.rmdir(path)
            else
                os.execute("del "..path)
            end
        end
    end
end

function lfs.create_folder(folder, new)
    folder = lfs.get_full_path(folder)
    local finalChar = string.sub(folder,string.len(folder))
    while finalChar == [==[\]==] or finalChar == [==[/]==] do
        folder = string.sub(folder, 1, string.len(folder)-1)
        finalChar = string.sub(folder,string.len(folder))
    end
    new = new or false
    local attr = lfs.attributes(folder)
    if type(attr) == "table" then
        -- already creat.
        if new and (attr.mode == "directory") then lfs.empty_dir(folder) end
        return folder
    end
    os.execute("md "..folder)
    return folder
end

function lfs.create_file(file)
    os.execute("c: > "..file)
end

--match_string: "*.txt", "*.log", "*.pk"
function lfs.del(file_path, match_string)
    local file_table = misc.find_names(match_string, file_path, 0)
    notice.message_lib("lfs.del", "clear files: "..match_string, "Info")
    if #file_table > 0 then
        for i = 1, #file_table do
            local file = string.gsub(file_table[i], "\\", "")
            os.execute("del \""..file_path..file.."\"")
        end
    end
end

function lfs.check_folder_exist(folder)
    while [==[/]==] == string.sub(folder, string.len(folder), string.len(folder)) or [==[\]==] == string.sub(folder, string.len(folder), string.len(folder)) do
        folder = string.sub(folder, 1, string.len(folder)-1)
    end
    local attr = lfs.attributes(folder)
    if type(attr) == "table" and attr.mode == "directory" then
        return true
    else
        return false
    end
end

function lfs.check_file_exist(filename)  --function for checking file exist or not, so it is not a error when file is not exist.
    filename = lfs.get_full_path(filename)
    local file = io.open(filename, "r")
    if nil == file then
        --notice.message_lib("lfs.check_file_exist", "file: ".. filename .. " not exist!", "Error")
        return false
    end
    file:close()

    return true
end

function lfs.copy(fileIn, fileOut, flagOverwrite)
    fileIn = lfs.get_full_path(fileIn)
    fileOut = lfs.get_full_path(fileOut)
    local flag = lfs.check_file_exist(fileIn)
    if not flag then
        notice.message_lib("lfs.copy", "file: ".. fileIn .. " not exist!", "Error")
        return false
    else
        if flagOverwrite then
            os.execute("copy \""..fileIn.."\" \""..fileOut.."\" /y")
        else
            os.execute("copy \""..fileIn.."\" \""..fileOut.."\" /n")
        end
    end
    
    return true
end

function lfs.rename(fileIn, new_name)
    local flag = lfs.check_file_exist(fileIn)
    if not flag then
        flag = lfs.check_folder_exist(fileIn)
    end
    if not flag then
        notice.message_lib("lfs.rename", "file: ".. fileIn .. " not exist!", "Error")
        return false
    else
        os.execute("move \""..fileIn.."\" \""..new_name.."\"")
    end

    return true
end

function lfs.check_name_legal(str)
    local illegal_str = {[==[\]==], [==[/]==], [==[:]==], [==[*]==], [==[?]==], [==["]==], [==[<]==], [==[>]==], [==[|]==]}

    for i = 1, #illegal_str do
        if string.find(str, illegal_str[i]) then return false end
    end

    --remove space at the end of str
    while (string.sub(str, -1) == " ") do
        str = string.sub(str, 1, -2)
    end

    return true, str
end

function lfs.copy_folder(folderIn, folderOut)
    folderIn = lfs.get_full_path(folderIn)
    folderOut = lfs.get_full_path(folderOut)
    local flag = lfs.check_folder_exist(folderIn)
    if not flag then
        notice.message_lib("lfs.copy_folder", "Input Folder: ".. folderIn .. " not exist!", "Error")
        return false
    end

    flag = lfs.check_folder_exist(folderOut)
    if not flag then
        notice.message_lib("lfs.copy_folder", "Output Folder: ".. folderOut .. " not exist!", "Error")
        return false
    end
    
    os.execute("copy \""..folderIn.."*.*\" \""..folderOut.."\"")
    
    return true
end

function lfs.xcopy(folderIn, folderOut)
    folderIn = lfs.get_full_path(folderIn)
    folderOut = lfs.get_full_path(folderOut)
    local flag = lfs.check_folder_exist(folderIn)
    if not flag then
        notice.message_lib("lfs.xcopy", "Input Folder: ".. folderIn .. " not exist!", "Error")
        return false
    end

    flag = lfs.check_folder_exist(folderOut)
    if not flag then
        notice.message_lib("lfs.xcopy", "Output Folder: ".. folderOut .. " not exist!", "Error")
        return false
    end
    if string.sub(folderIn, -1) == "\\" then folderIn = string.sub(folderIn, 1, -2) end
    if string.sub(folderOut, -1) == "\\" then folderOut = string.sub(folderOut, 1, -2) end
    --print(folderIn, folderOut)
    os.execute("xcopy \""..folderIn.."\" \""..folderOut.."\" /s /e /y")
    
    return true
end

function lfs.get_filelist(folder, key, keyNotInclude, cutFlag)
    local fileList = {}
    local file
    for file in lfs.dir(folder) do
        if file ~= "." and file ~= ".." then
            if key then
                local cStart, cEnd = string.find(file, key)
                local cStartExt, cEndExt = nil, nil
                if keyNotInclude then cStartExt, cEndExt = string.find(file, keyNotInclude) end
                if nil == cStartExt and cStart then
                    if cutFlag then
                        file = string.sub(file,1,cStart-1)..string.sub(file,cEnd+1)
                    end
                    table.insert(fileList, file)
                end
            else
                table.insert(fileList, file)
            end
        end
    end

    return fileList
end

 lfs.modification = function(file)
    file = lfs.get_full_path(file)
    local attr = lfs.attributes(file)
    if attr ~= nil then
       local date = os.date("%Y%m%d%H%M%S", attr.modification)
       --local time = os.date("%H%M%S", attr.modification)
       return date, attr.modification
    end
    
    return false
end

local function backticks_string(cmd)
	local string
	local pipe = io.popen(cmd)
	local line = pipe:read("*all")
	return line
end

function lfs.get_filename_from_fullpath(path)
    local s = string.find(path, "\\")
    while(s) do
        path = string.sub(path, s+1)
        s = string.find(path, "\\")
    end
    s = string.find(path, "/")
    while(s) do
        path = string.sub(path, s+1)
        s = string.find(path, "/")
    end

    return path
end

function lfs.check_task(image)
    image = lfs.get_filename_from_fullpath(image)
    local cmd = "tasklist"
    local line = backticks_string(cmd)
    if line == nil then return false end

    if string.find(line, image) ~= nil then return true end

    return false
end

function lfs.check_self_conflict(image)
    image = lfs.get_filename_from_fullpath(image)
    local cmd = "tasklist /fo LIST /fi ".."\"Imagename eq "..image.."\""
    local line = backticks_string(cmd)

    if( line == nil ) then
        return false
    end

    local conflict = false
    local str = line
    local image_num = 0
    while string.find(str, image) do
        local i, j = string.find(str, image)
        str = string.sub(str, j+1)
        image_num = image_num + 1 
        if image_num > 1 then
           conflict = true
        end
    end

    --[[
    if conflict then
        print(str)
        i, j = string.find(str, "PID:")
        str = string.sub(str, j+1)
        i, j = string.find(str, "\n")
        local pid = string.sub(str, 1, i-1)
        print(pid, tonumber(pid))
        os.execute("taskkill /f /pid "..tonumber(pid))
    end
    ]]
    return conflict
end

function lfs.run_task(image)
    image = lfs.get_full_path(image)
    os.execute("start "..image)
end

function lfs.kill_task(image)
    image = lfs.get_filename_from_fullpath(image)
    local i, j = string.find(image, ".exe")
    image = i and image or image..".exe"
    print(image)
    os.execute("taskkill /f /im "..image)
end

function lfs.generate_filename_from_str(nameIn, append)
	local name = ""
	for w in string.gmatch(nameIn, "%w+") do
	  name = name .. "_" .. w
	end
	if append then name = name..append end
	return name
end

function lfs.get_filename_form_filedlg(str)
    local index = 1
    local directory, file = "", {}
    repeat
        index = string.find(str, "|")
        local before_index = string.sub(str, 1, (index or 0) - 1)
        local after_index = string.sub(str, (index or 0)+1, -1)
        if string.len(before_index) > 0 then
            table.insert(file, before_index)
        end
        str = after_index
        --print(before_index, after_index)
    until index == nil
    if #file > 1 then 
        directory = file[1].."\\"
        table.remove(file, 1) -- the first one is directory.
    elseif #file == 1 then
        directory = file[1]
        file[1] = lfs.get_filename_from_fullpath(file[1])

        directory = string.sub(directory, 1, string.len(directory) - string.len(file[1]))
    end
    --print(directory, #file)
    --print(directory, unpack(file))
    return directory, file
end

lfs.get_extension = function(file_name)
    file_name = file_name or ""
    local pos = string.len(file_name)
    if pos < 1 then return end
    local str = file_name
    while (string.sub(str, -1) ~= "." and string.len(str)>0) do
        str = string.sub(str, 1, -2)
        pos = pos - 1
    end
    print(pos, file_name)
    
    if pos ~= string.len(file_name) and pos>1 then
        return string.sub(file_name, pos+1, -1)
    end
    return
end
--[[
wait_task_finish = function(exe_name)
    local sec1,sec2 = os.date("%S", os.time())
    local start_time = os.time()
    local calc_state = true
    printc("\n"..exe_name.." is running. Please wait.")
    repeat
        sec2 = os.date("%S", os.time())
        if sec2 - sec1 > 2 or sec2 - sec1 < 0 then
            sec1 = sec2
            local cost_t  =  os.time() - start_time
	        printc(string.format("%d M : %d S", cost_t/60, cost_t%60))
            calc_state = check_task(exe_name) --("BatchCalculatorTool.exe")
            --if calc_state then printc(exe_name.." is running, please wait.") end
        end
    until calc_state == false
end
]]
