-- extend table functions

table = table or {}
table.__help = table.__help or {}


function table.__help.save()
    str =[==[Usage: table.save(table, file)
Save table value to the given file in Lua syntax, which means the table can
be restore by script like:
    NewTable=dofile(file)  -- read the store table to NewTable
Parameter:
    file    --the file name
    table   --the table to be saved
Return:
    true if success, false otherwise
]==]
    print(str)
end

function table.save(tval, file)
	file = lfs.get_full_path(file)
	local function serialize(t)
		local mark={}
		local assign={}

		local underscore=string.byte("_")
		local A=string.byte("A")
		local Z=string.byte("Z")
		local a=string.byte("a")
		local z=string.byte("z")
		local n0=string.byte("0")
		local n9=string.byte("9")

		local function get_valid_key(k)
			for i = 1, #k do
				local ascii=string.byte(k,i)
				--check if k is a valid lua variant name
				if not( (ascii>=A and ascii<=Z) or (ascii>=a and ascii<=z) or (ascii==underscore) or (ascii>=n0 and ascii<=n9 and i~=1) ) then
					return "[ [==["..k.."]==] ]"
				end
			end
			return k
		end

		local function ser_table(tbl, parent, indent)
			mark[tbl]=parent
			local tmp={}
			for k, v in pairs(tbl) do
				v = tbl[k]
				local key= type(k)=="number" and "["..k.."]" or get_valid_key(k)
				if type(v)=="table" then
					local dotkey= parent..(type(k)=="number" and key or "."..key)
					if mark[v] then
						table.insert(assign,dotkey.."="..mark[v])
					elseif k~="__help" then ---we will not save __help which is the help information of the functions.
						table.insert(tmp, key.."="..ser_table(v,dotkey, indent+1))
					end
				elseif (type(v)=="string") then
					table.insert(tmp, key.."=".."[==["..v.."]==]")
				elseif (type(v)~="function") then
					local str=tostring(v)
					if type(v)=="number" then
						if str=="-1.#INF" or str=="-1.#IND" then str="-1e60" end
						if str=="1.#INF" or str=="1.#IND" or str=="1.#QNAN" then str="1e60" end
					end
					table.insert(tmp, key.."="..str)
				end
			end
			local function gen_indent(num)
				local indent="";
				for i = 1, num do
					indent=indent.."\t"
				end
				return indent
			end
			
			local key_table = {} 			
			for i,v in pairs(tmp) do
				table.insert(key_table,v)
			end
			table.sort(key_table)

			return "{\n"..gen_indent(indent)..table.concat(key_table,",\n"..gen_indent(indent)).."\n"..gen_indent(indent).."}"
		end

		return "do local ret="..ser_table(t, "ret", 1)..table.concat(assign, " ").." \n\nreturn ret end"
	end

	local fh= io.open(file, "w+")
	if fh then
		fh:write(serialize(tval))
		io.close(fh)
		return true
	end
	return false
end

function table.__help.load()
    str = [==[
Usage: tab = table.load("d:\\xxx.lua")
if load successfully, function returns a table
otherwise function returns nil
]==]
    print(str)
end

function table.load(filepath)
	filepath = lfs.get_full_path(filepath)
	local r, ndb = pcall(loadstring("return dofile([==["..filepath.."]==])") )
	if type(ndb) ~= "table" then return end -- Must be a table
	local function _sortpna(tbl)
		local numidx = true
		local nidx = {}
		for k, v in pairs(tbl) do
			if type(v) == "table" then
				tbl[k] = _sortpna(v)
			end
			if type(k) ~= "number" then
				numidx = false
			elseif numidx then
				table.insert(nidx, k)
			end
		end
		if not numidx then return tbl end
		table.sort(nidx)
		local _tbl = {}
		for k = 1, #nidx do
			_tbl[nidx[k]] = tbl[nidx[k]]
		end
		return _tbl
	end
	ndb = _sortpna(ndb)
	collectgarbage("collect")
	return ndb
end

local function copy(tabin, tabout)
	if tabin == nil then
		return false
	end
	
	for k, v in pairs(tabin) do
		if type(v) ~= "table" then
			tabout[k] = v
		else
			tabout[k] = tabout[k] or {}
			copy(tabin[k], tabout[k])
		end
	end
end

table.__help.copy = 
[==[
Description:
    Copy all the elements of table to a new table
Parameter:
    tabin -- table want to copy
    tabout -- table copy to
]==]
table.copy = copy


function table.sort_by_id(tabIn, idName)
	local tmpTable = {}
	for i=1,#tabIn do
		tmpTable[tabIn.idName] = tabIn[i]
	end
	
	local tabOut = {}
	for k, v in pairs(tmpTable) do
		table.insert(tabOut, v)
	end

	return tabOut
end

function table.dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k,v in pairs(o) do
            if type(k) ~= 'number' then k = '"'..k..'"' end
            s = s .. '['..k..'] = ' .. table.dump(v) .. ',\n'
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

--[[
index: nil: search by number ID.
       string: search string index.
]]
function table.get_id(tableIn, value, index)
    index = index or 1
    local id = 0
    for i = 1, #tableIn do
        local valueTab = tableIn[i]
        if type(index) == "string" then valueTab = tableIn[i][index] end
        if valueTab == value then 
            id = i
            break
        end
    end

    return id
end

--item type is string
function table.remove_item(tableIn, item)
    for k, v in pairs(tableIn) do
        if k == item then
            tableIn[k] = nil
        else
            if type(v) == "table" then
                table.remove_item(v, item)
            end
        end
    end
end