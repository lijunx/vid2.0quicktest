require "Wave"

require "libs.Wave.filter"
require "libs.Wave.delay"

Wave = Wave or {}
Wave.__help = Wave.__help or {}
--2019/02/28 add cut_sinetone_sync function is written by yichao
--Description:
--Return Value: convert the appointed time of wavefile to sample
--**************************************************--
function Wave.get_wave_property (path, filename)
    local wavefile = path..filename
    local wavedata = Wave.load(wavefile)
    local wave_property = {}
    wave_property.sample_rate = wave.get_property(wavedata, "sample_rate")
    wave_property.bit_rate = wave.get_property(wavedata, "bit_per_sample")
    wave_property.wave_length = wave.get_len(wavedata)
    --wave.clearWavdata(wavedata)
    Wave.clearWavdata(wavedata)

    return wave_property
end

--2019/02/26 add cut_sinetone_sync function is written by yichao 
function Wave.cut_sinetone_sync(orgpath, wav, length, powerthd)
    local wavedata_cut
    local wavedata = Wave.load(orgpath..wav)      ---save original files in CUT folder
   
    if nil==wavedata then 
        print("failed to load "..wav)
	    io.read()
	end
	local array_wav = wave.n_to_mono(wavedata)
	Wave.clearWavdata(wavedata)
	--------add a 800hz HFP to filter the low freq power ----------------
	local coef_b, coef_a  --to be initialize
	coef_b = {0.606600151420807,-2.426400605683227,3.639600908524840,-2.426400605683227,0.606600151420807}
	coef_a = {1.000000000000000,-3.100413839410381,3.715646459708122,-2.031400065376537,0.433235156874514}
	local array_wav_tmp = wave.filter(array_wav[1], coef_b, coef_a)
	------------------------------------------------------------
	local pos1= wave.silence_len(array_wav_tmp, powerthd)
	Wave.clearWavdata(array_wav_tmp)
	print("silence length", pos1)
	local pos_total_length = wave.get_len(array_wav[1])
	print("long wave length is: "..pos_total_length/1000)
    --local pos2 = pos1 + length
    local pos2 = length
	local wave_cut_array = {}
	if pos2 <= pos_total_length then
		print("cut from ", pos1, "to", pos2)
		for i = 1, #array_wav do
			wave_cut_array[i] = wave.cut(array_wav[i], pos1, pos2)
		end
        wavedata_cut = wave.mono_to_n(wave_cut_array)
        --new_file = orgpath.."\\cut\\"..wav.."_rec.wav"
		--local rst = wave.save(new_file, wavedata_cut)
		for i = 1, #array_wav do
			Wave.clearWavdata(array_wav[i])
			Wave.clearWavdata(wave_cut_array[i])
		end
		--wave.clearWavdata(wavedata_cut)
		return wavedata_cut
	else
		print("the wave power is too weak!")
	end
end

function Wave.clearWavdata(wavdata)
    if nil == wavdata then return end
    
    if "table" == type(wavdata) then
        for i=1, #wavdata do
            Wave.delete(wavdata[i])
        end
    elseif wavdata then
        Wave.delete(wavdata)
    end
    wavdata = nil
    collectgarbage("collect")
end

function Wave.load_safe(filename, type, channelNum, samplerate, bitLen)
    type = type or 1
    filename = lfs.get_full_path(filename)

    if type == 2 then
        return Wave.load(filename, type, channelNum, samplerate, bitLen)
    else
        return Wave.load(filename, type)
    end
end

function Wave.save_safe(filename, wave_data, wave_format)
    wave_format = wave_format or 1
    filename = lfs.get_full_path(filename)

    return Wave.save(filename, wave_data, wave_format)
end

function Wave.check_wavefile(filename, type, channelNum, samplerate, bitLen)
    local flag = lfs.check_file_exist(filename)
    if false == flag then
        notice.message_lib(" Wave.check_wavefile", "file: ".. filename .. "not exist!", "Error")
        return false
    end
    
    local wave_data = Wave.load_safe(filename, type, channelNum, samplerate, bitLen)
    if nil == wave_data then
        notice.message_lib(" Wave.check_wavefile", "file: ".. filename .. "is not valid wave file!", "Error")
        return false
    end
    Wave.delete(wave_data)
    collectgarbage("collect")

    return true
end

-- only for simple x or / a integer value
function Wave.resample(wavIn, dstFs)
    local fs = Wave.get_samplerate(wavIn)
    if dstFs == fs then return wavIn end

    local wavOut
    if fs > dstFs then -- downsample
        local downfactN = math.floor(fs/dstFs)
        if downfactN*dstFs ~= fs then
            notice.message_lib("Wave.resample", "Only support integer div", "Error")
            return nil
        end
        
        if 8000 == dstFs then
            local wavFilter = Wave.lowpass_4k(wavIn)
            wavOut = Wave.downsample_simple(wavFilter, downfactN)
            Wave.clearWavdata(wavFilter)
            collectgarbage("collect")
        elseif 16000 == dstFs then
            local wavFilter = Wave.lowpass_8k(wavIn)
            wavOut = Wave.downsample_simple(wavFilter, downfactN)
            Wave.clearWavdata(wavFilter)
            collectgarbage("collect")
        else
            wavOut = Wave.downsample_simple(wavIn, downfactN)
        end
    else -- upsample
        local upfactN = math.floor(dstFs/fs)
        if dstFs ~= fs*upfactN then
            notice.message_lib("Wave.resample", "Only support integer multi", "Error")
            return nil
        end
        
        local wavResample = Wave.upsample_simple(wavIn, upfactN)
        if 8000 == fs then
            wavOut = Wave.lowpass_4k(wavResample)
            Wave.clearWavdata(wavResample)
            collectgarbage("collect")
        elseif 16000 == fs then
            wavOut = Wave.lowpass_8k(wavResample)
            Wave.clearWavdata(wavResample)
            collectgarbage("collect")
        else
            return wavResample
        end
    end

    return wavOut
end

--**************************************************--
--Function Name: Wave.get_channel
--Description: 
--Return Value: the appointed channel of the wave data
--Parameter:
--  wav: wave data.
--Usage: new_wave_data = Wave.get_channel(wave_data, slot)
--**************************************************--
function Wave.get_channel(wav, slot)
    local channel_num = wave.get_property(wav, "channel_num")
    if channel_num < slot then
        notice.message_lib("Wave.get_channel", "The wave data do not has "..slot.." channel.", "Error")
        return nil
	end
    
    local tmpWavSplit = Wave.split(wav)

    local wavOut = nil
    for i=1,#tmpWavSplit do
        if slot == i then
            wavOut = tmpWavSplit[i]
        else
            Wave.clearWavdata(tmpWavSplit[i])
            collectgarbage("collect")
        end
    end
    collectgarbage("collect")

    return wavOut
end

--**************************************************--
--Function Name: Wave.power
--Description: Calculate the mean power of wave data.
--Return Value: Power of the wave.(in dB)
--Parameter: 
--  wave_data: Indicate the wave data that will be calculated.
--             Must be a mono wave.
--  start_time: Indicated the start time, in second.
--  end_time: Indicated the end time, in second.
--  cut_off_frequency: Indicated whether need high pass filter.
--Usage: power = Wave.power(wave_data, true)
--**************************************************--
function Wave.power_rmdc(waveIn) -- will be add to C++ code in future
    local tmpWav = wave.remove_dc(waveIn)
    local pow = Wave.power(tmpWav)
    Wave.clearWavdata(tmpWav)
    collectgarbage("collect")
    
    return pow
end

function Wave.Dat2Wav(datFile, wavFile)
    local loadWav = Wave.load_safe(datFile, 3)
    if nil ==  loadWav then
        notice.message_lib("Wave.Dat2Wav", "Input file: " .. datFile .. " is not a valid ACQUA dat file", "Error")
        return false
	end
    
    local splitWav = Wave.split(loadWav)
    Wave.clearWavdata(loadWav)

    local peakMax = -10000
    for i=1,#splitWav do
        peakMax = math.max(peakMax, Wave.peak(splitWav[i]))
    end

    for i=1,#splitWav do
        splitWav[i] = Wave.amplify(splitWav[i], -peakMax)
    end
    print("peak of " .. datFile .. " is " .. peakMax)
    local saveWav = Wave.merge(splitWav)
    Wave.clearWavdata(splitWav)

    Wave.save_safe(wavFile, saveWav)
    Wave.clearWavdata(saveWav)
    collectgarbage("collect")
    
    return true
end

--**************************************************--
--Function Name: Wave.cut
--Description: cut the wave from start time to end time.
--Return Value: new wave data
--Parameter:
--  wave_data: the wave_data need to be cut. can be n-channels wave.
--  start_time: the start time of the old wave_data, in second or sample.
--              if this parameter is negative, the file will be
--                calculated from the beginning.
--  end_time: the end time of the old wave_data, in second or sample.
--              if this parameter is negative, the file will be
--                calculated from the end.
--  cut_unit:  1 or "ms" : Default value is second
--             2 or "sample": sample.
--Usage: new_wave_data = Wave.cut(wave_data, 1000, 3000)
--       new_wave_data = Wave.cut(wvae_data, 10, 1000, "sample")
--**************************************************--
function Wave.cut(wave_data, start_time, end_time, cut_unit)
    cut_unit = cut_unit or 1
    if cut_unit == 2 or cut_unit == "sample" then
        return wave.cut(wave_data, start_time, end_time, cut_unit)
    end
    
    local time_last = wave.get_property(wave_data, "time_last")
    local t=math.round(time_last*10)/10
    print("cutwave time last is .."..t)
	if start_time > t*1000 then
	    printc("function wave_cut() ERROR: time started is longer than time last !")
	    assert(false, "")
	    return wave_data
	end
	
	if end_time > t*1000 then
	    end_time = t*1000
    end
    print("start time is... "..start_time)
    print("end time is... "..end_time)
	local cutwave=wave.cut(wave_data, start_time, end_time)
	return cutwave
end

-- should have the same fs
function Wave.align(record, source, delayCheckStartS, delayCheckEndS)
    local sampleLenSource = Wave.get_samplenum(source)
    local sampleLenRecord = Wave.get_samplenum(record)
    if sampleLenRecord <= sampleLenSource then return record end
    local fs = Wave.get_samplerate(source)
    
    local cutStartSample = 0
    local cutEndSample = sampleLenSource
    if delayCheckStartS then cutStartSample = delayCheckStartS*fs end
    if delayCheckEndS then cutEndSample = delayCheckEndS*fs end
    
    local cutSourceTmp = Wave.cut(source, cutStartSample, cutEndSample, 2)
    cutEndSample = cutEndSample + sampleLenRecord - sampleLenSource
    local cutRecordTmp = Wave.cut(record, cutStartSample, cutEndSample, 2)
    
    local tmpDelaySample = Wave.delay(cutRecordTmp, cutSourceTmp, "sample")
    Wave.clearWavdata(cutSourceTmp)
    Wave.clearWavdata(cutRecordTmp)
    collectgarbage("collect")
    
    cutStartSample = tmpDelaySample
    cutEndSample = tmpDelaySample + sampleLenSource
    if cutEndSample > sampleLenRecord then
        cutStartSample = sampleLenRecord - sampleLenSource + 1
        cutEndSample = sampleLenRecord
    end
    local alignRecord = Wave.cut(record, cutStartSample, cutEndSample, 2)
    
    return alignRecord
end

-- only for single channel
function Wave.get_speach_idx(wavdata)
    local ch = Wave.get_channelnum(wavdata)
    if ch > 1 then
        print("ERROR! Wave.get_speach_idx only support single channel wavedata")
        return false
    end

    local wavLen = Wave.get_samplenum(wavdata)
    local fs = Wave.get_samplerate(wavdata)
    local slotLenS = 0.2

    local slotLen = math.round(slotLenS*fs)
    local slotNum = math.floor(wavLen/slotLen)
    slotLenS = slotLen/fs

    local levelCurve = {}
    local startPos, endPos = 1, slotLen
    for i=1,slotNum do
        local data = wave.get_sample(wavdata, startPos, endPos)
        levelCurve[i] = math.abs_mean(data)
        startPos, endPos = startPos+slotLen, endPos+slotLen
    end
    collectgarbage("collect")
    
    local th = 0.2*math.mean(levelCurve)
    local speechIdx = {}
    for i=1,(slotNum-1) do
        if levelCurve[i]< th and levelCurve[i+1] >= th then
            speechIdx[#speechIdx+1] = {}
            speechIdx[#speechIdx][1] = i*slotLenS
        end
        if levelCurve[i]>= th and levelCurve[i+1] < th then
            speechIdx[#speechIdx][2] = i*slotLenS
        end
    end
    levelCurve = {}
    collectgarbage("collect")

    local timeLastTH = 0.3
    for i=#speechIdx,1,-1 do
        if speechIdx[i][2] - speechIdx[i][1] < timeLastTH then
            table.remove(speechIdx, i)
        end
    end
    
    return speechIdx
end

function Wave.peak_to_fullscale(waveIn)
    local peak = Wave.peak(waveIn)
    local waveOut = Wave.amplify(waveIn, -peak-1)
    return waveOut
end

function Wave.merge_silence(mono_in, max_ch, mono_ch)
	local sample_num  = Wave.get_samplenum(mono_in)
	local sample_rate = Wave.get_samplerate(mono_in)
	local bit_len     = Wave.get_bitlen(mono_in)
	local silence = Wave.gen_silence(sample_num, 1, sample_rate, bit_len, 2)
	local tab = {}
	for i = 1, max_ch do
		if i == mono_ch then table.insert(tab, mono_in)
		else table.insert(tab, silence) end
	end
	local wav_out = Wave.merge(tab)
	Wave.delete(silence)
	return wav_out
end

--[[
    sort freq data into the order user want,
    order: 1 (small to large) default
           2 (large to small)
]]
local function sort_freq (freqpoint, freq, order)
    if order == nil then order = 1 end
    local len = #freqpoint
    local idx_arr = {}
    for i = 1, len do table.insert(idx_arr, i) end

    local function cmp_function(v1, v2)
        if order == 1 then return freqpoint[v1] < freqpoint[v2]
        else return freqpoint[v2] < freqpoint[v1] end
    end
    table.sort(idx_arr, cmp_function)

    local new_freqpoint = {}
    local new_freq      = {}
    for i = 1, len do
        table.insert(new_freqpoint, freqpoint[idx_arr[i] ])
        table.insert(new_freq,      freq[idx_arr[i] ])
    end

    return new_freqpoint, new_freq
end

-- suppose frequency point low to high
local function merge_freqpoint(freqpoint, freqdata, ExFreqpoint)
    collectgarbage ("collect")
	local CurveOut = {}
	CurveOut[1] = {}
	CurveOut[2] = {}

	local pos1, pos2 = 1, 1
	if freqpoint[1] ~= nil and ExFreqpoint[1] ~= nil then
		while(math.round(1000*ExFreqpoint[pos2]) < math.round(1000*freqpoint[1])) do
			table.insert(CurveOut[1], ExFreqpoint[pos2])
			table.insert(CurveOut[2], 0)
			pos2 = pos2 + 1
			if pos2 > #ExFreqpoint then
				break
			end
		end
	end

	local dvalue
	while(pos1 <= #freqpoint and pos2 <= #ExFreqpoint) do
	    dvalue = 0
	    dvalue = math.round(1000*freqpoint[pos1]) -math.round(1000*ExFreqpoint[pos2])  --common.CompareA2B(freqpoint[pos1],ExFreqpoint[pos2])
		if dvalue  == 0 then
			table.insert(CurveOut[1], freqpoint[pos1])
			table.insert(CurveOut[2], freqdata[pos1])
			pos1 = pos1 + 1
			pos2 = pos2 + 1
		elseif dvalue  > 0 then

			table.insert(CurveOut[1], ExFreqpoint[pos2])
			table.insert(CurveOut[2], math.get_ypos_online(freqpoint[pos1-1], freqdata[pos1-1], freqpoint[pos1], freqdata[pos1], ExFreqpoint[pos2]))
			pos2 = pos2 + 1

		else
			table.insert(CurveOut[1], freqpoint[pos1])
			table.insert(CurveOut[2], freqdata[pos1])
			pos1 = pos1 + 1
		end
	end

	while pos1 <= #freqpoint do
		table.insert(CurveOut[1], freqpoint[pos1])
		table.insert(CurveOut[2], freqdata[pos1])
		pos1 = pos1 + 1
	end

	while pos2 <= #ExFreqpoint do
		table.insert(CurveOut[1], ExFreqpoint[pos2])
		table.insert(CurveOut[2], 0)
		pos2 = pos2 + 1
	end

	collectgarbage ("collect")
	return CurveOut[1], CurveOut[2]
end

function Wave.merge_spectrum(FreqPointA, FreqA, FreqPointB, FreqB, func, param)
	-- parameter handle
	local function_list = {
		[1] = function(A, B)	return A+B end,
		[2] = function(A, B)	return A-B end,
		[3] = function(A, B)	return A*B end,
		[4] = function(A, B)	return A/B end,
	}
	local function handle_param(param)
		local fullparam = {}
		-- bXLog
		if param == nil or param.bXLog == nil then
			fullparam.bXLog = false
		else
			fullparam.bXLog = param.bXLog
		end
		-- bXExtend
		if param == nil or param.bXExtend == nil then
			fullparam.bXExtend = false
		else
			fullparam.bXExtend = param.bXExtend
		end
		return fullparam
	end
	param = handle_param(param)
	if type(func) == "number" then param.Func = function_list[func]
	else param.Func = func end
	-- sort freq low to high
	FreqPointA, FreqA = sort_freq(FreqPointA, FreqA)
	FreqPointB, FreqB = sort_freq(FreqPointB, FreqB)
	-- transform to log space if need
	if param.bXLog == true then
		for i = 1, #FreqPointA do FreqPointA[i] = math.log10(FreqPointA[i]) end
		for i = 1, #FreqPointB do FreqPointB[i] = math.log10(FreqPointB[i]) end
	end
	-- extend A & B
	local FreqPointAL, FreqAL, FreqPointBL, FreqBL
	FreqPointAL, FreqAL = merge_freqpoint(FreqPointA, FreqA, FreqPointB)
	FreqPointBL, FreqBL = merge_freqpoint(FreqPointB, FreqB, FreqPointA)

	collectgarbage ("collect")

	-- calcu freq range
	local freq_ref
	if param.bXExtend == true then freq_ref = FreqPointAL
	else freq_ref = FreqPointA end
	-- calcu data
	local pos1 = 1 --pos for new data
	local pos2 = 1 --pos for extended A&B
	local Len1 = #freq_ref
	local Len2 = #FreqPointAL
	local new_data = {}
	while pos2 <= Len2 and freq_ref[Len1] ~= nil do
		if math.round(100*FreqPointAL[pos2]) > math.round(100*freq_ref[Len1]) then break end
		if math.round(100*FreqPointAL[pos2]) < math.round(100*freq_ref[pos1]) then
			pos2 = pos2 + 1
		else
			table.insert(new_data, param.Func(FreqAL[pos2], FreqBL[pos2]))
			pos1 = pos1 + 1
			pos2 = pos2 + 1
			if pos1 > # freq_ref then break end
		end
	end
	-- transform from log space if need
	if param.bXLog == true then
		for i = 1, #freq_ref do freq_ref[i] = 10^(freq_ref[i]) end
	end
	collectgarbage ("collect")
	return freq_ref, new_data
end
Wave.__help.merge_spectrum = [==[
description: merge two spectrum data, using specified function
return: freqpoint -- new freqpoint array
        freq -- new freqdata array
parameter: FreqPointA -- first frequency point
		   FreqA -- first frequency data
		   FreqPointB -- second frequency point
		   FreqB -- second frequency data
		   func -- 1(+), 2(-), 3(*), 4(/) or a function formated like this:
				   function xxx(A, B) return A+B end
		   param -- a table contains parameters
				{
					bXExtend -- false if merged spectrum keeps freqpoint as A
					bXLog -- true if using log space, false if using linear space
				}
]==]

wave.merge_freqpoint = merge_freqpoint