clock_config = clock_config or {}
clock_config.path = {
    general_report = NOAH_PATH.."Report\\",
}


clock_config.Headset_copy_range = {
    start_row = 2,
    start_copy_column = 6,
    end_row = 21, 
    end_copy_column = 6,
    start_paste_column = 5,
    end_paste_column = 5
}

clock_config.Automotive_copy_range = {
    start_row = 2,
    start_copy_column = 9,
    end_row = 21, 
    end_copy_column = 9,
    start_paste_column = 8,
    end_paste_column = 8
}

clock_config.EchoLite_copy_range = {
    start_row = 2,
    start_copy_column = 12,
    end_row = 21, 
    end_copy_column = 12,
    start_paste_column = 11,
    end_paste_column = 11
}

clock_config.PP_copy_range = {
    start_row = 2,
    start_copy_column = 3,
    end_row = 38,
    end_copy_column = 3,
    start_paste_column = 2,
    end_paste_column = 2
}

clock_config.item = {
    Delay,
    Output_Level,
    Noise_floor,
    Noise_suppression,
    Convergence_time,
    PESQ,
    AEC_on,
    AEC_off,
    LDO,
    MIPS,
    Current_AVD,
    Current_xSD,
    Current_SD,
    Current_Nmode,
    POLQA_Nobgn,
    POLQA_Mensa,
    POLQA_Car,
    POLQA_Train,
    POLQA_Road,
}

clock_config.start_row = 3
clock_config.end_row = 21
clock_config.Headset_column = 6
clock_config.Automotive_column = 9
clock_config.EchoLite_column = 12

clock_config.column = {
    Headset = 6,
    Automotive = 9,
    EchoLite = 12
}

clock_config.basic_item = {
    "Delay", "Output_level", "Noise_floor", "Noise_suppression", "Convergence_time", "PESQ", "AEC_on", "AEC_off", "LDO", "MIPS", "Current_Nmode"
}

clock_config.current_item = {
    "AVD", "xSD", "SD"
}

clock_config.POLQA_item = {
    "Nobgn", "Mensa", "Car", "Train", "Road"
}


clock_config.basic_report_range = {
    Delay = {row = 7, column = 2},
    Output_level = {row = 7, column = 3},
    Noise_floor = {row = 7, column = 6},
    Noise_suppression = {row = 8, column = 7},
    Convergence_time = {row = 7, column = 9},
    PESQ = {row = 7, column = 10},
    AEC_on = {row = 7, column = 12},
    AEC_off = {row = 7, column = 13},
    LDO = {row = 7, column = 15},
    MIPS = {row = 7, column = 16},
    Current_Nmode = {row = 7, column = 17},
}

clock_config.PSM_current_report_range = {
    AVD = {row = 4, column = 1},
    xSD = {row = 4, column = 3},
    SD = {row = 4, column = 5}
}


clock_config.POLQA_report_range = {
    Nobgn = {row = 4, column = 2},
    Mensa = {row = 4, column = 3},
    Car = {row = 4, column = 4},
    Train = {row = 4, column = 5},
    Road = {row = 4, column = 6}
}

clock_config.PP_report_range = {
    start_row = 3, end_row = 38, column = 2
}



