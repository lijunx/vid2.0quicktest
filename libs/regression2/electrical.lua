regression = regression or {}

local electrical = {}

function electrical.save_status_info()
    local readTable = {
        {0x0FFFFF22, 0x2},
        {0x0FFF9F68, 0x4},
        {regression.config.baseaddr, 0x2},
        {regression.config.baseaddr + 0x4, 0x2},
        {regression.config.baseaddr + 0x26, 0x2},
        {0x0FFE2B00, 0x2},
        {0x0FFE2B04, 0x2},
        {0x0FFE2B32, 0x2},
        {0x0FFE2B34, 0x2},
        {0x0FFF9FD0, 0x2},
        {0x0FFF9FD4, 0x2},
        {0x0FFF9FD8, 0x2},
        {0x0FFFBFF8, 0x2},
        {0x0FFFBFF9, 0x2},
        {0x0FFFBFFA, 0x2},
        {0x0FFFBFFB, 0x2},
        {0x0FFFBFFF, 0x2}
    }
    local result = {{},{},{}}
    for i=1,#readTable do
        local flagRead, value = iM501.read_dram(readTable[i][1], readTable[i][2])
        result[1][i] = string.format("0x%X", readTable[i][1])
        result[2][i] = string.format("0x%X", readTable[i][2])
        result[3][i] = string.format("0x%X", value)
    end

    local config = {
        name = [==[DUT Parameter Running]==], module = "electrical",
        report = {type = "matrix", sheet = "Parameter Running", lineStart = 2, col = {1,2,3}}
    }
    
    regression.report(config, result)
end

function electrical.MIPS(processed, unprocessed, source, analysis, delayS, fs)
    local addr = 0x0FFFFF32
    local flagRead, value = iM501.read_dram(addr, 0x2)
    --local mips = value%256 
    local mips = (math.floor(value/256))%256 + 1
    return mips
end
function electrical.mips(processed, unprocessed, source, analysis, delayS, fs)
    --Nor
    local  config = {
                report = {type = "matrix", sheet = "mips", lineStart = 3, col = {2,3}}
            }
    local normips = {}
    local psmmips = {}
    local addr = 0x0FFF9F50
    local  _, baseaddr = iM501.read_dram(0x0FFF9F50,0x4)
    local  _, vosmips = iM501.read_dram(baseaddr+0x28,0x2)
    local  _, venmips = iM501.read_dram(baseaddr+0x28+0x44,0x2)
    local  _, xaamips = iM501.read_dram(baseaddr+0x28+0x44*2,0x2)
    local  _, xsdmips = iM501.read_dram(baseaddr+0x28+0x44*3,0x2)
    local  _, sdmips = iM501.read_dram(baseaddr+0x28+0x44*4,0x2)
    normips = {vosmips,venmips,xaamips,xsdmips,sdmips}
     --PSM
    local addr = 0x0FFF9F54
    local  _, baseaddr_p = iM501.read_dram(0x0FFF9F54,0x4)
    local  _, vosmips_p = iM501.read_dram(baseaddr_p+0x28,0x2)
    local  _, venmips_p = iM501.read_dram(baseaddr_p+0x28+0x44,0x2)
    local  _, xaamips_p = iM501.read_dram(baseaddr_p+0x28+0x44*2,0x2)
    local  _, xsdmips_p = iM501.read_dram(baseaddr_p+0x28+0x44*3,0x2)
    local  _, sdmips_p = iM501.read_dram(baseaddr_p+0x28+0x44*4,0x2)
    psmmips = {vosmips_p,venmips_p,xaamips_p,xsdmips_p,sdmips_p}

    regression.report(config,{normips,psmmips})

end

function electrical.LDO(processed, unprocessed, source, analysis, delayS, fs)
    local addr = 0x0FFFFF22
    if 16000 == fs then addr = 0x0FFFFF22 end
    local flagRead, value = iM501.read_dram(addr, 0x2)

    local ldoValue = (math.floor(value/64))%16
    ldoValue = ldoValue*0.05+0.9
    ldoValue = math.min(1.5, ldoValue)

    return ldoValue
end

function electrical.Current()
    local AM = device[regression.config.meter]
    
    local pam, meter_port = AM.find_COM()
    pam = AM.connect(meter_port)
    local value, unit = AM.query_measurement(pam)
    AM.disconnect(pam)
    
    return value
end
 
local function switch_to_PSM(fastdowncode)
    if fastdowncode == 1 then 
        iM501.enter_PSM(2,12000)
    elseif fastdowncode == 2 then 
        iM501.enter_PSM(1,100)
    elseif fastdowncode == 3 then 
        iM501.enter_PSM(1,100)
    elseif fastdowncode == 4 then 
        iM501.enter_PSM(1,100)
    end
end
local function switch_to_NM(fastdowncode)
    if fastdowncode == 1 then 
        iM501.enter_NORMAL(2,12000)
    elseif fastdowncode == 2 then 
        iM501.enter_NORMAL(1,100)
    elseif fastdowncode == 3 then 
        iM501.enter_NORMAL(1,100)
    elseif fastdowncode == 4 then 
        iM501.enter_NORMAL(1,100)
    end
    local a,b=iM501.read_dram(0xfff9fc0,0x2)
    wait(0.1)
    local c,d=iM501.read_dram(0xfff9fc0,0x2)
    if d==b then 
       print("DSP dead...")
       io.read()
       --assert(false,"")
    end
end
-- AVD current test 
local loopTmp = 1
local config={
     report = {type = "value", sheet = "PSM_current", line = 4, col = 1, linIncrease=true}
}
function electrical.PSM_Current_AVD()  -- for VID 2.0

    local fastdowncode=regression.config.moduleSwitch.fastdowncode
    local codepath = regression.config.PATH.DSP.."AVD\\"
    iM501.init(codepath,"twoMic",16000,fastdowncode)
    wait(1)
    switch_to_PSM(fastdowncode)
    wait(0.1)
    while loopTmp < 6 do   -- means read 5 times 
        local avd_current=electrical.Current()
        while avd_current > 0.5  do
            avd_current=electrical.Current()
            if avd_current < 0.5 then break end
        end
        regression.report(config, avd_current)
        loopTmp = loopTmp+1
    end
    switch_to_NM(fastdowncode)
    wait(0.5)
end
--  xSD current test 
local loopTmp = 1
local config={
    report = {type = "value", sheet ="PSM_current", line = 4, col = 2,linIncrease=true}
}
local config1 = {
    report = {type = "value", sheet = "PSM_current", line = 4, col = 3,linIncrease=true}
}
function electrical.PSM_Current_xSD()  -- for VID 2.0
    local fastdowncode=regression.config.moduleSwitch.fastdowncode
    local codepath = regression.config.PATH.DSP.."xSD\\"

    iM501.init(codepath,"twoMic",16000,fastdowncode)
    wait(1)
    local addr = 0x0FFF9F54
    local  _, baseaddr_p = iM501.read_dram(0x0FFF9F54,0x4)
    --xSD  MIPS check 
    local  _, xsdmips_p = iM501.read_dram(baseaddr_p+0x28+0x44*3,0x2)
    switch_to_PSM(fastdowncode)
    wait(0.1)
    while loopTmp < 6 do 
        local xSD_current=electrical.Current()
        while xSD_current > 2  do
            avd_current=electrical.Current()
            if avd_current < 2 then break end
        end

        regression.report(config, xsdmips_p)
        regression.report(config1, xSD_current)
        loopTmp = loopTmp+1
    end
    switch_to_NM(fastdowncode)
    wait(0.5)
end
-- SD current test 
local loopTmp = 1
local config={
    report = {type = "value", sheet ="PSM_current", line = 4, col = 4,linIncrease=true}
}
local config1 = {
     report = {type = "value", sheet = "PSM_current", line = 4, col = 5,linIncrease=true}
}
function electrical.PSM_Current_SD()  -- for VID 2.0
    local fastdowncode=regression.config.moduleSwitch.fastdowncode
    local codepath = regression.config.PATH.DSP.."SD\\"

    iM501.init(codepath,"twoMic",16000,fastdowncode)
    wait(1)
    local addr = 0x0FFF9F54
    local  _, baseaddr_p = iM501.read_dram(0x0FFF9F54,0x4)
    --SD  MIPS check 
    local  _, sdmips_p = iM501.read_dram(baseaddr_p+0x28+0x44*4,0x2)
    switch_to_PSM(fastdowncode)
    wait(0.1)
    while loopTmp < 6 do 
        local SD_current=electrical.Current()
        while SD_current > 5  do
            avd_current=electrical.Current()
            if avd_current < 5 then break end
        end
        regression.report(config, sdmips_p)
        regression.report(config1, SD_current)
        loopTmp = loopTmp+1
    end
    switch_to_NM(fastdowncode)
    wait(0.5)
end

regression.electrical = regression.electrical or electrical