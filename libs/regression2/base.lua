local function switch_dram_bit(switch, switchOn, isReverse)
    local _, tmpValue = iM501.read_dram(regression.config.baseaddr + switch.byteOffset)
    if isReverse then
        if bit.band(tmpValue, bit.lshift(1,switch.bitPos)) > 0 then -- current on
            switchOn = false
        else
            switchOn = true
        end
    end

    if switchOn then
        tmpValue = bit.bor(tmpValue, bit.lshift(1,switch.bitPos))
    else
        tmpValue = bit.band(tmpValue, bit.bnot(bit.lshift(1,switch.bitPos)))
    end

    iM501.write_dram(regression.config.baseaddr + switch.byteOffset, tmpValue)

    return switchOn
end


function regression.switch(name, switchOn, isReverse)
    print("regression.switch>>>>>>>>>>>>>>>>>>>>>>", name, switchOn)
    local switch = regression.config.im501param.switch[name]
    if "dram" == switch.method and "bit" == switch.type then
        switchOn = switch_dram_bit(switch, switchOn, isReverse)
        return switchOn
    end
    
    if "message" == switch.method then
        iM501.write_message(switch.cmd, switch[switchOn])
        iM501.read_dram(switch.verify)
    end
end

function regression.tune(name, method, value, isRecover)
    --[[
        method: 'set', 'add', 'sub', 'multi', 'div'
    --]]
    local tune = regression.config.im501param.tune[name]
    local flagRead, valueCurrent
    if "set" == method then
        if isRecover then
            iM501.write_dram(regression.config.baseaddr + tune.byteOffset, tune.oldSetValue)
            tune.oldSetValue = nil
        else
            flagRead, tune.oldSetValue = iM501.read_dram(regression.config.baseaddr + tune.byteOffset, 0x2)
            iM501.write_dram(regression.config.baseaddr + tune.byteOffset, value)
        end 
        return
    end
    
    if isRecover then
        local inverseTbl = {
            add = "sub",
            sub = "add",
            multi = "div",
            div = "multi"
        }
        method = inverseTbl[method]
    end
    flagRead, valueCurrent = iM501.read_dram(regression.config.baseaddr + tune.byteOffset, 0x2)
    if "add" == method then
        iM501.write_dram(regression.config.baseaddr + tune.byteOffset, math.floor(valueCurrent+value))
    elseif "sub" == method then
        iM501.write_dram(regression.config.baseaddr + tune.byteOffset, math.floor(valueCurrent-value))
    elseif "multi" == method then
        iM501.write_dram(regression.config.baseaddr + tune.byteOffset, math.floor(valueCurrent*value))
    elseif "div" == method then
        iM501.write_dram(regression.config.baseaddr + tune.byteOffset, math.floor(valueCurrent/value))
    end
end

function regression.source_load(waveConfig, path, fs)
    if nil == waveConfig then return nil end

    local tmpWav = Wave.load(path..waveConfig.file, waveConfig.type)
    if nil == tmpWav then tmpWav = Wave.load(waveConfig.file, waveConfig.type) end
    if nil == tmpWav then return nil end

    local originalFs = Wave.get_samplerate(tmpWav)
    fs = fs or originalFs

    local tmpSplit
    if originalFs == fs then
        tmpSplit = Wave.split(tmpWav)
    else
        local tmpDownWav = Wave.downsample_simple(tmpWav, originalFs/fs)
        tmpSplit = Wave.split(tmpDownWav)
        Wave.clearWavdata(tmpDownWav)
        collectgarbage("collect")
    end
    Wave.clearWavdata(tmpWav)
    collectgarbage("collect")

    local sourceTmp = nil
    for i=1,#tmpSplit do
        if i == waveConfig.ch then
            sourceTmp = tmpSplit[i]
        else
            Wave.clearWavdata(tmpSplit[i])
            collectgarbage("collect")
        end
    end
    collectgarbage("collect")

    return sourceTmp
end

local function play_wave_gen(vector, sourcePath, chDef, fs)
    local waveMic0 = regression.source_load(vector.mic0, sourcePath, fs)
    local waveMic1 = regression.source_load(vector.mic1, sourcePath, fs)
    local waveMic2 = regression.source_load(vector.mic2, sourcePath, fs)
    local waveMic3 = regression.source_load(vector.mic3, sourcePath, fs)
    local waveAecrefL = regression.source_load(vector.aecrefL, sourcePath, fs)
    local waveAecrefR = regression.source_load(vector.aecrefR, sourcePath, fs)

    INPUT_FILE_CHANNEL = #chDef
    INPUT_FILE_ARRAY = "playtmp.wav"
    FILE_LOUT_NAME = "rectmp.wav"

    local waveMatrix = {}
    for i=1,INPUT_FILE_CHANNEL do
        if "mic0" == chDef[i] then waveMatrix[i] = waveMic0 end
        if "mic1" == chDef[i] then waveMatrix[i] = waveMic1 end
        if "mic2" == chDef[i] then waveMatrix[i] = waveMic2 end
        if "mic3" == chDef[i] then waveMatrix[i] = waveMic3 end
        if "aecrefL" == chDef[i] then waveMatrix[i] = waveAecrefL end
        if "aecrefR" == chDef[i] then waveMatrix[i] = waveAecrefR end
    end

    for i=1,INPUT_FILE_CHANNEL do
        if nil == waveMatrix[i] then waveMatrix[i] = Wave.gen_silence(2, 1, fs, 16, 2) end
    end

    local wavePlay = Wave.merge(waveMatrix, 2)
    Wave.save(INPUT_FILE_ARRAY, wavePlay)
    local timeLast = Wave.get_timelast(wavePlay)

    Wave.clearWavdata(waveMatrix)
    Wave.clearWavdata(wavePlay)
    Wave.clearWavdata(waveMic0)
    Wave.clearWavdata(waveMic1)
    Wave.clearWavdata(waveAecref)
    collectgarbage("collect")

    return timeLast
end

--output={"lineout", "TBD", "spkout", "TBD", "AVD", "xSD", "SD", "VoiceBuffer"}
local function get_record_wave(chDef, recordList, platformDelayS)
    local record = Wave.load(FILE_LOUT_NAME)
    if nil == record then return nil end
    
    local tmpSplit = Wave.split(record)
    local result = {}
    print("tmpSplit Chs:", #tmpSplit, "chDef:", #chDef)
    for i=1,#recordList do
        result[i] = nil
        for j=1,#chDef do
            if chDef[j] == recordList[i] then
                print("wave cut", j, chDef[j], tmpSplit[j], platformDelayS)
                Wave.save("debug.wav", tmpSplit[j])
                result[i] = Wave.cut(tmpSplit[j], platformDelayS*1000, -1)
            end
            print("end wave split get results")
        end
    end
    printc("begin wavedata clear")
    Wave.clearWavdata(record)
    Wave.clearWavdata(tmpSplit)
    printc("end wavedata clear")
    collectgarbage("collect")
    printc("end function")

    return result
end

function regression.play_rec(appConfig,vector, sourcePath, chDef, fs, recordList, isOneShot)
    if appConfig.name == "VoiceControl" then 
        recordList ={"mic0"} 
    end 
    printc("start regression.play_rec")
    local timeLast = play_wave_gen(vector, sourcePath, chDef.input, fs)
    collectgarbage("collect")
    if nil == timeLast then return nil end

    local platformDelayS = regression.config.platform.delay
    if isOneShot then iM501.rec_voice_buffer() end
    platformDelayS = platformDelayS or 0
    wait(0.1)
    start_play_record(0)
    
   --control.sleep(timeLast + platformDelayS + 0.3)
    local t =math.round((timeLast + platformDelayS + 0.3)*10)/10
    print("wait time is .."..t)
   -- wait(t)
    control.wait(t)
    print("regression.play_rec", "before play&rec")
    stop_play_record()
    print("regression.play_rec", "after play&rec")
    collectgarbage("collect")
    print("before get record wav ")
    local result = get_record_wave(chDef.output, recordList, platformDelayS)
    print("after get record wav")
    collectgarbage("collect")
    
    print("end regression.play_rec")
    
    return result
end

local function get_delay_check_range(source)
    local speechIdx = Wave.get_speech_idx(source)
    local nSpeech = #speechIdx
    if 0 == nSpeech then return nil end
    if 1 == nSpeech then return speechIdx[1][1], speechIdx[1][2] end

    if speechIdx[nSpeech][2] - speechIdx[nSpeech][1] > 0.5 then return speechIdx[nSpeech][1], speechIdx[nSpeech][2] end
    return speechIdx[nSpeech-1][1], speechIdx[nSpeech][2]
end

function regression.align(record, source)
    local checkStartS, checkEndS = get_delay_check_range(source)
    local sourceCut = Wave.cut(source, checkStartS*1000, checkEndS*1000)
    local recordCut = Wave.cut(record, checkStartS*1000, checkEndS*1000+(Wave.get_timelast(record)-Wave.get_timelast(source))*1000)
    local delayS = Wave.delay(record, source, 2)

    local recordAlign = Wave.cut(record, delayS*1000, -1)
    
    Wave.clearWavdata(sourceCut)
    Wave.clearWavdata(recordCut)
    collectgarbage("collect")
    
    return recordAlign, delayS
end

function regression.case_cal(processed, unprocessed, source, config, delayS, fs,resultPath,fastdowncode)
    notice.message_lib("regression.case_cal", "********\t" .. config.caseName .. " Start\t********", "Info", true, true, false)
    for i=1,#config.items do
        local tmpCfg = config.items[i]
        if tmpCfg.enable and nil ~= tmpCfg.module and nil ~= tmpCfg.submodule then
            notice.message_lib("regression.case_cal", "********\t" .. tmpCfg.name .. " Start\t********", "Info", true, true, false)
            local result = regression[tmpCfg.module][tmpCfg.submodule](processed, unprocessed, source, tmpCfg, delayS, fs,resultPath,fastdowncode)
            regression.report(tmpCfg, result)
            notice.message_lib("regression.case_cal", "********\t" .. tmpCfg.name .. "   End\t********", "Info", true, true, false)
            collectgarbage("collect")
        end
        collectgarbage("collect")
    end
    collectgarbage("collect")
    notice.message_lib("regression.case_cal", "********\t" .. config.caseName .. "   End\t********", "Info", true, true, false)
end

local function load_align_multi(processed, unprocessed, clean, segDefine)
    local checkStartS, checkEndS = get_delay_check_range(clean)
    local fs = Wave.get_samplerate(clean)
    local delayStartSample = checkStartS*fs
    local delayEndSample = checkEndS*fs
    local checkWav = Wave.cut(clean, delayStartSample, delayEndSample, 2)

    local lenProcessedSample = Wave.get_samplenum(processed)
    local lenUnprocessedSample = Wave.get_samplenum(unprocessed)
    local lenCleanSample = Wave.get_samplenum(clean)

    local processedStartSample = 0
    local nSeg = #segDefine
    local delayJump = 5
    local delayS = {processed = {}, unprocessed = {}}
    local processedOut = {}
    local unprocessedOut = {}
    local segJump = 0
    for i=1,nSeg do
        -- cut processed first
        local cutStartSample = processedStartSample + delayStartSample
        local cutEndSample = processedStartSample + math.floor((lenProcessedSample-processedStartSample)/(nSeg-i+1)) - (lenCleanSample - delayEndSample)
        if nSeg == 5 or nSeg == 9 then
            cutEndSample = math.min(cutEndSample, processedStartSample+1.5*lenCleanSample) + 16000   --for polqa /SNRi /3Quest
        else
            cutEndSample = math.min(cutEndSample, processedStartSample+1.5*lenCleanSample)   --for polar pattern /fast/ultrafast
        end 
        print("cutEndSample="..cutEndSample)
        local cutWavTmp = Wave.cut(processed, cutStartSample, cutEndSample, 2)

        delayS.processed[i] = Wave.delay(cutWavTmp, checkWav, 2) + processedStartSample/fs
        processedOut[i] = Wave.cut(processed, delayS.processed[i]*fs, delayS.processed[i]*fs+lenCleanSample, 2)
        
        processedStartSample = delayS.processed[i]*fs + lenCleanSample
        
        -- cut unprocessed
        if 1 == i then
            cutStartSample = delayStartSample
            cutEndSample = math.floor(lenUnprocessedSample/nSeg) - (lenCleanSample - delayEndSample)
            cutEndSample = math.min(cutEndSample, 1.5*lenCleanSample)
            cutWavTmp = Wave.cut(unprocessed, cutStartSample, cutEndSample, 2)
    
            delayS.unprocessed[i] = Wave.delay(cutWavTmp, checkWav, 2)
            unprocessedOut[i] = Wave.cut(unprocessed, delayS.unprocessed[i]*fs, delayS.unprocessed[i]*fs+lenCleanSample, 2)

            segJump = delayS.unprocessed[i] - delayS.processed[i]
        else
            delayS.unprocessed[i] = delayS.processed[i] + segJump
            unprocessedOut[i] = Wave.cut(unprocessed, delayS.unprocessed[i]*fs, delayS.unprocessed[i]*fs+lenCleanSample, 2)
        end
    end
    Wave.clearWavdata(checkWav)
    collectgarbage("collect")

    return processedOut, unprocessedOut, delayS
end

local function load_align(vecCfg, sourcePath, fs, record)
    local source = regression.source_load(vecCfg.clean, sourcePath, fs)
    local processed, unprocessed, delay = {}, nil, 0

    if vecCfg.bypassDelayCheck then
        processed = record
        unprocessed = regression.source_load(vecCfg.unprocessed, sourcePath, fs)
        return processed, unprocessed, source, delay
    end

    if nil == vecCfg.segList then
        local delayProcessed, delayUnprocessed = 0, 0
        processed[1], delayProcessed = regression.align(record[1], source)
        if #record > 1 then -- only for no segList
            for i=2,#record do
                processed[i] = Wave.cut(record[i], delayProcessed*1000, -1)
            end
        end

        if vecCfg.unprocessed then
            local unprocessedTmp = regression.source_load(vecCfg.unprocessed, sourcePath, fs)
            unprocessed, delayUnprocessed = regression.align(unprocessedTmp, source)
            Wave.clearWavdata(unprocessedTmp)
            collectgarbage("collect")
        end
        
        return processed, unprocessed, source, delayProcessed-delayUnprocessed
    end
    
    -- for VEN only
    local unprocessedTmp = regression.source_load(vecCfg.unprocessed, sourcePath, fs)
    if vecCfg.mainCheckProcessed then
        processed, unprocessed, delay = load_align_multi(record[1], unprocessedTmp, source, vecCfg.segList)
    else
        unprocessed, processed, delay = load_align_multi(unprocessedTmp, record[1], source, vecCfg.segList)
    end
    Wave.clearWavdata(unprocessedTmp)
    collectgarbage("collect")
    
    return processed, unprocessed, source, delay
end

function regression.case_test(appConfig, config, fs, sourcePath, resultPath, recordList,fastdowncode,isOneShot)
    notice.message_lib("regression.case_test", "********\t" .. config.caseName .. " Start\t********", "Info", true, true, false)
    if nil == config.vector then
        regression.case_cal(nil, nil, nil, config, nil, fs,resultPath,fastdowncode)
        collectgarbage("collect")
        notice.message_lib("regression.case_test", "********\t" .. config.caseName .. "   End\t********", "Info", true, true, false)

        return
    end
    
    -- play & record
    local record
    if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
        if config.switch then config.switch.runSwitch = regression.switch(config.switch.mode, config.switch.runSwitch, config.switch.isReverse) end
        if config.tune then
            for iTune = 1,#config.tune do
                regression.tune(config.tune[iTune].name, config.tune[iTune].method, config.tune[iTune].value)
            end
        end
        record = regression.play_rec(appConfig,config.vector, sourcePath, appConfig.chDef, fs, recordList, isOneShot)
        if config.tune then
            for iTune = 1,#config.tune do
                regression.tune(config.tune[iTune].name, config.tune[iTune].method, config.tune[iTune].value, true)
            end
        end
        if config.switch then regression.switch(config.switch.mode, config.switch.endSwtich, config.switch.isReverse) end

        local recordTmpSave = Wave.merge(record,2)
        Wave.save(resultPath .. config.caseName..appConfig.name.. "_rec.wav", recordTmpSave)
        Wave.clearWavdata(recordTmpSave)
        collectgarbage("collect")
    else
        local recordTmpSave = Wave.load(resultPath .. config.caseName..appConfig.name.. "_rec.wav")
        record = Wave.split(recordTmpSave)
        Wave.clearWavdata(recordTmpSave)
        collectgarbage("collect")
    end
    
    local processed, unprocessed, source, delay = load_align(config.vector, sourcePath, fs, record)
    
    if GLOBAL.DEBUG_V2 then
        if "table"==type(unprocessed) then
            for i=1,#processed do
                regression.wave_debug_save(resultPath .. config.caseName .. "_" .. i .. "_align.wav", {source, unprocessed[i], processed[i]}, true)
            end
        else
            regression.wave_debug_save(resultPath .. config.caseName .. "_align.wav", {source, unprocessed, processed[1], processed[2], processed[3], processed[4], processed[5]}, true)
        end
    end
    collectgarbage("collect")
    
    regression.case_cal(processed, unprocessed, source, config, delay, fs, resultPath,fastdowncode)
    
    Wave.clearWavdata(record)
    Wave.clearWavdata(processed)
    Wave.clearWavdata(unprocessed)
    Wave.clearWavdata(source)
    collectgarbage("collect")

    notice.message_lib("regression.case_test", "********\t" .. config.caseName .. "   End\t********", "Info", true, true, false)
end

function regression.wave_debug_save(file, wavArray, autoFullscale)
    for i=#wavArray,1,-1 do
        if not wavArray[i] then
            table.remove(wavArray, i)
        end
    end

    if not autoFullscale then
        Wave.save(file, Wave.merge(wavArray,2))
    else
        local wavArrayOut = {}
        for i=#wavArray,1,-1 do
            local peak = Wave.peak(wavArray[i])
            wavArrayOut[i] = Wave.amplify(wavArray[i], -peak)
        end
        Wave.save(file, Wave.merge(wavArrayOut,2))
        Wave.clearWavdata(wavArrayOut)
        collectgarbage("collect")
    end
end