regression = regression or {}

local vos = {}

function vos.oneshot(processed, unprocessed, source, analysis, delayS, fs)
    local lineOutId, xSDId, SDId = nil, nil, nil
    for i = 1,#analysis.recordList do
        if "lineout" == analysis.recordList[i] then lineOutId = i end
        if "xSD" == analysis.recordList[i] then xSDId = i end
        if "SD" == analysis.recordList[i] then SDId = i end
    end
    
    local wakeupIdx = Wave.get_wakeup_idx(processed[lineOutId])
    if nil == wakeupIdx then
        local MOS, xSD, SD = {}, {}, {}
        for i=1,50 do
            table.insert(MOS, -1)
            table.insert(xSD, -1)
            table.insert(xSD, 1)
            table.insert(SD, -1)
            table.insert(SD, 1)
        end
        if nil ~= xSDId and nil ~= SDId then return {MOS, xSD, SD} end
        if nil ~= xSDId then return {MOS, xSD} end
        if nil ~= SDId then return {MOS, SD} end
    end

    local xSDTriggerIdx, SDTriggerIdx = nil, nil
    if nil ~= xSDId then xSDTriggerIdx = Wave.get_sd_idx(processed[xSDId]) end
    if nil ~= SDId then
        local wakeupIdxSpread = {}
        for i=1,#wakeupIdx do
            table.insert(wakeupIdxSpread, wakeupIdx[i][1])
            table.insert(wakeupIdxSpread, wakeupIdx[i][2])
        end
        SDTriggerIdx = Wave.get_sd_idx(processed[SDId], wakeupIdxSpread)
    end
    
    local function get_trigger_valid(triggerIdx, currentIdx, segEnd, destimateStart, destimateEnd)
        local FT, FR = 0, 0
        local validTrigger = false
        while currentIdx <= #triggerIdx and triggerIdx[currentIdx][2] <= segEnd do
            if nil ~= destimateStart and triggerIdx[currentIdx][1] > destimateStart and triggerIdx[currentIdx][2] < destimateEnd then
                validTrigger = true
            else
                FT = FT + 1
            end
            currentIdx = currentIdx + 1
        end
        if not validTrigger then FR = 1 end

        return FT, FR, currentIdx
    end

    local sourceCut = Wave.cut(source, analysis.MOSStart*1000, analysis.MOSEnd*1000)
    local adjustSourceCut = Wave.peak_to_fullscale(sourceCut)
    Wave.clearWavdata(sourceCut)
    collectgarbage("collect")
    local sourceDelayCut = Wave.cut(source, analysis.MOSDelayStart*1000, analysis.MOSDelayEnd*1000)
    local checkStart = 0
    local checkEnd = checkStart + analysis.guideLenS
    local destimateStart, destimateEnd
    local iWakeup, ixSD, iSD = 1, 1, 1
    local xSDFT, xSDFR = {}, {}
    local SDFT, SDFR = {}, {}
    local MOS = {}
    local oldTriggerStart, oldTriggerIdx = nil, nil
    for iCase = 1,(1+analysis.caseNum) do
        local flagTrigger = false
        local currentTriggerStart = nil
        while (not flagTrigger) and iWakeup <= #wakeupIdx and wakeupIdx[iWakeup][1] <= checkEnd do
            if wakeupIdx[iWakeup][1] > checkStart and wakeupIdx[iWakeup][2] < checkEnd + analysis.BufferLenS+1 then -- trigger right
                checkEnd = checkEnd + analysis.BufferLenS
                flagTrigger = true
                currentTriggerStart = wakeupIdx[iWakeup][1]
            else
                iWakeup = iWakeup + 1
            end
        end
        
        if flagTrigger then
            local checkDelayRange = 2 -- s
            local tmpDelayCutStartMs = (wakeupIdx[iWakeup][2]-(analysis.MOSDelayEnd-analysis.MOSDelayStart)-checkDelayRange)*1000
            local tmpDelayCutEndMs = wakeupIdx[iWakeup][2]*1000
            local recordDelayCut = Wave.cut(processed[lineOutId], tmpDelayCutStartMs, tmpDelayCutEndMs)
            local delayS = Wave.delay(recordDelayCut, sourceDelayCut, 2)
            Wave.clearWavdata(recordDelayCut)
            collectgarbage("collect")
        
            local tmpCutStartMs = tmpDelayCutStartMs + delayS*1000 - (analysis.MOSDelayStart - analysis.MOSStart)*1000
            local tmpCutEndMs = tmpCutStartMs + (analysis.MOSEnd - analysis.MOSStart) * 1000
            local recordAlign = Wave.cut(processed[lineOutId], tmpCutStartMs, tmpCutEndMs)
            local adjustProcessed = Wave.peak_to_fullscale(recordAlign)
            Wave.clearWavdata(recordAlign)
            collectgarbage("collect")

            MOS[iCase] = wave.pesq(adjustSourceCut, adjustProcessed, {{0, analysis.MOSEnd-analysis.MOSStart}}, false)

            Wave.clearWavdata(adjustProcessed)
            collectgarbage("collect")
        else
            MOS[iCase] = -1
        end
        if nil ~= xSDId then
            if flagTrigger then
                destimateStart = wakeupIdx[iWakeup][1]-analysis.xSDTriggerDelay-analysis.gitterDelay
                destimateEnd = wakeupIdx[iWakeup][1]-analysis.xSDTriggerDelay+analysis.gitterDelay
            else
                destimateStart, destimateEnd = nil, nil
            end
            xSDFT[iCase], xSDFR[iCase], ixSD = get_trigger_valid(xSDTriggerIdx, ixSD, checkEnd, destimateStart, destimateEnd)
        end
        if nil ~= SDId then
            if flagTrigger then
                destimateStart = wakeupIdx[iWakeup][1]-analysis.SDTriggerDelay-analysis.gitterDelay
                destimateEnd = wakeupIdx[iWakeup][1]-analysis.SDTriggerDelay+analysis.gitterDelay
            else
                destimateStart, destimateEnd = nil, nil
            end
            SDFT[iCase], SDFR[iCase], iSD = get_trigger_valid(SDTriggerIdx, iSD, checkEnd, destimateStart, destimateEnd)
        end

        checkStart = checkEnd
        checkEnd = checkStart + analysis.caseLenS
        if flagTrigger then
            if nil ~= oldTriggerIdx then
                local stdDistance = (iCase-oldTriggerIdx)*analysis.caseLenS + analysis.BufferLenS
                if 1 == oldTriggerIdx then stdDistance = stdDistance + analysis.guideLenS - analysis.caseLenS end
                local currentDistance = currentTriggerStart - oldTriggerStart
                
                checkEnd = checkEnd + currentDistance - stdDistance
            end
            oldTriggerStart = currentTriggerStart
            oldTriggerIdx = iCase
            
            iWakeup = iWakeup + 1
        end
    end
    Wave.clearWavdata(adjustSourceCut)
    Wave.clearWavdata(sourceDelayCut)
    collectgarbage("collect")
    
    local xSD = nil
    if nil ~= xSDId then
        table.remove(xSDFT, 1)
        table.remove(xSDFR, 1)
        xSD = {}
        for i=1,#xSDFT do
            table.insert(xSD, xSDFT[i])
            table.insert(xSD, xSDFR[i])
        end
    end
    local SD = nil
    if nil ~= SDId then
        table.remove(SDFT, 1)
        table.remove(SDFR, 1)
        SD = {}
        for i=1,#SDFT do
            table.insert(SD, SDFT[i])
            table.insert(SD, SDFR[i])
        end
    end
    table.remove(MOS, 1)

    if nil ~= xSDId and nil ~= SDId then return {MOS, xSD, SD} end
    if nil ~= xSDId then return {MOS, xSD} end
    if nil ~= SDId then return {MOS, SD} end
end


regression.vos = regression.vos or vos