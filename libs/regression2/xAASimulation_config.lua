--xAA simulation configure 
local const = {
    reportCopy = true,
    subsheetCreate = true,
    fsMode = {16000},
    micMode = {"twoMic"},
    recordList = {"lineout"},     
    loop = 15,      --- 20, means  read xAA parameters 20 times
    testInterval = 1,     --- every 1s read once 
    convergecydelay = 10,  --- parameters read should  after convergency time 

    Readtable = { 
        SPL_output = {enable = true,  addr = 0x0fff9fd4, name = "SPL_output"}, 
        Noise_type = {enable = true,  addr = 0x0fff9fd0, name = "Noise_type"}, 
        Noise_floor = {enable = true,  addr = 0x0fff9fd8, name = "Noise_floor"},
    },
    NoiseList = {
        {enable = true, sub_dir = "Noise", name = "Noise_only"},  ----pure_noise
        {enable = true, sub_dir = "NE_Noise",name = "NE_Noise"},  ----ne_noise
    },
    LevelList = {
        {name = "30db", enable = true},
        {name = "35db", enable = true},
        {name = "40db", enable = true},
        {name = "45db", enable = true},
        {name = "50db", enable = true},
        {name = "55db", enable = true},
        {name = "60db", enable = true},
        {name = "65db", enable = true},
        {name = "70db", enable = true},
        {name = "75db", enable = true},
        {name = "80db", enable = true},
    }

}

regression.xAASimulation_const = const