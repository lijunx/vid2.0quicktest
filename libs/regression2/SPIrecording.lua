-- now 16k samplerate  SPI recording support xSD SD record 
-- 8 channel : output={"mic0", "mic1", "mic2", "mic3", "Lout", "AECref", "xSD", "SD"}
-- 6 channel : output={"mic0", "mic1", "mic2", "mic3",  "xSD", "SD"}  -- VooiceControl
local const = {
    fsMode = {16000},
    --fsMode = {8000},
    micMode = {"twoMic"},
    genreport = true,
    stresscycles = 5,
    testMode ={
        {name = "real_test" , enable = true , vectorfolder = "NEsource\\" ,},        -- real test and simulation test can not do at the same time 
        {name = "simulation_test" , enable = false , vectorfolder = "NEbypass\\" ,}   -- UIF do not support simulation test now 
    },
    engineList = {
        {name = "xSD",  enable = true},    
        {name = "SD",  enable = true},
    },
    keywordList = {
        {name = "NHXE",  enable = true},
        {name = "ZMKM",  enable = false},
        {name = "Alexa",  enable = false},
        {name = "Google", enable = false},       --- key word is ok_Google
        {name = "XATX", enable = false}         --- means xiao ai tong xue 
    }
}
local function getSPIFrameSize()
    _, frameSize = iM501.read_dram(iM501.message_return.spi_channel, 2)
    FRAMESIZE = frameSize == "nil" and 160 or frameSize
end
local function play_wave_gen(chDef, wavname, sourcePath,fs)
local inputfile = wave.load(sourcePath..wavname..".wav")
local wave_array = wave.n_to_mono(inputfile) 
Wave.clearWavdata(inputfile)
local waveMic0 = wave_array[1]
local waveMic1 = wave_array[2]
INPUT_FILE_CHANNEL = #chDef
INPUT_FILE_ARRAY = "playtmp.wav"
FILE_LOUT_NAME = "rectmp.wav"
local waveMatrix = {}
for i=1,INPUT_FILE_CHANNEL do
    if "mic0" == chDef[i] then waveMatrix[i] = waveMic0 end
    if "mic1" == chDef[i] then waveMatrix[i] = waveMic1 end
end
for i=1,INPUT_FILE_CHANNEL do
    if nil == waveMatrix[i] then waveMatrix[i] = Wave.gen_silence(2, 1, fs, 16, 2) end
end
local wavePlay = wave.mono_to_n(waveMatrix, 2)  --2: append silence to make all the length equal to the longest one
local timeLast = Wave.get_timelast(wavePlay)
Wave.save(INPUT_FILE_ARRAY, wavePlay)
Wave.clearWavdata(waveMatrix)
Wave.clearWavdata(wavePlay)
Wave.clearWavdata(waveMic0)
Wave.clearWavdata(waveMic1)
collectgarbage("collect")

return timeLast
end

function spi_configure(connectionMode,fs)
    SPI_EN = true
    local connectionModeStr
    local samplerate
    getSPIFrameSize()
    if FRAMESIZE == 160 or FRAMESIZE == 80 then    -- value 80 to be checked
        SPI_CHANNEL = 8
        SPI_REC_CH_MASK=0x7F
    else
        SPI_CHANNEL = 6
        SPI_REC_CH_MASK=0x1F
    end
    if fs == 16000 then 
        samplerate = "16K" 
        connectionModeStr = "SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_I2S_512"
    end
    if fs == 8000 then 
        samplerate = "8K"
        connectionModeStr = "SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_I2S_256"  
    end 
    if 2 == connectionMode then connectionModeStr = "SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_PDM_2048" end
    if 3 == connectionMode then connectionModeStr ="SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_PDM_1024" end
    if 4 == connectionMode then connectionModeStr = "SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_PDM_2048" end
    if 5 == connectionMode then connectionModeStr = "SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_PDM_3250" end
    if 6 == connectionMode then connectionModeStr = "SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_PDM_1024" end
    if 7 == connectionMode then connectionModeStr ="SPI_"..samplerate.."_"..SPI_CHANNEL.."ch_PDM_2048" end

    JSON_FILE = CONFIG_PATH.."iM501\\SPI\\"..connectionModeStr..".json"
    GUI.kernel = load_jsonfile1(JSON_FILE)
    --update_system_setting("ON","OFF")
    update_system_setting("ON", "ON", "NO")
    wait(1)
    --collectgarbage("collect")
end
--- begin report analysis  after SPI recording stress done
function spi_report(appConfig, input_path, inputfile, output_path)

    local start_row = 3
    local start_col = 2
    local sheetname 
    -- input vector 
    local sourcefile = input_path.."\\"..inputfile..".wav"
    local wave_org = wave.load(sourcefile)
    local timeLast = Wave.get_timelast(wave_org)
    local channel_num = wave.get_property(wave_org,"channel_num")
    local wave_array= wave.n_to_mono(wave_org)
    Wave.clearWavdata(wave_org)
    local wave_mic0_org 
    if channel_num == 1 then 
        wave_mic0_org= wave.resample(wave_array[1],16000)
        --wave.save("11.wav",wave_mic0_org)
    else
        wave_mic0_org= wave_array[1]
    end
    local excel_output = luacom.excel.create()
    local mybookout = luacom.excel.open_file(excel_output, GLOBAL.RESULT_EXCEL)
    --- outpout file
    for k = 1, const.stresscycles do 
        local fileout = inputfile.."_"..appConfig.name.."_"..k.."th_SPI_rec.wav"
        local wave_out = wave.load(output_path.."\\"..fileout)
        local length = Wave.get_timelast(wave_out)
        local wave_out_array = wave.n_to_mono(wave_out)
        Wave.clearWavdata(wave_out)
        for n = 1,#wave_out_array  do 
            wave_out_array[n] = wave.cut(wave_out_array[n],200,length*1000)
        end
        local t = length-0.5
        local powermic = {}
        local pesqmic = {}
        for m =1,4 do 
            powermic[m]= wave.power(wave_out_array[m])
            if  powermic[m] < -80 then 
                pesqmic[m] = "/"
            else
                pesqmic[m] = wave.pesq(wave_mic0_org,wave_out_array[m],{{0,t}},true)
            end
        end
        if #wave_out_array == 6 then 
            sheetname = #wave_out_array.."_slot"
            local powerxSD = wave.power(wave_out_array[5])
            local powerSD = wave.power(wave_out_array[6])
            local xSD_Width
            local SD_Width
            if  powerxSD < -80 then 
                xSD_Width = "/"
            else
                local pos_s= wave.silence_len(wave_out_array[5], -80)
                local new_data = wave.cut(wave_out_array[5],pos_s,t*1000)
                xSD_Width = wave.unsilence_len(new_data,-30) 
                Wave.clearWavdata(new_data) 

            end
            if  powerSD < -80 then 
                SD_Width = "/"
            else
                local pos_s= wave.silence_len(wave_out_array[6], -80)
                local new_data = wave.cut(wave_out_array[6],pos_s,t*1000)
                SD_Width = wave.unsilence_len(new_data,-30) 
                Wave.clearWavdata(new_data) 
            end
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+8, xSD_Width) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+9, SD_Width)

        end 
        if #wave_out_array == 8  then
            sheetname = #wave_out_array.."_slot" 
           -- wavexSD = wave_out_array[7]
           -- waveSD = wave_out_array[8]
            local powerlout = wave.power(wave_out_array[5])
            local pesqlout = wave.pesq(wave_mic0_org,wave_out_array[5],{{0,t}},true)
            local poweraecRef = wave.power(wave_out_array[6])
            if  poweraecRef < -80 then 
                pesqaeceRdf = "/"
            else
                pesqaeceRdf = "TBD"
            end
            local powerxSD = wave.power(wave_out_array[7])
            local powerSD = wave.power(wave_out_array[8])
            local xSD_Width
            local SD_Width
            if  powerxSD < -80 then 
                xSD_Width = "/"
            else
                local pos_s= wave.silence_len(wave_out_array[7], -80)
                local new_data = wave.cut(wave_out_array[7],pos_s,t*1000)
                xSD_Width = wave.unsilence_len(new_data,-30) 
                Wave.clearWavdata(new_data) 

            end
            if  powerSD < -80 then 
                SD_Width = "/"
            else
                local pos_s= wave.silence_len(wave_out_array[8], -80)
                local new_data = wave.cut(wave_out_array[8],pos_s,t*1000)
                SD_Width = wave.unsilence_len(new_data,-30) 
                Wave.clearWavdata(new_data) 
            end
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+8, powerlout) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+9, pesqlout)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+10, poweraecRef) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+11, pesqaeceRdf)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+12, xSD_Width) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+13, SD_Width)

        end
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col, powermic[1] ) 
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+1, pesqmic[1])
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+2, powermic[2])
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+3, pesqmic[2])
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+4, powermic[3])
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+5, pesqmic[3])
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+6, powermic[4])
        luacom.excel.write_cells_value(mybookout, sheetname, start_row + k, start_col+7, pesqmic[4])
        -- clear wavedata 
        for i = 1, #wave_out_array do 
            Wave.clearWavdata(wave_out_array[i])
        end
        for j = 1, #wave_array do 
            Wave.clearWavdata(wave_array[i])
        end
    end 
    Wave.clearWavdata(wave_mic0_org)
    if sheetname == "8_slot" then 
        mybookout = luacom.excel.delete_sheet(mybookout, "6_slot") 
    else
        mybookout = luacom.excel.delete_sheet(mybookout, "8_slot")
    end     
    luacom.excel.saveas(mybookout,GLOBAL.RESULT_EXCEL)
    luacom.excel.quit(excel_output)
end


function real_test_spi(appConfig, input_path, inputfile, output_path, fs,loop)
    local playdev
    local playdevice= audio.get_dev(1)
    for n = 1,#playdevice do 
        if playdevice[n] == const.Speakers then 
            playdev =playdevice[n]
        else
            playdev = playdevice[1]
        end
    end
    -- set volume need checked
    audio.set_volume(playdev, 50, 50)
    local sourcefile = input_path.."\\"..inputfile..".wav"
    local wave_data = wave.load(sourcefile)
    local timeLast = Wave.get_timelast(wave_data)
    local fileout = inputfile.."_"..appConfig.name.."_"..loop.."th_SPI_rec.wav"
    if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then 
        start_play_record(0)
        UIF.spi_record()
        audio.play(playdev,sourcefile,0,false)
        wait(timeLast)
        audio.stop_play(playdev)
        stop_play_record()
        local wave_out = wave.load("rectmp.wav")
        wave.save(output_path.."\\"..fileout,wave_out)
        Wave.clearWavdata(wave_out)
    end
    Wave.clearWavdata(wave_data)

end 

function simulation_test_spi(appConfig, input_path, inputfile, output_path, fs,loop)
    print(input_path)
    print(inputfile)
    print(output_path)
    --io.read()
    local timeLast = play_wave_gen(appConfig.chDef.input, inputfile, input_path,fs)
    local fileout = inputfile.."_"..loop.."th_SPI_rec.wav"
    if  not GLOBAL.DEBUG_V2_RECORD_OFFLINE then 
        start_play_record(0)
        UIF.spi_record()
        wait(timeLast)
        stop_play_record()
        local wave_out = wave.load("rectmp.wav")
        wave.save(output_path.."\\"..fileout,wave_out)
        Wave.clearWavdata(wave_out)
    end  

end

function test_spi(appConfig,source_path,record_path,fs,micstr,keyname)
    local year = os.date("%Y")
    local date = os.date("%m%d")
    local time = os.date("%H%M")
    -- run real test 
    if const.testMode[1].enable then 
        local output_path = lfs.create_folder(record_path.."\\"..const.testMode[1].name.."\\")
        local input_path = source_path..const.testMode[1].vectorfolder
        if const.genreport then
            GLOBAL.RESULT_EXCEL = output_path.."\\".."SPIrecording_"..appConfig.name .."_"..fs/1000 .. "k_"..year..date.."_"..time..".xlsx"
            regression.report_initial(regression.config.PATH.template.."SPIrecording_report_template.xlsx")
        end
        for m = 1, const.stresscycles do
            inputfile = keyname
            real_test_spi(appConfig, input_path, inputfile, output_path,fs,m)
        end
        if const.genreport then 
            spi_report(appConfig, input_path, inputfile, output_path)
        end
 
    end
    -- run simulation test 
    if const.testMode[2].enable then 
        local output_path = lfs.create_folder(record_path.."\\"..const.testMode[2].name.."\\")
        local input_path = source_path..const.testMode[2].vectorfolder
        if const.genreport then
            GLOBAL.RESULT_EXCEL = output_path.."\\".."SPIrecording_"..appConfig.name .."_"..fs/1000 .. "k_"..year..date.."_"..time..".xlsx"
            regression.report_initial(regression.config.PATH.template .. "SPIrecording_report_template.xlsx")
        end
        for m = 1,  const.stresscycles do 
            if micstr == nil then 
                inputfile = keyname.."_"..appConfig.name
            else 
                inputfile = keyname.."_"..appConfig.name.."_"..micstr
            end
            simulation_test_spi(appConfig, input_path, inputfile, output_path,fs,m)
        end
        if const.genreport then 
            spi_report(appConfig, input_path, inputfile, output_path)
        end
               
    end
end


function regression.test_SPIrecording(appConfig, moduleSwitch)    -- SPI recording  with xSD/SD     -- real test /simulation test   -- different keyword 
    local fastdowncode = moduleSwitch.fastdowncode
    if fastdowncode ~= 1 then 
        print("pls set interface to SPI , then start SPI recording !!!") 
        assert(false,"")
    end
    local micstr = nil
    local pdmstr = "I2S"
    if moduleSwitch.connectionMode == 2 then pdmstr = "PDMCLK2048K" end 
    if moduleSwitch.connectionMode == 3 then pdmstr = "PDMCLK1024K" end 
    if moduleSwitch.connectionMode == 4 then pdmstr = "PDMCLK2000K" end 
    if moduleSwitch.connectionMode == 5 then pdmstr = "PDMCLK3250K" end
    if moduleSwitch.connectionMode == 6 then pdmstr = "PDMCLK1200K" end
    if moduleSwitch.connectionMode == 7 then pdmstr = "PDMCLK2400K" end 
    local fileout_path 
    local source_path = regression.config.PATH.source .."SPI\\"
    for i = 1, #const.micMode do
        for j=1,#const.fsMode do
            if moduleSwitch.connectionMode == 1 then 
                pdmstr = "I2S_"..const.fsMode[j]
                fileout_path = lfs.create_folder(regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\SPI\\")
            else
                fileout_path = lfs.create_folder(regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\SPI\\") 
            end 
            --print(fileout_path)
            --io.read()
            for k = 1,#const.keywordList do 
                if const.keywordList[k].enable then
                    regression.config.PATH.DSP = regression.config.PATH.dspHome .. appConfig.name .. "\\"..pdmstr.."\\"

                    if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
                        intial_platform(regression.config.dut.name, const.fsMode[j], regression.config.platform.module, moduleSwitch.connectionMode,moduleSwitch.fastdowncode)
                        iM501.init(regression.config.PATH.DSP, const.micMode[i], const.fsMode[j],moduleSwitch.fastdowncode)
                        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        iM501.read_dram(0xfff9fc0)
                        wait(0.1)
                        iM501.read_dram(0xfff9fc0)
                        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        local flag ,value
                        if appConfig.name == "EchoLite" then
                            flag , value = iM501.read_dram(0xfff9f68,0x4)
                            regression.config.baseaddr= value
                            local flagmic ,micdistance = iM501.read_dram(regression.config.baseaddr + 0x26, 0x2)
                            local micdistance = string.format("%#x",micdistance) 
                            if micdistance == "0x5a" or "0x5c" then 
                                micstr = "9cm"
                            elseif micdistance == "0x3c" then 
                                micstr = "6cm"
                            end
                        end
                         -- reconfigure platform for SPI recording  
                        spi_configure(moduleSwitch.connectionMode ,const.fsMode[j])
                        local a,b=iM501.read_dram(0xfff9fc0,0x2)
                        wait(0.1)
                        local c,d=iM501.read_dram(0xfff9fc0,0x2)
                        if d==b then 
                            print("DSP dead... can not do SPI recording ,pls check configuration first ")
                            io.read()
                        end    
                         
                    end 
                     -- run test
                    test_spi(appConfig, source_path, fileout_path, const.fsMode[j], micstr,const.keywordList[k].name)
                end
            end  
        end      
        
    end
end
