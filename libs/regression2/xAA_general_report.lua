
local excel_file_info = {
    report_path = NOAH_PATH.."Report\\",
    file_name  = "Regression_General_Report.xlsx",
    sheet_name = "xAA_General",
}
local xAA_copy_range = {
      start_copy_row = 3,
      start_copy_col = 4,
      stop_copy_row = 24,
      stop_copy_col = 6,
}
local Headset_paste_range = {
    start_paste_row = 3,
    start_paste_col = 4,
    stop_paste_row = 24,
    stop_paste_col = 6,
}
local EchoLite_paste_range = {
    start_paste_row = 3,
    start_paste_col = 7,
    stop_paste_row = 24,
    stop_paste_col = 9,  
}
local VoiceControl_paste_range = {
    start_paste_row = 3,
    start_paste_col = 10,
    stop_paste_row = 24,
    stop_paste_col = 12,
}
function cover_Headset_data(appName, org_file)
    local dst_file =  excel_file_info.report_path..excel_file_info.file_name
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_xAA = "xAA_main"
    local start_row = xAA_copy_range.start_copy_row
    local end_row  = xAA_copy_range.stop_copy_row
    local start_col = xAA_copy_range.start_copy_col
    local xAA_data = {}
    for i = 1,(xAA_copy_range.stop_copy_col-xAA_copy_range.start_copy_col + 1) do 
        xAA_data[i] = {}
        xAA_data[i] = luacom.excel.get_onecolumn_cells_value(mybook_org, mysheet_org_xAA, start_row,end_row, start_col)
        start_col = start_col + 1
    end
    luacom.excel.quit(excel_org)
    local start_cover_row = Headset_paste_range.start_paste_row
    local start_cover_col= Headset_paste_range.start_paste_col
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_xAA = excel_file_info.sheet_name
    for j = 1, (Headset_paste_range.stop_paste_col - Headset_paste_range.start_paste_col +1) do
        for k = 1, #xAA_data[j] do 
            luacom.excel.write_cells_value(mybook_dst, mysheet_dst_xAA, start_cover_row, start_cover_col, xAA_data[j][k])
            start_cover_row = start_cover_row + 1
        end
        start_cover_col = start_cover_col + 1
        start_cover_row = Headset_paste_range.start_paste_row 
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_dst)
end

function cover_EchoLite_data(appName, org_file)
    local dst_file =  excel_file_info.report_path..excel_file_info.file_name
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_xAA = "xAA_main"
    local start_row = xAA_copy_range.start_copy_row
    local end_row  = xAA_copy_range.stop_copy_row
    local start_col = xAA_copy_range.start_copy_col
    local xAA_data = {}
    for i = 1,(xAA_copy_range.stop_copy_col-xAA_copy_range.start_copy_col + 1) do 
        xAA_data[i] = {}
        xAA_data[i] = luacom.excel.get_onecolumn_cells_value(mybook_org, mysheet_org_xAA, start_row,end_row, start_col)
        start_col = start_col + 1
    end
    luacom.excel.quit(excel_org)
    local start_cover_row = EchoLite_paste_range.start_paste_row
    local start_cover_col= EchoLite_paste_range.start_paste_col
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_xAA = excel_file_info.sheet_name
    for j = 1, (EchoLite_paste_range.stop_paste_col - EchoLite_paste_range.start_paste_col +1) do
        for k = 1, #xAA_data[j] do 
            luacom.excel.write_cells_value(mybook_dst, mysheet_dst_xAA, start_cover_row, start_cover_col, xAA_data[j][k])
            start_cover_row = start_cover_row + 1
        end
        start_cover_col = start_cover_col + 1
        start_cover_row = EchoLite_paste_range.start_paste_row 
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_dst)

end

function cover_Voicecontrol_data(appName, org_file)
    local dst_file =  excel_file_info.report_path..excel_file_info.file_name
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_xAA = "xAA_main"
    local start_row = xAA_copy_range.start_copy_row
    local end_row  = xAA_copy_range.stop_copy_row
    local start_col = xAA_copy_range.start_copy_col
    local xAA_data = {}
    for i = 1,(xAA_copy_range.stop_copy_col-xAA_copy_range.start_copy_col + 1) do 
        xAA_data[i] = {}
        xAA_data[i] = luacom.excel.get_onecolumn_cells_value(mybook_org, mysheet_org_xAA, start_row,end_row, start_col)
        start_col = start_col + 1
    end
    luacom.excel.quit(excel_org)
    local start_cover_row = VoiceControl_paste_range.start_paste_row
    local start_cover_col= VoiceControl_paste_range.start_paste_col
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_xAA = excel_file_info.sheet_name
    for j = 1, (VoiceControl_paste_range.stop_paste_col - VoiceControl_paste_range.start_paste_col +1) do
        for k = 1, #xAA_data[j] do 
            luacom.excel.write_cells_value(mybook_dst, mysheet_dst_xAA, start_cover_row, start_cover_col, xAA_data[j][k])
            start_cover_row = start_cover_row + 1
        end
        start_cover_col = start_cover_col + 1
        start_cover_row = VoiceControl_paste_range.start_paste_row 
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_dst)


end

function xAA_generate_report(appName, org_file)
    if appName == "Headset" then 
        cover_Headset_data(appName, org_file )
    end
    if appName == "EchoLite" then 
        cover_EchoLite_data(appName, org_file)
    end 
    if appName == "VoiceControl" then 
        cover_Voicecontrol_data(appName, org_file)
    end 
end

