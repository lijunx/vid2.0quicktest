local const = {   
    fsMode = {16000},
    --fsMode = {8000, 16000},
    micMode = {"twoMic"},
    --micMode = {"oneMic", "twoMic"},
    path = "Awareness\\",
    Speakers= "Speakers (Realtek High Definition Audio)" , -- PC speakers NE only test ,one SPK is ok
    loop = 10,     -- loop is for multivoice buffer read 
    NOR = false, 
    PSM = true,    --default is true , both PSM and NM will be checked
    genreport = true, -- true means  generate each case report after,false means not generature report, after whole record then enable true 
    testMode ={
        {name = "real_test" , enable = false , vectorfolder = "NEsource\\" ,},        -- real test and simulation test can not do at the same time 
        {name = "simulation_test" , enable = true , vectorfolder = "NEbypass\\" ,},
        {name = "Multi_voicebuffer_real" , enable = false , vectorfolder = "SPI\\NEsource\\" ,},       -- share same vector with SPI recording 
        {name = "Multi_voicebuffer_simulation" , enable = false , vectorfolder = "SPI\\NEbypass\\" ,}   -- Voicebuffer data show wwave cut need RC update FW
    },
    engineList = {
        {name = "AVD",  enable = false},  -- if AVD is true, PSM wake up must do "real test"
        {name = "xSD",  enable = true},
        {name = "SD",  enable = true},
    },
    keywordList = {
        {name = "NHXE",  enable = true},
        {name = "ZMKM",  enable = false},
        {name = "Alexa",  enable = false},
        {name = "Google", enable = false},       --- key word is ok_Google
        {name = "XATX", enable = false}         --- means xiao ai tong xue 
    },
    neList = { 
        {name = "NE_92dB_14s", enable = true},   -- 14s/7s means interval between adjacent two keywords is 14s /7s 
        {name = "NE_74dB_14s", enable = false},     
        {name = "NE_92dB_7s", enable = false},  
        {name = "NE_74dB_7s", enable = false}
    },
    -- paramters for repot 
    calparam = {
        toneThd = -30,    ----the thd of tone power in bypass wave ch1, for cut tone accurately 
        segments = 10,
        time_type = 'sample',
        key_sd_adjust_tri = 0.35,    ----margin adjust limit for sd timing delay
        key_offset_sd_tri = 1.0,    -----delay between mic0 and sd trigger
        --key_sd_adjust_ft = 0.35,
        --key_offset_sd_ft = 1.0,
        key_xsd_adjust_tri = 0.50,  ----margin adjust limit for xsd timing delay
        key_offset_xsd_tri = 0.25,  -----delay between mic0 and xsd trigger
        --key_xsd_adjust_ft = 0.50,
        --key_offset_xsd_ft = 0.25
        key = {sd_continus_num=100,sd_trigger_thd=100,xsd_continus_num=100,xsd_trigger_thd=100} ---NOR: 24 ~ 31ms; PSM: 39 ~ 46ms 
    } 
}


for i=#const.engineList,1,-1 do if not const.engineList[i].enable then table.remove(const.engineList, i) end end
--for i=#const.keywordList,1,-1 do if not const.keywordList[i].enable then table.remove(const.keywordList, i) end end
for i=#const.neList,1,-1 do if not const.neList[i].enable then table.remove(const.neList, i) end end

regression.AcousticAwareness_const = const