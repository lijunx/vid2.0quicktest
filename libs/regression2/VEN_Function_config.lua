-- for Headset

local const = {
    --fsMode = {8000, 16000},
    fsMode = {16000},
    --micMode = {"oneMic", "twoMic"},
    micMode = {"twoMic"},
    recordList = {"lineout"},
    --recordList = {"mic0"},  --- voicecontrol
    reportCopy = true
}

const.itemEnable = {
    [ [==[Sending Delay]==] ] =true,
    [ [==[Sending Power]==] ] =true,
    [ [==[Sending Noise Floor]==] ] =true,
    [ [==[Sending Voice Quality - PESQ]==] ] =true,

    [ [==[Sending Convergence Time]==] ] =false,
    [ [==[Sending Noise Suppression]==] ] =false,
    [ [==[Sending Noise Suppression(NS OFF)]==] ] =false,

    [ [==[PolarPattern Fast]==] ] = false,
    [ [==[PolarPattern Fast - PESQ(female)]==] ] = false,
    [ [==[PolarPattern Fast - PESQ(male)]==] ] = false,

    [ [==[AEC]==] ] = false,
    [ [==[AEC_OFF]==] ] = false,
    [ [==[AEC_ONOFF]==] ] = false,    -- for VID 2.2 To disable I2S Clock: write 0x0FFF_FF10=0x0007,To enable I2S Clock: write 0x0FFF_FF10= 0x0000 

    
    [ [==[MIPS]==] ] = true,                -- total mips Normal mode 
    [ [==[mips]==] ] = true,                -- vos ven xAA xSD SD each mips alone 
    [ [==[LDO]==] ] =true,
    [ [==[Current]==] ] = false,

    [ [==[HW_Bypass]==] ] =false,
    [ [==[HW_Bypass_pesq]==] ] =false,
    [ [==[HW_Bypass_power]==] ] =false,

    [ [==[Enhance_Powerdown]==] ] = false,
    [ [==[Enhance_Powerdown_pesq]==] ] = false,
    [ [==[Enhance_Powerdown_power]==] ] = false,

    [ [==[Production_Trigger]==] ] = false,             --	Host Request for Production Trigger Test:force SD trigger


    
    [ [==[Watch_dog]==] ] = true,


}

regression.VEN_const = const
require "libs.regression2.VEN_Function_config_Headset"
require "libs.regression2.VEN_Function_config_Automotive"
require "libs.regression2.VEN_Function_config_EchoLite"
require "libs.regression2.VEN_Function_config_VoiceControl"
--require "libs.regression2.VEN_config_Ultrasound"