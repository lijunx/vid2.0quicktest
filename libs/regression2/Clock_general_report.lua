require "libs.regression2.Clock_config_report"
--require "libs.luacom.excel"

local excel_report_file_infor = {
    file_name  = "Regression_General_Report.xlsx",
    sheet_name_1 = "Clock_General",
    sheet_name_2 = "Headset_clock_PP",
    sheet_name_3 = "Automotive_clock_PP",
    sheet_name_4 = "EchoLite_clock_PP",
}

local general_report_sheet_name = {
    "Clock_General", 
    "Headset_clock_PP",
    "Automotive_clock_PP",
    "EchoLite_clock_PP",
}

function copy_clock_data(mysheet, copy_paste_range)
    local copy_range = luacom.excel.get_range(mysheet, copy_paste_range.start_row, copy_paste_range.start_copy_column, copy_paste_range.end_row, copy_paste_range.end_copy_column)
    local paste_range = luacom.excel.get_range(mysheet, copy_paste_range.start_row, copy_paste_range.start_paste_column, copy_paste_range.end_row, copy_paste_range.end_paste_column)
    print(copy_range)
    luacom.excel.range_duplicate(copy_range, paste_range)
end

function cover_clock_basic_data(name, org_file, dst_file, str)
    local basic_item = clock_config.basic_item
    local basic_report_range = clock_config.basic_report_range
    local basic_data = {}
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_basic = luacom.excel.open_sheet(mybook_org, "Basic Item")
    print("xxxx")
    mysheet_org_basic:Activate()
    for i = 1, #basic_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_basic, basic_report_range[basic_item[i]].row, basic_report_range[basic_item[i]].column)
        if temp_data then
            table.insert(basic_data, temp_data)
        else
            table.insert(basic_data, "-")
        end
    end
    local current_item = clock_config.current_item
    local current_report_range = clock_config.PSM_current_report_range
    local current_data = {}
    local mysheet_org_current = luacom.excel.open_sheet(mybook_org, "PSM_current")
    print("yyyy")
    mysheet_org_current:Activate()
    for i = 1, #current_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_current, current_report_range[current_item[i]].row, current_report_range[current_item[i]].column)
        if temp_data then
            table.insert(current_data, temp_data)
        else
            table.insert(current_data, "-")
        end
    end
    local POLQA_item = clock_config.POLQA_item
    local POLQA_report_range = clock_config.POLQA_report_range
    local POLQA_data = {}
    local mysheet_org_polqa = luacom.excel.open_sheet(mybook_org, "POLQA Item")
    print("zzzz")
    mysheet_org_polqa:Activate()
    for i = 1, #POLQA_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_polqa, POLQA_report_range[POLQA_item[i]].row, POLQA_report_range[POLQA_item[i]].column)
        if temp_data then
            table.insert(POLQA_data, temp_data)
        else
            table.insert(POLQA_data, "-")
        end
    end
    
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_basic = luacom.excel.open_sheet(mybook_dst, "Clock_General")
    mysheet_dst_basic:Activate()
    local start_cover_row = clock_config.start_row
    luacom.excel.write_cell(mysheet_dst_basic, start_cover_row - 1, clock_config.column[name], str)
    for i = 1, #basic_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, clock_config.column[name], basic_data[i])
        start_cover_row = start_cover_row + 1
    end
    for i = 1, #current_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, clock_config.column[name], current_data[i])
        start_cover_row = start_cover_row + 1
    end
    for i = 1, #POLQA_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, clock_config.column[name], POLQA_data[i])
        start_cover_row = start_cover_row + 1
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_org)
    luacom.excel.quit(excel_dst)
end

function cover_clock_PP_data(name, org_file, dst_file, str)
    local PP_report_range = clock_config.PP_report_range
    local PP_data = {}
    local start_row = PP_report_range.start_row
    local end_row = PP_report_range.end_row
    local column = PP_report_range.column
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_PP = luacom.excel.open_sheet(mybook_org, "Polar Pattern")
    mysheet_org_PP:Activate()
    for i = 1, (end_row - start_row + 1) do
        local temp_data = luacom.excel.read_cell(mysheet_org_PP, start_row, column)
        if temp_data then
            table.insert(PP_data, temp_data)
        else
            table.insert(PP_data, "")
        end
        start_row = start_row + 1
    end
    local start_cover_row = clock_config.PP_copy_range.start_row + 1
    local cover_column = clock_config.PP_copy_range.end_copy_column
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_PP = luacom.excel.open_sheet(mybook_dst, name.."_clock_PP")
    mysheet_dst_PP:Activate()
    luacom.excel.write_cell(mysheet_dst_PP, start_cover_row-1, cover_column, str)
    print(#PP_data)
    for i = 1, #PP_data do
        luacom.excel.write_cell(mysheet_dst_PP, start_cover_row, cover_column, PP_data[i])
        start_cover_row = start_cover_row + 1
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_org)
    luacom.excel.quit(excel_dst)
end

function PDMCLK_report(appName, org_file, str)
    local excel_output = luacom.excel.create()
    local mybook = luacom.excel.open_file(excel_output, clock_config.path.general_report..excel_report_file_infor.file_name)
    for i = 1, #general_report_sheet_name do
        --print(general_report_sheet_name[i])
        local mysheet = luacom.excel.open_sheet(mybook, general_report_sheet_name[i])
        mysheet:Activate()
        if general_report_sheet_name[i] == "Clock_General" and appName == "Headset" then
            copy_clock_data(mysheet, clock_config.Headset_copy_range)
        end
        if general_report_sheet_name[i] == "Clock_General" and appName == "Automotive" then
            copy_clock_data(mysheet, clock_config.Automotive_copy_range)
        end
        if general_report_sheet_name[i] == "Clock_General" and appName == "EchoLite" then
            copy_clock_data(mysheet, clock_config.EchoLite_copy_range)
        end
        if general_report_sheet_name[i] == "Headset_clock_PP" and appName == "Headset" then  
            copy_clock_data(mysheet, clock_config.PP_copy_range)
        end
        if general_report_sheet_name[i] == "Automotive_clock_PP" and appName == "Automotive" then
            copy_clock_data(mysheet, clock_config.PP_copy_range)
        end
        if general_report_sheet_name[i] == "EchoLite_clock_PP" and appName == "EchoLite" then
            copy_clock_data(mysheet, clock_config.PP_copy_range)
        end
    end
    luacom.excel.saveas(mybook, clock_config.path.general_report..excel_report_file_infor.file_name)
    luacom.excel.quit(excel_output)
    cover_clock_basic_data(appName, org_file, clock_config.path.general_report..excel_report_file_infor.file_name, str)
    cover_clock_PP_data(appName, org_file, clock_config.path.general_report..excel_report_file_infor.file_name, str)
end

function PDMCLK_report2(appName, org_file, str)
    cover_clock_basic_data(appName, org_file, clock_config.path.general_report..excel_report_file_infor.file_name, str)
    cover_clock_PP_data(appName, org_file, clock_config.path.general_report..excel_report_file_infor.file_name, str)
end

