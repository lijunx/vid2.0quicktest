require "libs.regression2.VEN_config_report"
--require "libs.luacom.excel"

local excel_report_file_infor = {
    file_name  = "Regression_General_Report.xlsx",
    sheet_name_1 = "General",
    sheet_name_2 = "Headset_FR",
    sheet_name_3 = "Automotive_FR",
    sheet_name_4 = "EchoLite_FR",
    sheet_name_5 = "Headset_PP",
    sheet_name_6 = "Automotive_PP",
    sheet_name_7 = "EchoLite_PP",
}

local general_report_sheet_name = {
    "General", 
    "Headset_FR",
    "Automotive_FR",
    "EchoLite_FR",
    "Headset_PP",
    "Automotive_PP",
    "EchoLite_PP",
}

function copy_data(mysheet, copy_paste_range)
    local copy_range = luacom.excel.get_range(mysheet, copy_paste_range.start_row, copy_paste_range.start_copy_column, copy_paste_range.end_row, copy_paste_range.end_copy_column)
    local paste_range = luacom.excel.get_range(mysheet, copy_paste_range.start_row, copy_paste_range.start_paste_column, copy_paste_range.end_row, copy_paste_range.end_paste_column)
    print(copy_range)
    luacom.excel.range_duplicate(copy_range, paste_range)
end

function cover_basic_data(name, org_file, dst_file, date)
    local basic_item = report_config.basic_item
    local basic_report_range = report_config.basic_report_range
    local basic_data = {}
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_basic = luacom.excel.open_sheet(mybook_org, "Basic Item")
    print("xxxx")
    mysheet_org_basic:Activate()
    for i = 1, #basic_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_basic, basic_report_range[basic_item[i]].row, basic_report_range[basic_item[i]].column)
        if temp_data then
            table.insert(basic_data, temp_data)
        else
            table.insert(basic_data, "-")
        end
    end
    local current_item = report_config.current_item
    local current_report_range = report_config.PSM_current_report_range
    local current_data = {}
    local mysheet_org_current = luacom.excel.open_sheet(mybook_org, "PSM_current")
    print("yyyy")
    mysheet_org_current:Activate()
    for i = 1, #current_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_current, current_report_range[current_item[i]].row, current_report_range[current_item[i]].column)
        if temp_data then
            table.insert(current_data, temp_data)
        else
            table.insert(current_data, "-")
        end
    end
    local POLQA_item = report_config.POLQA_item
    local POLQA_report_range = report_config.POLQA_report_range
    local POLQA_data = {}
    local mysheet_org_polqa = luacom.excel.open_sheet(mybook_org, "POLQA Item")
    print("zzzz")
    mysheet_org_polqa:Activate()
    for i = 1, #POLQA_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_polqa, POLQA_report_range[POLQA_item[i]].row, POLQA_report_range[POLQA_item[i]].column)
        if temp_data then
            table.insert(POLQA_data, temp_data)
        else
            table.insert(POLQA_data, "-")
        end
    end
    local SNRi_item = report_config.SNRi_item
    local SNRi_report_range = report_config.SNRi_report_range
    local SNRi_data = {}
    local mysheet_org_snri = luacom.excel.open_sheet(mybook_org, "3QUEST+SNRi(Fast)")
    print("aaaa")
    mysheet_org_snri:Activate()
    for i = 1, #SNRi_item do
        local temp_data = luacom.excel.read_cell(mysheet_org_snri, SNRi_report_range[SNRi_item[i]].row, SNRi_report_range[SNRi_item[i]].column)
        if temp_data then
            table.insert(SNRi_data, temp_data)
        else
            table.insert(SNRi_data, "-")
        end
    end
    local param_report_range = report_config.Parameter_report_range
    local param_start_row = param_report_range.start_row
    local param_end_row = param_report_range.end_row
    local param_column = param_report_range.column
    local param_data = {}
    local mysheet_org_param = luacom.excel.open_sheet(mybook_org, "Parameter Running")
    print("bbbb")
    mysheet_org_param:Activate()
    for i = 1, (param_report_range.end_row - param_report_range.start_row + 1) do
        local temp_data = luacom.excel.read_cell(mysheet_org_param, param_start_row, param_column)
        if temp_data then
            table.insert(param_data, temp_data)
        else
            table.insert(param_data, "-")
        end
        param_start_row = param_start_row + 1
    end
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_basic = luacom.excel.open_sheet(mybook_dst, "General")
    mysheet_dst_basic:Activate()
    local start_cover_row = report_config.start_row
    luacom.excel.write_cell(mysheet_dst_basic, start_cover_row - 1, report_config.column[name], date)
    for i = 1, #basic_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, report_config.column[name], basic_data[i])
        start_cover_row = start_cover_row + 1
    end
    for i = 1, #current_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, report_config.column[name], current_data[i])
        start_cover_row = start_cover_row + 1
    end
    for i = 1, #POLQA_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, report_config.column[name], POLQA_data[i])
        start_cover_row = start_cover_row + 1
    end

    for i = 1, #SNRi_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, report_config.column[name], SNRi_data[i])
        start_cover_row = start_cover_row + 1
    end
    for i = 1, #param_data do
        luacom.excel.write_cell(mysheet_dst_basic, start_cover_row, report_config.column[name], param_data[i])
        start_cover_row = start_cover_row + 1
    end

    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_org)
    luacom.excel.quit(excel_dst)
end

function cover_FR_data(name, org_file, dst_file, date)
    local FR_report_range = report_config.FR_report_range
    local FR_data = {}
    local start_row = FR_report_range.start_row
    local end_row = FR_report_range.end_row
    local column = FR_report_range.column
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_FR = luacom.excel.open_sheet(mybook_org, "Frequency Response")
    mysheet_org_FR:Activate()
    for i = 1, (end_row - start_row + 1) do
        local temp_data = luacom.excel.read_cell(mysheet_org_FR, start_row, column)
        if temp_data then
            table.insert(FR_data, temp_data)
        else
            table.insert(FR_data, "")
        end
        start_row = start_row + 1
    end
    local start_cover_row = report_config.FR_copy_range.start_row + 1
    local cover_column = report_config.FR_copy_range.end_copy_column
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_FR = luacom.excel.open_sheet(mybook_dst, name.."_FR")
    mysheet_dst_FR:Activate()
    luacom.excel.write_cell(mysheet_dst_FR, start_cover_row-1, cover_column, date)
    for i = 1, #FR_data do
        luacom.excel.write_cell(mysheet_dst_FR, start_cover_row, cover_column, FR_data[i])
        start_cover_row = start_cover_row + 1
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_org)
    luacom.excel.quit(excel_dst)
end

function cover_PP_data(name, org_file, dst_file, date)
    local PP_report_range = report_config.PP_report_range
    local PP_data = {}
    local start_row = PP_report_range.start_row
    local end_row = PP_report_range.end_row
    local column = PP_report_range.column
    local excel_org = luacom.excel.create()
    local mybook_org = luacom.excel.open_file(excel_org, org_file)
    local mysheet_org_PP = luacom.excel.open_sheet(mybook_org, "Polar Pattern")
    mysheet_org_PP:Activate()
    for i = 1, (end_row - start_row + 1) do
        local temp_data = luacom.excel.read_cell(mysheet_org_PP, start_row, column)
        if temp_data then
            table.insert(PP_data, temp_data)
        else
            table.insert(PP_data, "")
        end
        start_row = start_row + 1
    end
    local start_cover_row = report_config.PP_copy_range.start_row + 1
    local cover_column = report_config.PP_copy_range.end_copy_column
    local excel_dst = luacom.excel.create()
    local mybook_dst = luacom.excel.open_file(excel_dst, dst_file)
    local mysheet_dst_PP = luacom.excel.open_sheet(mybook_dst, name.."_PP")
    mysheet_dst_PP:Activate()
    luacom.excel.write_cell(mysheet_dst_PP, start_cover_row-1, cover_column, date)
    print(#PP_data)
    for i = 1, #PP_data do
        luacom.excel.write_cell(mysheet_dst_PP, start_cover_row, cover_column, PP_data[i])
        start_cover_row = start_cover_row + 1
    end
    luacom.excel.saveas(mybook_dst, dst_file)
    luacom.excel.quit(excel_org)
    luacom.excel.quit(excel_dst)
end

function generate_report(appName, org_file, date)
    local excel_output = luacom.excel.create()
    local mybook = luacom.excel.open_file(excel_output, report_config.path.general_report..excel_report_file_infor.file_name)
    for i = 1, #general_report_sheet_name do
        --print(general_report_sheet_name[i])
        local mysheet = luacom.excel.open_sheet(mybook, general_report_sheet_name[i])
        mysheet:Activate()
        if general_report_sheet_name[i] == "General" and appName == "Headset" then
            copy_data(mysheet, report_config.Headset_copy_range)
        end
        if general_report_sheet_name[i] == "General" and appName == "Automotive" then
            copy_data(mysheet, report_config.Automotive_copy_range)
        end
        if general_report_sheet_name[i] == "General" and appName == "EchoLite" then
            copy_data(mysheet, report_config.EchoLite_copy_range)
        end
        if general_report_sheet_name[i] == "Headset_FR" and appName == "Headset" then
            copy_data(mysheet, report_config.FR_copy_range)
        end
        if general_report_sheet_name[i] == "Automotive_FR" and appName == "Automotive" then 
            copy_data(mysheet, report_config.FR_copy_range)
        end
        if general_report_sheet_name[i] == "EchoLite_FR" and appName == "EchoLite" then
            copy_data(mysheet, report_config.FR_copy_range)
        end
        if general_report_sheet_name[i] == "Headset_PP" and appName == "Headset" then  
            copy_data(mysheet, report_config.PP_copy_range)
        end
        if general_report_sheet_name[i] == "Automotive_PP" and appName == "Automotive" then
            copy_data(mysheet, report_config.PP_copy_range)
        end
        if general_report_sheet_name[i] == "EchoLite_PP" and appName == "EchoLite" then
            copy_data(mysheet, report_config.PP_copy_range)
        end
    end
    luacom.excel.saveas(mybook, report_config.path.general_report..excel_report_file_infor.file_name)
    luacom.excel.quit(excel_output)
    cover_basic_data(appName, org_file, report_config.path.general_report..excel_report_file_infor.file_name, date)
    cover_FR_data(appName, org_file, report_config.path.general_report..excel_report_file_infor.file_name, date)
    cover_PP_data(appName, org_file, report_config.path.general_report..excel_report_file_infor.file_name, date)
end

function generate_report2(appName, org_file, date)
    cover_basic_data(appName, org_file, report_config.path.general_report..excel_report_file_infor.file_name, date)
    cover_FR_data(appName, org_file, report_config.path.general_report..excel_report_file_infor.file_name, date)
    cover_PP_data(appName, org_file, report_config.path.general_report..excel_report_file_infor.file_name, date)
end

--generate_report2("Headset", NOAH_PATH.."report_VEN_Headset_twoMic_16k_20181203_1709.xlsx", os.date("%m%d"))