local config = {}
-- connectionMode: 1--I2S, 2--PDM_2048K, 3--PDM_1024K,4--PDM_2000K, Mclk 24M,5--PDM_3250K,Mclk 26M,6--PDM_1200k Mclk 28.8M,7--PDM_2400kMclk 28.8M
--fastdowncode:    3,fast dram and iram; 2 ,only fast down code;  1,SPI  full download code; 4:I2C full download code 
config.moduleSwitch = {connectionMode = 2, fastdowncode =1}
config.tester = "Lijun Xu"
config.baseaddr= 0
config.meter = "fluke189"
config.platform = {
    name = [==[UIF]==],
    version = [==[V2]==],
    module = [==[UIFV2]==],
    ABengine = "ABEngine\\ABDaemonAsync.exe",
    delay = 0.2,
}

config.dut = {
    name = [==[iM501]==],
    path = [==[DSP\]==],
    module = [==[iM501]==],
    sample_rate = 16000,
    delay = 0,
    gain = {
        SND = 0, -- dB
        RCV = 0,
    },
}

config.application = {
    [1] = {name = "Headset", enable=true, VEN_Function = true,  xAASimulation = false, AcousticAwareness = false , SPIreocrding = false, poniterRead = false},
    [2] = {name = "Automotive", enable=false, VEN_Function = true,  xAASimulation = false, AcousticAwareness = false , SPIreocrding = false,poniterRead = false},
    [3] = {name = "EchoLite", enable=false, VEN_Function = true, xAASimulation = false, AcousticAwareness = false, SPIreocrding = false ,poniterRead = false},
    [4] = {name = "VoiceControl", enable=false, VEN_Function = true,  xAASimulation = false, AcousticAwareness = false , SPIreocrding = false,poniterRead = false},
    --[5] = {name = "Ultrasound", enable=false, VEN = false,  xAASimulation = true, AcousticAwareness = false, keyword={NHXE= true,Alexa=true, Google=true, ZMKM=true, XATX=true}}
}
config.application[1].chDef = {input={"mic1", "mic0", "mic0", "mic1", "aecrefL", "TBD", "TBD", "aecrefR"}, output={"mic0", "lineout", "mic1", "aecref", "AVD", "xSD", "SD", "VoiceBuffer"}}
config.application[2].chDef = {input={"mic1", "mic0", "mic0", "mic1", "aecrefL", "TBD", "TBD", "aecrefR"}, output={"mic0", "lineout", "mic1", "aecref", "AVD", "xSD", "SD", "VoiceBuffer"}}
config.application[3].chDef = {input={"mic1", "mic0", "mic0", "mic1", "aecrefL", "TBD", "TBD", "aecrefR"}, output={"mic0", "lineout", "mic1", "aecref", "AVD", "xSD", "SD", "VoiceBuffer"}}
config.application[4].chDef = {input={"mic1", "mic0", "mic0", "mic1","TBD", "TBD", "TBD", "TBD"}, output={"mic0", "lineout",  "TBD", "TBD", "AVD", "xSD", "SD", "VoiceBuffer"}}
--config.application[5].chDef = {input={"mic1", "mic0", "mic0", "mic1","TBD", "TBD", "TBD", "TBD"}, output={"mic0", "lineout", "spkout", "TBD", "AVD", "xSD", "SD", "VoiceBuffer"}}


config.interface = {
    mic0 = {
        [1] = {name="PDMI0L"},
    },
    mic1 = {
        [1] = {name="PDMI0R"},
        [2] = {name="PDMI1L"},
    },
    aecref = {
        [1] = {name="PDMI1L"},
        [2] = {name="PDMI1R"},
        [3] = {name="I2SRXL"},
        [4] = {name="I2SRXR"},
    },
    lineout = {
        [1] = {name="PDMO0L"},
        [2] = {name="PDMO0R"},
        [3] = {name="PDMO1L"},
        [4] = {name="PDMO1R"},
        [5] = {name="I2STXL"},
        [6] = {name="I2STXR"},
    },
    spkout=nil
}
-----------------------
config.im501param = {
    switch = {
        -- VEN top module
        FM1388_MIC_HPF = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 0},
        Reserved = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 1},
        FM1388_FdEQ_LOW_FLOOR = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 2},
        AEC = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 3},
        BF = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 4},
        NS = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 5},
        FdEQ = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 6},
        FFP = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 7},
        DRC = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 8},
        HPF = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 9},
        BVE = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 10},
        PreEQ = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 11},
        WNS = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 12},
        MBDRC = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 13},
        DeREVB = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 1, bitPos = 14},
        SSA = {method = "dram", type = "bit",  byteOffset = 4, defaultByte = 0x437C, defaultBit = 0, bitPos = 15},

        -- VOS top module
        --xSD = {method = "message", cmd = 0x01, onWithVEN = 0x0003, onWithoutVEN = 0x0001, off = 0x0000, verify = 0x0fffbf74}, --normal
        xSD = {method = "message", cmd = 0x01, onWithVEN = 0x1003, onWithoutVEN = 0x1001, off = 0x1000, verify = 0x0fffbf74}, -- PSM
        --xSD = {method = "dram", type = "value", baseAddr = 0x0FFFBF00, byteOffset = 74, onWithVEN = 0x1003, onWithoutVEN = 0x1001, off = 0x1000},
        --SD = {method = "message", cmd = 0x01, onWithVEN = 0x0103, onWithoutVEN = 0x0101, off = 0x0100, verify = 0x0fffbf78}, --normal
        SD = {method = "message", cmd = 0x01, onWithVEN = 0x1103, onWithoutVEN = 0x1101, off = 0x1100, verify = 0x0fffbf78}, -- PSM
        --SD = {method = "dram", type = "value", baseAddr = 0x0FFFBF00, byteOffset = 78, onWithVEN = 0x1003, onWithoutVEN = 0x1001, off = 0x1000},
    },
    tune = {
        PGA_mic0 = { byteOffset = 50, default = 0x0800, min = 0x0, max = 0x7FFF},
        PGA_mic1 = { byteOffset = 52, default = 0x0800, min = 0x0, max = 0x7FFF},
        PGA_mic2 = { byteOffset = 54, default = 0x0800, min = 0x0, max = 0x7FFF},
        PGA_mic3 = { byteOffset = 56, default = 0x0800, min = 0x0, max = 0x7FFF},
        PGA_t1 = { byteOffset = 58, default = 0x0800, min = 0x0, max = 0x7FFF},
        PGA_t2 = { byteOffset = 60, default = 0x0800, min = 0x0, max = 0x7FFF},
        mic_volume = { byteOffset = 1274, default = 0x6666, min = 0x0, max = 0x7FFF},
        FdEQ_gain_0 = { byteOffset = 988, default = 0x6666, min = 0x0, max = 0xFFFF},
        FdEQ_gain_1 = { byteOffset = 990, default = 0x6666, min = 0x0, max = 0xFFFF},
        FdEQ_gain_2 = { byteOffset = 992, default = 0x6666, min = 0x0, max = 0xFFFF},
        FdEQ_gain_3 = { byteOffset = 994, default = 0x6666, min = 0x0, max = 0xFFFF},
        FdEQ_gain_4 = { byteOffset = 996, default = 0x6666, min = 0x0, max = 0xFFFF},
        FdEQ_gain_5 = { byteOffset = 998, default = 0x6666, min = 0x0, max = 0xFFFF},
        ven_enable = { byteOffset = 4, default = 0x437c, min = 0x0, max = 0xFFFF},
    }
}

config.PATH = {
    workspace = "workspace\\",
    source = "Vector\\",
    result = "workspace\\result\\",
    dspHome = "workspace\\dsp\\",
    --acquaHome = "workspace\\ACQUA\\",
    template = "Template\\",
}
for n,v in pairs(config.PATH) do lfs.create_folder(v) end

regression.config = config