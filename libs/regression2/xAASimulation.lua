-- xAA simulation test
require "libs.regression2.xAASimulation_config"
require "libs.regression2.xAABasic"
require "libs.regression2.xAA_general_report"

local const = regression.xAASimulation_const


local function test(appConfig,source_path,record_path,fastdowncode,fs)
    -- XAA parameters check
     local xAA_results={}
     local excel_output = luacom.excel.create()
     local main_temp_row = 3
     for i=1,#const.NoiseList do
        xAA_results[i]={}
        if const.NoiseList[i].enable then
            local filein_path =source_path..const.NoiseList[i].sub_dir.."\\" 
            local fileout_path = lfs.create_folder(record_path..const.NoiseList[i].sub_dir.."\\")

            for j=1,#const.LevelList do
                xAA_results[i][j] = {}
                if const.LevelList[j].enable then 
                    local sub_sheet_name = const.NoiseList[i].name.."_"..const.LevelList[j].name 
                    local mybook = luacom.excel.open_file(excel_output, GLOBAL.RESULT_EXCEL)
                    if const.subsheetCreate ==  true then 
                        local mysheet = luacom.excel.new_sheet_after(mybook, sub_sheet_name)
                        luacom.excel.write_cells_value(mybook, sub_sheet_name, 1, 1,"Cycles")
                        luacom.excel.write_cells_value(mybook, sub_sheet_name, 1, 2, const.Readtable.SPL_output.name)
                        luacom.excel.write_cells_value(mybook, sub_sheet_name, 1, 3, const.Readtable.Noise_type.name)
                        luacom.excel.write_cells_value(mybook, sub_sheet_name, 1, 4, const.Readtable.Noise_floor.name)
                    end
                    local bypassinput = "bypass_pink_"..const.LevelList[j].name
                    local SPL_output_data = {}
					local Noise_type_data = {}
					local Noise_floor_data = {}
                    xAA_results[i][j]=basic.case_test(appConfig,bypassinput,filein_path, fileout_path,const.Readtable,const.recordList,const.loop,const.testInterval,const.convergecydelay,fs)

                    local temp_row= 2
                    for k = 1, const.loop do
                        if const.subsheetCreate ==  true then  
                            luacom.excel.write_cells_value(mybook, sub_sheet_name, temp_row, 1, k)
                            luacom.excel.write_cells_value(mybook, sub_sheet_name, temp_row, 2, xAA_results[i][j][k].SPL_output)
                            luacom.excel.write_cells_value(mybook, sub_sheet_name, temp_row, 3, xAA_results[i][j][k].Noise_type)
                            luacom.excel.write_cells_value(mybook, sub_sheet_name, temp_row, 4, xAA_results[i][j][k].Noise_floor)
                        end
                        SPL_output_data[k] = xAA_results[i][j][k].SPL_output
                        Noise_type_data[k] = xAA_results[i][j][k].Noise_type
                        Noise_floor_data[k] = xAA_results[i][j][k].Noise_floor
                        temp_row = temp_row + 1   
                    end
                    local max_SPL_output, min_SPL_output = basic.max_min(SPL_output_data)
                    local temp_SPL_output_range = "["..tostring(min_SPL_output)..","..tostring(max_SPL_output).."]"
                    local max_Noise_type, min_Noise_type = basic.max_min(Noise_type_data)
                    local temp_Noise_type_range = "["..tostring(min_Noise_type)..","..tostring(max_Noise_type).."]"
                    local max_Noise_floor, min_Noise_floor = basic.max_min(Noise_floor_data)
                    local temp_Noise_floor_range = "["..tostring(min_Noise_floor)..","..tostring(max_Noise_floor).."]"
                    luacom.excel.write_cells_value(mybook, "xAA_main", main_temp_row, 4, temp_SPL_output_range)
                    luacom.excel.write_cells_value(mybook, "xAA_main", main_temp_row, 5, temp_Noise_type_range)
                    luacom.excel.write_cells_value(mybook, "xAA_main", main_temp_row, 6, temp_Noise_floor_range)
                    luacom.excel.saveas(mybook,GLOBAL.RESULT_EXCEL)
                    luacom.excel.quit(excel_output)
                end
                main_temp_row = main_temp_row + 1
            end
        else
            for j = 1, #const.LevelList do
                main_temp_row = main_temp_row + 1
            end

        end
            --collectgarbage("collect")
    end
        --collectgarbage("collect")
end


function regression.test_xAASimulation(appConfig, moduleSwitch)
    local pdmstr = "I2S"
    if moduleSwitch.connectionMode == 2 then pdmstr = "PDMCLK2048K" end 
    if moduleSwitch.connectionMode == 3 then pdmstr = "PDMCLK1024K" end 
    if moduleSwitch.connectionMode == 4 then pdmstr = "PDMCLK2000K" end 
    if moduleSwitch.connectionMode == 5 then pdmstr = "PDMCLK3250K" end
    if moduleSwitch.connectionMode == 6 then pdmstr = "PDMCLK1200K" end
    if moduleSwitch.connectionMode == 7 then pdmstr = "PDMCLK2400K" end 
    local  fileout_path = lfs.create_folder(regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\".."xAA\\")
    for i = 1, #const.micMode do
        for j=1,#const.fsMode do
            -- initial report from template
            local year = os.date("%Y")
            local date = os.date("%m%d")
            local time = os.date("%H%M")

            GLOBAL.RESULT_EXCEL = fileout_path.."\\".."report_xAA_"..appConfig.name .."_".. const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
            regression.report_initial(regression.config.PATH.template .. "xAASimulation_report_template.xlsx")

            -- code download
            regression.config.PATH.DSP = regression.config.PATH.dspHome .. appConfig.name .. "\\"..pdmstr.."\\"
            -- xAA test must during  real time recording  
            if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
                intial_platform(regression.config.dut.name, const.fsMode[j], regression.config.platform.module, moduleSwitch.connectionMode,moduleSwitch.fastdowncode)
                iM501.init(regression.config.PATH.DSP, const.micMode[i], const.fsMode[j],moduleSwitch.fastdowncode)
                print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                iM501.read_dram(0xfff9fc0)
                wait(0.1)
                iM501.read_dram(0xfff9fc0)
                print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
            end
                   
            regression.report_cover_page(appConfig.name, const.micMode[i], const.fsMode[j], regression.config.PATH.DSP)
                    
            -- run test
            local source_path = regression.config.PATH.source .."xAA\\"
            local record_path = regression.config.PATH.result .. appConfig.name.."\\"..pdmstr.."\\".."xAA\\"
            test(appConfig, source_path,record_path,moduleSwitch.fastdowncode,const.fsMode[j])  
        end      
        --collectgarbage("collect")
    end
    --collectgarbage("collect")
end
