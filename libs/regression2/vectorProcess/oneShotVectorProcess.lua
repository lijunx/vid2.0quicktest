local inputPath = NOAH_PATH .. "Vector\\VOS2\\"
local outputPath = NOAH_PATH .. "Vector\\VOS2Cut\\"
lfs.create_folder(outputPath)

local vectorList = {
    {name = "Alexa", enable = false},
    {name = "Google", enable = false},
    {name = "NHXE", enable = true},
    {name = "ZMKM", enable = false},
}
local snrList = {
    {name = "6dB", enable = true},
    {name = "12dB", enable = true},
}
local neLevelList = {
    {name = "89", enable = true},
    {name = "86", enable = true},
    {name = "83", enable = true},
    {name = "80", enable = true},
    {name = "77", enable = true},
    {name = "74", enable = true},
    {name = "71", enable = true},
}
local noiseList = {
    {name = "NE_only", enable = true},
    {name = "cafe", enable = true},
    {name = "car", enable = true},
    {name = "mensa", enable = true},
    {name = "xroads", enable = true},
    {name = "traffic", enable = true},
    {name = "pub", enable = true},
    {name = "train", enable = true},
    {name = "office", enable = true},
}
for i=#vectorList,1,-1 do if not vectorList[i].enable then table.remove(vectorList, i) end end
for i=#snrList,1,-1 do if not snrList[i].enable then table.remove(snrList, i) end end
for i=#neLevelList,1,-1 do if not neLevelList[i].enable then table.remove(neLevelList, i) end end
for i=#noiseList,1,-1 do if not noiseList[i].enable then table.remove(noiseList, i) end end

local chList = {2,4}
local headCheckLenMs = 25000 -- 25s
local vectorLenMs = (12*60+55)*1000 -- 12:55
local function vector_process(recFile, proFile, chList)
    -- current only simple channel select, other work will do by hand
    local rec = Wave.load(recFile)
    local recSplit = Wave.split(rec)
    Wave.clearWavdata(rec)
    collectgarbage("collect")

    local finalRec = {}
    for i=1,#chList do
        finalRec[i] = recSplit[chList[i]]
    end
    local recCheckCut = Wave.cut(finalRec[1], 0, headCheckLenMs)
    local speechIdx = Wave.get_speech_idx(recCheckCut, 0.005, 0.03, -70, 0, 0)
    Wave.clearWavdata(recCheckCut)
    --print(speechIdx[1][1])

    local recMerge = Wave.merge(finalRec)
    Wave.clearWavdata(recSplit)
    Wave.clearWavdata(finalRec)
    collectgarbage("collect")
    
    local recSave = Wave.cut(recMerge, speechIdx[1][1]*1000, speechIdx[1][1]*1000+vectorLenMs)
    Wave.clearWavdata(recMerge)
    collectgarbage("collect")

    Wave.save(proFile, recSave)
    Wave.clearWavdata(recSave)
    collectgarbage("collect")
end

for iVector=1,#vectorList do
    print(vectorList[iVector].name)
    local currentInputPath = inputPath .. vectorList[iVector].name .. "\\"
    local currentOutputPath = outputPath .. vectorList[iVector].name .. "\\"
    lfs.create_folder(currentOutputPath)

    -- NE only
    for iNELevel = 1,#neLevelList do
        local neRec = "NE_" .. neLevelList[iNELevel].name .. "dB_bypass.wav"
        print(neRec)
        if lfs.check_file_exist(currentInputPath .. neRec) then
            vector_process(currentInputPath .. neRec, currentOutputPath .. neRec, chList)
        end
    end

    for iSNR = 1,#snrList do
        lfs.create_folder(currentOutputPath .. snrList[iSNR].name .. "\\")
        for iNoise = 2,#noiseList do
            local rec = "NE_89dB_" .. noiseList[iNoise].name.. snrList[iSNR].name .. "_bypass.wav"
            print(rec)
            if lfs.check_file_exist(currentInputPath .. snrList[iSNR].name .. "\\" .. rec) then
                vector_process(currentInputPath .. snrList[iSNR].name .. "\\" .. rec, currentOutputPath .. snrList[iSNR].name .. "\\" .. rec, chList)
            end
        end
    end

end