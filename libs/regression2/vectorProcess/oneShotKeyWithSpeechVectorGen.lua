local counter = 0
local function vector_gen(keyfileIn, NEwave, totalLenMs, tmpAppendSilence, gain)
    local waveIn = Wave.load(keyfileIn)
    local waveInSplit = Wave.split(waveIn)
    Wave.clearWavdata(waveIn)
    collectgarbage("collect")

    local speechIdx = Wave.get_speech_idx(waveInSplit[1], 0.05)

    counter = counter + 1
    print(counter, speechIdx[1][1]*1000, speechIdx[#speechIdx][2]*1000, keyfileIn)
    
    local waveCut = Wave.cut(waveInSplit[1], 0, speechIdx[#speechIdx][2]*1000)
    Wave.clearWavdata(waveInSplit)
    collectgarbage("collect")
    
    local waveAmp = Wave.amplify(waveCut, gain)
    Wave.clearWavdata(waveCut)
    collectgarbage("collect")

    local waveAppendNE = Wave.append(waveAmp, NEwave)
    Wave.clearWavdata(waveAmp)
    collectgarbage("collect")

    local waveAppendAll = Wave.append(waveAppendNE, tmpAppendSilence)
    Wave.clearWavdata(waveAppendNE)
    collectgarbage("collect")

    local waveOut = Wave.cut(waveAppendAll, 0, totalLenMs)
    Wave.clearWavdata(waveAppendAll)
    collectgarbage("collect")
    
    return waveOut
end

local oneShot2PowerDownDelayS = 7 -- s
local PreSilenceS = 1 -- s
local totalLenS = 15 -- s 1 + 1.5 + 10 -> 15

local path = NOAH_PATH .. "Vector\\Original\\"

local NEwave = Wave.load(path .. "NE_American_English_for_oneshot.wav")
local NELenS = Wave.get_timelast(NEwave)
local fs = Wave.get_samplerate(NEwave)

local modeGainList = {
    Handset = 5,
    Handsfree = 0
}

local keywordList = {"NHXE", "Alexa", "Google", "ZMKM"}
local markFileList = {"yun_001.wav", "female1_1.wav", "female1_1.wav", "zmkm_001.wav"}
local globalGainList = {3.35, 1.5, 3, 2}

for iKey = 1,#keywordList do
    local keyword = keywordList[iKey]
    local markFile = markFileList[iKey]

    local originalPath = path .. "keywords\\" .. keyword .. "\\"

    for resultLevel = 89, 71, -3 do
        local globalGain = globalGainList[iKey]
        local tmpAppendSilence = Wave.gen_silence((totalLenS-NELenS-PreSilenceS)*1000, 1, fs)
        local waveMark = vector_gen(originalPath .. markFile, NEwave, totalLenS*1000, tmpAppendSilence, globalGain)
        local noiseWaitSilenceS = 5
        local tmpSilence = Wave.gen_silence(noiseWaitSilenceS*1000, 1, fs)
        local tmpWav1 = Wave.append(waveMark, tmpSilence)
        Wave.clearWavdata(waveMark)
        Wave.clearWavdata(tmpSilence)
        collectgarbage("collect")

        local tmpWav2 = nil

        local fileList = lfs.get_filelist(originalPath)
        print(#fileList)
        counter = 0
        globalGain = globalGain - 89 + resultLevel
        for i=1,50 do
            local fileIn = originalPath .. fileList[(i-1)%(#fileList)+1]
            local waveTmp = vector_gen(fileIn, NEwave, totalLenS*1000, tmpAppendSilence, globalGain)

            if 1 == i%2 then
                tmpWav2 = Wave.append(tmpWav1, waveTmp)
                Wave.clearWavdata(tmpWav1)
                collectgarbage("collect")
            else
                tmpWav1 = Wave.append(tmpWav2, waveTmp)
                Wave.clearWavdata(tmpWav2)
                collectgarbage("collect")
            end
            Wave.clearWavdata(waveTmp)
            collectgarbage("collect")
        end
        local trigerTone = Wave.load(path .. "aatone_16k.wav")

        local waveOut = nil
        if 1 == (50)%2 then
            waveOut = Wave.append(trigerTone, tmpWav2)
            Wave.clearWavdata(tmpWav2)
            collectgarbage("collect")
        else
            waveOut = Wave.append(trigerTone, tmpWav1)
            Wave.clearWavdata(tmpWav1)
            collectgarbage("collect")
        end

        Wave.clearWavdata(trigerTone)
        collectgarbage("collect")

        for mode, modeExtraGain in pairs(modeGainList) do
            if 0 == modeExtraGain then
                Wave.save(path.. "generate\\"..mode.."\\oneShotVector_keyWithSpeech_".. keyword .."_".. resultLevel ..".wav", waveOut)
            else
                local waveOutFinalAdjust = Wave.amplify(waveOut, modeExtraGain)
                Wave.save(path.. "generate\\"..mode.."\\oneShotVector_keyWithSpeech_".. keyword .."_".. resultLevel ..".wav", waveOutFinalAdjust)
                Wave.clearWavdata(waveOutFinalAdjust)
                collectgarbage("collect")
            end
        end
        Wave.clearWavdata(waveOut)
        collectgarbage("collect")
    end
end

Wave.clearWavdata(NEwave)
collectgarbage("collect")