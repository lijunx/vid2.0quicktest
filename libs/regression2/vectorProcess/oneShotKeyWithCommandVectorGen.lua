local counter = 0
local function vector_gen(keyfileIn, commandWave, totalLenMs, tmpInterSilence, tmpAppendSilence, gain, commandGain)
    local waveIn = Wave.load(keyfileIn)
    local waveInSplit = Wave.split(waveIn)
    Wave.clearWavdata(waveIn)
    collectgarbage("collect")

    local speechIdx = Wave.get_speech_idx(waveInSplit[1], 0.05)

    counter = counter + 1
    --print(counter, speechIdx[1][1]*1000, speechIdx[#speechIdx][2]*1000, keyfileIn)
    
    local waveCut = Wave.cut(waveInSplit[1], 0, speechIdx[#speechIdx][2]*1000)
    Wave.clearWavdata(waveInSplit)
    collectgarbage("collect")
    
    local waveAmp = Wave.amplify(waveCut, gain)
    Wave.clearWavdata(waveCut)
    collectgarbage("collect")
    
    local keyFs = Wave.get_samplerate(waveAmp)
    local commandFs = Wave.get_samplerate(commandWave)
    
    local waveCommandCut
    if commandFs == keyFs then
        local commandIdx = Wave.get_speech_idx(commandWave, 0.03)
        waveCommandCut = Wave.cut(commandWave, math.max(1, (commandIdx[1][1] - 0.03)*1000), -1)
    else
        local tmpDownWav = Wave.downsample_simple(commandWave, commandFs/keyFs)
        local commandIdx = Wave.get_speech_idx(tmpDownWav, 0.03)
        waveCommandCut = Wave.cut(tmpDownWav, math.max(1, (commandIdx[1][1] - 0.03)*1000), -1)
        Wave.clearWavdata(tmpDownWav)
        collectgarbage("collect")
    end
    
    local waveCommandAmp = Wave.amplify(waveCommandCut, commandGain)
    Wave.clearWavdata(waveCommandCut)
    collectgarbage("collect")
    
    local waveAmpAppendInterSilence = Wave.append(waveAmp, tmpInterSilence)
    Wave.clearWavdata(waveAmp)
    collectgarbage("collect")
    
    local waveAppendNE = Wave.append(waveAmpAppendInterSilence, waveCommandAmp)
    Wave.clearWavdata(waveAmpAppendInterSilence)
    Wave.clearWavdata(waveCommandAmp)
    collectgarbage("collect")
    
    local waveAppendAll = Wave.append(waveAppendNE, tmpAppendSilence)
    Wave.clearWavdata(waveAppendNE)
    collectgarbage("collect")
    
    local waveOut = Wave.cut(waveAppendAll, 0, totalLenMs)
    Wave.clearWavdata(waveAppendAll)
    collectgarbage("collect")
    
    return waveOut
end

local oneShot2PowerDownDelayS = 7 -- s
local PreSilenceS = 1 -- s
local internalSilenceS = 1
local totalLenS = 11 -- s 1 + 1.5 + 10 -> 15

local path = NOAH_PATH .. "Vector\\Original\\"

local commandMark = Wave.load(path .. "command\\play_music\\aiguo_1.wav")
local commandMarkLenS = Wave.get_timelast(commandMark)
local fs = 16000 --Wave.get_samplerate(commandMark)
local tmpAppendSilence = Wave.gen_silence((totalLenS-commandMarkLenS-PreSilenceS-internalSilenceS)*1000, 1, fs)

local noiseWaitSilenceS = 15
local tmpSilence = Wave.gen_silence(noiseWaitSilenceS*1000, 1, fs)
local tmpInterSilence = Wave.gen_silence(internalSilenceS*1000, 1, fs)

local modeGainList = {
    Handset = 5,
    Handsfree = 0
}

local keywordList = {"NHXE", "Alexa", "Google", "ZMKM"}
local markKeyFileList = {"yun_001.wav", "female1_1.wav", "female1_1.wav", "zmkm_001.wav"}
local globalGainList = {3.35, 1.5, 3, 2}

local commandList = {"equalizer", "next_song", "play_music", "previous_song", "silence", "stop_music"}
local commandGainList = {4, 4, 6, 4, 3, 4}

for iKey = 1,#keywordList do
    local keyword = keywordList[iKey]
    local markKeyFile = markKeyFileList[iKey]
    local originalKeyPath = path .. "keywords\\" .. keyword .. "\\"

    for resultLevel = 89, 71, -3 do
        local globalGain = globalGainList[iKey]

        local tmpWav2 = nil

        local keyFileList = lfs.get_filelist(originalKeyPath)
        counter = 0
        globalGain = globalGain - 89 + resultLevel
        for iCommand = 1,#commandList do
            local commandGain = commandGainList[iCommand]
            commandGain = commandGain - 89 + resultLevel
            
            local command = commandList[iCommand]
            local originalCommandPath = path .. "command\\" .. command .. "\\"
            local commandFileList = lfs.get_filelist(originalCommandPath)
            
            local commandMark = Wave.load(originalCommandPath .. "aiguo_1.wav")
            local waveMark = vector_gen(originalKeyPath .. markKeyFile, commandMark, totalLenS*1000, tmpInterSilence, tmpAppendSilence, globalGain, commandGain)
            local tmpWav1 = Wave.append(waveMark, tmpSilence)
            Wave.clearWavdata(waveMark)
            Wave.clearWavdata(commandMark)
            collectgarbage("collect")
            for i=1,50 do
                local keyFileIn = originalKeyPath .. keyFileList[(i-1)%(#keyFileList)+1]
                local commandFileIn = originalCommandPath .. commandFileList[(i-1)%(#commandFileList)+1]
                local commandWave = Wave.load(commandFileIn)
                local waveTmp = vector_gen(keyFileIn, commandWave, totalLenS*1000, tmpInterSilence, tmpAppendSilence, globalGain, commandGain)
                Wave.clearWavdata(commandWave)
                collectgarbage("collect")

                local WaveTmpAppend15s = nil
                if 0 == i%5 then
                    WaveTmpAppend15s = Wave.append(waveTmp, tmpSilence)
                else
                    WaveTmpAppend15s = nil
                end
                
                if 1 == i%2 then
                    tmpWav2 = Wave.append(tmpWav1, WaveTmpAppend15s or waveTmp)
                    Wave.clearWavdata(tmpWav1)
                    collectgarbage("collect")
                else
                    tmpWav1 = Wave.append(tmpWav2, WaveTmpAppend15s or waveTmp)
                    Wave.clearWavdata(tmpWav2)
                    collectgarbage("collect")
                end
                Wave.clearWavdata(WaveTmpAppend15s)
                Wave.clearWavdata(waveTmp)
                collectgarbage("collect")
            end
            local trigerTone = Wave.load(path .. "aatone_16k.wav")
            local waveOut = nil
            if 1 == (50)%2 then
                waveOut = Wave.append(trigerTone, tmpWav2)
                Wave.clearWavdata(tmpWav2)
                collectgarbage("collect")
            else
                waveOut = Wave.append(trigerTone, tmpWav1)
                Wave.clearWavdata(tmpWav1)
                collectgarbage("collect")
            end
          
            Wave.clearWavdata(trigerTone)
            collectgarbage("collect")
    
            for mode, modeExtraGain in pairs(modeGainList) do
                print(path.. "generate\\"..mode.."\\oneShotVector_keyWithCommand_".. keyword .."_".. command .."_".. resultLevel ..".wav")
                if 0 == modeExtraGain then
                    Wave.save(path.. "generate\\"..mode.."\\oneShotVector_keyWithCommand_".. keyword .."_".. command .."_".. resultLevel ..".wav", waveOut)
                else
                    local waveOutFinalAdjust = Wave.amplify(waveOut, modeExtraGain)
                    Wave.save(path.. "generate\\"..mode.."\\oneShotVector_keyWithCommand_".. keyword .."_".. command .."_".. resultLevel ..".wav", waveOutFinalAdjust)
                    Wave.clearWavdata(waveOutFinalAdjust)
                    collectgarbage("collect")
                end
            end
            Wave.clearWavdata(waveOut)
            collectgarbage("collect")
        end
    end
end

Wave.clearWavdata(tmpSilence)
Wave.clearWavdata(tmpInterSilence)
Wave.clearWavdata(commandMark)
Wave.clearWavdata(tmpAppendSilence)
collectgarbage("collect")