basic = basic or {}

local function read_xAA_values(readtable, loop, interval)
    local flag
    local group_values = {}
    for i = 1, loop do
        group_values[i] = {SPL_output = " ",Noise_type = " ",Noise_floor = " "}
        if readtable.SPL_output.enable then
            flag, group_values[i].SPL_output = iM501.read_dram(readtable.SPL_output.addr, 0x2)
            group_values[i].SPL_output = string.format("%d", group_values[i].SPL_output)
        end
        if readtable.Noise_type.enable then
            flag, group_values[i].Noise_type = iM501.read_dram(readtable.Noise_type.addr, 0x2)
            group_values[i].Noise_type = string.format("%d", group_values[i].Noise_type)
        end
        if readtable.Noise_floor.enable then
            flag, group_values[i].Noise_floor = iM501.read_dram(readtable.Noise_floor.addr, 0x2)
            group_values[i].Noise_floor = string.format("%d", group_values[i].Noise_floor)
        end
        wait(interval)
    end
    return group_values
end

local function play_wave_gen(chDef, wavname, sourcePath,fs)
    local inputfile = wave.load(sourcePath..wavname..".wav")
    local wave_array = wave.n_to_mono(inputfile) 
    Wave.clearWavdata(inputfile)
    local waveMic0 = wave_array[1]
    local waveMic1 = wave_array[2]
    INPUT_FILE_CHANNEL = #chDef
    INPUT_FILE_ARRAY = "playtmp.wav"
    FILE_LOUT_NAME = "rectmp.wav"
    local waveMatrix = {}
    for i=1,INPUT_FILE_CHANNEL do
        if "mic0" == chDef[i] then waveMatrix[i] = waveMic0 end
        if "mic1" == chDef[i] then waveMatrix[i] = waveMic1 end
    end
    for i=1,INPUT_FILE_CHANNEL do
        if nil == waveMatrix[i] then waveMatrix[i] = Wave.gen_silence(2, 1, fs, 16, 2) end
    end
    local wavePlay = wave.mono_to_n(waveMatrix, 2)  --2: append silence to make all the length equal to the longest one
    local timeLast = Wave.get_timelast(wavePlay)
    Wave.save(INPUT_FILE_ARRAY, wavePlay)
    Wave.clearWavdata(waveMatrix)
    Wave.clearWavdata(wavePlay)
    Wave.clearWavdata(waveMic0)
    Wave.clearWavdata(waveMic1)
    collectgarbage("collect")
    
    return timeLast
end

--output={"lineout", "TBD", "spkout", "TBD", "AVD", "xSD", "SD", "VoiceBuffer"}
local function get_record_wave(chDef, recordList,platformDelayS)
    local record = Wave.load(FILE_LOUT_NAME)
    if nil == record then return nil end
    
    local tmpSplit = Wave.split(record)
    local result = {}
    print("tmpSplit Chs:", #tmpSplit, "chDef:", #chDef)
    for i=1,#recordList do
        result[i] = nil
        for j=1,#chDef do
            if chDef[j] == recordList[i] then
                print("wave cut", j, chDef[j], tmpSplit[j], platformDelayS)
                Wave.save("debug.wav", tmpSplit[j])
                result[i] = Wave.cut(tmpSplit[j], platformDelayS*1000, -1)
            end
            print("end wave split get results")
        end
    end
    Wave.clearWavdata(record)
    Wave.clearWavdata(tmpSplit)
    collectgarbage("collect")

    return result
end
function basic.case_test(appConfig, wavname, sourcePath, resultPath,readtable,recordList,loop,interval,delay,fs)
    if appConfig.name == "VoiceControl" then 
        recordList = {"mic0"}
    end 
    local read_array={}
    local platformDelayS = regression.config.platform.delay
    local timeLast = play_wave_gen(appConfig.chDef.input, wavname, sourcePath,fs)
    -- play & record
    local record
    if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
        start_play_record(0)  
        wait(delay)  --- after delay begin read xAA values
        read_array=read_xAA_values(readtable, loop, interval)
        stop_play_record()
       -- collectgarbage()
        record=get_record_wave(appConfig.chDef.output,recordList,platformDelayS)
        local recordTmpSave = Wave.merge(record,2)
        Wave.save(resultPath .."\\"..wavname.."_"..appConfig.name.. "rec.wav", recordTmpSave)
        Wave.clearWavdata(recordTmpSave)
        Wave.clearWavdata(record)
        collectgarbage("collect")
    else
        print("pls set 'GLOBAL.DEBUG_V2_RECORD_OFFLINE = false', then do xAA check again ")
        local recordTmpSave = Wave.load(resultPath .."\\"..wavname.."_"..appConfig.name.. "rec.wav")
        record = Wave.split(recordTmpSave)
        Wave.clearWavdata(recordTmpSave)
        Wave.clearWavdata(record)
        collectgarbage("collect")
    end

    return read_array
end

function basic.max_min(data)
    local max_value
    local min_value
    local temp_max_value = 0
    local temp_min_value = 1000
    for i = 1, #data do
        max_value = math.max(temp_max_value, data[i])
        min_value = math.min(temp_min_value, data[i])
        temp_max_value = max_value
        temp_min_value = min_value
    end
    return max_value, min_value
end