report_config = report_config or {}
report_config.path = {
    general_report = NOAH_PATH.."Report\\",
    --temp_report = NOAH_PATH.."temp\\"
}

--report_config.name = "VEN_General_Report.xlsx"
report_config.Headset_copy_range = {
    start_row = 2,
    start_copy_column = 6,
    end_row = 28,
    end_copy_column = 9,
    start_paste_column = 5,
    end_paste_column = 8
}

report_config.Automotive_copy_range = {
    start_row = 2,
    start_copy_column = 12,
    end_row = 28,
    end_copy_column = 15,
    start_paste_column = 11,
    end_paste_column = 14
}

report_config.EchoLite_copy_range = {
    start_row = 2,
    start_copy_column = 18,
    end_row = 28,
    end_copy_column = 21,
    start_paste_column = 17,
    end_paste_column = 20
}

report_config.FR_copy_range = {
    start_row = 2,
    start_copy_column = 3,
    end_row = 83,
    end_copy_column = 6,
    start_paste_column = 2,
    end_paste_column = 5
}

report_config.PP_copy_range = {
    start_row = 2,
    start_copy_column = 3,
    end_row = 38,
    end_copy_column = 6,
    start_paste_column = 2,
    end_paste_column = 5
}

report_config.item = {
    Delay,
    Output_Level,
    Noise_floor,
    Noise_suppression,
    Convergence_time,
    PESQ,
    AEC_on,
    AEC_off,
    LDO,
    MIPS,
    Current_AVD,
    Current_xSD,
    Current_SD,
    Current_Nmode,
    POLQA_Nobgn,
    POLQA_Mensa,
    POLQA_Car,
    POLQA_Train,
    POLQA_Road,
    SNRi_Mensa,
    SNRi_Car,
    SNRi_Train,
    SNRi_Road,
    Parameter_1,
    Parameter_2,
    Parameter_3,
}

report_config.start_row = 3
report_config.end_row = 28
report_config.Headset_column = 9
report_config.Automotive_column = 15
report_config.EchoLite_column = 21

report_config.column = {
    Headset = 9,
    Automotive = 15,
    EchoLite = 21
}

report_config.basic_item = {
    "Delay", "Output_level", "Noise_floor", "Noise_suppression", "Convergence_time", "PESQ", "AEC_on", "AEC_off", "LDO", "MIPS", "Current_Nmode"
}

report_config.current_item = {
    "AVD", "xSD", "SD"
}

report_config.POLQA_item = {
    "Nobgn", "Mensa", "Car", "Train", "Road"
}

report_config.SNRi_item = {
    "Mensa", "Car", "Train", "Road"
}


report_config.basic_report_range = {
    Delay = {row = 7, column = 2},
    Output_level = {row = 7, column = 3},
    Noise_floor = {row = 7, column = 6},
    Noise_suppression = {row = 8, column = 7},
    Convergence_time = {row = 7, column = 9},
    PESQ = {row = 7, column = 10},
    AEC_on = {row = 7, column = 12},
    AEC_off = {row = 7, column = 13},
    LDO = {row = 7, column = 15},
    MIPS = {row = 7, column = 16},
    Current_Nmode = {row = 7, column = 17},
}

report_config.PSM_current_report_range = {
    AVD = {row = 4, column = 1},
    xSD = {row = 4, column = 3},
    SD = {row = 4, column = 5}
}

report_config.Parameter_report_range = {
    start_row = 2, end_row = 4, column = 3
}


report_config.POLQA_report_range = {
    Nobgn = {row = 4, column = 2},
    Mensa = {row = 4, column = 3},
    Car = {row = 4, column = 4},
    Train = {row = 4, column = 5},
    Road = {row = 4, column = 6}
}

report_config.SNRi_report_range = {
    Mensa = {row = 23, column = 3},
    Car = {row = 23, column = 4},
    Train = {row = 23, column = 5},
    Road = {row = 23, column = 6}
}

report_config.FR_report_range = {
    start_row = 3, end_row = 83, column = 2
}

report_config.PP_report_range = {
    start_row = 3, end_row = 38, column = 2
}



