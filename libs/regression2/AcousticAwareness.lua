require "libs.regression2.AcousticAwareness_config"
local const = regression.AcousticAwareness_const

local function reocrdChnnel(moudle)
        local recordList= {}
        local xsdch = nil
        local sdch = nil 
        if moudle == "_AVD_xSD_SD" then 
                recordList = {"mic0","lineout", "AVD", "xSD", "SD", "VoiceBuffer"}
                xsdch = 4
                sdch = 5
        elseif moudle == "_AVD_SD" then 
                recordList = {"mic0","lineout", "AVD", "SD", "VoiceBuffer"}
               -- xsdch = nil
                sdch = 4
        elseif moudle == "_xSD_SD" then 
                recordList = {"mic0","lineout","xSD", "SD", "VoiceBuffer"}
                xsdch = 3
                sdch = 4
        elseif moudle == "_SD" then 
                 recordList = {"mic0","lineout",  "SD", "VoiceBuffer"}
                -- xsdch = nil
                 sdch = 3
        end
        return recordList,xsdch,sdch
end

local function play_wave_gen(chDef, wavname, sourcePath,fs)
    local inputfile = wave.load(sourcePath..wavname..".wav")
    local wave_array = wave.n_to_mono(inputfile) 
    Wave.clearWavdata(inputfile)
    local waveMic0 = wave_array[1]
    local waveMic1 = wave_array[2]
    INPUT_FILE_CHANNEL = #chDef
    INPUT_FILE_ARRAY = "playtmp.wav"
    FILE_LOUT_NAME = "rectmp.wav"
    local waveMatrix = {}
    for i=1,INPUT_FILE_CHANNEL do
        if "mic0" == chDef[i] then waveMatrix[i] = waveMic0 end
        if "mic1" == chDef[i] then waveMatrix[i] = waveMic1 end
    end
    for i=1,INPUT_FILE_CHANNEL do
        if nil == waveMatrix[i] then waveMatrix[i] = Wave.gen_silence(2, 1, fs, 16, 2) end
    end
    local wavePlay = wave.mono_to_n(waveMatrix, 2)  --2: append silence to make all the length equal to the longest one
    local timeLast = Wave.get_timelast(wavePlay)
    Wave.save(INPUT_FILE_ARRAY, wavePlay)
    Wave.clearWavdata(waveMatrix)
    Wave.clearWavdata(wavePlay)
    Wave.clearWavdata(waveMic0)
    Wave.clearWavdata(waveMic1)
    collectgarbage("collect")
    
    return timeLast
end
--output={"lineout", "TBD", "spkout", "TBD", "AVD", "xSD", "SD", "VoiceBuffer"}
local function get_record_wave(chDef, recordList,platformDelayS)
    local record = Wave.load(FILE_LOUT_NAME)
    if nil == record then return nil end
    
    local tmpSplit = Wave.split(record)
    local result = {}
    print("tmpSplit Chs:", #tmpSplit, "chDef:", #chDef)
    for i=1,#recordList do
        result[i] = nil
        for j=1,#chDef do
            if chDef[j] == recordList[i] then
                print("wave cut", j, chDef[j], tmpSplit[j], platformDelayS)
                Wave.save("debug.wav", tmpSplit[j])
                result[i] = Wave.cut(tmpSplit[j], platformDelayS*1000, -1)
            end
            print("end wave split get results")
        end
    end
    Wave.clearWavdata(record)
    Wave.clearWavdata(tmpSplit)
    collectgarbage("collect")

    return result
end
local function time_table_read(keyname,start_row, end_row, column)
    local time_table={}
    local excel = luacom.excel.create()
    local mybook = luacom.excel.open_file(excel, NOAH_PATH..regression.config.PATH.template.."time_table.xlsx")
    time_table = luacom.excel.get_onecolumn_cells_value(mybook, keyname, start_row, end_row, column)
    luacom.excel.quit(excel)
    return time_table
end 

local function get_selected_channel_wavedata(path, filename, selected_channel)
        local wavefile = path..filename
        print(wavefile)
        local wavedata = wave.load(wavefile)
        local channel_num = wave.get_property(wavedata, "channel_num")
        if channel_num < selected_channel then
           print("Yichao_wave_process.get_selected_channel_wavedata", "The wave data do not has "..selected_channel.." channel.", "Error")
           return nil
        end
        local wavedata_array = wave.n_to_mono(wavedata)
        Wave.clearWavdata(wavedata)
        local select_wavedata = wavedata_array[selected_channel]
        for i = 1, #wavedata_array do
            if i ~= selected_channel then
                Wave.clearWavdata(wavedata_array[i])
            end
        end
    
        return select_wavedata
        
end
local function transfer_time_format(org_time)
    local org_time = tostring(org_time)
    local processed_time
    local minute_value = 0
    local hour_value = 0
    local second_value
    local temp_value
    local start_pos, end_pos
    start_pos, end_pos = string.find(org_time, ":") 
    if start_pos then 
        --print("start_pos "..start_pos)
        temp_value = string.sub(org_time, 1, end_pos-1)
        local temp_time = string.sub(org_time, end_pos+1, -1)
        --print("temp_time "..temp_time)
        local start_pos_2, end_pos_2
        start_pos_2, end_pos_2 = string.find(temp_time, ":")
        --print(start_pos_2 or "No hour value!!")
        if start_pos_2 then
            --print("hour_value is "..temp_value)
            hour_value = tonumber(temp_value)*60*60
            minute_value = string.sub(temp_time, 1, end_pos_2-1)
            --print("minute_value is "..minute_value)
            minute_value = tonumber(minute_value)*60
            second_value = string.sub(temp_time, end_pos_2+1, -1)
        else
            minute_value = tonumber(temp_value)*60
            second_value = tonumber(temp_time)
        end
    else
        second_value = tonumber(org_time)
    end

    processed_time = hour_value + minute_value + second_value
    --print("total time is "..processed_time)
    return processed_time
end
local function time_convert(time_type, time_value, sample_rate)     --default is 's'
    if time_type == 'ms' then
        time_value = time_value*1000
    elseif time_type =="sample" then
        time_value = time_value*sample_rate
    elseif time_type == 's' then
        time_value = time_value
    end
    --print("convert to type "..time_type..", the value is "..time_value)
    return time_value
end
local function amplitude_convert(bit_rate, nor_value_array)

        local pos_limit, neg_limit
        local sample_value_array = {}
        local i
        local bit_amplitude = 2^bit_rate
        pos_limit = bit_amplitude/2 - 1 -------16 bit means 32767
        neg_limit = -(bit_amplitude/2)   ----------16bit means -32768
        --print("positive limit is "..pos_limit..", negative limit is "..neg_limit)
        for i = 1, #nor_value_array do
            if nor_value_array[i] >= 0 then
                sample_value_array[i] = nor_value_array[i]*pos_limit
            elseif nor_value_array[i] < 0 then
                sample_value_array[i] = math.abs(nor_value_array[i])*neg_limit
            end
        end
    
        return sample_value_array
end

---- cal xSD SD trigger time duration 
function statistic_time_duration_for_awareness(start_time_array, end_time_array, path, filename,  offset_tri, tri_time_adjust)   ---time_type can be 's','ms', 'sample'
    local time_idx
    local time_array = {}
    time_array.ft_s_time_array = {}
    time_array.ft_e_time_array = {}
    time_array.tri_s_time_array = {}
    time_array.tri_e_time_array = {}
    local wave_property = Wave.get_wave_property (path, filename)   ----an array contains wave properties
    min_time = 1/wave_property.sample_rate   ---1 sample time
    time_array.ft_s_time_array[1] = 1/wave_property.sample_rate  ----the start of first sample of the wave, unit is s
    time_idx = math.min(#start_time_array, #end_time_array)   --in case the length of two arrays are not same
    for i = 1, time_idx - 1 do 
        time_array.ft_e_time_array[i] = transfer_time_format(start_time_array[i]) - min_time + offset_tri - tri_time_adjust
        time_array.tri_s_time_array[i] = transfer_time_format(start_time_array[i]) + offset_tri - tri_time_adjust
        time_array.tri_e_time_array[i] = transfer_time_format(end_time_array[i]) + offset_tri + tri_time_adjust
        time_array.ft_s_time_array[i+1] = transfer_time_format(end_time_array[i]) + min_time + offset_tri + tri_time_adjust
    end 

    time_array.ft_e_time_array[time_idx] = transfer_time_format(start_time_array[time_idx]) - min_time + offset_tri - tri_time_adjust
    time_array.tri_s_time_array[time_idx] = transfer_time_format(start_time_array[time_idx]) + offset_tri - tri_time_adjust
    --print("awareness start time "..time_idx.."th is "..time_array.tri_s_time_array[time_idx])
    time_array.tri_e_time_array[time_idx] = transfer_time_format(end_time_array[time_idx]) + offset_tri + tri_time_adjust
    --print("awareness end time "..time_idx.."th is "..time_array.tri_e_time_array[time_idx])
    --------------false duration should be one more than avaiable duration, so total is time index + 1--------------------
    time_array.ft_s_time_array[time_idx+1] = transfer_time_format(end_time_array[time_idx]) + min_time + offset_tri + tri_time_adjust
    time_array.ft_e_time_array[time_idx+1] = wave_property.wave_length/1000  ---change 'ms' to 's'
    return time_array
end 
---convert  xSD SD trigger time duration  to sample duration
function batch_time_convert(start_time_array, end_time_array, path, filename, time_type, offset_tri, tri_time_adjust)
    local org_time_array = {}
    local dst_time_array = {}
    dst_time_array.ft_s_time_array = {}
    dst_time_array.tri_s_time_array = {}
    dst_time_array.tri_e_time_array = {}
    dst_time_array.ft_e_time_array = {}
    org_time_array = statistic_time_duration_for_awareness(start_time_array, end_time_array, path, filename, offset_tri, tri_time_adjust)
    local time_idx = #org_time_array.tri_s_time_array
    local wave_property = Wave.get_wave_property (path, filename)   ----an array contains wave properties
    --print("wave_property.sample_rate is "..wave_property.sample_rate )
    --print("wave_property.wave_length is "..wave_property.wave_length )
    for i = 1, time_idx do
        dst_time_array.ft_s_time_array[i] = time_convert(time_type, org_time_array.ft_s_time_array[i], wave_property.sample_rate)
        dst_time_array.tri_s_time_array[i] = time_convert(time_type, org_time_array.tri_s_time_array[i], wave_property.sample_rate)
        dst_time_array.tri_e_time_array[i] = time_convert(time_type, org_time_array.tri_e_time_array[i], wave_property.sample_rate)
        dst_time_array.ft_e_time_array[i] = time_convert(time_type, org_time_array.ft_e_time_array[i], wave_property.sample_rate)
        print("-------------------------------------------")
        print("ft_s_time_array "..i.."th is "..dst_time_array.ft_s_time_array[i])
        print("ft_e_time_array "..i.."th is "..dst_time_array.ft_e_time_array[i])
        print("                                            ")
        print("tri_s_time_array "..i.."th is "..dst_time_array.tri_s_time_array[i])
        print("tri_e_time_array "..i.."th is "..dst_time_array.tri_e_time_array[i])
        print("-------------------------------------------")
    end
    dst_time_array.ft_s_time_array[time_idx+1] = time_convert(time_type, org_time_array.ft_s_time_array[time_idx+1], wave_property.sample_rate)
    dst_time_array.ft_e_time_array[time_idx+1] = time_convert(time_type, org_time_array.ft_e_time_array[time_idx+1], wave_property.sample_rate)
    print("tri_s_time_array "..tonumber(time_idx+1).."th is "..dst_time_array.ft_s_time_array[time_idx+1])  
    print("tri_e_time_array "..tonumber(time_idx+1).."th is "..dst_time_array.ft_e_time_array[time_idx+1])
    return dst_time_array
end

---- signle FA /trigger count
function single_duration_trigger_count(wave_data, continus_num, s_time, e_time, thd, bit_rate)

    print("start sample pos is "..s_time/16000)
    print("end sample pos is "..e_time/16000)
    local sample_value_array = wave.get_sample(wave_data, s_time, e_time)   ----here are normalized value between [-1, 1]
    --Wave.clearWavdata(wave_data)
    print("step1")


    local trigger_count = {single=0, continus=0, success=0}
    sample_value_array = amplitude_convert(bit_rate, sample_value_array)   ---convert normalized value to sample value
    print("step2")
    for i = 1, #sample_value_array do 
        if sample_value_array[i] > thd then 
            trigger_count.single = trigger_count.single + 1          -----calculate the continus meet requests' sample number
        elseif sample_value_array[i] < thd then
            trigger_count.single = trigger_count.single/continus_num  ----continus_num is the continus high level samples which the real trigger should have
            if trigger_count.single >=1 then 
                trigger_count.continus = trigger_count.single*continus_num
                print("trigger sample number is: "..trigger_count.continus)
                trigger_count.success = trigger_count.success + 1
                trigger_count.single = 0
            elseif trigger_count.single < 1 and trigger_count.single > 0 then
                trigger_count.continus = trigger_count.single*continus_num  ---will be overwritten, only for trigger rate, cannot use to statistic FA
                trigger_count.single = 0
                print("trigger sample number is: "..trigger_count.continus)
            else 
                trigger_count.single = 0
            end
        end
    end
    print("step3")
    collectgarbage("collect")
    print("success trigger is "..trigger_count.success)
    return trigger_count
end
-- all segments FA/trigger count
function batch_duration_trigger_count(path, filename, selected_channel, continus_num, s_time_array, e_time_array, thd)
    local trigger_success_array = {}
    local wave_data = get_selected_channel_wavedata(path, filename, selected_channel)
    wave.save("tridebug.wav",wave_data)
    local wave_property = Wave.get_wave_property (path, filename)
     -- print(wave_property.bit_rate)
    for i = 1, #s_time_array do
        trigger_success_array[i] = single_duration_trigger_count(wave_data, continus_num, s_time_array[i], e_time_array[i], thd, wave_property.bit_rate)
    end
    Wave.clearWavdata(wave_data)
    return trigger_success_array
end
-- trigger report in Excel 
function trigger_Count(mode,recordpath,fileout,xsdch,sdch,keyname,timeLast)
    local configxsd = {}
    local configsd = {}
    local xsd_sd_start_time = {}
    local xsd_end_time = {}
    local sd_end_time = {}
    --time table info in excel  
    local start_row = 3    ---14s
    if timeLast < 140  then start_row = 15 end  ---7s   
    local end_row = start_row +const.calparam.segments -1
    local timestartcol = 2 
    local xsdtimetcol = 3 
    local sdtimecol = 4 
    --------------------------------------------
    --report info in Excel 
    local xsdlinestart = 4
    local sdlinestart = 4
    if mode == "PSM" then 
        xsdlinestart = 18 
        sdlinestart = 18 
    end
    local xsdFAcol = 2
    local sdFAcol = 5
    --------------------------------------------
    local key_xsd_trigger_array ={}
    local key_xsd_FA_array = {}
    local key_sd_trigger_array ={}
    local key_sd_FA_array = {}
    --------------------------------------------
    xsd_sd_start_time = time_table_read(keyname,start_row, end_row, timestartcol)
    xsd_end_time = time_table_read(keyname,start_row, end_row, xsdtimetcol)
    sd_end_time = time_table_read(keyname,start_row, end_row, sdtimecol)

    local excel_output = luacom.excel.create()
    local mybookout = luacom.excel.open_file(excel_output, GLOBAL.RESULT_EXCEL)
    -- xsd trigger count
    if xsdch ~= nil then 
        local key_xsd_time_array=batch_time_convert(xsd_sd_start_time, xsd_end_time, recordpath, fileout, const.calparam.time_type, const.calparam.key_offset_xsd_tri, const.calparam.key_xsd_adjust_tri)
        key_xsd_trigger_array = batch_duration_trigger_count(recordpath, fileout, xsdch, const.calparam.key.xsd_continus_num, key_xsd_time_array.tri_s_time_array, key_xsd_time_array.tri_e_time_array, const.calparam.key.xsd_trigger_thd)
        key_xsd_FA_array = batch_duration_trigger_count(recordpath, fileout, xsdch, const.calparam.key.xsd_continus_num, key_xsd_time_array.ft_s_time_array, key_xsd_time_array.ft_e_time_array, const.calparam.key.xsd_trigger_thd)
        for j = 1, #key_xsd_trigger_array do
            luacom.excel.write_cells_value(mybookout, keyname, xsdlinestart, xsdFAcol, key_xsd_FA_array[j].success)
            luacom.excel.write_cells_value(mybookout, keyname, xsdlinestart, xsdFAcol+1, key_xsd_trigger_array[j].success)
            luacom.excel.write_cells_value(mybookout, keyname, xsdlinestart, xsdFAcol+2, key_xsd_trigger_array[j].continus)
            xsdlinestart = xsdlinestart + 1
        end
        luacom.excel.write_cells_value(mybookout, keyname, xsdlinestart, xsdFAcol, key_xsd_FA_array[#key_xsd_trigger_array+1].success)
    end 
    -- sd trigger count
    if sdch ~= nil then 
        local key_sd_time_array =batch_time_convert(xsd_sd_start_time, sd_end_time, recordpath, fileout, const.calparam.time_type, const.calparam.key_offset_sd_tri, const.calparam.key_sd_adjust_tri)
        key_sd_trigger_array = batch_duration_trigger_count(recordpath, fileout, sdch, const.calparam.key.sd_continus_num, key_sd_time_array.tri_s_time_array, key_sd_time_array.tri_e_time_array, const.calparam.key.sd_trigger_thd)
        key_sd_FA_array = batch_duration_trigger_count(recordpath, fileout, sdch, const.calparam.key.sd_continus_num, key_sd_time_array.ft_s_time_array, key_sd_time_array.ft_e_time_array, const.calparam.key.sd_trigger_thd)     

        for j = 1, #key_sd_trigger_array do
            print(#key_sd_trigger_array)
            print(key_sd_FA_array[j].success, key_sd_trigger_array[j].success)
            luacom.excel.write_cells_value(mybookout, keyname, sdlinestart, sdFAcol, key_sd_FA_array[j].success)
            luacom.excel.write_cells_value(mybookout, keyname, sdlinestart, sdFAcol+1, key_sd_trigger_array[j].success)
            luacom.excel.write_cells_value(mybookout, keyname, sdlinestart, sdFAcol+2, key_sd_trigger_array[j].continus)
            sdlinestart = sdlinestart + 1
        end
        luacom.excel.write_cells_value(mybookout, keyname, sdlinestart, sdFAcol, key_sd_FA_array[#key_sd_trigger_array+1].success)
    end 
    -- remove  useless sheet in excel report
    luacom.excel.saveas(mybookout,GLOBAL.RESULT_EXCEL)
    for n=1,#const.keywordList do
        if not const.keywordList[n].enable then 
            mybookout = luacom.excel.delete_sheet(mybookout, const.keywordList[n].name)
        end   
    end
    for ch = 1,4 do
        sheetname= "Multi_Voicebuffer_"..ch
        mybookout = luacom.excel.delete_sheet(mybookout, sheetname)
    end
    luacom.excel.saveas(mybookout,GLOBAL.RESULT_EXCEL)
    luacom.excel.quit(excel_output)
end

function real_test(appConfig, moduleSwitch, inputpath, inputfile, outputpath, recordList, module, fs, nor, psm, oneshot,keyname,xsdch,sdch)
    local playdev
    local playdevice= audio.get_dev(1)
    for n = 1,#playdevice do 
        if playdevice[n] == const.Speakers then 
            playdev =playdevice[n]
        else
            playdev = playdevice[1]
        end
    end
  -- set volume need checked
    audio.set_volume(playdev, 100, 100)
    local platformDelayS = regression.config.platform.delay
    local sourcefile = inputpath..inputfile..".wav"
    local wave_data= wave.load(sourcefile)
    local timeLast = Wave.get_timelast(wave_data)
    local length = timeLast*1000
    local fileout = inputfile.."_rec.wav"
    Wave.clearWavdata(wave_data)
    local t =math.round((timeLast + platformDelayS + 0.3)*10)/10
    local interface = "SPI"
    if moduleSwitch.fastdowncode ~= 1 then interface = "I2C" end

    local record
    if nor then 
        local mode = "NOR"
        local recordout= lfs.create_folder(outputpath .."\\"..mode.."\\")
        local recordpath = recordout.."\\"
        if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
            
            start_play_record()
            --wait(0.1)
            audio.play(playdev,sourcefile,0,false)
            print("wait time is .."..t)
			wait(t)
            audio.stop_play(playdev)
            --wait(0.1)
            stop_play_record()
            --collectgarbage("collect")
            record=get_record_wave(appConfig.chDef.output,recordList,platformDelayS)
            local recordTmpSave = Wave.merge(record,2)
            Wave.clearWavdata(record)
            Wave.save(recordpath..fileout, recordTmpSave)
            Wave.clearWavdata(recordTmpSave)
            record = Wave.cut_sinetone_sync(recordpath, fileout,length,const.calparam.toneThd)
            Wave.save(recordpath..fileout, record)
            Wave.clearWavdata(record)
            collectgarbage("collect")
        
        end
        if const.genreport then         -- generate report 
            trigger_Count (mode,recordpath,fileout,xsdch,sdch,keyname,timeLast)
        end
    end
    if psm then 
        local mode = "PSM"
        local recordout= lfs.create_folder(outputpath .."\\"..mode.."\\")
        local recordpath = recordout.."\\"
        if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then

            iM501.rec_novoice_buffer() 

            --play & record 
            start_play_record()
            audio.play(playdev,sourcefile,0,false)
            wait(3)   -- tone at 1s position 
            if interface == "SPI" then iM501.enter_PSM(2,12000) end
            if interface == "I2C" then iM501.enter_PSM(1,100) end
            --wait(0.1)

            print("wait time is .."..t)
			wait(t)
            audio.stop_play(playdev)
            --wait(0.1)
            stop_play_record()
            --collectgarbage("collect")
            record=get_record_wave(appConfig.chDef.output,recordList,platformDelayS)
            local recordTmpSave = Wave.merge(record,2)
            Wave.clearWavdata(record)
            Wave.save(recordpath..fileout, recordTmpSave)
            Wave.clearWavdata(recordTmpSave)
            record = Wave.cut_sinetone_sync(recordpath, fileout,length,const.calparam.toneThd)
            Wave.save(recordpath..fileout, record)
            Wave.clearWavdata(record)
            collectgarbage("collect")

            wait(0.1)
            if interface == "SPI" then iM501.enter_NORMAL(2,12000) end
            if interface == "I2C" then iM501.enter_NORMAL(1,100) end
            --collectgarbage("collect")

        end
        if const.genreport then         -- generate report 
            trigger_Count (mode,recordpath,fileout,xsdch,sdch,keyname,timeLast)
        end

    end
       

end
function simulation_test(appConfig, moduleSwitch, inputpath, inputfile, outputpath, recordList, module, fs, nor, psm, oneshot,keyname,xsdch,sdch)
    local platformDelayS = regression.config.platform.delay
    local timeLast = play_wave_gen(appConfig.chDef.input, inputfile, inputpath,fs)
    local length = timeLast*1000
    local fileout = inputfile.."_rec.wav"
    local t =math.round((timeLast + platformDelayS + 0.3)*10)/10
    local interface = "SPI"
    if moduleSwitch.fastdowncode ~= 1 then interface = "I2C" end
    
    local record
    if nor then 
        local mode = "NOR"
        local recordout = lfs.create_folder(outputpath .."\\"..mode.."\\")
        local recordpath = recordout.."\\"
        if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
            
            start_play_record()
            print("wait time is .."..t)
            wait(t)
            stop_play_record()
            --collectgarbage("collect")
            record=get_record_wave(appConfig.chDef.output,recordList,platformDelayS)
            local recordTmpSave = Wave.merge(record,2)
            Wave.clearWavdata(record)
            Wave.save(recordpath..fileout, recordTmpSave)
            Wave.clearWavdata(recordTmpSave)
            record = Wave.cut_sinetone_sync(recordpath, fileout,length,const.calparam.toneThd)
            Wave.save(recordpath..fileout, record)
            Wave.clearWavdata(record)
            collectgarbage("collect")

        end
        if const.genreport then         -- generate report 
            trigger_Count (mode,recordpath,fileout,xsdch,sdch,keyname,timeLast)
        end
    end
    if psm then 
        local mode = "PSM"
        local recordout= lfs.create_folder(outputpath .."\\"..mode.."\\")
        local recordpath = recordout.."\\"
        if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
            iM501.rec_novoice_buffer()

            -- play & record
            start_play_record()
            wait(3)   -- tone at 1s position 
            if interface == "SPI" then iM501.enter_PSM(2,12000) end
            if interface == "I2C" then iM501.enter_PSM(1,100) end
            print("wait time is .."..t)
            wait(t)
            stop_play_record()
            --collectgarbage("collect")
            record=get_record_wave(appConfig.chDef.output,recordList,platformDelayS)
            local recordTmpSave = Wave.merge(record,2)
            Wave.clearWavdata(record)
            Wave.save(recordpath..fileout, recordTmpSave)
            Wave.clearWavdata(recordTmpSave)
            record = Wave.cut_sinetone_sync(recordpath, fileout,length,const.calparam.toneThd)
            Wave.save(recordpath..fileout, record)
            Wave.clearWavdata(record)
            collectgarbage("collect")

            wait(0.1)
            if interface == "SPI" then iM501.enter_NORMAL(2,12000) end
            if interface == "I2C" then iM501.enter_NORMAL(1,100) end
            --collectgarbage("collect")
        end
        if const.genreport then         -- generate report 
            trigger_Count (mode,recordpath,fileout,xsdch,sdch,keyname,timeLast)
        end
    end       

end
-- Multi channel voicebuffer read report generate , 2,3,4 channel TBD
function  buffer_check (appConfig,mode,channel,inputfile, inputpath,outputpath,keyname,module)
    local delayplatform =  200  ---ms
    local start_row = 4 
    local start_col = 2
    local sheetname = "Multi_Voicebuffer_"..channel
    local wave_org = wave.load(inputpath..inputfile..".wav")
    local channel_num = wave.get_property(wave_org,"channel_num")
    --local timelast = wave.get_property(wave_org,"time_last")
    local wave_array= wave.n_to_mono(wave_org)
    local wave_mic0
    Wave.clearWavdata(wave_org)
    if channel_num == 1 then 
        wave_mic0= wave.resample(wave_array[1],16000)
        --wave.save("tets.wav",wave_mic0)
    else
        wave_mic0= wave_array[1]
        --wave_mic1= wave_array[2]
    end
    --wave.save("wave_mic0.wav",wave_mic0)
    --open excel report 
    local excel_output = luacom.excel.create()
    local mybookout = luacom.excel.open_file(excel_output, GLOBAL.RESULT_EXCEL)
    for i = 1,const.loop do 
       local fileout_name =mode.."_"..inputfile.."_"..i.."th_"..channel.."channel_rec.wav"
       local wave_out = wave.load(outputpath.."\\"..fileout_name)
       local wave_out_array = wave.n_to_mono(wave_out)
       Wave.clearWavdata(wave_out)
       local wave_mic0_out = wave_out_array[1]
       local wave_lout = wave_out_array[2]
       if appConfig.name == "VoiceControl" then
            wave_lout = wave_out_array[1]
       end
       local bit_rate = wave.get_property(wave_mic0_out,"bit_per_sample")
       local length = wave.get_property(wave_mic0_out,"time_last")
       --------------------------------------------------------------
       -- one buffer report 
        if channel == 1  then 
            wave_xSD= wave_out_array[3]
            wave_SD = wave_out_array[4]
            wave_buffer1 = wave_out_array[5]
            if mode =="PSM" then start_col = 8 end
            -- only 1 buffer read has xSD SD width report 
            if module == "_AVD_xSD_SD" or module == "_xSD_SD" then 
                local xSD_width
                local SD_width
                local xSD_array = single_duration_trigger_count(wave_xSD, const.calparam.key.xsd_continus_num, 2*16000, 5*16000, const.calparam.key.xsd_trigger_thd, bit_rate)
                local SD_array = single_duration_trigger_count(wave_SD, const.calparam.key.sd_continus_num, 3*16000, 7*16000, const.calparam.key.sd_trigger_thd, bit_rate)
                xSD_width = xSD_array.continus/bit_rate
                SD_width = SD_array.continus/bit_rate
                luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col, xSD_width)
                luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+1, SD_width) 
            end
            if module == "_AVD_SD"  or module == "_SD" then 
                local xSD_width = "/"
                local SD_width
                local SD_array = single_duration_trigger_count(wave_SD, const.calparam.key.sd_continus_num, 3*16000, 7*16000, const.calparam.key.sd_trigger_thd, bit_rate)
                SD_width = SD_array.continus/bit_rate
                luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col, xSD_width)
                luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+1, SD_width) 
            end
            Wave.clearWavdata(wave_xSD)
            Wave.clearWavdata(wave_SD)  
            if mode =="NOR" then 
                -- mic0 wavedata analysis 
                --[[
                local delaymic0 = Wave.delay(wave_mic0_out, wave_mic0, 2)
                wave_mic0_out = wave.cut(wave_mic0_out,delayplatform + delaymic0,length*1000)
                local pos_smic0 = wave.unsilence_len(wave_mic0_out,-120)  
                local pos_emic0 = wave.tail_unsilence_len(wave_mic0_out,-120)
                local newlength = wave.get_property(wave_mic0_out,"time_last")
                local wave_mic0_1 = wave.cut(wave_mic0_out,0,pos_smic0)
                local wave_mic0_2 = wave.cut(wave_mic0_out,newlength*1000-pos_emic0,newlength*1000)
                wave_mic0_out = wave.append(wave_mic0_1,wave_mic0_2)
                 wave.save("cutmic0outnew.wav",wave_mic0_out)
                ]]--

                -- buffer1  wavedata analysis
                local pos_s= wave.silence_len(wave_buffer1, -90)     
                local pos_e= wave.tail_silence_len(wave_buffer1, -90)  
                wave_buffer1= wave.cut(wave_buffer1,pos_s,length*1000-pos_e)
                wave.save("cutbuffer1.wav",wave_buffer1)
                --io.read()
                local tbuffer = wave.get_property(wave_buffer1,"time_last")

                -- Lout wavedata analysis 
                local delaylout = Wave.delay(wave_lout, wave_mic0, 2)
                wave_lout = wave.cut(wave_lout,delayplatform + delaylout,length*1000)
                local pos_slout = wave.unsilence_len(wave_lout,-120)  
                local pos_elout = wave.tail_unsilence_len(wave_lout,-120)
                local newlength = wave.get_property(wave_lout,"time_last")
                local wave_lout_1 = wave.cut(wave_lout,0,pos_slout)
                local wave_lout_2 = wave.cut(wave_lout,newlength*1000-pos_elout,newlength*1000)
                wave_lout = wave.append(wave_lout_1,wave_lout_2)
                --wave.save("cutlout1new"..i.."_.wav",wave_lout_1)
                --wave.save("cutlout2new"..i.."_.wav",wave_lout_2)
                wave.save("cutloutnew"..i.."_.wav",wave_lout)
                --io.read()
                local t = math.round((pos_slout+pos_elout)/1000-1)
                --print(t)
                --io.read()
                pesqlout = wave.pesq(wave_mic0,wave_lout,{{0,t}},true)
                powerlout = wave.power(wave_lout)
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{2.380,2.380+tbuffer},{0,tbuffer}})
                powerbuffer1 =  wave.power(wave_buffer1)
                ----------
                Wave.clearWavdata(wave_lout_1)
                Wave.clearWavdata(wave_lout_2)
            end
            if mode =="PSM" then 
             
                --buffer1  wavedata analysis
                local pos_s= wave.silence_len(wave_buffer1, -90)     
                local pos_e= wave.tail_silence_len(wave_buffer1, -90) 
                wave_buffer1= wave.cut(wave_buffer1,pos_s,length*1000-pos_e)
                --wave.save("cutbuffer1.wav",wave_buffer1) 
                local tbuffer = wave.get_property(wave_buffer1,"time_last")
                local pos_elout = wave.tail_unsilence_len(wave_lout,-80)
                local wave_lout = wave.cut(wave_lout,length*1000-pos_e+100,length*1000-pos_elout)
                wave.save("cutloutnew"..i.."_.wav",wave_lout)
                local newlength = wave.get_property(wave_lout,"time_last")
                --print(pos_s,pos_e)
                --print(pos_elout)
                --io.read()
                pesqlout = wave.pesq(wave_mic0,wave_lout,{{6.60,6.60+newlength},{0,newlength}},true)
                powerlout = wave.power(wave_lout)
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{2.380,2.380+tbuffer},{0,tbuffer}})
                powerbuffer1 =  wave.power(wave_buffer1)
     
            end
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+2, powerlout) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+3, pesqlout)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+4, powerbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+5, pesqbuffer1)

            Wave.clearWavdata(wave_buffer1)
        end
        -------------------------------------------------
        -- two buffer report 
        if channel == 2  then 
            wave_buffer1 = wave_out_array[3]
            wave_buffer2 = wave_out_array[4]
            if mode =="PSM" then start_col = 8 end
            if mode =="NOR" then 
                -- buffer1  buffer2 wavedata analysis
                local pos_s1= wave.silence_len(wave_buffer1, -90)     
                local pos_e1= wave.tail_silence_len(wave_buffer1, -90)  
                local pos_s2= wave.silence_len(wave_buffer2, -90)     
                local pos_e2= wave.tail_silence_len(wave_buffer2, -90)  
                wave_buffer1= wave.cut(wave_buffer1,pos_s1,length*1000-pos_e1)
                wave_buffer2= wave.cut(wave_buffer2,pos_s2,length*1000-pos_e2)
                --wave.save("cutbuffer1.wav",wave_buffer1)
                --wave.save("cutbuffer2.wav",wave_buffer2)
                local tbuffer1 = wave.get_property(wave_buffer1,"time_last")
                local tbuffer2 = wave.get_property(wave_buffer2,"time_last")
                -- Lout wavedata analysis 
                local delaylout = Wave.delay(wave_lout, wave_mic0, 2)
                wave_lout = wave.cut(wave_lout,delayplatform + delaylout,length*1000)
                local pos_slout = wave.unsilence_len(wave_lout,-120)  
                local pos_elout = wave.tail_unsilence_len(wave_lout,-120)
                local newlength = wave.get_property(wave_lout,"time_last")
                local wave_lout_1 = wave.cut(wave_lout,0,pos_slout)
                local wave_lout_2 = wave.cut(wave_lout,newlength*1000-pos_elout,newlength*1000)
                wave_lout = wave.append(wave_lout_1,wave_lout_2)
                --wave.save("cutloutnew"..i.."_.wav",wave_lout)
                local t = math.round((pos_slout+pos_elout)/1000-1)
                pesqlout = wave.pesq(wave_mic0,wave_lout,{{0,t}},true)
                powerlout = wave.power(wave_lout)
                -- the bufffer voice start piont in mic0 wave is importanr  !!!!!!
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{3.941,3.941+tbuffer1},{0,tbuffer1}})
                powerbuffer1 =  wave.power(wave_buffer1)
                pesqbuffer2 =  wave.pesq(wave_mic0,wave_buffer2,{{3.941,3.941+tbuffer2},{0,tbuffer2}})
                powerbuffer2 =  wave.power(wave_buffer2)       
                     
                Wave.clearWavdata(wave_lout_1)
                Wave.clearWavdata(wave_lout_2)
            end
            if mode =="PSM" then 

                --buffer1  wavedata analysis
                local pos_s1= wave.silence_len(wave_buffer1, -90)     
                local pos_e1= wave.tail_silence_len(wave_buffer1, -90)  
                local pos_s2= wave.silence_len(wave_buffer2, -90)     
                local pos_e2= wave.tail_silence_len(wave_buffer2, -90)  
                wave_buffer1= wave.cut(wave_buffer1,pos_s1,length*1000-pos_e1)
                wave_buffer2= wave.cut(wave_buffer2,pos_s2,length*1000-pos_e2)
                --wave.save("cutbuffer1.wav",wave_buffer1)
                --wave.save("cutbuffer2.wav",wave_buffer2)
                local tbuffer1 = wave.get_property(wave_buffer1,"time_last")
                local tbuffer2 = wave.get_property(wave_buffer2,"time_last")
                ---Lout calculation 
                local pos_elout = wave.tail_unsilence_len(wave_lout,-80)
                local wave_lout = wave.cut(wave_lout,length*1000-pos_e1+100,length*1000-pos_elout)
                --wave.save("cutloutnew"..i.."_.wav",wave_lout)
                local newlength = wave.get_property(wave_lout,"time_last")

                pesqlout = wave.pesq(wave_mic0,wave_lout,{{6.926,6.926+newlength},{0,newlength}},true)
                powerlout = wave.power(wave_lout)
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{3.941,3.941+tbuffer1},{0,tbuffer1}})
                powerbuffer1 =  wave.power(wave_buffer1)
                pesqbuffer2 =  wave.pesq(wave_mic0,wave_buffer2,{{3.941,3.941+tbuffer2},{0,tbuffer2}})
                powerbuffer2 =  wave.power(wave_buffer2)
     

            end
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col, powerlout) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+1, pesqlout)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+2, powerbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+3, pesqbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+4, powerbuffer2)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+5, pesqbuffer2)


            Wave.clearWavdata(wave_buffer1)
            Wave.clearWavdata(wave_buffer2)
        end
        ------------------------------
        -- three buffer report 
        if channel == 3  then 
            wave_buffer1 = wave_out_array[3]
            wave_buffer2 = wave_out_array[4]
            wave_buffer3 = wave_out_array[5]
            if mode =="PSM" then start_col = 10 end
            if mode =="NOR" then 
                -- buffer1  buffer2 wavedata analysis
                local pos_s1= wave.silence_len(wave_buffer1, -90)     
                local pos_e1= wave.tail_silence_len(wave_buffer1, -90)  
                local pos_s2= wave.silence_len(wave_buffer2, -90)     
                local pos_e2= wave.tail_silence_len(wave_buffer2, -90)
                local pos_s3= wave.silence_len(wave_buffer3, -90)     
                local pos_e3= wave.tail_silence_len(wave_buffer3, -90)  
                wave_buffer1= wave.cut(wave_buffer1,pos_s1,length*1000-pos_e1)
                wave_buffer2= wave.cut(wave_buffer2,pos_s2,length*1000-pos_e2)
                wave_buffer3= wave.cut(wave_buffer3,pos_s3,length*1000-pos_e3)
                --wave.save("cutbuffer1.wav",wave_buffer1)
                --wave.save("cutbuffer2.wav",wave_buffer2)
                --wave.save("cutbuffer3.wav",wave_buffer3)
                local tbuffer1 = wave.get_property(wave_buffer1,"time_last")
                local tbuffer2 = wave.get_property(wave_buffer2,"time_last")
                local tbuffer3 = wave.get_property(wave_buffer3,"time_last")
                -- Lout wavedata analysis 
                local delaylout = Wave.delay(wave_lout, wave_mic0, 2)
                wave_lout = wave.cut(wave_lout,delayplatform + delaylout,length*1000)
                local pos_slout = wave.unsilence_len(wave_lout,-120)  
                local pos_elout = wave.tail_unsilence_len(wave_lout,-120)
                local newlength = wave.get_property(wave_lout,"time_last")
                local wave_lout_1 = wave.cut(wave_lout,0,pos_slout)
                local wave_lout_2 = wave.cut(wave_lout,newlength*1000-pos_elout,newlength*1000)
                wave_lout = wave.append(wave_lout_1,wave_lout_2)
                --wave.save("cutloutnew"..i.."_.wav",wave_lout)
                local t = math.round((pos_slout+pos_elout)/1000-1)
                pesqlout = wave.pesq(wave_mic0,wave_lout,{{0,t}},true)
                powerlout = wave.power(wave_lout)
                -- the bufffer voice start piont in mic0 wave is importanr  !!!!!!
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{4.466,4.466+tbuffer1},{0,tbuffer1}})
                powerbuffer1 =  wave.power(wave_buffer1)
                pesqbuffer2 =  wave.pesq(wave_mic0,wave_buffer2,{{4.466,4.466+tbuffer2},{0,tbuffer2}})
                powerbuffer2 =  wave.power(wave_buffer2)
                pesqbuffer3 =  wave.pesq(wave_mic0,wave_buffer3,{{4.466,4.466+tbuffer3},{0,tbuffer3}})
                powerbuffer3 =  wave.power(wave_buffer3)
      
                Wave.clearWavdata(wave_lout_1)
                Wave.clearWavdata(wave_lout_2)
            end
            if mode =="PSM" then 
             
                --buffer1  wavedata analysis
                local pos_s1= wave.silence_len(wave_buffer1, -90)     
                local pos_e1= wave.tail_silence_len(wave_buffer1, -90)  
                local pos_s2= wave.silence_len(wave_buffer2, -90)     
                local pos_e2= wave.tail_silence_len(wave_buffer2, -90)  
                local pos_s3= wave.silence_len(wave_buffer3, -90)     
                local pos_e3= wave.tail_silence_len(wave_buffer3, -90) 
                wave_buffer1= wave.cut(wave_buffer1,pos_s1,length*1000-pos_e1)
                wave_buffer2= wave.cut(wave_buffer2,pos_s2,length*1000-pos_e2)
                wave_buffer3= wave.cut(wave_buffer3,pos_s3,length*1000-pos_e3)
                --wave.save("cutbuffer1.wav",wave_buffer1)
                --wave.save("cutbuffer2.wav",wave_buffer2)
                --wave.save("cutbuffer3.wav",wave_buffer3)
                local tbuffer1 = wave.get_property(wave_buffer1,"time_last")
                local tbuffer2 = wave.get_property(wave_buffer2,"time_last")
                local tbuffer3 = wave.get_property(wave_buffer3,"time_last")
                ---Lout calculation 
                local pos_elout = wave.tail_unsilence_len(wave_lout,-80)
                --print(pos_elout)
                --io.read()
                local wave_lout = wave.cut(wave_lout,length*1000-pos_e1+100,length*1000-pos_elout)
                local newlength = wave.get_property(wave_lout,"time_last")
                wave.save("cutloutnew"..i.."_.wav",wave_lout)
                ---- 7.906 this vaule is inportant , if test vector change ,this vaule will change 
                pesqlout = wave.pesq(wave_mic0,wave_lout,{{7.906,7.906+newlength},{0,newlength}},true)
                powerlout = wave.power(wave_lout)
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{4.466,4.466+tbuffer1},{0,tbuffer1}})
                powerbuffer1 =  wave.power(wave_buffer1)
                pesqbuffer2 =  wave.pesq(wave_mic0,wave_buffer2,{{4.466,4.466+tbuffer2},{0,tbuffer2}})
                powerbuffer2 =  wave.power(wave_buffer2)
                pesqbuffer3 =  wave.pesq(wave_mic0,wave_buffer3,{{4.466,4.466+tbuffer3},{0,tbuffer3}})
                powerbuffer3 =  wave.power(wave_buffer3)
     

            end
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col, powerlout) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+1, pesqlout)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+2, powerbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+3, pesqbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+4, powerbuffer2)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+5, pesqbuffer2)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+6, powerbuffer3)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+7, pesqbuffer3)


            Wave.clearWavdata(wave_buffer1)
            Wave.clearWavdata(wave_buffer2)
            Wave.clearWavdata(wave_buffer3)
        end
        ----------------------------------------------------------
        -- four buffer report 
        if channel == 4  then 
            wave_buffer1 = wave_out_array[3]
            wave_buffer2 = wave_out_array[4]
            wave_buffer3 = wave_out_array[5]
            wave_buffer4 = wave_out_array[6]
            if mode =="PSM" then start_col = 12 end
            if mode =="NOR" then 
                -- buffer1  buffer2 wavedata analysis
                local pos_s1= wave.silence_len(wave_buffer1, -90)     
                local pos_e1= wave.tail_silence_len(wave_buffer1, -90)  
                local pos_s2= wave.silence_len(wave_buffer2, -90)     
                local pos_e2= wave.tail_silence_len(wave_buffer2, -90)
                local pos_s3= wave.silence_len(wave_buffer3, -90)     
                local pos_e3= wave.tail_silence_len(wave_buffer3, -90) 
                local pos_s4= wave.silence_len(wave_buffer4, -90)     
                local pos_e4= wave.tail_silence_len(wave_buffer4, -90)  
                wave_buffer1= wave.cut(wave_buffer1,pos_s1,length*1000-pos_e1)
                wave_buffer2= wave.cut(wave_buffer2,pos_s2,length*1000-pos_e2)
                wave_buffer3= wave.cut(wave_buffer3,pos_s3,length*1000-pos_e3)
                wave_buffer4= wave.cut(wave_buffer4,pos_s4,length*1000-pos_e4)
                --wave.save("cutbuffer1.wav",wave_buffer1)
                --wave.save("cutbuffer2.wav",wave_buffer2)
                --wave.save("cutbuffer3.wav",wave_buffer3)
                --wave.save("cutbuffer4.wav",wave_buffer4)
                local tbuffer1 = wave.get_property(wave_buffer1,"time_last")
                local tbuffer2 = wave.get_property(wave_buffer2,"time_last")
                local tbuffer3 = wave.get_property(wave_buffer3,"time_last")
                local tbuffer4 = wave.get_property(wave_buffer4,"time_last")
                -- Lout wavedata analysis 
                local delaylout = Wave.delay(wave_lout, wave_mic0, 2)
                wave_lout = wave.cut(wave_lout,delayplatform + delaylout,length*1000)
                local pos_slout = wave.unsilence_len(wave_lout,-120)  
                local pos_elout = wave.tail_unsilence_len(wave_lout,-120)
                local newlength = wave.get_property(wave_lout,"time_last")
                local wave_lout_1 = wave.cut(wave_lout,0,pos_slout)
                local wave_lout_2 = wave.cut(wave_lout,newlength*1000-pos_elout,newlength*1000)
                wave_lout = wave.append(wave_lout_1,wave_lout_2)
                --wave.save("cutloutnew"..i.."_.wav",wave_lout)
                local t = math.round((pos_slout+pos_elout)/1000-1)
                pesqlout = wave.pesq(wave_mic0,wave_lout,{{0,t}},true)
                powerlout = wave.power(wave_lout)
                -- the bufffer voice start piont in mic0 wave is importanr  !!!!!!
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{4.790,4.790+tbuffer1},{0,tbuffer1}})
                powerbuffer1 =  wave.power(wave_buffer1)
                pesqbuffer2 =  wave.pesq(wave_mic0,wave_buffer2,{{4.790,4.790+tbuffer2},{0,tbuffer2}})
                powerbuffer2 =  wave.power(wave_buffer2)
                pesqbuffer3 =  wave.pesq(wave_mic0,wave_buffer3,{{4.790,4.790+tbuffer3},{0,tbuffer3}})
                powerbuffer3 =  wave.power(wave_buffer3)
                pesqbuffer4 =  wave.pesq(wave_mic0,wave_buffer4,{{4.790,4.790+tbuffer4},{0,tbuffer4}})
                powerbuffer4 =  wave.power(wave_buffer4)       
                Wave.clearWavdata(wave_lout_1)
                Wave.clearWavdata(wave_lout_2)
            end
            if mode =="PSM" then 
             
                --buffer1  wavedata analysis
                local pos_s1= wave.silence_len(wave_buffer1, -90)     
                local pos_e1= wave.tail_silence_len(wave_buffer1, -90)  
                local pos_s2= wave.silence_len(wave_buffer2, -90)     
                local pos_e2= wave.tail_silence_len(wave_buffer2, -90)  
                local pos_s3= wave.silence_len(wave_buffer3, -90)     
                local pos_e3= wave.tail_silence_len(wave_buffer3, -90) 
                local pos_s4= wave.silence_len(wave_buffer4, -90)     
                local pos_e4= wave.tail_silence_len(wave_buffer4, -90) 
                wave_buffer1= wave.cut(wave_buffer1,pos_s1,length*1000-pos_e1)
                wave_buffer2= wave.cut(wave_buffer2,pos_s2,length*1000-pos_e2)
                wave_buffer3= wave.cut(wave_buffer3,pos_s3,length*1000-pos_e3)
                wave_buffer4= wave.cut(wave_buffer4,pos_s4,length*1000-pos_e4)
                --wave.save("cutbuffer1.wav",wave_buffer1)
                --wave.save("cutbuffer2.wav",wave_buffer2)
                --wave.save("cutbuffer3.wav",wave_buffer3)
                --wave.save("cutbuffer4.wav",wave_buffer4)
                local tbuffer1 = wave.get_property(wave_buffer1,"time_last")
                local tbuffer2 = wave.get_property(wave_buffer2,"time_last")
                local tbuffer3 = wave.get_property(wave_buffer3,"time_last")
                local tbuffer4 = wave.get_property(wave_buffer4,"time_last")
 
                pesqlout = "/"
                powerlout = "/"
                pesqbuffer1 =  wave.pesq(wave_mic0,wave_buffer1,{{4.790,4.790+tbuffer1},{0,tbuffer1}})
                powerbuffer1 =  wave.power(wave_buffer1)
                pesqbuffer2 =  wave.pesq(wave_mic0,wave_buffer2,{{4.790,4.790+tbuffer2},{0,tbuffer2}})
                powerbuffer2 =  wave.power(wave_buffer2)
                pesqbuffer3 =  wave.pesq(wave_mic0,wave_buffer3,{{4.790,4.790+tbuffer3},{0,tbuffer3}})
                powerbuffer3 =  wave.power(wave_buffer3)
                pesqbuffer4 =  wave.pesq(wave_mic0,wave_buffer4,{{4.790,4.790+tbuffer4},{0,tbuffer4}})
                powerbuffer4 =  wave.power(wave_buffer4)
            end
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col, powerlout) 
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+1, pesqlout)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+2, powerbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+3, pesqbuffer1)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+4, powerbuffer2)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+5, pesqbuffer2)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+6, powerbuffer3)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+7, pesqbuffer3)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+8, powerbuffer4)
            luacom.excel.write_cells_value(mybookout, sheetname, start_row + i, start_col+9, pesqbuffer4)

            Wave.clearWavdata(wave_buffer1)
            Wave.clearWavdata(wave_buffer2)
            Wave.clearWavdata(wave_buffer3)
            Wave.clearWavdata(wave_buffer4)
        end
        for j = 1, #wave_out_array do
            Wave.clearWavdata(wave_out_array[j])
        end
        Wave.clearWavdata(wave_mic0_out)
        Wave.clearWavdata(wave_lout)
       
    end
    for k = 1, #wave_array do
        Wave.clearWavdata(wave_array[k])
    end
    Wave.clearWavdata(wave_mic0)
    -- Delete other sheet 
    for n=1,#const.keywordList do 
        mybookout = luacom.excel.delete_sheet(mybookout, const.keywordList[n].name)
    end
    for ch = 1,4 do
        if ch ~= channel then 
            sheetname= "Multi_Voicebuffer_"..ch
            mybookout = luacom.excel.delete_sheet(mybookout, sheetname)
        end
    end
    luacom.excel.saveas(mybookout,GLOBAL.RESULT_EXCEL)
    luacom.excel.quit(excel_output)
end


function  Multi_voicebuffer_real(appConfig,moduleSwitch, channel,inputpath, inputfile, outputpath, module, fs ,nor, psm, keyname,module)      
    local playdev
    local playdevice= audio.get_dev(1)
    for n = 1,#playdevice do 
        if playdevice[n] == const.Speakers then 
            playdev =playdevice[n]
        else
            playdev = playdevice[1]
        end
    end
  -- set volume need checked
    audio.set_volume(playdev, 50, 50)
    local platformDelayS = regression.config.platform.delay
    local sourcefile = inputpath..inputfile..".wav"
    --print(sourcefile)
    --io.read()
    local wave_org= wave.load(sourcefile)
    local timeLast = Wave.get_timelast(wave_org)
    local length = timeLast*1000
    local platformDelayS = regression.config.platform.delay
    local t =math.round((timeLast + platformDelayS + 0.3)*10)/10
    local interface = "SPI"
    if moduleSwitch.fastdowncode ~= 1 then interface = "I2C" end
    if nor then 
        local mode = "NOR"
        local outputpath = lfs.create_folder(outputpath .."\\"..mode.."\\")

        for i =1 ,const.loop do 
            local fileout_name =mode.."_"..inputfile.."_"..i.."th_"..channel.."channel_rec.wav"
            if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
                start_play_record()
                iM501.rec_voice_buffer_NM()
                audio.play(playdev,sourcefile,0,false)
                print("wait time is .."..t)
                wait(t)
                audio.stop_play(playdev)
                stop_play_record()
                --collectgarbage("collect")
                local wave_data = wave.load("rectmp.wav")
                local wave_array=wave.n_to_mono(wave_data)
                Wave.clearWavdata(wave_data)
                local num = #wave_array
                local new_data
                if channel == 1 then 
                -- mic0 lout xSD SD Voicebuffer
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data)  
                end
                if channel == 2 then
                    -- mic0 lout  Voicebuffer 1 2
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 3 then 
                    -- mic0 lout  Voicebuffer 1 2 3
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 4 then 
                    -- mic0 lout  Voicebuffer 1 2 3 4
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-3],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                Wave.clearWavdata(new_data)
                for i =1 ,#wave_array  do 
                    Wave.clearWavdata(wave_array[i])
                end
                collectgarbage("collect")
            end
        end
        if const.genreport then         -- generate report 
            buffer_check (appConfig,mode,channel,inputfile, inputpath,outputpath,keyname,module)
        end
    end
    if psm then
        local mode = "PSM"
        local outputpath = lfs.create_folder(outputpath .."\\"..mode.."\\")
        if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
            if interface == "SPI" then iM501.enter_PSM(2,12000) end
            if interface == "I2C" then iM501.enter_PSM(1,100) end   
        end
        for i = 1,const.loop do

            local fileout_name =mode.."_"..inputfile.."_"..i.."th_"..channel.."channel_rec.wav"
            if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
                -- play & record
                start_play_record()
                iM501.rec_voice_buffer()
                --iM501.rec_voice_buffer_PSM()
                audio.play(playdev,sourcefile,0,false)
                print("wait time is .."..t)
                wait(t)
                audio.stop_play(playdev)
                stop_play_record()
                local wave_data = wave.load("rectmp.wav")
                local wave_array=wave.n_to_mono(wave_data)
                Wave.clearWavdata(wave_data)
                local num = #wave_array
                local new_data
                if channel == 1 then 
                    -- mic0 lout xSD SD Voicebuffer
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data)  
                end
                if channel == 2 then
                    -- mic0 lout  Voicebuffer 1 2
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 3 then 
                    -- mic0 lout  Voicebuffer 1 2 3
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 4 then 
                    -- mic0 lout  Voicebuffer 1 2 3 4
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-3],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                Wave.clearWavdata(new_data)
                for i =1 ,#wave_array  do 
                    Wave.clearWavdata(wave_array[i])
                end
                collectgarbage("collect")
        
                      
            end
        end
        wait(1)
        if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then  
            if interface == "SPI" then iM501.enter_NORMAL(2,12000) end
            if interface == "I2C" then iM501.enter_NORMAL(1,100) end
        end
        
        if const.genreport then         -- generate report 
            buffer_check (appConfig,mode,channel,inputfile, inputpath,outputpath,keyname,module)
        end
    end       
        
 
end 


 function  Multi_voicebuffer_simulation(appConfig,moduleSwitch, channel,inputpath, inputfile, outputpath, module, fs ,nor, psm, keyname,module)

    local platformDelayS = regression.config.platform.delay
    local timeLast = play_wave_gen(appConfig.chDef.input, inputfile, inputpath,fs)
    local length = timeLast*1000
    local t =math.round((timeLast + platformDelayS + 0.3)*10)/10
    local interface = "SPI"
    if moduleSwitch.fastdowncode ~= 1 then interface = "I2C" end
    if nor then 
        local mode = "NOR"
        local outputpath = lfs.create_folder(outputpath .."\\"..mode.."\\")

        for i =1 ,const.loop do 
            local fileout_name =mode.."_"..inputfile.."_"..i.."th_"..channel.."channel_rec.wav"
            --print(fileout)
            --io.read()
            if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
                start_play_record()
                iM501.rec_voice_buffer_NM()
                print("wait time is .."..t)
                wait(t)
                stop_play_record()
                local wave_data = wave.load("rectmp.wav")
                local wave_array=wave.n_to_mono(wave_data)
                Wave.clearWavdata(wave_data)
                local num = #wave_array
                local new_data
                if channel == 1 then 
                    -- mic0 lout xSD SD Voicebuffer
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data)  
                end
                if channel == 2 then
                    -- mic0 lout  Voicebuffer 1 2
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 3 then 
                    -- mic0 lout  Voicebuffer 1 2 3
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 4 then 
                     -- mic0 lout  Voicebuffer 1 2 3 4
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-3],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                Wave.clearWavdata(new_data)
                for i =1 ,#wave_array  do 
                    Wave.clearWavdata(wave_array[i])
                end
                collectgarbage("collect")
            end
        end
        if const.genreport then         -- generate report 
            buffer_check (appConfig,mode,channel,inputfile, inputpath,outputpath,keyname,module)
        end
    end
    if psm then
        local mode = "PSM"
        local outputpath = lfs.create_folder(outputpath .."\\"..mode.."\\")
        if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
            if interface == "SPI" then iM501.enter_PSM(2,12000) end
            if interface == "I2C" then iM501.enter_PSM(1,100) end
           
        end
        for i = 1,const.loop do 

            local fileout_name =mode.."_"..inputfile.."_"..i.."th_"..channel.."channel_rec.wav"
            if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then
                -- play & record
                start_play_record()
                iM501.rec_voice_buffer()
                --iM501.rec_voice_buffer_PSM()
                print("wait time is .."..t)
                wait(t)
                stop_play_record()
                --collectgarbage("collect")
                local wave_data = wave.load("rectmp.wav")
                local wave_array=wave.n_to_mono(wave_data)
                Wave.clearWavdata(wave_data)
                local num = #wave_array
                local new_data
                if channel == 1 then 
                    -- mic0 lout xSD SD Voicebuffer
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data)  
                end
                if channel == 2 then
                    -- mic0 lout  Voicebuffer 1 2
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 3 then 
                    -- mic0 lout  Voicebuffer 1 2 3
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                if channel == 4 then 
                     -- mic0 lout  Voicebuffer 1 2 3 4
                    new_data = wave.mono_to_n({wave_array[1],wave_array[2],wave_array[num-3],wave_array[num-2],wave_array[num-1],wave_array[num]},1)
                    Wave.save(outputpath.."\\"..fileout_name , new_data) 
                end
                Wave.clearWavdata(new_data)
                for i =1 ,#wave_array  do 
                    Wave.clearWavdata(wave_array[i])
                end
                collectgarbage("collect")

               --collectgarbage("collect")
            end
        end
        wait(1)
        if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
            if interface == "SPI" then iM501.enter_NORMAL(2,12000) end
            if interface == "I2C" then iM501.enter_NORMAL(1,100) end
        end

        if const.genreport then         -- generate report 
            buffer_check (appConfig,mode,channel,inputfile, inputpath,outputpath,keyname,module)
        end
    end       

 end 


function regression.test_AcousticAwareness(appConfig, moduleSwitch)
    local micstr = nil
    local channel
    local pdmstr = "I2S"
    if moduleSwitch.connectionMode == 2 then pdmstr = "PDMCLK2048K" end 
    if moduleSwitch.connectionMode == 3 then pdmstr = "PDMCLK1024K" end 
    if moduleSwitch.connectionMode == 4 then pdmstr = "PDMCLK2000K" end 
    if moduleSwitch.connectionMode == 5 then pdmstr = "PDMCLK3250K" end
    if moduleSwitch.connectionMode == 6 then pdmstr = "PDMCLK1200K" end
    if moduleSwitch.connectionMode == 7 then pdmstr = "PDMCLK2400K" end  
    local micdistance = 0 
    local module = ""
    local recordList = {}
    local xsdch = nil 
    local sdch = nil 
    for i = 1, #const.engineList do 
        module = module.."_".. const.engineList[i].name
    end
    recordList,xsdch,sdch = reocrdChnnel(module)
    --print(xsdch,sdch)
    --io.read()
    local  fileout_path = lfs.create_folder(regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\"..const.path)
    --print(fileout_path)
    for i=1,#const.micMode do
        for j=1,#const.fsMode do
            for k=1,#const.keywordList do
                if const.keywordList[k].enable then 
                    notice.message_lib("AcousticAwareness", "********\t" .. const.micMode[i].." " .. const.fsMode[j] .."Hz Mode, Keywords: " .. const.keywordList[k].name .. " Start\t********", "Info", true, true, false)           
                    regression.config.PATH.DSP = regression.config.PATH.dspHome .. appConfig.name .. "\\"..pdmstr.."\\"
                    --regression.config.PATH.DSP = regression.config.PATH.dspHome .. appConfig.name .. "\\"..pdmstr.."\\"..const.keywordList[k].name.."\\"  -- for one application different keyword loop 
                    if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
                        intial_platform(regression.config.dut.name, const.fsMode[j], regression.config.platform.module, moduleSwitch.connectionMode,moduleSwitch.fastdowncode)
                        iM501.init(regression.config.PATH.DSP, const.micMode[i], const.fsMode[j],moduleSwitch.fastdowncode)
                        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        iM501.read_dram(0xfff9fc0)
                        wait(0.1)
                        iM501.read_dram(0xfff9fc0)
                        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        --io.read()
                        local flag ,value
                        if appConfig.name == "EchoLite" then
                            flag , value = iM501.read_dram(0xfff9f68,0x4)
                            regression.config.baseaddr= value
                            local flagmic ,micdistance = iM501.read_dram(regression.config.baseaddr + 0x26, 0x2)
                            local micdistance = string.format("%#x",micdistance) 
                            if micdistance == "0x5a" or "0x5c" then 
                                micstr = "9cm"
                            elseif micdistance == "0x3c" then 
                                micstr = "6cm"
                            end
                        end
                        channel= GUI.VID.VB_channel_update()
                    end
                    if channel == nil then  channel = 1 end
                    --if channel == nil then  channel = 3end
                    --print(micstr)
                    --io.read()
                    -- run wake up real test 
                    if const.testMode[1].enable then 

                        local year = os.date("%Y")
                        local date = os.date("%m%d")
                        local time = os.date("%H%M")
                        local output_path = lfs.create_folder(fileout_path.."\\"..const.testMode[1].name.."\\"..module.."\\")
                        local input_path = regression.config.PATH.source ..const.path..const.testMode[1].vectorfolder
                
                        for m = 1, #const.neList do
                            if const.genreport then 
                                GLOBAL.RESULT_EXCEL = output_path.."\\".."report_AA_"..appConfig.name ..module.."_"..const.neList[m].name.."_".. const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
                                regression.report_initial(regression.config.PATH.template .. "AcousticAwareness_report_template.xlsx")
                                regression.report_cover_page(appConfig.name, const.micMode[i], const.fsMode[j], regression.config.PATH.DSP,const.keywordList[k].name)
                            end
                            inputfile = const.keywordList[k].name.."_"..const.neList[m].name
                            real_test(appConfig,moduleSwitch, input_path, inputfile, output_path, recordList, module, const.fsMode[j],const.NOR, const.PSM, const.oneshot,const.keywordList[k].name,xsdch,sdch)
                         end
                   
                    end
                    -- run wale up simulation test 
                    if const.testMode[2].enable then 
                        local year = os.date("%Y")
                        local date = os.date("%m%d")
                        local time = os.date("%H%M")
                        local output_path = lfs.create_folder(fileout_path.."\\"..const.testMode[2].name.."\\"..module.."\\")
                        local input_path = regression.config.PATH.source ..const.path..const.testMode[2].vectorfolder..appConfig.name.."\\"

                   
                        for m = 1, #const.neList do 
                            if micstr == nil then 
                                inputfile = const.keywordList[k].name.."_"..const.neList[m].name
                            else 
                                inputfile = const.keywordList[k].name.."_"..const.neList[m].name.."_"..micstr
                            end
                            if const.genreport then 
                                GLOBAL.RESULT_EXCEL = output_path.."\\".."report_AA_"..appConfig.name ..module.."_"..const.neList[m].name.."_".. const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
                                regression.report_initial(regression.config.PATH.template .. "AcousticAwareness_report_template.xlsx")
                                regression.report_cover_page(appConfig.name, const.micMode[i], const.fsMode[j], regression.config.PATH.DSP,const.keywordList[k].name) 
                            end
                            simulation_test(appConfig,moduleSwitch, input_path, inputfile, output_path, recordList, module, const.fsMode[j],const.NOR, const.PSM, const.oneshot,const.keywordList[k].name,xsdch,sdch)
                        end

                    end
                    --- multi channel  voice buffer read real test , channel can be 1,2,3,4 
                    if const.testMode[3].enable then 
                        local year = os.date("%Y")
                        local date = os.date("%m%d")
                        local time = os.date("%H%M")
                        local output_path = lfs.create_folder(fileout_path.."\\"..const.testMode[3].name.."\\"..channel.."_channel\\")
                        local input_path = regression.config.PATH.source..const.testMode[3].vectorfolder
                        local inputfile = const.keywordList[k].name
                        if const.genreport then 
                            GLOBAL.RESULT_EXCEL = output_path.."\\".."report_"..channel.."_Voicebuffer_read_"..appConfig.name ..module.."_"..const.keywordList[k].name.."_".. const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
                            regression.report_initial(regression.config.PATH.template .. "AcousticAwareness_report_template.xlsx")
                            regression.report_cover_page(appConfig.name, const.micMode[i], const.fsMode[j], regression.config.PATH.DSP,const.keywordList[k].name) 
                        end
                         Multi_voicebuffer_real(appConfig,moduleSwitch, channel, input_path, inputfile, output_path, module, const.fsMode[j],const.NOR, const.PSM, const.keywordList[k].name,module)

                    end
                    ---- multi channel  voice buffer read simulation  test , channel can be 1,2,3,4 
                    if const.testMode[4].enable then 
                        local year = os.date("%Y")
                        local date = os.date("%m%d")
                        local time = os.date("%H%M")
                        local output_path = lfs.create_folder(fileout_path.."\\"..const.testMode[4].name.."\\"..channel.."_channel\\")
                        local input_path = regression.config.PATH.source..const.testMode[4].vectorfolder
                        if micstr == nil then 
                            inputfile = const.keywordList[k].name.."_"..appConfig.name
                        else 
                            inputfile = const.keywordList[k].name.."_"..appConfig.name.."_"..micstr
                        end
                        if const.genreport then 
                            GLOBAL.RESULT_EXCEL = output_path.."\\".."report_"..channel.."_Voicebuffer_read_"..appConfig.name ..module.."_"..const.keywordList[k].name.."_".. const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
                            regression.report_initial(regression.config.PATH.template .. "AcousticAwareness_report_template.xlsx")
                            regression.report_cover_page(appConfig.name, const.micMode[i], const.fsMode[j], regression.config.PATH.DSP,const.keywordList[k].name) 
                        end
                         Multi_voicebuffer_simulation(appConfig,moduleSwitch, channel,input_path, inputfile, output_path, module, const.fsMode[j],const.NOR, const.PSM, const.keywordList[k].name,module)
                        

                    end

                    collectgarbage("collect")
                end

                collectgarbage("collect")
            end
            collectgarbage("collect")
        end
        collectgarbage("collect")
    end
    collectgarbage("collect")
end
