local case = {
    {
        caseName = [==[basicItems]==],
        items = {
            [1] = {
                name = [==[Sending Delay]==], module = "audio", submodule = [==[delay]==], unit = "ms", timeStartS = 28.5, timeEndS = 31.0,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 2}
            },
            [2] = {
                name = [==[Sending Power]==], module = "audio", submodule = [==[level]==], unit = "dBFS", timeStartS = 0.5, timeEndS = 31.0,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 3}
            },
            [3] = {
                name = [==[Sending Noise Floor]==], module = "audio", submodule = [==[level]==], unit = "dBFS", timeLastS = 1.0, flagFromEnd = true,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 6}
            },
            [4] = {
                name = [==[Sending Voice Quality - PESQ]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 32.0, nBlock=4,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 10}
            },
            [5] = {
                name = [==[Sending Voice Quality - POLQA]==], module = "audio", submodule = [==[POLQA]==], unit = " ", timeStartS = 0.0, timeEndS = 32.0, nBlock=4,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 11}
            },
        },
        vector = {
            clean = {file = "basicItem.DAT", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "basicItem_Echolite.wav", ch = 1},
            mic0 = {file = "basicItem_Echolite.wav", ch = 1},
            mic1 = {file = "basicItem_Echolite.wav", ch = 2},
        }
    },
    {
        caseName = [==[basicItems(Mic PGA Gain x2)]==],
        items = {
            [1] = {
                name = [==[Sending Power(Mic PGA Gain x2)]==], module = "audio", submodule = [==[level]==], unit = "dBFS", timeStartS = 0.5, timeEndS = 31.0,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 4}
            },
        },
        vector = {
            clean = {file = "basicItem.DAT", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "basicItem_Echolite.wav", ch = 1},
            mic0 = {file = "basicItem_Echolite.wav", ch = 1},
            mic1 = {file = "basicItem_Echolite.wav", ch = 2},
        },
        tune = {
            {name = "PGA_mic0", method = "multi", value = 2},
            {name = "PGA_mic1", method = "multi", value = 2},
        }
    },
    {
        caseName = [==[basicItems(Mic Volume x2)]==],
        items = {
            [1] = {
                name = [==[Sending Power(Mic Volume x2)]==], module = "audio", submodule = [==[level]==], unit = "dBFS", timeStartS = 0.5, timeEndS = 31.0,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 5}
            },
        },
        vector = {
            clean = {file = "basicItem.DAT", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "basicItem_Echolite.wav", ch = 1},
            mic0 = {file = "basicItem_Echolite.wav", ch = 1},
            mic1 = {file = "basicItem_Echolite.wav", ch = 2},
        },
        tune = {
            {name = "mic_volume", method = "multi", value = 2}
        }
    },
    {
        caseName = [==[noiseSuppression]==],
        items = {
            [1] = {
                name = [==[Sending Convergence Time]==], module = "audio", submodule = [==[convergence_time]==], unit = "s",
                report = {type = "value", sheet = "Basic Item", line = 7, col = 9}
            },
            [2] = {
                name = [==[Sending Noise Suppression]==], module = "audio", submodule = [==[noise_suppression]==], unit = "dB", timeLastS = 20.0, flagFromEnd = true,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 7}
            },
        },
        vector = {
            clean = nil, -- ACQUA data
            unprocessed = {file = "noiseSuppression_Echolite.wav", ch = 1},
            mic0 = {file = "noiseSuppression_Echolite.wav", ch = 1},
            mic1 = {file = "noiseSuppression_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        },
      
    },
    {
        caseName = [==[noiseSuppression(NS OFF)]==],
        items = {
            [1] = {
                name = [==[Sending Noise Suppression(NS OFF)]==], module = "audio", submodule = [==[noise_suppression]==], unit = "dB", timeLastS = 20.0, flagFromEnd = true,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 8}
            },
        },
        vector = {
            clean = nil, -- ACQUA data
            unprocessed = {file = "noiseSuppression_Echolite.wav", ch = 1},
            mic0 = {file = "noiseSuppression_Echolite.wav", ch = 1},
            mic1 = {file = "noiseSuppression_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        },
        switch = {mode = "NS", runSwitch = false, endSwtich = true},

    },
    {
        caseName = [==[basicItems(AEC)]==],
        items = {
            [1] = {
                name = [==[AEC]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS =0.2, timeEndS = 31.0,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 12}
            },
        },
        vector = {
            clean = {file = "AEC_Echolite.wav",ch = 1}, 
            unprocessed = {file = "AEC_Echolite.wav", ch = 1},
            mic0 = {file = "AEC_Echolite.wav", ch = 1},
            mic1 = {file = "AEC_Echolite.wav", ch = 2},
            aecrefL = {file = "AEC_Echolite.wav", ch = 5},
            aecrefR = {file = "AEC_Echolite.wav", ch = 8},
            bypassDelayCheck = true,
        }
       
    },
    {
        caseName = [==[basicItems(AEC_OFF)]==],
        items = {
            [1] = {
                name = [==[AEC_OFF]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS =0.2, timeEndS = 31.0,
                report = {type = "value", sheet = "Basic Item", line = 7, col = 13}
            },
        },
        vector = {
            clean = {file = "AEC_Echolite.wav",ch = 1}, 
            unprocessed = {file = "AEC_Echolite.wav", ch = 1},
            mic0 = {file = "AEC_Echolite.wav", ch = 1},
            mic1 = {file = "AEC_Echolite.wav", ch = 2},
            aecrefL = {file = "AEC_Echolite.wav", ch = 5},
            aecrefR = {file = "AEC_Echolite.wav", ch = 8},
            bypassDelayCheck = true,
        },
        switch = {mode = "AEC", runSwitch = false, endSwtich = true},  
  
    },
    {
        caseName = [==[AEC_onff]==],
        loop = 6,
        items = {
            [1] = {
                name = [==[AEC_ONOFF]==], module = "audio", submodule = [==[AEC_onoff]==],
            },
        },
        vector = {
            clean = {file = "AEC_Echolite.wav",ch = 1}, 
            unprocessed = {file = "AEC_Echolite.wav", ch = 1},
            mic0 = {file = "AEC_Echolite.wav", ch = 1},
            mic1 = {file = "AEC_Echolite.wav", ch = 2},
            aecrefL = {file = "AEC_Echolite.wav", ch = 5},
            aecrefR = {file = "AEC_Echolite.wav", ch = 8},
            bypassDelayCheck = true,
        }
       
    },
    {
        caseName = [==[POLQA_Item]==],
        items = {
            [1] = {
                name = [==[POLQA Item]==], module = "audio", submodule = [==[POLQAN]==], unit = " ", timeStartS = 40, timeEndS = 110, nBlock=8,
                report = {type = "matrix", sheet = "POLQA Item", colStart = 2, line = {4}}
            },
        },
        vector = {
            clean = {file = "SNRi3quest.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "SNRi3quest_fast_Echolite.wav", ch = 1},
            mic0 = {file = "SNRi3quest_fast_Echolite.wav", ch = 1},
            mic1 = {file = "SNRi3quest_fast_Echolite.wav", ch = 2},
            mainCheckProcessed = true,
            segList = {
                [1] = [==[NoBGN]==],
                [2] = [==[Mensa]==],
                [3] = [==[Car]==],
                [4] = [==[Train]==],
                [5] = [==[Road]==],
            },
        },
    },
    {
        caseName = [==[SNRi3quest]==],
        items = {
            [1] = {
                name = [==[3QUEST]==], module = "audio", submodule = [==[3QUEST]==], unit = " ", timeStartS = 0, timeEndS = 110,
                report = {type = "matrix", sheet = "3QUEST+SNRi", colStart = 2, line = {4, 6, 8}}
            },
            [2] = {
                name = [==[SNRi]==], module = "audio", submodule = [==[SNRi]==], unit = " ", timeStartS = 0, timeEndS = 110, segStartIdx = 2,
                report = {type = "matrix", sheet = "3QUEST+SNRi", colStart = 2, line = {23, 25, 27, 29}}
            },
        },
        vector = {
            clean = {file = "SNRi3quest.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "SNRi3quest_Echolite.wav", ch = 1},
            mic0 = {file = "SNRi3quest_Echolite.wav", ch = 1},
            mic1 = {file = "SNRi3quest_Echolite.wav", ch = 2},
            mainCheckProcessed = true,
            segList = {
                [1] = [==[NoBGN]==],
                [2] = [==[Pub]==],
                [3] = [==[Road]==],
                [4] = [==[XRoads]==],
                [5] = [==[Train]==],
                [6] = [==[Car]==],
                [7] = [==[Cafeteria]==],
                [8] = [==[Mensa]==],
                [9] = [==[CallCenter]==],
            },
        },
    },
    {
        caseName = [==[SNRi3quest_fast]==],
        items = {
            [1] = {
                name = [==[3QUEST Fast]==], module = "audio", submodule = [==[3QUEST]==], unit = " ", timeStartS = 0, timeEndS = 110,
                report = {type = "matrix", sheet = "3QUEST+SNRi(Fast)", colStart = 2, line = {4, 6, 8}}
            },
            [2] = {
                name = [==[SNRi Fast]==], module = "audio", submodule = [==[SNRi]==], unit = " ", timeStartS = 0, timeEndS = 110, segStartIdx = 2,
                report = {type = "matrix", sheet = "3QUEST+SNRi(Fast)", colStart = 2, line = {23, 25, 27, 29}}
            },
        },
        vector = {
            clean = {file = "SNRi3quest.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "SNRi3quest_fast_Echolite.wav", ch = 1},
            mic0 = {file = "SNRi3quest_fast_Echolite.wav", ch = 1},
            mic1 = {file = "SNRi3quest_fast_Echolite.wav", ch = 2},
            mainCheckProcessed = true,
            segList = {
                [1] = [==[NoBGN]==],
                [2] = [==[Mensa]==],
                [3] = [==[Car]==],
                [4] = [==[Train]==],
                [5] = [==[Road]==],
            },
        },
    },
    {
        caseName = [==[PolarPattern]==],
        items = {
            [1] = {
                name = [==[PolarPattern]==], module = "audio", submodule = [==[polarPattern]==], unit = "dB",
                report = {type = "matrix", sheet = "Polar Pattern", lineStart = 3, col = {2,3}}
            },
            [2] = {
                name = [==[PolarPattern - PESQ(female)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 15.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern", lineStart = 3, col = {4}}
            },
            [3] = {
                name = [==[PolarPattern - PESQ(male)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 15.0, timeEndS = 30.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern", lineStart = 3, col = {5}}
            },
        },
        vector = {
            clean = {file = "polarPattern.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "polarPattern_Echolite.wav", ch = 1},
            mic0 = {file = "polarPattern_Echolite.wav", ch = 1},
            mic1 = {file = "polarPattern_Echolite.wav", ch = 2},
            mainCheckProcessed = false,
            degreeStep = 10,
            segList = {},
        },
    },
    {
        caseName = [==[PolarPattern_BF_OFF]==],
        items = {
            [1] = {
                name = [==[PolarPattern_BF_OFF]==], module = "audio", submodule = [==[polarPattern]==], unit = "dB",
                report = {type = "matrix", sheet = "Polar Pattern", lineStart = 3, col = {6,7}}
            },
            [2] = {
                name = [==[PolarPattern_BF_OFF - PESQ(female)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 15.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern", lineStart = 3, col = {8}}
            },
            [3] = {
                name = [==[PolarPattern_BF_OFF - PESQ(male)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 15.0, timeEndS = 30.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern", lineStart = 3, col = {9}}
            },
        },
        vector = {
            clean = {file = "polarPattern.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "polarPattern_Echolite.wav", ch = 1},
            mic0 = {file = "polarPattern_Echolite.wav", ch = 1},
            mic1 = {file = "polarPattern_Echolite.wav", ch = 2},
            mainCheckProcessed = false,
            degreeStep = 10,
            segList = {},
        },
        switch = {mode = "BF", runSwitch = false, endSwtich = true}
    },
    {
        caseName = [==[PolarPattern Fast]==],
        items = {
            [1] = {
                name = [==[PolarPattern Fast]==], module = "audio", submodule = [==[polarPattern]==], unit = "dB",
                report = {type = "matrix", sheet = "Polar Pattern (Fast)", lineStart = 3, col = {2,3}}
            },
            [2] = {
                name = [==[PolarPattern Fast - PESQ(female)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 15.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern (Fast)", lineStart = 3, col = {4}}
            },
            [3] = {
                name = [==[PolarPattern Fast - PESQ(male)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 15.0, timeEndS = 30.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern (Fast)", lineStart = 3, col = {5}}
            },
        },
        vector = {
            clean = {file = "polarPattern.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "polarPattern_fast_Echolite.wav", ch = 1},
            mic0 = {file = "polarPattern_fast_Echolite.wav", ch = 1},
            mic1 = {file = "polarPattern_fast_Echolite.wav", ch = 2},
            mainCheckProcessed = false,
            degreeStep = 30,
            segList = {},
        },
    },
    {
        caseName = [==[PolarPattern_BF_OFF Fast]==],
        items = {
            [1] = {
                name = [==[PolarPattern_BF_OFF Fast]==], module = "audio", submodule = [==[polarPattern]==], unit = "dB",
                report = {type = "matrix", sheet = "Polar Pattern (Fast)", lineStart = 3, col = {6,7}}
            },
            [2] = {
                name = [==[PolarPattern_BF_OFF Fast - PESQ(female)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 15.0, nBlock=1,
                report = {type = "matrix", sheet =  "Polar Pattern (Fast)", lineStart = 3, col = {8}}
            },
            [3] = {
                name = [==[PolarPattern_BF_OFF Fast - PESQ(male)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 15.0, timeEndS = 30.0, nBlock=1,
                report = {type = "matrix", sheet =  "Polar Pattern (Fast)", lineStart = 3, col = {9}}
            },
        },
        vector = {
            clean = {file = "polarPattern.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "polarPattern_fast_Echolite.wav", ch = 1},
            mic0 = {file = "polarPattern_fast_Echolite.wav", ch = 1},
            mic1 = {file = "polarPattern_fast_Echolite.wav", ch = 2},
            mainCheckProcessed = false,
            degreeStep = 30,
            segList = {},
        },
        switch = {mode = "BF", runSwitch = false, endSwtich = true}
    },
    {
        caseName = [==[PolarPattern Ultra Fast]==],
        items = {
            [1] = {
                name = [==[PolarPattern Ultra Fast]==], module = "audio", submodule = [==[polarPattern]==], unit = "dB",
                report = {type = "matrix", sheet = "Polar Pattern (Ultra Fast)", lineStart = 3, col = {2,3}}
            },
            [2] = {
                name = [==[PolarPattern Ultra Fast - PESQ(female)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 15.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern (Ultra Fast)", lineStart = 3, col = {4}}
            },
            [3] = {
                name = [==[PolarPattern Ultra Fast - PESQ(male)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 15.0, timeEndS = 30.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern (Ultra Fast)", lineStart = 3, col = {5}}
            },
        },
        vector = {
            clean = {file = "polarPattern.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "polarPattern_ufast_Echolite.wav", ch = 1},
            mic0 = {file = "polarPattern_ufast_Echolite.wav", ch = 1},
            mic1 = {file = "polarPattern_ufast_Echolite.wav", ch = 2},
            mainCheckProcessed = false,
            degreeStep = 90,
            segList = {},
        },
    },
    {
        caseName = [==[PolarPattern_BF_OFF Ultra Fast]==],
        items = {
            [1] = {
                name = [==[PolarPattern_BF_OFF Ultra Fast]==], module = "audio", submodule = [==[polarPattern]==], unit = "dB",
                report = {type = "matrix", sheet = "Polar Pattern (Ultra Fast)", lineStart = 3, col = {6,7}}
            },
            [2] = {
                name = [==[PolarPattern_BF_OFF Ultra Fast - PESQ(female)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 15.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern (Ultra Fast)", lineStart = 3, col = {8}}
            },
            [3] = {
                name = [==[PolarPattern_BF_OFF Ultra Fast - PESQ(male)]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 15.0, timeEndS = 30.0, nBlock=1,
                report = {type = "matrix", sheet = "Polar Pattern (Ultra Fast)", lineStart = 3, col = {9}}
            },
        },
        vector = {
            clean = {file = "polarPattern.dat", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "polarPattern_ufast_Echolite.wav", ch = 1},
            mic0 = {file = "polarPattern_ufast_Echolite.wav", ch = 1},
            mic1 = {file = "polarPattern_ufast_Echolite.wav", ch = 2},
            mainCheckProcessed = false,
            degreeStep = 90,
            segList = {},
        },
        switch = {mode = "BF", runSwitch = false, endSwtich = true}
    },
    {
        caseName = [==[basicItems(fdEQ Switch On)]==],
        items = {
            [1] = {
                name = [==[Sending Frequency Response(fdEQ Switch On)]==], module = "audio", submodule = [==[frequency_response]==], unit = "dB", timeStartS = 0.0, timeEndS = 32.0,
                FFTSize = 4096, win = 2, overlap = 0.66, R = 4,
                report = {type = "matrix", sheet = "Frequency Response", lineStart = 3, col = {1, 2}}
            },
        },
        vector = {
            clean = {file = "basicItem.DAT", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "basicItem_Echolite.wav", ch = 1},
            mic0 = {file = "basicItem_Echolite.wav", ch = 1},
            mic1 = {file = "basicItem_Echolite.wav", ch = 2},
        },
        switch = {mode = "FdEQ", runSwitch = true, endSwtich = false},
        tune = {
            {name = "FdEQ_gain_0", method = "sub", value = 8738},
            {name = "FdEQ_gain_1", method = "sub", value = 8738},
            {name = "FdEQ_gain_2", method = "sub", value = 8738},
            {name = "FdEQ_gain_3", method = "sub", value = 8738},
            {name = "FdEQ_gain_4", method = "sub", value = 8738},
            {name = "FdEQ_gain_5", method = "sub", value = 8738},
        }
    },
    {
        caseName = [==[basicItems(fdEQ Switch Off)]==],
        items = {
            [1] = {
                name = [==[Sending Frequency Response(fdEQ Switch Off)]==], module = "audio", submodule = [==[frequency_response]==], unit = "dB", timeStartS = 0.0, timeEndS = 32.0,
                FFTSize = 4096, win = 2, overlap = 0.66, R = 4,
                report = {type = "matrix", sheet = "Frequency Response", lineStart = 3, col = {1, 3}}
            },
        },
        vector = {
            clean = {file = "basicItem.DAT", type = 3, ch = 1}, -- ACQUA data
            unprocessed = {file = "basicItem_Echolite.wav", ch = 1},
            mic0 = {file = "basicItem_Echolite.wav", ch = 1},
            mic1 = {file = "basicItem_Echolite.wav", ch = 2},
        },
        switch = {mode = "FdEQ", runSwitch = false, endSwtich = true}
    },
    {
        caseName = [==[Electronic]==],
        items = {
            [1] = {name = [==[MIPS]==], module = "electrical", submodule = [==[MIPS]==], unit = " ", report = {type = "value", sheet = "Basic Item", line = 7, col = 16}},
            [2] = {name = [==[mips]==], module = "electrical", submodule = [==[mips]==], unit = " "},
            [3] = {name = [==[LDO]==], module = "electrical", submodule = [==[LDO]==], unit = "V", report = {type = "value", sheet = "Basic Item", line = 7, col = 15}},
            [4] = {name = [==[Current]==], module = "electrical", submodule = [==[Current]==], unit = "mA", report = {type = "value", sheet = "Basic Item", line = 7, col = 17}},
            [5] = {name = [==[PSM_Current_AVD]==], module = "electrical", submodule = [==[PSM_Current_AVD]==], unit = "mA"},
            [6] = {name = [==[PSM_Current_xSD]==], module = "electrical", submodule = [==[PSM_Current_xSD]==], unit = "mA"},
            [7] = {name = [==[PSM_Current_SD]==], module = "electrical", submodule = [==[PSM_Current_SD]==], unit = "mA"},
        },
        vector = nil
    },
    {
        caseName = [==[VEN_Stress]==],
        loop = 1000,
        items = {
            [1] = {name = [==[VEN_Stress]==], module = "audio", submodule = [==[VEN_Stress]==]},
            [2] = {
                name = [==[VEN_Stress_pesq]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 11.0, nBlock=1,
                report = {type = "value", sheet = "stress_test", line = 3, col = 2, linIncrease=true}
            },
            [3] = {
                name =[==[VEN_Stress_power]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS = 15.0, timeEndS =22.0, nBlock=1,
                report = {type = "value", sheet ="stress_test", line = 3, col = 3, linIncrease=true}
            },
        },
        vector = {
            clean = {file = "ven_stress_Echolite.wav", ch = 1}, 
            unprocessed = {file = "ven_stress_Echolite.wav", ch = 1},
            mic0 = {file = "ven_stress_Echolite.wav", ch = 1},
            mic1 = {file = "ven_stress_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        }
    },
    {
        caseName = [==[Mode_switch]==],
        loop = 20,
        items = {
            [1] = {name = [==[Mode_switch]==], module = "audio", submodule = [==[Mode_switch]==]},
            [2] = {
                name = [==[Mode_switch_pesq]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 12.5, nBlock=1,
                report = {type = "value", sheet = "Mode_switch", line = 3, col = 2, linIncrease=true}
            },
            [3] = {
                name =[==[Mode_switch_power]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS = 0.0, timeEndS = 12.5, nBlock=1,
                report = {type = "value", sheet ="Mode_switch", line = 3, col = 3, linIncrease=true}
            },
        },
        vector = {
            clean = {file = "Modeswitch_Echolite.wav", ch = 1}, 
            unprocessed = {file = "Modeswitch_Echolite.wav", ch = 1},
            mic0 = {file = "Modeswitch_Echolite.wav", ch = 1},
            mic1 = {file = "Modeswitch_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        }
    },
    {
        caseName = [==[Download_Stress]==],
        loop = 1000,
        items = {
            [1] = {name = [==[Download_Stress]==], module = "audio", submodule = [==[Download_Stress]==]},
            [2] = {
                name = [==[Download_Stress_pesq]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 8.0, nBlock=1,
                report = {type = "value", sheet = "download", line = 3, col = 2, linIncrease=true}
            },
            [3] = {
                name =[==[Download_Stress_power]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS = 0.0, timeEndS = 8.0, nBlock=1,
                report = {type = "value", sheet ="download", line = 3, col = 3, linIncrease=true}
            },
        },
        vector = {
            clean = {file = "download_stress_Echolite.wav", ch = 1}, 
            unprocessed = {file = "download_stress_Echolite.wav", ch = 1},
            mic0 = {file = "download_stress_Echolite.wav", ch = 1},
            mic1 = {file = "download_stress_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        }
    },
    {
        caseName = [==[HW_Bypass]==],
        loop = 20,
        items = {
            [1] = {name = [==[HW_Bypass]==], module = "audio", submodule = [==[HW_Bypass]==]},
            [2] = {
                name = [==[HW_Bypass_pesq]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 12.5, nBlock=1,
                report = {type = "value", sheet = "HW_Bypass", line = 3, col = 2, linIncrease=true}
            },
            [3] = {
                name =[==[HW_Bypass_power]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS = 0.0, timeEndS = 12.5, nBlock=1,
                report = {type = "value", sheet ="HW_Bypass", line = 3, col = 3, linIncrease=true}
            },
        },
        vector = {
            clean = {file = "HWBypass_Echolite.wav", ch = 1}, 
            unprocessed = {file = "HWBypass_Echolite.wav", ch = 1},
            mic0 = {file = "HWBypass_Echolite.wav", ch = 1},
            mic1 = {file = "HWBypass_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        }
    },
    {
        caseName = [==[Enhance_Powerdown]==],
        loop = 6,
        items = {
            [1] = {name = [==[Enhance_Powerdown]==], module = "audio", submodule = [==[Enhance_Powerdown]==]},
            [2] = {
                name = [==[Enhance_Powerdown_pesq]==], module = "audio", submodule = [==[PESQ]==], unit = " ", timeStartS = 0.0, timeEndS = 12.5, nBlock=1,
                report = {type = "value", sheet = "Enhance_Powerdown", line = 3, col = 2, linIncrease=true}
            },
            [3] = {
                name =[==[Enhance_Powerdown_power]==], module = "audio", submodule = [==[level]==], unit = " ", timeStartS = 0.0, timeEndS = 12.5, nBlock=1,
                report = {type = "value", sheet ="Enhance_Powerdown", line = 3, col = 3, linIncrease=true}
            },
        },
        vector = {
            clean = {file = "HWBypass_Echolite.wav", ch = 1}, 
            unprocessed = {file = "HWBypass_Echolite.wav", ch = 1},
            mic0 = {file = "HWBypass_Echolite.wav", ch = 1},
            mic1 = {file = "HWBypass_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        }
    },
    {
        caseName = [==[Production_Trigger]==],
        items = {
            [1] = {name = [==[Production_Trigger]==], module = "audio", submodule = [==[Production_Trigger]==]},
        },       
        vector = nil
    },
    {
        caseName = [==[Watch_dog]==],
        items = {
            [1] = {name = [==[Watch_dog]==], module = "audio", submodule = [==[Watch_dog]==]},
        },       
        vector = {
            clean = {file = "HWBypass_Echolite.wav", ch = 1}, 
            unprocessed = {file = "HWBypass_Echolite.wav", ch = 1},
            mic0 = {file = "HWBypass_Echolite.wav", ch = 1},
            mic1 = {file = "HWBypass_Echolite.wav", ch = 2},
            bypassDelayCheck = true,
        }
    },
}

for i=1,#case do
    -- update polarPattern seglist
    local s = string.find(case[i].caseName, [==[Pattern]==])
    if s then
        local segList = case[i].vector.segList
        local degreeStep = case[i].vector.degreeStep
        for i=1, 360/degreeStep do segList[i] = (i-1)*degreeStep end
    end

    -- update enable list
    case[i].enable = false
    for j=1,#case[i].items do
        case[i].items[j].enable = regression.VEN_const.itemEnable[case[i].items[j].name]
        case[i].enable = case[i].enable or case[i].items[j].enable
    end
end

regression = regression or {}
regression.VEN_const = regression.VEN_const or {}
regression.VEN_const.caseEchoLite = case