
local const = {
    fsMode = {16000},
    --fsMode = {8000},
    micMode = {"twoMic"},
    readlength = 44,
}
local pointer = {
		0x0fff9f50,
		0x0fff9f54,
		0x0fff9f78,
        0x0fff9f7a, 
        0x0fff9f7c, 
        0x0fff9f80 		
}

function regression.test_pointerRead(appConfig, moduleSwitch)
	local year = os.date("%Y")
    local date = os.date("%m%d")
	local time = os.date("%H%M")
	local start_row = 1
	local start_col = 1 
    local pdmstr = "I2S"
    if moduleSwitch.connectionMode == 2 then pdmstr = "PDMCLK2048K" end 
    if moduleSwitch.connectionMode == 3 then pdmstr = "PDMCLK1024K" end 
    if moduleSwitch.connectionMode == 4 then pdmstr = "PDMCLK2000K" end 
    if moduleSwitch.connectionMode == 5 then pdmstr = "PDMCLK3250K" end
    if moduleSwitch.connectionMode == 6 then pdmstr = "PDMCLK1200K" end
    if moduleSwitch.connectionMode == 7 then pdmstr = "PDMCLK2400K" end 
	local fileout_path 
    for i = 1, #const.micMode do
        for j=1,#const.fsMode do
            if moduleSwitch.connectionMode == 1 then 
                pdmstr = "I2S_"..const.fsMode[j]
                fileout_path = lfs.create_folder(regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\")
            else
                fileout_path = lfs.create_folder(regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\") 
            end 
			GLOBAL.RESULT_EXCEL = fileout_path.."\\".."pointerRead_"..appConfig.name .."_"..const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
			regression.report_initial(regression.config.PATH.template.."pointerRead_report_template.xlsx")
			regression.config.PATH.DSP = regression.config.PATH.dspHome .. appConfig.name .. "\\"..pdmstr.."\\"
			-- load code ,Run DSP 
            if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
                intial_platform(regression.config.dut.name, const.fsMode[j], regression.config.platform.module, moduleSwitch.connectionMode,moduleSwitch.fastdowncode)
                iM501.init(regression.config.PATH.DSP, const.micMode[i], const.fsMode[j],moduleSwitch.fastdowncode)  
                local a,b=iM501.read_dram(0xfff9fc0,0x2)
                wait(0.1)
                local c,d=iM501.read_dram(0xfff9fc0,0x2)
                if d==b then 
                    print("DSP dead... can not do parameters read ")
                    io.read()
                end                
			end
			-- if DSP run ,begin paeameters read
			local excel_output = luacom.excel.create()
			local mybookout = luacom.excel.open_file(excel_output, GLOBAL.RESULT_EXCEL)
			for k = 1, #pointer do 
				local state, value=iM501.read_dram(pointer[k], 4)
				pointer[k] = string.format("%#x", pointer[k])
				luacom.excel.write_cells_value(mybookout, "sheet1", start_row + k, start_col, pointer[k]) 
				if( state == false ) then
					printc("Read address error...")
					value = "n/a"
				else
					value= string.format("%#x",value)
				end 
				luacom.excel.write_cells_value(mybookout, "sheet1", start_row + k, start_col+1, value) 
				for m =1, const.readlength/2 do 
					local state, data=iM501.read_dram(value+m*2-2, 2, interface)
					local readAddr = string.format("%#x",value+m*2-2)
					if( state == false ) then
						printc("Read address error...")
						data = "n/a"
					else
						data= string.format("%#x",data)
					end 
					luacom.excel.write_cells_value(mybookout, "sheet1", start_row + m, start_col+2*k, readAddr)
					luacom.excel.write_cells_value(mybookout, "sheet1", start_row + m, start_col+2*k+1, data)
				end	
			end
			luacom.excel.saveas(mybookout,GLOBAL.RESULT_EXCEL)
			luacom.excel.quit(excel_output) 
        end         
	end
	

end
