----Path Define
SCRIPT_PATH = NOAH_PATH.."script\\source\\"
TEMPLATE_PATH = NOAH_PATH.."Template\\"
CONFIG_PATH = NOAH_PATH.."Configuration\\"

-- From Old config
RECORD_PATH = NOAH_PATH
RECORD_FILE = "rectmp.wav"--default setting
RECORD_TIME  = 0     --**   --The time of one cycle recording. equel 0 means stop recording until press any key.
DSP_NAME = regression.config.dut.name
GUI_EN = false                  --**
DEBUG = 1                       --**
