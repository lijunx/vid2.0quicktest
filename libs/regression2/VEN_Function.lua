require "libs.regression2.VEN_Function_config"

require "libs.regression2.VEN_general_report"
require "libs.regression2.Clock_general_report"

local const = regression.VEN_const

local function test(appConfig, caseList, fs, recordList,fastdowncode,pdmstr)

    for i=1,#caseList do
        if caseList[i].enable then
            local loop = caseList[i].loop or 1
            for iLoop = 1,loop do
                path=regression.config.PATH.result..appConfig.name.."\\" ..pdmstr.."\\"---MODIFIED BYLIJUN 20180109
                regression.case_test(appConfig, caseList[i], fs, regression.config.PATH.source, path, recordList,fastdowncode)
            end
        end
    end
end

function regression.test_VEN_Function(appConfig, moduleSwitch)
    
    local pdmstr = "I2S"
    local fileout 
    if moduleSwitch.connectionMode == 2 then pdmstr = "PDMCLK2048K" end 
    if moduleSwitch.connectionMode == 3 then pdmstr = "PDMCLK1024K" end 
    if moduleSwitch.connectionMode == 4 then pdmstr = "PDMCLK2000K" end 
    if moduleSwitch.connectionMode == 5 then pdmstr = "PDMCLK3250K" end
    if moduleSwitch.connectionMode == 6 then pdmstr = "PDMCLK1200K" end
    if moduleSwitch.connectionMode == 7 then pdmstr = "PDMCLK2400K" end 
    regression.VEN_const.case = regression.VEN_const["case" .. appConfig.name]
    for i=1,#const.micMode do
 
            for j=1,#const.fsMode do
                print(notice.const.str.functionLineDivider)
                notice.message_lib("VEN", "********\t" .. const.micMode[i].." " .. const.fsMode[j] .."Hz Mode" .. " Start\t********", "Info", true, true, false)
                if moduleSwitch.connectionMode == 1 then 
                    pdmstr = "I2S_"..const.fsMode[j]
                end
                -- initial report from template
                local year = os.date("%Y")
                local date = os.date("%m%d")
                local time = os.date("%H%M")
                fileout = lfs.create_folder(NOAH_PATH .. regression.config.PATH.result..appConfig.name.."\\"..pdmstr.."\\")
                GLOBAL.RESULT_EXCEL = fileout.."\\".."report_VEN_"..appConfig.name .. "_"..pdmstr.."_"  .. const.micMode[i] .. "_" .. const.fsMode[j]/1000 .. "k_"..year..date.."_"..time..".xlsx"
                regression.report_initial(regression.config.PATH.template .. "VEN_" .. const.fsMode[j]/1000 .. "k_report_template.xlsx")

                -- code download
                regression.config.PATH.DSP = regression.config.PATH.dspHome .. appConfig.name .. "\\"..pdmstr.."\\"
                if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
                    intial_platform(regression.config.dut.name, const.fsMode[j], regression.config.platform.module, moduleSwitch.connectionMode,moduleSwitch.fastdowncode)
                    iM501.init(regression.config.PATH.DSP, const.micMode[i], const.fsMode[j],moduleSwitch.fastdowncode)
                   
                    --------------------------------------------------------------------------------------
                    local flag ,value
                    if appConfig.name ~= "VoiceControl" then
                        flag , value = iM501.read_dram(0xfff9f68,0x4)
                        regression.config.baseaddr= value
                        --regression.electrical.save_status_info()
                    end
                    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                    iM501.read_dram(0xfff9fc0)
                    wait(0.1)
                    iM501.read_dram(0xfff9fc0)
                    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                 
                -- io.read()
                --- assert(false,"")

                end
                regression.report_cover_page(appConfig.name, const.micMode[i], const.fsMode[j], regression.config.PATH.DSP)
              
                -- run test
                test(appConfig, const.case, const.fsMode[j], const.recordList,moduleSwitch.fastdowncode,pdmstr)
                notice.message_lib("VEN", "********\t" .. const.micMode[i].." " .. const.fsMode[j] .."Hz Mode" .. "   End\t********", "Info", true, true, false)
                print(notice.const.str.functionLineDivider.."\n")

            end

    end
end