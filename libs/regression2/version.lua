regression.script_version =
[==[
    *****************************
    V2.0.0 : Created these files
    Date: 2017.9.28
    Author: Grass Guan
    *****************************
    *****************************
    *****************************
]==]

regression.RELEASE_DATE = "2017.10"
regression.Version = "V2.0.0"
regression.DATE = "2017.09.28"

