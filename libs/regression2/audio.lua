regression = regression or {}

local audio = {}

local function wav_cut(wavIn, analysis)
    local param = {
        timeStartS = 0.0,
        timeEndS = Wave.get_timelast(wavIn)
    }
    if analysis then
        if analysis.flagFromEnd and nil ~= analysis.timeLastS then
            param.timeStartS = param.timeEndS - analysis.timeLastS
        else
            param.timeStartS = analysis.timeStartS or param.timeStartS
            param.timeEndS = analysis.timeEndS or param.timeEndS
        end
    end
    local wavOut = Wave.cut(wavIn, param.timeStartS*1000, param.timeEndS*1000)

    return wavOut
end

audio[ [==[delay]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    if "ms" == analysis.unit then return delayS*1000 end

    return delayS
end

audio[ [==[level]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local tmpWav = wav_cut(processed[1], analysis)
    if tmpWav then
        local power = Wave.power_rmdc(tmpWav)
        Wave.clearWavdata(tmpWav)
        collectgarbage("collect")
        
        return power
    else
        notice.message_lib("regression.audio.power", "Invalid waveProcessed", "Info", true, true, false)
        return nil
    end
end

audio[ [==[PESQ]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local adjustSource = Wave.peak_to_fullscale(source)
    local scoreArray = {}
    local scoreArray0 = {}
    local scoreArray1 = {}
    for j=1,#processed do
        if #processed== 1 then
             adjustunprocessed = Wave.peak_to_fullscale(unprocessed)
        else
             adjustunprocessed = Wave.peak_to_fullscale(unprocessed[j])
        end
        local adjustProcessed = Wave.peak_to_fullscale(processed[j])

        --timeStartS = 0.0, timeEndS = 32.0, nBlock=4
        local shiftS = (analysis.timeEndS - analysis.timeStartS)/analysis.nBlock
        scoreArray0[j] = 0
        scoreArray1[j] = 0
        local start = analysis.timeStartS
        for i=1,analysis.nBlock do
            tmpScore = wave.pesq(adjustSource, adjustProcessed, {{start, start+shiftS}, {start, start+shiftS}}, false)
            tmpScore1 = wave.pesq(adjustunprocessed, adjustProcessed, {{start, start+shiftS}, {start, start+shiftS}}, false)
            start = start + shiftS
            scoreArray0[j] = scoreArray0[j] + tmpScore
            scoreArray1[j] = scoreArray1[j] + tmpScore1
        end
        scoreArray0[j] = scoreArray0[j]/analysis.nBlock
        scoreArray1[j] = scoreArray1[j]/analysis.nBlock
        scoreArray0[j]=string.format("%.2f",scoreArray0[j])
        scoreArray1[j]=string.format("%.2f",scoreArray1[j])
        scoreArray[j] =scoreArray0[j].."/"..scoreArray1[j]
        Wave.clearWavdata(adjustProcessed)
        Wave.clearWavdata(adjustunprocessed)
        collectgarbage("collect")
    end
    Wave.clearWavdata(adjustSource)
    scoreArray0 = nil
    scoreArray1 = nil
    if 1==#processed then return scoreArray[1] end 
    return {scoreArray}
end

--audio[ [==[POLQA]==] ] = function(waveProcessed, waveUnprocessed, waveClean, analysis) --TBD
--    local method = "POLQA"
--    local alignProcessed, waveCleanAdjust
--    if nil == regression.flag.VoiceQuality_File then
--        alignProcessed = algorithm.align(waveProcessed, waveClean, analysis)
--        local peak =  Wave.peak(waveClean)
--        waveCleanAdjust = Wave.amplify(waveClean,  -peak)
--    end
--    local fs = Wave.get_samplerate(waveProcessed)
--    local param = regression.const[method].WB
--    if fs < 16000 then param = regression.const[method].NB end
--    local filename = ACQUA[method](alignProcessed, waveCleanAdjust, regression.config.PATH.ACQUA, param, regression.flag.VoiceQuality_File)
--    if alignProcessed then Wave.clearWavdata(alignProcessed) end
--    if waveCleanAdjust then Wave.clearWavdata(waveCleanAdjust) end
--
--    regression.flag.VoiceQuality_File = filename
--    return "POLQA ACQUA Batch Path: " .. regression.config.PATH.ACQUA
--end

audio[ [==[POLQA]==] ] = function(waveProcessed, waveUnprocessed, waveClean, analysis) --TBD
    --local method = "POLQA"
    --local alignProcessed, waveCleanAdjust
    --if nil == regression.flag.VoiceQuality_File then
    --    alignProcessed = algorithm.align(waveProcessed, waveClean, analysis)
    --    local peak =  Wave.peak(waveClean)
    --    waveCleanAdjust = Wave.amplify(waveClean,  -peak)
    --end
    --local fs = Wave.get_samplerate(waveProcessed)
    --local param = regression.const[method].WB
    --if fs < 16000 then param = regression.const[method].NB end
    --local filename = ACQUA[method](alignProcessed, waveCleanAdjust, regression.config.PATH.ACQUA, param, regression.flag.VoiceQuality_File)
    --if alignProcessed then Wave.clearWavdata(alignProcessed) end
    --if waveCleanAdjust then Wave.clearWavdata(waveCleanAdjust) end
--
    --regression.flag.VoiceQuality_File = filename
    --return "POLQA ACQUA Batch Path: " .. regression.config.PATH.ACQUA
    local POLQA_score
    local source_peak = Wave.peak(waveClean)
    local waveCleanAdjust = Wave.amplify(waveClean, -source_peak)
    local source_fs = Wave.get_samplerate(waveCleanAdjust)
    local fs = Wave.get_samplerate(waveProcessed[1])
    local waveCleanAdjust_resample = wave.resample(waveCleanAdjust, 48000)
    local processed_resample = wave.resample(waveProcessed[1], 48000)
    local re_fs = Wave.get_samplerate(processed_resample)
    local processed_delay = wave.check_delay(processed_resample, waveCleanAdjust_resample)/48000*1000
    local processedAdjust = wave.cut(processed_resample, processed_delay, -1)
    if fs<16000 then 
        POLQA_score = wave.polqa(waveCleanAdjust_resample, processedAdjust, 0)
    else
        POLQA_score = wave.polqa(waveCleanAdjust_resample, processedAdjust, 1)
    end
    Wave.clearWavdata(waveCleanAdjust)
    Wave.clearWavdata(waveProcessed)
    Wave.clearWavdata(processedAdjust)
    Wave.clearWavdata(waveCleanAdjust_resample)
    Wave.clearWavdata(processed_resample)
    collectgarbage("collect")
    return POLQA_score
end

audio[ [==[convergence_time]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local timeWinS = 0.005
    local thCoef = 0.02
    local noiseFloor = -70 --dB
    local noConvergenceGapeS = 0.4
    local timeS, startS, endS =  Wave.convergence_time(processed[1], timeWinS, thCoef, noiseFloor, noConvergenceGapeS)
    if "ms" == analysis.unit then return timeS*1000 end
    
    return timeS
end

audio[ [==[noise_suppression]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local processedCut = wav_cut(processed[1], analysis)
    local unprocessedCut = wav_cut(unprocessed, analysis)

    local powerProcessed = Wave.power_rmdc(processedCut)
    local powerUnprocessed = Wave.power_rmdc(unprocessedCut)

    Wave.clearWavdata(processedCut)
    Wave.clearWavdata(unprocessedCut)
    collectgarbage("collect")

   -- local ns = powerUnprocessed - powerProcessed
    local ns = powerProcessed
    return ns
end

audio[ [==[frequency_response]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local processedCut = wav_cut(processed[1], analysis)
    local unprocessedCut = wav_cut(unprocessed, analysis)
    local curveOut = {}
    
    local spectrumProcessed, freqPoint = Wave.spectrum(processedCut, analysis.FFTSize, analysis.win, analysis.overlap, analysis.R)
    local spectrumUnprocessed = Wave.spectrum(unprocessedCut, analysis.FFTSize, analysis.win, analysis.overlap, analysis.R)

    Wave.clearWavdata(processedCut)
    Wave.clearWavdata(unprocessedCut)
    collectgarbage("collect")

    local response = {}
    for i=1,#spectrumProcessed do
        response[i] = spectrumProcessed[i] - spectrumUnprocessed[i]
    end

    for i=#freqPoint,1,-1 do
        if freqPoint[i] < 78 then
            table.remove(freqPoint, i)
            table.remove(response, i)
        end
    end
    
    return {freqPoint, response}
end

audio[ [==[AEC]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    print(processed, unprocessed, source, analysis, delayS, fs)
    return "AEC TBD"
end

audio[ [==[DoubleTalk]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    print(processed, unprocessed, source, analysis, delayS, fs)
    return "DoubleTalk TBD"
end
----- AECref on off simulation test 
local loopTmp=1
local config={
    report = {type = "value", sheet = "AEC_onoff", line = 3, col = 2, linIncrease=true}
}
local config1={
    report = {type = "value", sheet = "AEC_onoff", line = 3, col = 3, linIncrease=true}
}
local config2={
    report = {type = "value", sheet = "AEC_onoff", line = 3, col = 4, linIncrease=true}
}
local config3={
    report = {type = "value", sheet = "AEC_onoff", line = 3, col = 5, linIncrease=true}
}
audio[ [==[AEC_onoff]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath)
    local loutpesq = 0
    local loutlevel = 0
    local aecrefpesq = 0
    local aecreflevel = 0
    local wave_data = wave.load("rectmp.wav")
    local wave_org =  wave.load("playtmp.wav") 
    local wave_array = wave.n_to_mono(wave_data)
    local wave_array_org = wave.n_to_mono(wave_org)
    local wave_new = wave.mono_to_n({processed[1],wave_array[4]},1)
    Wave.save(resultPath .. "AEC_onoff_" ..loopTmp .. "th.wav", wave_new)
    local timeLast = math.round(Wave.get_timelast(processed[1]))
    print(timeLast)
    if loopTmp%2 ==0 then
        loutpesq =  wave.pesq(wave_array_org[1], processed[1], {{0.2,timeLast-2}}, true)
        aecrefpesq = "/"
        loutpesq=string.format("%.2f",loutpesq)
        loutlevel =  wave.power(processed[1])
        aecreflevel = wave.power(wave_array[4])
        iM501.write_dram(0xFFFFF10,0x0)  -- turn on I2S CLock
    else
        --loutpesq = "/"
        loutpesq =  wave.pesq(wave_array_org[1], processed[1], {{0.2,timeLast-2}}, true)
        aecrefpesq =  wave.pesq(wave_array_org[5], wave_array[4], {{0.2,timeLast-2}}, true)
        loutlevel =  wave.power(processed[1])
        aecreflevel = wave.power(wave_array[4])
        aecrefpesq=string.format("%.2f",aecrefpesq)
        iM501.write_dram(0xFFFFF10,0x7)  -- turn off I2S CLock
    end
    loopTmp=loopTmp+1
    regression.report(config, loutpesq)
    regression.report(config1, loutlevel) 
    regression.report(config2, aecrefpesq)
    regression.report(config3, aecreflevel)
    wave.delete_wavedata(wave_data)
    wave.delete_wavedata(wave_new)
    wave.delete_wavedata(wave_org)
    for i = 1, 8 do 
        wave.delete_wavedata(wave_array[i])
        wave.delete_wavedata(wave_array_org[i])
    end
end


--local acquaFileList = {"clean", "unprocessed", "processed"}
audio[ [==[POLQAN]==] ] = function(processed, unprocessed, source, analysis)
    local POLQA_score = {}
    local source_peak = Wave.peak(source)
    local sourceAdjust = Wave.amplify(source, -source_peak)
    local sourceAdjust_resample = wave.resample(sourceAdjust, 48000)
    wave.save("POLQAN_source.wav", sourceAdjust_resample) 
    local shiftS = 8
    local nblock = analysis.nBlock
    for i=1, #processed do
        local start = analysis.timeStartS
        POLQA_score[i] = 0
        local fs = Wave.get_samplerate(processed[i])
        local processed_resample = wave.resample(processed[i], 48000)
        wave.save("POLQA_processed_nomatch"..i..".wav", processed_resample)
        --local processed_delay = wave.check_delay(processed_resample, sourceAdjust_resample)/48000*1000
        --print("delay"..i.."="..processed_delay)
        --local processedAdjust = wave.cut(processed_resample, processed_delay, -1)
        --wave.save("POLQA_processed_"..i..".wav", processedAdjust)
        print("No"..i.."POLQA cal")
        for j=1, nblock do
            if fs<16000 then
                tmpScore = wave.polqa(sourceAdjust_resample, processed_resample, {{start, start+shiftS}, {start, start+shiftS}}, 0)
            else
                tmpScore = wave.polqa(sourceAdjust_resample, processed_resample, {{start, start+shiftS}, {start, start+shiftS}}, 1)
            end
            start = start + shiftS
            POLQA_score[i] = POLQA_score[i] + tmpScore
        end
        POLQA_score[i] = POLQA_score[i]/nblock
        print("POLQA="..POLQA_score[i])
        Wave.clearWavdata(processed_resample)
        Wave.clearWavdata(processedAdjust)
        Wave.clearWavdata(processedAdjust_cut)
        collectgarbage("collect")
    end
    Wave.clearWavdata(sourceAdjust)
    Wave.clearWavdata(sourceAdjust_resample)
    Wave.clearWavdata(sourceAdjust_resample_cut)
    collectgarbage("collect")
    return {POLQA_score}
end

audio[ [==[3QUEST]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local acquaPath = NOAH_PATH .. regression.config.PATH.acquaHome

    -- file save
    local files = {clean={}, unprocessed={}, processed={}}

    -- source
    local sourceFile = acquaPath .. "3QUEST_Source.dat"
    Wave.save(sourceFile, source, 3)

    local tmpFileName = ""
    local segStartIdx = analysis.segStartIdx or 1
    for i=segStartIdx,#processed do
        -- source
        table.insert(files.clean, sourceFile)

        -- unprocessed
        tmpFileName = acquaPath .. "3QUEST_Unprocessed_" .. i .. ".dat"
        Wave.save(tmpFileName, unprocessed[i], 3)
        table.insert(files.unprocessed, tmpFileName)

        -- processed
        tmpFileName = acquaPath .. "3QUEST_Processed_" .. i .. ".dat"
        Wave.save(tmpFileName, processed[i], 3)
        table.insert(files.processed, tmpFileName)
    end
    
    local param = {
        module = [==[3QUEST]==],
        Version3QUEST = 1,
        bandwidth = 1
    }
    if fs > 8000 then param.bandwidth = 2 end
    local rawResult = batchcalctool.run(param, files, acquaPath)

    local SMOS, NMOS, GMOS = {}, {}, {}
    for i=1,#rawResult do
        SMOS[i], NMOS[i], GMOS[i] = 0, 0, 0
        local segNum = #rawResult[i].SMOS
        for j=1,segNum do
            SMOS[i] = SMOS[i] + rawResult[i].SMOS[j]
            NMOS[i] = NMOS[i] + rawResult[i].NMOS[j]
            GMOS[i] = GMOS[i] + rawResult[i].GMOS[j]
        end
        SMOS[i], NMOS[i], GMOS[i] = SMOS[i]/segNum, NMOS[i]/segNum, GMOS[i]/segNum
    end
    
    return {SMOS, NMOS, GMOS}
end

audio[ [==[SNRi]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    local snri, tnlr, nplr, dsn = {}, {}, {}, {}
    print("come here")
    
    local sourceCut = wav_cut(source, analysis)
    local segStartIdx = analysis.segStartIdx or 1
    for i=segStartIdx,#processed do
        local processedCut = wav_cut(processed[i], analysis)
        local unprocessedCut = wav_cut(unprocessed[i], analysis)
        snri[i], tnlr[i], nplr[i], dsn[i] = Wave.SNRi(sourceCut, unprocessedCut, processedCut)

        Wave.clearWavdata(processedCut)
        Wave.clearWavdata(unprocessedCut)
        collectgarbage("collect")
    end
    Wave.clearWavdata(sourceCut)
    collectgarbage("collect")
    
    return {snri, tnlr, nplr, dsn}
end

audio[ [==[polarPattern]==] ] = function(processed, unprocessed, source, analysis, delayS, fs)
    --printc("audio.polarPattern start")
    local female, male = {}, {}
    local femaleBase, maleBase = nil, nil

    local midSample = math.floor(Wave.get_samplenum(source)/2)
    local segStartIdx = analysis.segStartIdx or 1
    for i=segStartIdx,#processed do
        local tmpWav = Wave.cut(processed[i], 1, midSample, 2)
        female[i] = Wave.power_rmdc(tmpWav)
        femaleBase = femaleBase or female[i]
        female[i] = female[i] - femaleBase
        Wave.clearWavdata(tmpWav)
        collectgarbage("collect")
        
        tmpWav = Wave.cut(processed[i], midSample, -1, 2)
        male[i] = Wave.power_rmdc(tmpWav)
        maleBase = maleBase or male[i]
        male[i] = male[i] - maleBase
        Wave.clearWavdata(tmpWav)
        collectgarbage("collect")  
    end
        print("audio.polarPattern end")                     
    return {female, male}
end
local loopTmp=1
local P1={}
local N1={}
local config={
         report = {type = "value", sheet ="stress_test", line = 3, col = 4, linIncrease=true}
}
local config1={
    report = {type = "value", sheet ="stress_test", line = 3, col = 5, linIncrease=true}
}
audio[ [==[VEN_Stress]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath,fastdowncode)

    if fastdowncode == 1 then 
        iM501.enter_PSM(2,12000)
    elseif fastdowncode == 2 then 
        iM501.enter_PSM(1,100)
    elseif fastdowncode == 3 then 
        iM501.enter_PSM(1,100)
    elseif fastdowncode == 4 then 
        iM501.enter_PSM(1,100)
    end
    wait(1)
    local p1=regression.electrical.Current()
    printc(loopTmp.."th".."PSM current is.."..p1)
    P1=p1
    wait(1)
    if fastdowncode == 1 then 
        iM501.enter_NORMAL(2,12000)
    elseif fastdowncode == 2 then 
        iM501.enter_NORMAL(1,100)
    elseif fastdowncode == 3 then 
        iM501.enter_NORMAL(1,100)
    elseif fastdowncode == 4 then 
        iM501.enter_NORMAL(1,100)
    end
    wait(1)
    local n1=regression.electrical.Current()
    printc(loopTmp.."th".."NM current is.."..n1)
    N1=n1
    wait(1)
    regression.report(config, P1)
    regression.report(config1, N1)
    --analysis.loopTmp = analysis.loopTmp or 1
   Wave.save(resultPath .. "VEN_Stress_" ..loopTmp .. ".wav", processed[1])
   loopTmp=loopTmp+1
        
end

local loopTmp=1
audio[ [==[Mode_switch]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath)
    Wave.save(resultPath .. "Mode_switch_" ..loopTmp .. ".wav", processed[1])
    loopTmp=loopTmp+1
    if loopTmp%2 ==0 then
        iM501.write_message(0xc,0x8)
    else
        iM501.write_message(0xc,0x10)
    end
end

local loopTmp=1
local N2={}
local config2={
    report = {type = "value", sheet ="download", line = 3, col = 4, linIncrease=true}
}
audio[ [==[Download_Stress]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath,fastdowncode)

    print("begin download stress test......")

    local n2=regression.electrical.Current()
    printc(loopTmp.."th".."NM current is.."..n2)
    N2=n2
    iM501.init(regression.config.PATH.DSP, "twoMic", 16000,fastdowncode)
    --iM501.write_dram(0x0fffbf70, 0x0)
    --iM501.write_dram(0x0fffbf80, 0x0)
   -- iM501.write_dram(0x0fffbf82, 0x3)
    iM501.read_dram(0xFFF9FC0,0x2)
    wait(0.1)
    iM501.read_dram(0xFFF9FC0,0x2)
    wait(1)
    local a,b=iM501.read_dram(0xFFF9FC0,0x2)
     wait(0.1)
    local c,d=iM501.read_dram(0xFFF9FC0,0x2)
    if d==b then 
        print("loopTmp is ..."..loopTmp)
        assert(false,"")
    end
    regression.report(config2, N2)
    --analysis.loopTmp = analysis.loopTmp or 1
   Wave.save(resultPath .. "download_Stress_" ..loopTmp .. ".wav", processed[1])
   loopTmp=loopTmp+1
        
end
local loopTmp=1
local Cuur={}
local config={
         report = {type = "value", sheet ="HW_Bypass", line = 3, col = 4, linIncrease=true}
}

audio[ [==[HW_Bypass]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath,fastdowncode)
   
    Cuur =regression.electrical.Current()
    printc(loopTmp.."th".." current is.."..Cuur)
    regression.report(config, Cuur)
    
    if loopTmp%2 ~=0 then

        if fastdowncode == 1 then 
            iM501.write_reg(0x1,0xd,1,2)
        elseif fastdowncode == 2 then 
            iM501.write_reg(0x1,0xd,1,1)
        elseif fastdowncode == 3 then 
            iM501.write_reg(0x1,0xd,1,1)
        elseif fastdowncode == 4 then 
            iM501.write_reg(0x1,0xd,1,1)
        end
        wait(1)
    else
        if fastdowncode == 1 then 
            iM501.write_reg(0x1,0x5,1,2)
        elseif fastdowncode == 2 then 
            iM501.write_reg(0x1,0x5,1,1)
        elseif fastdowncode == 3 then 
            iM501.write_reg(0x1,0x5,1,1)
        elseif fastdowncode == 4 then 
            iM501.write_reg(0x1,0x5,1,1)
        end
        wait(1)
    end
    --analysis.loopTmp = analysis.loopTmp or 1
    Wave.save(resultPath .. "HWBypass_NM_switch" ..loopTmp .. ".wav", processed[1])
    loopTmp=loopTmp+1
       
end
local loopTmp=1
local HW={}
local PD={}
local config={
         report = {type = "value", sheet ="Enhance_Powerdown", line = 3, col = 4, linIncrease=true}
}
local config1={
    report = {type = "value", sheet ="Enhance_Powerdown", line = 3, col = 5, linIncrease=true}
}

audio[ [==[Enhance_Powerdown]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath,fastdowncode)

    if fastdowncode == 1 then 
        iM501.write_reg(0x1,0xd,1,2)
    elseif fastdowncode == 2 then 
        iM501.write_reg(0x1,0xd,1,1)
    elseif fastdowncode == 3 then 
        iM501.write_reg(0x1,0xd,1,1)
    elseif fastdowncode == 4 then 
        iM501.write_reg(0x1,0xd,1,1)
    end
    wait(1)
    HW =regression.electrical.Current()
    printc(loopTmp.."th".."HW bypass mode current is.."..HW)
    wait(0.5)
    FM36.disable_pdmclk()
    wait(1)
    PD =regression.electrical.Current()
    printc(loopTmp.."th".."Power down mode current is.."..PD)
    FM36.enable_pdmclk()
    wait(1.5)
    regression.report(config, HW)
    regression.report(config1, PD)

   
    --analysis.loopTmp = analysis.loopTmp or 1
    Wave.save(resultPath .. "HW_Bypass_" ..loopTmp .. ".wav", processed[1])
    loopTmp=loopTmp+1
       
end
audio[ [==[Watch_dog]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath)
    --generate exception   
    local a,b=iM501.read_dram(0xFFF9FC0,0x2)
    wait(0.1)
    local c,d=iM501.read_dram(0xFFF9FC0,0x2)
    if d==b then 
       print("Restart DSP  ...")
       io.read()
       iM501.init(regression.config.PATH.DSP, "twoMic", 16000,fastdowncode)
    end
    start_play_record()
    wait(3)
    iM501.write_dram(0xFFFFF7A,0x800)
    wait(1)
    local readTable = {
        {0x0FFF9FC0, 0x2},
        {0x0FFF9FC0, 0x2},
        {0x0FFFBFF8, 0x2},
        {0x0FFFBFF9, 0x2},
        {0x0FFFBFFA, 0x2},
        {0x0FFFBFFB, 0x2},
        {0x0FFFBFFF, 0x2}
    }
    local result = {{},{},{}}
    for i=1,#readTable do
        local flagRead, value = iM501.read_dram(readTable[i][1], readTable[i][2])
        result[1][i] = string.format("0x%X", readTable[i][1])
        result[2][i] = string.format("0x%X", readTable[i][2])
        result[3][i] = string.format("0x%X", value)
    end

    local config = {
        report = {type = "matrix", sheet = "Watch_dog", lineStart = 2, col = {1,2,3}}
    }
    regression.report(config, result)
    wait(10)
    stop_play_record()
    local wave_array=wave.n_to_mono(wave.load("rectmp.wav"))
    Wave.save(resultPath .. "Watch_dogIRQrec"..".wav",wave_array[7])
    for i = 1, 8 do 
        wave.delete_wavedata(wave_array[i])
    end   
    
end
audio[ [==[Production_Trigger]==] ] =function(processed, unprocessed, source, analysis, delayS, fs, resultPath,fastdowncode)
    local vrFrames = 15  --ms    except Google VRFrame is 10ms 
    local cycles  = 10 
    local duration = {100,200,300,400,500}
    if not GLOBAL.DEBUG_V2_RECORD_OFFLINE then 
        for i = 1,#duration do 
            local  t = duration[i]*vrFrames*(cycles+1)    -- ms
            iM501.write_dram(0x0fffbff0, duration[i])  -- 100, 200... 500*VR Frames force SD trigger ,VR Frames can be 10ms/15ms
            iM501.write_dram(0x0fffbff6, 0x8003)  --0x8003 means both PSM NM  SD force trigger
            if fastdowncode == 1 then 
                iM501.write_reg(0x1,0x35,1,2)    --START_PRODUCTION_TRIGGER_TEST// HostReg = 0x35 
            else
                iM501.write_reg(0x1,0x35,1,1)
            end
            start_play_record()  -- see SD trigger
            wait(t/1000)
            stop_play_record()
            local wave_data = wave.load("rectmp.wav")
            local wave_array=wave.n_to_mono(wave_data)
            Wave.clearWavdata(wave_data)
            Wave.save(resultPath .. "Production_Trigger_NOR_"..duration[i].."vrFrame_rec.wav",wave_array[7])
            Wave.clearWavdata(wave_array)
            if fastdowncode == 1 then 
                iM501.enter_PSM(2,12000)
            else
                iM501.enter_PSM(1,100)
            end
            start_play_record()  -- see SD trigger
            wait(t/1000)
            stop_play_record()
            local wave_data = wave.load("rectmp.wav")
            local wave_array=wave.n_to_mono(wave_data)
            Wave.clearWavdata(wave_data)
            Wave.save(resultPath .. "Production_Trigger_PSM_"..duration[i].."vrFrame_rec.wav",wave_array[7])
            Wave.clearWavdata(wave_array)
             -- returen to Normal 
            if fastdowncode == 1 then 
                iM501.enter_NORMAL(2,12000)
            else 
                iM501.enter_NORMAL(1,100)
            end
            local a,b=iM501.read_dram(0xfff9fc0,0x2)
            wait(0.1)
            local c,d=iM501.read_dram(0xfff9fc0,0x2)
            if d==b then 
                print("DSP dead...")
                io.read()
            end
            --End_PRODUCTION_TRIGGER_TEST// HostReg = 0x35 ，and 0xFFFBFF6  BIT[15]=1
            iM501.write_reg(0x1,0x39,1,2)
            iM501.write_dram(0x0fffbff6, 0x8000)  
        end
    end
    -- generate report 
    local excel_output = luacom.excel.create()
    local mybook = luacom.excel.open_file(excel_output, GLOBAL.RESULT_EXCEL) 
    for j = 1,#duration do
        local setdelay  = duration[j]*vrFrames/1000
        luacom.excel.write_cells_value(mybook, "Production Trigger", 2, 4*j-3, duration[j].."_vrFrame".."("..setdelay.."s)")  
        local interval = {} 
        local width = {}
        local intervalpsm = {}  
        local widthpsm = {}  
        local filename = "Production_Trigger_NOR_"..duration[j].."vrFrame_rec.wav"
        
        local wave_property = Wave.get_wave_property (resultPath, filename)   ----an array contains wave properties
        local wave_data= wave.load(resultPath..filename)
        --local min_time = 1/wave_property.sample_rate   ---1 sample time
        local pos_s= wave.silence_len(wave_data, -80)     -- start postion
        local pos_e= wave.tail_silence_len(wave_data, -80)  -- end postion 
        local new_data = wave.cut(wave_data,pos_s,wave_property.wave_length-pos_e)
        Wave.clearWavdata(wave_data)
        local m =1 
        while(1) do
            local totallength = wave.get_len(new_data) 
            pos_s = wave.unsilence_len(new_data,-30)  --- first trigger width 
            if pos_s < totallength then 
                 new_data = wave.cut(new_data,pos_s,totallength)  -- remove first trigger width 
                 pos_e= wave.silence_len(new_data, -80)           -- silence before next trigger
                 new_data = wave.cut(new_data,pos_e,totallength-pos_s)
                 width[m] = pos_s
                 interval[m]= (pos_s+pos_e)/1000    --  change ms to s   trigger 1 and trigger 2 duration 
                 luacom.excel.write_cells_value(mybook,"Production Trigger", m+4, 4*j-3, width[m])
                 luacom.excel.write_cells_value(mybook,"Production Trigger", m+4, 4*j-2, interval[m])
                 --print(interval[m])
                 --io.read()
                 m = m+1
 
            end 
            if  pos_s >= totallength  then break end
        end
        Wave.clearWavdata(new_data)
 
        local filename = "Production_Trigger_PSM_"..duration[j].."vrFrame_rec.wav"
        local wave_property = Wave.get_wave_property (resultPath, filename)   ----an array contains wave properties
        local wave_data= wave.load(resultPath..filename)
        --local min_time = 1/wave_property.sample_rate   ---1 sample time
        local pos_s= wave.silence_len(wave_data, -80)     -- start postion
        local pos_e= wave.tail_silence_len(wave_data, -80)  -- end postion 
        local new_data = wave.cut(wave_data,pos_s,wave_property.wave_length-pos_e)
        Wave.clearWavdata(wave_data)
        local n =1 
        while(1) do
            local totallength = wave.get_len(new_data) 
            pos_s = wave.unsilence_len(new_data,-30)  --- first trigger width 
            if pos_s < totallength then 
                new_data = wave.cut(new_data,pos_s,totallength)  -- remove first trigger width 
                pos_e= wave.silence_len(new_data, -80)           -- silence before next trigger
                new_data = wave.cut(new_data,pos_e,totallength-pos_s)
                widthpsm[n] = pos_s
                intervalpsm[n]= (pos_s+pos_e)/1000    --  change ms to s   trigger 1 and trigger 2 duration 
                luacom.excel.write_cells_value(mybook,"Production Trigger", n+4, 4*j-1, widthpsm[n])
                luacom.excel.write_cells_value(mybook,"Production Trigger", n+4, 4*j, intervalpsm[n])
                --print(interval[n])
                --io.read()
                n = n+1
            end 
  
            if  pos_s >= totallength  then break end
        end
        Wave.clearWavdata(new_data)
        --print(#intervalpsm)
    end 
    luacom.excel.saveas(mybook,GLOBAL.RESULT_EXCEL)
    luacom.excel.quit(excel_output)  
    
end

regression.audio = regression.audio or audio