regression = regression or {}

local report = {}

local excel, mybook = nil, nil
local function excel_open(sheetName)
    if nil == excel then excel = luacom.excel.create() end
    if nil == mybook then mybook = luacom.excel.open_file(excel, GLOBAL.RESULT_EXCEL) end
    local mysheet = mybook.Sheets(sheetName)
    
    return mysheet
end

local function excel_exit()
    mybook.Sheets(1):activate()
    luacom.excel.saveas(mybook, GLOBAL.RESULT_EXCEL)
    luacom.excel.quit(excel)
    excel = nil
    mybook = nil
end

function report.value(config, result)
    local mysheet = excel_open(config.sheet)
    
    config.linAdd = config.linAdd or 0
    config.colAdd = config.colAdd or 0
    mysheet.Cells(config.line+config.linAdd, config.col+config.colAdd).Value2 = result
    excel_exit()

    if config.linIncrease then config.linAdd = config.linAdd + 1 end
    if config.colIncrease then config.colAdd = config.colAdd + 1 end

    return
end

function report.matrix(config, result)
    local mysheet = excel_open(config.sheet)
    if nil ~= config.lineStart then
        for i=1,#result[1] do
            for j,col in pairs(config.col) do
                mysheet.Cells(i-1+config.lineStart, col).Value2 = result[j][i]
            end
        end
    end
    
    if nil ~= config.colStart then
        for i=1,#result[1] do
            for j, line in pairs(config.line) do
                mysheet.Cells(line, i-1+config.colStart).Value2 = result[j][i]
            end
        end
    end
    excel_exit()

    return
end

function report.multi_vector(config, result)
    for i=1,#result do
        local mysheet = excel_open(config[i].sheet)
        if nil ~= config[i].lineStart then
            for j=1,#result[i] do
                mysheet.Cells(j-1+config[i].lineStart, config[i].col).Value2 = result[i][j]
            end
        end
        
        if nil ~= config[i].colStart then
            for j=1,#result[i] do
                mysheet.Cells(config[i].line, j-1+config[i].colStart).Value2 = result[i][j]
            end
        end
    end
    excel_exit()

    return
end

function regression.report_simple(reportFile, str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(reportFile,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end

function regression.report_initial(templateFile, resultFile)
    if nil  == resultFile then
        resultFile = GLOBAL.RESULT_EXCEL
    else
        GLOBAL.RESULT_EXCEL = resultFile
    end
    lfs.copy(templateFile, resultFile, false)
end

function regression.report(config, result)
    if config.report and report[config.report.type] then
        report[config.report.type](config.report, result)
    end
end
---- VOS VEN xAA xSD SD version read for VID 2.0 
function ascii_mask( char_data )
    if char_data >=0x20 and char_data<0x7F then 
        return char_data
    else             
        return 0x20 --space
    end
end

function fetch_version (p_ver_address, length)    
     
    local state, addr=iM501.read_dram(p_ver_address, 4, interface) 
    if( state == false ) then
        printc("Read fw version error...")
        return 'N/A'
    end 
    ver = ''
    for i =1, length/2 do 
        local state, data=iM501.read_dram(addr+i*2-2, 2, interface) 
        if( state == false ) then
            printc("Read fw version error...")
            return 'N/A'
        end
        local low, high = string.char(ascii_mask(data%256)), string.char(ascii_mask(data/256%256))              
        ver =  ver..low..high
    end 
    return ver

end
-- update  report cover for VID 2.x
--system pointer:
    -- VOS:0x0FFF9F78
    -- VEN:0x0FFF9F7C
    -- xAA:0x0FFF9F80
    -- xSD:0x0FFF9F84
    -- VR or SD :0x0FFF9F88
function regression.report_cover_page(application, micNum, fs, codePath,keyWord)
        local mysheet = excel_open("Cover")
        if keyWord == nil then 
            keyWord = "TBD"
        end   
        -- iM501
        mysheet.Cells(20, 3).Value2 = application
        mysheet.Cells(20, 4).Value2 = micNum
        mysheet.Cells(20, 5).Value2 = fs
        mysheet.Cells(20, 6).Value2 = keyWord
    
        if not GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then
            -- version read
            local baseAddress={
                0x0FFF9F78,
                0x0FFF9F7C, 
                0x0FFF9F80, 
                0x0FFF9F84, 
                0x0FFF9F88 
            }
            local startline = 28
            for i = 1,#baseAddress do
                local  version = fetch_version(baseAddress[i],44)
                mysheet.Cells(startline+i, 3).Value2 = version
            end
            
            -- platform
            local _, UIF_version = UIF.get_board_ver()
            local startPos, endPos = string.find(UIF_version, "%[")
            startCol = 2
            while startPos do
                local s,e = string.find(UIF_version, "%]")
                mysheet.Cells(25, startCol).Value2 = string.sub(UIF_version, startPos+1, s-1)
                UIF_version = string.sub(UIF_version, e+1)
                startPos, endPos = string.find(UIF_version, "%[")
                startCol = startCol + 1
            end
    
            -- general information
            mysheet.Cells(36, 3).Value2 = regression.config.tester
            mysheet.Cells(37, 3).Value2 = os.date("%Y-%m-%d %H:%M:%S")
        end
        excel_exit()
end