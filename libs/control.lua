control = control or {}

--**************************************************--
--Function Name: stop
--Description: stop script, use to debug
--Return Value: no
--Parameter: type
--  type: default value is 1, when type value is one, stop direct,
--        is type value is 0, hold running, then put any key to run continuation.
--Usage: control.stop()
--**************************************************--
function control.stop(type)
    local type = type or 1
    if type == 1 then
        assert(false, "stop")
    elseif type == 0 then
        os.execute("pause")
    end
end

--**************************************************--
--Function Name: sleep
--Description: delay
--Return Value: no
--Parameter: delay_time: in second
--Usage: sleep(10)
--**************************************************--
function control.sleep(t)
    if t == 0 then
        os.execute("pause")
    else
        printc("wait time "..t.."s")
        wait(t)
    end
end
--**************************************************--
function control.wait(t)
    if t == 0 then
        os.execute("pause")
    else
        printc("wait time "..t.."s")
        local clock=os.clock()
        while os.difftime (os.clock(),clock)<t do 
        end 
    end
end