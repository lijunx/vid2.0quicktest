batchcalctool = batchcalctool or {}
local const = {}

local CalcMode = {
    [==[3QUEST]==],
    [==[G160]==],
    [==[AutoDT]==],
    [==[3QUEST]==],
    [==[TOSQA]==],
    [==[DT]==],
    [==[POLQA]==],
}

-- const for ACQUA batch tool setting
const[ [==[3QUEST]==] ] = {
    NB = {
        CalcMode = [==[3QUEST]==],
        BandWidthMode = 5, -- 5-Narrowband Retrained (TS)   (ETSI TS 103 106)
        AdvancedXCorr = false,  -- true or false
        FilterIRSRCV = false,  -- true or false, only for NB
        SkipVariableDelay = true,  -- true or false
        TimeRange = {
            MODE = 2,
            Start = 39.700, -- ms, ACQUA.batchDouble2Hex
            Duration = 4.000, -- ms, ACQUA.batchDouble2Hex
            MaxNbrOfRanges = 16
            }
    },
    WB = {
        CalcMode = [==[3QUEST]==],
        BandWidthMode = 4, -- 4-Wideband Retrained (TS)     (ETSI TS 103 106)
        AdvancedXCorr = false,  -- true or false
        FilterIRSRCV = false,  -- true or false, only for NB
        SkipVariableDelay = true,  -- true or false
        TimeRange = {
            MODE = 2,
            Start = 39.700, -- s, ACQUA.batchDouble2Hex
            Duration = 4.000, -- s, ACQUA.batchDouble2Hex
            MaxNbrOfRanges = 16
            }
    }
}

const[ [==[POLQA]==] ] = {
    NB = {
        CalcMode = [==[POLQA]==],
        PolqaVersion = 1,
            -- 0-V1.10
            -- 1-V2.40
        Bandwidthmode = 0,
            -- 0-Narrowband el
            -- 1-Narrowband ac
            -- 2-Wideband
        UseFixedASLs = true, -- TRUE/FALSE: Use fixed ASL(req. for acoustic meas.)
        HighAccuracyMode = false, -- TRUE/FALSE: Use High Accuracy Mode
        HighAccuracyModeMT = false, -- TRUE/FALSE: Multithreaded High Accuracy Mode
        ForceCommonSamplingRate = false, -- no setting at ACQUA UI
        TimeRange = {
            MODE = 0
            }
    },
    WB = {
        CalcMode = [==[POLQA]==],
        PolqaVersion = 1,
            -- 0-V1.10
            -- 1-V2.40
        Bandwidthmode = 2,
            -- 0-Narrowband el
            -- 1-Narrowband ac
            -- 2-Wideband
        UseFixedASLs = true, -- TRUE/FALSE: Use fixed ASL(req. for acoustic meas.)
        HighAccuracyMode = false, -- TRUE/FALSE: Use High Accuracy Mode
        HighAccuracyModeMT = false, -- TRUE/FALSE: Multithreaded High Accuracy Mode
        ForceCommonSamplingRate = false, -- no setting at ACQUA UI
        TimeRange = {
            MODE = 0
            }
    }
}

const[ [==[TOSQA]==] ] = {
    NB = {
        CalcMode = [==[TOSQA]==],
        VarDelayRange = [==[<1000ms]==], -- Search var. delay
        FixedDelayRange = [==[<1000ms]==], -- Search fixed delay
        FixedDelayInMs = 0, -- Fixed delay, must be a interger
        UseTosqa2001 = true, -- TRUE/FALSE
        Tosqa2001Mode = 0,
        TimeRange = {
            MODE = 0
            }
    },
    WB = {
        CalcMode = [==[TOSQA]==],
        VarDelayRange = [==[<1000ms]==], -- Search var. delay
        FixedDelayRange = [==[<1000ms]==], -- Search fixed delay
        FixedDelayInMs = 0, -- Fixed delay, must be a interger
        UseTosqa2001 = true, -- TRUE/FALSE
        Tosqa2001Mode = 1,
        TimeRange = {
            MODE = 0
            }
    }
}

const[ [==[G160]==] ] = {
    CalcMode = [==[G160]==],
    FilterMode = 0, -- Filter Mode for unprocessed reference
    SkipTimeAlignment = false,  -- true or false, always use false
    AutoFilterCorrection = false, -- TRUE/FALSE : Auto Filter Correction
    SamplingRate = 99, --: Select sampling rate always use 99
    TimeRange = {
        MODE = 0
        }
}

const[ [==[AutoDT]==] ] = {
    CalcMode = [==[AutoDT]==],
    CalculationMode = 1,
    UseFilterForST = false, -- TRUE/FALSE
    UseSourceFile = false, -- TRUE/FALSE
    TimeRange = {
        MODE = 0
        }
}
batchcalctool.const = batchcalctool.const or const
