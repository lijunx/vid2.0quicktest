local get_param = {}

get_param[ [==[3QUEST]==] ] = function(param)
    local _3QUEST_VERSION = {
        [1] = {name = [==[EG 202 396-3 v1.3.1]==], value = 0},
        [2] = {name = [==[TS 103 106]==], value = 1},
        [3] = {name = [==[EG 202 396-3 v1.4.1]==], value = 2}
    }
    
    local acqua_param = {
        CalcMode = [==[3QUEST]==],
        --ResultValues = "0\n1\n2\n3\n4\n9\n21\n36",
        Version3QUEST = param.Version3QUEST,
        SkipVariableDelay = true,
        AdvancedXCorr = false,
        timeRange = {
            startS = 39.700,
            durationS = 4.000,
            nBlock = 16
        }
    }
    if 1 == param.bandwidth then --NB
        acqua_param.BandWidthMode = 0
    else --WB
        acqua_param.BandWidthMode = 1
    end
    if 1 == param.bandwidth and 0 == param.Version3QUEST then --NB
        acqua_param.FilterIRSRCV = true
    else --WB
        acqua_param.FilterIRSRCV = false
    end

    return acqua_param
end

function batchcalctool.param_gen(param)
    local acqua_param = get_param[param.module](param)
    
    return acqua_param
end