local function get_bool_str(value)
    if value then
        return "TRUE"
    else
        return "FALSE"
    end
end

--[[
	timeRange={
		durationS=38.99996,
		nBlock=1,
		startS=30,
			}
--]]
local function timeRange_gen(param)
    local str = ""
    str = str .. [==[    <TimeRange>]==] .. "\n"
    str = str .. [==[      <Start>]==] .. ACQUA.batchDouble2Hex(param.startS) .. [==[</Start>]==] .. "\n"
    str = str .. [==[      <Duration>]==] .. ACQUA.batchDouble2Hex(param.durationS) .. [==[</Duration>]==] .. "\n"
    str = str .. [==[      <MODE>2</MODE>]==] .. "\n"
    str = str .. [==[      <FromEnd>FALSE</FromEnd>]==] .. "\n"
    str = str .. [==[      <MaxNbrOfRanges>]==] .. param.nBlock .. [==[</MaxNbrOfRanges>]==] .. "\n"
    str = str .. [==[    </TimeRange>]==] .. "\n"

    return str
end

local function general_gen(param, CalcMode)
    local str = ""
    str = str .. [==[  <General>]==] .. "\n"
    str = str .. [==[    <ConfigFileVersion>1</ConfigFileVersion>]==] .. "\n"
    str = str .. [==[    <UseAllSignalTypes>FALSE</UseAllSignalTypes>]==] .. "\n"
    str = str .. [==[    <CalcMode>]==] .. CalcMode .. [==[</CalcMode>]==] .. "\n"
    str = str .. [==[    <StopOnException>FALSE</StopOnException>]==] .. "\n"
    str = str .. [==[    <CreateHeaderInfo>TRUE</CreateHeaderInfo>]==] .. "\n"
    str = str .. timeRange_gen(param)
    str = str .. [==[    <ChannelCleanRef>1</ChannelCleanRef>]==] .. "\n"
    str = str .. [==[    <ChannelUnprocessed>1</ChannelUnprocessed>]==] .. "\n"
    str = str .. [==[    <ChannelProcessed>1</ChannelProcessed>]==] .. "\n"
    str = str .. [==[  </General>]==] .. "\n"

    return str
end

local func_gen = {}

--[[
	3QUEST={
		AdvancedXCorr=false,
		BandWidthMode=1,
		CalcMode=[==[3QUEST]==],
		FilterIRSRCV=false,
		SkipVariableDelay=true,
		Version3QUEST=0,
    }
--]]
func_gen[ [==[3QUEST]==] ] = function(param)
    local str = ""
    str = str .. [==[    <3QUEST>]==] .. "\n"
    str = str .. [==[      <ResultValues>]==] .. "0\n1\n2\n3\n4\n9\n21\n36\n" .. [==[</ResultValues>]==] .. "\n"
    str = str .. [==[      <Version3QUEST>]==] .. param.Version3QUEST .. [==[</Version3QUEST>]==] .. "\n"
    str = str .. [==[      <BandwidthMode>]==] .. param.BandWidthMode .. [==[</BandwidthMode>]==] .. "\n"
    str = str .. [==[      <FilterIRSRCV>]==] .. get_bool_str(param.FilterIRSRCV) .. [==[</FilterIRSRCV>]==] .. "\n"
    str = str .. [==[      <SkipVariableDelay>]==] .. get_bool_str(param.SkipVariableDelay) .. [==[</SkipVariableDelay>]==] .. "\n"
    str = str .. [==[      <AdvancedXCorr>]==] .. get_bool_str(param.AdvancedXCorr) .. [==[</AdvancedXCorr>]==] .. "\n"
    str = str .. [==[    </3QUEST>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[G160]==],
        FilterMode = 1, -- Filter Mode for unprocessed reference
            -- 0-No filter
            -- 1-Maunal
            -- 2-Automatic(by Filename)
        ManualFilter = nil, -- hdf file name with path, FilterMode = 1
        SkipTimeAlignment = false,  -- true or false, always use false
        AutoLevelCorrection = false,
        AutoFilterCorrection = true, -- TRUE/FALSE : Auto Filter Correction
        SamplingRate = 1, --: Select sampling rate always use 99
            -- 0-8000
            -- 1-16000
            -- 2-48000
            -- 99-Use Processed
        timeRange = {
            startS = {},
            durationS = 0,
            nBlock = 0
        }
    }
    Channels:
        1 -- Processed
        2 -- Unprocessed
        3 -- Clean Speech
--]]
func_gen[ [==[G160]==] ] = function(param)
    local str = ""
    str = str .. [==[    <G160>]==] .. "\n"
    str = str .. [==[      <AutoLevelCorrection>]==] .. get_bool_str(param.AutoLevelCorrection) .. [==[</AutoLevelCorrection>]==] .. "\n"
    str = str .. [==[      <ResultValues>]==] .. "0\n7\n8\n9\n</ResultValues>\n"
    
    str = str .. [==[      <FilterMode>]==] .. param.FilterMode .. [==[</FilterMode>]==] .. "\n"
    if 1 == param.FilterMode then
        str = str .. [==[      <ManualFilter>]==] .. param.ManualFilter .. [==[</ManualFilter>]==] .. "\n"
    end
    if 2 == param.FilterMode then
        str = str .. [==[      <AutoFilterDirectory>]==] .. param.AutoFilterDirectory .. [==[</AutoFilterDirectory>]==] .. "\n"
    end
    
    str = str .. [==[      <AutoFilterCorrection>]==] .. get_bool_str(param.AutoFilterCorrection) .. [==[</AutoFilterCorrection>]==] .. "\n"
    str = str .. [==[      <SamplingRate>]==] .. param.SamplingRate .. [==[</SamplingRate>]==] .. "\n"
    str = str .. [==[      <SkipTimeAlignment>]==] .. get_bool_str(param.SkipTimeAlignment) .. [==[</SkipTimeAlignment>]==] .. "\n"
    str = str .. [==[    </G160>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[AutoDT]==],
        CalculationMode = 1,
            -- 0-Default CSS-based ITU-T P.502 Amd.1
            -- 1-Speech-based ITU-T P.502 Amd.1
        UseFilterForST = false, -- TRUE/FALSE
        FilterFilenameST = [==[x:\abc\def.hdf]==], -- file name with path
        UseSourceFile - TRUE/FALSE
        SourceFile = [==[x:\abc\def.dat]==], -- file name with path
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed File (DT)
        2 -- Near-end File(ST)
        3 -- Source File SND
--]]
func_gen[ [==[AutoDT]==] ] = function(param)
    local str = ""
    str = str .. [==[    <AutoDT>]==] .. "\n"
    if 0 == param.CalculationMode or 1 == param.CalculationMode then
        str = str .. [==[      <CalculationMode>]==] .. param.CalculationMode .. [==[</CalculationMode>]==] .. "\n"
    else -- default: Speech-based ITU-T P.502 Amd.1
        str = str .. [==[      <CalculationMode>]==] .. 1 .. [==[</CalculationMode>]==] .. "\n"
    end
    if true == param.UseFilterForST then
        str = str .. [==[      <UseFilterForST>TRUE</UseFilterForST>]==] .. "\n"
        str = str .. [==[      <FilterFilenameST>]==] .. param.FilterFilenameST .. [==[</FilterFilenameST>]==] .. "\n"
    else
        str = str .. [==[      <UseFilterForST>FALSE</UseFilterForST>]==] .. "\n"
    end
    if true == param.UseSourceFile then
        str = str .. [==[      <UseSourceFile>TRUE</UseSourceFile>]==] .. "\n"
        str = str .. [==[      <SourceFile>]==] .. param.SourceFile .. [==[</SourceFile>]==] .. "\n"
    else
        str = str .. [==[      <UseSourceFile>FALSE</UseSourceFile>]==] .. "\n"
    end
    str = str .. [==[    </AutoDT>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[TOSQA]==],
        VarDelayRange = [==[<62ms]==], -- Search var. delay
            -- 0    - No
            -- 512  - <62ms
            -- 1024 - <125ms
            -- 2048 - <250ms
            -- 4096 - <500ms
            -- 8192 - <1000ms
        FixedDelayRange = [==[<250ms]==], -- Search fixed delay
            -- 0     - No
            -- -2048 - < 250ms
            -- -4096 - < 500ms
            -- -8192 - < 1000ms
            -- 2048  - < 250ms (only pos)
            -- 4096  - < 500ms (only pos)
            -- 8192  - < 1000ms (only pos)
        FixedDelayInMs = 0, -- Fixed delay, must be a interger
        UseTosqa2001 = true, -- TRUE/FALSE
        Tosqa2001Mode = 3,
            -- 0 - electrical     & High quality handset
            -- 1 - electrical     & Headphone (Wideband)
            -- 3 - acoustical     & High quality handset
            -- ? - acoustical     & Average quality handset  no use???
            -- 2 - acoustical     & Headphone (Wideband)
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Degraded File
        2 -- Unprocessed - not need
        3 -- Reference file (Clean Speech)
--]]
func_gen[ [==[TOSQA]==] ] = function(param)
    local str = ""
    str = str .. [==[    <TOSQA>]==] .. "\n"
    VarDelayRangeTable = {
        [ [==[No]==] ]      = 0,
        [ [==[<62ms]==] ]   = 512,
        [ [==[<125ms]==] ]  = 1024,
        [ [==[<250ms]==] ]  = 2048,
        [ [==[<500ms]==] ]  = 4096,
        [ [==[<1000ms]==] ] = 8192
    }
    if VarDelayRangeTable[param.VarDelayRange] then
        str = str .. [==[      <VarDelayRange>]==] .. VarDelayRangeTable[param.VarDelayRange] .. [==[</VarDelayRange>]==] .. "\n"
    else -- default: No
        str = str .. [==[      <VarDelayRange>]==] .. 0 .. [==[</VarDelayRange>]==] .. "\n"
    end
    FixedDelayRangeTable = {
        [ [==[No]==] ]      = 0,
        [ [==[<250ms]==] ]  = -2048,
        [ [==[<500ms]==] ]  = -4096,
        [ [==[<1000ms]==] ] = -8192,
        [ [==[<250ms (only pos)]==] ]  = 2048,
        [ [==[<500ms (only pos)]==] ]  = 4096,
        [ [==[<1000ms (only pos)]==] ] = 8192
    }
    if FixedDelayRangeTable[param.FixedDelayRange] then
        str = str .. [==[      <FixedDelayRange>]==] .. FixedDelayRangeTable[param.FixedDelayRange] .. [==[</FixedDelayRange>]==] .. "\n"
    else -- default: No
        str = str .. [==[      <FixedDelayRange>]==] .. 0 .. [==[</FixedDelayRange>]==] .. "\n"
    end
    if param.FixedDelayInMs then
        str = str .. [==[      <FixedDelayInMs>]==] .. param.FixedDelayInMs .. [==[</FixedDelayInMs>]==] .. "\n"
    else
        str = str .. [==[      <FixedDelayInMs>]==] .. 0 .. [==[</FixedDelayInMs>]==] .. "\n"
    end
    if true == param.UseTosqa2001 then
        str = str .. [==[      <UseTosqa2001>TRUE</UseTosqa2001>]==] .. "\n"
        if 0 == param.Tosqa2001Mode or 1 == param.Tosqa2001Mode or 2 == param.Tosqa2001Mode or 3 == param.Tosqa2001Mode then
            str = str .. [==[      <Tosqa2001Mode>]==] .. param.Tosqa2001Mode .. [==[</Tosqa2001Mode>]==] .. "\n"
        else -- default: acoustical, High quality handset
            str = str .. [==[      <Tosqa2001Mode>]==] .. 3 .. [==[</Tosqa2001Mode>]==] .. "\n"
        end
    else
        str = str .. [==[      <UseTosqa2001>FALSE</UseTosqa2001>]==] .. "\n"
    end
    str = str .. [==[    </TOSQA>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[POLQA]==],
        PolqaVersion = 1，
            -- 0-V1.10
            -- 1-V2.40
        Bandwidthmode = 2，
            -- 0-Narrowband el
            -- 1-Narrowband ac
            -- 2-Wideband
        UseFixedASLs = true, -- TRUE/FALSE: Use fixed ASL(req. for acoustic meas.)
        HighAccuracyMode = true, -- TRUE/FALSE: Use High Accuracy Mode
        HighAccuracyModeMT = true, -- TRUE/FALSE: Multithreaded High Accuracy Mode
        ForceCommonSamplingRate = false, -- no setting at ACQUA UI
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Degraded File
        2 -- Unprocessed - not need
        3 -- Reference file (Clean Speech)
--]]
func_gen[ [==[POLQA]==] ] = function(param)
    local str = ""
    str = str .. [==[    <POLQA>]==] .. "\n"
    str = str .. [==[      <ForceCommonSamplingRate>FALSE</ForceCommonSamplingRate>]==] .. "\n"
    str = str .. [==[      <CommonSamplingRate>00000000000080BB0E40</CommonSamplingRate>]==] .. "\n"
    str = str .. [==[      <PolqaVersion>]==] .. param.PolqaVersion .. [==[</PolqaVersion>]==] .. "\n"
    str = str .. [==[      <Bandwidthmode>]==] .. param.BandWidthMode .. [==[</Bandwidthmode>]==] .. "\n"
    str = str .. [==[      <UseFixedASLs>]==] .. get_bool_str(param.UseFixedASLs) .. [==[</UseFixedASLs>]==] .. "\n"
    str = str .. [==[      <HighAccuracyMode>]==] .. get_bool_str(param.HighAccuracyMode) .. [==[</HighAccuracyMode>]==] .. "\n"
    str = str .. [==[      <HighAccuracyModeMT>]==] .. get_bool_str(param.HighAccuracyModeMT) .. [==[</HighAccuracyModeMT>]==] .. "\n"
    str = str .. [==[    </POLQA>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[DT]==],
        UseSrcSND = false, -- TRUE/FALSE
        DelaySrcRCVvsRecording = 56, -- 56 ms: Delay SND vs RCV (Round-Trip) -- only UseSrcSND = false 
            --<DelaySrcRCVvsRecording>00000000000000E00440</DelaySrcRCVvsRecording>
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed File (DT)
        2 -- Near-end File(ST)
        3 -- Source File RCV
--]]
func_gen[ [==[DT]==] ] = function(param)
    local str = ""
    str = str .. [==[    <DT>]==] .. "\n"
    if true == param.UseSrcSND then
        str = str .. [==[      <UseSrcSND>TRUE</UseSrcSND>]==] .. "\n"
    else
        str = str .. [==[      <UseSrcSND>FALSE</UseSrcSND>]==] .. "\n"
        str = str .. [==[      <DelaySrcRCVvsRecording>]==] .. ACQUA.batchDouble2Hex(DelaySrcRCVvsRecording) .. [==[</DelaySrcRCVvsRecording>]==] .. "\n"
    end
    str = str .. [==[    </DT>]==] .. "\n"
    
    return str
end

local CalcModeIdx = {
    [ [==[3QUEST]==] ] = 0,
    [ [==[TOSQA]==] ] = 1,
    [ [==[G160]==] ] = 3,
    [ [==[POLQA]==] ] = 4,
    [ [==[DT]==] ] = 5,
    [ [==[EQUEST]==] ] = 6,
}

function batchcalctool.batchxml_gen(paramIn, file)
    local param = batchcalctool.param_gen(paramIn)
    local str = [==[<?xml version="1.0" encoding="Windows-1252" standalone="no"?>]==] .. "\n"
    str = str .. [==[<Configuration>]==] .. "\n"
    str = str .. general_gen(param.timeRange, CalcModeIdx[param.CalcMode])
    str = str .. [==[  <CalcSettings>]==] .. "\n"
    str = str .. func_gen[param.CalcMode](param)
    str = str .. [==[  </CalcSettings>]==] .. "\n"
    str = str .. [==[</Configuration>]==] .. "\n"
    
    lfs.create_file(file)
    local fh= io.open(file, "w+")
	if fh then
		fh:write(str)
		io.close(fh)
		return true
    else
        return false
	end
end
