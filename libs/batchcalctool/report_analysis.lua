batchcalctool = batchcalctool or {}

local report = {}
local function report2table(resultFile)
    local keySperateStr = "\t"
    local file = io.open(resultFile, "r")
    local tableOut = {info = {}, result = {}}
    for line in file:lines(file) do
        if string.find(line, "#") then
            table.insert(tableOut.info, line)
        else
            tableOut[#tableOut+1] = {}
            local s,e = string.find(line, keySperateStr)
            while s do
                table.insert(tableOut[#tableOut], batchcalctool.str2num(string.sub(line, 1, s-1)))
                line = string.sub(line, e+1)
                s,e = string.find(line, keySperateStr)
            end
            table.insert(tableOut[#tableOut], batchcalctool.str2num(line))
        end
    end
    assert(file:close())

    return tableOut
end

report[ [==[3QUEST]==] ] = function(keyFileList, resultFile)
    resultTable = report2table(resultFile)

    local fileID, partID, smosID, nmosID, gmosID
    for i=1, #resultTable[1] do
        if string.find(resultTable[1][i], "Filename") then fileID = i end
        if string.find(resultTable[1][i], "Part") then partID = i end
        if string.find(resultTable[1][i], "S%-MOS") then smosID = i end
        if string.find(resultTable[1][i], "N%-MOS") then nmosID = i end
        if string.find(resultTable[1][i], "G%-MOS") then gmosID = i end
    end

    local result = {}
    for i=1,#keyFileList do
        result[i] = {name = lfs.get_filename_from_fullpath(keyFileList[i]), SMOS={}, NMOS={}, GMOS={}}
    end
    for i=2,#resultTable do
        local currentFile = resultTable[i][fileID]
        for j = 1,#result do
            if string.find(result[j].name, currentFile) then
                result[j].SMOS[resultTable[i][partID]] = resultTable[i][smosID]
                result[j].NMOS[resultTable[i][partID]] = resultTable[i][nmosID]
                result[j].GMOS[resultTable[i][partID]] = resultTable[i][gmosID]
                break
            end
        end
    end
    
    return result
end



function batchcalctool.report_analysis(param, fileList, resultFile)
    local result = report[param.module](fileList.processed, resultFile)
    
    return result
end