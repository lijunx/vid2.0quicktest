local tmpFile = "dongleTmp.txt"

-- get local dongle info
local function get_local_dongle(keyStr)
    os.execute("call devcon.exe find *" .. keyStr[1] .. "*" .. keyStr[2] .. "* > "..tmpFile)
    local file = io.open(tmpFile, "r")
    for line in file:lines(file) do
        local s,e = string.find(line, keyStr[1])
        if s then
            assert(file:close())
            os.execute("del \""..tmpFile.."\"")
            return true
        end
    end
    assert(file:close())
    os.execute("del \""..tmpFile.."\"")
    return false
end
local keyBatchStr = {"VID_0529", "PID_0001"}
local keyPolqaStr = {"VID_04B9", "PID_0300"}
local keyBatchLocal = get_local_dongle(keyBatchStr)
local keyPolqaLocal = get_local_dongle(keyPolqaStr)

-- get dongleTool
local dongleTool
function batchcalctool.check_dongleServerTool()
    dongleTool = [==[C:\Program Files (x86)\SEH Computertechnik GmbH\SEH UTN Manager\utnm.exe]==]
    local flag = lfs.check_file_exist(dongleTool)
    if not flag then
        dongleTool = [==[C:\Program Files\SEH Computertechnik GmbH\SEH UTN Manager\utnm.exe]==]
        flag = lfs.check_file_exist(dongleTool)
    end
    if not flag then
        dongleTool = nil
        return false
    end

    return true
end
batchcalctool.check_dongleServerTool()

-- update remote dongle server list
local serverList = {
    [1] = "192.168.50.145",
}
local flagFixServer = true
local function update_serverList()
    if nil == dongleTool then return false end
    if flagFixServer then return true end

    os.execute("call \""..dongleTool.."\" /c \"find\" /mr > "..tmpFile)
    local file = io.open(tmpFile, "r")
    local keyWord = "myUTN"
    serverList = {}
    for line in file:lines(file) do
        local s,e = string.find(line, keyWord)
        if s then
            s,e = string.find(line, ",")
            serverList[#serverList+1] = string.sub(line, 1, s-1)
        end
    end
    assert(file:close())
    os.execute("del \""..tmpFile.."\"")
    return true
end
update_serverList(dongleTool)

--[[
    dongle = {
        ip="192.168.50.145",
        port="1",
        type=1, --1 - batch; 2 - polqa
        status=1, -- 1 - active; 2 - free; 3 - occupied
    }
--]]
local dongleRemoteList = {}

local function active_dongle_remote(dongleID)
    os.execute("call \""..dongleTool.."\" /c \"activate ".. dongleRemoteList[dongleID].ip .. " " .. dongleRemoteList[dongleID].port .."\"")
    dongleRemoteList[dongleID].status = 1
end

local function release_dongle_remote(dongleID)
    os.execute("call \""..dongleTool.."\" /c \"deactivate ".. dongleRemoteList[dongleID].ip .. " " .. dongleRemoteList[dongleID].port .."\"")
    dongleRemoteList[dongleID].status = 2
end

local function update_dongleList()
    dongleRemoteList = {}
    dongleRemoteList.batch = false
    dongleRemoteList.polqa = false
    if nil == dongleTool then return end
    update_serverList(dongleTool)
    
    local dongleBatchStr = "Sentinel HL"
    local donglePolqaStr = "UltraPro"
    for i=1,#serverList do
        os.execute("call \""..dongleTool.."\" /c \"getlist ".. serverList[i] .."\" /mr > "..tmpFile)
        local file = io.open(tmpFile, "r")
        for line in file:lines(file) do
            local flagDongle = false
            local s,e = string.find(line, dongleBatchStr)
            if s then dongleRemoteList[#dongleRemoteList+1] = {type = 1}; flagDongle = true end
            local s,e = string.find(line, donglePolqaStr)
            if s then dongleRemoteList[#dongleRemoteList+1] = {type = 2}; flagDongle = true end
            if flagDongle then
                dongleRemoteList[#dongleRemoteList].ip = serverList[i]
                s,e = string.find(line, "0x")
                dongleRemoteList[#dongleRemoteList].port = tonumber(string.sub(line, 1, s-1))
                s,e = string.find(line, "Activated")
                if s then
                    dongleRemoteList[#dongleRemoteList].status = 1
                else
                    s,e = string.find(line, "Not activated")
                    if s then
                        dongleRemoteList[#dongleRemoteList].status = 2
                    else
                        dongleRemoteList[#dongleRemoteList].status = 3
                    end
                end
            end
        end
        assert(file:close())
        os.execute("del \""..tmpFile.."\"")
    end
    
    for i=1,#dongleRemoteList do
        if 1 == dongleRemoteList[i].status then -- active
            if 1 == dongleRemoteList[i].type then --batch dongle
                if keyBatchLocal or dongleRemoteList.batch then -- using more than one dongle should release one
                    release_dongle_remote(i)
                else
                    dongleRemoteList.batch = true
                end
            elseif 2 == dongleRemoteList[i].type then -- polqa dongle
                if keyPolqaLocal or dongleRemoteList.polqa then -- using more than one dongle should release one
                    release_dongle_remote(i)
                else
                    dongleRemoteList.polqa = true
                end
            end
        end
    end

    return true
end

-- type: 1 - batch; 2 - polqa
local function get_dongle(type)
    update_dongleList()
    if 1==type and (keyBatchLocal or dongleRemoteList.batch) then return true end
    if 2==type and (keyPolqaLocal or dongleRemoteList.polqa) then return true end
    if nil == dongleTool then return false end
    
    for i=1,#dongleRemoteList do
        if 2 == dongleRemoteList[i].status and type == dongleRemoteList[i].type then -- free
            active_dongle_remote(i)
        end
    end
    update_dongleList()
    if 1==type and (keyBatchLocal or dongleRemoteList.batch) then return true end
    if 2==type and (keyPolqaLocal or dongleRemoteList.polqa) then return true end
    
    -- if get fail, re-get all!!!
    if 1==type then keyBatchLocal = get_local_dongle(keyBatchStr) end
    if 2==type then keyPolqaLocal = get_local_dongle(keyPolqaStr) end
    for i=1,#dongleRemoteList do
        if 2 == dongleRemoteList[i].status and type == dongleRemoteList[i].type then -- free
            active_dongle_remote(i)
        end
    end
    update_dongleList()
    if 1==type and (keyBatchLocal or dongleRemoteList.batch) then return true end
    if 2==type and (keyPolqaLocal or dongleRemoteList.polqa) then return true end

    return false
end

local function release_dongle(type)
    update_dongleList()
    for i=1,#dongleRemoteList do
        if 1 == dongleRemoteList[i].status and type == dongleRemoteList[i].type then -- active
            release_dongle_remote(i)
        end
    end
    
    return true
end


local dongleRequireList = {
    [ [==[3QUEST]==] ] = {1},
    [ [==[POLQA]==] ] = {1, 2},
    [ [==[G160]==] ] = {1},
}
function batchcalctool.get_dongle(mode)
    if 1 == mode or 2 == mode then return get_dongle(mode) end

    -- tmp for current G160 self mode
    if [==[G160]==] == mode and GLOBAL.G60_MODE > 1 then return true end

    local dongleRequire = dongleRequireList[mode]
    if nil == dongleRequire then return true end

    local flag = true
    for i=1,#dongleRequire do
        local tmpFlag = get_dongle(dongleRequire[i])
        flag = flag and tmpFlag
    end

    if flag then return true end

    for i=1,#dongleRequire do release_dongle(dongleRequire[i]) end
    return false
end

function batchcalctool.release_dongle(mode)
    if 1 == mode or 2 == mode then return release_dongle(mode) end

    -- tmp for current G160 self mode
    if [==[G160]==] == mode and GLOBAL.G60_MODE > 1 then return end

    local dongleRequire = dongleRequireList[mode]
    if nil == dongleRequire then return end
    for i=1,#dongleRequire do release_dongle(dongleRequire[i]) end
end

function batchcalctool.pre_run_check_dongle()
    local errorStr = nil
    local flag = get_dongle(1)
    if flag then
        release_dongle(1)
        flag = get_dongle(2)
        if flag then
            release_dongle(2)
            return true
        else
            errorStr = "Dongle ERROR!\nPOLQA dongle Not Found!\nPOLQA item will be skiped!\n"
        end
    else
        errorStr = "Dongle ERROR!\nACQUA batch dongle Not Found!\nAll ACQUA batch related item will be skiped!\n"
    end

    notice.message_lib("pre_run_check_dongle", errorStr, "Error", false, false, true)
end