notice = notice or {}
notice.__help  = notice.__help or {}
notice.__test  = notice.__test or {}
notice.VERSION = 0.01

require "libs.notice.const"
require "libs.notice.base"
require "libs.notice.cmd"
require "libs.notice.ui"
require "libs.notice.log"

notice.cmdEN = true
notice.uiEN = true
notice.logEN = true
-- 0-No inner level     : no inner message show
-- 1-lib level          : UserInterface+structure+lib show
-- 2-re-structure level : UserInterface+structure show
-- 3-User level         : UserInterface only
notice.LEVEL = 1

-- general message
function notice.__help.message()
    local str = ""
    str = str .. notice.const.str.functionDividerStart
    str = str .. [==[function notice.message(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)]==] .. "\n"
    str = str .. notice.const.str.functionBlockDivider
    str = str .. [==[Usage:]==] .. "\n"
    str = str .. "\t" .. [==[notice.message(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)]==] .. "\n"
    str = str .. notice.const.str.functionBlockDivider
    str = str .. [==[Examples:]==] .. "\n"
    str = str .. notice.const.str.functionLineDivider
    str = str .. "\t" .. [==[notice.message("notice.message", "Message examples1")]==] .. "\n"
    str = str .. "\t" .. [==[      - default message, type: Info]==] .. "\n"
    str = str .. notice.const.str.functionLineDivider
    str = str .. "\t" .. [==[notice.message("notice.message", "Message examples2", "Warn")]==] .. "\n"
    str = str .. "\t" .. [==[      - Warning message]==] .. "\n"
    str = str .. notice.const.str.functionLineDivider
    str = str .. "\t" .. [==[notice.message("notice.message", "Message examples3", "Error", true, true, false)]==] .. "\n"
    str = str .. "\t" .. [==[      - Error message with cmd and UI information, and no log file information]==] .. "\n"
    str = str .. notice.const.str.functionBlockDivider
    str = str .. [==[Input:]==] .. "\n"
    str = str .. "\t" .. [==[functionStr -- string to show which function module show this information]==] .. "\n"
    str = str .. "\t" .. [==[messageStr  -- user defined message for show or debug]==] .. "\n"
    str = str .. "\t" .. [==[messageType -- type of message]==] .. "\n"
    str = str .. "\t" .. [==[               1 or "Info"  : General information (default)]==] .. "\n"
    str = str .. "\t" .. [==[               2 or "Warn"  : Warning information]==] .. "\n"
    str = str .. "\t" .. [==[               3 or "Error" : Error information]==] .. "\n"
    str = str .. "\t" .. [==[cmdEN       -- switch for cmd information output, use notice.cmdEN as default]==] .. "\n"
    str = str .. "\t" .. [==[uiEN        -- switch for ui information output, use notice.uiEN as default]==] .. "\n"
    str = str .. "\t" .. [==[logEN       -- switch for log information output, use notice.logEN as default]==] .. "\n"
    str = str .. notice.const.str.functionBlockDivider
    str = str .. [==[Output:]==] .. "\n"
    str = str .. "\t" .. [==[Delay       -- Delay value, format according to "outputUnit" option]==] .. "\n"
    str = str .. notice.const.str.functionDividerEnd

    print(str)
end

local function typeProcess(messageType)
    if nil == messageType then return "INFO " end
    if "number" == type(messageType) and 1 == messageType then return "INFO " end
    if "number" == type(messageType) and 2 == messageType then return "WARN " end
    if "number" == type(messageType) and 3 == messageType then return "ERROR" end
    if "string" == type(messageType) and "info" == string.lower(messageType) then return "INFO " end
    if "string" == type(messageType) and "warn" == string.lower(messageType) then return "WARN " end
    if "string" == type(messageType) and "error" == string.lower(messageType) then return "ERROR" end

    local messageStr = "Ivalid Parameter: messageType, use \"ms\" as default"
    notice.message("notice.message: typeProcess", messageStr, "Info")
    
    return "INFO "
end

function notice.message(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    if nil == cmdEN then cmdEN = notice.cmdEN end
    if nil == uiEN then uiEN = notice.uiEN end
    if nil == logEN then logEN = notice.logEN end
    local type = typeProcess(messageType)

    if cmdEN then notice.cmd.message(functionStr, messageStr, type) end
    if uiEN then notice.ui.message(functionStr, messageStr, type) end
    if logEN then notice.log.message(functionStr, messageStr, type) end
end

function notice.message_lib(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    if 1 == notice.LEVEL then
        notice.message(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    end
end

function notice.message_struct(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    if 1 == notice.LEVEL or 2 == notice.LEVEL then
        notice.message(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    end
end

function notice.message_user(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    if 1 == notice.LEVEL or 2 == notice.LEVEL or 3 == notice.LEVEL then
        notice.message(functionStr, messageStr, messageType, cmdEN, logEN, uiEN)
    end
end

function notice.__test.message()
    notice.message("notice.message", "Message examples1")
    notice.message("notice.message", "Message examples2", "Warn")
    notice.message("notice.message", "Message examples3", "Error", true, true, false)
end

