--[[
    TimeRange Table:
        -- No Time Range/ Always Full File
        parameter = {MODE = 0}

        -- Single Time Range
        parameter = {
            MODE = 1,
            Start = 0.0, -- ms, ACQUA.batchDouble2Hex
            Duration = 4.0, -- ms, ACQUA.batchDouble2Hex
            FromEnd = FALSE
            }
        parameter = {
            MODE = 1,
            Duration = 4.0, -- ms, ACQUA.batchDouble2Hex
            FromEnd = TRUE
            }

        -- Multiple Time Range
        parameter = {
            MODE = 2,
            Start = 0.0, -- ms, ACQUA.batchDouble2Hex
            Duration = 4.0, -- ms, ACQUA.batchDouble2Hex
            MaxNbrOfRanges = 8
            }
--]]
local function timeRange_gen(param)
    local str = ""
    str = str .. [==[    <TimeRange>]==] .. "\n"
    if 0 == param.MODE or 1 == param.MODE or 2 == param.MODE then
        str = str .. [==[      <MODE>]==] .. param.MODE .. [==[</MODE>]==] .. "\n"
    else
        str = str .. [==[      <MODE>]==] .. 0 .. [==[</MODE>]==] .. "\n"
    end
    if 1 == param.MODE then
        if true == param.FromEnd then
            str = str .. [==[      <FromEnd>TRUE</FromEnd>]==] .. "\n"
        else
            str = str .. [==[      <Start>]==] .. ACQUA.batchDouble2Hex(param.Start) .. [==[</Start>]==] .. "\n"
            str = str .. [==[      <FromEnd>FALSE</FromEnd>]==] .. "\n"
        end
        str = str .. [==[      <Duration>]==] .. ACQUA.batchDouble2Hex(param.Duration) .. [==[</Duration>]==] .. "\n"
    end
    if 2 == param.MODE then
        str = str .. [==[      <Start>]==] .. ACQUA.batchDouble2Hex(param.Start) .. [==[</Start>]==] .. "\n"
        str = str .. [==[      <Duration>]==] .. ACQUA.batchDouble2Hex(param.Duration) .. [==[</Duration>]==] .. "\n"
        str = str .. [==[      <MaxNbrOfRanges>]==] .. param.MaxNbrOfRanges .. [==[</MaxNbrOfRanges>]==] .. "\n"
    end
    str = str .. [==[    </TimeRange>]==] .. "\n"

    return str
end

local function general_gen(param, CalcMode)
    local str = ""
    str = str .. [==[  <General>]==] .. "\n"
    str = str .. [==[    <CalcMode>]==] .. CalcMode .. [==[</CalcMode>]==] .. "\n"
    str = str .. timeRange_gen(param)
    str = str .. [==[  </General>]==] .. "\n"

    return str
end


local function channelselect_gen(param, CalcMode)
    local str = ""
    str = str .. [==[  <FormBatchTool>]==] .. "\n"
    str = str .. [==[    <channelSelectCS_ItemIndex>2</channelSelectCS_ItemIndex>]==] .. "\n"
    str = str .. [==[    <channelSelectProc_ItemIndex>0</channelSelectProc_ItemIndex>]==] .. "\n"
    str = str .. [==[    <channelSelectUnproc_ItemIndex>1</channelSelectUnproc_ItemIndex>]==] .. "\n"
    str = str .. [==[  </FormBatchTool>]==] .. "\n"

    return str
end

local func_gen = {}

--[[
    param = {
        CalcMode = [==[3QUEST]==],
        BandWidthMode = 0, -- 0-"NB" 1-"WB" (ETSI EG 202 396-3 rev 1.3.1)
        FilterIRSRCV = true,  -- true or false, only for NB
        -- SkipVariableDelay = false,  -- true or false, always use false
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed
        2 -- Unprocessed
        3 -- Clean Speech
--]]
func_gen[ [==[3QUEST]==] ] = function(param)
    local str = ""
    str = str .. [==[    <3QUEST>]==] .. "\n"
    str = str .. [==[      <BandWidthMode>]==] .. param.BandWidthMode .. [==[</BandWidthMode>]==] .. "\n"
    if 0 == param.BandWidthMode and true == param.FilterIRSRCV then
        str = str .. [==[      <FilterIRSRCV>TRUE</FilterIRSRCV>]==] .. "\n"
    else
        str = str .. [==[      <FilterIRSRCV>FALSE</FilterIRSRCV>]==] .. "\n"
    end
    str = str .. [==[      <SkipVariableDelay>FALSE</SkipVariableDelay>]==] .. "\n" -- always use false for accurate
    str = str .. [==[    </3QUEST>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[3QUESTNG]==],
        BandWidthMode = 4,
            -- 4-Wideband Retrained (TS)     (ETSI TS 103 106)
            -- 5-Narrowband Retrained (TS)   (ETSI TS 103 106)
            -- 6-Narrowband Retrained (EG)   (ETSI EG 202 396-3 rev 1.4.1 or higher)
            -- 7-Wideband Retrained (EG)     (ETSI EG 202 396-3 rev 1.4.1 or higher)
        AdvancedXCorr = true,  -- true or false
        FilterIRSRCV = true,  -- true or false, only for NB
        SkipVariableDelay = false,  -- true or false, always use false
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed
        2 -- Unprocessed
        3 -- Clean Speech
--]]
func_gen[ [==[3QUESTNG]==] ] = function(param)
    local str = ""
    str = str .. [==[    <3QUESTNG>]==] .. "\n"
    str = str .. [==[      <BandWidthMode>]==] .. param.BandWidthMode .. [==[</BandWidthMode>]==] .. "\n"
    if true == param.AdvancedXCorr then
        str = str .. [==[      <AdvancedXCorr>TRUE</AdvancedXCorr>]==] .. "\n"
    else
        str = str .. [==[      <AdvancedXCorr>FALSE</AdvancedXCorr>]==] .. "\n"
    end
    if 0 == param.BandWidthMode and true == param.FilterIRSRCV then
        str = str .. [==[      <FilterIRSRCV>TRUE</FilterIRSRCV>]==] .. "\n"
    else
        str = str .. [==[      <FilterIRSRCV>FALSE</FilterIRSRCV>]==] .. "\n"
    end
    str = str .. [==[      <ResultValues>]==] .. "0\n1\n2\n3\n4\n9\n21\n36\n" .. [==[</ResultValues>]==] .. "\n"
    if true == param.SkipVariableDelay then
        str = str .. [==[      <SkipVariableDelay>TRUE</SkipVariableDelay>]==] .. "\n"
    else
        str = str .. [==[      <SkipVariableDelay>FALSE</SkipVariableDelay>]==] .. "\n"
    end
    str = str .. [==[    </3QUESTNG>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[G160]==],
        FilterMode = 1, -- Filter Mode for unprocessed reference
            -- 0-No filter
            -- 1-Maunal
            -- 2-Automatic(by Filename)
        ManualFilter = [==[x:\abc\def.hdf]==], -- file name with path, FilterMode = 1
        AutoFilterDirectory = [==[x:\abc\def.hdf]==], -- file path, FilterMode = 2
        SkipTimeAlignment = false,  -- true or false, always use false
        AutoFilterCorrection = true, -- TRUE/FALSE : Auto Filter Correction
        SamplingRate = 99, --: Select sampling rate always use 99
            -- 0-8000
            -- 1-16000
            -- 2-48000
            -- 99-Use Processed
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed
        2 -- Unprocessed
        3 -- Clean Speech
--]]
func_gen[ [==[G160]==] ] = function(param)
    local str = ""
    str = str .. [==[    <G160>]==] .. "\n"
    if 0 == param.FilterMode or 1 == param.FilterMode or 2 == param.FilterMode then
        str = str .. [==[      <FilterMode>]==] .. param.FilterMode .. [==[</FilterMode>]==] .. "\n"
    else -- default no filter
        str = str .. [==[      <FilterMode>]==] .. 0 .. [==[</FilterMode>]==] .. "\n"
    end
    if 1 == param.FilterMode then
        str = str .. [==[      <ManualFilter>]==] .. param.ManualFilter .. [==[</ManualFilter>]==] .. "\n"
    end
    if 2 == param.FilterMode then
        str = str .. [==[      <AutoFilterDirectory>]==] .. param.AutoFilterDirectory .. [==[</AutoFilterDirectory>]==] .. "\n"
    end
    if true == param.SkipTimeAlignment then
        str = str .. [==[      <SkipTimeAlignment>TRUE</SkipTimeAlignment>]==] .. "\n"
    else
        str = str .. [==[      <SkipTimeAlignment>FALSE</SkipTimeAlignment>]==] .. "\n"
    end
    if true == param.AutoFilterCorrection then
        str = str .. [==[      <AutoFilterCorrection>TRUE</AutoFilterCorrection>]==] .. "\n"
    else
        str = str .. [==[      <AutoFilterCorrection>FALSE</AutoFilterCorrection>]==] .. "\n"
    end
    str = str .. [==[      <SamplingRate>99</SamplingRate>]==] .. "\n"
    str = str .. [==[      <ResultValues>]==] .. "0\n7\n8\n9\n</ResultValues>\n"
    str = str .. [==[    </G160>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[AutoDT]==],
        CalculationMode = 1,
            -- 0-Default CSS-based ITU-T P.502 Amd.1
            -- 1-Speech-based ITU-T P.502 Amd.1
        UseFilterForST = false, -- TRUE/FALSE
        FilterFilenameST = [==[x:\abc\def.hdf]==], -- file name with path
        UseSourceFile - TRUE/FALSE
        SourceFile = [==[x:\abc\def.dat]==], -- file name with path
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed File (DT)
        2 -- Near-end File(ST)
        3 -- Source File SND
--]]
func_gen[ [==[AutoDT]==] ] = function(param)
    local str = ""
    str = str .. [==[    <AutoDT>]==] .. "\n"
    if 0 == param.CalculationMode or 1 == param.CalculationMode then
        str = str .. [==[      <CalculationMode>]==] .. param.CalculationMode .. [==[</CalculationMode>]==] .. "\n"
    else -- default: Speech-based ITU-T P.502 Amd.1
        str = str .. [==[      <CalculationMode>]==] .. 1 .. [==[</CalculationMode>]==] .. "\n"
    end
    if true == param.UseFilterForST then
        str = str .. [==[      <UseFilterForST>TRUE</UseFilterForST>]==] .. "\n"
        str = str .. [==[      <FilterFilenameST>]==] .. param.FilterFilenameST .. [==[</FilterFilenameST>]==] .. "\n"
    else
        str = str .. [==[      <UseFilterForST>FALSE</UseFilterForST>]==] .. "\n"
    end
    if true == param.UseSourceFile then
        str = str .. [==[      <UseSourceFile>TRUE</UseSourceFile>]==] .. "\n"
        str = str .. [==[      <SourceFile>]==] .. param.SourceFile .. [==[</SourceFile>]==] .. "\n"
    else
        str = str .. [==[      <UseSourceFile>FALSE</UseSourceFile>]==] .. "\n"
    end
    str = str .. [==[    </AutoDT>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[TOSQA]==],
        VarDelayRange = [==[<62ms]==], -- Search var. delay
            -- 0    - No
            -- 512  - <62ms
            -- 1024 - <125ms
            -- 2048 - <250ms
            -- 4096 - <500ms
            -- 8192 - <1000ms
        FixedDelayRange = [==[<250ms]==], -- Search fixed delay
            -- 0     - No
            -- -2048 - < 250ms
            -- -4096 - < 500ms
            -- -8192 - < 1000ms
            -- 2048  - < 250ms (only pos)
            -- 4096  - < 500ms (only pos)
            -- 8192  - < 1000ms (only pos)
        FixedDelayInMs = 0, -- Fixed delay, must be a interger
        UseTosqa2001 = true, -- TRUE/FALSE
        Tosqa2001Mode = 3,
            -- 0 - electrical     & High quality handset
            -- 1 - electrical     & Headphone (Wideband)
            -- 3 - acoustical     & High quality handset
            -- ? - acoustical     & Average quality handset  no use???
            -- 2 - acoustical     & Headphone (Wideband)
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Degraded File
        2 -- Unprocessed - not need
        3 -- Reference file (Clean Speech)
--]]
func_gen[ [==[TOSQA]==] ] = function(param)
    local str = ""
    str = str .. [==[    <TOSQA>]==] .. "\n"
    VarDelayRangeTable = {
        [ [==[No]==] ]      = 0,
        [ [==[<62ms]==] ]   = 512,
        [ [==[<125ms]==] ]  = 1024,
        [ [==[<250ms]==] ]  = 2048,
        [ [==[<500ms]==] ]  = 4096,
        [ [==[<1000ms]==] ] = 8192
    }
    if VarDelayRangeTable[param.VarDelayRange] then
        str = str .. [==[      <VarDelayRange>]==] .. VarDelayRangeTable[param.VarDelayRange] .. [==[</VarDelayRange>]==] .. "\n"
    else -- default: No
        str = str .. [==[      <VarDelayRange>]==] .. 0 .. [==[</VarDelayRange>]==] .. "\n"
    end
    FixedDelayRangeTable = {
        [ [==[No]==] ]      = 0,
        [ [==[<250ms]==] ]  = -2048,
        [ [==[<500ms]==] ]  = -4096,
        [ [==[<1000ms]==] ] = -8192,
        [ [==[<250ms (only pos)]==] ]  = 2048,
        [ [==[<500ms (only pos)]==] ]  = 4096,
        [ [==[<1000ms (only pos)]==] ] = 8192
    }
    if FixedDelayRangeTable[param.FixedDelayRange] then
        str = str .. [==[      <FixedDelayRange>]==] .. FixedDelayRangeTable[param.FixedDelayRange] .. [==[</FixedDelayRange>]==] .. "\n"
    else -- default: No
        str = str .. [==[      <FixedDelayRange>]==] .. 0 .. [==[</FixedDelayRange>]==] .. "\n"
    end
    if param.FixedDelayInMs then
        str = str .. [==[      <FixedDelayInMs>]==] .. param.FixedDelayInMs .. [==[</FixedDelayInMs>]==] .. "\n"
    else
        str = str .. [==[      <FixedDelayInMs>]==] .. 0 .. [==[</FixedDelayInMs>]==] .. "\n"
    end
    if true == param.UseTosqa2001 then
        str = str .. [==[      <UseTosqa2001>TRUE</UseTosqa2001>]==] .. "\n"
        if 0 == param.Tosqa2001Mode or 1 == param.Tosqa2001Mode or 2 == param.Tosqa2001Mode or 3 == param.Tosqa2001Mode then
            str = str .. [==[      <Tosqa2001Mode>]==] .. param.Tosqa2001Mode .. [==[</Tosqa2001Mode>]==] .. "\n"
        else -- default: acoustical, High quality handset
            str = str .. [==[      <Tosqa2001Mode>]==] .. 3 .. [==[</Tosqa2001Mode>]==] .. "\n"
        end
    else
        str = str .. [==[      <UseTosqa2001>FALSE</UseTosqa2001>]==] .. "\n"
    end
    str = str .. [==[    </TOSQA>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[POLQA]==],
        PolqaVersion = 1，
            -- 0-V1.10
            -- 1-V2.40
        Bandwidthmode = 2，
            -- 0-Narrowband el
            -- 1-Narrowband ac
            -- 2-Wideband
        UseFixedASLs = true, -- TRUE/FALSE: Use fixed ASL(req. for acoustic meas.)
        HighAccuracyMode = true, -- TRUE/FALSE: Use High Accuracy Mode
        HighAccuracyModeMT = true, -- TRUE/FALSE: Multithreaded High Accuracy Mode
        ForceCommonSamplingRate = false, -- no setting at ACQUA UI
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Degraded File
        2 -- Unprocessed - not need
        3 -- Reference file (Clean Speech)
--]]
func_gen[ [==[POLQA]==] ] = function(param)
    local str = ""
    str = str .. [==[    <POLQA>]==] .. "\n"
    str = str .. [==[      <ForceCommonSamplingRate>FALSE</ForceCommonSamplingRate>]==] .. "\n"
    if 0 == param.PolqaVersion or 1 == param.PolqaVersion then
        str = str .. [==[      <PolqaVersion>]==] .. param.PolqaVersion .. [==[</PolqaVersion>]==] .. "\n"
    else -- default: V2.40
        str = str .. [==[      <PolqaVersion>]==] .. 1 .. [==[</PolqaVersion>]==] .. "\n"
    end
    if 0 == param.Bandwidthmode or 1 == param.Bandwidthmode or 2 == param.Bandwidthmode then
        str = str .. [==[      <Bandwidthmode>]==] .. param.Bandwidthmode .. [==[</Bandwidthmode>]==] .. "\n"
    else -- default: Narrowband ac
        str = str .. [==[      <Bandwidthmode>]==] .. 1 .. [==[</Bandwidthmode>]==] .. "\n"
    end
    if true == param.UseFixedASLs then
        str = str .. [==[      <UseFixedASLs>TRUE</UseFixedASLs>]==] .. "\n"
    else
        str = str .. [==[      <UseFixedASLs>FALSE</UseFixedASLs>]==] .. "\n"
    end
    if true == param.HighAccuracyMode then
        str = str .. [==[      <HighAccuracyMode>TRUE</HighAccuracyMode>]==] .. "\n"
    else
        str = str .. [==[      <HighAccuracyMode>FALSE</HighAccuracyMode>]==] .. "\n"
    end
    if true == param.HighAccuracyModeMT then
        str = str .. [==[      <HighAccuracyModeMT>TRUE</HighAccuracyModeMT>]==] .. "\n"
    else
        str = str .. [==[      <HighAccuracyModeMT>FALSE</HighAccuracyModeMT>]==] .. "\n"
    end
    str = str .. [==[    </POLQA>]==] .. "\n"
    
    return str
end

--[[
    param = {
        CalcMode = [==[DT]==],
        UseSrcSND = false, -- TRUE/FALSE
        DelaySrcRCVvsRecording = 56, -- 56 ms: Delay SND vs RCV (Round-Trip) -- only UseSrcSND = false 
            --<DelaySrcRCVvsRecording>00000000000000E00440</DelaySrcRCVvsRecording>
        TimeRange = { ..... }, -- refer to TimeRange Table define
    }
    Channels:
        1 -- Processed File (DT)
        2 -- Near-end File(ST)
        3 -- Source File RCV
--]]
func_gen[ [==[DT]==] ] = function(param)
    local str = ""
    str = str .. [==[    <DT>]==] .. "\n"
    if true == param.UseSrcSND then
        str = str .. [==[      <UseSrcSND>TRUE</UseSrcSND>]==] .. "\n"
    else
        str = str .. [==[      <UseSrcSND>FALSE</UseSrcSND>]==] .. "\n"
        str = str .. [==[      <DelaySrcRCVvsRecording>]==] .. ACQUA.batchDouble2Hex(DelaySrcRCVvsRecording) .. [==[</DelaySrcRCVvsRecording>]==] .. "\n"
    end
    str = str .. [==[    </DT>]==] .. "\n"
    
    return str
end

local CalcModeIdx = {
    [ [==[3QUEST]==] ] = 0,
    [ [==[G160]==] ] = 3,
    [ [==[AutoDT]==] ] = 6,
    [ [==[3QUESTNG]==] ] = 4,
    [ [==[TOSQA]==] ] = 1,
    [ [==[DT]==] ] = 8,
    [ [==[POLQA]==] ] = 5,
}

function ACQUA.batchxml_gen(param, file)
    local str = [==[<?xml version="1.0" encoding="Windows-1252" standalone="no"?>]==] .. "\n"
    str = str .. [==[<Configuration>]==] .. "\n"
    str = str .. [==[  <CalcSettings>]==] .. "\n"
    str = str .. func_gen[param.CalcMode](param)
    str = str .. [==[  </CalcSettings>]==] .. "\n"
    str = str .. general_gen(param.TimeRange, CalcModeIdx[param.CalcMode])
    str = str .. channelselect_gen(param, CalcMode)
    str = str .. [==[</Configuration>]==] .. "\n"

    local fh= io.open(file, "w+")
	if fh then
		fh:write(str)
		io.close(fh)
		return true
    else
        return false
	end
end
