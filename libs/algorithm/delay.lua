algorithm  = algorithm or {}
algorithm.__help  = algorithm.__help or {}

function algorithm.__help.delay()
    local str = ""
    str = str .. const.str.functionDividerStart
    str = str .. [==[function algorithm.delay(record, source, outputUnit, searchSpeed)]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Usage:]==] .. "\n"
    str = str .. "\t" .. [==[Delay = algorithm.delay(record, source, outputUnit, searchSpeed)]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Examples:]==] .. "\n"
    str = str .. const.str.functionLineDivider
    str = str .. "\t" .. [==[Delay = algorithm.delay(record, source)]==] .. "\n"
    str = str .. "\t" .. [==[      - default setting, return delay in ms(mini-second)]==] .. "\n"
    str = str .. "\t" .. [==[      - Search speed = "normal", sample jump = 5]==] .. "\n"
    str = str .. const.str.functionLineDivider
    str = str .. "\t" .. [==[Delay = algorithm.delay(record, source, "s", "fast")]==] .. "\n"
    str = str .. "\t" .. [==[      - return delay in s(second)]==] .. "\n"
    str = str .. "\t" .. [==[      - Search speed = "fast", sample jump = 2]==] .. "\n"
    str = str .. const.str.functionLineDivider
    str = str .. "\t" .. [==[Delay = algorithm.delay(record, source, 3, 4)]==] .. "\n"
    str = str .. "\t" .. [==[      - return delay in sample]==] .. "\n"
    str = str .. "\t" .. [==[      - Search sample jump = 4]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Input:]==] .. "\n"
    str = str .. "\t" .. [==[record      -- record wavedata, should be mono]==] .. "\n"
    str = str .. "\t" .. [==[source      -- wavedata, should be mono]==] .. "\n"
    str = str .. "\t" .. [==[outputUnit  -- output result unit select]==] .. "\n"
    str = str .. "\t" .. [==[               1 or "ms"    : ms (default)]==] .. "\n"
    str = str .. "\t" .. [==[               2 or "s"     : s]==] .. "\n"
    str = str .. "\t" .. [==[               3 or "sample": sample]==] .. "\n"
    str = str .. "\t" .. [==[searchSpeed -- Searching speed select, more accurate lead to more calculataion]==] .. "\n"
    str = str .. "\t" .. [==[               "normal"     : Delay calculation sample jump = 5 (default)]==] .. "\n"
    str = str .. "\t" .. [==[               "fast"       : Delay calculation sample jump = 10]==] .. "\n"
    str = str .. "\t" .. [==[               "accurate"   : Delay calculation sample jump = 2]==] .. "\n"
    str = str .. "\t" .. [==[               n            : Delay calculation sample jump = n, should be a number >= 1]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Output:]==] .. "\n"
    str = str .. "\t" .. [==[Delay       -- Delay value, format according to "outputUnit" option]==] .. "\n"
    str = str .. const.str.functionDividerEnd

    print(str)
end

local function skipProcess(searchSpeed)
    if nil == searchSpeed then return 5 end
    if "number" == type(searchSpeed) and searchSpeed>=1 then return math.floor(searchSpeed) end
    if "string" == type(searchSpeed) and "normal" == string.lower(searchSpeed) then return 5 end
    if "string" == type(searchSpeed) and "fast" == string.lower(searchSpeed) then return 10 end
    if "string" == type(searchSpeed) and "accurate" == string.lower(searchSpeed) then return 2 end

    local messageStr = "Ivalid Parameter: searchSpeed, use \"normal\" as default"
    notice.message_lib("algorithm.delay: skipProcess", messageStr, "Info")
    
    return 5
end

local function unitProcess(unit)
    if nil == unit then return 1 end
    if "number" == type(unit) and (1 == unit or 2 == unit or 3 == unit) then return unit end
    if "string" == type(unit) and "ms" == string.lower(unit) then return 1 end
    if "string" == type(unit) and "s" == string.lower(unit) then return 2 end
    if "string" == type(unit) and "sample" == string.lower(unit) then return 3 end

    local messageStr = "Ivalid Parameter: outputUnit, use \"ms\" as default"
    notice.message_lib("algorithm.delay: unitProcess", messageStr, "Info")
    
    return 1
end

function algorithm.delay(record, source, outputUnit, searchSpeed)
    -- input check
    local sample_rate_rec = Wave.get_samplerate(record)
    if nil == sample_rate_rec then
        notice.message_lib("algorithm.delay", "Invalid record input", "Error")
        return nil
    end
    local sample_rate_src = Wave.get_samplerate(source)
    if nil == sample_rate_src then
        notice.message_lib("algorithm.delay", "Invalid source input", "Error")
        return nil
    end
    if sample_rate_src ~= sample_rate_rec then
        notice.message_lib("algorithm.delay", "Source & record do not have the same sample rate", "Error")
        return nil
    end
    local sampleLenSource = Wave.get_samplenum(source)
    local sampleLenRecord = Wave.get_samplenum(record)
    if sampleLenSource > sampleLenRecord then
        notice.message_lib("algorithm.delay", "Source length should > record length", "Error")
        return nil
    end
    
    -- parameter prepare
    local skip = skipProcess(searchSpeed)
    local unit = unitProcess(outputUnit)

    -- calculation
    local delay = Wave.check_delay(record, source, skip)

    -- output
    if 1 == unit then return delay*1000 end -- ms
    if 2 == unit then return delay end -- s
    if 3 == unit then return math.round(delay*sample_rate_src) end -- samples
end

-- param = {
--     delayCheckStartS = 28.8, --s
--     delayCheckEndS = 30.8, --s
-- }
function algorithm.align(record, source, param)
    local sampleLenSource = Wave.get_samplenum(source)
    local sampleLenRecord = Wave.get_samplenum(record)
    local fs = Wave.get_samplerate(source)
    
    local cutStartSample = 0
    local cutEndSample = sampleLenSource
    if param and param.delayCheckStartS then cutStartSample = param.delayCheckStartS*fs end
    if param and param.delayCheckEndS then cutEndSample = param.delayCheckEndS*fs end
    
    local cutSourceTmp = Wave.cut(source, cutStartSample, cutEndSample, 2)
    cutEndSample = cutEndSample + sampleLenRecord - sampleLenSource
    local cutRecordTmp = Wave.cut(record, cutStartSample, cutEndSample, 2)
    
    local tmpDelaySample = algorithm.delay(cutRecordTmp, cutSourceTmp, "sample")
    Wave.clearWavdata(cutSourceTmp)
    Wave.clearWavdata(cutRecordTmp)
    collectgarbage("collect")
    
    cutStartSample = tmpDelaySample
    cutEndSample = tmpDelaySample + sampleLenSource
    local alignRecord = Wave.cut(record, cutStartSample, cutEndSample, 2)
    
    return alignRecord
end