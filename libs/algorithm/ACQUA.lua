require "ACQUA"

ACQUA = ACQUA or {}
ACQUA.__help  = ACQUA.__help or {}
ACQUA.__test  = ACQUA.__test or {}
ACQUA.VERSION = 0.01

require "libs.algorithm.ACQUA.batchxml"

function ACQUA.batchdat_gen(waveCH1, waveCH2, waveCH3, file)
    -- input check
    local sample_rate_waveCH1 = Wave.get_samplerate(waveCH1)
    if nil == sample_rate_waveCH1 then
        notice.message_lib("algorithm.batchdat_gen", "Invalid waveCH1 input", "Error")
        return nil
    end
    local sample_rate_waveCH2 = Wave.get_samplerate(waveCH2)
    if nil == sample_rate_waveCH2 then
        notice.message_lib("algorithm.batchdat_gen", "Invalid waveCH2 input", "Error")
        return nil
    end
    local sample_rate_waveCH3 = Wave.get_samplerate(waveCH3)
    if nil == sample_rate_waveCH3 then
        notice.message_lib("algorithm.batchdat_gen", "Invalid waveCH3 input", "Error")
        return nil
    end
    if sample_rate_waveCH3 ~= sample_rate_waveCH1 or sample_rate_waveCH3 ~= sample_rate_waveCH2 then
        notice.message_lib("algorithm.batchdat_gen", "waveCH1, waveCH2, waveCH3 do not have the same sample rate", "Error")
        return nil
    end

    local splitWav = {}
    splitWav[1] = waveCH1
    splitWav[2] = waveCH2
    splitWav[3] = waveCH3
    local saveWav = Wave.merge(splitWav)
    local ret = Wave.save(file, saveWav, 3)
    Wave.clearWavdata(saveWav)

    if ret then
        return true
    else
        return false
    end
end

function ACQUA.batchtxt_gen(fileList, file)
    local str = ""
    for i=1,#fileList do
        str = str .. fileList[i] .. "\n"
        str = str .. fileList[i] .. "\n"
        str = str .. fileList[i] .. "\n"
        str = str .. "####\n"
    end
    local fp = assert( io.open(file,"w+") )
    assert(fp:write( str.."\n" ) )
    assert(fp:close())

    return true
end

function ACQUA.batchbat_gen(path, filename)
    local str = ""
    str = str .. [==[@echo off]==] .. "\n"
    str = str .. [==[IF DEFINED ProgramFiles(x86) (SET BatchCalculatorPath="%ProgramFiles(x86)%\HEAD Analyzer ACQUA\BatchCalculatorTool.exe") ELSE (SET BatchCalculatorPath="%ProgramFiles%\HEAD Analyzer ACQUA\BatchCalculatorTool.exe")]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    str = str .. [==[IF NOT EXIST %BatchCalculatorPath% (]==] .. "\n"
    str = str .. [==[echo Could not locate Batch Calculator executable!]==] .. "\n"
    str = str .. [==[GOTO THE_END) ]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    str = str .. [==[%BatchCalculatorPath% -b ".\]==] .. filename .. [==[.txt" -r ".\]==] .. filename .. [==[_Result.txt" -c ".\]==] .. filename .. [==[.xml" -s -e]==] .. "\n"
    str = str .. [==[echo Test vectors ]==] .. filename .. [==[.txt done! Results are located at:]==] .. "\n"
    str = str .. [==[echo ]==] .. filename .. [==[_Result.txt]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    str = str .. [==[pause]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    str = str .. [==[:THE_END]==] .. "\n"

    local fp = assert( io.open(path .. filename .. ".bat","w+") )
    assert(fp:write( str.."\n" ) )
    assert(fp:close())

    return true
end

function ACQUA.batch_all_bat_gen(path)
    local fileList = lfs.get_filelist(path, ".bat", "ALL", true)

    local str = ""
    str = str .. [==[@echo off]==] .. "\n"
    str = str .. [==[IF DEFINED ProgramFiles(x86) (SET BatchCalculatorPath="%ProgramFiles(x86)%\HEAD Analyzer ACQUA\BatchCalculatorTool.exe") ELSE (SET BatchCalculatorPath="%ProgramFiles%\HEAD Analyzer ACQUA\BatchCalculatorTool.exe")]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    str = str .. [==[IF NOT EXIST %BatchCalculatorPath% (]==] .. "\n"
    str = str .. [==[echo Could not locate Batch Calculator executable!]==] .. "\n"
    str = str .. [==[GOTO THE_END) ]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    for i=1,#fileList do
        str = str .. [==[%BatchCalculatorPath% -b ".\]==] .. fileList[i] .. [==[.txt" -r ".\]==] .. fileList[i] .. [==[_Result.txt" -c ".\]==] .. fileList[i] .. [==[.xml" -s -e]==] .. "\n"
        str = str .. [==[echo Test vectors ]==] .. fileList[i] .. [==[.txt done! Results are located at:]==] .. "\n"
        str = str .. [==[echo ]==] .. fileList[i] .. [==[_Result.txt]==] .. "\n"
        str = str .. [==[]==] .. "\n"
    end
    str = str .. [==[pause]==] .. "\n"
    str = str .. [==[]==] .. "\n"
    str = str .. [==[:THE_END]==] .. "\n"

    local fp = assert( io.open(path .. "batch_ALL.bat","w+") )
    assert(fp:write( str.."\n" ) )
    assert(fp:close())

    return true
end

ACQUA[ [==[3QUESTNG]==] ] = function(processed, unprocessed, clean, sampleIdx, path, param, existFileName)
    local flag
    local filename = "batch_3QUESTNG"
    if existFileName then
        lfs.copy(path .. existFileName .. ".txt", path .. filename .. ".txt")
    else
        local fileList = {}
        for i=1,#sampleIdx do
            -- ACQUA data write
            fileList[i] = sampleIdx[i].Name .. ".dat"
            local processedWrite = Wave.cut(processed, sampleIdx[i].processed.startSample, sampleIdx[i].processed.endSample, 2)
            local unprocessedWrite = Wave.cut(unprocessed, sampleIdx[i].unprocessed.startSample, sampleIdx[i].unprocessed.endSample, 2)
            ACQUA.batchdat_gen(processedWrite, unprocessedWrite, clean, path .. fileList[i])
            Wave.clearWavdata(processedWrite)
            Wave.clearWavdata(unprocessedWrite)
            collectgarbage("collect")
        end
        collectgarbage("collect")
        flag = ACQUA.batchtxt_gen(fileList, path .. filename .. ".txt")
    end
    flag = ACQUA.batchxml_gen(param, path .. filename .. ".xml")
    flag = ACQUA.batchbat_gen(path, filename)

    return filename
end

ACQUA[ [==[G160]==] ] = function(processed, unprocessed, clean, sampleIdx, path, param, existFileName)
    local flag
    local filename = "batch_SNRi"
    if existFileName then
        lfs.copy(path .. existFileName .. ".txt", path .. filename .. ".txt")
    else
        local fileList = {}
        for i=1,#sampleIdx do
            -- ACQUA data write
            fileList[i] = sampleIdx[i].Name .. ".dat"
            local processedWrite = Wave.cut(processed, sampleIdx[i].processed.startSample, sampleIdx[i].processed.endSample, 2)
            local unprocessedWrite = Wave.cut(unprocessed, sampleIdx[i].unprocessed.startSample, sampleIdx[i].unprocessed.endSample, 2)
            ACQUA.batchdat_gen(processedWrite, unprocessedWrite, clean, path .. fileList[i])
            Wave.clearWavdata(processedWrite)
            Wave.clearWavdata(unprocessedWrite)
            collectgarbage("collect")
        end
        collectgarbage("collect")
        flag = ACQUA.batchtxt_gen(fileList, path .. filename .. ".txt")
    end
    flag = ACQUA.batchxml_gen(param, path .. filename .. ".xml")
    flag = ACQUA.batchbat_gen(path, filename)

    return filename
end

ACQUA[ [==[TOSQA]==] ] = function(processed, clean, path, param, existFileName)
    local flag
    local filename = "batch_TMOS"
    if existFileName then
        lfs.copy(path .. existFileName .. ".txt", path .. filename .. ".txt")
    else
        -- ACQUA data write
        local fileList = {}
        fileList[1] = "VoiceQuality.dat"
        ACQUA.batchdat_gen(processed, processed, clean, path .. fileList[1])
        flag = ACQUA.batchtxt_gen(fileList, path .. filename .. ".txt")
    end
    flag = ACQUA.batchxml_gen(param, path .. filename .. ".xml")
    flag = ACQUA.batchbat_gen(path, filename)

    return filename
end

ACQUA[ [==[POLQA]==] ] = function(processed, clean, path, param, existFileName)
    local flag
    local filename = "batch_POLQA"
    if existFileName then
        lfs.copy(path .. existFileName .. ".txt", path .. filename .. ".txt")
    else
        -- ACQUA data write
        local fileList = {}
        fileList[1] = "VoiceQuality.dat"
        ACQUA.batchdat_gen(processed, processed, clean, path .. fileList[1])
        flag = ACQUA.batchtxt_gen(fileList, path .. filename .. ".txt")
    end
    flag = ACQUA.batchxml_gen(param, path .. filename .. ".xml")
    flag = ACQUA.batchbat_gen(path, filename)

    return filename
end

ACQUA.DT_note = [==[
[9:59:29] Jun Wang: echo需要从I2S灌还是从PDM，需要tony定好
[9:59:36] Jun Wang: 如果从PDM灌没有问题
[9:59:40] Timmy: 因为他们的code一会从PDM0左和PDM1左送入，一会从PDM0左右送入。
[9:59:51] Timmy: 不同的code送入方式不同。
[10:00:17] Jun Wang: 如果从I2S灌的话目前的UIF无法实现
[10:00:32] Timmy: 现在倒是明确都从PDM灌入信号。
[10:01:54] Jun Wang: 现在的脚本是只支持PDM灌数据的
[10:02:23] Jun Wang: 如果要I2S和PDM同时的话比较麻烦
[10:04:29] Jun Wang: 我觉得目前先通过设置501，把MIC和ECHO都从PDM走
[10:05:01] Jun Wang: 先这样把整个流程弄通
[10:06:16] Jun Wang: 他们需要知道
[10:06:27] Jun Wang: 这个代码和其他的测试项没有任何区别
[10:06:59] Jun Wang: 走PDM不需要改代码
]==]