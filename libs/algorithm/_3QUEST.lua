algorithm  = algorithm or {}
algorithm.__help  = algorithm.__help or {}

require "libs.algorithm.ACQUA"
function algorithm.__help._3QUEST()
    local str = ""
    str = str .. const.str.functionDividerStart
    str = str .. [==[function algorithm._3QUEST(processed, unProcessed, clean, param)]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Usage:]==] .. "\n"
    str = str .. "\t" .. [==[GMOS, SMOS, NMOS = algorithm._3QUEST(processed, unProcessed, clean, param)]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Examples:]==] .. "\n"
    str = str .. const.str.functionLineDivider
    str = str .. "\t" .. [==[param = {]==] .. "\n"
    str = str .. "\t" .. [==[    BandWidthMode = "NB", -- "NB or "WB"]==] .. "\n"
    str = str .. "\t" .. [==[    FilterIRSRCV = true,  -- true or false]==] .. "\n"
    str = str .. "\t" .. [==[    TimeRange = {]==] .. "\n"
    str = str .. "\t" .. [==[        Start = 1.2,      -- time in second]==] .. "\n"
    str = str .. "\t" .. [==[        Duration = 6.5,   -- time in second]==] .. "\n"
    str = str .. "\t" .. [==[        NbrOfRanges = 16  -- interger number > 0]==] .. "\n"
    str = str .. "\t" .. [==[    },]==] .. "\n"
    str = str .. "\t" .. [==[}]==] .. "\n"
    str = str .. "\t" .. [==[GMOS, SMOS, NMOS = algorithm._3QUEST(processed, unProcessed, clean, param)]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Input:]==] .. "\n"
    str = str .. "\t" .. [==[processed   -- processed wavedata, should be mono]==] .. "\n"
    str = str .. "\t" .. [==[unProcessed -- unProcessed wavedata, should be mono]==] .. "\n"
    str = str .. "\t" .. [==[clean       -- clean wavedata, should be mono]==] .. "\n"
    str = str .. "\t" .. [==[param       -- parameter table]==] .. "\n"
    str = str .. const.str.functionBlockDivider
    str = str .. [==[Output:]==] .. "\n"
    str = str .. "\t" .. [==[GMOS        -- GMOS Value]==] .. "\n"
    str = str .. "\t" .. [==[SMOS        -- SMOS Value]==] .. "\n"
    str = str .. "\t" .. [==[NMOS        -- NMOS Value]==] .. "\n"
    str = str .. const.str.functionDividerEnd

    print(str)
end

local errorMessage = {
    [1]  = [==[No valid parameter table]==],
    [2]  = [==[No valid param.BandWidthMode, should be: "NB" or "WB"]==],
    [3]  = [==[No valid param.FilterIRSRCV, should be boolean value: true or false]==],
    [4]  = [==[No valid param.TimeRange, should be a table]==],
    [5]  = [==[No valid param.TimeRange.Start, should be a number of time in s(>=0)]==],
    [6]  = [==[No valid param.TimeRange.Duration, should be a number of time in s(>0)]==],
    [7]  = [==[No valid param.TimeRange.NbrOfRanges, should be a nature number (int>=1)]==]
}
local function paramProcess(param)
    -- check parameter loss
    if "table" ~= type(param) then return 1 end
    if nil == param.BandWidthMode         or "string" ~= type(param.BandWidthMode)         then return 2 end
    if nil == param.FilterIRSRCV          or "boolean" ~= type(param.FilterIRSRCV)         then return 3 end
    if nil == param.TimeRange             or "table" ~= type(param.TimeRange)              then return 4 end
    if nil == param.TimeRange.Start       or "number" ~= type(param.TimeRange.Start)       then return 5 end
    if nil == param.TimeRange.Duration    or "number" ~= type(param.TimeRange.Duration)    then return 6 end
    if nil == param.TimeRange.NbrOfRanges or "number" ~= type(param.TimeRange.NbrOfRanges) then return 7 end

    -- value check
    if  "NB" == string.upper(param.BandWidthMode) or "WB" == string.upper(param.BandWidthMode) then
        param.BandWidthMode = string.upper(param.BandWidthMode)
    else
        return 2
    end

    if param.TimeRange.Start < 0 then return 5 end
    if param.TimeRange.Duration <= 0 then return 6 end
    if param.TimeRange.NbrOfRanges < 1 or (math.floor(param.TimeRange.NbrOfRanges) ~= param.TimeRange.NbrOfRanges) then return 7 end

    return param
end

local function waveCheck(processed, unProcessed, clean, param)
    -- check samplerate
    local sample_rate_processed = Wave.get_samplerate(processed)
    if nil == sample_rate_processed then
        notice.message_lib("algorithm._3QUEST", "Invalid processed input", "Error")
        return false
    end
    local sample_rate_unProcessed = Wave.get_samplerate(unProcessed)
    if nil == sample_rate_unProcessed then
        notice.message_lib("algorithm._3QUEST", "Invalid unProcessed input", "Error")
        return false
    end
    local sample_rate_clean = Wave.get_samplerate(clean)
    if nil == sample_rate_clean then
        notice.message_lib("algorithm._3QUEST", "Invalid clean input", "Error")
        return false
    end
    if sample_rate_clean ~= sample_rate_processed or sample_rate_clean ~= sample_rate_unProcessed then
        notice.message_lib("algorithm._3QUEST", "processed, unProcessed, clean do not have the same sample rate", "Error")
        return false
    end
    if sample_rate_clean < 16000 and "WB" == param.BandWidthMode then
        notice.message_lib("algorithm._3QUEST", "BandWidthMode set error, only narrowband data input", "Error")
        return false
    end

    -- check wave length
    local sampleLen_processed = Wave.get_samplenum(processed)
    local sampleLen_unProcessed = Wave.get_samplenum(unProcessed)
    local sampleLen_clean = Wave.get_samplenum(clean)
    if sampleLen_clean ~= sampleLen_processed or sampleLen_clean ~= sampleLen_unProcessed then
        notice.message_lib("algorithm._3QUEST", "processed, unProcessed, clean do not have the same sample length", "Error")
        return false
    end
    if (param.TimeRange.Start + param.TimeRange.Duration)*sample_rate_clean > sampleLen_clean then
        notice.message_lib("algorithm._3QUEST", "TimeRange.Start or TimeRange.Duration set error, TimeRange.Start+TimeRange.Duration > wave length", "Error")
        return false
    end

    return true
end
--[[
param = {
    BandWidthMode = "NB", -- "NB or "WB"
    FilterIRSRCV = true,  -- true or false
    TimeRange = {
        Start = 1.2,      -- time in second
        Duration = 6.5,   -- time in second
        NbrOfRanges = 16  -- interger number > 0
    },
}
--]]
function algorithm._3QUEST(processed, unProcessed, clean, param)
    param = paramProcess(param)
    if "number" == type(param) then
        notice.message_lib("algorithm._3QUEST", errorMessage[param], "Error")
        return false
    end

    if false == waveCheck(processed, unProcessed, clean, param) then
        return false
    end

    -- calculation
    local GMOS, SMOS, NMOS = ACQUA._3QUEST(processed, unProcessed, clean, param)

    return GMOS, SMOS, NMOS
end