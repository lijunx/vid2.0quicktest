"""
txt2bin_dspout
utility for txt to bin conversion of DSP output file
invoking binascii.a2b_hex to convert ASCII text string into binary

Apr 2015  adopted for VOS project
"""
import sys
import binascii

# .txt file
h = open(sys.argv[1])
# .bin file
b = open(sys.argv[2],'wb') 
line = h.readline()

while line:
    line = line.strip()
    my_bin = binascii.a2b_hex(line)
    b.write(my_bin)
    line = h.readline()

h.close()
b.close()


    

    
    
    
