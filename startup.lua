-- basic libs load
require "libs.lfs"
NOAH_PATH = lfs.currentdir().."\\" -- get working folder
DEBUG_PATH = NOAH_PATH.."debug_info\\"

require "bit"
require "iuplua"
require "libs.LuaXML"
require "libs.luacom"
require "libs.notice"
require "libs.math"
require "libs.table"
require "libs.control"
require "libs.Wave"
require "libs.batchcalctool"
require "libs.device"

GLOBAL = GLOBAL or {}
GLOBAL.DEBUG = false
GLOBAL.DEBUG_V2 = false
GLOBAL.DEBUG_V2_PLATFORM_OFFLINE = false
GLOBAL.DEBUG_V2_RECORD_OFFLINE = false
if GLOBAL.DEBUG_V2_PLATFORM_OFFLINE then GLOBAL.DEBUG_V2_RECORD_OFFLINE = true end

-- Notice information select, if not set at startup, default is all true
notice.cmdEN = true
notice.logEN = true
notice.uiEN = false
notice.LEVEL = 1

lfs.del(NOAH_PATH, "*.log")
