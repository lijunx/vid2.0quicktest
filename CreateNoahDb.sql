USE [NoahDB]
GO
/****** Object:  User [NoahTester]    Script Date: 08/04/2008 19:02:56 ******/
CREATE USER [NoahTester] FOR LOGIN [NoahTester] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[DataType]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataType](
	[DataTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[DataType] [nchar](15) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DataType] PRIMARY KEY CLUSTERED 
(
	[DataTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestItem]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestItem](
	[TestItemID] [smallint] IDENTITY(1,1) NOT NULL,
	[ItemName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_TestItem] PRIMARY KEY CLUSTERED 
(
	[TestItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestRecord]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestRecord](
	[TestID] [int] IDENTITY(1,1) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[Tester] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_TestRecord] PRIMARY KEY CLUSTERED 
(
	[TestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table summarizes the information of each test run. A test run means a tester performs a long term testing, which may include loops of many test items.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TestRecord'
GO
/****** Object:  Table [dbo].[Variant]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Variant](
	[VarID] [int] IDENTITY(1,1) NOT NULL,
	[VarName] [nvarchar](50) NOT NULL,
	[Type] [tinyint] NULL,
	[Description] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Variant] PRIMARY KEY CLUSTERED 
(
	[VarID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Data]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Data](
	[DataID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [int] NOT NULL,
	[BelongToTest] [int] NOT NULL,
	[GenerateTime] [datetime] NOT NULL,
	[TestLoopIndex] [int] NOT NULL,
	[TestItem] [smallint] NOT NULL,
 CONSTRAINT [PK_Data] PRIMARY KEY CLUSTERED 
(
	[DataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wav]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wav](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataId] [int] NOT NULL,
	[Value] [nchar](32) NOT NULL,
 CONSTRAINT [PK_Wav] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NumberArray]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NumberArray](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataId] [int] NOT NULL,
	[ArraySize] [int] NOT NULL,
	[Value] [image] NOT NULL,
 CONSTRAINT [PK_NumberArray] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[String]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[String](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataId] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_String] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Number]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Number](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataId] [int] NOT NULL,
	[Value] [float] NOT NULL,
 CONSTRAINT [PK_Number] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataId] [int] NOT NULL,
	[Addr] [int] NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_DM] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[new_data]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[new_data] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50),		--Variant name.
	@var_type	nchar(15)			--Variant data type
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	declare @varid	int
	declare @typeid	int

	--get type id
	select @typeid = datatypeid  from datatype where datatype = @var_type
	if @@ROWCOUNT = 0
	begin
		RaisError('Invalid data type: %s', 11, 1,  @var_type)
		return 0
	end	

	declare @testitemid int
	select @testitemid=testitemid  from testitem where itemname = @test
	if @@ROWCOUNT = 0  --No matched record 
	begin
		insert into testitem 
		(itemname, description)
		values
		(@test, '')
		select @testitemid = SCOPE_IDENTITY()  
	end

	select @varid =varid  from variant where varname = @var_name and type=@typeid
	if @@ROWCOUNT = 0
	begin
		update variant
		set type = @typeid
		where varname = @var_name and type is null

		if @@ROWCOUNT = 0
		begin
			insert into variant 
			(varname, type, description)
			values
			(@var_name, @typeid, '')

			if @@ERROR <> 0
			begin
				RaisError('Conflict variant name and type: %s ---%s', 11, 1, @var_name, @var_type)
				return 0
			end
			select @varid = SCOPE_IDENTITY()  ---save the new var id
		end
		else begin
			select @varid =varid  from variant where varname = @var_name and type=@typeid
		end
	end

	declare @dataid int
	select @dataid=dataid from data
	where value=@varid and belongtotest=@test_run and testloopindex=@loop and testitem=@testitemid

	if @@ROWCOUNT = 0
	begin
		insert into data 
		(value, belongtotest, generatetime, testloopindex, testitem)
		values( @varid, @test_run, GETDATE(), @loop, @testitemid)
		set @dataid = SCOPE_IDENTITY()
	end
	else
	begin
		update data
		set generatetime = GETDATE()
		where dataid=@dataid
	end

	return @dataid
END
GO
/****** Object:  StoredProcedure [dbo].[query_data]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[query_data] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50)		--Variant name.
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @test_run = 0
	begin
		select top 1 @test_run = testid from testrecord order by testid desc
		if @@ROWCOUNT = 0  --No matched record 
			return	-- no record
	end

	declare @data_id int
	declare @var_type nvarchar(15)
	select @data_id=dataid, @var_type=datatype from variant, datatype, data, testitem
	where varname=@var_name and itemname=@test and type=datatypeid
		and value=varid and belongtotest=@test_run
		and testloopindex=@loop and testitem=testitemid
	
	if @@ROWCOUNT=0
	begin
		return
	end
	else
	begin
		if @var_type='dm'
			select  Value, Addr  from DM where dataid=@data_id
		else if @var_type='numberarray'
			select  Value, ArraySize from numberarray where dataid=@data_id
		else if @var_type='string'
			select  Value from string where dataid=@data_id
		else if @var_type='number'
			select  Value from number where dataid=@data_id
		else if @var_type='wav'
			select  Value from wav where dataid=@data_id
	end	

END
GO
/****** Object:  StoredProcedure [dbo].[define_test_item]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[define_test_item] 
	-- Add the parameters for the stored procedure here
	@item     nvarchar(50), 
	@desc	  nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update testitem 
	set description = @desc
	where itemname = @item
	
	if @@ROWCOUNT = 0
		insert into testitem 
		(itemname, description)
		values
		(@item, @desc)
END
GO
/****** Object:  StoredProcedure [dbo].[delete_test_run]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_test_run] 
	-- Add the parameters for the stored procedure here
	@test_run	int				--test run id
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @test_run = 0
	begin
		select top 1 @test_run = testid from testrecord order by testid desc
		if @@ROWCOUNT = 0  --No matched record 
		begin
			select @test_run -- return deleted test run id
			return	-- no record
		end
	end
	
	declare @dataid table(
		dataid int NOT NULL
	);

	insert into @dataid (dataid) 
	select dataid from data 
	where belongtotest = @test_run

	
	delete from DM where DataId in (select dataid from @dataid)
	delete from Number where DataId in (select dataid from @dataid)
	delete from String where DataId in (select dataid from @dataid)
	delete from NumberArray where DataId in (select dataid from @dataid)
	delete from Wav where DataId in (select dataid from @dataid)
	
	delete from Data where DataId in (select dataid from @dataid)
	delete from testrecord where testid = @test_run
	
	select @test_run -- return deleted test run id
END
GO
/****** Object:  StoredProcedure [dbo].[new_test_run]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[new_test_run] 
	-- Add the parameters for the stored procedure here
	@tester  nvarchar(30), 
	@desc	  nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into testrecord (starttime, tester, description) values (GETDATE(), @tester, @desc) 
	select cur_idx = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[define_variant]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[define_variant] 
	-- Add the parameters for the stored procedure here
	@var      nvarchar(50), 
	@desc	  nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update variant 
	set description = @desc
	where varname = @var
	
	if @@ROWCOUNT = 0
		insert into variant 
		(varname, description)
		values( @var, @desc)
END
GO
/****** Object:  StoredProcedure [dbo].[save_wav]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_wav] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50),		--Variant name.
	@fileid		int					--file id value
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION 
	declare @data_id int
	execute @data_id=new_data @test_run, @loop, @test, @var_name, 'wav'
	
	declare @Orgval		nvarchar(50)
	select @Orgval=value from wav where dataid = @data_id
	update wav
	set value=CAST(@test_run AS varchar(15))+', '+CAST(@fileid AS varchar(15))
	where dataid = @data_id
	if @@ROWCOUNT = 0
	begin
		insert into wav
		(dataid, value)
		values
		(@data_id, CAST(@test_run AS varchar(15))+', '+CAST(@fileid AS varchar(15)) )
	end

	If @@ERROR=0 and @data_id<>0
	begin
		commit
	end
	else
	begin
		rollback
		RaisError('Save wav "%d:%d" failed!', 11, -1, @test_run, @fileid)
	end	
	
	select @Orgval -- return the origin value
END
GO
/****** Object:  StoredProcedure [dbo].[save_numberarray]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_numberarray] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50),		--Variant name.
	@arraysize	int,				--array size
	@val		image				--number value
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION 
	declare @data_id int
	execute @data_id=new_data @test_run, @loop, @test, @var_name, 'numberarray'
	
	if exists (select * from numberarray where dataid = @data_id)
	begin
		update numberarray  
		set value = @val , arraysize=@arraysize
		where dataid = @data_id  
	end
	else
	begin
		insert into numberarray
		(dataid, arraysize, value)
		values
		(@data_id, @arraysize, @val)
	end

	If @@ERROR=0 and @data_id<>0
	begin
		commit
	end
	else
	begin
		rollback
		RaisError('Save numberarray failed!', 11, -1)
	end	

END
GO
/****** Object:  StoredProcedure [dbo].[save_string]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_string] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50),		--Variant name.
	@val		nvarchar(max)		--string value
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION 
	declare @data_id int
	execute @data_id=new_data @test_run, @loop, @test, @var_name, 'string'

	update string
	set value = @val
	where dataid = @data_id
	if @@ROWCOUNT = 0
	begin
		insert into string
		(dataid, value)
		values
		(@data_id, @val)
	end

	If @@ERROR=0 and @data_id<>0
	begin
		commit
	end
	else
	begin
		rollback
		RaisError('Save string "%s" failed!', 11, -1, @val)
	end	

END
GO
/****** Object:  StoredProcedure [dbo].[save_number]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_number] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50),		--Variant name.
	@val		float				--number value
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION 
	declare @data_id int
	execute @data_id=new_data @test_run, @loop, @test, @var_name, 'number'
	
	update number
	set value = @val
	where dataid = @data_id
	if @@ROWCOUNT = 0
	begin
		insert into number
		(dataid, value)
		values
		(@data_id, @val)
	end


	If @@ERROR=0 and @data_id<>0
	begin
		commit
	end
	else
	begin
		rollback
		RaisError('Save number failed!', 11, -1)
	end	

END
GO
/****** Object:  StoredProcedure [dbo].[save_dm]    Script Date: 08/04/2008 19:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[save_dm] 
	-- Add the parameters for the stored procedure here
	@test_run	int,				--test run id
	@loop		int,				--Loop index in the test run.	
	@test		nvarchar(50),       --Test item name.
	@var_name	nvarchar(50),		--Variant name.
	@addr		int,				--DM addr value
	@val		int					--DM value value
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION 
	declare @data_id int
	execute @data_id=new_data @test_run, @loop, @test, @var_name, 'dm'
	
	update DM
	set value=@val
	where dataid = @data_id
	if @@ROWCOUNT = 0
	begin
		insert into DM
		(dataid, addr, value)
		values
		(@data_id, @addr, @val)
	end

	If @@ERROR=0 and @data_id<>0
	begin
		commit
	end
	else
	begin
		rollback
		RaisError('Save DM addr=%d, val=%d failed!', 11, -1, @addr, @val)
	end	

END
GO
/****** Object:  Check [CK_DM]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[DM]  WITH CHECK ADD  CONSTRAINT [CK_DM] CHECK  (([addr]>=(0) AND [addr]<=(65535) AND ([value]>=(0) AND [value]<=(65535))))
GO
ALTER TABLE [dbo].[DM] CHECK CONSTRAINT [CK_DM]
GO
/****** Object:  Check [CK_NumberArray]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[NumberArray]  WITH CHECK ADD  CONSTRAINT [CK_NumberArray] CHECK  (([ArraySize]>=(1)))
GO
ALTER TABLE [dbo].[NumberArray] CHECK CONSTRAINT [CK_NumberArray]
GO
/****** Object:  ForeignKey [FK_Data_TestItem]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[Data]  WITH CHECK ADD  CONSTRAINT [FK_Data_TestItem] FOREIGN KEY([TestItem])
REFERENCES [dbo].[TestItem] ([TestItemID])
GO
ALTER TABLE [dbo].[Data] CHECK CONSTRAINT [FK_Data_TestItem]
GO
/****** Object:  ForeignKey [FK_Data_TestRecord]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[Data]  WITH CHECK ADD  CONSTRAINT [FK_Data_TestRecord] FOREIGN KEY([BelongToTest])
REFERENCES [dbo].[TestRecord] ([TestID])
GO
ALTER TABLE [dbo].[Data] CHECK CONSTRAINT [FK_Data_TestRecord]
GO
/****** Object:  ForeignKey [FK_Data_Variant]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[Data]  WITH CHECK ADD  CONSTRAINT [FK_Data_Variant] FOREIGN KEY([Value])
REFERENCES [dbo].[Variant] ([VarID])
GO
ALTER TABLE [dbo].[Data] CHECK CONSTRAINT [FK_Data_Variant]
GO
/****** Object:  ForeignKey [FK_DM_Data]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[DM]  WITH CHECK ADD  CONSTRAINT [FK_DM_Data] FOREIGN KEY([DataId])
REFERENCES [dbo].[Data] ([DataID])
GO
ALTER TABLE [dbo].[DM] CHECK CONSTRAINT [FK_DM_Data]
GO
/****** Object:  ForeignKey [FK_Number_Data]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[Number]  WITH CHECK ADD  CONSTRAINT [FK_Number_Data] FOREIGN KEY([DataId])
REFERENCES [dbo].[Data] ([DataID])
GO
ALTER TABLE [dbo].[Number] CHECK CONSTRAINT [FK_Number_Data]
GO
/****** Object:  ForeignKey [FK_NumberArray_Data]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[NumberArray]  WITH CHECK ADD  CONSTRAINT [FK_NumberArray_Data] FOREIGN KEY([DataId])
REFERENCES [dbo].[Data] ([DataID])
GO
ALTER TABLE [dbo].[NumberArray] CHECK CONSTRAINT [FK_NumberArray_Data]
GO
/****** Object:  ForeignKey [FK_String_Data]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[String]  WITH CHECK ADD  CONSTRAINT [FK_String_Data] FOREIGN KEY([DataId])
REFERENCES [dbo].[Data] ([DataID])
GO
ALTER TABLE [dbo].[String] CHECK CONSTRAINT [FK_String_Data]
GO
/****** Object:  ForeignKey [FK_Variant_DataType]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[Variant]  WITH CHECK ADD  CONSTRAINT [FK_Variant_DataType] FOREIGN KEY([Type])
REFERENCES [dbo].[DataType] ([DataTypeID])
GO
ALTER TABLE [dbo].[Variant] CHECK CONSTRAINT [FK_Variant_DataType]
GO
/****** Object:  ForeignKey [FK_Wav_Data]    Script Date: 08/04/2008 19:02:56 ******/
ALTER TABLE [dbo].[Wav]  WITH CHECK ADD  CONSTRAINT [FK_Wav_Data] FOREIGN KEY([DataId])
REFERENCES [dbo].[Data] ([DataID])
GO
ALTER TABLE [dbo].[Wav] CHECK CONSTRAINT [FK_Wav_Data]
GO



/*****grant access right****/
GRANT EXECUTE ON [dbo].[define_test_item] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[define_variant] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[delete_test_run] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[new_test_run] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[query_data] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[save_dm] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[save_number] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[save_numberarray] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[save_string] TO [NoahTester]
GO
GRANT EXECUTE ON [dbo].[save_wav] TO [NoahTester]
GO
/*------------------------------------*/
GRANT VIEW DEFINITION ON [dbo].[define_test_item] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[define_variant] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[delete_test_run] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[new_test_run] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[query_data] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[save_dm] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[save_number] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[save_numberarray] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[save_string] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[save_wav] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[Data] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[DataType] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[DM] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[Number] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[NumberArray] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[String] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[TestItem] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[TestRecord] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[Variant] TO [NoahTester]
GO
GRANT VIEW DEFINITION ON [dbo].[Wav] TO [NoahTester]
GO

/*** grant select right ****/
GRANT SELECT ON [dbo].[Data] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[DataType] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[DM] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[Number] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[NumberArray] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[String] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[TestItem] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[TestRecord] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[Variant] TO [NoahTester]
GO
GRANT SELECT ON [dbo].[Wav] TO [NoahTester]
GO

/** grant DELETE right **/
GRANT DELETE ON [dbo].[TestItem] TO [NoahTester]
GO
GRANT DELETE ON [dbo].[Variant] TO [NoahTester]
GO


/**** insert data type ******/
insert into datatype
(datatype, description)
values
('Wav','The data is a wav file' )
insert into datatype
(datatype, description)
values
('DM','The data is a DSP DM' )
insert into datatype
(datatype, description)
values
('Number','The data is a generic number' )
insert into datatype
(datatype, description)
values
('String','The data is a generic string' )
insert into datatype
(datatype, description)
values
('NumberArray','The data is an array of numbers' )
go


