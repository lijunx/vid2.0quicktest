dofile "SAMFuncs.lua"
dofile "DrawFunctions.lua"

g_VecNames = {"Chinese","American English","English"} --,"Finish","French","German","Italian","Japanese","Polish","Spanish"}
g_AECnames = {"linear AEC","non-linear AEC","total AEC"}
g_NSSpot = {"babble","music","single voice"}
g_NSDiffuse = {"car","medium size car"}

g_NEVoiceQualityResult = {
		{1,2,3,4,5,6,7,8,9,1.1,2.1,3.1,4.1,5.1,6.1,7.1,8.1,9.1,1.2,2.2,3.2,4.2,5.2,6.2,7.2,8.2,9.2},
		{},
		{},
		{1,2,3,4,5,6,7,8}
}

g_NEVoiceQualityDTResult = {
		{1,2,3,4,5,6,7,8,9,1.1,2.1,3.1,4.1,5.1,6.1,7.1,8.1,9.1,1.2,2.2,3.2,4.2,5.2,6.2,7.2,8.2,9.2},
		{},
		{},
		{1,2,3,4,5,6,7,8},
		{5},
		{6},
		{7},
		{8}
}

g_AECDSPParam = {"ffff=ffff"}

g_AECresult = {
	{1,2,3,4,5,6,7,8,9},
	{2},
	{3},
	{4},
	{5},
	{6},
	{7},
	{8}
}

h_p = pdf.createpdf()

x,y = h_p:drawtext(120,260,"SAMSoft Test Report",{fontsize = 30})
x,y = h_p:drawtext(180,y+10,"Date: 2008-10-22"   ,{fontsize = 22})

g_PageHight = h_p:getproperty("pageheight")
--print("Page height: ",g_PageHight)

----------------------------------------------------------------------
--NE Test Report Example
----------------------------------------------------------------------
--Test result table
iPage = 1
h_p:setproperty({fontsize=12,bdrawboxline=false})
PageInc = CreateNEVoiceQualityTable(h_p, iPage, g_VecNames,g_PageHight,g_NEVoiceQualityResult)

--draw Spectrum
WAVE_PATH = "F:\\SolutionGrp\\Noah\\Noah\\1_LG_NE_only.pcm"

h_p:setproperty({fontsize=12,bdrawboxline=false})
iPage = iPage + PageInc
y_pos = iPage*g_PageHight + 30
x_pos, y_pos = h_p:drawtext(6,y_pos,"Spectrum demo")
START_TIME = {21000, 28500}
END_TIME = {25500,33900}
stmdata = wave.load_raw(WAVE_PATH,2,8000)

wavarray = wave.n_to_mono(stmdata)
g_NE_Draw_Num = 2

for i = 1, g_NE_Draw_Num
do
	left_part = wave.cut(wavarray[1],START_TIME[i],END_TIME[i])
	right_part = wave.cut(wavarray[2],START_TIME[i],END_TIME[i])

	left_spec = wave.spectrum(left_part)
	right_spec = wave.spectrum(right_part)

	spctarray = {left_spec, right_spec}
	y_pos = y_pos + 30
	spectxt = string.format("Spectrum of the %dth section:", i)
	x_pos, y_pos = h_p:drawtext(6,y_pos,spectxt)
	y_pos = y_pos + 20
	y_pos = PDF_DrawSpectrum(h_p, spctarray, 4000, "log", y_pos, 3)

end
--draw Wave
iPage = iPage + 1
URI_PATH = PDF_ToLinkURI(WAVE_PATH)
y_pos = iPage*g_PageHight + 30
x_pos, y_pos = h_p:drawtext(6,y_pos,"Wave demo")
y_pos = y_pos + 20
x_pos, y_pos = h_p:drawtext(6,y_pos,"Link to the file is:"..URI_PATH, {link = URI_PATH,fontcolor={0,0,1}})
y_pos = y_pos + 20
PDF_DrawWaveform(h_p, stmdata, y_pos, 0, 1, 0, 10, 100)



--[[
iPage = iPage + PageInc
h_p:setproperty({fontsize=12,bdrawboxline=false})
PageInc = CreateNEVoiceQualityatDTTable(h_p, iPage, g_VecNames,g_PageHight,g_NEVoiceQualityDTResult)

iPage = iPage + PageInc
h_p:setproperty({fontsize=12,bdrawboxline=false})
PageInc = CreateAECTable(h_p, iPage, g_AECDSPParam, g_AECresult,g_PageHight)


iPage = iPage + 1
h_p:setproperty({fontsize=12,bdrawboxline=false})
x,y = h_p:drawtext(8,g_PageHight*iPage+30,"2. Near End Voice Quality at DT Result")
CreateNEVoiceQualityatDTTable(h_p, 10, y+10, g_VecNames)

iPage = iPage + 1
h_p:setproperty({fontsize=12,bdrawboxline=false})
x,y = h_p:drawtext(8,g_PageHight*iPage+30,"3. AEC Test Result")
CreateAECTable(h_p, 10, y+10, g_AECnames)

iPage = iPage + 1
h_p:setproperty({fontsize=12,bdrawboxline=false})
x,y = h_p:drawtext(8,g_PageHight*iPage+30,"4. Noise Suppression Test Result")
CreateNSTable(h_p, 80, y+10, g_NSSpot, g_NSDiffuse)
--]]

--save file
h_p:savetofile("SAMSoft.pdf")
