SERVERIP = "192.168.50.19"
TESTROUND = 2

FPGA_RUN_SCRIPT = 0
FPGA_GET_FILE = 1
FPGA_STOP_TEST = 2
FPGA_WAIT_DIR_NAME = 3
FPGA_WAIT_FILE_NAME = 4
FPGA_END_DIR = 5
FPGA_END_TEST = 6

function Test3020FPGA(filename)
	while(1)
	do
		dp = mcu.get_gpio(9)
		if (dp == 1)
		then
			break
		end
		wait(3)
		print("wait for downloading param")
	end

	wait(21)

	print("downloading param to fpga...")

	mcu.write_dm(0x2308, 0x61ff)
	wait(0.3)
	mcu.write_dm(0x230c, 0)
	wait(1)
	a = mcu.read_dm(0x230c)
	if(a == 0x5a5a)
	then
		print("donw1 ok")
	else
		print("a is ", a)
		error("downloading fail")
	end

	a = mcu.read_dm(0x2313)
	b = mcu.read_dm(0x2313)
	if(a < b)
	then
		print("downloading param OK")
	end

	print("waiting for linux script over...")

	wait(80)
	while(1)
	do
		dp = mcu.get_gpio(11)
		if (dp == 1)
		then
			break;
		end
		control.sleep(3)
		print("wait for transferring file")
	end

	print("    geting file ",filename )
	fpga.get_file(filename)
	wait(1)

	--fpga.separate_file(filename,2)
end


print("Initatizing mcu")
mcu.mcu_initialize(1)
print("Initatizing GPIO")
mcu.set_gpio({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},2)
print("Initatizing ok")
wait(0.5)


fpga.initialize()
wait(1)

fpga.connecto(SERVERIP)
wait(2)
fpga.start_fpga()
while true
do
	l_notify = fpga.get_notify()
	if (l_notify == FPGA_END_TEST)
	then
		print("End of whole test")
		break
	elseif (type(l_notify) == "string")
	then
		l_dirname = l_notify
		print("  Testing ",l_notify," directory")
		i = 0

		while true
		do
			i = i + 1
			ll_notify = fpga.get_notify()
			if (ll_notify == FPGA_END_DIR)
			then
				print("  End of ",l_dirname," dir")
				break
			elseif (type(ll_notify) == "string")
			then
				print("    Testing ",ll_notify)
				l_filename = string.format("%s\\%s",l_dirname,ll_notify)

				Test3020FPGA(l_filename)

			else
				error("Err or found when getting file name")
			end
		end

	else
		error("Err or found when getting dir name")
	end
end

fpga.stop_test()

