
--Function: Create Near End Voice Quality Table
--h_pdf: pdf handle
--pagenum: on which page this table will be drawn from
--vecname: the table of test vector names
--pagesize: The height of the page
--result: PESQ result table
function CreateNEVoiceQualityTable(h_pdf, pagenum, vecname, pagesize,result)
	l_cost_page = 1
	local l_NEVQ_Num = 0
	local l_CellHight = 15
	local l_CellWidth = 60

	local l_LangHight = l_CellHight*9
	local l_LangWidth = l_CellWidth

	local l_TitleHight = l_CellHight

	local l_ConstString = {
			"Note: LR external----external mode and the 2 FE speakers are placed at the left and right side of DUT",
			"LB external----external mode and the 2 FE speakers are placed at the left and ",
			"BB external----external mode and the 2 FE speakers are placed at the back side of DUT"
	}
	xpos, ypos = h_pdf:drawtext(8,pagesize*pagenum+20,"Near End Voice Quality Result")
	xpos = 80
	ypos = ypos + 10
	if type(vecname) ~= "table"  then error("Test vector name not found") end
	l_NEVQ_Num = #vecname
	h_pdf:setproperty({linewidth=0.5,fontsize=7,bdrawboxline=true})
	xpos1,ypos1 = h_pdf:drawtext(xpos, ypos,"Language",{width=l_LangWidth*2,height=l_TitleHight*2,font="Helvetica-Bold",horizonalign="center",verticalalign="middle"})
	xpos2,ypos2 = h_pdf:drawtext(xpos1,ypos,"mockup setting",{width=l_CellWidth*4,height=l_TitleHight,font="Helvetica-Bold",horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1,			 ypos2,"internal mode",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth  ,ypos2,"LR external",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*2,ypos2,"LB external",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*3,ypos2,"BB external",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})

	subvec = {"F11","F12","F21","F22","M11","M12","M21","M22","Average"}
	iY = ypos1
	for i=1, l_NEVQ_Num
	do
		subY = iY
		iX, iY = h_pdf:drawtext(xpos,iY,vecname[i],{width=l_LangWidth,height=l_LangHight,horizonalign="center",verticalalign="middle"})
		l_subNum = #subvec
		for j=1, l_subNum
		do
			jX,jY = h_pdf:drawtext(iX,subY + (j-1)*l_CellHight,subvec[j],{width=l_LangWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
			for k = 1, 4
			do
				l_NumID = (i-1)*l_subNum + j
				if(result[k][l_NumID] == nil)
				then
					l_rslt = ""
				else
					l_rslt = string.format("%.2f",result[k][l_NumID])
				end
				jX,jY = h_pdf:drawtext(jX,subY + (j-1)*l_CellHight,l_rslt,{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
			end
		end
		l_occupy = iY % pagesize
		if (l_occupy + l_LangHight > pagesize - 20 )	--Next page
		then
			iY = (l_cost_page+pagenum)*pagesize+20
			l_cost_page = l_cost_page + 1
		end
	end

	local l_TotalWidth = l_LangWidth*2 + l_CellWidth*4
	local l_NoteHight = 30
	for t=1, 3
	do
		h_pdf:drawtext(xpos,iY+(t-1)*l_NoteHight,l_ConstString[t],{fontsize=9,width=l_TotalWidth,height=l_NoteHight,horizonalign="left",verticalalign="middle"})
	end

	return l_cost_page

end

--Function: Create Near End Voice Quality at DT Table
--h_pdf: pdf handle
--pagenum: on which page this table will be drawn from
--vecname: the table of test vector names
--pagesize: The height of the page
--result: PESQ result table
function CreateNEVoiceQualityatDTTable(h_pdf, pagenum, vecname, pagesize,result)
	local l_NEVQ_Num = 0
	local l_CellHight = 15
	local l_CellWidth = 50

	local l_LangHight = 75
	local l_LangWidth = 45

	local l_TitleHight = 15
	local l_ConstString = {
			"Note: LR external----external mode and the 2 FE speakers are placed at the left and right side of DUT",
			"LB external----external mode and the 2 FE speakers are placed at the left and back side of DUT",
			"BB external----external mode and the 2 FE speakers are placed at the back side of DUT"
	}

	l_cost_page = 1
	local l_NEVQ_Num = 0
	local l_CellHight = 15
	local l_CellWidth = 50

	local l_LangHight = l_CellHight*9
	local l_LangWidth = l_CellWidth

	local l_TitleHight = l_CellHight

	local l_ConstString = {
			"Note: LR external----external mode and the 2 FE speakers are placed at the left and right side of DUT",
			"LB external----external mode and the 2 FE speakers are placed at the left and ",
			"BB external----external mode and the 2 FE speakers are placed at the back side of DUT"
	}
	xpos, ypos = h_pdf:drawtext(6,pagesize*pagenum+20,"Near End Voice Quality at 3	Double Talk Result")
	xpos = 8
	ypos = ypos + 10

	if type(vecname) ~= "table"  then error("Test vector name not found") end
	l_NEVQ_Num = #vecname
	h_pdf:setproperty({linewidth=0.5,fontsize=7,bdrawboxline=true})
	xpos1,ypos1 = h_pdf:drawtext(xpos, ypos,"NE Language",{width=l_LangWidth*2,height=l_TitleHight*3,font="Helvetica-Bold",horizonalign="center",verticalalign="middle"})
	xpos2,ypos2 = h_pdf:drawtext(xpos1,ypos,"mockup setting",{width=l_CellWidth*8,height=l_TitleHight,font="Helvetica-Bold",horizonalign="center",verticalalign="middle"})

	h_pdf:drawtext(xpos1				,ypos2,"internal mode",{width=l_CellWidth*2,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*2  ,ypos2,"LR external",  {width=l_CellWidth*2,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*4  ,ypos2,"LB external",  {width=l_CellWidth*2,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	xpos3,ypos3 = h_pdf:drawtext(xpos1+l_CellWidth*6,ypos2,"BB external",{width=l_CellWidth*2,height=l_TitleHight,horizonalign="center",verticalalign="middle"})

	h_pdf:drawtext(xpos1,			   ypos3,"normal FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth  ,ypos3,"continuous FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*2,ypos3,"normal FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*3,ypos3,"continuous FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*4,ypos3,"normal FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*5,ypos3,"continuous FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*6,ypos3,"normal FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos1+l_CellWidth*7,ypos3,"continuous FE",{width=l_CellWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})



	subvec = {"F11","F12","F21","F22","M11","M12","M21","M22","Average"}
	iY = ypos1
	for i=1, l_NEVQ_Num
	do
		subY = iY
		iX, iY = h_pdf:drawtext(xpos,iY,vecname[i],{width=l_LangWidth,height=l_LangHight,horizonalign="center",verticalalign="middle"})
		l_subNum = #subvec
		for j=1, l_subNum
		do
			jX,jY = h_pdf:drawtext(iX,subY + (j-1)*l_CellHight,subvec[j],{width=l_LangWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
			for k = 1, 8
			do
				l_NumID = (i-1)*l_subNum + j
				if(result[k][l_NumID] == nil)
				then
					l_rslt = ""
				else
					l_rslt = string.format("%.2f",result[k][l_NumID])
				end
				jX,jY = h_pdf:drawtext(jX,subY + (j-1)*l_CellHight,l_rslt,{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
			end
		end
		l_occupy = iY % pagesize
		if (l_occupy + l_LangHight > pagesize - 20 )	--Next page
		then
			iY = (l_cost_page+pagenum)*pagesize+20
			l_cost_page = l_cost_page + 1
		end
	end

	local l_TotalWidth = l_LangWidth*2 + l_CellWidth*8
	local l_NoteHight = 30
	for t=1, 3
	do
		h_pdf:drawtext(xpos,iY+(t-1)*l_NoteHight,l_ConstString[t],{fontsize=9,width=l_TotalWidth,height=l_NoteHight,horizonalign="left",verticalalign="middle"})
	end

	return l_cost_page

end

--Function: Create AEC result Table
--h_pdf: pdf handle
--pagenum: on which page this table will be drawn from
--param: table containing the DSP parameters
--result: AEC result table
--pagesize:
function CreateAECTable(h_pdf, pagenum, DSPParam, result, pagesize)--(h_pdf, xpos, ypos, aecname)
	local l_AECWidth = 45
	local l_AECHight = 45
	local l_ParamWidth = 50
	local l_ParamHight = 45
	local l_CellWidth = 50
	local l_CellHight = 15
	local l_ParamNum  = 3
	local l_ConstString = {"Note: LR external----external mode and the 2 FE speakers are placed at the left and right side of DUT"
	,"LB external----external mode and the 2 FE speakers are placed at the left and back side of DUT"
	,"BB external----external mode and the 2 FE speakers are placed at the back side of DUT"}

	local l_aecname = {"linear AEC","non-linear AEC","total AEC"}
	local l_AECNum = #l_aecname
	local l_StringNum = #l_ConstString

	xpos, ypos = h_pdf:drawtext(6,pagesize*pagenum+20,"AEC Test Result")
	xpos = 8
	ypos = ypos + 10

	h_pdf:setproperty({linewidth=0.5,fontsize=7,bdrawboxline=true})

	xpos1,ypos1 = h_pdf:drawtext(xpos,  ypos,"",{width=l_AECWidth,height=l_ParamHight})
	xpos2,ypos2 = h_pdf:drawtext(xpos1, ypos,"parameters setting",{width=l_ParamWidth,height=l_ParamHight,font="Helvetica-Bold",horizonalign="center",verticalalign="middle"})
	xpos3,ypos3 = h_pdf:drawtext(xpos2, ypos,"mockup setting",{width=l_CellWidth*8,height=l_CellHight,font="Helvetica-Bold",horizonalign="center",verticalalign="middle"})

	h_pdf:drawtext(xpos2				,ypos3,"internal mode",{width=l_CellWidth*2,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*2  ,ypos3,"LR external",  {width=l_CellWidth*2,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*4  ,ypos3,"LB external",  {width=l_CellWidth*2,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	xpos4,ypos4 = h_pdf:drawtext(xpos2+l_CellWidth*6,ypos3,"BB external",{width=l_CellWidth*2,height=l_CellHight,horizonalign="center",verticalalign="middle"})

	h_pdf:drawtext(xpos2,			   ypos4,"normal FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth  ,ypos4,"continuous FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*2,ypos4,"normal FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*3,ypos4,"continuous FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*4,ypos4,"normal FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*5,ypos4,"continuous FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*6,ypos4,"normal FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
	h_pdf:drawtext(xpos2+l_CellWidth*7,ypos4,"continuous FE",{width=l_CellWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})

	for i=1, l_AECNum
	do
		iX,iY = h_pdf:drawtext(xpos,ypos1+(i-1)*l_AECHight,l_aecname[i],{width=l_AECWidth,height=l_AECHight,horizonalign="center",verticalalign="middle"})
		for j=1, l_ParamNum
		do
			l_DSPParam = (i-1)*l_ParamNum + j
			if(DSPParam[l_DSPParam] == nil)
			then
				paramstr = ""
			else
				paramstr = DSPParam[l_DSPParam]
			end
			h_pdf:drawtext(iX,ypos1+(i-1)*l_AECHight+(j-1)*l_CellHight,paramstr,{width=l_ParamWidth,height=l_CellHight,horizonalign="center",verticalalign="middle"})
		end
		for k=1,8
		do
			if(result[k][i] == nil)
			then
				resultstr = ""
			else
				resultstr = string.format("%.2f",result[i][k])
			end
			h_pdf:drawtext(xpos2+(k-1)*l_CellWidth,ypos2+(i-1)*l_AECHight,resultstr,{width=l_CellWidth,height=l_CellHight*3,horizonalign="center",verticalalign="middle"})
		end
	end

	local l_TotalWidth = l_AECWidth + l_ParamWidth + l_CellWidth*8
	local l_NoteHight = 30
	for t=1, l_StringNum
	do
		h_pdf:drawtext(xpos,iY+(t-1)*l_NoteHight,l_ConstString[t],{fontsize=9,width=l_TotalWidth,height=l_NoteHight,horizonalign="left",verticalalign="middle"})
	end

	return 1
end

--Function: Create noise suppression result Table
--h_pdf: pdf handle
--xpos, ypos: the left top point of the table
--spot: the table of spot noise names
--diffuse: the table of diffuse noise names
function DrawSNR(h_pdf, xpos, ypos, snr_width,np_width,snr_height)
	local l_table = {"0","6","12"}
	for dB=1, 3
	do
		h_pdf:drawtext(xpos,ypos+(dB-1)*snr_height,l_table[dB],{width=snr_width,height=snr_height,horizonalign="center",verticalalign="middle"})
		h_pdf:drawtext(xpos+snr_width,ypos+(dB-1)*snr_height,"",{width=np_width,height=snr_height})
	end
end

function CreateNSTable(h_pdf, xpos, ypos, spot,diffuse)
	local l_TitleHight = 12
	local l_NoiseWidth = 120
	local l_SNRWidth   = 36
	local l_Suppression= 90
	local l_SubNoiseWidth1 = 35
	local l_SubNoiseWidth2 = 35
	local l_SubNoiseWidth3 = 50
	local l_CellHight = 12

	local l_SpotNum = #spot
	local s_DiffuseNum = #diffuse
	local l_SpotHight = 0
	local l_DiffuseHight = 0

	h_pdf:setproperty({linewidth=0.5,fontsize=7,bdrawboxline=true})

	l_x,l_y = h_pdf:drawtext(xpos,ypos,"noise type",{width=l_NoiseWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	l_x,l_y = h_pdf:drawtext(l_x ,ypos,"SNR(dB)",{width=l_SNRWidth,height=l_TitleHight,horizonalign="center",verticalalign="middle"})
	l_x,l_y = h_pdf:drawtext(l_x ,ypos,"noise suppression(dB)",{width=l_Suppression,height=l_TitleHight,horizonalign="center",verticalalign="middle"})

	iX = l_SubNoiseWidth1+xpos
	iY = l_y
	ySNR = iY
	for i=1, l_SpotNum
	do
		if(spot[i] == "babble")
		then
			iX = l_SubNoiseWidth1+xpos
			ySNR = iY
			iX,iY = h_pdf:drawtext(iX,iY,"babble",{width=l_SubNoiseWidth2+l_SubNoiseWidth3,height=l_CellHight*3,horizonalign="center",verticalalign="middle"})
			DrawSNR(h_pdf, iX, ySNR, l_SNRWidth, l_Suppression, l_CellHight)
			l_SpotHight = l_SpotHight+l_CellHight*3
		end

		if(spot[i] == "music")
		then
			iX = l_SubNoiseWidth1+xpos
			ySNR = iY
			iX,iY = h_pdf:drawtext(iX,iY,"music",{width=l_SubNoiseWidth2,height=l_CellHight*12,horizonalign="center",verticalalign="middle"})

			local l_MusicAngle = {"0and 180","45and 225","90and 270","135and 315"}
			iX = l_SubNoiseWidth2+l_SubNoiseWidth1+xpos
			for j=1, 4
			do
				h_pdf:drawtext(iX,ySNR+(j-1)*3*l_CellHight,l_MusicAngle[j],{width=l_SubNoiseWidth3,height=l_CellHight*3,horizonalign="center",verticalalign="middle"})
				DrawSNR(h_pdf, l_NoiseWidth+xpos, ySNR+(j-1)*3*l_CellHight, l_SNRWidth, l_Suppression, l_CellHight)
			end
			l_SpotHight = l_SpotHight+l_CellHight*3*4
		end

		if(spot[i] == "single voice")
		then
			iX = l_SubNoiseWidth1+xpos
			ySNR = iY
			iX,iY = h_pdf:drawtext(iX,iY,"single voice",{width=l_SubNoiseWidth2,height=l_CellHight*3*8,horizonalign="center",verticalalign="middle"})
			local l_VoiceAngle = {"0","45","90","135","180","225","270","315"}

			iX = l_SubNoiseWidth2+l_SubNoiseWidth1+xpos
			for k=1, 8
			do
				h_pdf:drawtext(iX,ySNR+(k-1)*3*l_CellHight,l_VoiceAngle[k],{width=l_SubNoiseWidth3,height=l_CellHight*3,horizonalign="center",verticalalign="middle"})
				DrawSNR(h_pdf, l_NoiseWidth+xpos, ySNR+(k-1)*3*l_CellHight, l_SNRWidth, l_Suppression, l_CellHight)
			end
			l_SpotHight = l_SpotHight+l_CellHight*3*8
		end
	end
	h_pdf:drawtext(xpos,l_y,"spot noise",{width=l_SubNoiseWidth1,height=l_SpotHight,horizonalign="center",verticalalign="middle"})
end

