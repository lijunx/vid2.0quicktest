--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--FILE NAME: DrawFunctions
--Description: this file contains functions associated with drawing 
--Function List:
--		PDF_DrawHorizontalGrid
--		PDF_DrawWaveform
--		PDF_DrawSpectrum
--		PDF_DrawDiagram_Cylinder
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------
--Include Files
--------------------------------------------------
dofile "CalculateFunctions.lua"

--------------------------------------------------
--Function Name: PDF_DrawHorizontalGrid
--------------------------------------------------
--Description: Draw horizontal grid				(Xpos,Ypos)______________________________
--Return Value:	No Return						   ______________________________  |
--Parameter: 								   ______________________________  |
--	Pdf: PDF instance						   ______________________________  Height
--	Xpos, Ypos: Start X and Y position				   __Steps_______________________  |
--	Width, Height: Width and Height					   ______________________________  |
--	Steps: how may gridlines between top and bottom			   ---------- Width -------------
function PDF_DrawHorizontalGrid(Pdf, Xpos, Ypos, Width, Height, Steps)              
	local i = 0
	local property = {linewidth = 0.5}
	local xend = Xpos + Width
	local delta = Height / Steps
	while i <= Height do
		Pdf:drawline(Xpos, Ypos + i, xend, Ypos + i, property)
		i = delta + i
	end
end

--------------------------------------------------
--Function Name: PDF_DrawWaveform
--------------------------------------------------
--Description: Draw waveform
--Return Value: Return the bottom Y position
--Parameter:
--	pdf: PDF instance
--	wave_data: wave data to be drawn
--	Ypos: the start Y position to draw
--	channelnum: the channel that user want to draw, now only can be:
--		0: left channel
--		1: right channel
--		-1: both channel
--	vscale: if the vertical range of wave data is too small, user can use this factor to extrude the vertical size
--	TStart: the start time point of the wave (maybe this wave data is cut from a long wave data)
--	TStep: the time step of the horizontal coordinate
--	Height: the height of the wave form picture
function PDF_DrawWaveform(pdf, wave_data, Ypos, channelnum, vscale, TStart, TStep, Height)
	local Xpos = 40
	local TextWidth = 30
	local Width = 400
	--local Height = 200
	Ypos = PDF_CheckCrossPage(pdf, Ypos, Height)
	--draw background
	pdf:drawrect(Xpos, Ypos, Width, Height, {bfill = true, fillcolor = {0.75,0.75,0.75}})
	--draw horizontal line and time marks
	local TimeLast = wave.get_property(wave_data, "time_last")
	local HMarks = PDF_CalcuStepArray(0, TimeLast, TStep)
	local HScale = Width / TimeLast
	for i = 1, #HMarks - 1 do
		local X = Xpos + HMarks[i] * HScale
		pdf:drawline(X, Ypos, X, Ypos + Height, {linecolor = {0.95, 0.95, 0.95}, linewidth = 0.5})
		pdf:drawline(X, Ypos + Height, X, Ypos + Height + 4)
		local tstr = PDF_ConvertToString(HMarks[i] + TStart, 5)
		pdf:drawtext(X-TextWidth/2, Ypos+Height+4, tstr, {fontsize = 6, width = TextWidth, horizonalign = "center"})
	end
	pdf:drawtext(Xpos + Width + 3, Ypos+Height+4, PDF_ConvertToString(HMarks[#HMarks] + TStart, 5), {fontsize = 6, width = TextWidth})
	local HDStep = HScale / 5 * TStep
	for HDPos = HDStep, Width, HDStep do
		local X = Xpos + HDPos
		pdf:drawline(X, Ypos + Height, X, Ypos + Height + 3, {linewidth = 0.5})
	end
	--draw vertical line and power marks
	local VMarks = {10000 / vscale, 20000 / vscale, 30000 / vscale}
	local MaxData
	if wave.get_property(wave_data, "bit_per_sample") == 16 then MaxData = 32767 / vscale else MaxData = 255 / vscale end
	local VStep = Height / 2 / MaxData * VMarks[1]
	local VCenter = Ypos + Height / 2
	for i = 1, #VMarks do
		pdf:drawline(Xpos+Width, VCenter+VStep*i, Xpos+Width+4,VCenter+VStep*i)
		pdf:drawline(Xpos, VCenter+VStep*i, Xpos+Width,VCenter+VStep*i, {linewidth = 0.5, linecolor = {0.95, 0.95, 0.95}})
		pdf:drawtext(Xpos+Width+5, VCenter+VStep*i-3, PDF_ConvertToString(-VMarks[i], 6), {fontsize = 6})
		pdf:drawline(Xpos+Width, VCenter-VStep*i, Xpos+Width+4,VCenter-VStep*i)
		pdf:drawline(Xpos, VCenter-VStep*i, Xpos+Width,VCenter-VStep*i, {linewidth = 0.5, linecolor = {0.95, 0.95, 0.95}})
		pdf:drawtext(Xpos+Width+5, VCenter-VStep*i-3, PDF_ConvertToString( VMarks[i], 6), {fontsize = 6})
	end
	local VDStep = VStep / 5
	local VDPos = VDStep
	while VDPos < Height / 2 do
		pdf:drawline(Xpos+Width, VCenter+VDPos, Xpos+Width+3, VCenter+VDPos, {linewidth = 0.5})
		pdf:drawline(Xpos+Width, VCenter-VDPos, Xpos+Width+3, VCenter-VDPos, {linewidth = 0.5})
		VDPos = VDPos + VDStep
	end
	--draw waveform
	pdf:drawwaveform(Xpos, Ypos, wave_data, {channel = channelnum, width = Width, height = Height, vscale = vscale})
	--draw boundingbox
	pdf:drawrect(Xpos, Ypos, Width, Height, {bdrawboxline = true})
	pdf:drawline(Xpos, VCenter, Xpos + Width + 4, VCenter)
	pdf:drawtext(Xpos + Width + 5, VCenter - 3, "0", {fontsize = 6})
	return Ypos + Height + 15
end

--------------------------------------------------
--Function Name: PDF_DrawSpectrum
--------------------------------------------------
--Description: draw spectrum
--Return Value:  Return the bottom Y position
--Parameter:
--	pdf: PDF instance
--	spec: the spectrum array that user want to draw, now only support two channels,
--	      the spectrum comes from the function wave.spectrum
--	MaxFreq: the max frequency of the spectrum, it can be be 4000 or 8000
--	Ypos: the start Y position to draw
--	Dbreserve: how much db reserve on the top and bottom, often this value is set to 3
function PDF_DrawSpectrum(pdf, spec, MaxFreq, drawtype, Ypos, Dbreserve)
	local Xpos = 40
	local TextWidth = 30
	local Width = 400
	local Height = 200
	--calcu max and min
	local Maxval, Minval
	Maxval, Minval = PDF_CalcuMaxMin(spec)
	local Yreserve = Height * Dbreserve / (Maxval - Minval)
	Ypos = PDF_CheckCrossPage(pdf, Ypos, Height + Yreserve * 2 + 15)
	--draw background
	pdf:drawrect(Xpos, Ypos, Width, Height + Yreserve * 2, {bfill = true, fillcolor = {0.75,0.75,0.75}})
	local YStart = Ypos + Yreserve
	--draw vertical marks
	local DbMax = math.ceil((Maxval + Dbreserve) / 10) * 10
	local Yscale = Height / (Maxval - Minval)
	local YDbMax = YStart - (DbMax - Maxval) * Yscale
	--if YDbMax <= Ypos then YDbMax = YDbMax + 10 end
	for i = DbMax, (Minval - Dbreserve), -10 do
		if i <= Maxval + 3 and i >= Minval - 3 then
			local YDb = YDbMax - (i-DbMax) * Yscale
			pdf:drawline(Xpos , YDb, Xpos + Width, YDb, {linewidth = 0.5, linecolor = {0.95, 0.95, 0.95}})
			pdf:drawline(Xpos + Width , YDb, Xpos + Width + 4, YDb)
			pdf:drawtext(Xpos + Width + 5, YDb - 3, PDF_ConvertToString(i, 6), {fontsize = 6})
		end
	end
	for i = DbMax, Minval, -2 do
		if i <= Maxval + 3 and i >= Minval - 3 then
			local YDb = YDbMax - (i-DbMax) * Yscale
			pdf:drawline(Xpos + Width , YDb, Xpos + Width + 3, YDb, {linewidth = 0.5})
		end
	end
	--draw horizontal marks
	local HMarks = {}
	local Y = Ypos + Height + Yreserve * 2
	if drawtype == "linear" then
		if MaxFreq == 8000 then HMarks = {0,1000,2000,3000,4000,5000,6000,7000,8000}
		elseif MaxFreq == 4000 then HMarks = {0,500,1000,1500,2000,2500,3000,3500,4000}
		else error("MaxFreq parameter only can be 4000 or 8000") end
		local Xscale = Width / MaxFreq	
		for i = 1, #HMarks do
			local X = Xpos + HMarks[i] * Xscale
			pdf:drawline(X, Y, X, Y + 4)
			pdf:drawline(X, Ypos, X, Y, {linewidth = 0.5, linecolor = {0.95, 0.95, 0.95}})
			pdf:drawtext(X-TextWidth/2, Y+4, PDF_ConvertToString(HMarks[i], 4), {fontsize = 6, width = TextWidth, horizonalign = "center"})
		end
		XDStep = Width / (#HMarks - 1) / 5
		for i = XDStep, Width, XDStep do
			pdf:drawline(Xpos + i, Y, Xpos + i, Y + 3, {linewidth = 0.5})
		end
	elseif drawtype == "log" then
		if MaxFreq == 8000 then HMarks = {50,100,200,300,500,1000,2000,3000,4000,6000,8000}
		elseif MaxFreq == 4000 then HMarks = {50,100,200,400,700,1000,1500,2000,3000,4000}
		else error("MaxFreq parameter only can be 4000 or 8000") end
		pdf:drawtext(Xpos - TextWidth / 2, Y + 4, "0", {fontsize = 6, width = TextWidth, horizonalign = "center"})
		local Xscale = Width / (math.log10(512))
		for i = 1, #HMarks do
			local X = (math.log10(HMarks[i] / MaxFreq * 512)) * Xscale + Xpos
			pdf:drawline(X, Ypos, X, Y, {linewidth = 0.5, linecolor = {0.95, 0.95, 0.95}})
			pdf:drawline(X, Y, X, Y + 4)
			pdf:drawtext(X - TextWidth / 2, Y + 4, PDF_ConvertToString(HMarks[i], 4), {fontsize = 6, width = TextWidth, horizonalign = "center"})
		end
	end
	--draw spectrum
	pdf:drawspectrum(Xpos, YStart, drawtype, spec, {width = Width, height = Height})
	--draw bounding
	pdf:drawrect(Xpos, Ypos, Width, Height + Yreserve * 2, {bdrawboxline = true})
	return Ypos + Height + Yreserve * 2 + 15
end

--------------------------------------------------
--Function Name: PDF_DrawDiagram_Cylinder
--------------------------------------------------
--Description: Draw Cylinder Diagram according to the table
--Return Value: Return the bottom Y position
--Parameter:
--	pdf: PDF instance
--	ypos: the start Y position to draw
--	data: the 2D table to be drawn
--	maxval: the maxval value limited in the diagram
--	steps: how many steps that divide the maxval
function PDF_DrawDiagram_Cylinder(pdf, ypos, data, maxval, steps)
	if PDF_Check2DTab(data) == false then error("Data is not for Cylinder. -- PDF_DrawDiagram_Cylinder --") end
	local xpos = 40
	local w = 400
	local h = 200
	ypos = PDF_CheckCrossPage(pdf, ypos, h + 15)
	local tabrow, tabcol
	local tabrow, tabcol = PDF_Check2DTab(data)
	if tabrow == false then error("Table does not consistent") end
	if tabcol > 14 then error("Item Number should be under 14") end
	--draw back
	local hstep = h / steps
	local vstep = maxval / steps
	for i = 0, steps do
		local Y = ypos + h - i * hstep
		pdf:drawline(xpos, Y, xpos + w, Y, {linewidth = 0.5})
		pdf:drawtext(0, Y , PDF_ConvertToString(i * vstep, 10), {width = xpos - 1, fontsize = 9, horizonalign = "right"})
	end
	--init useful data
	local groupnum = #(data.Row)
	local itemnum = #(data.Col)
	local groupwidth = w / groupnum
	local itemwidth = groupwidth / (itemnum + 1)
	local scale = h / maxval
	--draw cylinder
	local coloridx = {	
		{0.8,0.3,0.5},
		{0.2,0.8,0.8},
		{0,0,1},
		{0.6,0.1,0.7},
		{0.8,0.8,0.8},
		{0.1,0.5,0.7},
		{0,0,0},
		{0.4,0.8,0.1},
		{0.7,0.3,0.1},
		{0.1,0.8,1},
		{0.2,0.4,1},
		{0.5,0.1,0.1},
		{0.6,0.8,1},
		{0.6,0.6,1}
	}
	local gnum
	local ioff = itemwidth / 2
	for gnum = 1, groupnum do
		local gpos = xpos + (gnum - 1) * groupwidth
		local inum
		for inum = 1, itemnum do
			local ipos = gpos + ioff + (inum - 1) * itemwidth
			local val = data[gnum][inum]
			if type(val) ~= "number" then error("invalid table data type in row:", gnum, "col:", inum) end
			local height = scale * val
			pdf:drawrect(ipos, ypos + h - height, itemwidth, height, {bfill = true, fillcolor = coloridx[inum]})
			--draw helper
			pdf:drawline(gpos, ypos + h, gpos, ypos + h + 4)
		end
		pdf:drawtext(gpos, ypos + h, data.Row[gnum], {width = groupwidth, horizonalign = "center", fontsize = 9})
	end
	--draw vertical and horizontal line
	pdf:drawline(xpos, ypos + h, xpos + w, ypos + h)
	pdf:drawline(xpos, ypos, xpos, ypos + h)
	--draw identifier
	local x = xpos + w
	local y = ypos + 5
	for inum = 1, itemnum do
		x, _ = pdf:drawrect(xpos + w + 5, y, 9, 9, {bfill = true, fillcolor = coloridx[inum]})
		x, y = pdf:drawtext(x + 3, y, data.Col[inum], {fontsize = 9})
		x = xpos + w
	end
	return ypos + h + 15
end

--------------------------------------------------
--Function Name: PDF_AddIndex
--------------------------------------------------
--Description: Add Index for a PDF file, this file can only be used before save the file
--Return Value: No return
--Parameter:
--	pdf: PDF instance
--	Index: the Index data
function PDF_AddIndex(pdf, Index)
	local idxnum = #Index
	pdf:insertpage(0, 1)
	local width = pdf:getproperty("pagewidth")
	local height = pdf:getproperty("pageheight")
	local contextpage = 1
	local x, y
	y = 1
	local lv = {5, 15, 25}
	x, y = pdf:drawtext(lv[1], y, "Index", {fontsize = 25, horizonalign = "center"})
	local dot = " ..."
	local dotspace =  pdf:getproperty(dot, {})
	for i = 1, idxnum do
		x = lv[Index[i].level]
		local page = PDF_CalcuPage(pdf, y + 15)
		if page >= contextpage then
			pdf:insertpage(contextpage,1)
			contextpage = contextpage + 1
		end
		pdf:drawtext(x, y, Index[i].text)
		x = pdf:getproperty(Index[i].text, {}) + x + 20
		while x < width - 30 - dotspace do
			pdf:drawtext(x, y, dot)
			x = x + dotspace
		end
		_, y = pdf:drawtext(0, y, PDF_ConvertToString(Index[i].page + 1, 3), {horizonalign = "right"})
		y = y + 4
	end
end

--------------------------------------------------
--Test Sector
--------------------------------------------------
wl = wave.load("d:\\testl.wav")
wr = wave.load("d:\\testr.wav")
p = pdf.createpdf()

X, Y = p:drawtext(1, 1, "Go To Link Dest 1", {link = "www.sina.com.cn"})
X, Y = p:drawtext(1, Y, "Go To Link Dest 2", {link = "file:///d:/test.wav"})

print("--------- wave1 ---------")
Y = PDF_DrawWaveform(p, wl, Y, 0, 1, 0, 2, 150)
print("--------- wave2 ---------")
Y = PDF_DrawWaveform(p, wl, Y, 0, 0.5, 10, 2, 150)
print("--------- wave3 ---------")
Y = PDF_DrawWaveform(p, wr, Y, -1, 1, 5, 2, 150)

spl = wave.spectrum(wl)
spr = wave.spectrum(wr)
print("--------- sp1 ---------")
Y = PDF_DrawSpectrum(p, {spl,spr}, 8000, "linear", Y, 3)
print("--------- sp2 ---------")
Y = PDF_DrawSpectrum(p, {spl,spr}, 4000, "linear", Y, 3)
print("--------- sp3 ---------")
Y = PDF_DrawSpectrum(p, {spl,spr}, 8000, "log", Y, 3)
print("--------- sp4 ---------")
Y = PDF_DrawSpectrum(p, {spl,spr}, 4000, "log", Y, 3)

print("--------- dia1 ---------")
datatable = {
	Row = {"group1", "group2", "group2", "group4", "group5"},
	Col = {"item1", "item2", "item3", "item4", "item5", "item6", "item7", "item8"},
	{1, 2, 3, 4, 5, 6, 7, 8},
	{1, 2, 3, 4, 5, 6, 7, 8},
	{1, 2, 3, 4, 5, 6, 7, 8},
	{1, 2, 3, 4, 5, 6, 7, 8},
	{1, 2, 3, 4, 5, 6, 7, 8}
}
PDF_DrawDiagram_Cylinder(p, Y, datatable, 10, 10)

print("--------- Index ---------")
index = {
		{level = 1, page = 1, text = "1 TestVec1"},
		{level = 2, page = 2, text = "1.1 TestVec1.1"},
		{level = 2, page = 3, text = "1.2 TestVec1.2"},
		{level = 2, page = 4, text = "1.3 TestVec1.3"},
		{level = 1, page = 5, text = "2 TestVec2"},
		{level = 2, page = 6, text = "2.1 TestVec2.1"},
		{level = 2, page = 7, text = "2.2 TestVec2.2"},
		{level = 2, page = 8, text = "2.3 TestVec2.3"}
	}
PDF_AddIndex(p, index)

p:savetofile("testDrawFunctions.pdf")
