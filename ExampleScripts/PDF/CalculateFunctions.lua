--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--FILE NAME: CalculateFunctions
--Description: this file contains functions associated with calculating
--Function List:
--		PDF_GetPageAt
--		PDF_CheckCrossPage
--		PDF_GetTablen
--        PDF_Check_2DArray
--		PDF_Check2DTab
--		PDF_Convert1DTo2D
--        PDF_2DTabTranspose
--        PDF_Convert2DArrayTo2DTab
--        PDF_CalcuStep
--        PDF_CalcuStepArray
--        PDF_ConvertToString
--        PDF_CalcuMaxMin
--        PDF_CalcuPage
--        PDF_ToLinkURI
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------
--PDF_GetPageAt
--------------------------------------------------
--Description:	Get the page that the given Y position belongs to, 
--Return Value: PageIndex (number): The page number of the given Y position at the pdf file
--Parameter:	
--	Pdf: PDF instance
--	Ypos: the given Y position
--PS: 	This function calculate the page of Y position to the first page of pdf file, not to the Indexd "startpage" page
function PDF_GetPageAt(Pdf, Ypos)
	local startpage = Pdf:getproperty("startpage")
	local pageheight = Pdf:getproperty("pageheight")
	local pagecount = startpage
	local yp = Ypos
	while yp >= pageheight do 
		yp = yp - pageheight 
		pagecount = pagecount + 1
	end		
	return pagecount
end

--------------------------------------------------
--Get_Tablen(tab) PDF_CheckCrossPage
--------------------------------------------------
--Description:	Check if the given position and height (which indicates a group of meta, treated as one entity) cross two pages
--		 cross: |           |     not cross: |  ________  |
--                |           |                | | 	   | |
--                |  _______  |                | |        | |
--                | |       | |                | |________| |
--                |_|_______|_|                |____________|
--                 ___________                  ____________
--                | |	   | |                |		     |
--                | |_______| |                |            |
--                |           |                |            |
--                |           |                |            |
--Return Value: Y (number): Y position that the entity should be.
--		If it cross two pages, return the start Y position of second page
--		else return parameter y.
--Parameter:
--	pdf (userdata):	PDF instance
--	y (number):	Y position of the entity
--	h (number):	height of the entity
function PDF_CheckCrossPage(Pdf, Y, H)
	local pageheight = Pdf:getproperty("pageheight")
	local t1 = math.floor(Y / pageheight)
	local t2 = math.floor((Y + H) / pageheight)
	if t1 == t2
	then
		return Y
	else
		return t2 * pageheight + 1
	end
end

--------------------------------------------------
--Function Name: Get_Tablen(tab)
--------------------------------------------------
--Description:	Get the element number of a table. Unlike operator #, this function also can handle hash_type tables
--Return Value:	Length (number): length (element number) of the table
--Parameter:
--	tab (table):	the table to be calculated
function PDF_GetTablen(Tab)
	local k, v
	local i = 0
	for k, v in pairs(Tab) do
		i = i + 1
	end
	return i
end

--------------------------------------------------
--Function Name: Check_2DArray
--------------------------------------------------
--Description:	Check 2D array, see whether it is a valid 2D array
--Return Value:	Return false if the table is not a Valid table format.
--		Return RowNum (number): number of rows in the 2D array,
--		       ColNum (number): number of cols in the 2D array.
function PDF_Check2DArray(Tab)
	local num = 0
	if type(Tab.Col) == "table" then num = #Tab.Col end
	if type(Tab.Row) == "table" and #Tab ~= #Tab.Row then return false end
	for row = 1, #Tab do
		if type(Tab[row]) ~= "table" then return false end
		if num == 0 then num = #(Tab[row])
		elseif num ~= #(Tab[row]) then return false end
	end
	return #Tab, #Tab[1]
end
--------------------------------------------------
--Function Name: Check_2DTab
--------------------------------------------------
--Description:	Check 2D table, see whether it is a valid 2D table
--Return Value:	Return false if the table is not a Valid table format.
--		Return RowNum (number): number of rows in the 2D table,
--		       ColNum (number): number of cols in the 2D table.
function PDF_Check2DTab(Tab)
	if type(Tab) ~= "table" then return false end
	if type(Tab.Row) == nil or type(Tab.Col) == nil then return false end
	local row, col
	local rows = #(Tab.Row)
	local cols = #(Tab.Col)
	if #Tab ~= rows then return false end
	for row = 1, rows do
		if #(Tab[row]) ~= cols then return false end
	end
	return rows, cols
end

--------------------------------------------------
--Function Name: Convert1DTo2D
--------------------------------------------------
--Description:	Convert a 1D table to a 2D table
--Return Value:	Return the 2D table
--Parameter:
--	Tab1D (table): the 1D table to be converted
--	RowNames (table): the name array of the rows, each element of this table should be a string
--	ColNames (table): the name array of the columns, each element of this table should be a string
--	The length of the three tables should meet #Tab1D = #RowNames * #ColNames
--Example: Tab1D = {A, B, C, D, E, F, G, H, I, J}
--	   RowNames = {"Row1", "Row2"}
--	   ColNames = {"Col1", "Col2", "Col3", "Col4", "Col5"}
--	   It returns Tab2D = {
--				{"Row1", "Row2"},
--				{"Col1", "Col2", "Col3", "Col4", "Col5"},
--				Row1 = {Col1 = A, Col2 = B, Col3 = C, Col4 = D, Col5 = E},
--				Row2 = {Col1 = F, Col2 = G, Col3 = H, Col4 = I, Col5 = J}
--	   		      }
function PDF_Convert1DTo2D(Tab1D, RowNames, ColNames)
	if #Tab1D ~= #RowNames * #ColNames then error("table length error in function --Convert1DTo2D--") end
	local tabout = {}
	tabout.Row = RowNames
	tabout.Col = ColNames
	for row = 1, #RowNames do
		tabout[row] = {}
		for col = 1, #ColNames do
			tabout[row][col] = Tab1D[(row - 1) * #ColNames + col]
		end
	end
	return tabout
end

--------------------------------------------------
--Function Name: Transpose
--------------------------------------------------
--Description:	Get the Transpose 2D Table
--Return Value: Return the transposed table
--Parameter: An 2D table
function PDF_2DTabTranspose (Data)
	if PDF_Check2DTab == false then return false end
	local tabout = {Row = Data.Col, Col = Data.Row}
	for row = 1, #tabout.Row do
		tabout[row] = {}
		for col = 1, #tabout.Col do
			tabout[row][col] = Data[col][row]
		end
	end
	return tabout
end

--------------------------------------------------
--Function Name: PDF_Convert2DArrayTo2DTab
--------------------------------------------------
--Description:	Convert 2D Array to 2D table
--Return Value: 2D Table
--Parameter:
--	Array2D: the array want to convert
--	RowNames,ColNames: the row and column names
function PDF_Convert2DArrayTo2DTab(Array2D, RowNames, ColNames)
	if #Array2D ~= #RowNames then error("table length error in function --PDF_Convert2DArrayTo2DTab--") end
	local tabout = Array2D
	tabout.Row = RowNames
	tabout.Col = ColNames
	return tabout
end

--------------------------------------------------
--Function Name: CalcuStep
--------------------------------------------------
--Description:	Calculate the step of a given range
--Return Value:	Return the value of one step, it must be a integer
--Parameter:
--	Start, End: The range to calculate
--	Stepnum: Number of steps 
function PDF_CalcuStep(Start, End, Stepnum)
	local delta = (End - Start) / Stepnum
	if delta > 1 then delta = math.floor(delta) end
	return delta
end

--------------------------------------------------
--Function Name: PDF_CalcuStepArray
--------------------------------------------------
--Description:	Get the step array of a given range
--Return Value:	Return the step array
--Parameter:
--	Start, End: The range to calculate
--	Stepnum: Value of one step
function PDF_CalcuStepArray(Start, End, Step)
	local Array = {}
	for i = Start, End, Step do
		table.insert(Array, i)
	end
	table.insert(Array, End)
	return Array
end

--------------------------------------------------
--Function Name: PDF_ConvertToString
--------------------------------------------------
--Description:	Convert a number to string with the specified length
--Return Value:	Return the string
--Parameter:
--	num: Number to be convert
--	len: Length of the string limited in
function PDF_ConvertToString(num, len)
	local str = tostring(num)
	str = string.sub(str, 1, len)
	return str
end

--------------------------------------------------
--Function Name: PDF_CalcuMaxMin
--------------------------------------------------
--Description:	Calculate the max and min value of the spectrum array
--Return Value:	Return the max and min value
--Parameter:
--	spec: Spectrum array
function PDF_CalcuMaxMin(spec)
	local Max = spec[1][1]
	local Min = Max
	for i = 1, #spec do
		for j = 1, #spec[i] do
			if Max < spec[i][j] then Max = spec[i][j]
			elseif Min > spec[i][j] then Min = spec[i][j] end
		end
	end
	return Max, Min
end

--------------------------------------------------
--Function Name: CalcuPage
--------------------------------------------------
--Description:	Calculate what page that a specified Y position is at
--Return Value:	Return the page that position is at
--Parameter:
--	pdf: PDF instance
--	ypos: the position wanted to be calculated
function PDF_CalcuPage(pdf, ypos)
	local startpage = pdf:getproperty("startpage")
	local pageheight = pdf:getproperty("pageheight")
	local pagenum = math.floor(ypos / pageheight)
	return startpage + pagenum
end

--------------------------------------------------
--Function Name: ToLinkURI
--------------------------------------------------
--Description:	Convert a path string to a URI string
--     Example: d:\test\ => file:///d:/test/
--Return Value: Return a URI string
--Parameter:
--	path: path string
function PDF_ToLinkURI(path)
	return "file:///"..string.gsub(path, "\\", "/")
end

--------------------------------------------------
--Test Sector
--------------------------------------------------
--[[p = pdf.createpdf()
p:drawtext(0, 0, "CalcuFunctionTest", {horizonalign = "center"})

g_h = p:getproperty("pageheight")
print("---------- Create File ----------")
print("Page Height:", g_h)

--test PDF_GetPageAt
print("---------- Test PDF_GetPageAt ----------")
print("Height:", 40, "Page:", PDF_GetPageAt(p, 40))
print("Height:", g_h + 40, "Page:", PDF_GetPageAt(p, g_h + 40))
print("Height:", g_h, "Page:", PDF_GetPageAt(p, g_h))

--test Get_Tablen
print("---------- Test PDF_GetTablen ----------")
testtab = {
	{11, 12, 13, 14},
	{21, 22, 23, 24},
	{31, 32, 33, 34},
	Row = {"G1", "G2", "G3"},
	Col = {"I1", "I2", "I3", "I4"}
}
print(PDF_GetTablen(testtab))
print(PDF_GetTablen(testtab[1]))
print(PDF_GetTablen(testtab.Row))
print(PDF_GetTablen(testtab.Col))

--test checkcrosspage
print("---------- Test PDF_CheckCrossPage ----------")
print("Y:", 100, "H:", 400, "Yout:", PDF_CheckCrossPage(p, 100, 400))
print("Y:", 500, "H:", 400, "Yout:", PDF_CheckCrossPage(p, 500, 400))
print("Y:", 800, "H:", 400, "Yout:", PDF_CheckCrossPage(p, 800, 400))

--test check2dtable
print("---------- Test PDF_Check2DTab ----------")
print(PDF_Check2DTab(testtab))

--test check2darray
print("---------- Test PDF_Check2DArray ----------")
print(PDF_Check2DArray(testtab))

--test Convert1DTo2D
print("---------- Test Convert1DTo2D ----------")
testtab1D = {11,12,13,14,21,22,23,24,31,32,33,34}
gname = {"G1","G2","G3"}
iname = {"I1","I2","I3","I4"}
tabout = PDF_Convert1DTo2D(testtab1D, gname, iname)

p:savetofile("testCalcufunction.pdf")
print("---------- File Saved ----------")]]--
