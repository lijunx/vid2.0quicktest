--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--FILE NAME: CommonDrawFunctions
--Description: this file contains functions associated with CommonDrawing
--Function List:
--		Draw Title
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------
--Include Files
--------------------------------------------------
require "Format.lua"
require "CalculateFunctions.lua"

--------------------------------------------------
--Draw Title
--------------------------------------------------
--Description: Draw title of a report
--Return Value: the bottom of the picture
--Parameter:
--	pdf: PDF instance
--	Ypos: the start Y position
--	Title and SubTitle: Title and SubTitle to be drawn
function PDF_DrawTitle(pdf, Ypos, Title, SubTitle)
     local y
     _, y = pdf:drawtext(0, Ypos, Title, {horizonalign = "center", fontsize = PDF_FONTSIZE_TITLE_LV1})
     _, y = pdf:drawtext(0, y, SubTitle, {horizonalign = "center", fontsize = PDF_FONTSIZE_TITLE_LV2})
     return y
end

--------------------------------------------------
--Draw Simple 2D Table
--------------------------------------------------
--Description: Draw simple 2D table, the data to be drawn can be string or number, and if the data is number,
--             please make sure the length can not be too long, for example: 3.1415926535 often should be cut
--             to 3.14 in order to meet the width limit of grid
--Return Value: draw a 2D table, and according to the given position and width, each cell has the same width 
--Parameter:
--	pdf: PDF instance
--	Xpos, Ypos: the start X, Y position
--	Width: the width of the table
--	CellHeight: height of each cell
--	Data: Data to be drawn, it can be 2D Array or 2D Table, or can be 2D array with only Rownames or Colnames
--	TopleftText: the topleft cell text, if set to false or the Data does not contain the rownames, simply don't draw
--   FontSize: the fontsize of the text in the table
function PDF_DrawSimple2DTable(pdf, Xpos, Ypos, Width, CellHeight, Data, TopleftText, FontSize)
	--check valid
	local Row, Col
	Row, Col = PDF_Check2DArray(Data)
	if Row == false then error("Data invalid in PDF_DrawSimple2DTable") end
	--init data
	local x, y, ytemp
	local pagewidth = pdf:getproperty("pagewidth")
	if Xpos + Width > pagewidth then error("Page can not contain so wide") end
	local off = 1
	if Data.Row == nil then off = 0 end
	local cellwidth = Width / (Col + off)
	local cellpro = {width = cellwidth, height = CellHeight, bdrawboxline = true, verticalalign = "middle", horizonalign = "center", fontsize = FontSize}
	x = Xpos
	if TopleftText ~= false and Data.Col ~= nil and Data.Row ~= nil then x, _ = pdf:drawtext(Xpos, Ypos, TopleftText, cellpro) end
	--draw column title if necessary
	ytemp = Ypos
	if Data.Col ~= nil then
		local off = 0
		if Data.Row == nil then off = 1 end
		for i = 1, #Data.Col do
			x, ytemp = pdf:drawtext(Xpos + (i - off) * cellwidth, Ypos, Data.Col[i], cellpro)
		end
	end
	
	for i = 1, Row do
		y = ytemp
		x = Xpos
		if Data.Row ~= nil then x = pdf:drawtext(Xpos, y, Data.Row[i], cellpro) end
		for j = 1, Col do
			local data = Data[i][j]
			if type(data) ~= "string" then
				data = tostring(data)
			end
			x, ytemp = pdf:drawtext(x, y, data, cellpro)
		end
	end
	return ytemp
end

----------------------------------------------------------------
function PDF_DrawTimeTableHeader (pdf, Ypos)
	local TabWidth = 310
	local DataWidth = 200
	local LanWidth = 60
	local CellHeight = 20
	
	local pagewidth = pdf:getproperty("pagewidth")
	local Xpos = (pagewidth - TabWidth) / 2
	local x = Xpos
	local y = Ypos
	--draw title	
	x, _ = pdf:drawtext(x, Ypos, "Language", {bdrawboxline = true, font = "Helvetica-Bold", width = TabWidth - DataWidth, height = CellHeight * 3, horizonalign = "center", verticalalign = "middle"})
	_, y = pdf:drawtext(x, Ypos, "Time Setting", {bdrawboxline = true, font = "Helvetica-Bold", width = DataWidth, height = CellHeight, horizonalign = "center", verticalalign = "middle"})
	x, _ = pdf:drawtext(x, y, "Origin", {bdrawboxline = true, width = DataWidth / 2, height = CellHeight, horizonalign = "center", verticalalign = "middle"})
	_, y = pdf:drawtext(x, y, "Test", {bdrawboxline = true, width = DataWidth / 2, height = CellHeight, horizonalign = "center", verticalalign = "middle"})
	local Col = {"start","end","start","end"}
	x = Xpos + TabWidth - DataWidth
	for i = 1, 4 do
		x, _ = pdf:drawtext(x, y, Col[i], {bdrawboxline = true, width = DataWidth / 4, height = CellHeight, horizonalign = "center", verticalalign = "middle"})
	end
	return Ypos + CellHeight * 3
end

function PDF_DrawTimeTableData(pdf, Ypos, Data, Language)
	local TabWidth = 310
	local DataWidth = 200
	local LanWidth = 60
	local CellHeight = 20
	
	local pagewidth = pdf:getproperty("pagewidth")
	local Xpos = (pagewidth - TabWidth) / 2
	local x = Xpos
	local y = Ypos
	
	local RowBak = Data.Row
	Data.Row = {"F11","F12","F21","F22","M11","M12","M21","M22"}
	local ColBak = Data.Col
	Data.Col = nil
	if PDF_Check2DArray(Data) == false then error("Time Data error in --PDF_DrawTimeTable--") end
	PDF_DrawSimple2DTable(pdf, Xpos + LanWidth, Ypos, TabWidth - LanWidth, CellHeight, Data, false, 12)
	_, y = pdf:drawtext(Xpos, Ypos, Language, {bdrawboxline = true, width = LanWidth, height = CellHeight * #Data.Row, horizonalign = "center", verticalalign = "middle"})
	Data.Row = RowBak
	Data.Col = ColBak
	return y
end
-----------------------------------------------------------------

--------------------------------------------------
--test sector
--------------------------------------------------
p = pdf.createpdf()

print("---------- test drawsimpletable ----------")
_, Y = p:drawtext(0, 1, "Begin Test")
Data = {
	{1,3,5,7},
	{2,4,6,8.6},
	{23,43,56,84},
	{35,78,94,36},
	{1,3,5,7},
	{2,4,6,8},
	{23,43,56,84},
	{35,78,94,36}
}

print("------------ 1 ------------")
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 400, 20, Data, false, 9)
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 260, 15, Data, "Hi", 9)
print("------------ 2 ------------")
Data.Row = {"R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8"}
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 400, 20, Data, false, 9)
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 260, 15, Data, "Hi", 9)
print("------------ 3 ------------")
Data.Row = nil
Data.Col = {"C1", "C2", "C3", "C4"}
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 400, 20, Data, false, 9)
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 260, 15, Data, "Hi", 9)
print("------------ 4 ------------")
Data.Col = {"C1", "C2", "C3", "C4"}
Data.Row = {"R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8"}
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 400, 20, Data, false, 9)
Y = PDF_DrawSimple2DTable (p, 10, Y + 50, 260, 15, Data, "Hi", 9)
print("------------ 5 ------------")
Data.Col = nil
Data.Row = nil
Y = PDF_DrawTimeTableHeader(p, Y + 50)
Y = PDF_DrawTimeTableData(p, Y, Data, "American English")

p:savetofile("testCommonDrawFunctions.pdf")
