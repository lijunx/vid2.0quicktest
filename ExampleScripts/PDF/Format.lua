--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--FILE NAME: Format
--Description: this file contains data format definitions
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------
--Indentation
--------------------------------------------------
PDF_INDENTATION_STEP = 15
PDF_INDENTATION_LV1 = 5
PDF_INDENTATION_LV2 = PDF_INDENTATION_LV1 + PDF_INDENTATION_STEP
PDF_INDENTATION_LV3 = PDF_INDENTATION_LV2 + PDF_INDENTATION_STEP
PDF_INDENTATION_LV4 = PDF_INDENTATION_LV3 + PDF_INDENTATION_STEP
PDF_INDENTATION_LV5 = PDF_INDENTATION_LV4 + PDF_INDENTATION_STEP

--------------------------------------------------
--FontSize
--------------------------------------------------
PDF_FONTSIZE_TITLE_LV1 = 25
PDF_FONTSIZE_TITLE_LV2 = 15
PDF_FONTSIZE_CONTEXT = 12
PDF_FONTSIZE_ILLUSTRATE = 9
PDF_FONTSIZE_MARKS = 6

--------------------------------------------------
--FontFormatTPL
--------------------------------------------------
PDF_FONTFORMAT_TITLE_LV1 = {fontsize = 20, font = "Helvetica-Bold"}
PDF_FONTFORMAT_TITLE_LV2 = {fontsize = 16, font = "Helvetica-Bold"}
PDF_FONTFORMAT_TITLE_LV3 = PDF_FONTFORMAT_TITLE_LV2

--------------------------------------------------
--Space Reserve
--------------------------------------------------
PDF_TABLE_SPACE_LR = PDF_INDENTATION_LV3
PDF_PARAGRAPH_SPACE = 10