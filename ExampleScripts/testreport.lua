TOTAL_TIME = 61000		--ms
TOTAL_ROUND = 1
pesq_score = {}

tmp_data = wave.load_raw("standard.pcm",1,8000)
std_data = wave.cut(tmp_data, 0, TOTAL_TIME)

for i = 1, TOTAL_ROUND
do

	wav_name = string.format("test_%d_2.pcm",i)

	whole_data = wave.load_raw(wav_name,1,8000)
	new_data = wave.cut(whole_data, 0, TOTAL_TIME)

	pesq_score[i] = wav.pesq(std_data, new_data)

	seg = i%6
	if(seg == 0)
	then
		seg = 6
	end

	start_pos = (seg-1)*10000
	stop_pos  = seg*10000
	demo_data = wave.cut(new_data, start_pos, stop_pos)

end