ATT_NBHS =
{
    S_MOS = 
    {
        NoBGN = 4.4, --Nobgn
        Pub = 3.7, --Pub
        Road = 3.5, --Road
        XRoads = 3.7, --Xroad
        Train = 3.8, --Train
        Car = 4.1, --Car
        Cafeteria = 4.1, --Cafe
        Mensa = 4.2, --Mensa
        CallCenter = 4.3, --Callcenter
    },
    N_MOS = 
    {
        NoBGN = 4.5, --Nobgn
        Pub = 3.9, --Pub
        Road = 4, --Road
        XRoads = 4.3, --Xroad
        Train = 4.1, --Train
        Car = 4.5, --Car
        Cafeteria = 4.0, --Cafe
        Mensa = 4.3, --Mensa
        CallCenter = 4.3, --Callcenter
    },
    G_MOS = {}
}

ATT_NBHF =
{
    N_MOS = 
    {
        ave = 2.2,
    },
    S_MOS = 
    {
        ave = 3.3,
    },
    G_MOS = {}
}

ATT_WBHS =
{
    S_MOS =
    {
        NoBGN = 4.4, --Nobgn
        Pub = 3.7, --Pub
        Road = 3.5, --Road
        XRoads = 3.9, --Xroad
        Train = 3.9, --Train
        Car = 4.0, --Car
        Cafeteria = 4.0, --Cafe
        Mensa = 4.0, --Mensa
        CallCenter = 4.2, --Callcenter
    },
    N_MOS = 
    {
        NoBGN = 4.4, --Nobgn
        Pub = 3.4, --Pub
        Road = 4.1, --Road
        XRoads = 4.2, --Xroad
        Train = 4, --Train
        Car = 4.3, --Car
        Cafeteria = 4.0, --Cafe
        Mensa = 4.0, --Mensa
        CallCenter = 4.0, --Callcenter
    },
    G_MOS = {}
}

ATT_WBHF =
{
    N_MOS = 
    {
        ave = 2.2,
    },
    S_MOS =
    {
        ave = 3.3,
    },
    G_MOS = {}
}

VFTST_NBHS =
{
    S_MOS = 
    {
        Road = 3.5, --Road
        Train = 3.8, --Train
        Car = 3.9, --Car
        Mensa = 4.1, --Mensa
        ave = 4.0, --ave
    },
    N_MOS = 
    {
        Road = 2.5, --Road
        Train = 3.3, --Train
        Car = 3.6, --Car
        Mensa = 3.5, --Mensa
        ave = 4.0, --ave
    },
    G_MOS =
    {
        Road = 2.8, --Road
        Train = 3.4, --Train
        Car = 3.7, --Car
        Mensa = 3.7, --Mensa
        ave = 4.0, --ave
    },
}

VFTST_NBHF =
{
    S_MOS = 
    {
        Road = 1.2, --Road
        Train = 1.9, --Train
        Car = 2.3, --Car
        Mensa = 2.5, --Mensa
    },
    N_MOS = 
    {
        Road = 1.2, --Road
        Train = 1.9, --Train
        Car = 2.3, --Car
        Mensa = 2.5, --Mensa
    },
    G_MOS = 
    {
        Road = 1.2, --Road
        Train = 1.9, --Train
        Car = 2.3, --Car
        Mensa = 2.5, --Mensa
    }
}

VFTST_WBHS =
{
    S_MOS =
    {
        Road = 3.4, --Road
        Train = 3.8, --Train
        Car = 4.0, --Car
        Mensa = 4.1, --Mensa
        ave = 4.0, --ave
    },
    N_MOS =
    {
        Road = 2.8, --Road
        Train = 3.2, --Train
        Car = 3.5, --Car
        Mensa = 3.1, --Mensa
        ave = 4.0, --ave
    },
    G_MOS = 
    {
        Road = 2.9, --Road
        Train = 3.4, --Train
        Car = 3.6, --Car
        Mensa = 3.6, --Mensa
        ave = 4.0, --ave
    }
}

VFTST_WBHF =
{
    S_MOS = 
    {
        Road = 1.7, --Road
        Train = 2.3, --Train
        Car = 2.8, --Car
        Mensa = 2.2, --Mensa
    },
    N_MOS = 
    {
        Road = 1.7, --Road
        Train = 2.3, --Train
        Car = 2.8, --Car
        Mensa = 2.2, --Mensa
    },
    G_MOS = 
    {
        Road = 1.7, --Road
        Train = 2.3, --Train
        Car = 2.8, --Car
        Mensa = 2.2, --Mensa
    }
}

function load_spec(library_name, mode, score_list)
    local table_name = library_name.."_"..mode
    print("spec_tab = "..library_name.."_"..mode)
    pcall(loadstring("spec_tab = "..library_name.."_"..mode))
    local spec = {}
    for i = 1, #score_list do
        spec[score_list[i]] = {}
        print(score_list[i])
        for j = 1, #NOISE do
            --print(NOISE[j])
            spec[score_list[i]][j] = spec_tab[score_list[i]][NOISE[j]]
        end
    end
    
    return spec
end