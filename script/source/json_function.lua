function load_jsonfile(jsonfile)
    printc("\n\nLoad mydesign.json file, file path: "..jsonfile)
    local table_json = {}
    file = io.open(jsonfile, "r")
    if file == nil then
        printc("ERROR: Can't find the json file! Please check it! "..jsonfile)
        return
    end

    local i,j = 0,0
    local json_tab = {}
    for line in file:lines(file) do
        --line = string.gsub(line, "[ %[%]{}\"]", "")
        --print(line)
        line = string.gsub(line, "[ %[%]\"]", "")
        --line = string.gsub(line, "\"", "")
        line = string.gsub(line, ":", " ")
        if string.find(line, ",", string.len(line), string.len(line)) then
            line = string.sub(line, 1, string.len(line)-1)
        end
        i = i+1
        local tab = line_to_token(line)
        if string.len(line) > 0  then
            --print(i, line)
            --io.read()
            j = j+1
            table_json[j] = tab
            table_json[j].line = i
        end
        --print(i)
    end
    file:close()
    --print("\n")
    table_json[1] = {"{"}
    return table_json
end

--**************** the string has " ".
function load_jsonfile1(jsonfile)
    printc("\n\nLoad json file, file path: "..jsonfile)
    table_json = {}
    file = io.open(jsonfile, "r")
    if file == nil then
        printc("ERROR: Cann't find the json file! Please check it! "..jsonfile)
        return nil
    end

    local i,j = 1,1
    str = ""
    for line in file:lines(file) do
        if i == 1 and line == "" then line = "{" end
        str = str..line
        i = i + 1
    end
    --printc(str)
    file:close()
    file = nil
    local flag_change = 0
    local pos_quote = 0
    local polar_quote = 1
    local pos, nextpos, lastpos = 1,1,1
    local flag_array = 0
    --for i = 2, string.len(str)-1 do
    while(1) do
        print("pos is..."..pos)
        local char = string.sub(str, pos, pos)
        --print(char)
        local temp_char = char
        if flag_change == 0 and char == "\"" and flag_array == 0 then  --"
             if pos_quote == 0 then
                 char = "[\""
                 pos_quote = 1
             else
                 char = "\"]"
                 pos_quote = 0
             end
        elseif flag_change == 0 and char == ":" and flag_array == 0 then
            flag_change = 1
            char = "="
        elseif char == "[" and (pos_quote ~= 1 and polar_quote ~= -1) then   char = "{"; flag_array = 1
        elseif char == "]" and (pos_quote ~= 1 and polar_quote ~= -1) then   char = "}"; flag_array = 0
        elseif (char == "," and polar_quote == 1) or char == "}" then flag_change = 0
        elseif char == "{" then flag_change = 0; flag_array = 0
        elseif char == "\"" and flag_change == 1 then  polar_quote = -polar_quote  --"
        elseif char == " " and (pos_quote ~= 1 and polar_quote ~= -1) then char = ""
        end
        if char ~= temp_char then
            local first_str = string.sub(str, 1, pos-1)
            --print(first_str,pos)
            local end_str = string.sub(str, pos+1, -1)
            --print(end_str)
            str = first_str..char..end_str
            --io.read()
            --print(str)
        end
        --print(pos, string.len(char), string.byte(char))
        pos = pos + string.len(char)
        --print(pos)
        if pos >= string.len(str) then break end
    end
    
    --printc(str, DEBUG)
    --pcall(loadstring("table_json=str"))
    assert(pcall(loadstring("table_json="..str)))
    --table_json = _G[str]
    --stop()
    --collectgarbage("collect")
    return table_json
end

function load_vecfile(vecfile)
    printc("\n\nLoad vec file, file path: "..vecfile)
    local vec_table = {}
    local file = io.open(vecfile, "r")
    if file == nil then
        printc("ERROR: Can't find the vec file! Please check it! "..vecfile)
        return
    end
    
    --local address, para
    local i = 0
    for line in file:lines(file) do
        if string.sub(line, 1, 1) ~= "#" and string.sub(line, 1, 1) ~= "-" and string.len(line)>0 then 
            i = i + 1
            line = string.gsub(line, "%W", " ")
            local token = line_to_token (line)
            for j = 1, #token do
                --print(token[j], j)
                if string.find(token[j], "0x") == nil and string.find(token[j], "0X") == nil then  --�����ַ������ǰû��OX�����ϡ�
                    token[j] = "0x"..token[j]
                end
            end
            if #token >= 2 then
                --address, para = tonumber(token[1]), tonumber(token[2])
                vec_table[i] = {tonumber(token[1]), tonumber(token[2])}
                --printc(string.format("0x%04X", vec_table[i][1]).." = "..string.format("0x%04X", vec_table[i][2]))
            elseif #token ~= 0 then 
                printc("LOG_READ_MAP(line "..tostring(i)..", token_num "..tostring(#token).."): "..line)
            end
        end
    end
    file:close()
    printc("\n")
    return vec_table
end

function import_vecfile(input_vecfile, target_file)
    local file = io.open(target_file, "r")
    if file == nil then
        printc("ERROR: Can't find the target file! Please check it! "..target_file)
        return
    end
    
    local title = {}
    for line in file:lines(file) do
        if string.sub(line, 1, 1) == "#" then 
            table.insert(title, line)
        end
    end
    file:close()
    
    local vec_tab = load_vecfile(input_vecfile)
    local temp_file = "c:\\temp.vec"
    os.execute("c: > "..temp_file)
    local FILE = assert( io.open(temp_file,"a+") )
    for i = 1,#title do
        assert(FILE:write(title[i].."\n"))
        --print(title[i])
    end
    for i = 1,#vec_tab do
    --print(i, vec_tab[i][1])
        if vec_tab[i][1] ~= 0x2089 and vec_tab[i][1] ~= 0x208a and vec_tab[i][1] ~= 0x2020 and vec_tab[i][1] ~= 0x2021 and #vec_tab[i]>1 then 
            if i == #vec_tab then
                assert(FILE:write(string.format("0x%04x", vec_tab[i][1]).." "..string.format("0x%04x", vec_tab[i][2])))
            else
               assert(FILE:write(string.format("0x%04x", vec_tab[i][1]).." "..string.format("0x%04x", vec_tab[i][2]).."\n"))
            end
            printc(string.format("0x%04x", vec_tab[i][1]).." "..string.format("0x%04x", vec_tab[i][2]))
        end
    end
    assert(FILE:close())
    os.execute("xcopy \""..temp_file.."\" \""..target_file.."\"")--.." ".."/s/e")
    os.execute("del \""..temp_file.."\"")

end

function export_vecfile(input_vecfile, output_file, export_tab)
    os.execute("xcopy \""..input_vecfile.."\" \""..output_file.."\"")--.." ".."/s/e")
    local FILE = assert( io.open(output_file,"a+") )
    assert(FILE:write("\n"))
    for i = 1, #export_tab do
        if i == #export_tab then
            assert(FILE:write(export_tab[i]))
        else
            assert(FILE:write(export_tab[i].."\n"))
        end
        --print(export_tab[i])
    end
    assert(FILE:close())
end

function dump_json(o)
    if type(o) == 'table' then
        local s = ''
        for k,v in pairs(o) do
            if type(k) == "string" and type(v) == "table" then
                k = "\""..k.."\""
                s = s .. k..': [\n'..dump_json(v)..'],\n'
            elseif type(k) == "number" and type(v) == "table"  then
                s = s.."{\n"..dump_json(v).."},\n"
            else
                if type(k) == "number" then k = tostring(k) end
                k = "  \""..k.."\""
                if type(v) == "number" or type(v) == "boolean" then v = tostring(v)
                else 
                    v = "\""..v.."\""
                    v = string.gsub(v, "\\", "\\\\")
                end
                s = s..k..": "..v..",\n"
            end
        end
        --print("aaaa"..s)
        return s
    else
    --print("bbbb"..o)
        if type(o) == "string" then return "\""..string.gsub(o, "\\", "\\\\").."\",\n"
        else  return tostring(o)..",\n"
        end
    end
end

function export_jsonfile(output_file, export_tab)
    file = io.open(output_file,"a+")
    if file == nil then 
        os.execute("c: > "..output_file)
        io.open(output_file,"a+")
    end
    --assert(file:write("{\n"))
    local str = dump_json(export_tab)
    str = "{\n"..str.."}"
    assert(file:write(str))
    assert(file:close())
end

local function line_to_token(line)
    local token = {}
    local line_start = string.sub(line, 1, 1)

    if line_start == "\," then return token end
    local line_m = line

    local len = string.len(line_m)
    local idx = 0

    while idx < len do
        local pos = string.find(line_m, " ", idx + 1)
        if pos == nil then pos = len+1 end
        local t = string.sub(line_m, idx + 1, pos-1)
        --print(t, pos, string.len(t))
        if t ~= " " and string.len(t)>0 then table.insert(token, t) end
        idx = pos
    end
    
    return token
end

function dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k,v in pairs(o) do
            if type(k) ~= 'number' then k = '"'..k..'"' end
            s = s .. '['..k..'] = ' .. dump(v) .. ',\n'
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

--pcall(loadstring())

function tab_init(table_name, depth, width)
    local str = ""
    --if depth == 1 then return table_name end
    for i = 1, depth do
        str = str.."["..width[i].."]"
    end
    --print(table_name..str)
    return table_name..str
end

function file_to_table(file_tab)
    design_table = {}
    local depth, width = 0,{}
    for i = 1, #file_tab do
        --print(unpack(file_tab[i]))
        if file_tab[i][1] == "{" then --and #file_tab[i] == 1 then
            depth = depth + 1
            width[depth] = width[depth] or 1
            local str = tab_init("design_table", depth, width).."={}"
            --print(str)
            pcall(loadstring(str))
            --pcall(loadstring("print("..tab_init("design_table", depth, width)..")"))
        elseif file_tab[i][1] == "}" then
            -- io.read()
            width[depth] = width[depth] + 1
            width[depth+1] = nil  ----
            depth = depth - 1
            if depth == 0 then
                --print(#new_tab, #new_tab[1], #new_tab[1][1])--, #new_tab[1][1][1])
               -- print(depth, unpack(width))
                return design_table
            end
        else
            line_temp = 0
            TEMP = 0
            if #file_tab[i] == 2 then
                TEMP = file_tab[i][2]
                line_temp = file_tab[i].line
                local str = tab_init("design_table", depth, width).."[\""..file_tab[i][1].."\"]".."=TEMP"
                pcall(loadstring(str))
                --printc(tab_init("design_table", depth, width).."[\""..file_tab[i][1].."\"]".." = ")
                --pcall(loadstring("printc("..tab_init("design_table", depth, width).."."..file_tab[i][1]..")\n"))
                local str = tab_init("design_table", depth, width).."[\""..file_tab[i][1].."_line\"]".."=line_temp"
                pcall(loadstring(str))
            elseif #file_tab[i] == 1 and string.len(file_tab[i+1][1]) > 1 then
                TEMP = file_tab[i+1][1]
                line_temp = file_tab[i].line--, file_tab[i+1].line} 
                local str = tab_init("design_table", depth, width).."[\""..file_tab[i][1].."\"]".."=TEMP"
                pcall(loadstring(str))
                --printc(tab_init("design_table", depth, width).."[\""..file_tab[i][1].."\"]".." = ")
                --pcall(loadstring("printc("..tab_init("design_table", depth, width).."."..file_tab[i][1]..")\n"))
                local str = tab_init("design_table", depth, width).."[\""..file_tab[i][1].."_line\"]".."=line_temp"
                pcall(loadstring(str))
                --print("aaaaa")
            end
            --[[
            local str = tab_init("design_table", depth, width).."[\""..file_tab[i][1].."\"]".."=TEMP"
            pcall(loadstring(str))
            --print(tab_init("design_table", depth, width).."[\""..file_tab[i][1].."\"]".." = ")
            --pcall(loadstring("print("..tab_init("design_table", depth, width).."."..file_tab[i][1]..")\n"))
            local str = tab_init("design_table", depth, width).."[\""..file_tab[i][1].."_line\"]".."=line_temp"
            pcall(loadstring(str))
            --print(str)

         --   print(depth, width[depth])
         ]]
        end
    end
end

function write_jsonfile(json_file, ns_level, input_feature, feature_ns)
    local file_r = assert(io.open(json_file,"r"))
    local file_r_line = {}
    for line in file_r:lines(file_r) do
        table.insert(file_r_line, line)
    end
    --print(#file_r_line)
    --print("aaa")
    assert(file_r:close())
    
    local temp_file = "c:\\temp.json"
    os.execute("c: > "..temp_file)
    local FILE = assert( io.open(temp_file,"a+") )

    local i = 1
    while i<=#file_r_line do
        --print(file_r_line[i])

        if i == feature_ns.NsLevel_line then
            assert(FILE:write(ns_level.."\n"))
        --end
        elseif  i == feature_ns.NsLevel_line -4 and #feature_ns == 0 then --feature ��mydesign.json����û����δ����
            for j = 1, #input_feature do
                assert(FILE:write("{\n"))
                for k = 1, #input_feature[j] do
                    assert(FILE:write(input_feature[j][k].."\n"))
                end
                assert(FILE:write("},\n"))
            end
           --line ��һ��
           i = i + 1
        --end
        elseif  #feature_ns ~= 0 and #input_feature == 0 and i == feature_ns[1].Mask_line - 1 then
           --line�������һ��feature�����ĵط�+1
           i = feature_ns[#feature_ns].Addr_line + 1
        --end
        elseif  #feature_ns ~= 0 and #input_feature ~= 0 and i == feature_ns[1].Mask_line - 1 then
            for j = 1, #input_feature do
                assert(FILE:write("{\n"))
                for k = 1, #input_feature[j] do
                    assert(FILE:write(input_feature[j][k].."\n"))
                end
                assert(FILE:write("},\n"))
            end
            --line�������һ��feature�����ĵط�+1
            i = feature_ns[#feature_ns].Addr_line + 1
        --end      
        --if #feature_ns
        else
            assert(FILE:write(file_r_line[i].."\n"))
        end

        i = i + 1
    end
    

    assert(FILE:close())
    os.execute("xcopy \""..temp_file.."\" \""..json_file.."\"")--.." ".."/s/e")
    os.execute("del \""..temp_file.."\"")
end

