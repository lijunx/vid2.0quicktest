arithmetic_version = "v0.2"
--**************************************************--
--Function List:
--  get_leftchannel
--  get_rightchannel
--  wave_cut
--  wave_load
--  wave_pesq
--  wave_HPF
--  detect_delay
--  decimal_round
--  get_average
--  copy_table
--  bit functions: 
--    bit:d2b(arg); bit:b2d(arg); bit:_xor(a,b); bit:_and(a,b); bit:_or(a,b); bit:_not(a); 
--    bit:_rshift(a,n); bit:_lshift(a,n); bit:print(ta).
--
--**************************************************--


--**************************************************--
--Function Name: get_leftchannel
--Description: 
--Return Value: the left channel of the wave data
--Parameter:
--  wav: wave data, the channel number is must more than 2.
--Usage: new_wave_data = get_leftchannel(wave_data)
--**************************************************--
function get_leftchannel(wav)
	local tab = wave.n_to_mono(wav)
	for i = 2, #tab do Wave.clearWavdata(tab[i]) end
	return tab[1]
end

--**************************************************--
--Function Name: get_rightchannel
--Description: 
--Return Value: the second channel of the wave data.
--Parameter:
--  wav: wave data, the channel number is must more than 2.
--Usage: new_wave_data = get_rightchannel(wave_data)
--**************************************************--
function get_rightchannel(wav)
    local tab = wave.n_to_mono(wav)
    if #tab > 2 then
        for i = 3, #tab do Wave.clearWavdata(tab[i]) end
        print("wave file is not double channel")
        --return nil
    end
    Wave.clearWavdata(tab[1])                      -- clear the temporary memory used to store wave data. 
    return tab[2]
end

--**************************************************--
--Function Name: wave_cut
--Description: cut the wave from start time to end time.
--Return Value: new wave data
--Parameter:
--  wave_data: the wave_data need to be cut. can be n-channels wave.
--  start_time: the start time of the old wave_data, in second or sample.
--              if this parameter is negative, the file will be
--                calculated from the beginning.
--  end_time: the end time of the old wave_data, in second or sample.
--              if this parameter is negative, the file will be
--                calculated from the end.
--  cut_unit:  1 or "ms" : Default value is second
--             2 or "sample": sample.
--Usage: new_wave_data = wave_cut(wave_data, 1000, 3000)
--       new_wave_data = wave_cut(wvae_data, 10, 1000, "sample")
--**************************************************--
function wave_cut(wave_data, start_time, end_time, cut_unit)
    cut_unit = cut_unit or 1
    if cut_unit == 2 or cut_unit == "sample" then
        return wave.cut(wave_data, start_time, end_time, cut_unit)
    end
    
	local time_last = wave.get_property(wave_data, "time_last")
	if start_time > time_last*1000 then
	    printc("function wave_cut() ERROR: time started is longer than time last !")
	    assert(false, "")
	    return wave_data
	end
	
	if end_time > time_last*1000 then
	    end_time = time_last*1000
	end
	
	return wave.cut(wave_data, start_time, end_time)
end

--**************************************************--
--Function Name: wave_load 
--Description: Load wave file into userdata wave.
--Return Value: new wave data.
--Parameter: wave_file-- wave file will be loaded, 
--                       it must has whole path.
--Usage: wave_data = wave_load("E:\\FM_SQA_Test_Noah\\Vector\\NE8K\\NE_American English.wav")
--**************************************************--
function wave_load(wave_file)
    local wave_data = wave.load(wave_file)
    if wave_data == nil then
        printc("function wave_load() ERROR: wave file is not exist !\n"..wave_file, 1)
    end
    return wave_data
end

--**************************************************--
--Function Name: wave_pesq.
--Description: Calculate the speech quality between specified wave data.
--Return Value: The result of PESQ_MOS.(0~4.5)
--Parameter: 
--  wave_org: Indicate the original wave data that will be calculated.
--            must be a mono wave.
--  wave_pro: Indicate the processed wavedata that will be calculated.
--            must be a mono wave.
--  time_table: Indicate the original and processed's start and end time, in second.
--              Default value is -1, which means the whole wave file
--              {{0,2}} means both original and processed time are from 0 to 2.
--              {{0, 2},{0.5, 2.5}} means origianl time is from 0 to 2,
--              processed time is from 0.5 to 2.5.
-- sync_flag: Indicate whether to trigger auto_sync.
--Usage: pesq = wave_pesq(org_wave_data, pro_wave_data)
--       pesq = wave_pesq(org_wave_data, pro_wave_data, {{0, 2}}, true)
--       pesq = wave_pesq(org_wave_data, pro_wave_data, {{0, 2}, {0.5, 2.5}}, true)
--**************************************************--
function wave_pesq(wave_org, wave_pro, time_table, sync_flag)
    if time_table == nil then
        print_error_info("function wave.pesq ERROR: the time is not been setting !")
        return 0
    end

    -- If the time_table is a number but is not -1, error.
    if type(time_table) == "number" and time_table ~= -1 then
        print_error_info("function wave.pesq ERROR: the setting of the time is not right !")
        return 0
    end

    local org_time_last = wave.get_property(wave_org , "time_last")
    local pro_time_last = wave.get_property(wave_pro , "time_last")

    -- If time_table's type is table, but the length is greater than 2, error.
    if type(time_table) ~= "table" or (#time_table ~= 1 and #time_table ~= 2) then
        print_error_info("function wave.pesq ERROR: the setting of the time is not right !")
        return 0
    end

    if #time_table == 1 and type(time_table[1]) == "table" and #time_table[1] == 2 then
        if time_table[1][1] >= org_time_last or time_table[1][1] >= pro_time_last then
            print_error_info("function wave.pesq ERROR: the start time is larger than time last")
            return 0
        end
        if time_table[1][2] > org_time_last or time_table[1][2] > pro_time_last then
            time_table[1][2] = org_time_last < pro_time_last and org_time_last or pro_time_last
        end
    elseif #time_table == 2 and type(time_table[1]) == "table" and type(time_table[2]) == "table" and #time_table[1] == 2 and #time_table[2] == 2 then
        if time_table[1][1] >= org_time_last or time_table[2][1] >= pro_time_last then
            print_error_info("function wave.pesq ERROR: the start time is larger than time last")
            return 0
        end
        time_table[1][2] = time_table[1][2] < org_time_last and time_table[1][2] or org_time_last
        time_table[2][2] = time_table[2][2] < pro_time_last and time_table[2][2] or pro_time_last
    else
        print_error_info("function wave.pesq ERROR: the setting of the time is not right !")
        return 0
    end

    return decimal_round(wave.pesq(wave_org, wave_pro, time_table, sync_flag), 2)
end

-- The A coefficient for wave.filter.
HPF_COEF_A = 
{
    _1K = 
    {
        1.000000000000000,
        -4.468491664362055,
        8.000298333211273,
        -7.167963916101014,
        3.210889033017562,
        -0.574566679099247
    },
    _2K = 
    {
        1.000000000000000,
        -4.737601372381500,
        9.496622089668069,
        -10.301957961947059,
        6.389245587016050,
        -2.157109079591156,
         0.312690622895091
    },
    _4K = 
    {
        1.000000000000000,
        -3.454173311839094,
        5.397844174213454,
        -4.715167264265913,
        2.470136658267126,
        -0.754004025292779,
        0.125337426315065
    },
}

-- The B coefficient for wave.filter.
HPF_COEF_B = 
{
    _1K = 
    {
        0.763194050805973,
        -3.815970254029867,
        7.631940508059735,
        -7.631940508059735,
        3.815970254029867,
        -0.763194050805973
    },
    _2K = 
    {
        0.507362310487481,
        -3.044173862924887,
        7.610434657312219,
        -10.147246209749625,
        7.610434657312219,
        -3.044173862924887,
        0.507362310487481
    },
    _4K = 
    {
        0.264287819373651,
        -1.585726916241907,
        3.964317290604766,
        -5.285756387473023,
        3.964317290604766,
        -1.585726916241907,
        0.264287819373651
    },
}

--**************************************************--
--Function Name: wave_HPF
--Description: high pass filter on wave_data.
--Return Value: new wave data.
--Parameter:
--  wave_data: Indicate the wave data that will be filtered.
--  cut_off_frequency: 1k, 2k or 4k, if no indication, nothing would be done.
--Usage: new_wave_data = wave_HPF(wave_data, 1000)
--**************************************************--
function wave_HPF(wave_data, cut_off_frequency)
    if cut_off_frequency == nil then
        return wave_data
    end

    local coef_b, coef_a = {}, {}
    if cut_off_frequency == 1000 then
        coef_b = HPF_COEF_B._1K
	    coef_a = HPF_COEF_A._1K
    elseif cut_off_frequency == 2000 then
        coef_b = HPF_COEF_B._2K
	    coef_a = HPF_COEF_A._2K
    elseif cut_off_frequency == 4000 then
        coef_b = HPF_COEF_B._4K
	    coef_a = HPF_COEF_A._4K
    else
        print_error_info("function wave_HPF() ERROR: the cut off frequency is not supported! only support 1k, 2k and 4k. "
                          ..cut_off_frequency)
    end

    return wave.filter(wave_data, coef_b, coef_a)
end

--**************************************************--
--Function Name: detect_delay
--Description: For synchronization, some test vector added a tone signal
--             at the end, so, in the recording, need to detect the tone
--             to calculate the delay.
--Return Value: the delay between the vector and the recording.
--Parameter:
--  wave_data: Indicate the wave data that will be calculated.
--             Must be a mono wave.
--  sample_THD: Indicate the threshold of sample that used to calculate.
--  time_start: Indicate the start time, in second.
--  time_end: Indicate the end time, in second.
--Usage: detect_delay(wave_data, 0.024, 263.5, 264.5)
--**************************************************--
function detect_delay(wave_data, sample_THD, time_start, time_end)
    local time_sync = 0
    local time_last = wave.get_property(wave_data , "time_last")
    local sample_rate = wave.get_property(wave_data , "sample_rate")
    
    if time_last < time_start then
        print_error_info("function detect_delay() ERROR: the start time is longer than last time !"
                         .." start time is "..time_start..", last time is "..time_last)
        return 0
    end
    
    if (time_last < time_end) or (time_end < 0) then
        --printc("NOTE: the end time is longer than last time !")
        time_end = time_last-0.01
    end

	local sample_table = wave.get_sample(wave_data, time_start * sample_rate + 1, time_end * sample_rate + 1)
	local num = 0
	local flg = 0

	while flg == 0 do
	  for i = 1, #sample_table do
	    if sample_table[i] - sample_THD > 0 or sample_table[i] + sample_THD < 0 then
	      time_sync = (math.floor(i / sample_rate * 1000))/1000
		  --printc("delay time is "..time_sync.."s")
		  flg = 1
		  break
	    end
	    num = i + 1
      end

      -- if cannot find any sample greater than sample_THD, then sample_THD = sample_THD / 2. Try again.
	  if num > #sample_table then
	    num = 0
	    sample_THD = sample_THD / 2
		if sample_THD < 0.006 then  -- The minimum value of sample_THD.
		  flg = 1
		  print_error_info("function detect_delay() ERROR: : script can not find the synchronize signal !")
          time_sync = time_end
		  break
		end
	  end
	  
    end
    collectgarbage("collect")
	return time_sync
end

--**************************************************--

--**************************************************--
function detect_spec_sample(wave_data, time_start, time_end, spec_type)
    local time_sync = 0
    local time_last = wave.get_property(wave_data , "time_last")
    local sample_rate = wave.get_property(wave_data , "sample_rate")
    spec_type = spec_type or "min"
    
    if time_last < time_start then
        print_error_info("function detect_delay() ERROR: the start time is longer than last time !"
                         .." start time is "..time_start..", last time is "..time_last)
        return 0
    end
    
    if time_last < time_end then
        printc("the end time is longer than last time !")
        time_end = time_last
    end

	local sample_table = wave.get_sample(wave_data, time_start * sample_rate + 1, time_end * sample_rate + 1)
    local point = 0
    local spec_num = 0
    
    if spec_type == "min" then
     	for i = 1, #sample_table do
     	    if sample_table[i] < point then 
     	        point = sample_table[i]
     	        spec_num = i
     	    end
     	end
    elseif spec_type == "max" then
     	for i = 1, #sample_table do
     	    if sample_table[i] > point then 
     	        point = sample_table[i]
     	        spec_num = i
     	    end
     	end
    elseif spec_type == "revert" then
        for i = 1, #sample_table do
     	    if sample_table[i] < point then 
     	        point = sample_table[i]
     	        spec_num = i
     	    end
     	end
     	for i = spec_num, #sample_table-1 do
     	    if (sample_table[i] < 0 and sample_table[i+1] > 0) then 
     	        point = sample_table[i]
     	        spec_num = i
     	        break
     	    end
     	end    
    end

    return spec_num + time_start * sample_rate, sample_table[spec_num]*32768
end
--**************************************************--
--Function name: decimal_round
--Description: To retain the specified number of decimal.
--Return Value: Return the new value.
--Parameter:
  --data_value: Indicate the decimal will be calculated.
  --decimal: the number will be retained, the default is 3
--Usage: pesq = decimal_rount(pesq, 2)
--**************************************************--
function  decimal_round(data_value, decimal)
    local a = 0
    local Decimal = decimal or 3
    
    if data_value == nil or tostring(data_value) == "-1.#QNAN" or tostring(data_value) == "-1.#INF" or tostring(data_value) == "-1.#IND" then
        print_error_info("function decimal_round ERROR: a nil value"..data_value)
        return 0 
    end
    
    data_value = data_value * (10^Decimal)
    a = data_value
    data_value = math.floor(data_value)
    
    if (a - data_value) >= 0.5 then 
        data_value = data_value + 1
    end
    
    data_value = data_value / (10^Decimal)
    return data_value
end

--**************************************************--
--Function name: get_average
--Description: calculate average of table
--Return Value: average.
--Parameter:
  --tab: Indicate the table will be calculated.
  --decimal: decimal number.
  --type: default 0. if set to 1, subb MAX and MIN.
--Usage: ave = get_average(tab, 2, type)
--**************************************************--
function get_average(tab, decimal, type)
    type = type or 0
    decimal = decimal or 3
    local sum = 0
    for i = 1, #tab do
        sum = sum + tab[i]
    end
    return decimal_round(sum / (#tab), decimal)
end

function get_MAX_MIN(tab)
    local Max = tab[1]
    local Min = Max
    for i = 1, #tab do
        if Max < tab[i] then
          Max = tab[i]
        elseif Min > tab[i] then 
          Min = tab[i] 
        end
    end
    return Max, Min
end

--**************************************************--
--Function name: copy_table
--Description: copy table A to B.
--Return Value: new table
--Parameter:
  --tabin: Indicate the table will be copied.
  --tabout: The new table
--Usage: copy_table(tabin, tabout)
--**************************************************--
function copy_table(tabin)
    local tabout = {}
	for k, v in pairs(tabin) do
		if type(v) ~= "table" then
			tabout[k] = v
		else
			tabout[k] = {}
			copy_table(tabin[k], tabout[k])
		end
	end
	return tabout
end

--**************************************************--
--Function name: hex_to_string
--Description: convert hex to string
--Return Value: string value
--Parameter:
  --data: the hex number
--Usage: string = hex_to_string(0x22c0)
--**************************************************--
function hex_to_string(data)
    if type(data) == "number" then
        return string.format("0x%X", data)
    end
--string.upper()
    if type(data) == "table" then
        local temp_data = {}
        for i = 1, #data do
            if type(data[i]) == "number" then
                temp_data[i] = string.format("0x%X", data[i])
            elseif type(data[i]) == "table" then
                temp_data[i] = {}
                for j = 1, #data[i] do
                    temp_data[i][j] = string.format("0x%X", data[i][j])
                end
            end
        end
        return temp_data
    end
end


function Convert_String_Table( str )

	 local data_tab = {}
	 local len = string.len( str )
	 
	 for i = 1, len do
        data_tab[i]= string.byte(str, i)           
     end
     data_tab[len +1 ] = 0

     return data_tab

end

function big_convert2_small(value, length)
--print(string.format("0x%0x", value))
    length = length or 2
    local tab = {}
    for i = length-1, 1, -1 do
        table.insert(tab, math.floor(value/(256^i)))
        value= value%(256^i)
    end
    table.insert(tab, value)
    --print(unpack(tab))
    value = tab[1]
    for i = 2, #tab do
        value = value + tab[i]*(256^(i-1))
    end
    --print(string.format("0x%0x", value))
    return value
end

--**************************************************--
--Function Name: line_to_token
------
--Description:  read string on the line
--Return Value: no
--Parameter:    line
--  line: 
--Usage: 
--**************************************************--
function line_to_token(line)
    local token = {}
    local line_start = string.sub(line, 1, 2)
    if line_start == "//" or line_start == "--" then return token end -- 如果行开头是"//" 或者"--"，表示此行数据无效，返回空表。
    local line_m = line
    if string.sub(line, 1, 1) ~= "@" and string.sub(line, 1, 1) ~= "#" then  --清除所有非字母和非数字字符
        line_m = string.gsub(line, "%W", " ")
    end
    local len = string.len(line_m)
    local idx = 0
    
    while idx < len do
        local pos = string.find(line_m, " ", idx + 1)
        if pos == nil then pos = len+1 end
        if pos ~= idx+1 then
            local t = string.sub(line_m, idx + 1, pos-1)
            if t ~= " " then table.insert(token, t) end
        end
        idx = pos
    end
    
    return token
end

--**************************************************--
function line_to_token1(line)
    local token = {}
    line = string.gsub(line, "	", " ")
    local len = string.len(line)
    local idx = 0
    while idx < len do
        local pos = string.find(line, " ", idx + 1)
        if pos == nil then pos = len+1 end
        if pos ~= idx+1 then
            local t = string.sub(line, idx + 1, pos-1)
            if t ~= ' ' then table.insert(token, t) end
        end
        idx = pos
    end
    
    return token
end

--**************************************************--
--standardization_spectrum
--**************************************************--
function standardization_spectrum(spectrum, standard_frequency)
    standard_frequency = standard_frequency or 1000
    local standard_num = math.floor(standard_frequency * (1024 / SAMPLE_RATE))
    --print(standard_num)
    local standard_value = spectrum[standard_num]
    for i = 1, #spectrum do
       spectrum[i] = spectrum[i] - standard_value
    end

    return spectrum
end


function db_to_hex(db_level)
    return decimal_round(32768 * 10^(- db_level/20), 0)
end

function hex_to_db(hex)
    return decimal_round(- math.log10(hex/32768) * 20, 1)
end

--flag: 1, equel; 2, contain
--case_sensitive: 1, case; 2, not care
function table_item_match(tab, source, flag, case_sensitive)
    flag = flag or 1
    case_sensitive = case_sensitive or 1

    local match = 0
    local match_tab = {}
    for i = 1, #tab do
        if type(source) == "number" and type(tab[i]) == "number" then
            if source == tab[i] then match = i break end
        elseif type(source) == "string" and type(tab[i]) == "string"  then
            if flag == 1 and case_sensitive == 1 then
                if source == tab[i] then match = i break end
            elseif flag == 1 and case_sensitive ~= 1 then
                if string.lower(source) == string.lower(tab[i]) then match = i break end
            elseif flag ~= 1 then
                local item = tab[i]
                if case_sensitive == 2 then
                    source = string.lower(source)
                    item = string.lower(item)
                end

                local source_word = line_to_token(source)
                --item = string.gsub(item, "%W", " ")
                local item_word = line_to_token(item)
--print(unpack(source_word))
--print(unpack(item_word))

                local str_item = ""
                for j = 1, #item_word do str_item = str_item..item_word[j] end
                for j = 1, #source_word - #item_word + 1 do
                    local str = ""
                    for k = 1, #item_word do str = str..source_word[j+k-1] end
                    if str == str_item then 
                        table.insert(match_tab, i)
                        match = i
                        break
                    end
                end
--print(match)
            end
        end
    end

    if #match_tab > 1 then
        local max = string.len(tab[match_tab[1]])
        for i = 1, #match_tab do
            if string.len(tab[match_tab[i]]) > max then
                max = string.len(tab[match_tab[i]])
                match = match_tab[i]
            end
        end
    end
    return match
end

--**************************************************--
--bit arithmetic functions
--**************************************************--
bit = bit or {}
bit.data32={}
for i=1,32 do
    bit.data32[i]=2^(32-i)
end
----
function bit:d2b(arg)
    local   tr={}
    for i=1,32 do
        if arg >= self.data32[i] then
        tr[i]=1
        arg=arg-self.data32[i]
        else
        tr[i]=0
        end
    end
    return   tr
end   --bit:d2b
----
function    bit:b2d(arg)
    local   nr=0
    for i=1,32 do
        if arg[i] ==1 then
        nr=nr+2^(32-i)
        end
    end
    return  nr
end   --bit:b2d
----
function    bit:_xor(a,b)
    local   op1=self:d2b(a)
    local   op2=self:d2b(b)
    local   r={}

    for i=1,32 do
        if op1[i]==op2[i] then
            r[i]=0
        else
            r[i]=1
        end
    end
    return  self:b2d(r)
end --bit:xor
----
function    bit:_and(a,b)
    local   op1=self:d2b(a)
    local   op2=self:d2b(b)
    local   r={}

    for i=1,32 do
        if op1[i]==1 and op2[i]==1  then
            r[i]=1
        else
            r[i]=0
        end
    end
    return  self:b2d(r)

end --bit:_and
----
function    bit:_or(a,b)
    local   op1=self:d2b(a)
    local   op2=self:d2b(b)
    local   r={}

    for i=1,32 do
        if  op1[i]==1 or   op2[i]==1   then
            r[i]=1
        else
            r[i]=0
        end
    end
    return  self:b2d(r)
end --bit:_or
----
function    bit:_not(a)
    local   op1=self:d2b(a)
    local   r={}

    for i=1,32 do
        if  op1[i]==1   then
            r[i]=0
        else
            r[i]=1
        end
    end
    return  self:b2d(r)
end --bit:_not
----
function    bit:_rshift(a,n)
    local   op1=self:d2b(a)
    local   r=self:d2b(0)
    if n == 0 then return a end
    if n < 32 and n > 0 then
        for i=1,n do
            for i=31,1,-1 do
                op1[i+1]=op1[i]
            end
            op1[1]=0
        end
    r=op1
    end
    return  self:b2d(r)
end --bit:_rshift
----
function    bit:_lshift(a,n)
    local   op1=self:d2b(a)
    local   r=self:d2b(0)
    if n == 0 then return a end
    if n < 32 and n > 0 then
        for i=1,n   do
            for i=1,31 do
                op1[i]=op1[i+1]
            end
            op1[32]=0
        end
    r=op1
    end
    return  self:b2d(r)
end --bit:_lshift
----
function    bit:print(ta)
    local   sr=""
    for i=1,32 do
        sr=sr..ta[i]
    end
    print(sr)
end

