EXCEL = 
{
   fill_color = 
   {
        red = 0x0000f0,
        green = 0x008000,
        blue = 0xff0000,
        white = 0xffffff,
        grey = 0xc0c0c0,
        black = 0x000000,
        yellow = 0x0080ff,
    };
    row_heigh = 12,
    column_width = 45.755 ;--(7); --51.755(8);   --63.755 (10);   --45.755 (7);
    range_space = 2;
    null = "no data";
}

EXCEL.__help = 
[==[
creat excel application: myexecl = luacom.CreateObject("Excel.Application");
serch excel: myexcel = luacom.GetObject("Excel.Application");

Work Book:
    operation: .Add; .Save; .SaveAs(filename); .Open(filename)
      creat workbook: newbook = excel.Workbooks.Add
      open workbook: mybook = excel.Workbooks.Open("myexcel.xls")
      save workbook: mybook.Save()
      saveas workbook: mybook.SaveAs("c:\\mynewbook.xls")
      
    attribution:  .Title; .Subject;

Work Sheet:
    activate work sheet: mybook.Worksheets(1).Activate; mybook.Worksheets("Sheet1").Activate
      mysheet = mybook.Worksheets(1)
      mysheet.Name = "mysheet"

Other object: Charts; DialogSheets; ChartObjects
    chart = excel.Charts:Add()
    chart.ChartType = 4 �� xlLine
    range = mysheet:Range(��A1:A30��)
    chart:SetSourceData(range)
    
Operate cell:
    mysheet.Cells.ClearContents; mysheet.[A1:B5].ClearContents
    mysheet.Cells(6,1).Value = 10; mysheet.Cells(counter, 1).Value = counter
    myrange = mysheet.Range("A1"); .Range("A1:B5"); .Range("C5:D9, G9:H16"); .Range("A:A"); .Range("1:1")
    myrange.Formula = "=RAND()"
    myrange.Font.Bold = True
    myrange.Value2 = "Hello world"
    myrange1 = mysheet.Range("A1:B2"), myrange2 = mysheet.Range("C3:D4")
    myMultipleRange = Union(myrange1, myrange2)
    Range("myrange").ClearContents
    Range.Select
    myrow = mysheet.Rows(1); myrow.Font.Bold = True
    mycolumns = mysheet.Columns("A")
   
Copy:
   myrange.Copy(destination); myrange.PasteSpeical()

Send mail:
    mybook.SendMail("junw@fortemedia.com", "Test")

PrintOut:
    mysheet.PrintOut(page_start, page_end, number, reveiw(true/false), printer_name��...��   
    
CVErr = {xlErrDiv0, xlErrNA, xlErrName, xlErrNull, xlErrNum, xlErrRef, xlErrValue}
]==]

function check_error(value)
    local str = ""
    if value == CVErr.xlErrDiv0 then
        str = ("#DIV/0! Error")
    elseif value == CVErr.xlErrNA then
        str = ("#N/A Error")
    elseif value == CVErr.xlErrName then
        str = ("#NAME? Error") 
    elseif value == CVErr.xlErrNull then
        str = ("#NULL! Error")
    elseif value == CVErr.xlErrNum then
        str = ("#NUM! Error")
    elseif value == CVErr.xlErrRef then
        str = ("#REF! Error")
    elseif value == CVErr.xlErrValue then
        str = ("#VALUE! Error")
    end
    printc(str)
    return st
end

function create_excel()
    local excel = luacom.GetObject("Excel.Application")
    if not excel then
        excel = luacom.CreateObject("Excel.Application")
    end
    
    if not excel then
        return false, "Can't instantiate Excel"
    end
    
    local typeinfo = luacom.GetTypeInfo(excel)
    local typelib = typeinfo:GetTypeLib()
    local enums = typelib:ExportEnumerations()

    return excel, enums
end



-- add a simple line chart from the data in "datasheet",
-- place the chart in "graphsheet"
function AddGraph(graphsheet, datasheet)

    local width, height = 640, 320
    local xOffset, yOffset = 32, 32
    -- add the chart at the specified location
    local chart = graphsheet:ChartObjects():Add(xOffset, yOffset, width, height).Chart
    -- set the title
    chart.HasTitle = true
    chart.ChartTitle.Text = graphsheet.Name .. " for " .. datasheet.Name
    -- set the chart type
    chart.ChartType = xlenums.XlChartType.xlLine
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    -- figure out how large our data set is
    local numRows = datasheet.Columns(1):End(xlenums.XlDirection.xlDown).Row
    local numColumns = datasheet.Rows(1):End(xlenums.XlDirection.xlToRight).Column
    
    -- this range describes the data set to add to the chart
    local sourceData = datasheet:Range(datasheet.Cells(1, 1), datasheet.Cells(numRows, numColumns))
    -- associate the source data with the chart
    chart:SetSourceData(sourceData)
    
    return chart
end

function convert_num2column(column) 
    local column_index = "A"
    if column < 27 then
        column_index = string.char(tostring(column+64))
    else
        local d = math.floor(column/26)+64
        local d1 = column%26 + 64
        --print(d, d1)
        if column%26 == 0 then 
            d = d - 1 
            d1 = d1 + 26
        end
        d = string.char(tostring(d))
        d1 = string.char(tostring(d1))
        --print(d, d1)
        column_index = d..d1
    end
    --print(column_index)
    return column_index
end

--[[
-- add a new workbook 
local book = excel.Workbooks:Add()
-- use sheet 1 for the data
local sheet1 = book.Worksheets(1)
sheet1.Name = "Data"
-- and sheet 2 for the graph
local sheet2 = book.Worksheets(2)
sheet2.Name = "Chart"

excel.Visible = true
excel.DisplayAlerts = false

math.randomseed(os.time())

-- create three columns, 100 entries each, of data
for col=1, 3 do
-- row 1: title
sheet1.Cells(1, col).Value2 = ("Data series %d"):format(col)
-- row 2-100: data
for row=2, 100 do
sheet1.Cells(row, col).Value2 = math.floor(math.random() * 100)
end
end

-- add a graph on sheet 2, taking its data from sheet 1
AddGraph(sheet2, sheet1)

book:SaveAs("d:\\Noah_for_debug\\mygraph.xlsx")
book:Close()
]]
