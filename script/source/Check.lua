check_version = "v0.1"
--**************************************************--
--Function List:
--  check_power
--  check_PESQ
--  check_DSP_PM
--  check_DSP_run
--  check_audio
--  check_DM
--  check_file_exist
--  
--**************************************************--

--**************************************************--
--Function Name: check_power
--Description:   check recording file power level
--Return Value:  1: abnormal; 0: normal
--Parameter:
--  wave_file:   recording file path and name
--  record_time: theory recording time
--  start_time, end_time: the start time and end time of the part will been checked.
--  threshold:   0 or nil: just check power level of itself;
--               other number: check power, then compare with other wave file, for stress test.
--  item_num:   test time, for stress test.
--  cut_off_frequency: Indicate whether need to filter, for special test vector, example tone noise.
--Usage:        check_power("e:\\1.wav", 10, 1, 3, 0, 1)
--**************************************************--
check_power_flag = 0
function check_power(wave_file, record_time, start_time, end_time, threshold, item_num, cut_off_frequency)
    collectgarbage("collect")
    item_num = item_num or 1
    local wave_data = wave_load(wave_file)
    local wave_data_array = wave.n_to_mono(wave_data)
    local time_last = wave.get_property(wave_data_array[2], "time_last")
    if time_last < record_time - 0.5 then
        print_error_info("function check_power() ERROR: wave file is too short ! "..wave_file)
        --check_DSP_run()
        return 1
    end

    -- ���������£���ʼʱ��������ʱ���ļ�����С��60�ģ��������ж���Ϊ�˱�������ʱ��̫����ռ���ڴ�������Ӱ�����������ٶȡ�
    if end_time - start_time > 60 then end_time = start_time + 60 end
    local power1 = Wave.power_rmdc(wave_data_array[1], start_time, end_time, cut_off_frequency)
    local power2 = Wave.power_rmdc(wave_data_array[2], start_time, end_time, cut_off_frequency)
    --local power2 = power1
    
    Wave.clearWavdata(wave_data_array[1])
    Wave.clearWavdata(wave_data_array[2])
    Wave.clearWavdata(wave_data)
    collectgarbage("collect")
    
    if (power1 <= POWER_THD) or (power2 <= POWER_THD) or power1 == 0 or power2 == 0 then
        print_error_info("function check_power() ERROR: wave is empty! "..wave_file)
        return 1
    end

    printc("Power level is normal!")
    printc("Left power: "..power1..", Right power: "..power2.."\n")

    -- ����thresholdû�裬�򲻽��������Աȡ�
    threshold = threshold or 0
    if threshold == 0 then return 0  end

    if check_power_flag == 0 then
        check_power_THD = power2
        check_power_flag = 1
        return 0
    end

    if ((power2 - check_power_THD > threshold) or (power2 - check_power_THD < -threshold)) then
        print_error_info("function check_power() ERROR: power level is different with other wave file! "
          .."\n  time "..item_num..":"..power2.." - "..check_power_THD.." > "..threshold) 
        return 1
	end
	
	return 0
end

--**************************************************--
--Function Name: check_DSP_PM
--Description: check the PM of DSP.
--Return Value: 0 or 1
--Parameter: no
--Usage: check_DSP_PM()
--**************************************************--
function check_DSP_PM()
  wait(0.2)
  if mcu.read_dm(0x3fa0) ~= 0x91cb then
    printc("\nTest time is : "..tostring(STRESS_REAL_TIME))
    printc("DSP patch is not effective!")
    PATCH_error_count = PATCH_error_count + 1
    printc("Patch download error time is: "..tostring(PATCH_error_count))
    printc("0x3fa0 = "..tostring(string.format("%0x", mcu.read_dm(0x3fa0))))
    for i = 1, 127 do
      Read_PM(0x41000 + i)
    end
    return 0
  else 
	local a = Read_PM(0x41000+10)
	local b = Read_PM(0x41000+100)
    if a ~= 0x944fb6 or b ~= 0x22621f then  --patch_vec[23 + 100]
      printc("\nTest time is : "..tostring(STRESS_REAL_TIME))
      printc("DSP patch is not error!")
      PATCH_error_count = PATCH_error_count + 1
      printc("Patch download error time is: "..tostring(PATCH_error_count))
      return 0
    else 
      printc("\nDownload DSP patch is successful!")
      return 1
    end
  end
end


--**************************************************--
--Function Name: check_DSP_run
--Description: check the status of DSP, run or stop.
--Return Value: no
--Parameter: no
--Usage: check_DSP_run()
--**************************************************--
DSP_error_count  = 0
STRESS_REAL_TIME = 1

function check_DSP_run()
    printc("Check the status of DSP")
    if CHIP_NAME == "iM401" then
        if a  == 2 or a == 3 then
            printc("DSP is running...")
            return true
        end
    end

    if CHIP_NAME == "iM501" then
        return check_pass_fail() 
    end
	
	local a = read_DSP(CHIP.frame_counter)
	wait(0.1)
	local b = read_DSP(CHIP.frame_counter)

	-- if frame counter is nil or 0xcccc, indicated the DSP is stop.
	-- then read frame counter twice, if the result is same, indicated DSP is stop, if the result is different, DSP is running.


	if a == nil or b == nil or a == b  then 
		DSP_error_count = DSP_error_count + 1
		printc("\nTest time is : "..tostring(STRESS_REAL_TIME))
		printc("DSP is dead!")
		printc("\nDSP error time is: "..tostring(DSP_error_count).."\nFrame counter = "..a) 
		return false
	else
		printc("DSP is running...")
		return true
	end
end


--**************************************************--
--Function Name: check_audio
--Description: check if audio device is exist.
--Return Value: true or false
--Parameter: 
-- audio_dev: indicate the device name will be checked.
--Usage: check_audio("Jupiter_AudioDP")
--**************************************************--
function check_audio(audio_dev)
    local check_flag = false
    local audio_dev_exist = audio.get_dev(1)
    
    for i = 1, #audio_dev_exist do
        --print_debug(audio_dev_exist[i])
        
        if audio_dev == audio_dev_exist[i] then
            check_flag = true
            break
        end
        
    end

    return check_flag
end

--**************************************************--
--Function Name: check_DM
--Description: check DM
--Return Value: no, it has print info.
--Parameter:
-- address: DM address.
-- value: theoretical value.
--Usage: check_DM(0x2300, 0)
--**************************************************--
function check_MMR(address, value)
    if value == read_DSP(address) then
        printc("MMR value is right!")
    else
        printc("MMR value is wrong.")
    end
end

function check_soundcard(play_device)
    local play_dev_tab = audio.get_dev(1)
    for i = 1, #play_dev_tab do
        if play_device == play_dev_tab[i] then return play_device end
    end
    printc("\nPC Sound setting is wrong, no this paly device "..play_device.."\n")
    for i = 1, #play_dev_tab do
        printc(i..": "..play_dev_tab[i])
    end
    --print("Which one you want use to play, please input the number.")
    local i = 1--io.read() + 0
    printc("Use "..play_dev_tab[i].." to play\n")
    return play_dev_tab[i]
end

function soundcard_select(device_type)
    device_type = device_type or 1
    local dev_tab = audio.get_dev(device_type)
    if dev_tab == nil  then return {} end
    for i = #dev_tab, 1, -1 do
        if string.find(dev_tab[i], "PA:") ~= nil then table.remove(dev_tab, i) end
    end
    printc(dev_tab)
    return dev_tab
end

function backticks_string(cmd)
	local string
	local pipe = io.popen(cmd)
	local line = pipe:read("*all")
	return line
end

function check_task(image)
    local cmd = "tasklist /fo LIST /fi ".."\"Imagename eq "..image.."\""
    local line = backticks_string(cmd)
    --print(line)
    --io.read()
    if( line == nil ) then
        return false
    end
    
    if string.find(line, image) ~= nil then return true end

    return false
end


function check_task1(image)
    os.execute("tasklist /fo LIST /fi ".."\"Imagename eq "..image.."\" > tasklist.txt")
    local task_list = io.open("tasklist.txt","r")
    if( task_list == nil ) then
        return false
    end
    for line in task_list:lines(task_list) do
        --print(line, image)
        if string.find(line, image) ~= nil then task_list:close() return true end
    end

    task_list:close()
    return false
end

wait_task_finish = function(exe_name)
    local sec1,sec2 = os.date("%S", os.time())
    local start_time = os.time()
    local calc_state = true
    printc("\n"..exe_name.." is running. Please wait.")
    repeat
        sec2 = os.date("%S", os.time())
        if sec2 - sec1 > 2 or sec2 - sec1 < 0 then
            sec1 = sec2
            local cost_t  =  os.time() - start_time
	        printc(string.format("%d M : %d S", cost_t/60, cost_t%60))
            calc_state = check_task(exe_name) --("BatchCalculatorTool.exe")
            --if calc_state then printc(exe_name.." is running, please wait.") end
        end
    until calc_state == false
end

function check_project_file(path)
    print(path)
    local project_dir = {"ACQUA", "TestVector", "Parameter"}
    local project_file = {"wavefile_attribute.json", "ACQUA_score.txt"}
    local state, error = true, 0
    if lfs.attributes(path) == nil then
        message("Error", "    No project files in project directory.\n Please copy file to this folder: "..path)
        lfs.create_folder(path)
        for i = 1, #project_dir do lfs.create_folder(path.."\\"..project_dir[i]) end
        if SYNC.ACQUA_file_attr.save_style == 1 then
            for i = 1, #NOISE do lfs.create_folder(path.."\\"..project_dir[1].."\\"..NOISE[i]) end
        end
        for i = 1, #project_file do
            os.execute("copy \""..TEMPLATE_PATH..project_file[i].."\" \""..path.."\\"..project_file[i].."\" ".."/Y")
        end
        state, error = false, 0
    else
        local dirs = getdirs(path)
        if #dirs == 0 then
            for i = 1, #project_dir do lfs.create_folder(path.."\\"..project_dir[i]) end
            if SYNC.ACQUA_file_attr.save_style == 1 then
                for i = 1, #NOISE do lfs.create_folder(path.."\\"..project_dir[1].."\\"..NOISE[i]) end
            end
            message("Error", "    No project files in project directory.\n Please copy file to this folder: "..path)
            state, error = false, 1
        else
            for i = 1, #project_dir do
                local flag = 0
                for j = 1, #dirs do
                    if dirs[j] == project_dir[i] then flag = j break end
                end
                if flag == 0 then 
                     lfs.create_folder(path.."\\"..project_dir[i])
                     message("Error", "    No project directory: "..path.."\\"..project_dir[i]..".\n Generate it.")
                     state, error = false, i+1
                     if i == 1 and SYNC.ACQUA_file_attr.save_style == 1 then
                         for j = 1, #NOISE do lfs.create_folder(path.."\\"..project_dir[1].."\\"..NOISE[j]) end
                     end
                end
            end
        end
        
        local files = getfiles(path, ".")
        if #files == 0 then
            local str = ""
            for i = 1, #project_file do
                os.execute("copy \""..TEMPLATE_PATH..project_file[i].."\" \""..path.."\\"..project_file[i].."\" ".."/Y")
                str = str..path..project_file[i].." "
            end
            message("Error", "    Please check these files first: "..str)
            state, error = false, 5
        else
            for i = 1, #project_file do
                local flag = 0
                for j = 1, #files do
                    if files[j] == project_file[i] then flag = j break end
                end
                if flag == 0 then 
                     os.execute("copy \""..TEMPLATE_PATH..project_file[i].."\" \""..path.."\\"..project_file[i].."\" ".."/Y")
                     message("Error", "    Please check this file first: "..path.."\\"..project_file[i])
                     state, error = false, i+5
                end
            end
        end
    end
    return state, error
end