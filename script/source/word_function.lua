WORD = 
{
   fill_color = 
   {
        red = 0x0000f0,
        green = 0x008000,
        blue = 0xff0000,
        white = 0xffffff,
        grey = 0xc0c0c0,
        black = 0x000000,
        yellow = 0x0080ff,
    };

    null = "no data";
}

WORD.__help = 
[==[
creat word application: myexecl = luacom.CreateObject("Word.Application");
serch word: myword = luacom.GetObject("Word.Application");

]==]


function create_word()
    local word = luacom.GetObject("Word.Application")
    if not word then
        word = luacom.CreateObject("Word.Application")
    end
    
    if not word then
        return false, "Can't instantiate Word"
    end
    
    return word
end



