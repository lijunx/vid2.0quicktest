--Function List:
--[[
iM205.
{
  init(),
  write_reg(),
  read_reg(),
}
]]
--**************************************************--
iM205 = 
{
    I2C_addr = 0xA4,
    SDA_GPIO = 9,
    SCL_GPIO = 8,
    vec =      --** vec files. Pls put these files in the path of CODE_VEC_PATH.
       {
        init  = "init.vec",
        run   = "run.vec",
        reset = "reset.vec",
       },
    ATTRI = 11,
    write_cmd = 0x40,
    read_cmd = 0xc0,
    protocol = 1,  --1 or 2, with restart(gpio8&9) or without restart(Host I2C bus). default 1
}
    
iM205.__help = {}

--*****************
iM205.__help.init =
[==[
Usage: iM205.init() 
  Init DSP, download the vec file: DSP.vec_init, that defined in "config"
  Param: No
  return: true or false
]==]
iM205.init = function()
    printc("\nInitialize iM205")
    local str = ""--"    If you do not use UIF to control iM205, please ignore this warning.\n"
    if iM205.protocol == 1 then
        UIF.set_i2c_gpio(100, iM205.SDA_GPIO, iM205.SCL_GPIO)
        if DSP_NAME == "iM501" then 
            local str1 = "  iM205 will be controlled via GPIO of UIF board, you need to switch the jumper of J23 from 1&3 to 3&5."
            --message("Warning", str..str1)
            printc(str..str1)
        elseif DSP_NAME == "iM205" then
            local str1 = "  iM205 will be controlled via GPIO of UIF board, you need to connect R118&R120 and disconnect R117&R119."
            --message("Warning", str..str1)
            printc(str..str1)
        end
    elseif DSP_NAME == "iM205" and iM205.protocol == 2 then
        local str1 = "  iM205 will be controlled via I2C of UIF board, you need to disconnect R118&R120 and connect R117&R119."
        --message("Warning", str..str1)
        printc(str..str1)
    else
       message("Warning", "You can not access iM205 under this condition.")
       return false
    end
    return true
end

--*****************
iM205.__help.write_reg =
[==[Usage: iM205.write_reg(reg_addr, reg_data)
  write addr = data.
Param:
    reg_addr     register address.
    reg_data     Thedata will be written.
return: true or false
]==]
iM205.write_reg = function (reg_addr, reg_data)
    if iM205.protocol == 1 then
        local state = false
        UIF.set_interface(UIF.type.I2C, 100, iM205.ATTRI, 0)
        --state = UIF.raw_write(UIF.type.I2C, iM205.I2C_addr, {iM205.write_cmd, reg_addr, reg_data})
        state = UIF.raw_write(UIF.type.I2C, iM205.I2C_addr, {reg_addr, reg_data})
        printc("\nWrite 0x"..string.format("%X", reg_addr).." = ".."0x"..string.format("%X", reg_data))
        UIF.set_interface(UIF.type.I2C, I2C_SPEED, 0)
    elseif iM205.protocol == 2 then
        local state = false
        state = UIF.raw_write(UIF.type.I2C, iM205.I2C_addr, {iM205.write_cmd, reg_addr, reg_data})
        printc("\nWrite 0x"..string.format("%0X", reg_addr).." = ".."0x"..string.format("%0X", reg_data))
    end
    return state
end

--*****************
iM205.__help.read_reg =
[==[Usage: iM205.read_reg(addr)
  read data of addr.
Param:
    addr     register address.
return:
    data     if successful, return the data of read. else return false.
]==]
iM205.read_reg = function (reg_addr)
    if iM205.protocol == 1 then
        UIF.set_interface(UIF.type.I2C, 100, iM205.ATTRI, 0)
        --local value = UIF.raw_read(UIF.type.I2C, iM205.I2C_addr, 1, {iM205.read_cmd, reg_addr})
        local value = UIF.raw_read(UIF.type.I2C, iM205.I2C_addr, 1, {reg_addr})
        printc("\nReg "..string.format("%X", reg_addr).."= 0x"..string.format("%X", value[1]))
        UIF.set_interface(UIF.type.I2C, I2C_SPEED, 0)
        return true, value[1]
    end
    if iM205.protocol == 2 then
        local value = UIF.raw_read(UIF.type.I2C, iM205.I2C_addr, 1, {iM205.read_cmd, reg_addr})     
        printc("\nReg "..string.format("%X", reg_addr).."= 0x"..string.format("%X", value[1]))
        return true, value[1]
    end
end

--*****************
iM205.__help.avd_mode =
[==[Usage: iM205.avd_mode()
  read data of addr.
Param:  no
return: no
]==]
iM205.avd_mode = function ()
    iM205.write_reg(6,0x48)
    iM205.write_reg(1,0x38)
end

--*****************
iM205.__help.load_vec =
[==[
Usage: iM205.load_vec() 
  Download parameters to DSP.
  Param:  vec_file
  Return: no
]==]
iM205.load_vec = function(vec_file)
    printc("\n******************** Download parameters")
    print(vec_file)
    local file = io.open(vec_file, "r") --assert(io.open(vec_file, "r"), "function load_vec() ERROR: Can't find the vec file! Please check it! "..vec_file)
    if file == nil then
        message("ERROR", "function load_vec() ERROR: Can't find the vec file! Please check it! "..vec_file)
    end
    printc("vec file name is: "..vec_file)
    local i, address, para = 0
    
    for line in file:lines(file) do
        i = i + 1
        local token = line_to_token (line)
        for j = 1, #token do
            if string.find(token[j], "0x") == nil and string.find(token[j], "0X") == nil then  --如果地址和数据前没加OX，补上。
                token[j] = "0x"..token[j]
            end
        end

        if #token >= 2 then
            address, para = tonumber(token[1]), tonumber(token[2])
            printc(string.format("0x%0X", address).." = "..string.format("0x%0X", para))
            iM205.write_reg(address, para)
        elseif #token ~= 0 then 
            printc("LOG_READ_MAP(line "..tostring(i)..", token_num "..tostring(#token).."): "..line)
        end
    end
    file:close()
    printc("******************** Down parameters successful! ^_^\n") 
end

--DSP = iM205
