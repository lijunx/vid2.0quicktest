--****NOTE:in iup library, there is no number value, need use tonumber() or +0.
----
GUI = {}
----Color setting
GUI.color = {
    blue = "0 0 255",
    red = "255 0 0",
    black = "0 0 0",
    green = "25 128 25",
    white = "255 255 255",
    gray = "220 220 220",
}
GUI.constant = 
{
    switch_on = "ON";
    switch_off = "OFF";
}
--module define
OUTPUT = {
    mic0_bypass = "TDM/PDM s1";
    mic1_bypass = "TDM/PDM s2";
    file_in =  "Wavefile CH1";
}

dofile(SCRIPT_PATH..sub_dirctory.."Play_record.lua")
dofile(SCRIPT_PATH..sub_dirctory..regression.config.platform.module..".lua")
dofile(SCRIPT_PATH..sub_dirctory.."FM36.lua")
dofile(SCRIPT_PATH..sub_dirctory.."audio_bridge.lua")
dofile(SCRIPT_PATH..sub_dirctory..DSP_NAME..".lua")
dofile(SCRIPT_PATH..sub_dirctory.."iM205.lua")
CLIENT_MODE = ""

ABengine = io.popen(regression.config.platform.ABengine)
AB_connect_GUI()
UIF.get_board_ver()
if UIF.version == "N/A" then
    AB_disconnect()
    wait(1)
    AB_connect()
end

if not UIF.Check_FW_Version( Host_FW_Ver, Audio_FW_Ver ) then control.stop() end

dofile(SCRIPT_PATH..sub_dirctory.."GUI_global_function.lua")
dofile(SCRIPT_PATH..sub_dirctory.."GUI_system_update_server.lua")
--[[
os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
print("copy json file !!!!")
collectgarbage("collect")
wait(0.1)
GUI.kernel = load_jsonfile1(TEMP_JSON)  -- this table is the kernel daatbase of GUI.
--printc(dump(GUI.kernel))
]]--
print("111")
os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
print("copy json file done !!!!")
GUI.kernel = load_jsonfile1(TEMP_JSON)
print("load json file done !!!!")
--collectgarbage("collect")
print("222")
--**************************************************--
dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_audio_path_setting.lua")
dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_command_path_setting.lua")
dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_ab_setting.lua")
dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_waveview.lua")
dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_im205_rw.lua")
if DSP_NAME == "iM501" then
    dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_im501_rw.lua")
    dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_im501_vr_setting.lua")
    dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_im501_VID.lua")
    dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_VR.lua")
    UIF.set_interface(UIF.type.CHIP_TYPE, 0, UIF.attribute.CHIP_TYPE_IM501)
end
GUI.system_server.toggle.auto_update.value = "ON"
--dofile(SCRIPT_PATH..sub_dirctory.."GUI_module_VR.lua")
--PDM_UPDATE_EN = true
dofile(SCRIPT_PATH..sub_dirctory.."GUI_UIF_setting.lua")
--require(SCRIPT_PATH..sub_dirctory.."GUI_design_manage")
update_system_setting(GUI.system_server.toggle.auto_update.value, "OFF")
GUI.audio_path.local_frame.detail.active1()

