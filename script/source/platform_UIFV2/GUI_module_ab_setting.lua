--module define

out_node_src =  --default, will be change base on new setting.
{
   "TDM/PDM s1",
   "TDM/PDM s2",
   "TDM/PDM s3",
   "TDM/PDM s4",
   "GPIO2(IRQ)",
   "GPIO3(xSD)",
   "GPIO5(AVD)",
   "Wavefile CH1",
   --"Soundcard L",
   --"Soundcard R",
}
GPIO_LIST = {"GPIO2(IRQ)","GPIO3(xSD)","GPIO4","GPIO5(AVD)","GPIO6","GPIO7"}
SPI_LIST = {"SPI1:MIC0","SPI2:MIC1","SPI3:MIC2","SPI4:MIC3","SPI5:AECREFL","SPI6:AECREFR","SPI7:LIN","SPI8:LOUT","SPI9:SPKOUT","SPI10:DBG_INFO",}
DEBUG_INFO_LIST1 = 
{
"DBG:MAX_MIC0_READOUT",       
"DBG:MAX_MIC1_READOUT",       
"DBG:MAX_MIC2_READOUT",       
"DBG:MAX_MIC3_READOUT",       
"DBG:MAX_LIN_READOUT",       
"DBG:MAX_LINEOUT_READOUT",    
"DBG:MAX_SPKOUT_READOUT",    
"DBG:FE_PITCH_T0_READOUT",    
"DBG:NE_PITCH_T0_READOUT",    
"DBG:PREVAD_READOUT",    
"DBG:DOA_PAIR0_READOUT",    
"DBG:DOA_PAIR1_READOUT",    
"DBG:DOA_PAIR2_READOUT",    
"DBG:GLBVAD_PAIR0_READOUT",   
"DBG:GLBVAD_PAIR1_READOUT",   
"DBG:GLBVAD_PAIR2_READOUT",   
"DBG:DVFLAG_0_READOUT",   
"DBG:DVFLAG_1_READOUT",   
"DBG:DVFLAG_2_READOUT",   
"DBG:SN_FLAG_READOUT",   
"DBG:SE_FLAG_READOUT",   
"DBG:DT_FLAG_READOUT",   
"DBG:LF_VAD_READOUT",   
}
DEBUG_INFO_LIST2 =            
{
"DBG:LFVAD_NFLOOR_READOUT",   
"DBG:MEAN_RTO_VAD_READOUT",   
"DBG:MIN_FQ_ERL_READOUT",   
"DBG:MIN_EQ_RHO0_READOUT",   
"DBG:MIN_EQ_RHO1_READOUT",   
"DBG:MIN_EQ_RHO2_READOUT",   
"DBG:MIN_EQ_RHO3_READOUT",   
"DBG:MIN_EQ_RHO4_READOUT",   
"DBG:MIN_EQ_RHO5_READOUT",   
"DBG:MIN_EQ_RHO6_READOUT",   
"DBG:DT_RATIO0_READOUT",      
"DBG:DT_RATIO1_READOUT",      
"DBG:DT_RATIO2_READOUT",      
"DBG:DT_RATIO3_READOUT",      
"DBG:DT_RATIO4_READOUT",      
"DBG:DT_RATIO5_READOUT",      
"DBG:DT_RATIO6_READOUT",      
"DBG:B_POSTFLT_READOUT",      
"DBG:FFP_GAIN_READOUT",      
"DBG:BVE_FINAL_GAIN0_READOUT",
"DBG:BVE_FINAL_GAIN1_READOUT",
"DBG:BVE_FINAL_GAIN2_READOUT",
"DBG:BVE_FINAL_GAIN3_READOUT",
}
sync_waveview_value = "OFF"

printc("Define Unified Interface Path GUI module.")
GUI.ab_setting = {}
GUI.ab_setting.constant =
{
    ab_in_default = 2,
    ab_in_max = 8,
    ab_out_max = 8,
    spi_in_max = 10,
    spi_out_max = 8,
    spi_in_default = "++++++++++",
    debug_info1_default = "--------------------",
    debug_info2_default = "--------------------------",
    debug_info_max = 8;
    gpio_in_default = "++-+--",
    gpio_in_tab_default = {1,1,0,1,0,0},  
    spi_in_tab_default = {1,1,1,1,1,1,1,1,1,1},
    debug_info1_tab_default = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    debug_info2_tab_default = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    file_in_default = NOAH_PATH.."Vector\\vector_"..SAMPLE_RATE..".wav",
    --NOAH_PATH.."Vector\\vector_"..tostring(GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0])..".wav",
    file_out_default = RECORD_PATH..RECORD_FILE,
    sc_out_l_default = 1,
    sc_out_r_default = 1,
    file_out_sel_default = {out_node_src[1],out_node_src[2],out_node_src[3],out_node_src[4],out_node_src[7],out_node_src[6],out_node_src[5],},
    waveview_default = {out_node_src[1],out_node_src[2],out_node_src[3],out_node_src[4],out_node_src[7],out_node_src[6],out_node_src[5],},
    ab_out_default = {out_node_src[1],out_node_src[2],out_node_src[3],out_node_src[4]},
    spi_out_default = {out_node_src[1],out_node_src[1],out_node_src[1],out_node_src[1],out_node_src[1],out_node_src[1],out_node_src[1],out_node_src[1]},
}

temp_out_node_src = {}--out_node_src
temp_out_node_src = copy_table(out_node_src)
--GUI.ab_setting.constant.file_out_src_default = copy_table(out_node_src)

channels_label = {}

GUI.ab_setting.label = 
{
    ab_in = iup.label{size="30x16";title = [==[Channel Number]==]};
    file_in = iup.label
    {
        size = "70x";
        title= [==[Test vector for simulation testing]==];
    };
    sc_in = iup.label{title=[==[]==]};
    gpio_in = iup.label{size="30x";title=[==[]==]};
    ab_out = iup.label{title=[==[Connected to DSP for simulation testing]==]};
    file_out = iup.label{title=[==[ ]==]};
    waveview = iup.label{title=[==[Realtime display]==]};
    sc_out = iup.label{title=[==[]==]};
    sc_out_l = iup.label{title=[==[Left:]==]};
    sc_out_r = iup.label{title=[==[Right:]==]};
    src = iup.label{title="Source"};
    sel_src = iup.label{title="Selected Source"};
    sep1 = iup.label{fgcolor = GUI.color.gray, size="x5";title="---------------------------------------------------------------------------------------------------------------------------"};
    sep2 = iup.label{fgcolor = GUI.color.gray, size="x5";title="---------------------------------------------------------------------------------------------------------------------------"};
    sep3 = iup.label{fgcolor = GUI.color.gray, size="x5";title="---------------------------------------------------------------------------------------------------------------------------"};
    sync_waveview = iup.label{title="Same with Waveviewer", size="49x16"};
    spi_out_name = iup.label{size="34x64", title="     MIC0\n     MIC1\n     MIC2\n     MIC3\nAecRefL\nAecRefR\n        LIN"};
    out_source_name = iup.label{size="34x64", title=[==[]==]};
}

GUI.ab_setting.toggle = 
{
    ab_in = iup.toggle{title="Enable",value="ON";};
    file_in = iup.toggle{title="En",value="ON";};
    sc_in = iup.toggle{title="Enable",value="OFF";};
    spi_in = iup.toggle{title="Enable",value="OFF";};
    gpio_in = iup.toggle{title="GPIO",value="OFF";};
    debug_info = iup.toggle{title="Decode Debug Info",value="OFF";};
    ab_out = iup.toggle{title="Enable",value="ON";};
    spi_out = iup.toggle{title="Enable",value="OFF";};
    file_out = iup.toggle{title="En",value="ON";};
    waveview = iup.toggle{title="Enable",value="ON";};
    sync_waveview = iup.toggle{title="",value="ON"};
    sc_out = iup.toggle{title="Enable",value="OFF";}; 
}

GUI.ab_setting.list = 
{
    node = {"ab_in","spi_in","file_in","sc_in","gpio_in","ab_out","spi_out","file_out","waveview","sc_out"},
----------- input node
    ab_in = iup.list
    {
        "1","2","3","4","5","6","7","8";
        dropdown="YES", visible_items=8, value = GUI.ab_setting.constant.ab_in_default, size="30x", action_time = false,
    };
    sc_in_dev = iup.list
    {
        dropdown="YES", visible_items=#sc_in_dev_list, value = 1, len = 0, size="95x", active = "no", action_time = false,
    };
    spi_in = iup.list
    {
        --"SPI1:MIC0","SPI2:MIC1","SPI3:MIC2","SPI4:MIC3","SPI5:AECREFL","SPI6:AECREFR","SPI7:Lin","SPI8:Lout","SPI9:Spkout","SPI10:VADs";
        multiple="YES", value = GUI.ab_setting.constant.spi_in_default, len = 0,sample_rate=16000,
        value_tab=GUI.ab_setting.constant.spi_in_tab_default,size="100x90",action_time = false, type = "spi_in"
    };
    sel_list = iup.list
    {
        --"SPI1:MIC0","SPI2:MIC1","SPI3:MIC2","SPI4:MIC3","SPI5:AECREFL","SPI6:AECREFR","SPI7:Lin","SPI8:Lout","SPI9:Spkout","SPI10:VADs";
        multiple="YES", value = GUI.ab_setting.constant.spi_in_default, len = 0,
        value_tab={},size="100x90",action_time = false, type = "spi_in"
    };
    debug_info1 = iup.list
    {
        multiple="YES", value = GUI.ab_setting.constant.debug_info1_default, len = 0,
        value_tab=GUI.ab_setting.constant.debug_info1_tab_default,size="150x190",action_time = false, type = "debug_info"
    };
    debug_info2 = iup.list
    {
        multiple="YES", value = GUI.ab_setting.constant.debug_info2_default, len = 0,
        value_tab=GUI.ab_setting.constant.debug_info2_tab_default,size="150x190",action_time = false, type = "debug_info"
    };
    debug_sel1 = iup.list
    {
        multiple="YES", value = GUI.ab_setting.constant.spi_in_default, len = 0,
        value_tab={},size="150x190",action_time = false, type = "debug_info"
    };
    debug_sel2 = iup.list
    {
        multiple="YES", value = GUI.ab_setting.constant.spi_in_default, len = 0,
        value_tab={},size="150x190",action_time = false, type = "debug_info"
    };
    gpio_in = iup.list
    {
        "GPIO2(IRQ)","GPIO3(xSD)","GPIO4","GPIO5(AVD)","GPIO6","GPIO7";
        multiple="YES", value = GUI.ab_setting.constant.gpio_in_default, len = 0,
        value_tab=GUI.ab_setting.constant.gpio_in_tab_default,size="100x90",action_time = false, type = "gpio_in"
    };
---------- output node
    sc_out_dev = iup.list
    {
        dropdown="YES", visible_items=#sc_out_dev_list, value = 1, len = 0, size="95x", active = "no", action_time = false,
    };
    sc_out_l = iup.list
    {
        dropdown="YES", visible_items=#out_node_src, size="70x",
        value = GUI.ab_setting.constant.sc_out_l_default, active = "no", action_time = false,
    };
    sc_out_r = iup.list
    {
        dropdown="YES", visible_items=#out_node_src, size="70x",
        value = GUI.ab_setting.constant.sc_out_r_default, active = "no", action_time = false,
    };
    ab_out = iup.list
    {
        dropdown="NO", size="70x80", value = 1, len = 0, action_time = false, type = "ab_out";
    };
    spi_out = iup.list
    {
        dropdown="NO", size="70x80", value = 1, len = 0, action_time = false, type = "spi_out";
    };
    file_out = iup.list
    {
        dropdown="NO", size="70x80", value = 1, len = 0, action_time = false, type = "file_out";
    };
    waveview = iup.list
    {
        dropdown="NO", size="70x80", value = 1, len = 0, action_time = false, type = "waveview";
    };
    out_source = iup.list
    {
        multiple="YES", size="120x130", action_time = false, len = 0,
    };
    sel_src = iup.list
    {
        dropdown="NO", size="120x110", value = 1, len = 0, action_time = false,
    };
}
GUI.ab_setting.list.file_out = update_list_item(GUI.ab_setting.list.file_out, GUI.ab_setting.constant.file_out_sel_default)
GUI.ab_setting.list.ab_out = update_list_item(GUI.ab_setting.list.ab_out, GUI.ab_setting.constant.ab_out_default)
GUI.ab_setting.list.gpio_in = update_list_item(GUI.ab_setting.list.gpio_in, GPIO_LIST)
GUI.ab_setting.list.waveview = update_list_item(GUI.ab_setting.list.waveview, GUI.ab_setting.constant.waveview_default)
GUI.ab_setting.list.spi_out = update_list_item(GUI.ab_setting.list.spi_out, GUI.ab_setting.constant.spi_out_default)
GUI.ab_setting.list.spi_in = update_list_item(GUI.ab_setting.list.spi_in, SPI_LIST)
GUI.ab_setting.list.debug_info1 = update_list_item(GUI.ab_setting.list.debug_info1, DEBUG_INFO_LIST1)
GUI.ab_setting.list.debug_info2 = update_list_item(GUI.ab_setting.list.debug_info2, DEBUG_INFO_LIST2)

GUI.ab_setting.text =
{
    file_in = iup.text{value = GUI.ab_setting.constant.file_in_default, channel_num = 2, size="95x", name = GUI.ab_setting.constant.file_in_default};
    file_out = iup.text{value = GUI.ab_setting.constant.file_out_default, size="95x"};
    ab_out_order = iup.text{value = GUI.ab_setting.constant.ab_out_order_default, size="50x"};
    file_out_order = iup.text{value = GUI.ab_setting.constant.file_out_order_default, size="50x"};
    waveview_order = iup.text{value = GUI.ab_setting.constant.waveview_order_default, size="50x"};
    test = iup.text{value = "text", size="50x"};
}

GUI.ab_setting.button =
{
    spi_in = iup.button{size="35x", title="Channel", active = "no"};
    debug_info = iup.button{size="35x", title="Info", active = "no"};
    gpio_in = iup.button{size="35x", title="Source"};
    browse_file_in = iup.button{size="25x", title="..."}; --Browse
    browse_file_out = iup.button{size="25x", title="..."};
    --export = iup.button{size="60x", title="Export",};
    --import = iup.button{size="60x", title="Import",};
    update = iup.button{size="40x", title="Update",};
    default = iup.button{size="40x", title="Default",};
    version = iup.button{size="40x", title="Version",};
    fileout_src_sel = iup.button{size="35x", title="Source",};
    ab_out_src_sel = iup.button{size="35x", title="Source",};
    waveview_src_sel = iup.button{size="35x", title="Source",};
    spi_out_src_sel = iup.button{size="35x", title="Source",};
    add = iup.button{size="67x", title="Add"};
    add_all = iup.button{size="67x", title="Add all"};
    remove = iup.button{size="67x", title="Remove"};
    remove_all = iup.button{size="67x", title="Remove all",};
    up = iup.button{size="47x", title="Up",};
    down = iup.button{size="47x", title="Down",};
    ok_src = iup.button{size="67x", title="Ok"};
    cancel_src = iup.button{size="67x", title="Cancel"};
    ok_source_sel = iup.button{size="60x", title="Ok"};
    cancel_source_sel = iup.button{size="60x", title="Cancel"};
    ok_debug = iup.button{size="90x", title="Ok"};
    cancel_debug = iup.button{size="90x", title="Cancel"};
    clear_select_item = iup.button{size="90x", title="Clear Selected Items"};
    clear_select_item2 = iup.button{size="100x", title="Clear Selected Items"};
}

GUI.ab_setting.dialog = 
{
    source_sel = iup.dialog
    {
        iup.vbox
        {
            GUI.ab_setting.list.sel_list;
            iup.hbox
            {
                GUI.ab_setting.button.ok_source_sel;
                GUI.ab_setting.button.cancel_source_sel;
                margin="5x5", gap="5"
            };
            margin="10x5", gap="10",alignment = "ACENTER",
       };
       title="", margin="10x10", gap="10"--,size="100x20"
    };
    debug_sel = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.ab_setting.list.debug_sel1;
                GUI.ab_setting.list.debug_sel2;
                margin="0x5", gap="0",alignment = "ACENTER",
            },
            iup.hbox
            {
                GUI.ab_setting.button.clear_select_item;
                GUI.ab_setting.button.ok_debug;
                GUI.ab_setting.button.cancel_debug;
                margin="5x5", gap="5"
            };
            margin="10x10", gap="10",alignment = "ACENTER",
       };
       title="Select Debug Information", margin="10x10", gap="10"--,size="100x20"
    };
    src_sel = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                iup.vbox
                {
                    GUI.ab_setting.label.src;
                    GUI.ab_setting.list.out_source;
                    margin="5", gap="5",alignment = "ACENTER"
                };
                iup.hbox
                {
                    iup.vbox
                    {
                        GUI.ab_setting.button.add;
                        GUI.ab_setting.button.add_all;
                        GUI.ab_setting.button.remove;
                        GUI.ab_setting.button.remove_all;
                        iup.hbox
                        {
                            GUI.ab_setting.toggle.sync_waveview;
                            GUI.ab_setting.label.sync_waveview;
                            margin="0x0", gap="0",alignment = "ACENTER"
                        };
                        margin="8x40", gap="5",alignment = "ACENTER"
                    };
                    iup.hbox
                    {
                        iup.hbox
                        {
                            GUI.ab_setting.label.out_source_name;
                            margin="0x20", gap="0",alignment = "ACENTER"
                        };
                        iup.vbox
                        {
                            GUI.ab_setting.label.sel_src;
                            GUI.ab_setting.list.sel_src;
                            iup.hbox
                            {
                                GUI.ab_setting.button.up;
                                GUI.ab_setting.button.down;
                                margin="5", gap="5",alignment = "ACENTER"
                            };
                            margin="0", gap="5",alignment = "ACENTER",
                        };
                        margin="0x0", gap="0"
                    };
                    margin="0x0", gap="10"
                };
                margin="10x", gap="45",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.ab_setting.button.ok_src;
                GUI.ab_setting.button.cancel_src;
                margin="10x10", gap="20",alignment = "ACENTER"
            };
            margin="10x5", gap="20",alignment = "ACENTER",
        };
        title="Source Select", margin="10x30", gap="10"--,size="100x20"
    };
}

------ general function

GUI.ab_setting.list.sc_in_dev = update_sc_in_list(GUI.ab_setting.list.sc_in_dev)
GUI.ab_setting.list.sc_out_dev = update_sc_out_list(GUI.ab_setting.list.sc_out_dev)
bak_list_table = {}

function active_design_save_button(active)
    if active == "yes" or active == "YES" then
        GUI.uif_setting.button.update.title = "Update Setting *"
        if GUI.design_manage.list.group.type == "mydesign" then
            GUI.uif_setting.button.save.active = active
        end
    else
        GUI.uif_setting.button.save.active = "no"
    end
end

function ab_setting_to_kernal()
    printc("\n*********** Update setting to kernal ***********\n")
    convert_enable = convert_enable or 1
    if convert_enable == 0 then return end
    for k, v in pairs(GUI.kernel.node) do
        node_name = GUI.kernel.node[k].name
        local debug_str = "---->"
        debug_str=debug_str..", "..node_name..", "..GUI.kernel.node[k].active
        str = "a=GUI.ab_setting.toggle."..node_name..".value"
        assert(pcall(loadstring(str)))
        GUI.kernel.node[k].active=a
        --print(GUI.kernel.node[k].active)
        debug_str=debug_str..", "..GUI.kernel.node[k].active
        if node_name == "ab_in" then
            GUI.kernel.node[k].sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
            GUI.kernel.node[k].channum = GUI.ab_setting.list.ab_in.value+0

        elseif node_name == "gpio_in" then
            GUI.kernel.node[k].filename = GUI.ab_setting.list.gpio_in.value
        elseif node_name == "spi_in" then
            GUI.kernel.node[k].sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
            GUI.kernel.node[k].filename = GUI.ab_setting.list.spi_in.value
--standby
        elseif node_name == "debug_info" then
            GUI.kernel.node[k].filename1 = GUI.ab_setting.list.debug_info1.value
            GUI.kernel.node[k].filename2 = GUI.ab_setting.list.debug_info2.value
        elseif node_name == "file_in" then
            GUI.kernel.node[k].filename = dir_remove(GUI.ab_setting.text.file_in.value)
        elseif node_name == "sc_in" then
            GUI.kernel.node[k].sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
            GUI.kernel.node[k].filename = GUI.ab_setting.list.sc_in_dev[GUI.ab_setting.list.sc_in_dev.value+0]
        elseif node_name == "sc_out" then
            GUI.kernel.node[k].filename = GUI.ab_setting.list.sc_out_dev[GUI.ab_setting.list.sc_out_dev.value+0]
            GUI.kernel.node[k].src_des[1] ={sId = 1, name = GUI.ab_setting.list.sc_out_l[GUI.ab_setting.list.sc_out_l.value]}
            GUI.kernel.node[k].src_des[2] ={sId = 2, name = GUI.ab_setting.list.sc_out_r[GUI.ab_setting.list.sc_out_r.value]}
        else
            src_des_tab = {}
            str = "src_des_tab=GUI.ab_setting.list."..node_name
            assert(pcall(loadstring(str)))
            GUI.kernel.node[k].src_des={}  --empty
            for i = 1, src_des_tab.len do
                GUI.kernel.node[k].src_des[i]={sId=i,name=src_des_tab[i]}
            end
            if node_name == "file_out" then
                GUI.kernel.node[k].filename = dir_remove(GUI.ab_setting.text.file_out.value)
                GUI.kernel.node[k].samewithwaveview = sync_waveview_value
            end
            
            debug_str=debug_str..", "..dump(GUI.kernel.node[k].src_des)
        end
       -- print(debug_str)
    end
    active_design_save_button("yes")
end
  
function update_dropdownlist_value(value)
    local position = 0
    for i = 1, #out_node_src do
        if temp_out_node_src[value] == out_node_src[i] then
            position = i
            break
        end
    end

    return position == 0 and 1 or position
end
function update_multilist_value(list)
    local list_tab = {}
    for i = 1, list.len do
        list_tab[i] = list[i]
        list[i] = nil
    end
    list.len=0

    local pos = 0
    for i = 1, #list_tab do
            local position = 0
            pos = pos + 1
            for j = 1, #out_node_src do
                --if out_node_src[j] == list_tab[pos] or list_tab[pos] == "Silence" then
                if out_node_src[j] == list_tab[pos] then
                    position = j
                    break
                end
            end
            if position == 0 then -- no same node be found.
                table.remove(list_tab, pos)
                pos = pos - 1
            end
    end
    
    for i = 1, #list_tab do
        list[i] = list_tab[i]
    end
    list.len=#list_tab
    list.value = #list_tab == 0 and 0 or 1
    return list
end

function update_out_node_list()
    for i = 1, #temp_out_node_src do --empty
        GUI.ab_setting.list.sc_out_l[i] = nil
        GUI.ab_setting.list.sc_out_r[i] = nil
        GUI.ab_setting.list.out_source[i] = nil
    end
    for i = 1, #out_node_src do
        GUI.ab_setting.list.sc_out_l[i] = out_node_src[i]
        GUI.ab_setting.list.sc_out_r[i] = out_node_src[i]
        GUI.ab_setting.list.out_source[i] = out_node_src[i]
    end
    
    --GUI.ab_setting.list.out_source[#out_node_src+1] = "Silence"
    GUI.ab_setting.list.out_source.len = #out_node_src --+1
    GUI.ab_setting.list.out_source.value = "+"
end

update_out_node_list()

--********************************************************************
function update_out_node_src()
    printc("\n******* Update out node source *******\n")
    --temp_out_node_src = out_node_src
    temp_out_node_src = copy_table(out_node_src)
    --if out_node_src == 0 then  end --out toggle disable ???? 
    ------sc_in
--print(unpack(temp_out_node_src))
--print(unpack(out_node_src))
    if GUI.ab_setting.toggle.sc_in.value == "ON" then
        local position = 0
        for i = 1, #out_node_src do
            if out_node_src[i] == "Soundcard L" then position = 1 break end
        end
        if position == 0 then
            table.insert(out_node_src, "Soundcard L")
            table.insert(out_node_src, "Soundcard R")
        end
    else
        for i = 1, #out_node_src do
            if out_node_src[i] == "Soundcard L" then
                table.remove(out_node_src, i)
                table.remove(out_node_src, i) --"Soundcard L" is removed, the next position is i too.
                break
            end
        end
    end
    ------ab in
--print(unpack(out_node_src))
    local position = 0
    for i = 1, #out_node_src do
        if string.find(out_node_src[i], "TDM/PDM s") ~= nil then position = i break end
    end
    if position ~= 0 then
        for i = position, #out_node_src do
            if string.find(out_node_src[position], "TDM/PDM s") ~= nil then
            --print(position)
                table.remove(out_node_src, position)
            end
        end
        for i = position, #out_node_src do
            if string.find(out_node_src[position], "GPIO") ~= nil then
            --print(position)
                table.remove(out_node_src, position)
            end
        end
    end
    ---- without TDM/PDM or out_node_src is empty, insert at start position.

    if GUI.ab_setting.toggle.ab_in.value == "ON" then
        position = position == 0 and 1 or position  --in case, out_node_src is empty.
        for i = 1, GUI.ab_setting.list.ab_in.value+0 do
            table.insert(out_node_src, position+i-1, "TDM/PDM s"..i)
        end
        --if (GUI.ab_setting.toggle.gpio_in.active == "yes" or GUI.ab_setting.toggle.gpio_in.active == "YES") and GUI.ab_setting.toggle.gpio_in.value == "ON" then
        if GUI.ab_setting.toggle.gpio_in.value == "ON" then
            position = position+GUI.ab_setting.list.ab_in.value
            --[[
            for i = #GUI.ab_setting.list.gpio_in.value_tab, 1, -1 do  ----??
                if GUI.ab_setting.list.gpio_in.value_tab[i] == 1 then
                    table.insert(out_node_src, position, GUI.ab_setting.list.gpio_in[#GUI.ab_setting.list.gpio_in.value_tab+1 - i])
                    position = position + 1
                end
            end
            ]]
            for i = 1, #GUI.ab_setting.list.gpio_in.value_tab do  ----??
                if GUI.ab_setting.list.gpio_in.value_tab[i] == 1 then
                    table.insert(out_node_src, position, GUI.ab_setting.list.gpio_in[i])
                    position = position + 1
                end
            end
        end
    end
--print(unpack(out_node_src))
    ------spi_in  
    local position = 0
    for i = 1, #out_node_src do
        if string.find(out_node_src[i], "SPI") ~= nil then position = i break end
    end
    if position ~= 0 then
        for i = position, #out_node_src do
            if string.find(out_node_src[position], "SPI") ~= nil then
                table.remove(out_node_src, position)
            end
        end
    end
    if position ~= 0 then
        for i = position, #out_node_src do
            if string.find(out_node_src[position], "DBG") ~= nil then
                table.remove(out_node_src, position)
            end
        end
    end
--print(unpack(out_node_src))

    ---- without SPI, insert at end position
    if GUI.ab_setting.toggle.spi_in.value == "ON" then
        position = position == 0 and 1 or position  --in case, out_node_src is empty.
        SPI_REC = {}
--print(GUI.ab_setting.list.spi_in.value)
--print(unpack(GUI.ab_setting.list.spi_in.value_tab))
        for i = 1, #GUI.ab_setting.list.spi_in.value_tab do
            if GUI.ab_setting.list.spi_in.value_tab[i] == 1 then
                table.insert(out_node_src, position, GUI.ab_setting.list.spi_in[i])
                table.insert(SPI_REC, GUI.ab_setting.list.spi_in[i])
                --if GUI.ab_setting.list.spi_in[i] == SPI_LIST[#SPI_LIST] then DEBUG_INFO_SRC_IDX = #SPI_REC end
                position = position + 1
            end
        end
        if (GUI.ab_setting.toggle.debug_info.active == "yes" or GUI.ab_setting.toggle.debug_info.active == "YES") and GUI.ab_setting.toggle.debug_info.value == "ON" then
--print(GUI.ab_setting.list.debug_info1.value)
--print(unpack(GUI.ab_setting.list.debug_info1.value_tab))
            for i = 1, #GUI.ab_setting.list.debug_info1.value_tab do
                if GUI.ab_setting.list.debug_info1.value_tab[i] == 1 then
                    table.insert(out_node_src, position, GUI.ab_setting.list.debug_info1[i])
                    position = position + 1
                    --print(position)
                end
            end
--print(GUI.ab_setting.list.debug_info2.value)
--print(unpack(GUI.ab_setting.list.debug_info2.value_tab))
            for i = 1, #GUI.ab_setting.list.debug_info2.value_tab do
                if GUI.ab_setting.list.debug_info2.value_tab[i] == 1 then
                    table.insert(out_node_src, position, GUI.ab_setting.list.debug_info2[i])
                    position = position + 1
                    --print(position)
                end
            end
        end
--standby
    end
--print(unpack(out_node_src))
--------------
    ------file_in  
    local position = 0
    for i = 1, #out_node_src do
        if string.find(out_node_src[i], "Wavefile") ~= nil then position = i break end
        position = -1
    end
    if position > 0 then
        for i = position, #out_node_src do
            if string.find(out_node_src[position], "Wavefile") ~= nil then
                table.remove(out_node_src, position)
            end
        end
    end
--print(unpack(out_node_src))
    ---- without wavefile, insert at end position
    if GUI.ab_setting.toggle.file_in.value == "ON" then
        position = position == 0 and 1 or position  --in case, out_node_src is empty.
        position = position == -1 and #out_node_src or position-1 --in case, no wavefile in out_node_src
        for i = 1, GUI.ab_setting.text.file_in.channel_num do
            if channels_label[i] ~= nil and channels_label[i] ~= "" then
                table.insert(out_node_src, position+i, "Wavefile CH"..i..": "..channels_label[i])
            else
                table.insert(out_node_src, position+i, "Wavefile CH"..i)
            end
        end
    end

    ------Silence
    position = 0
    for i = #out_node_src, 1, -1 do
        if string.find(out_node_src[i], "Silence") ~= nil then position = i break end
    end
    if position == 0 then table.insert(out_node_src, "Silence") end
    
--print(unpack(out_node_src))
--------------
    GUI.ab_setting.list.sc_out_l.value = update_dropdownlist_value(GUI.ab_setting.list.sc_out_l.value)
    GUI.ab_setting.list.sc_out_r.value = update_dropdownlist_value(GUI.ab_setting.list.sc_out_r.value)
    update_out_node_list()
    GUI.ab_setting.list.ab_out  = update_multilist_value(GUI.ab_setting.list.ab_out)
    GUI.ab_setting.list.spi_out  = update_multilist_value(GUI.ab_setting.list.spi_out)
    GUI.ab_setting.list.file_out = update_multilist_value(GUI.ab_setting.list.file_out)
    GUI.ab_setting.list.waveview = update_multilist_value(GUI.ab_setting.list.waveview)
    --end
    print(unpack(out_node_src))
    ab_setting_to_kernal()
end

function list_convert_node(list)
    local select, output_ch = {},{}
    if type(list) == "number" then
        select[1] = out_node_src[list] 
    else
        for i = 1, list.len+0 do table.insert(select, list[i]) end
    end
--print(list.len)
    local src_tab = {}
    local gpio_start_pos = 0
    for i = 1, #out_node_src do
        if string.find(out_node_src[i], "TDM/PDM s") ~= nil then
            gpio_start_pos = gpio_start_pos + 1
        end
    end
--print(unpack(select))
    for i = 1, #select do
        src_tab[i] = {}
        if gpio_start_pos ~= 0 then
            if string.find(select[i], "TDM/PDM s") ~= nil then
                src_tab[i][1] = NODE_AB_MICIN
                src_tab[i][2] = tonumber(string.sub(select[i], string.len(select[i]), string.len(select[i]))) - 1
                src_tab[i][3] = select[i]
                if select[i] == OUTPUT.mic0_bypass then output_ch.mic0_bypass = i end
                if select[i] == OUTPUT.mic1_bypass then output_ch.mic1_bypass = i end
                if select[i] == OUTPUT.lout then output_ch.lout = i end
            elseif string.find(select[i], "GPIO") ~= nil then
                src_tab[i][1] = NODE_AB_MICIN
                local pos = 0
                for j = 1, #GPIO_REC do
                    if tonumber(string.sub(select[i],5,5)) == GPIO_REC[j] then
                        pos = j
                        break
                    end
                end
                src_tab[i][2] = gpio_start_pos + pos-1
                src_tab[i][3] = select[i]
                --gpio_start_pos = gpio_start_pos + 1
                if select[i] == OUTPUT.irq then output_ch.irq = i end
                if select[i] == OUTPUT.xsd then output_ch.xsd = i end
                if select[i] == OUTPUT.avd then output_ch.avd = i end
            end
        end

        if string.find(select[i], "Wavefile") ~= nil then
            src_tab[i][1] = NODE_FILE_ARRAY_S
            local pos = string.find(select[i], ":")
            if pos ~= nil then
                src_tab[i][2] = tonumber(string.sub(select[i], pos-1, pos-1)) - 1
            else
                src_tab[i][2] = tonumber(string.sub(select[i], string.len(select[i]), string.len(select[i]))) - 1
            end
            src_tab[i][3] = select[i]
            if select[i] == OUTPUT.file_in then output_ch.file_in = i end
        elseif string.find(select[i], "SPI") ~= nil then
            src_tab[i][1] = NODE_SPI_IN
            local pos = 1
            for j = 1, #SPI_REC do
                --print(select[i], SPI_REC[j])
                if select[i] == SPI_REC[j] then pos = j break end  --SPI NODE IS NOT CONTIUNS
            end
            src_tab[i][2] = pos - 1
            --src_tab[i][3] = select[i]
            
----remove "SPI" in channel name of wave file
            local label_name = select[i]
            if type(list) ~= "number" and list.type == "file_out" then
                local pos = string.find(label_name, ":")
                if pos ~= nil then
                    label_name = string.sub(label_name, pos+1, -1)
                end
            end
----

            src_tab[i][3] = label_name  
--standby
        elseif string.find(select[i], "DBG") ~= nil then
            src_tab[i][1] = NODE_DEG_INFO
            local pos = 0
            for j = 1, #DEBUG_INFO_LIST1 do
                --print(select[i], DEBUG_LIST1[j])
                if select[i] == DEBUG_INFO_LIST1[j] then pos = j break end
            end
            if pos == 0 then
                for j = 1, #DEBUG_INFO_LIST2 do
                    --print(select[i], DEBUG_LIST2[j])
                    if select[i] == DEBUG_INFO_LIST2[j] then pos = j + #DEBUG_INFO_LIST1 break end
                end
            end
            src_tab[i][2] = pos - 1
            src_tab[i][3] = select[i]
        elseif string.find(select[i], "Soundcard L") ~= nil then
            src_tab[i][1] = NODE_SOUNDCARD_IN
            src_tab[i][2] = 0
            src_tab[i][3] = select[i]
        elseif string.find(select[i], "Soundcard R") ~= nil then
            src_tab[i][1] = NODE_SOUNDCARD_IN
            src_tab[i][2] = 1
            src_tab[i][3] = select[i]
        elseif string.find(select[i], "Silence") ~= nil then
            src_tab[i][1] = NODE_SILENCE
            src_tab[i][2] = 0
            src_tab[i][3] = select[i]
        end
    --print(unpack(src_tab[i]))
    end
--print("bbbb")

    return src_tab, output_ch
end

GUI.ab_setting.local_frame =
{
    ab_in = iup.frame
    {
        iup.hbox  --uif_in & GPIO
        {
            iup.hbox
            {
                GUI.ab_setting.toggle.ab_in,
                iup.hbox
                {
                    GUI.ab_setting.label.ab_in,
                    GUI.ab_setting.list.ab_in,
                    gap="5",margin="5",alignment = "ACENTER"
                };
                gap="5",margin="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.ab_setting.toggle.gpio_in,
                GUI.ab_setting.button.gpio_in,
                gap="5",margin="10",alignment = "ACENTER"
            };
            gap="20",margin="0x5",alignment = "ACENTER"
         };
         title = "I2S/PDM/GPIO Recording",
     };
    sc_in = iup.frame
    {
        iup.hbox
        {
            GUI.ab_setting.toggle.sc_in,
            --GUI.ab_setting.label.sc_in,
            GUI.ab_setting.list.sc_in_dev;
            gap="20",margin="5x5",alignment = "ACENTER"
        };
        title = "Sound Card Recording",
    };
    file_in = iup.frame
    {
        iup.hbox
        {
            GUI.ab_setting.toggle.file_in,
            iup.hbox
            {
                GUI.ab_setting.text.file_in;
                GUI.ab_setting.button.browse_file_in;
                gap="5",margin="0",alignment = "ACENTER"
            };
            --GUI.ab_setting.label.file_in;
            gap="10",margin="5x5",alignment = "ACENTER"
        };
        title = "Wavefile Loading",
    };
    sc_out = iup.frame
    {
        iup.hbox  --sc_out
        {
            GUI.ab_setting.toggle.sc_out,
            --GUI.ab_setting.label.sc_out,
            GUI.ab_setting.list.sc_out_dev;
            iup.hbox
            {
              GUI.ab_setting.label.sc_out_l,
              GUI.ab_setting.list.sc_out_l;
              gap="5",margin="0",alignment = "ACENTER"
            };
            iup.hbox
            {
              GUI.ab_setting.label.sc_out_r,
              GUI.ab_setting.list.sc_out_r;
              gap="5",margin="0",alignment = "ACENTER"
            };
            gap="24",margin="5x5",alignment = "ACENTER"
        };
        title = "Sound Card Playing",
    };
    file_out = iup.frame
    {
        iup.hbox  --file_out
        {
            GUI.ab_setting.toggle.file_out,
            iup.hbox
            {
                GUI.ab_setting.text.file_out;
                GUI.ab_setting.button.browse_file_out;
                gap="5",margin="0",alignment = "ACENTER"
            };
            GUI.ab_setting.button.fileout_src_sel;
            gap="10",margin="5x5",alignment = "ACENTER"
        };
        title = "Save to Wavefile",
    };
    waveview = iup.frame
    {
        iup.hbox
        {
            GUI.ab_setting.toggle.waveview,
            GUI.ab_setting.button.waveview_src_sel;
            gap="20",margin="5x5",alignment = "ACENTER"
        };
        title = "Wave Viewer Displaying",
    };
    ab_out = iup.frame
    {
        iup.hbox
        {
            GUI.ab_setting.toggle.ab_out,
            GUI.ab_setting.button.ab_out_src_sel;
            gap="20",margin="5x5",alignment = "ACENTER"
        };
        title = "I2S/PDM Playing",
    };
    spi_in = iup.frame
    {
        iup.hbox
        {
            iup.hbox
            {
                GUI.ab_setting.toggle.spi_in,
                GUI.ab_setting.button.spi_in,
                gap="5",margin="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.ab_setting.toggle.debug_info,
                GUI.ab_setting.button.debug_info,
                gap="5",margin="5",alignment = "ACENTER"
            };
            gap="20",margin="5x5",alignment = "ACENTER"
        };
        title = "SPI Recording",
    };
    spi_out = iup.frame
    {
        iup.hbox
            {
                GUI.ab_setting.toggle.spi_out,
                GUI.ab_setting.button.spi_out_src_sel,
                gap="20",margin="5x5",alignment = "ACENTER"
            };
        title = "SPI Playing",
    };
    button  = iup.hbox
    {
        iup.hbox
        {
            --GUI.ab_setting.button.export;
            --GUI.ab_setting.button.import;
            GUI.ab_setting.button.update;
            GUI.ab_setting.button.default;
            GUI.ab_setting.button.version;
            --GUI.ab_setting.button.fileout_src_sel;
            gap="30",margin="10",alignment = "ACENTER"
        };
        title = "",
    };
}

GUI.ab_setting.frame = iup.frame
{
    iup.vbox
    {
        iup.hbox  --ab_in&out
        {
            GUI.ab_setting.local_frame.ab_in;
            GUI.ab_setting.local_frame.ab_out;
            gap="71",margin="0x5",alignment = "ACENTER",
        };
        GUI.ab_setting.label.sep1;
        iup.hbox  -- spi in & out
        {
            GUI.ab_setting.local_frame.spi_in;
            GUI.ab_setting.local_frame.spi_out;
            --GUI.ab_setting.local_frame.button;
            --GUI.ab_setting.button.version;
            gap="49",margin="0x5",alignment = "ACENTER"
        };
        GUI.ab_setting.label.sep2;
        iup.hbox  -- file_in & file_out
        {
            GUI.ab_setting.local_frame.file_in;
            GUI.ab_setting.local_frame.file_out;
            gap="20",margin="0x5",alignment = "ACENTER",
        };
        GUI.ab_setting.label.sep3;
        iup.hbox  --waveview & sc_in
        {
            GUI.ab_setting.local_frame.sc_in;
            GUI.ab_setting.local_frame.waveview;
            gap="160",margin="0x5",alignment = "ACENTER"
        };
        GUI.ab_setting.local_frame.sc_out;
        gap="5",margin="10x10",alignment = "ALEFT"
    };
    title = "Signal Path Define", 
    size="400x",margin="0",alignment = "ACENTER",
}

function source_list_copy(lista)
    for i = 1, GUI.ab_setting.list.sel_list.len+0 do
        GUI.ab_setting.list.sel_list[i] = nil
    end
    for i = 1, lista.len+0 do
        GUI.ab_setting.list.sel_list[i] = lista[i]
    end
    GUI.ab_setting.list.sel_list.len = lista.len
    GUI.ab_setting.list.sel_list.value = lista.value
    GUI.ab_setting.list.sel_list.type = lista.type
    GUI.ab_setting.list.sel_list.size = lista.size
end

GUI.ab_setting.button.gpio_in.action = function()
    source_list_copy(GUI.ab_setting.list.gpio_in)
    GUI.ab_setting.dialog.source_sel.title = "Select GPIO Input"
    GUI.ab_setting.dialog.source_sel:popup()
end

GUI.ab_setting.button.spi_in.action = function()
    source_list_copy(GUI.ab_setting.list.spi_in)
    GUI.ab_setting.dialog.source_sel.title = "Select SPI Input Channels"
    GUI.ab_setting.dialog.source_sel:popup()
end

GUI.ab_setting.button.cancel_source_sel.action = function() 
    GUI.ab_setting.dialog.source_sel:hide()
end

GUI.ab_setting.button.ok_source_sel.action = function()
    if GUI.ab_setting.list.sel_list.type == "spi_in" then
        GUI.ab_setting.list.spi_in.value = GUI.ab_setting.list.sel_list.value
print(GUI.ab_setting.list.spi_in.value)
        GUI.ab_setting.list.spi_in.action_time = false
        GUI.ab_setting.list.spi_in.action()
    elseif GUI.ab_setting.list.sel_list.type == "gpio_in" then
        GUI.ab_setting.list.gpio_in.value = GUI.ab_setting.list.sel_list.value
        GUI.ab_setting.list.gpio_in.action_time = false
        GUI.ab_setting.list.gpio_in.action()
    end
    GUI.ab_setting.dialog.source_sel:hide()
end

function debug_list_copy(list1, list2)
    for i = 1, GUI.ab_setting.list.debug_sel1.len+0 do
        GUI.ab_setting.list.debug_sel1[i] = nil
    end
    for i = 1, list1.len+0 do
        GUI.ab_setting.list.debug_sel1[i] = list1[i]
    end
    for i = 1, GUI.ab_setting.list.debug_sel2.len+0 do
        GUI.ab_setting.list.debug_sel2[i] = nil
    end
    for i = 1, list2.len+0 do
        GUI.ab_setting.list.debug_sel2[i] = list2[i]
    end
    GUI.ab_setting.list.debug_sel1.len = list1.len
    GUI.ab_setting.list.debug_sel1.value = list1.value
    GUI.ab_setting.list.debug_sel2.len = list2.len
    GUI.ab_setting.list.debug_sel2.value = list2.value
end

GUI.ab_setting.button.debug_info.action = function()
    debug_list_copy(GUI.ab_setting.list.debug_info1, GUI.ab_setting.list.debug_info2)
    GUI.ab_setting.dialog.debug_sel:popup()
end

GUI.ab_setting.button.clear_select_item.action = function() 
    GUI.ab_setting.list.debug_sel1.value = "-"
    GUI.ab_setting.list.debug_sel2.value = "-"
end

GUI.ab_setting.button.cancel_debug.action = function() 
    GUI.ab_setting.dialog.debug_sel:hide()
end
GUI.ab_setting.button.ok_debug.action = function()
    GUI.ab_setting.list.debug_info1.value = GUI.ab_setting.list.debug_sel1.value
    GUI.ab_setting.list.debug_info2.value = GUI.ab_setting.list.debug_sel2.value
    GUI.ab_setting.list.debug_info1 = update_list_value_tab(GUI.ab_setting.list.debug_info1)
    GUI.ab_setting.list.debug_info2 = update_list_value_tab(GUI.ab_setting.list.debug_info2)
    GUI.ab_setting.list.debug_info2.action_time = false
    GUI.ab_setting.list.debug_info2.action()
    GUI.ab_setting.dialog.debug_sel:hide()
end

GUI.ab_setting.button.browse_file_in.action = function()
    local fd=iup.filedlg{dialogtype="OPEN", title="Load wavefile", multiplefiles = "YES",
                         nochangedir="YES", directory=NOAH_PATH.."Vector\\",value=GUI.ab_setting.text.file_in.value,
                         filter="*.wav", filterinfo="*.wav", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status
 
    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
        if string.find(fd.value, ".wav") == nil then
            message("ERROR", "The file of selected should be wrong, please double check!")
        end
        GUI.ab_setting.text.file_in.value = fd.value
        printc("Input file: "..GUI.ab_setting.text.file_in.value)
        system_setting_sync(0x21)
    end
    fd:destroy()
    update_out_node_src()
end

GUI.ab_setting.button.browse_file_out.action = function()
    local fd=iup.filedlg{dialogtype="SAVE", title="Save wavefile", 
                         nochangedir="YES", directory=RECORD_PATH,value=GUI.ab_setting.text.file_out.value,
                         filter="*.wav", filterinfo="*.wav", allownew="YES"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "0") or (status == "1") then -- 0 normal; 1,new; -1, cancel
        if string.find(fd.value, ".wav") == nil then
            fd.value = fd.value..".wav"
        end
        GUI.ab_setting.text.file_out.value = fd.value

        printc(GUI.ab_setting.text.file_out.value)
    end
    
    fd:destroy()
    ab_setting_to_kernal()
end

function select_src(list_sel, type)
    for i = 1, GUI.ab_setting.list.sel_src.len+0 do
        GUI.ab_setting.list.sel_src[i] = nil
    end
print(type, list_sel.len)
    for i = 1, list_sel.len do
        GUI.ab_setting.list.sel_src[i] = list_sel[i]
        print(list_sel[i])
    end
    GUI.ab_setting.list.sel_src.len = list_sel.len
    GUI.ab_setting.list.sel_src.value = 1
    GUI.ab_setting.list.sel_src.type = type
    GUI.ab_setting.dialog.src_sel:popup()
end

function GUI.ab_setting.dialog.src_sel:close_cb()
  --iup.ExitLoop()  -- should be removed if used inside a bigger application
    if GUI.ab_setting.list.sel_src.type == "spi_out" then
        update_out_node_list()
    end
    
    --return iup.IGNORE
end

GUI.ab_setting.toggle.sync_waveview.action = function()
    if GUI.ab_setting.toggle.sync_waveview.value == "ON" and GUI.ab_setting.toggle.sync_waveview.active == "YES" then
        --sync_waveview_value = "ON"
        select_src(GUI.ab_setting.list.waveview, "file_out")
    else
        --sync_waveview_value = "OFF"
        --select_src(GUI.ab_setting.list.file_out, "file_out")
    end
end

GUI.ab_setting.button.fileout_src_sel.action = function()
--print("Test1.......")
    GUI.ab_setting.dialog.src_sel.title = "Select source to save to wave file"
    GUI.ab_setting.label.out_source_name.title = ""
    GUI.ab_setting.toggle.sync_waveview.active = "YES"
    GUI.ab_setting.label.sync_waveview.active = "YES"
    GUI.ab_setting.toggle.sync_waveview.value = sync_waveview_value
    if GUI.ab_setting.toggle.sync_waveview.value == "ON" then
        select_src(GUI.ab_setting.list.waveview, "file_out")
    else
        select_src(GUI.ab_setting.list.file_out, "file_out")
    end
end
GUI.ab_setting.button.waveview_src_sel.action = function()
--print("Test2.......")
    GUI.ab_setting.dialog.src_sel.title = "Select source to display in WaveViewer"
    GUI.ab_setting.label.out_source_name.title = ""
    GUI.ab_setting.toggle.sync_waveview.active = "NO"
    GUI.ab_setting.label.sync_waveview.active = "NO"
    GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    select_src(GUI.ab_setting.list.waveview, "waveview")
end
GUI.ab_setting.button.ab_out_src_sel.action = function()
--print("Test3.......")
    system_setting_sync(0x14)
    GUI.ab_setting.dialog.src_sel.title = "Select source to play via audio bus"
    GUI.ab_setting.label.out_source_name.title = ""
    GUI.ab_setting.toggle.sync_waveview.active = "NO"
    GUI.ab_setting.label.sync_waveview.active = "NO"
    GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    select_src(GUI.ab_setting.list.ab_out, "ab_out")
end
GUI.ab_setting.button.spi_out_src_sel.action = function()
--print("Test4.......")
    --system_setting_sync(0x15)
    GUI.ab_setting.dialog.src_sel.title = "Select source to play via SPI bus"
    GUI.ab_setting.label.out_source_name.title = GUI.ab_setting.label.spi_out_name.title
    GUI.ab_setting.toggle.sync_waveview.active = "NO"
    GUI.ab_setting.label.sync_waveview.active = "NO"
    GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    local bak_list_table = {}
    for i = GUI.ab_setting.list.out_source.len+0, 1, -1 do
        bak_list_table[i] = GUI.ab_setting.list.out_source[i]
        GUI.ab_setting.list.out_source[i] = nil
    end
    local num = 1
    for i = 1, #bak_list_table do
        if string.find(bak_list_table[i], "SPI") == nil and string.find(bak_list_table[i], "DBG") == nil then
            GUI.ab_setting.list.out_source[num] = bak_list_table[i]
            num = num + 1
        end
    end
    GUI.ab_setting.list.out_source.len = num-1
    GUI.ab_setting.list.out_source.value = "+"
    
    select_src(GUI.ab_setting.list.spi_out, "spi_out")
end

GUI.ab_setting.button.add.action = function()
--print(GUI.ab_setting.list.sel_src.len, GUI.ab_setting.constant.ab_in_max)
    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    end
    
    if GUI.ab_setting.list.sel_src.type == "ab_out"    --ab out has limit set by slot number
      and GUI.ab_setting.list.sel_src.len+0 >= GUI.ab_setting.constant.ab_out_max then
      message("Warning", "The number out of range")
      return
    end
    if GUI.ab_setting.list.sel_src.type == "spi_out"    --ab out has limit set by slot number
      and GUI.ab_setting.list.sel_src.len+0 >= GUI.ab_setting.constant.spi_out_max then
      message("Warning", "The number out of range")
      return
    end
    
    local n_value = s_2_n(GUI.ab_setting.list.out_source.value)
    local sel_tab = {}
    for i = 1, GUI.ab_setting.list.sel_src.len+0 do
        sel_tab[i] = GUI.ab_setting.list.sel_src[i]
    end
    local position = GUI.ab_setting.list.sel_src.value+0
    position = position == 0 and #sel_tab or position
    for i = 1, #n_value do
        if n_value[i] == 1 then
            local pos = 0
            --[[ if selection table allow same item, comment
            for j = 1, #sel_tab do
                if sel_tab[j] == GUI.ab_setting.list.out_source[i] then pos = j break end
            end
            --]]
            if pos == 0 then
                table.insert(sel_tab, position+1, GUI.ab_setting.list.out_source[i])
                position = position+1
            end
        end
    end
    for i = 1, #sel_tab do
        GUI.ab_setting.list.sel_src[i] = sel_tab[i]
    end
    GUI.ab_setting.list.sel_src.value = position
    GUI.ab_setting.list.sel_src.len = #sel_tab
end

GUI.ab_setting.button.add_all.action = function()
    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    end

    local n_value = s_2_n(GUI.ab_setting.list.out_source.value)
    if GUI.ab_setting.list.sel_src.type == "ab_out"   --ab out has limit set by slot number
      and GUI.ab_setting.list.sel_src.len+#n_value >= GUI.ab_setting.constant.ab_out_max then
      message("Warning", "The number out of range")
      return
    end
    if GUI.ab_setting.list.sel_src.type == "spi_out"    --ab out has limit set by slot number
      and GUI.ab_setting.list.sel_src.len+0 >= GUI.ab_setting.constant.spi_out_max then
      message("Warning", "The number out of range")
      return
    end
    
    local sel_tab = {}
    for i = 1, GUI.ab_setting.list.sel_src.len+0 do
        sel_tab[i] = GUI.ab_setting.list.sel_src[i]
    end
    local position = GUI.ab_setting.list.sel_src.value+0
    position = position == 0 and #sel_tab or position
    for i = 1, #n_value do
      -- if selection table allow same item, comment
        local pos = 0
        --[[
        for j = 1, #sel_tab do
            if sel_tab[j] == GUI.ab_setting.list.out_source[i] then pos = j break end
        end
        ]]--
        if pos == 0 then
            table.insert(sel_tab, position+1, GUI.ab_setting.list.out_source[i])
            position = position+1
        end
    end
    for i = 1, #sel_tab do
        GUI.ab_setting.list.sel_src[i] = sel_tab[i]
    end
    GUI.ab_setting.list.sel_src.value = position
    GUI.ab_setting.list.sel_src.len = #sel_tab
end

GUI.ab_setting.button.remove.action = function()
    local sel_tab = {}
    local value = GUI.ab_setting.list.sel_src.value+0

    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    end
    
    for i = 1, GUI.ab_setting.list.sel_src.len+0 do
        sel_tab[i] = GUI.ab_setting.list.sel_src[i]
        GUI.ab_setting.list.sel_src[i] = nil
    end

    table.remove(sel_tab, value)
    for i = 1, #sel_tab do
        GUI.ab_setting.list.sel_src[i] = sel_tab[i]
    end
    GUI.ab_setting.list.sel_src.len = #sel_tab
    value = #sel_tab == 0 and 0 or value
    value = value > #sel_tab and #sel_tab  or value
    GUI.ab_setting.list.sel_src.value = value
end

GUI.ab_setting.button.remove_all.action = function()
    local sel_tab = {}
    
    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    end
    
    for i = 1, GUI.ab_setting.list.sel_src.len+0 do
        GUI.ab_setting.list.sel_src[i] = nil
    end

    GUI.ab_setting.list.sel_src.len = 0
    GUI.ab_setting.list.sel_src.value = 0
end

GUI.ab_setting.button.up.action = function()
    local value = GUI.ab_setting.list.sel_src.value+0
    if value < 2 then return end

    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    end
    local temp = GUI.ab_setting.list.sel_src[value-1]
    GUI.ab_setting.list.sel_src[value-1] = GUI.ab_setting.list.sel_src[value]
    GUI.ab_setting.list.sel_src[value] = temp
    GUI.ab_setting.list.sel_src.value = value - 1
end

GUI.ab_setting.button.down.action = function()
    local value = GUI.ab_setting.list.sel_src.value+0
    --print(value,GUI.ab_setting.list.sel_src.len)
    if value == 0 or  value == GUI.ab_setting.list.sel_src.len+0 then return end

    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.toggle.sync_waveview.value = "OFF"
    end
    local temp = GUI.ab_setting.list.sel_src[value+1]
    GUI.ab_setting.list.sel_src[value+1] = GUI.ab_setting.list.sel_src[value]
    GUI.ab_setting.list.sel_src[value] = temp
    GUI.ab_setting.list.sel_src.value = value + 1
end
GUI.ab_setting.button.cancel_src.action = function() 
    --GUI.ab_setting.list.gpio_in.value = gpio_in_value
    --GUI.ab_setting.list.gpio_in.action()
    if GUI.ab_setting.list.sel_src.type == "spi_out" then
        update_out_node_list()
    end
    GUI.ab_setting.dialog.src_sel:hide()
end
GUI.ab_setting.button.ok_src.action = function()
    local error = 0 
    if GUI.ab_setting.list.sel_src.type == "file_out" then
        for i = 1, GUI.ab_setting.list.file_out.len+0 do
            GUI.ab_setting.list.file_out[i] = nil
        end
        for i = 1, GUI.ab_setting.list.sel_src.len+0 do
            GUI.ab_setting.list.file_out[i] = GUI.ab_setting.list.sel_src[i]
        end
        GUI.ab_setting.list.file_out.len = GUI.ab_setting.list.sel_src.len
        
        if GUI.ab_setting.toggle.sync_waveview.value == "ON" and GUI.ab_setting.toggle.sync_waveview.active == "YES" then
            sync_waveview_value = "ON"
        else
            sync_waveview_value = "OFF"
        end
    elseif GUI.ab_setting.list.sel_src.type == "ab_out" then
        for i = 1, GUI.ab_setting.list.ab_out.len+0 do
            GUI.ab_setting.list.ab_out[i] = nil
        end
        for i = 1, GUI.ab_setting.list.sel_src.len+0 do
            GUI.ab_setting.list.ab_out[i] = GUI.ab_setting.list.sel_src[i]
        end
        GUI.ab_setting.list.ab_out.len = GUI.ab_setting.list.sel_src.len
    elseif GUI.ab_setting.list.sel_src.type == "spi_out" then
        for i = 1, GUI.ab_setting.list.spi_out.len+0 do
            GUI.ab_setting.list.spi_out[i] = nil
        end
        for i = 1, GUI.ab_setting.list.sel_src.len+0 do
            GUI.ab_setting.list.spi_out[i] = GUI.ab_setting.list.sel_src[i]
        end
        GUI.ab_setting.list.spi_out.len = GUI.ab_setting.list.sel_src.len
        if GUI.ab_setting.list.spi_out.len + 0 ~= GUI.ab_setting.constant.spi_out_max then
            message("Warning", "Only support 8 channels output.")
            error = 1
        else
            update_out_node_list()
        end
    elseif GUI.ab_setting.list.sel_src.type == "waveview" then
        for i = 1, GUI.ab_setting.list.waveview.len+0 do
            GUI.ab_setting.list.waveview[i] = nil
        end
        for i = 1, GUI.ab_setting.list.sel_src.len+0 do
            GUI.ab_setting.list.waveview[i] = GUI.ab_setting.list.sel_src[i]
        end
        GUI.ab_setting.list.waveview.len = GUI.ab_setting.list.sel_src.len

        if sync_waveview_value == "ON" then
            for i = 1, GUI.ab_setting.list.file_out.len+0 do
                GUI.ab_setting.list.file_out[i] = nil
            end
            for i = 1, GUI.ab_setting.list.sel_src.len+0 do
                GUI.ab_setting.list.file_out[i] = GUI.ab_setting.list.sel_src[i]
            end
            GUI.ab_setting.list.file_out.len = GUI.ab_setting.list.sel_src.len
        end
    end

    if error == 0 then 
        GUI.ab_setting.dialog.src_sel:hide()
        ab_setting_to_kernal()
    end
end

GUI.ab_setting.button.update.action = function()
    GUI.system_server.button.update_audio_path.action()
    GUI.system_server.button.update_com_path.action()
    GUI.system_server.button.update_ab_setting.action()
    update_audio_setting()
end

GUI.ab_setting.button.default.action = function()
    printc("Use default setting.")
    GUI.kernel = load_jsonfile1(TEMP_JSON)
    update_system_setting(GUI.system_server.toggle.auto_update.value)
end

GUI.ab_setting.button.version.action = function()
    local state, version = UIF.get_board_ver()
    if state == false then
        message("ERROR", version)
    else
        message("UIF Version", "Version is "..version)
    end
end

GUI.ab_setting.list.ab_in.action = function()
    GUI.ab_setting.list.ab_in.action_time = not GUI.ab_setting.list.ab_in.action_time
    if GUI.ab_setting.list.ab_in.action_time then
        system_setting_sync(0x11)
        update_out_node_src()
    end
end

GUI.ab_setting.list.spi_in.action = function()
    GUI.ab_setting.list.spi_in.action_time = not GUI.ab_setting.list.spi_in.action_time
    if GUI.ab_setting.list.spi_in.action_time then
        printc("SPI input selection: "..GUI.ab_setting.list.spi_in.value)
        GUI.ab_setting.list.spi_in = update_list_value_tab(GUI.ab_setting.list.spi_in)
        system_setting_sync(0x12)
        update_out_node_src()
    end
end

GUI.ab_setting.list.gpio_in.action = function()
    GUI.ab_setting.list.gpio_in.action_time = not GUI.ab_setting.list.gpio_in.action_time
    if GUI.ab_setting.list.gpio_in.action_time then
        printc("GPIO selection: "..GUI.ab_setting.list.gpio_in.value)
        GUI.ab_setting.list.gpio_in = update_list_value_tab(GUI.ab_setting.list.gpio_in)
        system_setting_sync(0x13)
        update_out_node_src()
    end
end

function check_debug_max()
    debug_list1 = GUI.ab_setting.list.debug_sel1.value
    debug_list2 = GUI.ab_setting.list.debug_sel2.value
    local len1 = string.len(debug_list1)
    local len2 = string.len(debug_list2)
    local select_num = 0
    local num_max = GUI.ab_setting.constant.debug_info_max
    local flag_warn = 0
    for i = 1, len1 do
        if string.sub(debug_list1, i, i) == "+" then
            if select_num == num_max then
                message("Warning", "The number of Debug info beyond MAX: "..num_max)
                flag_warn = 1
                local str = string.sub(debug_list1, 1, i-1)  --clear 
                for j = i, len1 do
                    str = str.."-"
                    GUI.ab_setting.list.debug_sel1.value_tab[len1-j+1]=0
                end
                GUI.ab_setting.list.debug_sel1.value = str
                break
            end
            select_num = select_num + 1
        end
    end
   --print(spi_list)
    --print(unpack(GUI.ab_setting.list.debug_sel1.value_tab))
    printc("debug_list1 value: "..debug_list1)
    for i = 1, len2 do
        if string.sub(debug_list2, i, i) == "+" then
            if select_num == num_max then
                if flag_warn == 0 then message("Warning", "The number of Debug info beyond MAX: "..num_max) end
                    local str = string.sub(debug_list2, 1, i-1)  --clear 
                    for j = i, len2 do
                        str = str.."-"
                        GUI.ab_setting.list.debug_sel2.value_tab[len2-j+1]=0
                    end
                    GUI.ab_setting.list.debug_sel2.value = str
                    break
                end
            select_num = select_num + 1
        end
    end
    printc("debug_list2 value: "..debug_list2)
end

GUI.ab_setting.list.debug_sel1.action = function()
    print("Check debug list selection.")
    check_debug_max()
end

GUI.ab_setting.list.debug_sel2.action = function()
print("Check debug list selection.")
    check_debug_max()
end

GUI.ab_setting.list.debug_info1.action = function()
    GUI.ab_setting.list.debug_info1.action_time = not GUI.ab_setting.list.debug_info1.action_time
    if GUI.ab_setting.list.debug_info1.action_time then
        printc("Debug info1 selection: "..GUI.ab_setting.list.debug_info1.value)
        system_setting_sync(0x16)
        update_out_node_src()
    end
end

GUI.ab_setting.list.debug_info2.action = function()
    GUI.ab_setting.list.debug_info2.action_time = not GUI.ab_setting.list.debug_info2.action_time
    if GUI.ab_setting.list.debug_info2.action_time then
        printc("Debug info2 selection: "..GUI.ab_setting.list.debug_info2.value)
        system_setting_sync(0x16)
        update_out_node_src()
    end
end

GUI.ab_setting.list.sc_in_dev.action = function()
     GUI.ab_setting.list.sc_in_dev.action_time = not GUI.ab_setting.list.sc_in_dev.action_time
     if GUI.ab_setting.list.sc_in_dev.action_time then
         GUI.ab_setting.list.sc_in_dev = update_sc_in_list(GUI.ab_setting.list.sc_in_dev)
     end
     ab_setting_to_kernal()
end
GUI.ab_setting.list.sc_out_l.action = function()
    --active_design_save_button("yes")
    ab_setting_to_kernal()
end
GUI.ab_setting.list.sc_out_r.action = function()
    --active_design_save_button("yes")
    ab_setting_to_kernal()
end

GUI.ab_setting.list.sc_out_dev.action = function()
     GUI.ab_setting.list.sc_out_dev.action_time = not GUI.ab_setting.list.sc_out_dev.action_time
     if GUI.ab_setting.list.sc_out_dev.action_time then
         GUI.ab_setting.list.sc_out_dev = update_sc_out_list(GUI.ab_setting.list.sc_out_dev)
     end
     ab_setting_to_kernal()
end

GUI.ab_setting.text.file_in.action = function(self,c,new_value)
--print(self,c,new_value,GUI.ab_setting.text.file_in.value)
    if tonumber(c) == 13 then
        printc("Input file: "..new_value)
        GUI.ab_setting.text.file_in.value = new_value
        system_setting_sync(0x21)
        update_out_node_src()
    end
end

GUI.ab_setting.text.file_out.action = function(self,c,new_value)
    if tonumber(c) == 13 then
        if string.find(new_value, ".wav") == nil then
            new_value = new_value..".wav"
        end
        GUI.ab_setting.text.file_out.value = new_value

        printc("Recording file: "..GUI.ab_setting.text.file_out.value)
        ab_setting_to_kernal()
    end
end

GUI.ab_setting.toggle.ab_in.action = function()
    if GUI.ab_setting.toggle.ab_in.value == "ON" and GUI.ab_setting.toggle.ab_in.active == "YES" then
        GUI.ab_setting.list.ab_in.active = "yes"
        GUI.ab_setting.label.ab_in.active = "yes"
        GUI.ab_setting.list.ab_in.action_time = false
        GUI.ab_setting.list.ab_in.action()
        --[[
        GUI.ab_setting.toggle.gpio_in.active = "yes"
        if GUI.ab_setting.toggle.gpio_in.value == "ON" then
            --GUI.ab_setting.list.gpio_in.active = "yes"
            --GUI.ab_setting.label.gpio_in.active = "yes"
            GUI.ab_setting.button.gpio_in.active = "yes"
        end
        ]]
        GUI.ab_setting.toggle.spi_in.value = "OFF"
        GUI.ab_setting.toggle.spi_in.action()
        GUI.ab_setting.toggle.spi_out.value = "OFF"
        GUI.ab_setting.toggle.spi_out.action()
    else
        GUI.ab_setting.list.ab_in.active = "no"
        GUI.ab_setting.label.ab_in.active = "no"
        GUI.ab_setting.toggle.gpio_in.active = "no"
        GUI.ab_setting.toggle.gpio_in.value = "OFF"
        GUI.ab_setting.button.gpio_in.active = "no"
        update_out_node_src()
    end
end
GUI.ab_setting.toggle.gpio_in.action = function()
    if GUI.ab_setting.toggle.gpio_in.value == "ON" and GUI.ab_setting.toggle.gpio_in.active == "YES" then
        GUI.ab_setting.button.gpio_in.active = "yes"
    else
        GUI.ab_setting.button.gpio_in.active = "no"
    end
    update_out_node_src()
end

function update_debug_toggle_active()
    local spi_list = GUI.ab_setting.list.spi_in.value
    if GUI.ab_setting.toggle.spi_in.active == "YES" and GUI.ab_setting.toggle.spi_in.value == "ON" and string.sub(spi_list, #spi_list, #spi_list) == "+" then
        GUI.ab_setting.toggle.debug_info.active = "yes"
        if GUI.ab_setting.toggle.debug_info.value == "ON" then
            GUI.ab_setting.button.debug_info.active = "yes"
        end
    else
        GUI.ab_setting.toggle.debug_info.active = "no"
        GUI.ab_setting.toggle.debug_info.value = "off"
        GUI.ab_setting.button.debug_info.active = "no"
    end
end

GUI.ab_setting.toggle.spi_in.action = function()
    update_debug_toggle_active()
    if GUI.ab_setting.toggle.spi_in.value == "ON" then
        GUI.ab_setting.button.spi_in.active = "yes"
        GUI.ab_setting.toggle.ab_in.value = "OFF"
        GUI.ab_setting.toggle.ab_in.action()
        GUI.ab_setting.toggle.ab_out.value = "OFF"
        GUI.ab_setting.toggle.ab_out.action()
    else
        GUI.ab_setting.button.spi_in.active = "no"
        if GUI.ab_setting.toggle.spi_out.value == "ON" then
            GUI.ab_setting.toggle.spi_out.value = "OFF"
            GUI.ab_setting.toggle.spi_out.action()
        end
        update_out_node_src()
    end
end
GUI.ab_setting.toggle.debug_info.action = function()
    if GUI.ab_setting.toggle.debug_info.value == "ON" then
        GUI.ab_setting.button.debug_info.active = "yes"
    else
        GUI.ab_setting.button.debug_info.active = "no"
    end
    update_out_node_src()
end

GUI.ab_setting.toggle.ab_out.action = function()
    if GUI.ab_setting.toggle.ab_out.value == "ON" and GUI.ab_setting.toggle.ab_out.active == "YES" then
        GUI.ab_setting.list.ab_out.active = "yes"
        GUI.ab_setting.button.ab_out_src_sel.active = "yes"
        GUI.ab_setting.toggle.spi_out.value = "OFF"
        GUI.ab_setting.toggle.spi_out.action()
        GUI.ab_setting.toggle.spi_in.value = "OFF"
        GUI.ab_setting.toggle.spi_in.action()
    else
        GUI.ab_setting.list.ab_out.active = "no"
        GUI.ab_setting.button.ab_out_src_sel.active = "no"
        ab_setting_to_kernal()
    end
end
GUI.ab_setting.toggle.spi_out.action = function()
    if GUI.ab_setting.toggle.spi_out.value == "ON" and GUI.ab_setting.toggle.spi_out.active == "YES" then
        GUI.ab_setting.list.spi_out.active = "yes"
        GUI.ab_setting.button.spi_out_src_sel.active = "yes"
        GUI.ab_setting.toggle.ab_out.value = "OFF"
        GUI.ab_setting.toggle.ab_out.action()
        GUI.ab_setting.toggle.spi_in.value = "ON"
        GUI.ab_setting.toggle.spi_in.action()
    else
        GUI.ab_setting.list.spi_out.active = "no"
        GUI.ab_setting.button.spi_out_src_sel.active = "no"
        ab_setting_to_kernal()
    end
    --ab_setting_to_kernal()
end
--

GUI.ab_setting.toggle.sc_in.action = function()
    if GUI.ab_setting.toggle.sc_in.value == "ON" then
        GUI.ab_setting.list.sc_in_dev = update_sc_in_list(GUI.ab_setting.list.sc_in_dev)
        GUI.ab_setting.list.sc_in_dev.active = "yes"
    else
        GUI.ab_setting.list.sc_in_dev.active = "no"
    end
    update_out_node_src()
end

GUI.ab_setting.toggle.file_in.action = function()
    if GUI.ab_setting.toggle.file_in.value == "ON" then
        GUI.ab_setting.text.file_in.active = "yes"
        GUI.ab_setting.button.browse_file_in.active = "yes"
    else
        GUI.ab_setting.text.file_in.active = "no"
        GUI.ab_setting.button.browse_file_in.active = "no"
    end
    update_out_node_src()
end

GUI.ab_setting.toggle.sc_out.action = function()
    if GUI.ab_setting.toggle.sc_out.value == "ON" then
        GUI.ab_setting.list.sc_out_dev = update_sc_out_list(GUI.ab_setting.list.sc_out_dev)
        GUI.ab_setting.list.sc_out_dev.active = "yes"
        GUI.ab_setting.list.sc_out_l.active = "yes"
        GUI.ab_setting.list.sc_out_r.active = "yes"
        GUI.ab_setting.label.sc_out_l.active = "yes"
        GUI.ab_setting.label.sc_out_r.active = "yes"
    else
        GUI.ab_setting.list.sc_out_dev.active = "no"
        GUI.ab_setting.list.sc_out_l.active = "no"
        GUI.ab_setting.list.sc_out_r.active = "no"
        GUI.ab_setting.label.sc_out_l.active = "no"
        GUI.ab_setting.label.sc_out_r.active = "no"
    end
    ab_setting_to_kernal()
    --[[
    for k, v in pairs(GUI.kernel.node) do
        if GUI.kernel.node[k].name == "sc_out" then
            GUI.kernel.node[k].active = GUI.ab_setting.toggle.sc_out.value
            active_design_save_button("yes")
        end
    end
    ]]
end

GUI.ab_setting.toggle.file_out.action = function()
    if GUI.ab_setting.toggle.file_out.value == "ON" then
        GUI.ab_setting.text.file_out.active = "yes"
        GUI.ab_setting.button.browse_file_out.active = "yes"
        GUI.ab_setting.button.fileout_src_sel.active = "yes"
    else
        GUI.ab_setting.text.file_out.active = "no"
        GUI.ab_setting.button.browse_file_out.active = "no"
        GUI.ab_setting.button.fileout_src_sel.active = "no"
    end
    --[[
    if GUI.ab_setting.list.sel_src.type == "file_out" then
        GUI.ab_setting.dialog.src_sel:hide()
    end
    ]]
    ab_setting_to_kernal()
    --[[
    for k, v in pairs(GUI.kernel.node) do
        if GUI.kernel.node[k].name == "file_out" then
            GUI.kernel.node[k].active = GUI.ab_setting.toggle.file_out.value
            active_design_save_button("yes")
        end
    end
    ]]
end

GUI.ab_setting.toggle.waveview.action = function()
    if GUI.ab_setting.toggle.waveview.value == "ON" then
        GUI.ab_setting.list.waveview.active = "yes"
        GUI.ab_setting.button.waveview_src_sel.active = "yes"
    else
        GUI.ab_setting.list.waveview.active = "no"
        GUI.ab_setting.button.waveview_src_sel.active = "no"
    end
    --[[
    if GUI.ab_setting.list.sel_src.type == "waveview" then
        GUI.ab_setting.dialog.src_sel:hide()
    end
    ]]
    ab_setting_to_kernal()
    --[[
    for k, v in pairs(GUI.kernel.node) do
        if GUI.kernel.node[k].name == "waveview" then
            GUI.kernel.node[k].active = GUI.ab_setting.toggle.waveview.value
            active_design_save_button("yes")
        end
    end
    ]]
end

function list_convert2_scr_tab()
    update_out_node_list()
end

ab_setting_active = {}
function save_ab_setting_active()
    ab_setting_active[1] = GUI.ab_setting.toggle.ab_in.active
    ab_setting_active[2] = GUI.ab_setting.label.ab_in.active
    ab_setting_active[3] = GUI.ab_setting.list.ab_in.active
    ab_setting_active[4] = GUI.ab_setting.toggle.spi_in.active
    ab_setting_active[5] = GUI.ab_setting.button.spi_in.active
    ab_setting_active[6] = GUI.ab_setting.toggle.gpio_in.active
    ab_setting_active[7] = GUI.ab_setting.button.gpio_in.active
    ab_setting_active[8] = GUI.ab_setting.toggle.sc_in.active
    ab_setting_active[9] = GUI.ab_setting.list.sc_in_dev.active
    ab_setting_active[10] = GUI.ab_setting.toggle.file_in.active
    ab_setting_active[11] = GUI.ab_setting.text.file_in.active
    ab_setting_active[12] = GUI.ab_setting.button.browse_file_in.active
    ab_setting_active[13] = GUI.ab_setting.toggle.sc_out.active
    ab_setting_active[14] = GUI.ab_setting.list.sc_out_dev.active
    ab_setting_active[15] = GUI.ab_setting.label.sc_out_l.active
    ab_setting_active[16] = GUI.ab_setting.list.sc_out_l.active
    ab_setting_active[17] = GUI.ab_setting.label.sc_out_r.active
    ab_setting_active[18] = GUI.ab_setting.list.sc_out_r.active
    ab_setting_active[19] = GUI.ab_setting.toggle.file_out.active
    ab_setting_active[20] = GUI.ab_setting.text.file_out.active
    ab_setting_active[21] = GUI.ab_setting.button.browse_file_out.active
    ab_setting_active[22] = GUI.ab_setting.button.fileout_src_sel.active
    ab_setting_active[23] = GUI.ab_setting.toggle.waveview.active
    ab_setting_active[24] = GUI.ab_setting.button.waveview_src_sel.active
    ab_setting_active[25] = GUI.ab_setting.toggle.ab_out.active
    ab_setting_active[26] = GUI.ab_setting.button.ab_out_src_sel.active
    ab_setting_active[27] = GUI.ab_setting.toggle.spi_out.active
    ab_setting_active[28] = GUI.ab_setting.button.spi_out_src_sel.active
    ab_setting_active[29] = GUI.ab_setting.toggle.debug_info.active
    ab_setting_active[30] = GUI.ab_setting.button.debug_info.active
end

function return_ab_setting_active()
    GUI.ab_setting.toggle.ab_in.active = ab_setting_active[1]
    GUI.ab_setting.label.ab_in.active = ab_setting_active[2]
    GUI.ab_setting.list.ab_in.active = ab_setting_active[3]
    GUI.ab_setting.toggle.spi_in.active = ab_setting_active[4]
    GUI.ab_setting.button.spi_in.active = ab_setting_active[5]
    GUI.ab_setting.toggle.gpio_in.active = ab_setting_active[6]
    GUI.ab_setting.button.gpio_in.active = ab_setting_active[7]
    GUI.ab_setting.toggle.sc_in.active = ab_setting_active[8]
    GUI.ab_setting.list.sc_in_dev.active = ab_setting_active[9]
    GUI.ab_setting.toggle.file_in.active = ab_setting_active[10]
    GUI.ab_setting.text.file_in.active = ab_setting_active[11]
    GUI.ab_setting.button.browse_file_in.active = ab_setting_active[12]
    GUI.ab_setting.toggle.sc_out.active = ab_setting_active[13]
    GUI.ab_setting.list.sc_out_dev.active = ab_setting_active[14]
    GUI.ab_setting.label.sc_out_l.active = ab_setting_active[15]
    GUI.ab_setting.list.sc_out_l.active = ab_setting_active[16]
    GUI.ab_setting.label.sc_out_r.active = ab_setting_active[17]
    GUI.ab_setting.list.sc_out_r.active = ab_setting_active[18]
    GUI.ab_setting.toggle.file_out.active = ab_setting_active[19]
    GUI.ab_setting.text.file_out.active = ab_setting_active[20]
    GUI.ab_setting.button.browse_file_out.active = ab_setting_active[21]
    GUI.ab_setting.button.fileout_src_sel.active = ab_setting_active[22]
    GUI.ab_setting.toggle.waveview.active = ab_setting_active[23]
    GUI.ab_setting.button.waveview_src_sel.active = ab_setting_active[24]
    GUI.ab_setting.toggle.ab_out.active = ab_setting_active[25]
    GUI.ab_setting.button.ab_out_src_sel.active = ab_setting_active[26]
    GUI.ab_setting.toggle.spi_out.active = ab_setting_active[27]
    GUI.ab_setting.button.spi_out_src_sel.active = ab_setting_active[28]
    GUI.ab_setting.toggle.debug_info.active = ab_setting_active[29]
    GUI.ab_setting.button.debug_info.active = ab_setting_active[30]
end

GUI.ab_setting.active = function(active)
    active = active or "yes"
    if (active == "yes") or (active == "YES") then
        return_ab_setting_active()
        print(GUI.ab_setting.toggle.ab_in.active)
    else
        save_ab_setting_active()
        GUI.ab_setting.toggle.ab_in.active = "NO"
        GUI.ab_setting.label.ab_in.active = "NO"
        GUI.ab_setting.list.ab_in.active = "NO"
        GUI.ab_setting.toggle.spi_in.active = "NO"
        GUI.ab_setting.button.spi_in.active = "NO"
        GUI.ab_setting.toggle.gpio_in.active = "NO"
        GUI.ab_setting.button.gpio_in.active = "NO"
        GUI.ab_setting.toggle.sc_in.active = "NO"
        GUI.ab_setting.list.sc_in_dev.active = "NO"
        GUI.ab_setting.toggle.file_in.active = "NO"
        GUI.ab_setting.text.file_in.active = "NO"
        GUI.ab_setting.button.browse_file_in.active = "NO"
        GUI.ab_setting.toggle.sc_out.active = "NO"
        GUI.ab_setting.list.sc_out_dev.active = "NO"
        GUI.ab_setting.label.sc_out_l.active = "NO"
        GUI.ab_setting.list.sc_out_l.active = "NO"
        GUI.ab_setting.label.sc_out_r.active = "NO"
        GUI.ab_setting.list.sc_out_r.active = "NO"
        GUI.ab_setting.toggle.file_out.active = "NO"
        GUI.ab_setting.text.file_out.active = "NO"
        GUI.ab_setting.button.browse_file_out.active = "NO"
        GUI.ab_setting.button.fileout_src_sel.active = "NO"
        GUI.ab_setting.toggle.waveview.active = "NO"
        GUI.ab_setting.button.waveview_src_sel.active = "NO"
        GUI.ab_setting.toggle.ab_out.active = "NO"
        GUI.ab_setting.button.ab_out_src_sel.active = "NO"
        GUI.ab_setting.toggle.spi_out.active = "NO"
        GUI.ab_setting.button.spi_out_src_sel.active = "NO"
        GUI.ab_setting.toggle.debug_info.active = "no"
        GUI.ab_setting.button.debug_info.active = "no"
    end
end
--GUI.ab_setting.button.default.action()

dofile(SCRIPT_PATH..sub_dirctory.."GUI_config_ab.lua")

save_ab_setting_active()
