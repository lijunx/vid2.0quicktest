local temp_i2c_speed = GUI.com_path.text.i2c_speed.value
local temp_spi_speed = GUI.com_path.text.spi_speed.value
printc("define VID operation GUI module.")
PDMCLK_ONOFF = 1
DSP_RUN = 0
DL_STATE = 0
EFT_FILE = import_gui_default("eft_file")
EFT_ADDR = import_gui_default("eft_addr")
--print(EFT_ADDR)
EFT_ADDR = EFT_ADDR == "" and iM501.eft_code[1][1] or (EFT_ADDR + 0)
--module define
GUI.VID = {};
GUI.VID.constant = {};
GUI.VID.list = 
{
    mic0_i2c_addr = iup.list
    {
        "0xA0","0xA2","0xA4","0xA6";
        dropdown="YES", visible_items=4, value = 3, size="40x", action_time = false,
    };
    mic1_i2c_addr = iup.list
    {
        "0xA0","0xA2","0xA4","0xA6";
        dropdown="YES", visible_items=4, value = 4, size="40x", action_time = false,
    };
    mic2_i2c_addr = iup.list
    {
        "0xA0","0xA2","0xA4","0xA6";
        dropdown="YES", visible_items=4, value = 1, size="40x", action_time = false,
    };
    mic3_i2c_addr = iup.list
    {
        "0xA0","0xA2","0xA4","0xA6";
        dropdown="YES", visible_items=4, value = 2, size="40x", action_time = false,
    };
    xaa_timer = iup.list
    {
        dropdown="YES", visible_items=8, value = 1, size="45x", action_time = false,
    };
    avd_clear_timer = iup.list
    {
        dropdown="YES", visible_items=4, value = 1, size="45x", action_time = false,
    };
    psm_xsd_adc = iup.list
    {
        "Low power ADC","Normal ADC";
        dropdown="YES", visible_items=3, size="80x", value = 1, action_time = false,
        action=function()
            printc("Power saving mode xSD use "..GUI.im501_vr_setting.list.psm_xsd_adc[GUI.im501_vr_setting.list.psm_xsd_adc.value])
        end
    };
    psm_sd_adc = iup.list
    {
        "Low power ADC","Normal ADC";
        dropdown="YES", visible_items=3, size="80x", value = 1, action_time = false,
        action=function()
            printc("Power saving mode SD use "..GUI.VID.list.psm_sd_adc[GUI.VID.list.psm_sd_adc.value])
        end
    };
    xsd = iup.list
    {
        "Hi";
        dropdown="YES", visible_items=1, size="40x", value = 1, action_time = false, active = "no"
    };
    sd_kw = iup.list
    { 
        dropdown="YES", visible_items=3, size="80x", value = 1, action_time = false,
        action=function()
            printc("SD voice is "..GUI.VID.list.sd_kw[GUI.VID.list.sd_kw.value])
        end
    };
    sd_function = iup.list
    { 
        dropdown="YES", visible_items=3, size="120x", value = 1, action_time = false,
        action=function()
            printc("SD function is "..GUI.VID.list.sd_function[GUI.VID.list.sd_function.value])
        end
    };
    nm_sr = iup.list
    {
        "8000", "16000";
        dropdown="YES", visible_items=1, size="40x", value = 1, action_time = false, active = "no"
    };
    pdm0o = iup.list
    {
        dropdown="YES", visible_items=1, size="40x", value = 1, action_time = false, active = "no"
    };
    pdm1o = iup.list
    {
        dropdown="YES", visible_items=1, size="40x", value = 1, action_time = false, active = "no"
    };
};

GUI.VID.list.xaa_timer = update_list_item(GUI.VID.list.xaa_timer, iM501.avd_attribute.xaa_timer.option)
GUI.VID.list.avd_clear_timer = update_list_item(GUI.VID.list.avd_clear_timer, iM501.avd_attribute.avd_clr_timer[1].option)
GUI.VID.list.sd_kw = update_list_item(GUI.VID.list.sd_kw, iM501.sd_attribute.option)
GUI.VID.list.sd_function = update_list_item(GUI.VID.list.sd_function, iM501.sd_function.option)
GUI.VID.list.pdm0o = update_list_item(GUI.VID.list.pdm0o, iM501.output_attribute.option)
GUI.VID.list.pdm1o = update_list_item(GUI.VID.list.pdm1o, iM501.output_attribute.option)

GUI.VID.image = 
{
    aline = iup.image
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}, 
        {3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3}, 
        {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4}, 
        {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5}, 
        {6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    shortaline = iup.image
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}, 
        {3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3}, 
        {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4}, 
        {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5}, 
        {6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 

        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    right2down = iup.image
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1}, 
        {3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1}, 
        {4,4,4,4,4,4,4,4,4,4,4,4,4,1,1,1}, 
        {5,5,5,5,5,5,5,5,5,5,5,5,5,1,1,1}, 
        {6,6,6,6,6,6,6,6,6,6,6,6,6,1,1,1}, 
        {1,1,1,1,1,1,1,1,2,3,4,5,6,1,1,1}, 
        {1,1,1,1,1,1,1,1,2,3,4,5,6,1,1,1}, 
        {1,1,1,1,1,1,1,1,2,3,4,5,6,1,1,1}, 
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    right2up = iup.image
    {
        {1,1,1,1,1,1,1,1,2,3,4,5,6,1,1,1}, 
        {1,1,1,1,1,1,1,1,2,3,4,5,6,1,1,1}, 
        {1,1,1,1,1,1,1,1,2,3,4,5,6,1,1,1}, 
        {2,2,2,2,2,2,2,2,2,3,4,5,6,1,1,1}, 
        {3,3,3,3,3,3,3,3,3,3,4,5,6,1,1,1}, 
        {4,4,4,4,4,4,4,4,4,4,4,5,6,1,1,1}, 
        {5,5,5,5,5,5,5,5,5,5,5,5,6,1,1,1}, 
        {6,6,6,6,6,6,6,6,6,6,6,6,6,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    left2down = iup.image
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2}, 
        {1,1,1,2,3,3,3,3,3,3,3,3,3,3,3,3}, 
        {1,1,1,2,3,4,4,4,4,4,4,4,4,4,4,4}, 
        {1,1,1,2,3,4,5,5,5,5,5,5,5,5,5,5}, 
        {1,1,1,2,3,4,5,6,6,6,6,6,6,6,6,6}, 
        {1,1,1,2,3,4,5,6,1,1,1,1,1,1,1,1}, 
        {1,1,1,2,3,4,5,6,1,1,1,1,1,1,1,1}, 
        {1,1,1,2,3,4,5,6,1,1,1,1,1,1,1,1}, 
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    left2up = iup.image
    {
        {1,1,1,2,3,4,5,6,1,1,1,1,1,1,1,1}, 
        {1,1,1,2,3,4,5,6,1,1,1,1,1,1,1,1}, 
        {1,1,1,2,3,4,5,6,1,1,1,1,1,1,1,1}, 
        {1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2}, 
        {1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3}, 
        {1,1,1,4,4,4,4,4,4,4,4,4,4,4,4,4}, 
        {1,1,1,5,5,5,5,5,5,5,5,5,5,5,5,5}, 
        {1,1,1,6,6,6,6,6,6,6,6,6,6,6,6,6}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    down2right = iup.image
    {
        {1,1,2,3,4,5,6,1,1,1},
        {1,1,2,3,4,5,6,1,1,1},
        {1,1,2,3,4,5,6,1,1,1},
        {1,1,2,2,2,2,2,2,2,2},
        {1,1,3,3,3,3,3,3,3,3},
        {1,1,4,4,4,4,4,4,4,4},
        {1,1,5,5,5,5,5,5,5,5},
        {1,1,6,6,6,6,6,6,6,6},
        {1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,1,1,1,1,1,1}, 
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    vline = iup.image
    {
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},

        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    arrowleft = iup.image
    {
        {1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,2,2,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,2,2,2,1,1,1,1,1,1,1,1,1,1},
        {1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
        {1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3},
        {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
        {1,5,4,4,4,5,5,5,5,5,5,5,5,5,5,5},
        {1,1,6,5,5,6,6,6,6,6,6,6,6,6,6,6},
        {1,1,1,6,5,6,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,6,6,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,1,6,1,1,1,1,1,1,1,1,1,1},

        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    arrowup = iup.image
    {
        {1,1,1,1,1,4,1,1,1,1,1},
        {1,1,1,1,3,4,5,1,1,1,1},
        {1,1,2,2,3,4,4,5,6,1,1},
        {1,2,2,2,3,4,4,5,5,6,1},
        {2,2,2,2,3,4,5,6,6,6,6},
        {1,1,1,2,3,4,4,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    arrowup_short = iup.image
    {
        {1,1,1,1,1,4,1,1,1,1,1},
        {1,1,1,1,3,4,5,1,1,1,1},
        {1,1,2,2,3,4,4,5,6,1,1},
        {1,2,2,2,3,4,4,5,5,6,1},
        {2,2,2,2,3,4,5,6,6,6,6},
        {1,1,1,2,3,4,4,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},

        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    arrowdown = iup.image
    {
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,5,6,1,1,1},
        {1,1,1,2,3,4,4,6,1,1,1},
        {2,2,2,2,3,4,5,6,6,6,6},
        {1,2,2,2,3,4,4,5,5,6,1},
        {1,1,2,2,3,4,4,5,6,1,1},
        {1,1,1,1,3,4,5,1,1,1,1},
        {1,1,1,1,1,4,1,1,1,1,1},

        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
    arrowright = iup.image
    {
        {1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,2,2,1,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,2,2,2,1,1},
        {2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1},
        {3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1},
        {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
        {5,5,5,5,5,5,5,5,5,5,4,5,4,4,5,1},
        {6,6,6,6,6,6,6,6,6,6,6,6,5,5,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,6,5,6,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,6,6,1,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,6,1,1,1,1},


        colors = {"240 240 240", "137 156 127", "151 192 137", "91 139 75", "80 129 67", "28 34 27"}
    },
}

GUI.VID.label = 
{
    JTAG = iup.label{title = "JTAG\nMode", size = "24x16"};
	main_title = iup.label{title = "Lower Power Always On Acoustics Awareness"};
	voice_buffer = iup.label{title = "Voice Buffer"};
	dsp = iup.label{title = "DSP"};
	hardware = iup.label{title = "Hardware"};
	psm_state1 = iup.label{title = "State I"};
	psm_state2 = iup.label{title = "State II"};
	
    vb = iup.label{title = "One Shot Voice Buffer Data Recorder"};
    header = iup.label{title = "Start Address of header.bin:"};
    net = iup.label{title =    "Start Address of net.bin:      "};
    gram = iup.label{title =   "Start Address of gram.bin:   "};
    eft_code = iup.label{title =   "File:"};
    eft_addr = iup.label{title =   "Start Address(0x):"};
    im501 = iup.label{title = "iM501:", FONT = "Arial::24"};
    im205 = iup.label{title = "iM205:", FONT = "Arial::24"};
    unit_im501 = iup.label{title = "mADC", FONT = "Arial::24"};
    unit_im205 = iup.label{title = "mADC", FONT = "Arial::24"};
    code = iup.label{title = "RAM Code:"};
    vec = iup.label{title =  "Configuration:",size="57x"};

    mic0_i2c_addr = iup.label{title = "I2C Address:"};
    mic1_i2c_addr = iup.label{title = "I2C Address:"};
    mic2_i2c_addr = iup.label{title = "I2C Address:"};
    mic3_i2c_addr = iup.label{title = "I2C Address:"};
    mic_reg_addr = iup.label{title = "Reg(0x):"};
    mic_reg_value = iup.label{title = "Value(0x):"};
    
    separator1 = iup.label{separator = "HORIZONTAL"};
    separator2 = iup.label{separator = "HORIZONTAL"};
    separator3 = iup.label{separator = "HORIZONTAL"};
    separator4 = iup.label{separator = "HORIZONTAL"};
    separator5 = iup.label{separator = "HORIZONTAL"};
    separator6 = iup.label{separator = "HORIZONTAL"};
    separator7 = iup.label{separator = "HORIZONTAL"};
    separator_modeswitch = iup.label{separator = "HORIZONTAL"};


    avd = iup.label{title = "    AVD  \ndetection"};
    avd_no = iup.label{title = "No"};
    avd_yes = iup.label{title = "Yes"};
    
    sleep = iup.label{title = "Sleep"};--, size = "50x", bgcolor = GUI.color.green};

    xsd = iup.label{title = "   xSD  \ndetection"};--, size = "50x", bgcolor = GUI.color.gray};
    xsd_no = iup.label{title = "No"};
    xsd_yes = iup.label{title = "Yes"};

    sd = iup.label{title = "     SD  \ndetection"};--, size = "50x", bgcolor = GUI.color.gray};
    sd_no = iup.label{title = "No"};
    sd_yes = iup.label{title = "Yes"};
    sd_kw = iup.label{title = "Trigger Phrase:"};
    sd_function = iup.label{title = "Function:"};
    psm_sd_adc = iup.label{title = "ADC Type:"};

    vb_en = iup.label{title = "Voice buffer\nread enable"};
    vb_read = iup.label{title = "Voice buffer\n     read"};
    vb_en_yes = iup.label{title = "Yes", size = "x8"};
    vb_en_no = iup.label{title = "No"};
    
    spl = iup.label{title = "SPL Output: "};
    noise_type = iup.label{title = "Noise Type:"};
	xaa_floor = iup.label{title = "Floor:"};

    spi_read_interval = iup.label{title = "SPI Read Interval Time(s):  ", size = "x8"};
    spi_read_cycle = iup.label{title =    "SPI Read Cycle:                 ", size = "x8"};
    after_spir_mode = iup.label{title =   "Work mode after oneshot: ", size = "x8"};
    
    wakeup_time = iup.label{title = "Host wake up time(s): "};
    back_to_psm_delay = iup.label{title = "Back to PSM delay time(s): "};
    --back_to_psm_delay_comment = iup.label{title = "0 means keep normal mode."};
    nm_sr = iup.label{title = "Sample Rate:"};
    pdm0o = iup.label{title = "PDMO Output signal:"};
    pdm1o = iup.label{title = "PDMO Output signal:"};
    
    avd_reg6 = iup.label{title = "Be written to Reg 0x6:", active = "no"};
    xaa_timer = iup.label{title =       "xAA Timer:         "};
    avd_clear_timer = iup.label{title = "AVD Clear Timer:"};

    command_detect = iup.label{title = "Command Detect:"};
    command_num = iup.label{title = "Command Number:"};
    dsp_info = iup.label{title = "",expand = "YES"};
};

GUI.VID.spin = 
{
    wakeup_time = iup.spin{spinvalue = 0.01, spinmax = 3, spininc = 0.01, value=0.01};
    --back_to_psm_delay = iup.spin{ GUI.VID.text.back_to_psm_delay};
    --spi_read_interval = iup.spin{ GUI.VID.text.spi_read_interval};
    --spi_read_cycle = iup.spin{ GUI.VID.text.spi_read_cycle};
}

GUI.VID.text =
{
    ram_code = iup.text{value = CODE_VEC_PATH, size="120x"};
    vec = iup.text{value = CODE_VEC_PATH..iM501.vec.init, size="120x"};
    eft_code = iup.text{value = EFT_FILE, size="140x"};
    eft_addr = iup.text{value = string.format("0x%08x", EFT_ADDR), size="60x"};
    current_im501 = iup.text{value = "", size="40x", READONLY = "YES", font = "Arial::36", FGCOLOR = "34 177 76", BGCOLOR = "240 240 240"};
    current_im205 = iup.text{value = "", size="40x", READONLY = "YES", font = "Arial::36", FGCOLOR = "0 162 232", BGCOLOR = "240 240 240"};
    --header = iup.text{value = string.format("0x%08x", iM501.eft_code[1][1]), size="60x"};
    --net = iup.text{value = string.format("0x%08x", iM501.eft_code[2][1]), size="60x"};
    --gram = iup.text{value = string.format("0x%08x", iM501.eft_code[3][1]), size="60x"};
    spl = iup.text{value = "", size="40x", READONLY = "YES", alignment = "ARIGHT"};
    noise_type = iup.text{value = "", size="40x", READONLY = "YES", alignment = "ARIGHT"};
	xaa_floor = iup.text{value = "", size="40x", READONLY = "YES", alignment = "ARIGHT"};
    wakeup_time = iup.text{value = 0.01, size = "35x", spin = "YES", spinmax = "3", spininc = "0.01",};
    back_to_psm_delay = iup.text{value = 3, size = "35x", spin = "YES", spinmin = "0", spinmax = "5", spininc = "1",tip = "0 means keep normal mode."};
    spi_read_interval = iup.text{value = 0, size = "35x", spin = "YES", spinmin = "0", spinmax = "3", spininc = "1"};
    spi_read_cycle = iup.text{value = 0, size = "35x", spin = "YES", spinmin = "0", spinmax = "3", spininc = "1"};

    mic_reg_addr = iup.text{value = import_gui_default("im205_reg_addr"), size = "40x", spin = "YES", spinmax = "16", spininc = "1",};
    mic_reg_value = iup.text{value = import_gui_default("im205_reg_value"), size = "30x"};

    avd_reg6 = iup.text{value = import_gui_default("avd_reg6"), size = "30x", active = "NO"};

    command_detect = iup.text{value = string.format("0x%07x", 0), size="50x", READONLY = "YES", alignment = "ARIGHT"};
    command_num = iup.text{value = 0, size="20x", READONLY = "YES", alignment = "ARIGHT"};
};

GUI.VID.toggle =
{
    JTAG = iup.toggle{title=" ",value="OFF";};

    mic = iup.toggle{title="iM205 Smart MIC",value="ON";};
    bypass = iup.toggle{title="HW Bypass Mode",value="OFF"};
    nm = iup.toggle{title="Normal Mode",value="ON";};
    psm = iup.toggle{title="Low Power Always On",value="OFF"};
    one_shot  = iup.toggle{title="One Shot",value="OFF"};
    avd = iup.toggle{title="AVD",value="ON"};
    ven  = iup.toggle{title="VEN",value="ON"};
    psm_xaa = iup.toggle{title="xAA",value="ON", active = "no"};
    psm_xsd = iup.toggle{title="xSD",value="ON"};
    psm_sd  = iup.toggle{title="SD",value="ON"};
    nm_xaa  = iup.toggle{title="xAA",value="OFF"};
    nm_config = iup.toggle{title="Normal Mode",value="ON", active = "no"};
    psm_config = iup.toggle{title="Low Power Alway On Mode",value="ON", active = "no"};
    one_shot_config = iup.toggle{title="One Shot",value="ON", active = "no"};
    spi_read = iup.toggle{title="Voice Buffer Read",value="ON"};
    ultra_sound = iup.toggle{title="Ultra Sound",value="OFF", active = "no"};
    third_part_sw = iup.toggle{title="Third Part SW",value="OFF", active = "no"};
    
    --vb = iup.toggle{title="Voice Buffer Read",value="OFF";};
    bin = iup.toggle{title="Binary",value="ON";};
    text = iup.toggle{title="Text",value="OFF";};

    spi2psm = iup.toggle{title="Sleep",value="OFF";};
    spi2nm = iup.toggle{title="Normal Mode",value="OFF";};

    mic_cfg_by_501 = iup.toggle{title="Access by iM501",value="ON";};
    mic_cfg_by_host = iup.toggle{title="Access by Host",value="OFF";};
    mic0_active = iup.toggle{title="Active", value = "ON"};
    mic1_active = iup.toggle{title="Active", value = "OFF"};
    mic2_active = iup.toggle{title="Active", value = "ON"};
    mic3_active = iup.toggle{title="Active", value = "OFF"};    
    mic0type_a = iup.toggle{title ="Analog MIC", value = "ON"};
    mic0type_d = iup.toggle{title ="Digital MIC", value = "OFF"};
    mic1type_a = iup.toggle{title ="Analog MIC", value = "ON"};
    mic1type_d = iup.toggle{title ="Digital MIC", value = "OFF"};
    mic2type_a = iup.toggle{title ="Analog MIC", value = "ON"};
    mic2type_d = iup.toggle{title ="Digital MIC", value = "OFF"};
    mic3type_a = iup.toggle{title ="Analog MIC", value = "ON"};
    mic3type_d = iup.toggle{title ="Digital MIC", value = "OFF"};

    hex = iup.toggle{title="H",value="ON"};
    dec = iup.toggle{title="D",value="OFF"};

    spi_record = iup.toggle{title="SPI Recording", value="OFF"};
}

GUI.VID.radio = 
{
    mode_switch = iup.radio
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.VID.toggle.nm,
                GUI.VID.toggle.psm,
                GUI.VID.toggle.one_shot;
                gap="10",margin="10x10",alignment = "ACENTER"
            };
            GUI.VID.label.separator_modeswitch,
            iup.hbox
            {
                GUI.VID.toggle.bypass;
                gap="10",margin="10x10",alignment = "ACENTER"
            };
        };
        value=GUI.VID.toggle.nm;
    };
    file_type = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.bin, 
            GUI.VID.toggle.text, 
            gap="20",margin="0",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.bin;
    };
    after_spir_mode = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.spi2psm, 
            GUI.VID.toggle.spi2nm, 
            gap="10",margin="0",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.spi2nm;
    };
    sw_switch = iup.radio
    {
        iup.vbox
        {
            GUI.VID.toggle.ven, 
            GUI.VID.toggle.ultra_sound, 
            GUI.VID.toggle.third_part_sw,
            gap="8",margin="5x0",alignment = "LEFT"
        };
        value=GUI.VID.toggle.ven;
    };
    mic_access = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.mic_cfg_by_501, 
            GUI.VID.toggle.mic_cfg_by_host, 
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.mic_cfg_by_501;
    };
    mic0_type = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.mic0type_a, 
            GUI.VID.toggle.mic0type_d, 
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.mic0type_a;
    };
    mic1_type = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.mic1type_a, 
            GUI.VID.toggle.mic1type_d, 
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.mic1type_a;
    };
    mic2_type = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.mic2type_a, 
            GUI.VID.toggle.mic2type_d, 
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.mic2type_a;
    };
    mic3_type = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.mic3type_a, 
            GUI.VID.toggle.mic3type_d, 
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.mic3type_a;
    };
    unit_switch = iup.radio
    {
        iup.hbox
        {
            GUI.VID.toggle.hex, 
            GUI.VID.toggle.dec, 
            gap="0",margin="0x0",alignment = "ACENTER"
        };
        value=GUI.VID.toggle.hex;
    };
}

GUI.VID.button =
{
    reset = iup.button{size="30x24", title="Reset"};
    run = iup.button{size="30x24", title="Run", fgcolor = GUI.color.black, active = "no"};
    dl = iup.button{size="40x", title="Load"};
    dl_mode = iup.button{image = "IUP_ToolsSettings", tip = "Download option",size = "16x13"};
    dl_vec = iup.button{size="40x", title="Load"};
    --[[
    reset = iup.button{size="65x", title="Reset"};
    run = iup.button{size="65x", title="Run", fgcolor = GUI.color.black, active = "no"};
    dl = iup.button{size="65x", title="Load"};
    ]] --current
    
    vb_start = iup.button{size="40x", title="Start", active = "yes"};
    vb_stop = iup.button{size="40x", title="Stop", active = "yes"};
    browse_code = iup.button{size="20x", image = "IUP_FileOpen"}; --title="..."};
    browse_vec = iup.button{size="20x", image = "IUP_FileOpen"}; --title="..."};
    browse_eft = iup.button{size="65x", title="Browse"};
    --eft_start_addr = iup.button{size="65x", title="Start Address"};
    dl_eft = iup.button{size="65x", title="Download"};
    --start_addr_ok = iup.button{size="65x", title="OK"};
    --start_addr_cancel = iup.button{size="65x", title="Cancel"};
    
    vid = iup.button{size="30x24", title="VID", active = "no"};

    --test = iup.Button{size="20x20", title = "test", flat = "YES", image = "iup.Cancel", tip = "test"};
    mic_set = iup.button{image = "IUP_ToolsSettings", flat = "YES"}; --title="Setting"};
    avd_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"}; --title="Setting"};
    psm_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"};--, active = "no"};
    nm_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"}; --title="Setting"};
    xsd_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"}; --title="Setting"};
    sd_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"}; --title="Setting"};
    ven_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"}; --title="Setting"};
    xaa_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"}; --title="Setting"};
    one_shot_set = iup.button{flat = "YES", image = "IUP_ToolsSettings"};
    ultra_sound = iup.button{flat = "YES", image = "IUP_ToolsSettings", active = "no"};
    third_part_sw = iup.button{flat = "YES", image = "IUP_ToolsSettings", active = "no"};

    --spi_record = iup.button{size="70x", title="SPI Recording"};
    dsp_control = iup.button{size="70x", title="DSP Control"};
    mic_control = iup.button{size="80x", title="Smart MIC Control"};
    update_set = iup.button{size="70x", title="Update Setting", image = "IUP_NavigateRefresh"};
    monitor_current = iup.button{size="70x", title="Current Monitor"};

    mic_read = iup.button{size="20x", title="R",};
    mic_write = iup.button{size="20x", title="W",};
    
    command_read = iup.button{size="20x", title="",image = "IUP_NavigateRefresh",
        action = function()
            local flag, command_num, command_detect = iM501.check_command()
             if flag then
                     GUI.VID.text.command_detect.value = string.format("0x%07x", command_detect)
                     GUI.VID.text.command_num.value = command_num
             end
        end
    };
};

GUI.VID.time = 
{
    current_timer = iup.timer{time=1000, run="NO"},
    --command_timer = iup.timer{time=1000, run="NO"},
    oneshot_timer = iup.timer{time=T_IRQ2CLKON + T_SPIR2SPIR*C_SPIR + T_NM2PSM, run="NO"},
    xaa_timer = iup.timer{time=1000, run="NO"},
    command_timer = iup.timer{time=1000, run="NO"},
}

--Frame
GUI.VID.module = 
{
    JTAG = iup.vbox{GUI.VID.toggle.JTAG, GUI.VID.label.JTAG, gap="0",margin="0x0", alignment = "ACENTER"};

    mic_set = iup.hbox{GUI.VID.toggle.mic, GUI.VID.button.mic_set, gap="0",margin="5x5", alignment = "ACENTER"};
    avd_detection = iup.frame
    {
        iup.vbox{GUI.VID.label.avd,gap="0x0",margin="5x3", alignment = "ACENTER"};
        bgcolor = GUI.color.gray, sunken = "YES"
    };
    sleep = iup.frame
    {
        iup.vbox{GUI.VID.label.sleep,gap="0x0",margin="16x11", alignment = "ACENTER"};
        bgcolor = GUI.color.green, sunken = "YES"
    };
    psm_set = iup.hbox
    {
        iup.fill{size = "85x"};
        GUI.VID.toggle.psm_config,
        GUI.VID.button.psm_set;
        gap="0",margin="0x5", alignment = "ACENTER"
    };
    vb = iup.frame
    {
        iup.vbox{GUI.VID.label.vb,gap="0x0",margin="95x10", alignment = "ACENTER"};
        bgcolor = GUI.color.green, sunken = "YES"
    };
    vb_read = iup.frame
    {
        iup.vbox{GUI.VID.label.vb_read,gap="0x0",margin="5x2", alignment = "ACENTER"};
        bgcolor = GUI.color.green, sunken = "YES"
    };
    vb_en = iup.frame
    {
        iup.vbox{GUI.VID.label.vb_en,gap="0x0",margin="5x2", alignment = "ACENTER"};
        bgcolor = GUI.color.gray, sunken = "YES"
    };
    xsd_detection = iup.frame
    {
        iup.vbox{GUI.VID.label.xsd,gap="0x0",margin="5x3", alignment = "ACENTER"};
        bgcolor = GUI.color.gray, sunken = "YES"
    };
    sd_detection = iup.frame
    {
        iup.vbox{GUI.VID.label.sd,gap="0x0",margin="5x3", alignment = "ACENTER"};
        bgcolor = GUI.color.gray, sunken = "YES"
    };
    avd_set = iup.frame
    {
        iup.hbox{GUI.VID.toggle.avd, GUI.VID.button.avd_set; gap="0",margin="2x0", alignment = "ACENTER",};
        --bgcolor = GUI.color.yellow, 
        --size= "60x22"
    };
    xsd_set = iup.frame
    {
        iup.hbox{GUI.VID.toggle.psm_xsd, GUI.VID.button.xsd_set; gap="5",margin="5x4", alignment = "ACENTER",};
        --bgcolor = GUI.color.yellow,
        size= "60x22"
    };
    sd_set = iup.frame
    {
        iup.hbox{GUI.VID.toggle.psm_sd, GUI.VID.button.sd_set; gap="5",margin="5x4", alignment = "ACENTER",};
        --bgcolor = GUI.color.yellow,
        size= "60x22"
    };
    sw_switch = iup.frame
    {
        iup.hbox
        {
            GUI.VID.radio.sw_switch;
            iup.vbox
            {
                GUI.VID.button.ven_set;
                GUI.VID.button.ultra_sound;
                GUI.VID.button.third_part_sw;
                 gap="0",margin="5",alignment = "ALEFT"
            };
           gap="0",margin="0",alignment = "ACENTER"
        };
    };

    psm_xaa_set = iup.frame  --XAA
    {
        iup.hbox{GUI.VID.toggle.psm_xaa;gap="5",margin="140x6", alignment = "ACENTER",};--bgcolor = GUI.color.blue;};
        size="275x20",alignment = "ACENTER"
    };
    psm_command = iup.frame  --Command
    {
        iup.hbox
        {
            iup.hbox{GUI.VID.label.command_detect, GUI.VID.text.command_detect; gap="1",margin="0x0", alignment = "ACENTER",};--bgcolor = GUI.color.blue;};
            iup.hbox{GUI.VID.label.command_num, GUI.VID.text.command_num; gap="1",margin="0x0", alignment = "ACENTER",};--bgcolor = GUI.color.blue;};
            GUI.VID.button.command_read;
            gap="5",margin="10x5", alignment = "ACENTER",
        };
        size="275x20",alignment = "ACENTER"
    };
    nm_xaa_set = iup.frame
    {
        iup.vbox
        {
            iup.hbox{GUI.VID.toggle.nm_xaa, GUI.VID.button.xaa_set, GUI.VID.radio.unit_switch; gap="8",margin="5x0", alignment = "ACENTER",};
            iup.hbox{GUI.VID.label.spl, GUI.VID.text.spl; gap="7",margin="5x0", alignment = "ACENTER",};
            iup.hbox{GUI.VID.label.noise_type, GUI.VID.text.noise_type; gap="10",margin="5x0", alignment = "ACENTER",};
			iup.hbox{GUI.VID.label.xaa_floor, GUI.VID.text.xaa_floor; gap="42",margin="5x0", alignment = "ACENTER",};
            gap="1",margin="0x0", alignment = "ALEFT"
        };
        --bgcolor = GUI.color.blue;};
        gap="0x0",margin="10x0", size= "105x"
    };
    mic0_cfg = iup.hbox
    {
        GUI.VID.toggle.mic0_active,
        iup.hbox
        {
            GUI.VID.label.mic0_i2c_addr,
            GUI.VID.list.mic0_i2c_addr;
            alignment = "ACENTER"
        };
        GUI.VID.radio.mic0_type,
        gap="0",margin="5x5", alignment = "ACENTER"
    };
    mic1_cfg = iup.hbox
    {
        GUI.VID.toggle.mic1_active,
        iup.hbox
        {
            GUI.VID.label.mic1_i2c_addr,
            GUI.VID.list.mic1_i2c_addr;
            alignment = "ACENTER"
        };
        GUI.VID.radio.mic1_type,
        gap="0",margin="5x5", alignment = "ACENTER"
    };
    mic2_cfg = iup.hbox
    {
        GUI.VID.toggle.mic2_active,
        iup.hbox
        {
            GUI.VID.label.mic2_i2c_addr,
            GUI.VID.list.mic2_i2c_addr;
            alignment = "ACENTER"
        };
        GUI.VID.radio.mic2_type,
        gap="0",margin="5x5", alignment = "ACENTER"
    };
    mic3_cfg = iup.hbox
    {
        GUI.VID.toggle.mic3_active,
        iup.hbox
        {
            GUI.VID.label.mic3_i2c_addr,
            GUI.VID.list.mic3_i2c_addr;
            alignment = "ACENTER"
        };
        GUI.VID.radio.mic3_type,
        gap="0",margin="5x5", alignment = "ACENTER"
    };
    mic_rw = iup.hbox
    {
        GUI.VID.label.mic_reg_addr,
        GUI.VID.text.mic_reg_addr,
        GUI.VID.label.mic_reg_value,
        GUI.VID.text.mic_reg_value,
        GUI.VID.button.mic_read,
        GUI.VID.button.mic_write,
        gap="5",margin="5x5", alignment = "ACENTER"
    };
}

GUI.VID.tabs = 
{

    mic_set = iup.tabs
    {
        GUI.VID.module.mic0_cfg,
        GUI.VID.module.mic1_cfg,
        GUI.VID.module.mic2_cfg,
        GUI.VID.module.mic3_cfg,
        tabtitle0 = "Mic0", tabtitle1 = "Mic1",tabtitle2 = "Mic2", tabtitle3 = "Mic3",
    },
    
}

GUI.VID.local_frame = 
{
--[[
    code = iup.frame
    {
        iup.hbox
        {
            GUI.VID.text.ram_code, 
            GUI.VID.button.browse_code;
            gap="5",margin="0x5", alignment = "ACENTER"
        };
        title = "Select RAM Code Directory", size="80x", alignment = "ACENTER",
    };
         
    vec = iup.frame
    {
        iup.hbox
        {
            GUI.VID.text.init_vec, 
            GUI.VID.button.browse_init;
            gap="5",margin="0x5", alignment = "ACENTER"
        };
        title = "Select Parameter", size="80x", alignment = "ACENTER",
    };
]]
    eft = iup.frame
    {
        iup.vbox
        {
            iup.hbox
            {
                iup.hbox
                {
                    GUI.VID.label.eft_code,
                    GUI.VID.text.eft_code, 
                    GUI.VID.button.browse_eft,
                    gap="5",margin="5", alignment = "ACENTER"
                },
                GUI.VID.radio.file_type,
                gap="30",margin="0", alignment = "ACENTER"
            },
            iup.hbox
            {
                iup.hbox
                {
                    GUI.VID.label.eft_addr,
                    GUI.VID.text.eft_addr, 
                    gap="10",margin="5", alignment = "ACENTER"
                },
                GUI.VID.button.dl_eft;
                gap="150",margin="0", alignment = "ACENTER"
            },
            gap="5",margin="10x5", alignment = "ALEFT"
        };
        title = "Voice Model Control", size="80x", alignment = "ALEFT",
    };

    mic = iup.frame
    {
        iup.vbox
        {
            GUI.VID.module.mic_set;
            GUI.VID.label.separator1;
            iup.fill{size = "x16"};
            
            --iup.fill{size = "x5"};
                --GUI.VID.label.avd_space6,
            iup.hbox  --                                                                                                
            {                                                                                                           
                iup.fill{size = "7x"};                                                                                 
                iup.vbox                                                                                                
                {                                                                                                       
                    iup.hbox{GUI.VID.label.avd_no, gap="0x0",margin="7x0", alignment = "ACENTER"};                     
                    iup.hbox                                                                                            
                    {                                                                                                   
                        iup.label{image = GUI.VID.image.left2down},                                                     
                        iup.label{image = GUI.VID.image.shortaline};                                                    
                        gap="0x0",margin="3x0", alignment = "ACENTER"                                                   
                    },                                                                                                 
                    iup.hbox{iup.label{image = GUI.VID.image.vline}; gap="0x0",margin="3x0", alignment = "ALEFT"};     
                    gap="0x0",margin="0x0", alignment = "ALEFT"                                                        
                };                                                                                                     
                GUI.VID.module.avd_detection;                                                                          
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.hbox{GUI.VID.label.avd_yes, gap="0x0",margin="4x0", alignment = "ACENTER"};                   
                    iup.hbox                                                                                           
                    {                                                                                                  
                        iup.label{image = GUI.VID.image.shortaline},                                                   
                        iup.label{image = GUI.VID.image.right2down};                                                   
                        gap="0x0",margin="3x0", alignment = "ACENTER"                                                  
                    },                                                                                                 
                    iup.hbox{iup.label{image = GUI.VID.image.arrowdown}; gap="0x0",margin="3x0", alignment = "ARIGHT"};
                    gap="0x0",margin="0x0", alignment = "ARIGHT"                                                       
                };                                                                                                     
                iup.fill{size = "3"};                                                                                 
                gap="0x0",margin="0x0", alignment = "ACENTER"                                                          
            };                                                                                                         
            iup.hbox                                                                                                   
            {                                                                                                          
                iup.fill{size = "9x"};                                                                                
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.label{image = GUI.VID.image.vline},                                                            
                    iup.label{image = GUI.VID.image.vline},                                                            
                    iup.label{image = GUI.VID.image.vline},                                                            
                    iup.label{image = GUI.VID.image.vline},                                                            
                    gap="0x0",margin="0x0", alignment = "ALEFT"                                                        
                };                                                                                                     
                iup.fill{size = "12"};                                                                                
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.label{image = GUI.VID.image.arrowup},                                                          
                    GUI.VID.module.avd_set;                                                                            
                    iup.label{image = GUI.VID.image.arrowup},                                                          
                    gap="0x0",margin="0x0", alignment = "ACENTER"                                                      
                };                                                                                                     
                iup.fill{size = "11"};                                                                                 
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.label{image = GUI.VID.image.vline},                                                            
                    iup.label{image = GUI.VID.image.vline},                                                            
                    iup.label{image = GUI.VID.image.vline},                                                            
                    iup.label{image = GUI.VID.image.vline},                                                            
                    gap="0x0",margin="0x0", alignment = "ALEFT"                                                        
                };                                                                                                     
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.label{image = GUI.VID.image.arrowright},                                                       
                    iup.fill{size = "20"};                                                                             
                    gap="0x0",margin="0x0", alignment = "ACENTER"                                                      
                };                                                                                                     
                iup.fill{size = "1x"};                                                                               
                gap="0x0",margin="0x0", alignment = "ALEFT"                                                            
            };                                                                                                         
            iup.hbox                                                                                                   
            {                                                                                                          
                iup.fill{size = "6x"};                                                                                 
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.hbox{iup.label{image = GUI.VID.image.vline}; gap="0x0",margin="5x0", alignment = "ALEFT"};     
                    iup.hbox                                                                                           
                    {                                                                                                  
                        iup.label{image = GUI.VID.image.left2up},                                                      
                        iup.label{image = GUI.VID.image.arrowright};                                                   
                        gap="0x0",margin="5x0", alignment = "ACENTER"                                                  
                    },                                                                                                 
                    iup.fill{size = "5"};                                                                            
                    gap="0x0",margin="0x0", alignment = "ALEFT"                                                        
                };                                                                                                     
                iup.vbox                                                                                               
                {                                                                                                      
                    GUI.VID.module.sleep;                                                                              
                    --iup.fill{size = "1"};                                                                             
                    gap="0x0",margin="0x0", alignment = "ACENTER"                                                      
                };                                                                                                     
                iup.vbox                                                                                               
                {                                                                                                      
                    iup.hbox
                    {
                        iup.label{image = GUI.VID.image.vline};
                        iup.fill{size = "10"};
                        gap="0x0",margin="0x0", alignment = "ARIGHT"
                    };    
                    iup.hbox                                                                                           
                    {                                                                                                  
                        iup.label{image = GUI.VID.image.arrowleft},                                                    
                        iup.label{image = GUI.VID.image.shortaline};                                                   
                        iup.label{image = GUI.VID.image.shortaline};                                                   
                        gap="0x0",margin="0x0", alignment = "ACENTER"                                                  
                    },                                                                                                 
                    iup.hbox
                    {
                        iup.label{image = GUI.VID.image.vline};
                        iup.fill{size = "10"};
                        gap="0x0",margin="0x0", alignment = "ARIGHT"
                    }; 
                                                                                   
                    gap="0x0",margin="0x0", alignment = "ARIGHT"                                                       
                };                                                                                                     
                gap="0x0",margin="0x0", alignment = "ALEFT"                                                          
            };  
            iup.hbox                                                                                           
            {      
                iup.fill{size = "95"};
                iup.label{image = GUI.VID.image.down2right};                                                   
                iup.label{image = GUI.VID.image.arrowright}; 

                gap="0x0",margin="0x0", alignment = "ARIGHT"                                                    
            }; 
            
            iup.fill{size = "x10"};

            gap="0",margin="0x0", alignment = "ALEFT"
        };
        title = "", gap="0",margin="0x0", alignment = "ALEFT",
    };
    
    psm = iup.frame
    {
        iup.vbox
        {
            GUI.VID.module.psm_set;
            GUI.VID.label.separator2,
            
            iup.hbox
            {
                iup.fill{size = "15x"};
                GUI.VID.module.vb;
                iup.hbox
                {
                    --iup.label{image = GUI.VID.image.aline};
                    --iup.label{image = GUI.VID.image.shortaline};
                    iup.label{image = GUI.VID.image.arrowright},
                    gap="0x0",margin="5x0", alignment = "ACENTER"
               },
                gap="0x0",margin="0x4", alignment = "ACENTER"
            };
            
            GUI.VID.label.separator4,
            iup.fill{size = "x4"};
            ------------------
            iup.hbox
            {
                iup.hbox
                {
                    iup.label{image = GUI.VID.image.arrowright},
                    iup.fill{size = "5x"};
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                };
                GUI.VID.module.xsd_set,
                iup.hbox{iup.label{image = GUI.VID.image.arrowright}; gap="0x0",margin="5x0", alignment = "ACENTER"};

                --detection
                GUI.VID.module.xsd_detection;
                --arrow, yes
                iup.vbox
                {
                    GUI.VID.label.xsd_yes,
                    iup.hbox{iup.label{image = GUI.VID.image.shortaline};iup.label{image = GUI.VID.image.arrowright};gap="0x0",margin="0x0", alignment = "ACENTER"};
                    iup.fill{size = "x8"};
                    gap="0x0",margin="5x0", alignment = "ACENTER"
                };

                GUI.VID.module.sd_set;

                iup.hbox{iup.label{image = GUI.VID.image.arrowright}; gap="0x0",margin="5x0", alignment = "ACENTER"};

                GUI.VID.module.sd_detection;
                --arrow, yes
                iup.vbox
                {
                    GUI.VID.label.sd_yes,
                    --iup.hbox{iup.label{image = GUI.VID.image.shortaline};
                    iup.label{image = GUI.VID.image.arrowright};
                    --;gap="0x0",margin="0x0", alignment = "ACENTER"};
                    iup.fill{size = "x8"};
                    gap="0x0",margin="5x0", alignment = "ACENTER"
                };
                gap="0",margin="0x0", alignment = "ACENTER"
            };
            iup.fill{size = "3"};
            iup.hbox
            {
                iup.fill{size = "112x"};
                iup.vbox
                {
                    iup.label{image = GUI.VID.image.vline}, 
                    iup.label{image = GUI.VID.image.vline}, 
                    iup.label{image = GUI.VID.image.arrowdown}, 
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                };
                iup.hbox{GUI.VID.label.xsd_no;gap="0x0",margin="0x0", alignment = "ACENTER"};
                iup.fill{size = "130x"};
                iup.vbox
                {
                    iup.label{image = GUI.VID.image.vline},
                    iup.label{image = GUI.VID.image.vline}, 
                    iup.label{image = GUI.VID.image.arrowdown}, 
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                };
                iup.hbox{GUI.VID.label.sd_no;gap="0x0",margin="0x0", alignment = "ACENTER"};
                --iup.fill{size = "40x"};
                gap="0x0",margin="0x0", alignment = "ACENTER"
            };
            iup.hbox
            {
                iup.label{image = GUI.VID.image.arrowleft},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},
                iup.label{image = GUI.VID.image.aline},

                gap="0x0",margin="0x0", alignment = "ACENTER"
                
            };

            --------------------------------
            iup.hbox
            {
                iup.hbox
                {
                    iup.label{image = GUI.VID.image.arrowright},
                    iup.fill{size = "5x"};
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                };
                --GUI.VID.module.psm_xaa_set;
                GUI.VID.module.psm_command;

                gap="0x0",margin="0x3", alignment = "ACENTER"
            };
            --------------------------------
            iup.fill{size = "x1"};
            gap="0",margin="0x0", alignment = "ALEFT"

        };
        title = "", size="100x", gap="0",margin="0x0", alignment = "ALEFT",
    };
    oneshot = iup.frame
    {
    
        iup.vbox
        {
            iup.hbox
            {
                iup.fill{size = "10"};
                GUI.VID.toggle.one_shot_config, 
                GUI.VID.button.one_shot_set, 
                gap="0",margin="0x5", alignment = "ACENTER"
            };
            GUI.VID.label.separator6,
            iup.fill{size = "2"};
            iup.hbox
            {
                iup.hbox
                {
                    iup.label{image = GUI.VID.image.arrowright},
                    iup.fill{size = "4"};
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                },
                GUI.VID.module.vb_read,
                iup.hbox
                {
                    --iup.label{image = GUI.VID.image.shortaline};
                    iup.label{image = GUI.VID.image.arrowright};
                    gap="0x0",margin="2x0", alignment = "ACENTER"
                };
                gap="0x0",margin="0x0", alignment = "ACENTER"
            };

            iup.hbox
            {
                iup.fill{size = "38x"};
                iup.hbox
                {
                    iup.label{image = GUI.VID.image.arrowup_short},
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                },
                GUI.VID.label.vb_en_yes,
                --iup.fill{size = "25x"};
                gap="0x0",margin="0x0", alignment = "ACENTER"
            };
            
            iup.hbox
            {
                iup.hbox
                {
                    iup.label{image = GUI.VID.image.arrowright},
                    iup.fill{size = "4"};
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                },
                GUI.VID.module.vb_en,
                iup.vbox
                {
                    GUI.VID.label.vb_en_no,
                    iup.label{image = GUI.VID.image.arrowright};
                    iup.fill{size = "x8"};
                    gap="0x0",margin="2x0", alignment = "ACENTER"
                };
                gap="0x0",margin="0x0", alignment = "ACENTER"
            };
            iup.fill{size = "59"};
            gap="0",margin="0x0", alignment = "ALEFT"
        };
        title = "", size="50x", alignment = "ALEFT",
    };
    nm = iup.frame
    {
        iup.vbox
        {
            iup.hbox
            {
                iup.fill{size = "8"};
                GUI.VID.toggle.nm_config, 
                GUI.VID.button.nm_set, 
                gap="0",margin="0x5", alignment = "ACENTER"
            };
            GUI.VID.label.separator3,
            iup.fill{size = "7"};

            iup.hbox
            {
                iup.vbox
                {
                    iup.fill{size = "x3"};
                    iup.label{image = GUI.VID.image.arrowright};
                    iup.fill{size = "x23"};
                    iup.label{image = GUI.VID.image.arrowright};
                    iup.fill{size = "x4"};
                    gap="0x0",margin="0x0", alignment = "ACENTER"
                };
                GUI.VID.module.sw_switch,
                gap="0x0",margin="0x0", alignment = "ACENTER"
            };
            iup.fill{size = "7"};
            GUI.VID.label.separator5,
            iup.fill{size = "2"};
            GUI.VID.module.nm_xaa_set,
            iup.fill{size = "2"};
            gap="0",margin="0x0", alignment = "ACENTER"
        };
        title = "", alignment = "ALEFT",
    };
}

GUI.VID.dialog = 
{
--[[
    start_address = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.VID.label.header;
                GUI.VID.text.header;
                margin="5x5", gap="5"
            };

            iup.hbox
            {
                GUI.VID.label.net;
                GUI.VID.text.net;
                margin="5x5", gap="5"
            };
            iup.hbox
            {
                GUI.VID.label.gram;
                GUI.VID.text.gram;
                margin="5x5", gap="5"
            };

            iup.hbox
            {
                GUI.VID.button.start_addr_ok;
                GUI.VID.button.start_addr_cancel;
                margin="5x5", gap="5"
            };
            margin="10x5", gap="10",alignment = "ACENTER",
       };
       title="Setting Start Address", margin="10x10", gap="10"--,size="100x20"
    };
    ]]
    monitor = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.VID.label.im501;
                GUI.VID.text.current_im501;
                GUI.VID.label.unit_im501;
                margin="20x5", gap="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.im205;
                GUI.VID.text.current_im205;
                GUI.VID.label.unit_im205;
                margin="20x5", gap="5",alignment = "ACENTER"
            };
            margin="20x15", gap="10",alignment = "ACENTER"
        };
        title="Current Monitor", margin="20x20", gap="10"
    };
    dsp_control = iup.dialog
    {
        GUI.im501_rw.frame;
        title="DSP Control", margin="20x20", gap="10"
    };
    mic_control = iup.dialog
    {
        GUI.im205.frame;
        title="Smart MIC Control", margin="20x20", gap="10"
    };
    config = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
    
                GUI.VID.local_frame.mic;
                GUI.VID.local_frame.psm;
                GUI.VID.local_frame.oneshot;
                GUI.VID.local_frame.nm;
                margin="5x5", gap="0",alignment = "ALEFT"
            };

            iup.hbox
            {
                GUI.VID.local_frame.bypass;
                margin="5x5", gap="0",alignment = "ALEFT"
            };
            iup.hbox
            {
                iup.frame
                {
                    GUI.VID.radio.mode_switch,
                    title = "Mode Switch", margin="5x5", gap="0",alignment = "ACENTER"
                };
                --GUI.im501_rw.frame;
                iup.vbox
                {
                    iup.frame
                    {
                        GUI.VID.label.dsp_info, expand = "YES",
                        title = "DSP information", margin="5x5", gap="0",alignment = "ALEFT"
                    };
                    
                    iup.hbox
                    {
                        GUI.VID.toggle.spi_record;
                        GUI.VID.button.dsp_control;

                        --GUI.VID.button.mic_control;
                        --GUI.im205.frame;
                        GUI.VID.button.monitor_current;
                        GUI.VID.button.update_set;
                        margin="5x5", gap="30",alignment = "ACENTER"
                    };
                    margin="5x5", gap="10",alignment = "ALEFT"
                };
                margin="5x5", gap="30",alignment = "ACENTER"
            };
        };
        title="Lower Power Always On Acoustics Awareness", margin="10x10", gap="10"
    };
    mic_set = iup.dialog
    {
        iup.vbox
        {
        GUI.VID.tabs.mic_set;
        --[[
        GUI.VID.module.mic0_cfg;
        GUI.VID.module.mic1_cfg;
        GUI.VID.module.mic2_cfg;
        GUI.VID.module.mic3_cfg;
        ]]
        GUI.VID.radio.mic_access;
        GUI.VID.module.mic_rw;
        };
        title="Smart MIC Setting", margin="20x20", gap="10"
    };
    avd_set = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.VID.label.xaa_timer,
                GUI.VID.list.xaa_timer,
                margin="5x5", gap="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.avd_clear_timer,
                GUI.VID.list.avd_clear_timer,
                margin="5x5", gap="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.avd_reg6,
                GUI.VID.text.avd_reg6,
                margin="5x5", gap="5",alignment = "ACENTER"
            };
            margin="15x5", gap="5",alignment = "ALEFT"
        };
        title="AVD Setting", margin="20x20", gap="10"
    };
    xsd_set = iup.dialog
    {
        iup.hbox
        {
            GUI.VID.list.xsd,
            GUI.VID.list.psm_xsd_adc;
        };
        title="xSD Setting", margin="20x20", gap="20"
    };
    sd_set = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
            --[[
                iup.hbox{
                    GUI.VID.label.sd_kw,
                    GUI.VID.list.sd_kw,
                    margin="5x5", gap="5",alignment = "ACENTER"
                };
                ]]
                iup.hbox{
                    GUI.VID.label.sd_function,
                    GUI.VID.list.sd_function,
                    margin="5x5", gap="5",alignment = "ACENTER"
                };
                iup.hbox
                {
                    GUI.VID.label.psm_sd_adc,
                    GUI.VID.list.psm_sd_adc;
                    margin="5x5", gap="5",alignment = "ACENTER"
                };
            };
            iup.hbox{GUI.VID.local_frame.eft; margin="5x5",alignment = "ACENTER"};
            margin="5x5", gap="5",alignment = "ALEFT"
        };
        title="SD Setting", margin="20x20", gap="0"
    };
    one_shot_set = iup.dialog
    {
        iup.vbox
        {
            iup.hbox{GUI.VID.toggle.spi_read; gap="0",margin="0x0", alignment = "ACENTER",};  
            iup.hbox
            {
                GUI.VID.label.spi_read_interval;
                GUI.VID.text.spi_read_interval;
                gap="5x5",margin="0x0",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.spi_read_cycle;
                GUI.VID.text.spi_read_cycle;
                gap="5x5",margin="0x0",alignment = "ACENTER"
            };
            --[[
            iup.hbox
            {
                GUI.VID.label.after_spir_mode;
                GUI.VID.radio.after_spir_mode;
                gap="5x5",margin="0x0",alignment = "ACENTER"
            };]]
        };
        title="One Shot Work Mode Setting", margin="20x20", gap="10"
    };
    xaa_set = iup.dialog
    {
        
        title="xAA Setting", margin="20x20", gap="10"
    };
    ven_set = iup.dialog
    {
        iup.vbox
        {

        };
        title="VEN Setting", margin="20x20", gap="10"
    };
    nm_set = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.VID.label.back_to_psm_delay,
                GUI.VID.text.back_to_psm_delay,
                gap="0x0",margin="0x5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.nm_sr,
                GUI.VID.list.nm_sr,
                gap="76x0",margin="0x0",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.pdm0o,
                GUI.VID.list.pdm0o,
                gap="30x0",margin="0x5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.VID.label.pdm1o,
                GUI.VID.list.pdm1o,
                gap="30x0",margin="0x0",alignment = "ACENTER"
            };
            gap="0x20",margin="20x10",alignment = "ACENTER"
        };
        title="Normal Mode Setting"
    };
    psm_set = iup.dialog
    {
        iup.hbox
        {
            GUI.VID.label.wakeup_time;
            GUI.VID.text.wakeup_time;
            gap="5x5",margin="50x20",alignment = "ACENTER"
        };
        title="Power Save Mode Setting", margin="20x20", gap="10"
    };
}

GUI.VID.frame = iup.frame
{
    iup.hbox
    {
        GUI.VID.module.JTAG,
        GUI.VID.button.reset,
        iup.vbox
        {
            iup.hbox
            { 
                GUI.VID.label.code,
                GUI.VID.button.dl_mode,
                GUI.VID.text.ram_code,
                GUI.VID.button.browse_code;
                GUI.VID.button.dl,
                gap="1x5",margin="0x0",alignment = "ACENTER"
            },
            iup.hbox
            { 
                GUI.VID.label.vec,
                GUI.VID.text.vec, 
                GUI.VID.button.browse_vec;
                GUI.VID.button.dl_vec,
                gap="1x5",margin="0x0",alignment = "ACENTER"
            },
            gap="5x5",margin="0x0",alignment = "ACENTER"
        };
        GUI.VID.button.vid,
        GUI.VID.button.run,
            --GUI.VID.button.vb;
            --GUI.VID.button.monitor_current;
            --gap="40",margin="20",alignment = "ACENTER"
        gap="10x5",margin="10x5",alignment = "ACENTER"

--[[        
        iup.hbox
        {
            GUI.VID.radio.mode_switch;
            iup.hbox
            {
                GUI.VID.label.vb;
                GUI.VID.button.vb_start;
                GUI.VID.button.vb_stop;
                gap="10",margin="0",alignment = "ACENTER"
            };
            gap="30",margin="10",alignment = "ACENTER"
        };
        iup.hbox
        {
            GUI.VID.local_frame.eft,
            gap="10",margin="10",alignment = "ACENTER"
        };
]]
    };
    title = "VID Initialize", --size="400x",
    gap="10",margin="0x10",alignment = "ACENTER",
}

----
local function gui_to_kernal()
    GUI.kernel.code_vec[1].code = dir_remove(GUI.VID.text.ram_code.value)
    GUI.kernel.code_vec[1].vec = dir_remove(GUI.VID.text.init_vec.value)
end

NM_PSM = 1
--0: PSM, 1:NM, 2:PSM-ONESHOT, 3:NM-ONESHOT, 4:BYPASS
GUI.VID.toggle.psm.action = function()
    if NM_PSM == 0 then return end
    --GUI.VID.time.command_timer.run = "NO"
    if NM_PSM == 2 or NM_PSM == 3 then
        GUI.VID.time.command_timer.run = "NO"
        --[[
        GUI.VID.button.vb_start.active = "yes"
        --GUI.VID.button.vb_stop.active = "no"
        GUI.waveview.button.start_p_r.active = "yes" 
        GUI.waveview.button.stop_p_r.active = "yes"
        if PLAY_STATUS == 1 then stop_play_record() end
        wait(1)
        JSON_FILE = CONFIG_PATH.."iM501\\Simple_tuner.json"
        os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
        GUI.kernel = load_jsonfile1(TEMP_JSON)
        update_system_setting("ON", "OFF")
        --iM501.write_message(iM501.message.start_vb_transfer, 0, DL_PATH)
        ]]
    end
    if GUI.VID.toggle.psm.value == "ON" then
        
        --GUI.VID.toggle.nm.value = "OFF"
        --UIF.set_vec_config(3,DL_PATH,DELAY_POWER_DOWN,51,iM501.GPIO.IRQ,1, PDMCLK_TURNOFF_EN, DL_PATH)
        if NM_PSM == 1 then
            iM501.enter_PSM(DL_PATH)
        end

        if NM_PSM == 1 or NM_PSM == 3 then
            GUI.VID.active("no")
        end
        
        NM_PSM = 0
        PDMCLK_ONOFF = 0
        --[[
        ----set I2C/SPI speed to high speed
        --print(temp_spi_speed, SPI_SPEED, temp_i2c_speed, I2C_SPEED)
        temp_i2c_speed = GUI.com_path.text.i2c_speed.value
        temp_spi_speed = GUI.com_path.text.spi_speed.value
        I2C_SPEED = 20
        GUI.com_path.text.i2c_speed.value = 20
        UIF.set_interface(UIF.type.I2C, I2C_SPEED)
        SPI_SPEED = 1000
        GUI.com_path.text.spi_speed.value = 1 
        UIF.set_interface(UIF.type.SPI, SPI_SPEED, SPI_MODE)
        ]]
        ----enable iM501 read and write
        --GUI.im501_rw.active("no")
        GUI.VID.button.update_set.action()
    end
    --GUI.VID.time.command_timer.run = "YES"
end

GUI.VID.toggle.nm.action = function()
    if NM_PSM == 1 then return end
   -- GUI.VID.time.command_timer.run = "NO"
    if NM_PSM == 2 or NM_PSM == 3 then
        GUI.VID.time.command_timer.run = "NO"
    --[[
        GUI.VID.button.vb_start.active = "yes"
        --GUI.VID.button.vb_stop.active = "no"
        GUI.waveview.button.start_p_r.active = "yes" 
        GUI.waveview.button.stop_p_r.active = "yes"
        if PLAY_STATUS == 1 then stop_play_record() end
        wait(1)
        JSON_FILE = CONFIG_PATH.."iM501\\Simple_tuner.json"
        os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
        GUI.kernel = load_jsonfile1(TEMP_JSON)
        update_system_setting("ON", "OFF")
        --iM501.write_message(iM501.message.start_vb_transfer, 0, DL_PATH)
        ]]
    end
    if GUI.VID.toggle.nm.value == "ON" then
        --GUI.VID.toggle.psm.value = "OFF"
        --UIF.set_vec_config(3,DL_PATH,DELAY_POWER_DOWN,51,iM501.GPIO.IRQ,0, PDMCLK_TURNOFF_EN, DL_PATH)
        printc("Back to normal mode.")
        --if NM_PSM == 0 then 
            iM501.backto_NM(DL_PATH)
        --end
        NM_PSM = 1
        PDMCLK_ONOFF = 1
        ----set I2C/SPI speed to high speed
        --[[
        --print(temp_spi_speed, SPI_SPEED, temp_i2c_speed, I2C_SPEED)
        I2C_SPEED = temp_i2c_speed
        GUI.com_path.text.i2c_speed.value = temp_i2c_speed
        UIF.set_interface(UIF.type.I2C, I2C_SPEED)
        SPI_SPEED = temp_spi_speed*1000
        GUI.com_path.text.spi_speed.value = temp_spi_speed 
        UIF.set_interface(UIF.type.SPI, SPI_SPEED, SPI_MODE)
]]
        ----enable iM501 read and write
        GUI.im501_rw.active("yes")

        if DSP_RUN == 1 then ----if dsp is running, enable VR function setting
            GUI.VID.active("yes")
            GUI.VID.toggle.psm.active = 'YES'
            GUI.VID.toggle.one_shot.active = 'YES'
        end
        GUI.VID.button.update_set.action()
    end
    --GUI.VID.time.command_timer.run = "YES"
end

GUI.VID.toggle.one_shot.action = function()
    if NM_PSM == 2 or NM_PSM == 3 then return end
    --GUI.VID.time.command_timer.run = "NO"
    if NM_PSM == 0 or NM_PSM == 1 then
    --[[
        GUI.VID.button.vb_start.active = "yes"
        GUI.VID.button.vb_stop.active = "yes"
        GUI.waveview.button.start_p_r.active = "no" 
        GUI.waveview.button.stop_p_r.active = "no"
        if PLAY_STATUS == 1 then stop_play_record() end
        JSON_FILE = CONFIG_PATH.."iM501\\Voice_buffer.json"
        os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
        GUI.kernel = load_jsonfile1(TEMP_JSON)
        update_system_setting("ON", "OFF")
        ]]
        --[[
        if NM_PSM == 0 then
            UIF.set_vec_config(3,DL_PATH,DELAY_POWER_DOWN,51,iM501.GPIO.IRQ,0, PDMCLK_TURNOFF_EN, DL_PATH)
            PDMCLK_ONOFF = 1
        end
        ]]
        if NM_PSM == 1 then
            --iM501.enter_PSM(DL_PATH)  --VOS2.0 support one shot in NM
            --PDMCLK_ONOFF = 0
        end
    end

    if GUI.VID.toggle.one_shot.value == "ON" then
        NM_PSM = NM_PSM + 2
        --iM501.write_message(iM501.message.start_vb_transfer, 1, DL_PATH)
        GUI.im501_rw.active("yes")
        iM501.enter_Oneshot()

        if MODE_AFTER_SPIR == 1 then
            GUI.VID.time.oneshot_timer.time = T_IRQ2CLKON + T_SPIR2SPIR*C_SPIR + T_NM2PSM + 1
        else
            GUI.VID.time.oneshot_timer.time = T_IRQ2CLKON + T_SPIR2SPIR*C_SPIR
        end
        --print(GUI.VID.time.oneshot_timer.time)
        GUI.VID.time.oneshot_timer.run = "YES"
    end
    
    --GUI.VID.time.command_timer.run = "YES"
end

GUI.VID.toggle.bypass.action = function()
    if NM_PSM == 4 then return end
    if NM_PSM == 0 or NM_PSM == 2 or NM_PSM == 3 then
        GUI.VID.time.command_timer.run = "NO"
        iM501.backto_NM(DL_PATH)
        PDMCLK_ONOFF = 1
    end

    if GUI.VID.toggle.bypass.value == "ON" then
        NM_PSM = 4
        GUI.VID.toggle.psm.active = 'NO'
        GUI.VID.toggle.one_shot.active = 'NO'
        GUI.VID.active("no", true)
        iM501.enter_bypass(DL_PATH)
    end
end

local oldChannelNum = 1
local oldTempJson = TEMP_JSON
function GUI.VID.VB_channel_update(channelNum)
    if not channelNum then
        _, channelNum = iM501.read_dram(iM501.message_return.vb_channel, 2)
        channelNum = channelNum == "nil" and 1 or channelNum
    end

    VB_CHANNEL = channelNum
    if oldChannelNum == channelNum then
        print("Same setting.")
        return channelNum
    end

    oldChannelNum = channelNum
    if AUDIO_BUS_TYPE == 1 then
        local playStatus = PLAY_STATUS
        if PLAY_STATUS == 1 then
            GUI.uif_setting.button.start_p_r.action()
        end

        if channelNum ~= 1 then
            fileName = tostring(channelNum) .. 'channel.json'
            JSON_FILE = CONFIG_PATH.."iM501\\VoiceBuffer\\"..fileName
            os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
            GUI.kernel = load_jsonfile1(TEMP_JSON)
        else
            GUI.kernel = load_jsonfile1(oldTempJson)
        end
        update_system_setting("ON", "OFF", "NO")
    end
    return channelNum
end

local function getSPIFrameSize()
    _, frameSize = iM501.read_dram(iM501.message_return.spi_channel, 2)
    FRAMESIZE = frameSize == "nil" and 160 or frameSize
end

SPI_EN = false
SPI_RUN = false
function GUI.VID.toggle.spi_record:action()

    if GUI.VID.toggle.spi_record.value == "ON" then
        GUI.VID.toggle.nm.value = "ON"
        GUI.VID.toggle.nm.action()
        --GUI.VID.radio.mode_switch.active = "no"
        mode_switch_active('NO')

        local playStatus = PLAY_STATUS
        if PLAY_STATUS == 1 then
            GUI.uif_setting.button.start_p_r.action()
        end

        SPI_EN = true
        collectgarbage("collect")
        if FRAMESIZE == 160 then
            SPI_CHANNEL = 8
            if AUDIO_BUS_TYPE == 1 then
                print(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_8channel_TEST.json")
                GUI.kernel = load_jsonfile1(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_8channel_TEST.json")
            else
                print(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_8channel_TEST_I2S.json")
                GUI.kernel = load_jsonfile1(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_8channel_TEST_I2S.json")
            end
            SPI_REC_CH_MASK=0x7F
        else
            SPI_CHANNEL = 6
            if AUDIO_BUS_TYPE == 1 then
                print(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_6channel_TEST.json")
                GUI.kernel = load_jsonfile1(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_6channel_TEST.json")
            else
                print(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_6channel_TEST_I2S.json")
                GUI.kernel = load_jsonfile1(CONFIG_PATH..DSP_NAME.."\\RfDesign\\SPI\\SPI_16K_6channel_TEST_I2S.json")
            end
            SPI_REC_CH_MASK=0x1F
        end
        -- update_system_setting("OFF")
        update_system_setting("ON", "ON", "NO")

        collectgarbage("collect")
        
        if playStatus == 1 then
            GUI.uif_setting.button.start_p_r.action()
        end
    else
        local playStatus = PLAY_STATUS
        if PLAY_STATUS == 1 then
            GUI.uif_setting.button.start_p_r.action()
        end
        SPI_EN = false
        GUI.kernel = load_jsonfile1(TEMP_JSON)
        update_system_setting("ON", "OFF", "NO")
        collectgarbage("collect")
        if playStatus == 1 then
            GUI.uif_setting.button.start_p_r.action()
        end
        --GUI.VID.radio.mode_switch.active = "yes"
        mode_switch_active('YES')
    end

end

function GUI.VID.button.vb_stop:action()
    stop_play_record()
    --GUI.VID.radio.mode_switch.active = "yes"
    mode_switch_active('YES')
    GUI.VID.button.vb_start.active = "yes"
    GUI.im501_rw.active("yes")
    GUI.im501_vr_setting.active("yes")
    if NM_PSM == 2 then
        GUI.VID.toggle.psm.action()
    elseif NM_PSM == 3 then
        GUI.VID.toggle.nm.action()
    end
end

JTAG_info =
[==[
Please Follow the steps below:

Disconnect SPI bus by removing SPI jumpers at:
		J43 SPI_SS
		J44 SPI_MOSI 
		J51 SPI_MISO
		J55 SPI_CLK
		
Disconnect jumper Between J6 Pin3 and Pin 4

KEEP I2C jumpers connected at:
		J11 UIF_S_SCL
		J16 UIF_S_SDA
		
Connect ICE connector to EVM J56
]==]
GUI.VID.toggle.JTAG.action = function()
    if GUI.VID.toggle.JTAG.value == "ON" then
        message("Warning", JTAG_info)
        if DL_PATH == 2 then
            GUI.com_path.radio.bus.value = GUI.com_path.toggle.i2c
            GUI.com_path.toggle.i2c.action()
            GUI.system_server.button.update_com_path.action()
        end
        
        local state = iM501.JTAG()
        if state == false then message("Error", "Switch unsuccessful, Please try again.") return end
        GUI.VID.radio.mode_switch.active = "yes"
        mode_switch_active('YES')
        --GUI.im501_rw.active("yes")
        GUI.VID.active("yes")
    end
end

GUI.VID.button.vid.action = function()
    --update_vid_gui()    
    update_vid_gui()
    GUI.VID.dialog.config:showxy(iup.CENTER, iup.UP)
end

function update_vid_gui()
--[[
    GUI.VID.toggle.mic.value = iM501.check_mic_setting() == 1 and "ON" or "OFF"
    print( GUI.VID.toggle.mic.value)
    GUI.VID.button.mic_set.active = GUI.VID.toggle.mic.value
    GUI.VID.toggle.avd.value = iM501.check_avd_en() == 1 and "ON" or "OFF"
    GUI.VID.button.avd_set.active = GUI.VID.toggle.avd.value
    GUI.VID.toggle.psm_xsd.value = iM501.check_psm_xsd() == 0 and "OFF" or "ON"
    GUI.VID.button.xsd_set.active = GUI.VID.toggle.psm_xsd.value
    GUI.VID.toggle.psm_sd.value = iM501.check_psm_sd() == 0 and "OFF" or "ON"
    --GUI.VID.button.sd_set.active = GUI.VID.toggle.psm_sd.value
]]

    GUI.VID.label.dsp_info.title = "Voice buffer channel: " ..tostring(VB_CHANNEL) .. ", SPI frame size: ".. tostring(FRAMESIZE)
end

function GUI.VID.button.reset:action()
    if PLAY_STATUS == 1 then
        GUI.uif_setting.button.start_p_r.action()
    end
    GUI.VID.time.command_timer.run = "NO"
    GUI.VID.time.xaa_timer.run = "NO"
    GUI.VID.time.command_timer.run = "NO"
    DSP_RUN = 0
    GUI.VID.radio.mode_switch.value = GUI.VID.toggle.nm
    GUI.VID.toggle.nm.action()
    UIF.clear_flag()
    iM501.reset(3)
    --[[
    printc("Reset iM501.")
    if DL_PATH == 2 and SPI_MODE ~= 0 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 8+SPI_MODE, 1, 1)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, SPI_MODE, 1, 1)
    elseif DL_PATH == 1 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 0x4)
    end
    ]]
--    iM501.write_reg(DSP_ONOFF, 0x5, 1, DL_PATH)
    --GUI.VID.radio.mode_switch.active = "no"
    mode_switch_active('NO')
    GUI.im501_rw.active("yes")
    GUI.VID.active("no")
    GUI.VID.button.run.fgcolor = GUI.color.black
    --GUI.VID.button.vid.active = "no"
    GUI.VID.button.run.active = "no"
    GUI.VID.button.vb_start.active = "no"
    GUI.VID.button.vb_stop.active = "no"
    DL_STATE = 0
end

local defaultRandomize = import_gui_default("downloadRandomize") or false
local downloadSetting =
{
    mode= tonumber(import_gui_default("downloadMode")) or 1 , --1: full download; 2:fast download
    type= tonumber(import_gui_default("downloadType")) or 1 , --1: load IRAM and DRAM; 2: load DRAM only
    randomize= defaultRandomize == "" and false or defaultRandomize,
}
function GUI.VID.button.dl_mode:action()
    local fullDL = iup.toggle{title="Full Download", value= "ON"}
    local fastDL = iup.toggle{title="Fast Download", value= "OFF"}
    local modeSwtich = iup.radio{iup.hbox{fullDL, fastDL}, value = fullDL, gap="20x5"}
    local randomizedSize = iup.toggle{title="Randomized DRAM Size", value="OFF"}
    local downloadBoth = iup.toggle{title="Download IRAM and DRAM", value="ON"}
    local downloadDRAM = iup.toggle{title="Download DRAM only", value="ON"}
    local typeSwitch = iup.radio{iup.hbox{downloadBoth, downloadDRAM}, value = downloadBoth, gap="20x5",margin="0x0"}
    local optionFrame = iup.frame
    {
        iup.vbox{
            typeSwitch,
            randomizedSize;
            gap="10x5", alignment="ALEFT"
        };
        title="Fast Download Option"
    }
    optionFrame.active_update = function(mode)
        mode = mode or 1
        local active = mode == 1 and "NO" or "YES"
        randomizedSize.active=active
        downloadBoth.active=active
        downloadDRAM.active=active
    end
    local function ui_update(param)
        fullDL.value = param.mode==1 and "ON" or "OFF"
        fastDL.value = param.mode==2 and "ON" or "OFF"
        optionFrame.active_update(param.mode)
        downloadBoth.value=(param.type==1) and "ON" or "OFF"
        downloadDRAM.value=(param.type==2) and "ON" or "OFF"
        randomizedSize.active = (fastDL.value=="ON") and "YES" or "NO"
        randomizedSize.value = param.randomize and "ON" or "OFF"
    end

    ui_update(downloadSetting)

    fullDL.action = function()
        downloadSetting.mode=1
        export_gui_default("downloadMode", 1)
        optionFrame.active_update(downloadSetting.mode)
    end
    fastDL.action = function()
        downloadSetting.mode=2
        export_gui_default("downloadMode", 2)
        optionFrame.active_update(downloadSetting.mode)
    end
    downloadBoth.action = function(ih, state)
        downloadSetting.type = state==1 and 1 or 2
        export_gui_default("downloadType", downloadSetting.type)
        print("a")
    end
    downloadDRAM.action = function(ih, state)
        downloadSetting.type = state==0 and 1 or 2
        export_gui_default("downloadType", downloadSetting.type)
        --randomizedSize.active = state==1 and "YES" or "NO"
        print("b")
    end
    randomizedSize.action = function(ih, state)
        downloadSetting.randomize = state==1
        export_gui_default("downloadRandomize", downloadSetting.randomize)
    end
    local downloadModeDlg = iup.dialog
    {
        iup.vbox
        {
            modeSwtich, optionFrame
        },
        title="Download Setting", gap="10x10",margin="10x10"
    }

    downloadModeDlg:popup()
end

local function getRandomizeTab()
    local binTab = {00,00,0xfc,0xf, 01,0, 00,0xc0,3,0}
    local length = 0x3c000
    for i=1, length do
        table.insert(binTab, math.random(0,0xff))
    end
    printc(#binTab)
    return binTab
end
function GUI.VID.button.dl:action()
    if PDMCLK_ONOFF == 0 or DSP_RUN == 1 then
        message("Error", "Please reset first.")
        return
    end
    if PLAY_STATUS == 1 then
        GUI.uif_setting.button.start_p_r.action()
    end
    GUI.VID.button.dl.title = "Loading"
    UIF.load_vec(1, CODE_VEC_PATH..iM501.vec.power_down_i2c, iM501.vec.power_down_i2c, 1)
    UIF.load_vec(2, CODE_VEC_PATH..iM501.vec.power_down_spi, iM501.vec.power_down_spi, 2)
    UIF.load_vec(3, CODE_VEC_PATH..iM501.vec.power_up, iM501.vec.power_up, 2)
    printc("Download FW to iM501.")
    iM501.load_vec(CODE_VEC_PATH..iM501.vec.init)
    wait(0.2)
    local str = "."
    if downloadSetting.mode == 1 then
        for i = 1, #iM501.code do
            GUI.VID.button.dl.title = GUI.VID.button.dl.title..str
            iM501.load_code(iM501.code[i][1], CODE_VEC_PATH..iM501.code[i][2], iM501.code[i][3])
        end
    elseif downloadSetting.mode == 2 then
        printc("Fast I2C mode download...")
        local interface = DL_PATH
        --iM501.switch_I2C_SPI(1)
        local codeName = iM501.fast_code[1]
        GUI.VID.button.dl.title = GUI.VID.button.dl.title..str
        if downloadSetting.type == 1 then
            printc("Load IRAM and DRAM.")
            iM501.fast_load_code(CODE_VEC_PATH..codeName, nil, DL_PATH)
        end
        GUI.VID.button.dl.title = GUI.VID.button.dl.title..str
        if downloadSetting.randomize then
            printc("Load randomized data to DRAM before download real DARM data.")
            iM501.fast_load_code("", getRandomizeTab(), DL_PATH)
        end
        GUI.VID.button.dl.title = GUI.VID.button.dl.title..str
        codeName = iM501.fast_code[2]
        iM501.fast_load_code(CODE_VEC_PATH..codeName, nil, DL_PATH)
       -- iM501.switch_I2C_SPI(DL_PATH)
    end
    --iup.Alarm("Download", "Download Successfully", "OK")
    GUI.VID.button.dl.title = "Load"
    DL_STATE = 1
    iM501.load_vec(CODE_VEC_PATH..iM501.vec.run)
    GUI.VID.button.sd_set.active = "yes"
    GUI.VID.button.run.active = "yes"
    GUI.VID.button.vid.active = "yes"
    --GUI.VID.active("yes")
end

function GUI.VID.button.run:action()
    if PDMCLK_ONOFF == 0 then
        message("Error", "Please turn on PDM clock.")
        return
    end
    --print(DL_PATH)
    DSP_ONOFF =  DL_PATH == 1 and iM501.I2C_REG.dsp_onoff or iM501.SPI_REG.dsp_onoff
    INTERRUPT_DSP = DL_PATH == 1 and iM501.I2C_REG.interrupt_dsp or iM501.SPI_REG.interrupt_dsp
    iM501.write_reg(DSP_ONOFF, 0x4, 1, DL_PATH)
    wait(0.2)

   -- _, im501_system_config = iM501.read_dram(iM501.vec.system_config_check)  -- read back system config register, it's default setting.
    --im501_system_config = bit:_or(im501_system_config, 0xfff88cf0)
  --  _, im501_keyword = iM501.read_dram(iM501.vec.keyword_check) 
   -- _, im501_psap = iM501.read_dram(iM501.vec.psap)
   -- update_vr_set()
    --io.read()
  
    GUI.VID.button.run.fgcolor = GUI.color.green
    
    GUI.VID.button.vb_start.active = "yes"
    GUI.VID.button.vb_stop.active = "yes"
    --update_vr_set()
    DSP_RUN = 1
    printc("DSP is running...")
    --GUI.VID.time.command_timer.run = "YES"
    if SAMPLE_RATE ~= 96000 then


        --GUI.VID.radio.mode_switch.active = "yes"
        mode_switch_active('YES')
        --GUI.im501_rw.active("yes")
        GUI.VID.active("yes")
        GUI.VID.time.xaa_timer.run = "YES"
        --GUI.VID.time.command_timer.run = "YES"
        GUI.VID.button.update_set.action()
        GUI.VID.VB_channel_update()
        getSPIFrameSize()
        update_vid_gui()
        --iM501.backto_NM()
    end
end


function GUI.VID.button.vb_start:action()
    local dsp_status = NM_PSM
    if dsp_status == 0 then
        UIF.set_vec_config(3,DL_PATH,DELAY_POWER_DOWN,51,iM501.GPIO.IRQ,0, PDMCLK_TURNOFF_EN, DL_PATH)
        PDMCLK_ONOFF = 1
    end
    
    if NM_PSM == 0 or NM_PSM == 1 then
        --GUI.VID.radio.mode_switch.active = "no"
        mode_switch_active('NO')
        GUI.VID.button.vb_start.active = "yes"
        GUI.VID.button.vb_stop.active = "yes"
        GUI.waveview.button.start_p_r.active = "no" 
        GUI.waveview.button.stop_p_r.active = "no"
        if PLAY_STATUS == 1 then stop_play_record() end
        wait(1)
        JSON_FILE = CONFIG_PATH.."iM501\\Voice_buffer.json"
        os.execute("copy "..JSON_FILE.." "..TEMP_JSON)
        GUI.kernel = load_jsonfile1(TEMP_JSON)
        update_system_setting("ON", "OFF", "NO")
        NM_PSM = NM_PSM + 2
    end

    --iM501.write_message(iM501.message.start_vb_transfer, 1, DL_PATH)

    GUI.im501_rw.active("no")
    GUI.im501_vr_setting.active("no")
    GUI.VID.button.vb_start.active = "no"
    if NM_PSM == 2 then
        iM501.enter_PSM(DL_PATH)
    end
    wait(0.2)
    start_play_record(0)
    wait(0.2)
    iM501.rec_voice_buffer(iM501.GPIO.IRQ)
end

function GUI.VID.button.vb_stop:action()
    stop_play_record()
    --GUI.VID.radio.mode_switch.active = "yes"
    mode_switch_active('YES')
    GUI.VID.button.vb_start.active = "yes"
    GUI.im501_rw.active("yes")
    GUI.im501_vr_setting.active("yes")
    if NM_PSM == 2 then
        GUI.VID.toggle.psm.action()
    elseif NM_PSM == 3 then
        GUI.VID.toggle.nm.action()
    end
end

function GUI.VID.button.browse_code:action()
    local fd=iup.filedlg{dialogtype="DIR", title="Select Code Directory", 
                         nochangedir="YES", directory=CODE_VEC_PATH,value="",
                         filter="", filterinfo="File Directory", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status
 
    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
        GUI.VID.text.ram_code.value = fd.value
        local match_string = "*.bin"
        local bin_file = misc.find_names(match_string, fd.value, 0)
        --print(unpack(bin_file))
        if bin_file == nil or #bin_file == 0 then 
            message("ERROR", "This directory do not include any bin file.") 
        else
        	local file_check = 0
            for i = 1, #bin_file do
                if string.find(bin_file[i], "iram") ~= nil then  file_check = file_check + 1 break end
            end
            for i = 1, #bin_file do
                if string.find(bin_file[i], "dram0") ~= nil then  file_check = file_check + 1 break end
            end
            for i = 1, #bin_file do
                if string.find(bin_file[i], "dram1") ~= nil then  file_check = file_check + 1 break end
            end
            if file_check ~= 3 then
                message("ERROR", "This directory do not include right bin file, please double check!")
            end
        end
        
        CODE_VEC_PATH = fd.value.."\\"
        
        local str = fd.value
        local index = 1
        repeat
            index = string.find(str, "\\")
            str = string.sub(str, (index or 0)+1, -1)
        until index == nil
        printc("Code version"..str)
        SW_VERSION = str

        GUI.VID.text.eft_code.value = fd.value
        EFT_PATH = fd.value.."\\"
        GUI.VID.text.vec.value = fd.value.."\\"..iM501.vec.init
        --print(CODE_VEC_PATH)
    end
    fd:destroy()
end

function GUI.VID.button.browse_vec:action()
    local fd=iup.filedlg{dialogtype="OPEN", title="Load init parameter", 
                         nochangedir="YES", directory=CODE_VEC_PATH,value=iM501.vec.init,
                         filter="*.vec", filterinfo="parameter file", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status
    CODE_VEC_PATH = fd.directory
    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
        if string.find(fd.value, "init") == nil then
            message("ERROR", "The file of selected should be wrong, please double check!")
        end
        GUI.VID.text.vec.value = fd.value
        local str = fd.value
        local index = 1
        repeat
            index = string.find(str, "\\")
            str = string.sub(str, (index or 0)+1, -1)
        until index == nil

        iM501.vec.init = str

        printc(GUI.VID.text.vec.value)
    end
    fd:destroy()
end

function GUI.VID.button.browse_eft:action()
    local fd=iup.filedlg{dialogtype="OPEN", title="Select File", 
                         nochangedir="YES", directory=EFT_PATH,value="",
                         filter="", filterinfo="File", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status
 
    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
        GUI.VID.text.eft_code.value = fd.value
        --[[
        local match_string = "*.bin"
        local bin_file = misc.find_names(match_string, fd.value, 0)
        print(unpack(bin_file))
        if bin_file == nil or #bin_file == 0 then 
            message("ERROR", "This directory do not include any bin file.") 
        else
        	local file_check = 0
            for i = 1, #bin_file do
                if string.find(bin_file[i], "header") ~= nil then  file_check = file_check + 1 break end
            end
            for i = 1, #bin_file do
                if string.find(bin_file[i], "net") ~= nil then  file_check = file_check + 1 break end
            end
            for i = 1, #bin_file do
                if string.find(bin_file[i], "gram") ~= nil then  file_check = file_check + 1 break end
            end
            if file_check ~= 3 then
                message("ERROR", "This directory do not include right bin file, please double check!")
            end
        end
        ]]
    end
    fd:destroy()
end

function GUI.VID.button.dl_eft:action()
    
    file = io.open(GUI.VID.text.eft_code.value, "rb")
    if file == nil then
        message("Error", "Download file ERROR : data file is not exist! "..GUI.VID.text.eft_code.value)
        return
    end
    
    EFT_FILE = GUI.VID.text.eft_code.value
    printc(EFT_FILE)
    local str = file:read("*all")
    local data_tab = {}

    local x = string.len(str)
    for i = 1, x do
        data_tab[i] = string.byte(str, i)
    end
    file:close()
    file = nil

    local addr = tonumber(GUI.VID.text.eft_addr.value)
    if addr >= iM501.EFT_range[1] and addr < iM501.EFT_range[2] then
        iM501.eft_code[1][1] = addr
    else
        message("Error", "The start address for downloading set error.")
        return
    end
    EFT_ADDR = addr
    printc(string.format("0x%04x", EFT_ADDR))

    iM501.load_code(EFT_ADDR, EFT_FILE, "EFT")

    iup.Alarm("Download", "Download Successfully", "OK")
    export_gui_default("eft_file", EFT_FILE)
    export_gui_default("eft_addr", string.format("0x%04x", EFT_ADDR))
end

function GUI.VID.button.monitor_current:action()
--
    local counter = 0
    repeat
        wait(0.5)
        p501, meter_501_port = AM_501.find_COM()
        counter = counter + 1
    until meter_501_port or counter > 10
    --print(meter_501_port)
    if counter > 10 then
        message("Error", "Can't find "..iM501_meter.." instrument, please check HW connect and instrument.")
    end
    --p501 = AM_501.connect(meter_501_port)
    
    counter = 0
    repeat
        wait(0.5)
        p205, meter_205_port = AM_205.find_COM()
        counter = counter + 1
    until meter_205_port or counter > 10
    --print(meter_501_port)
    if counter > 10 then
        message("Error", "Can't find "..iM205_meter.." instrument, please check HW connect and instrument.")
    else
        p205 = AM_205.connect(meter_205_port)
        GUI.VID.dialog.monitor:show()
        GUI.VID.time.current_timer.run = "YES"
    end

end

function GUI.VID.button.dsp_control:action()
    GUI.VID.dialog.dsp_control:showxy(iup.RIGHT, iup.TOP)
end

function GUI.VID.button.mic_control:action()
    GUI.VID.dialog.mic_control:showxy(iup.RIGHT, iup.TOP)
end


function GUI.VID.time.oneshot_timer.action_cb()
    GUI.VID.time.oneshot_timer.run = "NO"
    print("\n\nRecall one shot timer...\n")
    --print(os.clock())
    GUI.VID.toggle.one_shot.value = "OFF"
    if MODE_AFTER_SPIR == 1 and T_NM2PSM == 0 then
        --NM_PSM = 1
        GUI.VID.toggle.nm.value = "ON"
        GUI.VID.toggle.nm.action()
    else
        --NM_PSM = 0
        GUI.VID.toggle.psm.value = "ON"
        GUI.VID.toggle.psm.action()
    end 
    --print(os.clock())

end

function GUI.VID.time.xaa_timer.action_cb()
   -- GUI.VID.time.xaa_timer.run = "NO"
    --print(os.clock())
    if GUI.VID.toggle.nm_xaa.value == "ON" and GUI.VID.toggle.nm_xaa.active == "YES" and (not (SPI_EN and PLAY_STATUS == 1)) then
        printc("\n\nRecall xAA read timer...\n")
        local state1, value1 = iM501.read_dram(iM501.message_return.xaa_environment, 2, DL_PATH)
        local state2, value2 = iM501.read_dram(iM501.message_return.xaa_spl, 2, DL_PATH)
		local state3, value3 = iM501.read_dram(iM501.message_return.xaa_floor, 2, DL_PATH)

        if state1 and state2 and state3 then
            if GUI.VID.toggle.hex.value == "ON" then
                value1 = string.format("0x%04x", value1)
                value2 = string.format("0x%04x", value2)
				value3 = string.format("0x%04x", value3)
            else
                value1 = string.format("%d", value1)
                value2 = string.format("%d", value2)
				value3 = string.format("%d", value3)
            end
            GUI.VID.text.noise_type.value = value1
            GUI.VID.text.spl.value = value2
			GUI.VID.text.xaa_floor.value = value3
        else
            --message("ERROR", value)
        end
    end
end


function GUI.VID.time.command_timer.action_cb()
   -- GUI.VID.time.xaa_timer.run = "NO"
    --print(os.clock())
    if DL_PATH == 2 and (GUI.VID.list.sd_function.value ~= 1) then
        printc("\n\nRecall command read timer...\n")
        local flag, command_num, command_detect = iM501.check_command()
if flag then
        GUI.VID.text.command_detect.value = string.format("0x%07x", command_detect)
        GUI.VID.text.command_num.value = command_num
end
    end
end

time_counter = 0
step_501 = 4
step_205 = 6
function GUI.VID.time.current_timer.action_cb()
    print("\n\nRecall timer...\n")
    print(os.clock())
    --time_counter = time_counter + 1
    
    --if time_counter = 1 or time_counter == step_501 then p501 = AM_501.connect(meter_501_port) end
    p501 = AM_501.connect(meter_501_port)
    print(os.clock())
    local value, unit = AM_501.query_measurement(p501)
    print(os.clock())
    print(value, unit)--, value*100/100)
    --[[
    if time_counter == step_501 then 
        AM_501.disconnect(p501) 
        p501 = AM_501.connect(meter_501_port)
        time_counter = 0
    end
    ]]
    AM_501.disconnect(p501) 
    print(os.clock())
    GUI.VID.text.current_im501.value = value
    GUI.VID.label.unit_im501.title = unit
    
    --if time_counter == step_205 then p205 = AM_205.connect(meter_205_port) end
    value, unit = AM_205.query_measurement(p205)
    print(os.clock())
    print(value, unit)--, value*100/100)
    --if time_counter == step_205 then AM_205.disconnect(p205) time_counter = 0 end
    GUI.VID.text.current_im205.value = value
    GUI.VID.label.unit_im205.title = unit
    print(os.clock())
end

function GUI.VID.dialog.monitor:close_cb()
    AM_501.disconnect(p501)
    AM_205.disconnect(p205)
    GUI.VID.time.current_timer.run = "NO"
    GUI.VID.dialog.monitor:hide()
    return iup.IGNORE
end

-------

MUSIC_PATH = NOAH_PATH.."music\\"
MUSIC_LIST = misc.find_names("*.wav", MUSIC_PATH, 0)
NO_MUSIC = MUSIC_LIST == nil and true or false
for i = 1, #MUSIC_LIST do
    MUSIC_LIST[i] = MUSIC_PATH..string.gsub(MUSIC_LIST[i], "\\", "")
end
--[[
command_pre = 0
music_list_item = 1
function GUI.VID.time.command_timer.action_cb()
    GUI.VID.time.command_timer.run = "NO"
    print("\n\nRecall command timer...\n")
    print(os.clock())
    local state, command = iM501.read_dram(iM501.vec.command_check)

    if state and bit:_and(command, 0x8000) == 0x8000 then
        iM501.write_dram(iM501.vec.command_check, 0)
        if command ~= command_pre then
            command_pre = command
            local com = bit:_and(command, 0x00ff)
            if com == 12 then -- play music
                if NO_MUSIC then
                    message("Error", "No file in music list.")
                else
                    audio.play(PC_SOUNDCARD, MUSIC_LIST[music_list_item], 0, true)
                    MUSIC_PLAY = true
                end
            elseif com == 13 then -- next one
                if NO_MUSIC then
                    message("Error", "No file in music list.")
                elseif MUSIC_PLAY then
                    music_list_item = music_list_item + 1
                    if music_list_item > #MUSIC_LIST then music_list_item = 1 end
                    audio.stop_play(PC_SOUNDCARD)
                    audio.play(PC_SOUNDCARD, MUSIC_LIST[music_list_item], 0, true)
                end
            elseif com == 15 then -- previous one
                if NO_MUSIC then
                    message("Error", "No file in music list.")
                elseif MUSIC_PLAY then
                    music_list_item = music_list_item - 1
                    if music_list_item < 1 then music_list_item = #MUSIC_LIST end
                    audio.stop_play(PC_SOUNDCARD)
                    audio.play(PC_SOUNDCARD, MUSIC_LIST[music_list_item], 0, true)
                end
            elseif com == 14 then -- stop play
                if MUSIC_PLAY then
                    audio.stop_play(PC_SOUNDCARD)
                    MUSIC_PLAY = false
                end
            else
                printc("Cann't support this command: "..command)
            end
        end
    end
    print(os.clock())
    GUI.VID.time.command_timer.run = "YES"
end
]]
--------------------

GUI.VID.time.command_timer.run = "NO"
--[[
GUI.VID.button.eft_start_addr.action = function() 
    GUI.VID.text.header.value = string.format("0x%08x", iM501.eft_code[1][1])
    GUI.VID.text.net.value = string.format("0x%08x", iM501.eft_code[2][1])
    GUI.VID.text.gram.value = string.format("0x%08x", iM501.eft_code[3][1])
    GUI.VID.dialog.start_address:popup()
end

GUI.VID.button.start_addr_cancel.action = function() 
    GUI.VID.dialog.start_address:hide()
end

GUI.VID.button.start_addr_ok.action = function()
    local addr = tonumber(GUI.VID.text.header.value)
    if addr >= iM501.EFT_range[1] and addr < iM501.EFT_range[2] then
        iM501.eft_code[1][1] = addr
    else
        message("Error", "The start address of header.bin set error.")
    end
    
    addr = tonumber(GUI.VID.text.net.value)
    if addr >= iM501.EFT_range[1] and addr < iM501.EFT_range[2] then
        iM501.eft_code[2][1] = addr
    else
        message("Error", "The start address of net.bin set error.")
    end
    addr = tonumber(GUI.VID.text.gram.value)
    if addr >= iM501.EFT_range[1] and addr < iM501.EFT_range[2] then
        iM501.eft_code[3][1] = addr
    else
        message("Error", "The start address of gram.bin set error.")
    end
    
    GUI.VID.dialog.start_address:hide()
end
]]

----MIC setting
GUI.VID.toggle.mic.action = function()
    if GUI.VID.toggle.mic.value == "ON" then
        printc("Use Smart MIC.")
        iM501.write_mic_setting(1)
        GUI.VID.button.mic_set.active = "ON"
        GUI.VID.toggle.avd.active = "ON"
        --if GUI.VID.toggle.avd.value == "OFF" then GUI.VID.toggle.avd.value = "ON" end
    else
        printc("Use General MIC.")
        iM501.write_mic_setting(0)
        GUI.VID.button.mic_set.active = "OFF"
        GUI.VID.toggle.avd.value = "OFF"
        GUI.VID.toggle.avd.action()
        GUI.VID.toggle.avd.active = "OFF"
    end
end

GUI.VID.button.mic_set.action = function()
    GUI.VID.dialog.mic_set:popup(iup.CENTER, iup.CENTER)
end

GUI.VID.toggle.avd.action = function()
    if GUI.VID.toggle.avd.value == "ON" then
        printc("Enable AVD.")
        iM501.update_vid_config(2,0,0,0,1)
        GUI.VID.button.avd_set.active = "ON"
    else
        printc("Disable AVD.")
        iM501.update_vid_config(2,0,0,0,0)
        GUI.VID.button.avd_set.active = "OFF"
    end
end

GUI.VID.toggle.psm_xsd.action = function()
    if GUI.VID.toggle.psm_xsd.value == "ON" then
        printc("Enable xSD.")
        iM501.update_vid_config(1,0,1,0)
        GUI.VID.button.xsd_set.active = "ON"
    else
        printc("Disable xSD.")
        iM501.update_vid_config(1,0,0,0)
        GUI.VID.button.xsd_set.active = "OFF"
    end
end

GUI.VID.toggle.psm_sd.action = function()
    if GUI.VID.toggle.psm_sd.value == "ON" then
        printc("Enable SD.")
        iM501.update_vid_config(1,1,1,0)
        GUI.VID.button.sd_set.active = "ON"
    else
        printc("Disable SD.")
        iM501.update_vid_config(1,1,0,0)
        --GUI.VID.button.sd_set.active = "OFF"
    end
end

GUI.VID.button.avd_set.action = function()
    GUI.VID.dialog.avd_set:popup(iup.CENTER, iup.CENTER)
end

GUI.VID.toggle.mic0_active.action = function()
    if GUI.VID.toggle.mic0_active.value == "ON" then
        printc("Enable MIC0.")
        GUI.VID.label.mic0_i2c_addr.active = "YES"
        GUI.VID.list.mic0_i2c_addr.active = "YES"
        GUI.VID.toggle.mic0type_a.active = "YES"
        GUI.VID.toggle.mic0type_d.active = "YES"
    else
        printc("Disable MIC0.")
        GUI.VID.label.mic0_i2c_addr.active = "NO"
        GUI.VID.list.mic0_i2c_addr.active = "NO"
        GUI.VID.toggle.mic0type_a.active = "NO"
        GUI.VID.toggle.mic0type_d.active = "NO"
    end
end
GUI.VID.toggle.mic1_active.action = function()
    if GUI.VID.toggle.mic1_active.value == "ON" then
        printc("Enable MIC1.")
        GUI.VID.label.mic1_i2c_addr.active = "YES"
        GUI.VID.list.mic1_i2c_addr.active = "YES"
        GUI.VID.toggle.mic1type_a.active = "YES"
        GUI.VID.toggle.mic1type_d.active = "YES"
    else
        printc("Disable MIC1.")
        GUI.VID.label.mic1_i2c_addr.active = "NO"
        GUI.VID.list.mic1_i2c_addr.active = "NO"
        GUI.VID.toggle.mic1type_a.active = "NO"
        GUI.VID.toggle.mic1type_d.active = "NO"
    end
end
GUI.VID.toggle.mic2_active.action = function()
    if GUI.VID.toggle.mic2_active.value == "ON" then
        printc("Enable MIC2.")
        GUI.VID.label.mic2_i2c_addr.active = "YES"
        GUI.VID.list.mic2_i2c_addr.active = "YES"
        GUI.VID.toggle.mic2type_a.active = "YES"
        GUI.VID.toggle.mic2type_d.active = "YES"
    else
        printc("Disable MIC2.")
        GUI.VID.label.mic2_i2c_addr.active = "NO"
        GUI.VID.list.mic2_i2c_addr.active = "NO"
        GUI.VID.toggle.mic2type_a.active = "NO"
        GUI.VID.toggle.mic2type_d.active = "NO"
    end
end
GUI.VID.toggle.mic3_active.action = function()
    if GUI.VID.toggle.mic3_active.value == "ON" then
        printc("Enable MIC3.")
        GUI.VID.label.mic3_i2c_addr.active = "YES"
        GUI.VID.list.mic3_i2c_addr.active = "YES"
        GUI.VID.toggle.mic3type_a.active = "YES"
        GUI.VID.toggle.mic3type_d.active = "YES"
    else
        printc("Disable MIC3.")
        GUI.VID.label.mic3_i2c_addr.active = "NO"
        GUI.VID.list.mic3_i2c_addr.active = "NO"
        GUI.VID.toggle.mic3type_a.active = "NO"
        GUI.VID.toggle.mic3type_d.active = "NO"
    end
end

---------------------------
GUI.VID.button.psm_set.action = function()
    GUI.VID.dialog.psm_set:popup(iup.CENTER, iup.CENTER)
end

GUI.VID.button.xsd_set.action = function()
    GUI.VID.dialog.xsd_set:popup(iup.CENTER, iup.CENTER)
end
GUI.VID.button.sd_set.action = function()
    GUI.VID.dialog.sd_set:popup(iup.CENTER, iup.CENTER)
end

function GUI.VID.list.psm_xsd_adc:action()
    GUI.VID.list.psm_xsd_adc.action_time = not GUI.VID.list.psm_xsd_adc.action_time
    if GUI.VID.list.psm_xsd_adc.action_time then
        printc("Select xSD power ADC")
        local value, index = {GUI.VID.list.psm_xsd_adc.value-1}, {PSM_XSD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.VID.list.psm_sd_adc:action()
    GUI.VID.list.psm_sd_adc.action_time = not GUI.VID.list.psm_sd_adc.action_time
    if GUI.VID.list.psm_sd_adc.action_time then
        printc("Select SD power ADC")
        local value, index = {GUI.VID.list.psm_sd_adc.value-1}, {PSM_SD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end
function GUI.VID.list.sd_kw:action()
    GUI.VID.list.sd_kw.action_time = not GUI.VID.list.sd_kw.action_time
    if GUI.VID.list.sd_kw.action_time then
        printc("Select SD keyword")
        iM501.write_message(iM501.message.configure_sd, GUI.VID.list.sd_kw.value+0, DL_PATH)
    end
end
function GUI.VID.list.sd_function:action()
    GUI.VID.list.sd_function.action_time = not GUI.VID.list.sd_function.action_time
    if GUI.VID.list.sd_function.action_time then
        printc("Select SD Function")
        iM501.write_message(iM501.message.configure_sd, GUI.VID.list.sd_function.value-1, DL_PATH)
    end
end
function GUI.VID.list.xsd:action()
    GUI.VID.list.xsd.action_time = not GUI.VID.list.xsd.action_time
    if GUI.VID.list.xsd.action_time then
        printc("Select xSD hotword")
    end
end


--------------------------
GUI.VID.toggle.one_shot_config.action = function()
    if GUI.VID.toggle.one_shot_config.value == "ON" then
        printc("Enable one shot.")
        GUI.VID.button.one_shot_set.active = "ON"
    else
        printc("Disable one shot.")
        GUI.VID.button.one_shot_set.active = "OFF"
    end
end
GUI.VID.button.one_shot_set.action = function()
    GUI.VID.dialog.one_shot_set:popup(iup.CENTER, iup.CENTER)
end

local function update_goto_value()
    if GUI.VID.toggle.spi_read.value == "ON" and DL_PATH == 2 then
        printc("Enable SPI read.")
        GOTO = 3
    else
        printc("Disable SPI read.")
        if GUI.VID.toggle.nm_config.value == "ON" then
            GOTO = 1
        else
            message("Error", "Illege operation.")
            GOTO = 1
            if GOTO == 3 then
                GUI.VID.toggle.spi_read.value = "ON" 
                GUI.VID.toggle.spi_read.action()
            elseif GOTO == 1 then
                GUI.VID.toggle.nm_config.value = "ON" 
                GUI.VID.toggle.nm_config.action()
            end
        end
    end
end

GUI.VID.toggle.spi_read.action = function()
    if GUI.VID.toggle.spi_read.value == "ON" then
        GUI.VID.toggle.spi2psm.active = "YES"
        GUI.VID.toggle.spi2nm.active = "YES"
    else
        GUI.VID.toggle.spi2psm.active = "NO"
        GUI.VID.toggle.spi2nm.active = "NO"
    end
    update_goto_value()
end
GUI.VID.toggle.spi2psm.action = function()
    if GUI.VID.toggle.spi2psm.value == "ON" then
        MODE_AFTER_SPIR = 2
        print(MODE_AFTER_SPIR)
    end
end

GUI.VID.toggle.spi2nm.action = function()
    if GUI.VID.toggle.spi2nm.value == "ON" then
        MODE_AFTER_SPIR = 1
        print(MODE_AFTER_SPIR)
    end
end

GUI.VID.toggle.spi_read.action = function()
    update_goto_value()
end

----------------
GUI.VID.toggle.nm_config.action = function()
    if GUI.VID.toggle.nm_config.value == "ON" then
        GUI.VID.button.nm_set.active = "YES"
        GUI.VID.button.ven_set.active = "YES"
        GUI.VID.button.xaa_set.active = "YES"
    else
        GUI.VID.button.nm_set.active = "NO"
        GUI.VID.button.ven_set.active = "NO"
        GUI.VID.button.xaa_set.active = "NO"
    end
    update_goto_value()
end

GUI.VID.button.nm_set.action = function()
    GUI.VID.dialog.nm_set:popup(iup.CENTER, iup.CENTER)
end


GUI.VID.button.ven_set.action = function()
    --GUI.VID.dialog.ven_set:popup(iup.CENTER, iup.CENTER)
end
---------------
--[[
GUI.VID.text.wakeup_time.active = function(self,c,new_value)
--print(self,c,new_value,GUI.ab_setting.text.file_in.value)
    if tonumber(c) == 13 then
        printc("Input file: "..new_value)
        GUI.VID.text.wakeup_time.value = new_value
        T_IRQ2CLKON = new_value
        print(T_IRQ2CLKON)
    end
end

GUI.VID.text.wakeup_time.spin_cb = function()
    print(GUI.VID.text.wakeup_time.spinvalue)
    T_IRQ2CLKON = GUI.VID.text.wakeup_time.value
    print(T_IRQ2CLKON)
end
]]

GUI.VID.setting_init = function()
    local mic_type = iM501.check_mic_setting()

    if mic_type == 1 then
        GUI.VID.toggle.mic.value = "ON"
    else 
        GUI.VID.toggle.mic.value = "OFF"
    end
    GUI.VID.toggle.mic.action()
    GUI.VID.toggle.mic0_active.action()
    GUI.VID.toggle.mic1_active.action()
    GUI.VID.toggle.mic2_active.action()
    GUI.VID.toggle.mic3_active.action()
    
end


GUI.VID.button.update_set.action = function()
    if AUDIO_BUS_TYPE ~= 1 then return end
    T_IRQ2CLKON = (GUI.VID.text.wakeup_time.value+0)*1000
    T_SPIR2SPIR = (GUI.VID.text.spi_read_interval.value+0)*1000
    T_NM2PSM  = (GUI.VID.text.back_to_psm_delay.value+0)*1000
    C_SPIR  = GUI.VID.text.spi_read_cycle.value+0

    --if GUI.VID.toggle.spi2psm.value == "ON" then
    if NM_PSM == 0 then
        MODE_AFTER_SPIR = 2
    --elseif GUI.VID.toggle.spi2nm.value == "ON" then
    elseif NM_PSM == 1 then   
        MODE_AFTER_SPIR = 1
    end
    update_goto_value()
    print(T_IRQ2CLKON, T_NM2PSM, T_SPIR2SPIR, C_SPIR, GOTO, MODE_AFTER_SPIR)
    iM501.rec_voice_buffer()
end

function mode_switch_active(active)
    GUI.VID.toggle.nm.active = active
    GUI.VID.toggle.bypass.active = active
    GUI.VID.toggle.psm.active = AUDIO_BUS_TYPE == 1 and active or 'NO'
    GUI.VID.toggle.one_shot.active = AUDIO_BUS_TYPE == 1 and active or 'NO'
end

table_vid_state = {}
function  save_vid_state()
        table_vid_state[1] = GUI.VID.toggle.mic.active
        --table_vid_state[2] = GUI.VID.toggle.psm_config.active
        --table_vid_state[3] = GUI.VID.toggle.one_shot_config.active
        --table_vid_state[4] = GUI.VID.toggle.nm_config.active
        table_vid_state[5] = GUI.VID.toggle.avd.active
        table_vid_state[6] = GUI.VID.toggle.psm_xsd.active
        table_vid_state[7] = GUI.VID.toggle.psm_sd.active
        table_vid_state[8] = GUI.VID.toggle.nm_xaa.active
        table_vid_state[9] = GUI.VID.toggle.ven.active
        --table_vid_state[10] = GUI.VID.toggle.ultra_sound.active
        --table_vid_state[11] = GUI.VID.toggle.third_part_sw.active
        table_vid_state[12] = GUI.VID.button.mic_set.active
        table_vid_state[13] = GUI.VID.button.avd_set.active
        table_vid_state[14] = GUI.VID.button.psm_set.active
        table_vid_state[15] = GUI.VID.button.xsd_set.active
        table_vid_state[16] = GUI.VID.button.sd_set.active
        table_vid_state[17] = GUI.VID.button.one_shot_set.active
        table_vid_state[18] = GUI.VID.button.nm_set.active
        table_vid_state[19] = GUI.VID.button.ven_set.active
        table_vid_state[20] = GUI.VID.button.xaa_set.active
        --table_vid_state[21] = GUI.VID.button.ultra_sound.active
        --table_vid_state[22] = GUI.VID.button.third_part_sw.active
        --table_vid_state[23] = GUI.VID.button.dsp_control.active
        
end

GUI.VID.active = function(active, bypass)
    if active == "yes" or active == "YES" then
        --[[
        GUI.VID.toggle.mic.active = "YES"  --table_vid_state[1]
        --GUI.VID.toggle.psm_config.active = "YES"  --table_vid_state[2]
        --GUI.VID.toggle.one_shot_config.active = "YES"  --table_vid_state[3]
        --GUI.VID.toggle.nm_config.active = "YES"  --table_vid_state[4]
        GUI.VID.toggle.avd.active = "YES"  --table_vid_state[5]
        GUI.VID.toggle.psm_xsd.active = "YES"  --table_vid_state[6]
        GUI.VID.toggle.psm_sd.active = "YES"  --table_vid_state[7]
        GUI.VID.toggle.nm_xaa.active = "YES"  --table_vid_state[8]
        GUI.VID.toggle.ven.active = "YES"  --table_vid_state[9]
        --GUI.VID.toggle.ultra_sound.active = "YES"  --table_vid_state[10]
        --GUI.VID.toggle.third_part_sw.active = "YES"  --table_vid_state[11]
        GUI.VID.button.mic_set.active = "YES"  --table_vid_state[12]
        GUI.VID.button.avd_set.active = "YES"  --table_vid_state[13]
        GUI.VID.button.psm_set.active = "YES"  --table_vid_state[14]
        GUI.VID.button.xsd_set.active = "YES"  --table_vid_state[15]
        GUI.VID.button.sd_set.active = "YES"  --table_vid_state[16]
        GUI.VID.button.one_shot_set.active = "YES"  --table_vid_state[17]
        GUI.VID.button.nm_set.active = "YES"  --table_vid_state[18]
        GUI.VID.button.ven_set.active = "YES"  --table_vid_state[19]
        GUI.VID.button.xaa_set.active = "YES"  --table_vid_state[20]
        --GUI.VID.button.ultra_sound.active = "YES"  --table_vid_state[21]
        --GUI.VID.button.third_part_sw.active = "YES"  --table_vid_state[22]
        --GUI.VID.button.dsp_control.active = "YES"  --table_vid_state[23]
        GUI.VID.button.command_read.active = "YES"
        ]]
        GUI.VID.button.psm_set.active = "YES"  --table_vid_state[14]
        GUI.VID.toggle.spi_record.active = "YES"

    elseif active == "no" or active == "NO" then
        save_vid_state()
        GUI.VID.toggle.mic.active = "no"
        --GUI.VID.toggle.psm_config.active = "no"
        --GUI.VID.toggle.one_shot_config.active = "no"
        --GUI.VID.toggle.nm_config.active = "no"
        GUI.VID.toggle.avd.active = "no"
        GUI.VID.toggle.psm_xsd.active = "no"
        GUI.VID.toggle.psm_sd.active = "no"
        GUI.VID.toggle.nm_xaa.active = "no"
        GUI.VID.toggle.ven.active = "no"
        --GUI.VID.toggle.ultra_sound.active = "no"
        --GUI.VID.toggle.third_part_sw.active = "no"
        GUI.VID.button.mic_set.active = "no"
        GUI.VID.button.avd_set.active = "no"
        GUI.VID.button.psm_set.active = "no"
        GUI.VID.button.xsd_set.active = "no"
        GUI.VID.button.sd_set.active = "no"
        GUI.VID.button.one_shot_set.active = "no"
        GUI.VID.button.nm_set.active = "no"
        GUI.VID.button.ven_set.active = "no"
        GUI.VID.button.xaa_set.active = "no"
        --GUI.VID.button.ultra_sound.active = "no"
        --GUI.VID.button.third_part_sw.active = "no"
        --GUI.VID.button.dsp_control.active = "no"
        GUI.VID.toggle.spi_record.active = "no"
        --if bypass then
            GUI.VID.button.command_read.active = "no" 
        --end

    end
end

