----If variable has this mark "--**", 
----  means you must check or modify it before start test!!!!
----Test Variable

--****** Define parameters of platform
    PC_SOUNDCARD_PLAY = 0
    PC_SOUNDCARD = "Speakers (Realtek High Definition Audio)"  --** pleas make sure this name is exist in you PC play soundcards list.
    PC_SOUNDCARD = PC_SOUNDCARD_PLAY == 1 and check_soundcard(PC_SOUNDCARD) or PC_SOUNDCARD

    AUDIO_BUS = "PDM"          --** Select audio bus.  "PDM", "I2S", "TDM", "PCM"
    --1: 1.024M, 2: 2.048M, 3: 3.072M, 4: 4.096M
    PDM_ADC_CLK_SEL = 2          --** pdm ADC clock setting. default is 2.
    PDM_DAC_CLK_SEL = 1          --** pdm DAC clock setting. default is 1.

    AUDIO_BUS_TYPE = 1       --1:  I2S  2:  TDM   3:   PCM
    SAMPLE_RATE    = regression.config.dut.sample_rate or 16000   --** sample rate
    SAMPLE_BIT_LEN = 16   --** FM1388: 32bit length, other: 16bit
      ONE_CYCLE_DELAY = 1  -- 1: enable, 0: disable   --PCM: no delay; I2S/TDM: enalbe
      BCLK_POLARITY = 0    --0: Rising edge, 1: Falling edge
      --DATE_LATCH = 0    --0: Rising edge, 1: Falling edge
      --FRAM_START = 5    --5: Rising edge, 4: Falling edge  --PCM/TDM: Rising edge; I2S: Falling edge
      M_S = 1           --1: master, 0: slave              --base on UIF
      BIT_ORDER = 1     --1: MSB first, 2: LSB first
      SLOT_NUM = 8


    I2C_SPEED     = 400      --I2C speed. 400 means 400k.
    SPI_SPEED     = 5000    --SPI speed. 1000 means 1000k.
      SPI_MODE    = 0        --SPI format setting. 0,1,2,3. CPHA,CPOL 

--****** Define parameters of DSP code and vec.
    SW_VERSION = regression.config.dut.version
    CODE_VEC_PATH = regression.config.PATH.DSP 

    DL_PATH = 2  -- For iM501, 1 means I2C; 2 means SPI.
    
    SELF_POWER_DOWN_EN = 0     -- for iM501 and iM401. 1: after power up, delay DELAY_POWER_DOWN, MCU control DSP to power down.
    DELAY_POWER_DOWN = 3000    -- for iM501 and iM401, ms.
    PDMCLK_TURNOFF_EN = 1      -- for iM501 and iM401, if equel 1, means pdmclk will turn off after power down.

    SR_LEN = "S"..SAMPLE_BIT_LEN.."_LE"
    
    AB_ENABLE = 1
    Enable_Load_Code_Check = false   --**

    Host_FW_Ver = "FW:H:V2.7"
    Audio_FW_Ver = "FW:A:V2.53"