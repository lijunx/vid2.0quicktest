--module define
printc("define waveview GUI module.")
GUI.waveview = 
{
    constant = {};
    list = {};
    label = {};
    text = {};
    button =
    {
        waveview = iup.button
        {
            size="80x", title = "Wave View",
            action = function() f1 = io.popen(NOAH_PATH.."WaveViewer\\WaveWindow.exe") end
        };
        start_p_r = iup.button
        {
            size="80x", title = "Play & Record",
            action = function() start_play_record(0) end
        };
        stop_p_r = iup.button
        {
            size="80x", title = "Stop",
            action = function() stop_play_record() end
        };
    };
}

GUI.waveview.frame = iup.frame
{
    iup.hbox
    {
        GUI.waveview.button.waveview,
        GUI.waveview.button.start_p_r,
        GUI.waveview.button.stop_p_r;
        gap="62",margin="45x10",alignment = "ACENTER",
    };
    title = "Play and Record", 
    size="400x",margin="0x10",alignment = "ACENTER",
};

GUI.waveview.active = function(active)
    if active == "yes" or active == "YES" then
        GUI.waveview.button.waveview.active = "yes"
        GUI.waveview.button.start_p_r.active = "yes" 
        GUI.waveview.button.stop_p_r.active = "yes"
    elseif active == "no" or active == "NO" then
        GUI.waveview.button.waveview.active = "no"
        GUI.waveview.button.start_p_r.active = "no" 
        GUI.waveview.button.stop_p_r.active = "no"
    end
end

GUI.waveview.active("yes")
