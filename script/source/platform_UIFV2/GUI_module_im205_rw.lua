--why iup.list action will run twice?
--module define
printc("define iM205 write&read GUI module.")
GUI = GUI or {}
GUI.im205 = {}
GUI.im205.constant =
{
    i2c_protocol_default = iM205.protocol, --"Combined re-S"
    i2c_address_default = (iM205.I2C_addr-0xa0)/2 + 1;
    i2c_address_o_default = (iM205.I2C_addr-0xa0)/2 + 1;
    avd_thd_default = 1,
    clk_trim_default = 1,
    vpump_default = 1,
    efuse = 0xffff;
    
}
GUI.im205.label = 
{
    addr_s_1 = iup.label{title = "1: Reg(0x):"};
    data_s_1 = iup.label{title = "Value(0x):"};
    addr_s_2 = iup.label{title = "2: Reg(0x):"};
    data_s_2 = iup.label{title = "Value(0x):"};
    i2c_protocol = iup.label{title = "I2C Format:"};
    i2c_address = iup.label{title = "Address:"};
    i2c_address_o = iup.label{title = "I2C Address:"};--,size="33x20"};
    clk_trim = iup.label{title = "CLK Trim:"};
    vpump = iup.label{title = "VPUMP:"};
    avd_thd = iup.label{title = "AVD Threshold:"};--,size="38x20"};
    tf_gain = iup.label{title = "TF(0x):"};
}
GUI.im205.list = 
{
    i2c_protocol = iup.list
    {
        "Combined re-S","Standard";
        dropdown="YES", visible_items=2, value = GUI.im205.constant.i2c_protocol_default, size="75x",action_time = false,
    };
    i2c_address = iup.list
    {
        "0xA0","0xA2","0xA4","0xA6";
        dropdown="YES", visible_items=4, value = GUI.im205.constant.i2c_address_default, size="40x",action_time = false,
    };
    i2c_address_o = iup.list
    {
        "0xA0","0xA2","0xA4","0xA6";
        dropdown="YES", visible_items=4, value = GUI.im205.constant.i2c_address_o_default, size="40x",action_time = false,
    };
    clk_trim = iup.list
    {
        "920KHz","1025KHz","750KHz","800KHz",
        dropdown="YES", visible_items=4, value = GUI.im205.constant.clk_trim_default, size="50x",action_time = false,
    };
    vpump = iup.list
    {
        "10.5V","9.0V","13.5V","12.0V",
        dropdown="YES", visible_items=4, value = GUI.im205.constant.vpump_default, size="50x",action_time = false,
    };
    avd_thd = iup.list
    {
        "7.5mV","6.5mV","5.0mV","3.5mV","12.0mV","11.0mV","10.0mV","8.5mV";
        dropdown="YES", visible_items=8, value = GUI.im205.constant.avd_thd_default, size="50x",action_time = false,
    };
}

GUI.im205.list.i2c_protocol.action = function()
     GUI.im205.list.i2c_protocol.action_time = not GUI.im205.list.i2c_protocol.action_time
     if GUI.im205.list.i2c_protocol.action_time then
         iM205.protocol = GUI.im205.list.i2c_protocol.value+0
         iM205.init()
     end
end
GUI.im205.list.i2c_address.action = function()
     GUI.im205.list.i2c_address.action_time = not GUI.im205.list.i2c_address.action_time
     if GUI.im205.list.i2c_address.action_time then
         iM205.I2C_addr = (GUI.im205.list.i2c_address.value-1)*2+0xa0
         GUI.im205.list.i2c_address_o.value = GUI.im205.list.i2c_address.value
     end
end

GUI.im205.list.i2c_address_o.action = function()
     GUI.im205.list.i2c_address_o.action_time = not GUI.im205.list.i2c_address_o.action_time
     if GUI.im205.list.i2c_address_o.action_time then
     --    iM205.efuse = 4 - GUI.im205.list.i2c_address_o.value
     end
end

GUI.im205.list.avd_thd.action = function()
     GUI.im205.list.avd_thd.action_time = not GUI.im205.list.avd_thd.action_time
     if GUI.im205.list.avd_thd.action_time then
     --    efuse = 8 - GUI.im205.list.avd_thd.value
     end
end

GUI.im205.text =
{
    reg_addr_1 = iup.text{value = import_gui_default("im205_reg_addr1"), size="30x"};
    reg_data_1 = iup.text{value = import_gui_default("im205_reg_data1"), size="30x"};
    reg_addr_2 = iup.text{value = import_gui_default("im205_reg_addr2"), size="30x"};
    reg_data_2 = iup.text{value = import_gui_default("im205_reg_data2"), size="30x"};
    tf_gain = iup.text{value = "0x1F", size="30x"};
    init_vec = iup.text{value = CODE_VEC_PATH..iM205.vec.init, size="150x"};
}

GUI.im205.button =
{
    reg_r_1 = iup.button{size="20x", title="R",};
    reg_w_1 = iup.button{size="20x", title="W",};
    reg_r_2 = iup.button{size="20x", title="R",};
    reg_w_2 = iup.button{size="20x", title="W",};
    efuse_w = iup.button{size="40x", title="Write",};
    efuse_r = iup.button{size="40x", title="Read",};
    browse_vec = iup.button{size="60x", title="Browse Vec"};
    load_vec = iup.button{size="60x", title="Load Vec"};
}

GUI.im205.toggle = 
{
    enable = iup.toggle{title="",value="OFF";};
    i2c_addr = iup.toggle{title="",value="OFF";};
    vpump = iup.toggle{title="",value="OFF";};
    avd_thd = iup.toggle{title="",value="OFF";};
    tf_gain = iup.toggle{title="",value="OFF";};
    clk_trim = iup.toggle{title="",value="OFF";};
    efuse_wr = iup.toggle{title="",value="OFF";};
}

GUI.im205.local_frame = 
{
    rw = iup.frame
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.im205.toggle.enable,
                iup.hbox{GUI.im205.label.i2c_protocol,GUI.im205.list.i2c_protocol,gap="0",margin="0",alignment = "ACENTER"};
                iup.hbox{GUI.im205.label.i2c_address,GUI.im205.list.i2c_address,gap="0",margin="0",alignment = "ACENTER"};
                gap="30",margin="5x0",alignment = "ACENTER"
            };
            iup.vbox
            {
                iup.hbox
                {
                    iup.hbox{GUI.im205.label.addr_s_1,GUI.im205.text.reg_addr_1;gap="0",margin="0",alignment = "ACENTER"};
                    iup.hbox{GUI.im205.label.data_s_1,GUI.im205.text.reg_data_1;gap="0",margin="0",alignment = "ACENTER"};
                    iup.hbox{GUI.im205.button.reg_r_1,GUI.im205.button.reg_w_1;gap="0",margin="0",alignment = "ACENTER"};
                    gap="5",margin="0x0",alignment = "ACENTER"
                };
                iup.hbox
                {
                    iup.hbox{GUI.im205.label.addr_s_2,GUI.im205.text.reg_addr_2;gap="0",margin="0",alignment = "ACENTER"};
                    iup.hbox{GUI.im205.label.data_s_2,GUI.im205.text.reg_data_2;gap="0",margin="0",alignment = "ACENTER"};
                    iup.hbox{GUI.im205.button.reg_r_2,GUI.im205.button.reg_w_2;gap="0",margin="0",alignment = "ACENTER"};
                    gap="5",margin="0x0",alignment = "ACENTER";
                };
                gap="5",margin="7x0",alignment = "ACENTER"
            };
            gap="5",margin="0x5",alignment = "ALEFT"
        };
        title = "Register Read and Write", 
        size="200x",margin="5x5",alignment = "ACENTER",
    },
    load_vec = iup.frame
    {
        iup.hbox
        {
            GUI.im205.text.init_vec;
            GUI.im205.button.browse_vec;
            GUI.im205.button.load_vec;
            gap="80",margin="5x5",alignment = "ALEFT"
        };
        title = "Load Vec", 
        size="400x",margin="0x5",alignment = "ALEFT",
    },
    efuse = iup.frame
    {
        iup.vbox
        {
            iup.hbox
            {
                iup.hbox{GUI.im205.toggle.i2c_addr,GUI.im205.label.i2c_address_o,GUI.im205.list.i2c_address_o;gap="2",margin="0",alignment = "ACENTER"};
                iup.hbox{GUI.im205.toggle.vpump,GUI.im205.label.vpump,GUI.im205.list.vpump;gap="2",margin="0",alignment = "ACENTER"};
                iup.hbox{GUI.im205.toggle.avd_thd,GUI.im205.label.avd_thd,GUI.im205.list.avd_thd;gap="2",margin="0",alignment = "ACENTER"};
                gap="39",margin="0",alignment = "ACENTER"
            };
            iup.hbox
            {
                iup.hbox{GUI.im205.toggle.clk_trim,GUI.im205.label.clk_trim,GUI.im205.list.clk_trim;gap="2",margin="0",alignment = "ACENTER"};
                iup.hbox{GUI.im205.toggle.tf_gain,GUI.im205.label.tf_gain,GUI.im205.text.tf_gain;gap="2",margin="0",alignment = "ACENTER"};
                iup.hbox{GUI.im205.button.efuse_r,GUI.im205.button.efuse_w;gap="25",margin="27",alignment = "ACENTER"};
                --GUI.im205.button.efuse_r,GUI.im205.button.efuse_w,
                gap="45",margin="0",alignment = "ACENTER"
            };
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        title = "eFuse Write", 
        size="400x",margin="5x5",alignment = "ACENTER",
    }
}
GUI.im205.frame = iup.frame
{
    iup.vbox
    {
        GUI.im205.local_frame.rw;
        --GUI.im205.local_frame.load_vec;
        --GUI.im205.local_frame.efuse;
        gap="5",margin="0x5",alignment = "ACENTER"
    };
    title = "iM205 Operation", 
    size="200x",margin="15x10",alignment = "ACENTER",
}

function update_efuse_value()
    local i2c_addr = GUI.im205.toggle.i2c_addr.value == "ON" and (4 - GUI.im205.list.i2c_address_o.value) or 0x3
    local clk_trim = GUI.im205.toggle.clk_trim.value == "ON" and (4 - GUI.im205.list.clk_trim.value) or 0x3
    local vpump = GUI.im205.toggle.vpump.value == "ON" and (4 - GUI.im205.list.vpump.value) or 0x3
    local avd_thd = GUI.im205.toggle.avd_thd.value == "ON" and (8 - GUI.im205.list.avd_thd.value) or 0x7
    local tf_gain = GUI.im205.toggle.tf_gain.value == "ON" and (GUI.im205.text.tf_gain.value + 0) or 0x1f
    local efuse = bit:_lshift(i2c_addr,12) + bit:_lshift(clk_trim,10) +  bit:_lshift(vpump,8) + bit:_lshift(avd_thd,5) + tf_gain
    efuse = bit:_or(efuse, 0xc000)
    printc(string.format("0x%0x", efuse))
    return efuse
end

function update_efuse_gui(value)
    local i2c_addr = bit:_rshift(value,12)
    i2c_addr = bit:_and(i2c_addr,0x3)
    GUI.im205.list.i2c_address_o.value = 4 - i2c_addr
    local clk_trim = bit:_rshift(value,10)
    clk_trim = bit:_and(clk_trim,0x3)
    GUI.im205.list.clk_trim.value = 4 - clk_trim
    local vpump = bit:_rshift(value,8)
    vpump = bit:_and(vpump,0x3)
    GUI.im205.list.vpump.value = 4 - vpump
    local avd_thd = bit:_rshift(value,5)
    avd_thd = bit:_and(avd_thd,0x7)
    GUI.im205.list.avd_thd.value = 8 - avd_thd
    local tf_gain = bit:_and(value,0x1f)
    GUI.im205.text.tf_gain.value = string.format("0x%0x", tf_gain)
    printc(i2c_addr,clk_trim,vpump,avd_thd,string.format("0x%0x", tf_gain))
end

function GUI.im205.button.reg_r_1:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr = remove_0x(spi_reg_addr.value)
    local addr = remove_0x(GUI.im205.text.reg_addr_1.value)
    if string.find(addr, "%X") ~= nil then
        message("ERROR", "The input contains illegal characters.")
    elseif addr == "" then
        message("ERROR", "Nothing input")
    else
        local state, value = iM205.read_reg(tonumber("0x"..addr))
        if state then
            GUI.im205.text.reg_data_1.value = string.format("0x%02x", value)
            export_gui_default("im205_reg_addr1", "0x"..addr)
            export_gui_default("im205_reg_data1", string.format("0x%02x", value))
        else
            message("ERROR", value)
        end
    end
end

function GUI.im205.button.reg_w_1:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr, data = remove_0x(spi_reg_addr.value), remove_0x(spi_reg_data.value)
    local addr = remove_0x(GUI.im205.text.reg_addr_1.value)
    if addr == false then return end
    local data = remove_0x(GUI.im205.text.reg_data_1.value)
    if data == false then return end
  
    local state = iM205.write_reg(tonumber("0x"..addr), tonumber("0x"..data))
    export_gui_default("im205_reg_addr1", "0x"..addr)
    export_gui_default("im205_reg_data1", "0x"..data)
    if state == false then
        message("ERROR", "Write iM205 Register Error")
    end
end

function GUI.im205.button.reg_r_2:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr = remove_0x(spi_reg_addr.value)
    local addr = remove_0x(GUI.im205.text.reg_addr_2.value)
    if string.find(addr, "%X") ~= nil then
        message("ERROR", "The input contains illegal characters.")
    elseif addr == "" then
        message("ERROR", "Nothing input")
    else
        local state, value = iM205.read_reg(tonumber("0x"..addr))
        if state then
            GUI.im205.text.reg_data_2.value = string.format("0x%02x", value)
            export_gui_default("im205_reg_addr2", "0x"..addr)
            export_gui_default("im205_reg_data2", string.format("0x%02x", value))
        else
            message("ERROR", value)
        end
    end
end

function GUI.im205.button.reg_w_2:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr, data = remove_0x(spi_reg_addr.value), remove_0x(spi_reg_data.value)
    local addr = remove_0x(GUI.im205.text.reg_addr_2.value)
    if addr == false then return end
    local data = remove_0x(GUI.im205.text.reg_data_2.value)
    if data == false then return end
  
    local state = iM205.write_reg(tonumber("0x"..addr), tonumber("0x"..data))
    export_gui_default("im205_reg_addr2", "0x"..addr)
    export_gui_default("im205_reg_data2", "0x"..data)
    if state == false then
        message("ERROR", "Write iM205 Register Error")
    end
end

efuse_r = 0
function GUI.im205.button.efuse_r:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end

    local state, value = iM205.read_reg(4)
    if state then
    else
        message("ERROR", value)
        return
    end
    local efuse = value
    state, value = iM205.read_reg(5)
    if state then
    else
        message("ERROR", value)
        return
    end
    efuse = value*256+efuse
    --print(efuse)
    update_efuse_gui(efuse)
    efuse_r = 1
end

function GUI.im205.button.efuse_w:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    if efuse_r == 0 then
        message("Warning", "Please read eFuse first!")
        return
    end
     efuse_r = 0
    --local addr, data = remove_0x(spi_reg_addr.value), remove_0x(spi_reg_data.value)
    iM205.efuse = update_efuse_value()

    local state, value = iM205.read_reg(3)
    iM205.write_reg(3, bit:_or(value, 0xc))
    local state = iM205.write_reg(4, iM205.efuse % 256)
    if state == false then
        message("ERROR", "Write iM205 Register Error")
    end
    local temp =  math.floor(iM205.efuse / 256)
    --print("aaaaaaaaaaa"..tostring(temp))
    local state = iM205.write_reg(5, temp)
    if state == false then
        message("ERROR", "Write iM205 Register Error")
    end
    local state, value = iM205.read_reg(7)
    iM205.write_reg(7, bit:_or(value, 0x1))

    GUI.im205.list.i2c_address.value = GUI.im205.list.i2c_address_o.value
    iM205.I2C_addr = (GUI.im205.list.i2c_address.value-1)*2+0xa0
end

GUI.im205.toggle.i2c_addr.action = function()
    if GUI.im205.toggle.i2c_addr.value == "ON" then
        GUI.im205.label.i2c_address_o.active = "yes"
        GUI.im205.list.i2c_address_o.active = "yes"
    else
        GUI.im205.label.i2c_address_o.active = "no"
        GUI.im205.list.i2c_address_o.active = "no"
    end
    GUI.im205.toggle.efuse_wr.action()
end
GUI.im205.toggle.clk_trim.action = function()
    if GUI.im205.toggle.clk_trim.value == "ON" then
        GUI.im205.label.clk_trim.active = "yes"
        GUI.im205.list.clk_trim.active = "yes"
    else
        GUI.im205.label.clk_trim.active = "no"
        GUI.im205.list.clk_trim.active = "no"
    end
    GUI.im205.toggle.efuse_wr.action()
end
GUI.im205.toggle.vpump.action = function()
    if GUI.im205.toggle.vpump.value == "ON" then
        GUI.im205.label.vpump.active = "yes"
        GUI.im205.list.vpump.active = "yes"
    else
        GUI.im205.label.vpump.active = "no"
        GUI.im205.list.vpump.active = "no"
    end
    GUI.im205.toggle.efuse_wr.action()
end
GUI.im205.toggle.avd_thd.action = function()
    if GUI.im205.toggle.avd_thd.value == "ON" then
        GUI.im205.label.avd_thd.active = "yes"
        GUI.im205.list.avd_thd.active = "yes"
    else
        GUI.im205.label.avd_thd.active = "no"
        GUI.im205.list.avd_thd.active = "no"
    end
    GUI.im205.toggle.efuse_wr.action()
end
GUI.im205.toggle.tf_gain.action = function()
    if GUI.im205.toggle.tf_gain.value == "ON" then
        GUI.im205.label.tf_gain.active = "yes"
        GUI.im205.text.tf_gain.active = "yes"
    else
        GUI.im205.label.tf_gain.active = "no"
        GUI.im205.text.tf_gain.active = "no"
    end
    GUI.im205.toggle.efuse_wr.action()
end
GUI.im205.toggle.efuse_wr.action = function()
    if GUI.im205.toggle.i2c_addr.value == "ON"
      or  GUI.im205.toggle.vpump.value == "ON"
      or GUI.im205.toggle.avd_thd.value == "ON"
      or GUI.im205.toggle.tf_gain.value == "ON"
      or GUI.im205.toggle.clk_trim.value == "ON" then
        GUI.im205.button.efuse_r.active = "yes"
        GUI.im205.button.efuse_w.active = "yes"
    else
        GUI.im205.button.efuse_r.active = "no"
        GUI.im205.button.efuse_w.active = "no"
    end
end

function GUI.im205.button.browse_vec:action()
    local fd=iup.filedlg{dialogtype="OPEN", title="Load vec file", 
                         nochangedir="YES", directory=CODE_VEC_PATH,value=iM205.vec.init,
                         filter="*.vec", filterinfo="parameter file", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status
    CODE_VEC_PATH = fd.directory
    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
    --[[
        if string.find(fd.value, "init") == nil then
            message("ERROR", "The file of selected should be wrong, please double check!")
        end
        ]]
        GUI.im205.text.init_vec.value = fd.value
        local str = fd.value
        local index = 1
        repeat
            index = string.find(str, "\\")
            str = string.sub(str, (index or 0)+1, -1)
        until index == nil

        iM205.vec.init = str

        printc(GUI.im205.text.init_vec.value)
    end
    fd:destroy()
end

function GUI.im205.button.load_vec:action()
    iM205.load_vec(CODE_VEC_PATH..iM205.vec.init)
end


GUI.im205.toggle.enable.action = function()
    if GUI.im205.toggle.enable.value == "ON" then
        GUI.im205.toggle.enable.active = "yes"
        GUI.im205.toggle.i2c_addr.active = "yes"
        GUI.im205.toggle.clk_trim.active = "yes"
        GUI.im205.toggle.vpump.active = "yes"
        GUI.im205.toggle.avd_thd.active = "yes"
        GUI.im205.toggle.tf_gain.active = "yes"
        GUI.im205.text.reg_addr_1.active = "yes"
        GUI.im205.text.reg_data_1.active = "yes"
        GUI.im205.button.reg_r_1.active = "yes" 
        GUI.im205.button.reg_w_1.active = "yes"
        GUI.im205.label.addr_s_1.active = "yes"
        GUI.im205.label.data_s_1.active = "yes"
        GUI.im205.text.reg_addr_2.active = "yes"
        GUI.im205.text.reg_data_2.active = "yes"
        GUI.im205.button.reg_r_2.active = "yes" 
        GUI.im205.button.reg_w_2.active = "yes"
        GUI.im205.label.addr_s_2.active = "yes"
        GUI.im205.label.data_s_2.active = "yes"
        GUI.im205.list.i2c_protocol.active = "yes"
        GUI.im205.list.i2c_address.active = "yes"
        GUI.im205.label.i2c_protocol.active = "yes"
        GUI.im205.label.i2c_address.active = "yes"
        GUI.im205.toggle.i2c_addr.action()
        GUI.im205.toggle.clk_trim.action()
        GUI.im205.toggle.vpump.action()
        GUI.im205.toggle.avd_thd.action()
        GUI.im205.toggle.tf_gain.action()        
        iM205.protocol = GUI.im205.list.i2c_protocol.value+0
        iM205.init()
        --message("Warning","Please make sure the I2C address is right!")
    else
        GUI.im205.toggle.i2c_addr.active = "no"
        GUI.im205.toggle.vpump.active = "no"
        GUI.im205.toggle.avd_thd.active = "no"
        GUI.im205.toggle.tf_gain.active = "no"
        GUI.im205.toggle.clk_trim.active = "no"
        GUI.im205.text.reg_addr_1.active = "no"
        GUI.im205.text.reg_data_1.active = "no"
        GUI.im205.text.reg_addr_2.active = "no"
        GUI.im205.text.reg_data_2.active = "no"
        GUI.im205.text.tf_gain.active = "no"
        GUI.im205.button.reg_r_1.active = "no" 
        GUI.im205.button.reg_w_1.active = "no"
        GUI.im205.button.reg_r_2.active = "no" 
        GUI.im205.button.reg_w_2.active = "no"
        GUI.im205.button.efuse_w.active = "no"
        GUI.im205.button.efuse_r.active = "no"
        GUI.im205.list.i2c_protocol.active = "no"
        GUI.im205.list.i2c_address.active = "no"
        GUI.im205.list.vpump.active = "no"
        GUI.im205.list.i2c_address_o.active = "no"
        GUI.im205.list.avd_thd.active = "no"
        GUI.im205.label.addr_s_1.active = "no"
        GUI.im205.label.data_s_1.active = "no"
        GUI.im205.label.addr_s_2.active = "no"
        GUI.im205.label.data_s_2.active = "no"
        GUI.im205.label.i2c_protocol.active = "no"
        GUI.im205.label.i2c_address.active = "no"
        GUI.im205.label.vpump.active = "no"
        GUI.im205.label.i2c_address_o.active = "no"
        GUI.im205.label.avd_thd.active = "no"
        GUI.im205.label.tf_gain.active = "no"
        GUI.im205.label.clk_trim.active = "no"
        GUI.im205.list.clk_trim.active = "no"
    end
end

GUI.im205.active = function(active)
    if active == "yes" or active == "YES" then
        GUI.im205.toggle.i2c_addr.active = "yes"
        GUI.im205.toggle.vpump.active = "yes"
        GUI.im205.toggle.avd_thd.active = "yes"
        GUI.im205.toggle.tf_gain.active = "yes"
        GUI.im205.toggle.clk_trim.active = "yes"
        GUI.im205.list.i2c_protocol.active = "yes"
        GUI.im205.list.i2c_address.active = "yes"
        GUI.im205.text.reg_addr_1.active = "yes"
        GUI.im205.text.reg_data_1.active = "yes"
        GUI.im205.button.reg_r_1.active = "yes" 
        GUI.im205.button.reg_w_1.active = "yes"
        GUI.im205.label.addr_s_1.active = "yes"
        GUI.im205.label.data_s_1.active = "yes"
        GUI.im205.text.reg_addr_2.active = "yes"
        GUI.im205.text.reg_data_2.active = "yes"
        GUI.im205.button.reg_r_2.active = "yes" 
        GUI.im205.button.reg_w_2.active = "yes"
        GUI.im205.label.addr_s_2.active = "yes"
        GUI.im205.label.data_s_2.active = "yes"
        GUI.im205.label.i2c_protocol.active = "yes"
        GUI.im205.label.i2c_address.active = "yes"
        GUI.im205.toggle.enable.active = "yes"
        GUI.im205.toggle.i2c_addr.action()
        GUI.im205.toggle.vpump.action()
        GUI.im205.toggle.avd_thd.action()
        GUI.im205.toggle.tf_gain.action()   
    elseif active == "no" or active == "NO" then
        GUI.im205.text.reg_addr_1.active = "no"
        GUI.im205.text.reg_data_1.active = "no"
        GUI.im205.text.reg_addr_2.active = "no"
        GUI.im205.text.reg_data_2.active = "no"
        GUI.im205.text.tf_gain.active = "no"
        GUI.im205.button.reg_r_1.active = "no" 
        GUI.im205.button.reg_w_1.active = "no"
        GUI.im205.button.reg_r_2.active = "no" 
        GUI.im205.button.reg_w_2.active = "no"
        GUI.im205.button.efuse_w.active = "no"
        GUI.im205.button.efuse_r.active = "no"
        GUI.im205.list.i2c_protocol.active = "no"
        GUI.im205.list.i2c_address.active = "no"
        GUI.im205.list.vpump.active = "no"
        GUI.im205.list.i2c_address_o.active = "no"
        GUI.im205.list.avd_thd.active = "no"
        GUI.im205.label.addr_s_1.active = "no"
        GUI.im205.label.data_s_1.active = "no"
        GUI.im205.label.addr_s_2.active = "no"
        GUI.im205.label.data_s_2.active = "no"
        GUI.im205.label.i2c_protocol.active = "no"
        GUI.im205.label.i2c_address.active = "no"
        GUI.im205.label.vpump.active = "no"
        GUI.im205.label.i2c_address_o.active = "no"
        GUI.im205.label.avd_thd.active = "no"
        GUI.im205.label.tf_gain.active = "no"
        GUI.im205.toggle.enable.active = "no"
        GUI.im205.toggle.i2c_addr.active = "no"
        GUI.im205.toggle.vpump.active = "no"
        GUI.im205.toggle.avd_thd.active = "no"
        GUI.im205.toggle.tf_gain.active = "no"
        GUI.im205.toggle.clk_trim.active = "no"
        GUI.im205.label.clk_trim.active = "no"
        GUI.im205.list.clk_trim.active = "no"
    end
end

GUI.im205.active("yes")

GUI.im205.toggle.enable.value = "ON"
GUI.im205.toggle.enable.action()
