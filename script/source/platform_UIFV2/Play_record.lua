--**************************************************--
--Function List:
--  audio_play
--  audio_record
--  audio_stop_play
--  audio_stop_record
--  simulation_2channels_SND_test
--  simulation_2channels_RCV_test
--  simulation_4channels_test
--  sidetone_generate_play_record
--**************************************************--
TEMP_RCD_DC  = ""--TEMP_PATH.."rcd_DC.wav"  --NOAH_PATH.."Vector\\".."rcd_DC.wav"
TEMP_PLAY_DC = ""--NOAH_PATH.."Vector\\".."silence_"..SAMPLE_RATE_S..".wav"
TEMP_RCD_DP  = ""--TEMP_PATH.."rcd_DP.wav"  --NOAH_PATH.."Vector\\".."rcd_DP.wav"
 
NE_FILE = 
{
	"NE_American English.wav",
}

TONE_NOISE = "2k_tone.wav"

--**************************************************--
--Function Name:    audio_play
--Description:      play wave file
--Return Value:     no
--Parameter:        play_audio_dev, play_file, channel, loop_flay, pre_flag
--  play_audio.dev: Indicate play audio device;
--  play_file:      Indicate full path of the wave file that will be played.
--  channel:        Indicate which channel to play.
--                  1 or "left" for left and 2 or "right" for right channel.
--                  0 or "both" for both channels.
--                  (Default value is 0, any invalid input will be regarded as 0)
--  loop_flay:      If this parameter is set as true, the file will be played until
--                  stop_play for corresponding device is called.
--                  (Default value is true)
--  pre_flay:       when false: init the device and play immediately.
--                  when true:  init the device only, the play will be started by
--                  a next Play with init_only set to false.
--                  (Default value is false)
--Usage:            audio_play(AUDIO_DP, "test.wav", 0, false, true) 
--**************************************************--
function audio_play(play_audio_dev, play_file, channel, loop_flay, pre_flag)
    if pre_flag then
        local state = audio.play(play_audio_dev, play_file, channel, loop_flay, true)
        if state then
            printc(play_audio_dev.." prepare for playing. --> "..tostring(state))
        else
            print_error_info("funtion audio.play() ERROR: "..play_audio_dev.." prepare for playing. --> "..tostring(state))
        end
    else
        local state = audio.play(play_audio_dev, play_file, channel, loop_flay, false)
        if state then
            printc(play_audio_dev.." playing. --> "..tostring(state))
        else
            print_error_info("function audio.play() ERROR: "..play_audio_dev.." playing. --> "..tostring(state))
        end
    end
end

--**************************************************--
--Function Name:      audio_record
--Description:        recording
--Return Value: 
--Parameters:         record_audio_dev, record_file, sample_rate, bit, channel, pre_flag
--  record_audio_dev: Input audio device name.
--  record_file:      Indicate the full path and name where the recorded data will
--                    be stored.
--  sample_rate:      Indicate the recording sample rate, the default value is 16K.
--  bit:              Indicate the bits per sample, the default value is 16 bits.
--  channel:          Indicate the channel of recording, the default value is 2.
--  pre_flag:         when false: init the device and record immediately.
--                    when true:  init the device only, the record will be started by
--                    a next Play with init_only set to false.
--                    (Default value is false)
--Usage:              audio_record(AUDIO_DC, "test.wav", 16000, 16, 2, false)
--**************************************************--
function audio_record(record_audio_dev, record_file, sample_rate, bit, channel, pre_flag)
    if pre_flag then
        local state = audio.record(record_audio_dev, record_file, sample_rate, bit, channel, true)
        if state then
            printc(record_audio_dev.." prepare for recording. --> "..tostring(state))
        else
            print_error_info("funtion audio.play() ERROR: "..record_audio_dev.." prepare for recording. --> "..tostring(state))
        end
    else
        local state = audio.record(record_audio_dev, record_file, sample_rate, bit, channel, false)
        if state then
            printc(record_audio_dev.." recording. --> "..tostring(state))
        else
            print_error_info("function audio.play() ERROR: "..record_audio_dev.." recording. --> "..tostring(state))
        end
    end
end

--**************************************************--
--Function Name: audio_stop_play
--Description:   Stop playing for indicated audio device.
--Return Value: 
--Parameter:
-- 
--**************************************************--
function audio_stop_play(play_audio_dev)
    local state = audio.stop_play(play_audio_dev)
    if state then
        printc(play_audio_dev.." stop playing. --> "..tostring(state))
    else
        print_error_info("function audio.play() ERROR: "..play_audio_dev.." stop playing. --> "..tostring(state))
    end
end

--**************************************************--
--Function Name: audio_stop_record
--Description:   Stop recording for indicated audio device.
--Return Value: 
--Parameter:
-- 
--**************************************************--
function audio_stop_record(record_audio_dev)
    local state = audio.stop_record(record_audio_dev)
    if state then
        printc(record_audio_dev.." stop playing. --> "..tostring(state))
    else
        print_error_info("function audio.play() ERROR: "..record_audio_dev.." stop playing. --> "..tostring(state))
    end
end


--**************************************************--
--Function Name: simulation_2channels_SND_test
--Description: define 2channels simulation function, send dirction, input is DC, output is DP.
--Return Value: 
--Parameter:
-- 
--**************************************************--
function simulation_2channels_SND_test(SOURCE_PATH, SOURCE_FILE, RECORD_PATH, RECORD_FILE, t, delta, record_sr, check_power_flag)
    check_power_flag = check_power_flag or 1
    delta = delta or 0
    record_sr = record_sr or SAMPLE_RATE
    local TEMP_FILE = (delta == 0) and SOURCE_PATH..SOURCE_FILE or TEMP_PATH..SOURCE_FILE

    SOURCE_FILE = SOURCE_PATH..SOURCE_FILE
    RECORD_FILE = RECORD_PATH..RECORD_FILE
    
    printc("\n********************")
    printc("Source file is: "..SOURCE_FILE)
    printc("Recording file is: "..RECORD_FILE)

    if delta ~= 0 then
        local wave_data = wave.load(SOURCE_FILE) --wave.load_raw(SOURCE_FILE, 2, SAMPLE_RATE, 16)
        local wave_data_array = wave.n_to_mono(wave_data)
        --wave_data_array[1], saturated = wave.amplify(wave_data_array[1], mic0)
        --wave_data_array[2], saturated = wave.amplify(wave_data_array[2], mic1)
        wave_data_array[1], saturated = wave.amplify(wave_data_array[1], delta)
        wave_data_array[2], saturated = wave.amplify(wave_data_array[2], -delta)
        wave_data = wave.mono_to_n(wave_data_array)
        wave.save(TEMP_FILE, wave_data)
        Wave.clearWavdata(wave_data_array[1])
        Wave.clearWavdata(wave_data_array[2])
        Wave.clearWavdata(wave_data)
    end
    
    audio_record(AUDIO_RECORD_DP, RECORD_FILE, record_sr, 16, 2, true)
    audio_record(AUDIO_RECORD_DC, TEMP_RCD_DC, record_sr, 16, 2, true)
    audio_play(AUDIO_PLAY_DP, TEMP_PLAY_DP, 0, true, true)
    audio_play(AUDIO_PLAY_DC, TEMP_FILE, 0, true, true)
    wait(1)
    printc("Begin to record...")
    audio_record(AUDIO_RECORD_DP, RECORD_FILE, record_sr, 16, 2, false)
    audio_record(AUDIO_RECORD_DC, TEMP_RCD_DC, record_sr, 16, 2, false)
    printc("Begin to play...")
    audio_play(AUDIO_PLAY_DP, TEMP_PLAY_DP, 0, true)
    audio_play(AUDIO_PLAY_DC, TEMP_FILE, 0, true)
    printc("Delay "..tostring(t).."s")
    wait(t)
    audio_stop_play(AUDIO_PLAY_DP)
    audio_stop_play(AUDIO_PLAY_DC)
    audio_stop_record(AUDIO_RECORD_DP)
    audio_stop_record(AUDIO_RECORD_DC)

    os.execute("del "..TEMP_RCD_DC)
    if delta~= 0 then os.execute("del "..TEMP_FILE) end
    printc("Playing and recording end.")
    printc("********************\n")

    if check_power_flag == 1 then
        check_power(RECORD_FILE, t, 0, t/2)
    end
end

--**************************************************--
--Function Name: simulation_2channels_RCV_test
--Description: define 2channels simulation function, receive dirction
--Return Value: 
--Parameter:
-- 
--**************************************************--
function simulation_2channels_RCV_test(SOURCE_PATH, SOURCE_FILE, RECORD_PATH, RECORD_FILE, t, record_sr, check_power_flag)
    check_power_flag = check_power_flag or 1
    record_sr = record_sr or SAMPLE_RATE
    SOURCE_FILE = SOURCE_PATH..SOURCE_FILE
    RECORD_FILE = RECORD_PATH..RECORD_FILE

    printc("\n********************")
    printc("Source file is: "..SOURCE_FILE)
    printc("Recording file is: "..RECORD_FILE)

    audio_record(AUDIO_RECORD_DP, TEMP_RCD_DP, record_sr, 16, 2, true)
    audio_record(AUDIO_RECORD_DC, RECORD_FILE, record_sr, 16, 2, true)
    audio_play(AUDIO_PLAY_DP, SOURCE_FILE, 0, true, true)
    audio_play(AUDIO_PLAY_DC, TEMP_PLAY_DC, 0, true, true)
    wait(1)
    printc("Begin to record...")
    audio_record(AUDIO_RECORD_DP, TEMP_RCD_DP, record_sr, 16, 2, false)
    audio_record(AUDIO_RECORD_DC, RECORD_FILE, record_sr, 16, 2, false)
    printc("Begin to play...")
    audio_play(AUDIO_PLAY_DP, SOURCE_FILE, 0, true)
    audio_play(AUDIO_PLAY_DC, TEMP_PLAY_DC, 0, true)
    printc("Delay "..tostring(t).."s")
    wait(t)
    audio_stop_play(AUDIO_PLAY_DP)
    audio_stop_play(AUDIO_PLAY_DC)
    audio_stop_record(AUDIO_RECORD_DP)
    audio_stop_record(AUDIO_RECORD_DC)
    os.execute("del "..TEMP_RCD_DP)

    printc("Playing and recording end.")
    printc("********************\n")

    if check_power_flag == 1 then
        check_power(RECORD_FILE, t, 0, t/2)
    end
end

--**************************************************--
--Function Name: simulation_4channels_test
--Description: 
--Return Value: 
--Parameter:
-- 
--**************************************************--
function simulation_4channels_test(SOURCE_PATH, SOURCE_FILE_1,SOURCE_FILE_2, RECORD_PATH, RECORD_FILE_1, RECORD_FILE_2, t, MOCKUP, record_sr, check_power_flag)
    check_power_flag = check_power_flag or 1
    record_sr = record_sr or SAMPLE_RATE
    --local record_sr_dp, record_sr_dc = TDM_DUAL_SR_DP, TDM_DUAL_SR_DC
    
    SOURCE_FILE_1 = SOURCE_PATH..SOURCE_FILE_1
    SOURCE_FILE_2 = SOURCE_PATH..SOURCE_FILE_2
    RECORD_FILE_1 = RECORD_PATH..RECORD_FILE_1
    RECORD_FILE_2 = RECORD_PATH..RECORD_FILE_2
    printc("\n********************")
    printc("Mic in:   "..SOURCE_FILE_1)
    printc("Line in:  "..SOURCE_FILE_2)
    printc("Line out: "..RECORD_FILE_1)
    printc("SPK out:  "..RECORD_FILE_2)

    audio_record(AUDIO_RECORD_DP, RECORD_FILE_1, record_sr, 16, 2, true)
    audio_record(AUDIO_RECORD_DC, RECORD_FILE_2, record_sr, 16, 2, true)
    audio_play(AUDIO_PLAY_DP, SOURCE_FILE_2, 0, true, true)
    audio_play(AUDIO_PLAY_DC, SOURCE_FILE_1, 0, true, true)

    wait(1)
    printc("Begin to record...")
    audio_record(AUDIO_RECORD_DP, RECORD_FILE_1, record_sr, 16, 2, false)
    audio_record(AUDIO_RECORD_DC, RECORD_FILE_2, record_sr, 16, 2, false)
    printc("Begin to play...")
    audio_play(AUDIO_PLAY_DP, SOURCE_FILE_2, 0, true, false)
    audio_play(AUDIO_PLAY_DC, SOURCE_FILE_1, 0, true, false)

    printc("Delay "..tostring(t).."s")
    wait(t)
    
    audio_stop_play(AUDIO_PLAY_DP)
    audio_stop_play(AUDIO_PLAY_DC)
    audio_stop_record(AUDIO_RECORD_DP)
    audio_stop_record(AUDIO_RECORD_DC)

    printc("Playing and recording end.")
    printc("********************\n")

    if check_power_flag == 1 then
        check_power(RECORD_FILE_1, t, 0, t/2)
        check_power(RECORD_FILE_2, t, 0, t/2)
    end
end

--**************************************************--
--Function Name: simulation_4channels_test
--Description: 
--Return Value: 
--Parameter:
-- 
--**************************************************--
function simulation_4channels_test_PDM(SOURCE_PATH, SOURCE_FILE_1,SOURCE_FILE_2, RECORD_PATH, RECORD_FILE_1, RECORD_FILE_2, t, MOCKUP, record_sr, check_power_flag)
    check_power_flag = check_power_flag or 1
    record_sr = record_sr or SAMPLE_RATE
    --local record_sr_dp, record_sr_dc = TDM_DUAL_SR_DP, TDM_DUAL_SR_DC
    
    SOURCE_FILE_1 = SOURCE_PATH..SOURCE_FILE_1
    SOURCE_FILE_2 = SOURCE_PATH..SOURCE_FILE_2
    RECORD_FILE_1 = RECORD_PATH..RECORD_FILE_1
    RECORD_FILE_2 = RECORD_PATH..RECORD_FILE_2
    printc("\n********************")
    printc("Mic in:   "..SOURCE_FILE_1)
    printc("Line in:  "..SOURCE_FILE_2)
    printc("Line out: "..RECORD_FILE_1)
    printc("SPK out:  "..RECORD_FILE_2)

    audio_record(AUDIO_RECORD_DP, RECORD_FILE_1, record_sr, 16, 2, true)
    audio_record(AUDIO_RECORD_DC, RECORD_FILE_2, record_sr, 16, 2, true)
    audio_record(AUDIO_RECORD_DC_PDM, TEMP_RCD_DC, record_sr, 16, 2, true)
    audio_play(AUDIO_PLAY_DP, SOURCE_FILE_2, 0, true, true)
    audio_play(AUDIO_PLAY_DC, TEMP_PLAY_DC, 0, true, true)
    audio_play(AUDIO_PLAY_DC_PDM, SOURCE_FILE_1, 0, true, true)

    wait(1)
    printc("Begin to record...")
    audio_record(AUDIO_RECORD_DP, RECORD_FILE_1, record_sr, 16, 2, false)
    audio_record(AUDIO_RECORD_DC, RECORD_FILE_2, record_sr, 16, 2, false)
    audio_record(AUDIO_RECORD_DC_PDM, TEMP_RCD_DC, record_sr, 16, 2, false)
    printc("Begin to play...")
    audio_play(AUDIO_PLAY_DP, SOURCE_FILE_2, 0, true, false)
    audio_play(AUDIO_PLAY_DC, TEMP_PLAY_DC, 0, true, false)
    audio_play(AUDIO_PLAY_DC_PDM, SOURCE_FILE_1, 0, true, false)

    printc("Delay "..tostring(t).."s")
    wait(t)
    
    audio_stop_play(AUDIO_PLAY_DP)
    audio_stop_play(AUDIO_PLAY_DC)
    audio_stop_play(AUDIO_PLAY_DC_PDM)
    audio_stop_record(AUDIO_RECORD_DP)
    audio_stop_record(AUDIO_RECORD_DC)
    audio_stop_record(AUDIO_RECORD_DC_PDM)

    printc("Playing and recording end.")
    printc("********************\n")

    if check_power_flag == 1 then
        check_power(RECORD_FILE_1, t, 0, t/2)
        check_power(RECORD_FILE_2, t, 0, t/2)
    end
end
--**************************************************--
--Function Name: sidetone_generate_play_record
--Description: 
--Return Value: 
--Parameter:
-- 
--**************************************************--
function simulation_2chanels_play_record(SOURCE_PATH, SOURCE_FILE, RECORD_PATH, RECORD_FILE, t, record_sr)
    record_sr = record_sr or SAMPLE_RATE
    SOURCE_FILE = SOURCE_PATH..SOURCE_FILE
    RECORD_FILE = RECORD_PATH..RECORD_FILE

    printc("\n********************")
    printc("Source file is: "..SOURCE_FILE)
    printc("Recording file is: "..RECORD_FILE)
    audio_play(AUDIO_PLAY_PC, SOURCE_FILE, 0, true, true)
    wait(1)
    printc("Begin to record...")

    os.execute(APP_PATH)
    
    printc("Begin to play...")
    audio_play(AUDIO_PLAY_PC, SOURCE_FILE, 0, true)
    printc("Delay "..tostring(t).."s")
    wait(t)
    audio_stop_play(AUDIO_PLAY_PC)
    
    --os.execute("del "..TEMP_RCD_DP)

    printc("Playing and recording end.")
    printc("********************\n")
    check_power(RECORD_FILE, t, 0, t/2)
end


--**************************************************--
--Function Name: sidetone_generate_play_record
--Description: 
--Return Value: 
--Parameter:
-- 
--**************************************************--
function AB_play_record(SOURCE_PATH, SOURCE_FILE, RECORD_PATH, RECORD_FILE, t, record_sr, mic_mask)
    record_sr = record_sr or 48000
    SOURCE_FILE = SOURCE_PATH..SOURCE_FILE
    RECORD_FILE = RECORD_PATH..RECORD_FILE

    mic_mask = mic_mask or "0:0,0:1,0:2,0:3,0:4,0:5"

    printc("\n********************")
    printc("Source file is: "..SOURCE_FILE)
    printc("Recording file is: "..RECORD_FILE)
    --audio_play(AUDIO_PLAY_PC, SOURCE_FILE, 0, true, true)
    --wait(1)
    printc("Begin to play and record...")
    printc("Delay "..tostring(t).."s")
    printc(APP_PATH..APP_NAME)
    os.execute(APP_PATH..APP_NAME.." "..SOURCE_FILE.." "..t.." "..mic_mask.." "..record_sr)
    --os.execute(APP_PATH..APP_NAME.." "..SOURCE_FILE.." "..t.." 0:0,0:1,0:2,0:3,0:4,0:5".." 48000")
    --os.execute(APP_PATH..APP_NAME.." "..SOURCE_FILE.." "..t.." 0:6,0:7,0:8,0:9,0:10,0:11".." 48000")
    --os.execute(APP_PATH..APP_NAME.." "..SOURCE_FILE.." ".."0".." 0:9,0:10,0:11,0:12,0:13,0:14".." 48000")

    printc("Playing and recording end.")
    printc("********************\n")
    control.stop()
    
    --collectgarbage("collect")
    --check_power(RECORD_FILE, 5, 0, t/2)
    collectgarbage("collect")
end

