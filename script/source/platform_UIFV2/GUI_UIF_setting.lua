--require(SCRIPT_PATH.."init_GUI")
--
GUI.uif_setting = {}

GUI.uif_setting.constant =
{
    sab_fw = NOAH_PATH.."Driver\\SAB\\SAB_V1.0.bin";
}
GUI.uif_setting.label = 
{
    info = iup.label{size="390x32";title = [==[]==]};
}

GUI.uif_setting.button =
{
    tuner = iup.button
    {
        size="80x24", title = "Tuner", active = "NO",
        action = function() 
            CLIENT_MODE = "Client"; 
            if not check_task("Tuner.exe") then
                os.execute("start "..NOAH_PATH.."Tuner\\Tuner.exe");
            else
                message("Warning", "Tuner is running or not yet terminated!")
            end;
        end
    };
    waveview = iup.button
    {
        size="80x24", title = "Wave Viewer",
        action = function() 
            if not check_task("WaveWindow.exe") then
                os.execute("start "..NOAH_PATH.."WaveViewer\\WaveWindow.exe")
            else
                message("Warning", "WaveViewer is running or not yet terminated!")
            end;
        end
    };
    waveedit = iup.button
    {
        size="80x24", title = "Wave Editor", active = "NO",
        --action = function() os.execute("start "..NOAH_PATH.."WaveEditor\\WaveEditor.exe") end
        action = function() 
            if not check_task("WaveEditor.exe") then
                os.execute("start "..NOAH_PATH.."WaveEditor\\WaveEditor.exe")
            else
                message("Warning", "WaveEditor is running or not yet terminated!")
            end;
        end
    };
    start_p_r = iup.button
    {
        size="80x24", title = "Start Play&Record",
    };
    stop_p_r = iup.button
    {
        size="80x24", title = "Stop",
        action = function() if HW_USB_CONNECTION_STATE == 1 then stop_play_record() end end
    };
    update = iup.button
    {
        size="100x", title = "Update Setting *",
    };
    update_fw = iup.button
    {
        size="100x", title = "Update Firmware", active = "yes"--(UIF.board_version == "SAB") and "yes" or "no";
    };
    save = iup.button
    {
        size="80x", title = "Save Setting", active = "no";
    };
    saveas = iup.button
    {
        size="80x", title = "Save As...", active = "no";
    };
    back = iup.button
    {
        size="100x", title = "Back to Main Page", multiple = "Yes", tip = "Back to design manage",
    };
    info = iup.button
    {
        size="100x", title = "System Information",
    };
    signal_path = iup.button
    {
        size="100x", title = "Signal Path Setting",
    };
    audio_bus = iup.button
    {
        size="100x", title = "Audio Bus Setting",
    };
    command_bus = iup.button
    {
        size="100x", title = "Command Bus Setting",
    };
};

GUI.uif_setting.toggle = 
{
    edit = iup.toggle{title="Active Setting Edit",value="OFF"};
}

GUI.uif_setting.time = 
{
    _1s = iup.timer{time=1000, run="NO"}
}

GUI.uif_setting.local_frame = 
{
    tool = iup.frame
    {
        iup.hbox
        {
            GUI.uif_setting.button.tuner;
            GUI.uif_setting.button.waveedit;
            GUI.uif_setting.button.waveview;
            GUI.uif_setting.button.start_p_r; 
            margin="10x10", gap="31x10",alignment = "ACENTER",
        };
        title = "Tools",margin="0x5",alignment = "ACENTER",
    };
    control = iup.frame
    {
        iup.vbox
        {
        --    GUI.uif_setting.label.info;
            iup.hbox
            {
                --iup.hbox
               -- {
                    GUI.uif_setting.button.info;
                    GUI.uif_setting.button.update_fw;
                 --   margin="42x0", gap="45x0",alignment = "ACENTER",
                --};
                --GUI.uif_setting.button.back;
                GUI.uif_setting.button.update;
                margin="30x10", gap="38x5",alignment = "ACENTER",
            };

            iup.hbox
            {
                GUI.uif_setting.button.signal_path;
                GUI.uif_setting.button.audio_bus;
                GUI.uif_setting.button.command_bus;
                margin="30x10", gap="38x5",alignment = "ACENTER",
            };
            
            iup.hbox
            {
                iup.hbox
                {
                    GUI.uif_setting.toggle.edit;
                    GUI.uif_setting.button.save;
                    GUI.uif_setting.button.saveas;
                    margin="0x0", gap="10x0",alignment = "ACENTER",
                };
                --GUI.uif_setting.button.update;
                GUI.uif_setting.button.back;
                margin="10x10", gap="40x5",alignment = "ACENTER",
            };
            margin="0x0", gap="0x0",alignment = "LCENTER",
        };
        title = "UIF HW Setting Information and Control",margin="0x5",alignment = "ACENTER",
    };
    --[[
    uif_hw = iup.frame
    {
        iup.hbox
        {
            GUI.uif_setting.button.signal_path;
            GUI.uif_setting.button.audio_bus;
            GUI.uif_setting.button.command_bus;
            margin="25x10", gap="40x10",alignment = "ACENTER",
        };
        title = "UIF HW Control",margin="0x5",alignment = "ACENTER",
    };
    ]]
}

GUI.uif_setting.dialog = 
{
    signal_path = iup.dialog
    {
        iup.hbox{GUI.ab_setting.frame};
        title="Signal Path Setting", margin="5x5", gap="5" --size="450x420", dropdown="YES"
    };
    audio_bus = iup.dialog
    {
        iup.hbox{GUI.audio_path.frame};
        title="Audio Path Setting", margin="5x5", gap="5" --size="450x420", dropdown="YES"
    };
    command_bus = iup.dialog
    {
        iup.hbox{GUI.com_path.frame};
        title="Command Path Setting", margin="5x5", gap="5" --size="450x420", dropdown="YES"
    };
}

if DSP_NAME == "FM1388" then
    GUI.uif_setting.dialog.main = iup.dialog
    {
        iup.hbox
        {
        --[[
            iup.vbox
            {
                --iup.hbox{GUI.com_path.frame};
                iup.hbox{GUI.ab_setting.frame};
                margin="5x5", gap="5x0",alignment = "ACENTER",
            };
            ]]
            iup.vbox
            {
                --iup.hbox{GUI.audio_path.frame};
                iup.hbox{GUI.uif_setting.local_frame.control;};
                iup.hbox{GUI.uif_setting.local_frame.tool;};
                margin="5x5", gap="5x0",alignment = "LCENTER",
            };
            margin="0x0", gap="0x0",alignment = "LCENTER", 
        };
        title="", margin="5x5", gap="5" --size="450x420", dropdown="YES"
    };
elseif DSP_NAME == "iM501" then
    GUI.uif_setting.dialog.main = iup.dialog
    {
        iup.vbox
        {
            iup.hbox{GUI.VID.frame;};
            --iup.hbox{GUI.uif_setting.local_frame.uif_hw;};
            iup.hbox{GUI.uif_setting.local_frame.control;};
            iup.hbox{GUI.uif_setting.local_frame.tool;};
            margin="5x5", gap="5x0",alignment = "LCENTER",
        };
        title="", margin="5x5", gap="5" --size="450x420", dropdown="YES"
    }
end

function GUI.uif_setting.toggle.edit:action()
    print(GUI.uif_setting.toggle.edit.value)
    if GUI.uif_setting.toggle.edit.value == "ON" then
        if UIF.board_version == "UIF" then
            GUI.audio_path.active("yes")
        else
            GUI.audio_path.active("no")
        end
        GUI.ab_setting.active("yes")
        GUI.uif_setting.button.saveas.active = "yes"
        GUI.uif_setting.button.save.active = "no"
        GUI.uif_setting.toggle.edit.active = "no"
        GUI.uif_setting.button.signal_path.active = "yes"
        GUI.uif_setting.button.audio_bus.active = "yes"
        GUI.uif_setting.button.command_bus.active = "yes"
        GUI.uif_setting.button.update.active = "yes"
    else
        GUI.audio_path.active("no")
        GUI.ab_setting.active("no")
        GUI.uif_setting.button.save.active = "no"
        GUI.uif_setting.button.saveas.active = "no"
        GUI.uif_setting.toggle.edit.active = "yes"
        GUI.uif_setting.button.signal_path.active = "no"
        GUI.uif_setting.button.audio_bus.active = "no"
        GUI.uif_setting.button.command_bus.active = "no"
        GUI.uif_setting.button.update.active = "no"
    end
end


GUI.uif_setting.button.signal_path.action = function()
    GUI.uif_setting.dialog.signal_path:showxy(iup.CENTER, iup.TOP)
    GUI.uif_setting.dialog.audio_bus:hide()
    GUI.uif_setting.dialog.command_bus:hide()
end
GUI.uif_setting.button.audio_bus.action = function()
    GUI.uif_setting.dialog.audio_bus:showxy(iup.CENTER, iup.TOP)
    GUI.uif_setting.dialog.signal_path:hide()
    GUI.uif_setting.dialog.command_bus:hide()
end
GUI.uif_setting.button.command_bus.action = function()
    GUI.uif_setting.dialog.command_bus:showxy(iup.CENTER, iup.TOP)
    GUI.uif_setting.dialog.audio_bus:hide()
    GUI.uif_setting.dialog.signal_path:hide()
end

GUI.uif_setting.button.update.action = function()
    if HW_USB_CONNECTION_STATE == 0 then
        message("Error", "USB Detached. Please check HW connection.")
        return
    end
    GUI.system_server.button.update_com_path.action()
    GUI.system_server.button.update_audio_path.action()
    GUI.system_server.button.update_ab_setting.action()
    GUI.uif_setting.button.update.title = "Update Setting"
    update_audio_setting()
end

GUI.uif_setting.button.update_fw.action = function()
    if HW_USB_CONNECTION_STATE == 0 then
        message("Error", "USB Detached. Please check HW connection.")
        return
    end
    if UIF.board_version ~= "SAB" then 
        message("Warning", "Sorry! Only support SAB FW updating.")
        return
    end
    
    local fd=iup.filedlg{dialogtype="OPEN", title="Load *.bin file", multiplefiles = "NO",
                         nochangedir="YES", directory=NOAH_PATH.."Driver\\SAB\\",value="SAB_V1.0.bin",
                         filter="*.bin", filterinfo="*.bin", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status
 
    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
        if string.find(fd.value, ".bin") == nil then
            message("ERROR", "The file of selected should be wrong, please double check!")
        end
        GUI.uif_setting.constant.sab_fw = fd.value
        printc("BIN file: "..GUI.uif_setting.constant.sab_fw)
        UIF.Updata_FW(GUI.uif_setting.constant.sab_fw)
    end
    fd:destroy()
end


GUI.uif_setting.button.save.action = function()
    printc("Save Design.")
    local save_alarm = iup.Alarm("Alarm", "Setting will be save to design.", "Confirm", "Cancel")
    if save_alarm ~= 1 then return end
    
    local temp_josn = DESIGN_PATH..DSP_NAME.."\\temp.json"
    os.execute("c: > "..temp_josn)
    export_jsonfile(temp_josn, GUI.kernel)

    local select_group = GUI.design_manage.list.group.value+0
    local path = GUI.design_manage.list.group[select_group] == "No Group" and design_path
                 or design_path..GUI.design_manage.list.group[select_group].."\\"
    local select_design, index = get_select_value_from_list(GUI.design_manage.list.design)
    print(path..select_design[1])
    --os.execute("ren \""..path..select_design[1]..".json\" \""..GUI.design_manage.text.new_name.value.."\"")

    os.execute("copy \""..DESIGN_PATH..DSP_NAME.."\\temp.json\"".." \""..path..select_design[1]..".json\" ".."/Y")
    os.execute("del \""..DESIGN_PATH..DSP_NAME.."\\temp.json\"")

    local bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel= update_audio_global_value()
    local note = get_audio_info(bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel)
    modify_note_info(path, select_design[1], note)
    GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[GUI.design_manage.list.group.value+0], 
                                                  get_select_value_from_list(GUI.design_manage.list.design) )

    GUI.uif_setting.button.save.active = "no"
end

GUI.uif_setting.button.saveas.action = function()
    printc("Save As a New Design.")
    local temp_josn = DESIGN_PATH..DSP_NAME.."\\temp.json"
    os.execute("c: > "..temp_josn)
    export_jsonfile(temp_josn, GUI.kernel)
    GUI.design_manage.button.duplicate.action(1)
end

function get_audio_info(bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel)
    local audio_info = ""
    bus_type = bus_type or 2
    if GUI.ab_setting.toggle.ab_in.active == "YES" or GUI.ab_setting.toggle.ab_out.active == "YES" or 
       GUI.ab_setting.toggle.ab_in.value == "ON" or GUI.ab_setting.toggle.ab_out.value == "ON" 
    then
        local bus_type_s = {"PDM", "I2S", "PCM", "TDM"}
        --local audio_bus_type = audio_bus == "PDM" and " Type: "..audio_bus..", " or " Type: "..i2s_type[bus_type]..", "
        local audio_bus_type = " "..bus_type_s[bus_type]..", "
        --sample_rate = "Sample rate: "..tostring(sample_rate).."Hz, "
        --local bit_length = "Bit length: "..tostring(bit_len).."bit, "
        sample_rate = tostring(sample_rate/1000).."KHz, "
        local bit_length = tostring(bit_len).."bit, "
        m_s = m_s == 1 and "Master, " or "Slave, "
        local delay = one_cycle_dely == 1 and "One cycle delay, " or "No delay, "
        bclk_polarity = bclk_polarity == 0 and "BCLK polarity normal(Rising), " or "BCLK polarity invert(Falling), "
        --fram_start = fram_start == 5 and "Frame start rising edge, " or "Frame start falling edge, "
        --bit_order = bit_order == 1 and "MSB, " or "LSB."
        local pdmclko = "PDM clock out: "..tostring(pdm_clk_sel*1.024).."MHz, "
        audio_info = bus_type == 1 and audio_bus_type..pdmclko or audio_bus_type..sample_rate..bit_length..m_s..delay..bclk_polarity --date_latch..fram_start..bit_order
    end
    
    if --GUI.ab_setting.toggle.spi_in.active == "YES" or GUI.ab_setting.toggle.spi_out.active == "YES" or
       GUI.ab_setting.toggle.spi_in.value == "ON" or GUI.ab_setting.toggle.spi_out.value == "ON" 
    then
        sample_rate = tostring(GUI.ab_setting.list.spi_in.sample_rate/1000).."KHz, "
        local input_channel = 0
        for i = 1, #GUI.ab_setting.list.spi_in.value_tab do
            if GUI.ab_setting.list.spi_in.value_tab[i] == 1 then input_channel = input_channel + 1 end
        end
        -- output channels is fixed at 8.
        audio_info = "SPI, "..sample_rate.."8 channels output, "..tostring(input_channel).." channels input."
    end
    return audio_info
end

GUI.uif_setting.button.info.action = function()
    if HW_USB_CONNECTION_STATE == 0 then
        message("Error", "USB Detached. Please check HW connection.")
        return
    end
    
    local audio_info = get_audio_info(AUDIO_BUS_TYPE,SAMPLE_RATE,SAMPLE_BIT_LEN,M_S,ONE_CYCLE_DELAY,BCLK_POLARITY,FRAM_START,BIT_ORDER,PDM_ADC_CLK_SEL)

    local command_type = DL_PATH == 1 and "Type: I2C, " or "Type: SPI, "
    local i2c_speed = "I2C speed: "..tostring(I2C_SPEED).."KHz, "
    local spi_speed = "SPI speed: "..tostring(SPI_SPEED/1000).."MHz, "
    local spi_mode = "SPI mode: mode "..tostring(SPI_MODE)..", "
    local command_speed = DL_PATH == 1 and i2c_speed or spi_speed..spi_mode
    local command_info = command_type..command_speed

    UIF.get_board_ver()
    
    local info = 
    [==[    Board Version  :  ]==]..UIF.version..".\n"..
    [==[    Script Version :  []==]..Version.."].\n\n"..
    [==[    Audio Bus      : ]==]..audio_info.."\n\n"..
    [==[    Comment Bus : ]==]..command_info.."\n"

    message("System Information", info)
end


GUI.uif_setting.button.back.action = function()
    
    if GUI.uif_setting.button.save.active == "YES" then
        local save_alarm = iup.Alarm("Alarm", "Design has been modified, do you want to save these modification?", "Yes", "No", "Cancel")
        if save_alarm == 1 then
            GUI.uif_setting.button.save.action()
        elseif save_alarm ~= 2 then
            return
        end
    end
    GUI.uif_setting.button.save.active = "no"
    GUI.uif_setting.dialog.main:close_cb()
    --[[
    GUI.uif_setting.dialog.main:hide()
    return_ab_setting_active()
    GUI.design_manage.dialog.main:show()
    ]]
end

local save_active = GUI.uif_setting.button.save.active
GUI.uif_setting.button.start_p_r.action = function()
    if PLAY_STATUS == 0 and HW_USB_CONNECTION_STATE ==  1 then

        GUI.VID.button.update_set.action()
        start_play_record(0)
        if SPI_EN then
            UIF.spi_record()
            SPI_RUN = true
        end
        GUI.uif_setting.button.start_p_r.title = "Stop"
        GUI.uif_setting.button.update.active = "no";
        GUI.uif_setting.button.update_fw.active = "no";
        save_active = GUI.uif_setting.button.save.active
        GUI.uif_setting.button.save.active = "no";
        GUI.uif_setting.button.saveas.active = "no";
        GUI.uif_setting.button.back.active = "no"       
    elseif PLAY_STATUS == 1 and HW_USB_CONNECTION_STATE == 1 then
        stop_play_record()
        if SPI_EN then
            iM501.write_message(iM501.message.terminate_vb_transfer, 0, DL_PATH)
            iM501.write_message(iM501.message.spi_record, 0, DL_PATH)
            SPI_RUN = false
        end
        GUI.uif_setting.button.start_p_r.title = "Start Play&Record"
        GUI.uif_setting.button.update.active = "yes";
        GUI.uif_setting.button.update_fw.active = "yes";
        GUI.uif_setting.button.save.active = save_active
        GUI.uif_setting.button.saveas.active = "yes";
        GUI.uif_setting.button.back.active = "yes" 
    else
        message("Error", "USB Detached. Please check HW connection.")
    end
end

function GUI.uif_setting.dialog.main:close_cb()
  --iup.ExitLoop()  -- should be removed if used inside a bigger application
    if PLAY_STATUS == 1 then
        message("Error", "Play&Recording is running, please end them first!")
        GUI.uif_setting.dialog.main:show()
    else
        --GUI.uif_setting.dialog.main:hide()
        return_ab_setting_active()
        GUI.design_manage.dialog.main:show()
        GUI.uif_setting.dialog.main:hide()
        printc("UIF setting window closed")
    end
    return iup.IGNORE
end

--GUI.uif_setting.dialog.main:show()

--GUI.uif_setting.time._1s.run = "YES"
function GUI.uif_setting.time._1s.action_cb()
    print("Time recalled")
    --GUI.im501_vr_setting.time.monitor_timer.time = GUI.im501_vr_setting.text.time_interval.value + 0
    local usb_state = check_USB_state()
    print(usb_state, HW_USB_CONNECTION_STATE)
    
    if usb_state ~= nil and usb_state ~= HW_USB_CONNECTION_STATE then

        local str = GUI.uif_setting.dialog.main.title
        local i, j = string.find(str, "    ")
        str = string.sub(str, 1, j)
    
        if usb_state == 0 then      --detach USB.
            printc("USB Detached.")
            GUI.uif_setting.dialog.main.title = str.."offline"
            --GUI.uif_setting.button.update.active = (UIF.board_version == "SAB") and "OFF" or "ON";
            if PLAY_STATUS == 1 then  --playing when detach USB.
                PLAY_STATUS = 0
                GUI.uif_setting.button.start_p_r.title = "Start Play&Record"
                GUI.uif_setting.button.update_fw.active = "yes";
                GUI.uif_setting.button.update.active = "yes"
                GUI.uif_setting.button.save.active = save_active
                GUI.uif_setting.button.saveas.active = "yes";
                GUI.uif_setting.button.back.active = "yes" 
            end
        elseif usb_state == 1 then
            printc("USB Attached.")
            GUI.uif_setting.dialog.main.title = str.."online"
            UIF.get_board_ver()
            UIF.check_SAB()
            --GUI.uif_setting.button.update.active = (UIF.board_version == "SAB") and "OFF" or "ON";
        end
        
        check_board_function("AB")
        check_board_function("SAB")
        
        HW_USB_CONNECTION_STATE = usb_state
    end
    GUI.uif_setting.time._1s.time = 1000
end

