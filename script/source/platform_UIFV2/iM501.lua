if DL_PATH == nil then
    DL_PATH = iup.Alarm("Interfacee Select", "Please select code download interface", "I2C", "SPI")
end
--Function List:
--[[
iM501.
{
  init(),
  write_reg(),
  read_reg(),
}
]]

--**************************************************--
iM501 = {
    I2C_addr = 0xe2,
    code = --** {start address, bit file}. Pls put "*.dat" or "*.bin" files in the path of CODE_VEC_PATH.
    {
        {0x0FFC0000, "dram0.bin", "dram"},
        {0x0FFE0000, "dram1.bin", "dram"},
        --{0x0FFE2000, "header.bin", "dram"},
        --{0x0FFE2010, "net.bin", "dram"},
        --{0x0FFE67B0, "gram.bin", "dram"},
        {0x10000000, "iram0.bin", "iram"},
    }, 
    fast_code = 
    {
        "fast_iram.bin", "fast_dram.bin"
    },
    eft_code = 
    {
        {0xffdf6a8, "header.bin", "dram"},
        {0, "net.bin",  "dram"},  ----start address base on header.bin.
        {0, "gram.bin", "dram"},
    },
    vec =      --** vec files. Pls put these files in the path of CODE_VEC_PATH.
    {
        default_path = NOAH_PATH.."Code_vec\\iM501\\default\\";
        init  = "init.vec",
        run   = "run.vec",
        reset = "reset.vec",
        power_down_i2c = "power_down_i2c.vec",
        power_down_spi = "power_down_spi.vec",
        power_up = "power_up.vec",
        dsp_command_buf = 0xfffbff0,
        host_command_buf = 0xfffbff8,
        system_config_check = 0xfffbefe,
        mic_configuration = 0xfffbefc,
        keyword_check = 0xfffbef6,
        flag_address = 0xfffff60,
        psap = 0xfffbee0,  ----
        command_check = 0xfffbf08,
    },
    CMD =
    {
        DM_WR     = 0x2B,  --For burst mode, only can be 2 bytes
        DM_RD     = 0x27,  --Normal W/R, can be 1,2,4 bytes. 
        IM_WR     = 0x0D,  --only support 4 bytes R/W for IRAM
        IM_RD     = 0x07,
        REG_WR_1 = 0x48,
        REG_WR_2 = 0x4A,
        REG_RD   = 0x46,  --only support one byte read.
        DM_WR_BST = 0xA8,
        DM_RD_BST = 0xA0,
        IM_WR_BST = 0x88,
        IM_RD_BST = 0x80,
        SPI_IM_RD = 0,
        SPI_DM_RD = 1,
        SPI_REG_RD = 2,
        SPI_IM_WR = 0x4,--4,
        SPI_DM_WR = 0x5,--5,
        SPI_REG_WR = 6,
    },
    GPIO =
    {
        RTC = 0,
        RST = 1,
        IRQ = 2,
        GPO1 = 3,
        GPO2 = 4,
        AVD = 5,
        GPO3 = 6,
        GPO4 = 7,
        SCL_H = 8,
        SDA_H = 9,
    },
    I2C_REG =
    {
        byte_counter = 0x8,
        dsp_onoff = 0x0f,
        interrupt_dsp = 0x10,
        spi_i2c = 0x12,  --bit2 = 1, means I2C mode, default is SPI mode
    },
    SPI_REG =
    {
        dsp_onoff = 0,
        interrupt_dsp = 1,
    },

    EMB_DATA_SIZE = 4000,  --must x8
    EMB_DATA_NUM = 1,
    ATTRI_LOAD_IRAM  = 52,
    ATTRI_LOAD_DRAM  = 51,
    EFT_range = {0xffc0000, 0xfff8d00},
    LOAD_METHOD = 2,  ----Start order write by 1: Script, 2: HOST MCU.
}

iM501.__help = {}

DSP_ONOFF =  DL_PATH == 1 and iM501.I2C_REG.dsp_onoff or iM501.SPI_REG.dsp_onoff
INTERRUPT_DSP = DL_PATH == 1 and iM501.I2C_REG.interrupt_dsp or iM501.SPI_REG.interrupt_dsp
--EFT_PATH = CODE_VEC_PATH

iM501.ldo_setting = 
{ 
    ["0.90V"] = (0*64 + 0x10),
    ["0.95V"] = (1*64 + 0x10),
    ["1.00V"] = (2*64 + 0x10),
    ["1.05V"] = (3*64 + 0x10),
    ["1.10V"] = (4*64 + 0x10), 
    ["1.15V"] = (5*64 + 0x10),
    ["1.20V"] = (6*64 + 0x10),
    ["1.25V"] = (7*64 + 0x10),
    ["1.30V"] = (8*64 + 0x10),
    ["1.35V"] = (9*64 + 0x10),
    ["1.40V"] = (10*64 + 0x10), 
    ["1.45V"] = (11*64 + 0x10),
    ["1.50V"] = (12*64 + 0x10),
}
dofile(SCRIPT_PATH..sub_dirctory.."VID_message.lua")

--*****************
iM501.__help.dsp_status_sync =
[==[
Usage: iM501.dsp_status_sync() 
  Sync DSP on/off register status when change interface between I2C and SPI
  Return: no
]==]
iM501.dsp_status_sync = function()
    local state, dsp_interface = iM501.read_reg(iM501.I2C_REG.spi_i2c, 1, 1)
    if state == false then printc("SPI and I2C status sync ERROR.") return end
    dsp_interface = bit:_and(dsp_interface, 0x4)
    if dsp_interface == 0x4 then   -- If spi_i2c = 4, means in I2C mode, need change to SPI mode.
        local state, dsp_status = iM501.read_reg(iM501.I2C_REG.dsp_onoff, 1, 1)
        iM501.write_reg(iM501.SPI_REG.dsp_onoff, dsp_status, 1, 2)
    else
        local state, dsp_status = iM501.read_reg(iM501.SPI_REG.dsp_onoff, 1, 2)
        iM501.write_reg(iM501.I2C_REG.dsp_onoff, dsp_status, 1, 1)
    end
    wait(0.01)
end

--*****************
iM501.__help.switch_I2C_SPI =
[==[
Usage: iM501.switch_I2C_SPI(interface) 
  Switch RAM W/R interface between I2C and SPI
  Param: interface: 1:I2C, 2:SPI
  Return: switch: 0: no change, 2: from SPI to I2C, 1: from I2C to SPI.
]==]
iM501.switch_I2C_SPI = function(interface)
    local switch = 0
    if interface == 0 then return 0 end

    DSP_ONOFF =  interface == 1 and iM501.I2C_REG.dsp_onoff or iM501.SPI_REG.dsp_onoff
    INTERRUPT_DSP = interface == 1 and iM501.I2C_REG.interrupt_dsp or iM501.SPI_REG.interrupt_dsp
    
    local state, dsp_interface = iM501.read_reg(iM501.I2C_REG.spi_i2c, 1, 1)
    if state == false then printc("SPI and I2C switch ERROR.") return end
    --if dsp_interface == 0xff then iup.Message("ERROR", "DSP's status is wrong") return 0 end
    if dsp_interface == 0xff then printc("ERROR, ".."DSP's status is wrong") return 0 end
    dsp_interface = bit:_and(dsp_interface, 0x4)
    if dsp_interface == 0x4 and interface == 2 then   --Switch interface from I2C to SPI.
        printc("Switch interface from I2C to SPI.")
        local state, dsp_status = iM501.read_reg(iM501.I2C_REG.dsp_onoff, 1, 1)
        if state == false then printc("SPI and I2C switch ERROR.") return end
        iM501.write_reg(iM501.SPI_REG.dsp_onoff, dsp_status, 1, 2)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 8+SPI_MODE, 1, 1)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, SPI_MODE, 1, 1)
        switch = 1
    elseif dsp_interface == 0 and interface == 1 then   --Switch interface from SPI to I2C.
        printc("Switch interface from SPI to I2C.")
        local state, dsp_status = iM501.read_reg(iM501.SPI_REG.dsp_onoff, 1, 2)
        if state == false then printc("SPI and I2C switch ERROR.") return end
        iM501.write_reg(iM501.I2C_REG.dsp_onoff, dsp_status, 1, 1)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 4, 1, 1)
        switch = 2
    end
    wait(0.01)
    
    return switch
end


--*****************
iM501.__help.reset =
[==[
Usage: iM501.reset() 
  Reset DSP, pull low reset pin, then wait 0.2s, then pull high.
  Param:  type. 1: HW reset; 2, SW reset; 3, both HW and SW. default is 1.
  Return: no
]==]
iM501.reset = function(type)
    UIF.set_interface(UIF.type.CHIP_TYPE, 0, UIF.attribute.CHIP_TYPE_IM501)
    type = type or 1
    DSP_ONOFF =  DL_PATH == 1 and iM501.I2C_REG.dsp_onoff or iM501.SPI_REG.dsp_onoff
    INTERRUPT_DSP = DL_PATH == 1 and iM501.I2C_REG.interrupt_dsp or iM501.SPI_REG.interrupt_dsp

    if type == 1 or type == 3 then
        printc("\nPin reset iM501")
        --UIF.set_gpio(iM501.GPIO.AVD,0)
        --wait(0.1)
        UIF.set_gpio(iM501.GPIO.RST,0)
        wait(0.2)
        UIF.set_gpio(iM501.GPIO.RST,1)
        wait(0.2)
    end
    if type == 2 or type == 3 then
        printc("SW reset iM501")
        iM501.write_reg(DSP_ONOFF, 0x07, 1, DL_PATH)
    end
    wait(0.2)
    iM501.write_reg(DSP_ONOFF, 0x05, 1, DL_PATH)
    if DL_PATH == 2 and SPI_MODE ~= 0 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 8+SPI_MODE, 1, 1)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, SPI_MODE, 1, 1)
    elseif DL_PATH == 1 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 0x4)
    end
end

--*****************
iM501.__help.read_reg =
[==[
Usage: iM501.read_reg(addr, length, interface) 
  Read host register, then return the value.
  Param: 
    addr:    the register's address that will been read.
    length:  data length, 1 or 2, default is 1 byte.
    interface: access interface, 1 or 2, means I2C or SPI. default is 1: I2C.
  Return:    register's data.
]==]
iM501.read_reg = function(reg_addr, length, interface)
    --interface = (interface or DL_PATH) or 1
    interface = interface or 1
    if interface == 1 and reg_addr > 0x13 then printc("Address is out of the range of I2C Reg.") return false,"Address is out of the range of I2C Reg." end
    if interface == 2 and reg_addr > 0x3 then printc("Address is out of the range of SPI Reg.") return false,"Address is out of the range of SPI Reg." end

    local reg_type = "I2C"
    local rw_cmd = iM501.CMD.REG_RD
    local type = UIF.type.I2C
    if interface == 2 then
        if reg_addr > 2 then printc("Register address is out of the range of SPI register.") return end
        rw_cmd = iM501.CMD.SPI_REG_RD
        type = UIF.type.SPI
        reg_type = "SPI"
    end
    length = length or 1
    --print(iM501.I2C_addr, rw_cmd, reg_addr)
    local value = UIF.raw_read(type, iM501.I2C_addr, 1, {rw_cmd, reg_addr})
    if value == false then printc("Register read ERROR.") return false, "nil" end
    local value1 = {}

    if length == 2 then  --Just support 1byte REG read. So if you want read two bytes, read twice.
        value1 = UIF.raw_read(type, iM501.I2C_addr, 1, {rw_cmd, reg_addr+1})
        if value1 == false then printc("Register read ERROR.") return false, "nil" end
        value[1] = value1[1]*256+value[1]
    end

    printc("Read "..reg_type.." Register "..string.format("0x%0x", reg_addr).." = "..string.format("0x%0x", value[1]))
    return true, value[1]
end

--*****************
iM501.__help.read_dram =
[==[
Usage: iM501.read_dram(addr, length, interface)
  Read DRAM, then return the value.
  Param: 
    addr: the DRAM's address that will been read. Range:0x0ffc0000-0x0fffffff
    length:  data length, 2 or 4, default is 4.
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: DRAM's data. 
]==]
iM501.read_dram = function(addr, length, interface)
    if addr < 0x0ffc0000 or addr > 0x0fffffff then printc("Address is out of the range of DRAM.") return false,"Address is out of the range of DRAM."end
    interface = (interface or DL_PATH) or 1
    local interface_type = "I2C"
    length = length or 4

    local rw_cmd = iM501.CMD.DM_RD
    --local addrbyte1 = math.floor(addr/(256*256*256))
    local addrbyte2 = (math.floor(addr/(256*256)))%256
    local addrbyte3 = (math.floor(addr/256))%256    
    local addrbyte4 =  bit:_and(addr%256, 0xfe)
    local dat = {rw_cmd, addrbyte4, addrbyte3, addrbyte2}

    --local switch = iM501.switch_I2C_SPI(interface)
    local a,b,c,d,e = 0,0,0,0,0
    if interface == 2 then
        interface_type = "SPI"
        dat = {iM501.CMD.SPI_DM_RD, bit:_and(addrbyte4, 0xfc), addrbyte3, addrbyte2, 2, 0}
        dat = UIF.raw_read(UIF.type.SPI, 0, 4, dat)
        --print(dat)
        if dat == false then printc("DRAM read ERROR.") return false, "nil" end
        d,c,b,a = dat[4] or 0,dat[3] or 0, dat[2] or 0, dat[1] or 0
    else
        UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, dat) 
        local value4 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xd})
        if value4 == false then printc("DRAM read ERROR.") return false, "nil" end
        local value3 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xc})
        if value3 == false then printc("DRAM read ERROR.") return false, "nil" end
        local value2 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xb})
        if value2 == false then printc("DRAM read ERROR.") return false, "nil" end
        local value1 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xa})
        if value1 == false then printc("DRAM read ERROR.") return false, "nil" end
        d,c,b,a = value4[1],value3[1],value2[1],value1[1]
    end
   -- print(d,c,b,a)
    printc("Read DRAM "..string.format("0x%08x", addr).." via "..interface_type)
    printc("\n")
    if bit:_and(addrbyte4, 0x3) == 0 then
        if length == 4 then
            printc(string.format("databyte4=0x%02x  " ,d))
            printc(string.format("databyte3=0x%02x  " ,c))
            e = d*256+c
        end
        printc(string.format("databyte2=0x%02x  " ,b))
        printc(string.format("databyte1=0x%02x  " ,a))
        e = e*256*256+b*256+a
    elseif bit:_and(addrbyte4, 0x3) == 2 then
        printc("Address is not multiple of 4, in this case only 2 bytes read.")
        printc(string.format("databyte2=0x%02x  " ,d))
        printc(string.format("databyte1=0x%02x  " ,c))
        e = d*256+c
    else
        printc("Address is not multiple of 2.")
        printc(string.format("databyte2=0x%02x  " ,d))
        printc(string.format("databyte1=0x%02x  " ,c))
        e = d*256+c
    end
    printc("\n")
    --iM501.switch_I2C_SPI(switch)
    return true, e
end

--*****************
iM501.__help.read_iram =
[==[
Usage: iM501.read_iram(addr, interface) 
  Read IRAM, then return the value.
  Param: 
    addr: the IRAM's address that will been read. Range:0x10000000-0x1001ffff. Address must multiple of 4
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: IRAM's data. 4 Bytes.
]==]
iM501.read_iram = function(addr, interface)
    if addr < 0x10000000 or addr > 0x1001ffff then printc("Address is out of the range of IRAM.") return false,"Address is out of the range of IRAM." end
    local rw_cmd = iM501.CMD.IM_RD
    interface = (interface or DL_PATH) or 1
    local interface_type = "I2C"
    --local addrbyte1 = math.floor(addr/(256*256*256))
    local addrbyte2 = (math.floor(addr/(256*256)))%256
    local addrbyte3 = (math.floor(addr/256))%256    
    local addrbyte4 =  bit:_and(addr%256, 0xfe)
    local dat = {rw_cmd, addrbyte4, addrbyte3, addrbyte2}
    --local switch = iM501.switch_I2C_SPI(interface)
    local a,b,c,d,e = 0,0,0,0,0
    
    if interface == 2 then
        interface_type = "SPI"
        dat = {iM501.CMD.SPI_IM_RD, bit:_and(addrbyte4, 0xfc), addrbyte3, addrbyte2, 2, 0}
        dat = UIF.raw_read(UIF.type.SPI, 0, 4, dat)
        if dat == false then printc("IRAM read ERROR.") return false, "nil" end
        d,c,b,a = dat[4],dat[3],dat[2],dat[1]
    else 
        UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, dat)  
        local value4 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xd})
        if value4 == false then printc("IRAM read ERROR.") return false, "nil" end
        local value3 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xc})
        if value3 == false then printc("IRAM read ERROR.") return false, "nil" end
        local value2 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xb})
        if value2 == false then printc("IRAM read ERROR.") return false, "nil" end
        local value1 = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, 1, {iM501.CMD.REG_RD, 0xa})
        if value1 == false then printc("IRAM read ERROR.") return false, "nil" end
        d,c,b,a = value4[1],value3[1],value2[1],value1[1]
    end
    
    printc("Read IRAM "..string.format("0x%08x", addr).." via "..interface_type)
    if bit:_and(addrbyte4, 0x3) == 0 then
        printc(string.format("databyte4=0x%02x  " ,d))
        printc(string.format("databyte3=0x%02x  " ,c))
        printc(string.format("databyte2=0x%02x  " ,b))
        printc(string.format("databyte1=0x%02x  " ,a))
        e = d*256*256*256+c*256*256+b*256+a
    elseif bit:_and(addrbyte4, 0x3) == 2 then
        printc("Address is not multiple of 4, in this case only 2 bytes read.")
        printc(string.format("databyte2=0x%02x  " ,d))
        printc(string.format("databyte1=0x%02x  " ,c))
        e = d*256+c
    else
        printc("Address is not multiple of 2.")
        printc(string.format("databyte2=0x%02x  " ,d))
        printc(string.format("databyte1=0x%02x  " ,c))
        e = d*256+c
    end
    --iM501.switch_I2C_SPI(switch)
    return true, e
end

--*****************--
iM501.__help.read_dram_burst =
[==[
Usage: iM501.read_dram_burst(addr, data_length, interface)
  Burst read DRAM.
  Param: 
    addr:        the start address of DRAM that will been readed.
    data_length: date length will be readed.
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: no
]==]
iM501.read_dram_burst = function(addr, data_length, interface)
    printc("Start address: "..string.format("0x%08x", addr), DEBUG)
    printc("Data length  : "..string.format("%d", data_length), DEBUG)    
    interface = (interface or DL_PATH) or 1
    --local switch = iM501.switch_I2C_SPI(interface)
    printc("Interface type :"..(interface == 1 and "I2C" or "SPI"), DEBUG)

    local dat = {}
    local addrbyte2 = (math.floor(addr/(256*256)))%256
    local addrbyte3 = (math.floor(addr/256))%256    
    local addrbyte4 = addr %256

    if interface == 2 then
        dat = {iM501.CMD.SPI_DM_RD, addrbyte4, addrbyte3, addrbyte2, math.floor((data_length/2))%256, math.floor((data_length/2)/256)}
        dat = UIF.raw_read(UIF.type.SPI, 0, data_length, dat)
        if dat == false then printc("DRAM burst read ERROR.") return false, {} end
    else
        UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.REG_WR_2, 08, math.floor(data_length)%256, math.floor(data_length/256)})
        UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.DM_RD, addrbyte4, addrbyte3, addrbyte2})
        --UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.DM_RD_BST})
        dat = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, data_length, {iM501.CMD.DM_RD_BST})
        if dat == false then printc("DRAM burst read ERROR.") return false, {} end
    end

    printc("DRAM Burst Read Successful!", DEBUG)
    --iM501.switch_I2C_SPI(switch)
    return dat
end

--*****************--
iM501.__help.read_iram_burst =
[==[
Usage: iM501.read_iram_burst(addr, data_length, interface)
  Burst read IRAM.
  Param: 
    addr:        the start address of IRAM that will been readed.
    data_length: date length will be readed.
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: no
]==]
iM501.read_iram_burst = function(addr, data_length, interface)
    printc("Start address: "..string.format("0x%08x", addr), DEBUG)
    printc("Data length  : "..string.format("%d", data_length), DEBUG)    
    interface = (interface or DL_PATH) or 1
    --local switch = iM501.switch_I2C_SPI(interface)
    printc("Interface type :"..(interface == 1 and "I2C" or "SPI"), DEBUG)

    local dat = {}
    local addrbyte2 = (math.floor(addr/(256*256)))%256
    local addrbyte3 = (math.floor(addr/256))%256    
    local addrbyte4 = addr %256
    
    if interface == 2 then
        dat = {iM501.CMD.SPI_IM_RD, addrbyte4, addrbyte3, addrbyte2, math.floor((data_length/2))%256, math.floor((data_length/2)/256)}
        dat = UIF.raw_read(UIF.type.SPI, 0, data_length, dat)
        if dat == false then printc("IRAM burst read ERROR.") return false, {} end
    else
        UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.REG_WR_2, 08, data_length%256, math.floor(data_length/256)})
        UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.IM_RD, addrbyte4, addrbyte3, addrbyte2})
        --UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.DM_RD_BST})
        dat = UIF.raw_read(UIF.type.I2C, iM501.I2C_addr, data_length, {iM501.CMD.IM_RD_BST})
        if dat == false then printc("IRAM burst read ERROR.") return false, {} end
    end
   
    printc("IRAM Burst Read Successful!", DEBUG)
    --iM501.switch_I2C_SPI(switch)
    return dat
end

--*****************
iM501.__help.write_reg =
[==[
Usage: iM501.write_reg(reg_addr, value, length, interface)
  Write host register.
  Param: 
    addr:   the host register's address that will been written.
    value:  date of written.
    length: date length of written. 1 or 2. default is 1.
    interface: access interface, 1 or 2, means I2C or SPI. default is 1.
  Return: true or false
]==]
iM501.write_reg = function(reg_addr, value, length, interface)
    interface = interface or 1
    if interface == 1 and reg_addr > 0x13 then printc("Address is out of the range of I2C Reg.") return false,"Address is out of the range of I2C Reg." end
    if interface == 2 and reg_addr > 0x3 then printc("Address is out of the range of SPI Reg.") return false,"Address is out of the range of SPI Reg." end
    --interface = (interface or DL_PATH) or 1
    local reg_type = "I2C"
    length = length or 1
    local rw_cmd = iM501.CMD.REG_WR_1
    local type = UIF.type.I2C
    if interface == 2 then
        if reg_addr > 2 then printc("Register address is out of the range of SPI register.") return end
        rw_cmd = iM501.CMD.SPI_REG_WR
        type = UIF.type.SPI
        length = 1
        reg_type = "SPI"
    end
    local dat = {rw_cmd, reg_addr, value%256}

    if length == 2 and interfave == 1 then
        rw_cmd = iM501.CMD.REG_WR_2
        dat = {rw_cmd, reg_addr, value%256, math.floor(value/256)}
    end
    printc("Write "..reg_type.." register "..string.format("0x%02x", reg_addr).." = "..string.format("0x%0x", value))
    local state = UIF.raw_write(type, iM501.I2C_addr, dat)
    if state == false then printc("Write Error") end
    --[[
    local state = iM501.read_reg(reg_addr, length, interface)
    if state == value then
        printc("Write "..reg_type.." register successful.") 
    else
        printc("Write "..reg_type.." register error.")
    end
    ]]
    return
end

--*****************
iM501.__help.write_dram =
[==[
Usage: iM501.write_dram(addr, value, interface)
  Write DRAM.
  Param: 
    addr:   the DRAM's address that will been written.
    value:  date of written. 2 byte.
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: true or false
]==]
iM501.write_dram = function(addr, value, interface)
    if addr < 0x0ffc0000 or addr > 0x0fffffff then printc("Address is out of the range of DRAM.") return false,"Address is out of the range of DRAM." end

   --local addrbyte1 = math.floor(addr/(256*256*256))
    local addrbyte2 = (math.floor(addr/(256*256)))%256
    local addrbyte3 = (math.floor(addr/256))%256    
    local addrbyte4 =  addr %256
    local rw_cmd = iM501.CMD.DM_WR
    interface = (interface or DL_PATH) or 1
    local interface_type = "I2C"
    --local switch = iM501.switch_I2C_SPI(interface)
    local type = UIF.type.I2C
    local dat = {rw_cmd, addrbyte4, addrbyte3, addrbyte2, value%256, math.floor(value/256)}

    if interface == 2 then
        interface_type = "SPI"
        rw_cmd = iM501.CMD.SPI_DM_WR
        type = UIF.type.SPI
        dat = {rw_cmd, addrbyte4, addrbyte3, addrbyte2, 1, 0, value%256, math.floor(value/256)}
    end
    printc("Write DRAM "..string.format("0x%08x", addr).." = "..string.format("0x%04x", value).." via "..interface_type)
 
    local state = UIF.raw_write(type, iM501.I2C_addr, dat)
    if state == true then
        printc("Write DRAM successful.") 
    else
        printc("Write DRAM error.")
    end
    --iM501.switch_I2C_SPI(switch)
    return state
end

--*****************
iM501.__help.write_dsp =
[==[
Usage: iM501.write_message(cmd, dat, interface)
  Write DSP.
  Param: 
    cmd:  to DSP command.
    dat:  command data.
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: true or false
]==]
iM501.write_message = function(cmd, dat, interface)
    interface = (interface or DL_PATH) or 1

    DSP_ONOFF =  interface == 1 and iM501.I2C_REG.dsp_onoff or iM501.SPI_REG.dsp_onoff
    INTERRUPT_DSP = interface == 1 and iM501.I2C_REG.interrupt_dsp or iM501.SPI_REG.interrupt_dsp

    cmd =  bit:_or(bit:_lshift(cmd,2), 0x1) --(cmd << 2 )or 0x01
    --print(cmd,string.format("0x%0x",dat))
    iM501.write_dram(iM501.vec.dsp_command_buf, dat%0x10000,interface)
    iM501.write_dram(iM501.vec.dsp_command_buf+2, bit:_or(math.floor(dat/0x10000) % 0x10000,0x8000),interface) --read back = 0, means OK.
	iM501.write_dram(iM501.vec.dsp_command_buf+4, math.floor(dat/0x10000/0x10000) % 0x10000,interface)
	iM501.write_dram(iM501.vec.dsp_command_buf+6, math.floor(dat/0x10000/0x10000/0x10000),interface)
    iM501.write_reg(INTERRUPT_DSP, cmd, 1, interface)

    local state, value = iM501.read_dram(iM501.vec.dsp_command_buf+2, 2,interface)
    if state == true and bit:_and(value,0x8000) == 0 then
        printc("Write DSP successful.") 
    else
        printc("Write DSP error.")
    end
    return state
end

--*****************
iM501.__help.write_iram =
[==[
Usage: iM501.write_iram(addr, value, interface)
  Write IRAM.
  Param: 
    addr:   the IRAM's address that will been written.
    value:  date of written. 4 byte.
    interface: access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: true or false
]==]
iM501.write_iram = function(addr, value, interface)
    if addr < 0x10000000 or addr > 0x1001ffff then printc("Address is out of the range of IRAM.") return false,"Address is out of the range of IRAM." end
    local rw_cmd = iM501.CMD.IM_WR
    --local addrbyte1 = math.floor(addr/(256*256*256))
    local addrbyte2 = (math.floor(addr/(256*256)))%256
    local addrbyte3 = (math.floor(addr/256))%256    
    local addrbyte4 =  addr %256
    local databyte4 = math.floor(value/(256*256*256))
    local databyte3 = (math.floor(value/(256*256)))%256
    local databyte2 = (math.floor(value/256))%256    
    local databyte1 =  value %256
    local dat = {rw_cmd, addrbyte4, addrbyte3, addrbyte2, databyte1, databyte2, databyte3, databyte4}
    interface = (interface or DL_PATH) or 1
    local type = UIF.type.I2C
    local interface_type = "I2C"
    --local switch = iM501.switch_I2C_SPI(interface)

    if interface == 2 then
        interface_type = "SPI"
        rw_cmd = iM501.CMD.SPI_IM_WR
        type = UIF.type.SPI
        dat = {rw_cmd, addrbyte4, addrbyte3, addrbyte2, 2, 0, databyte1, databyte2, databyte3, databyte4}
    end
    printc("Write IRAM "..string.format("0x%08x", addr).." = "..string.format("0x%08x", value).." via "..interface_type)

    local state = UIF.raw_write(type, iM501.I2C_addr, dat)
    if state == true then
        printc("Write IRAM successful.") 
    else
        printc("Write IRAM error.")
    end

    --iM501.switch_I2C_SPI(switch)
    return state
end

--*****************
iM501.__help.write_iram_burst =
[==[
Usage: iM501.write_iram_burst(addr, data_tab)
  Burst write IRAM.
  Param: 
    addr:      the start address of IRAM that will been written.
    data_tab:  date table will be written. must be x8
  Return: no
]==]
iM501.write_iram_burst = function(addr, data_tab)
    --local data_tab = iM501.code_convert(code_file, "iram")
    local length = (#data_tab-1)*iM501.EMB_DATA_SIZE + #data_tab[#data_tab]
    printc("Start address: "..string.format("0x%08x", addr)..", Data length: "..string.format("%d", length))
    local rw_cmd = iM501.CMD.REG_WR_2
    for i = 1, #data_tab do
        --local addrbyte1 = math.floor(addr/(256*256*256))
        local addrbyte2 = (math.floor(addr/(256*256)))%256
        local addrbyte3 = (math.floor(addr/256))%256    
        local addrbyte4 = addr % 256

        local databyte1 = data_tab[i][1]
        local databyte2 = data_tab[i][2]
        local databyte3 = data_tab[i][3]   
        local databyte4 = data_tab[i][4]            
        local new_tab = {}
        for j = 1, #data_tab[i]-4 do
            new_tab[j] = data_tab[i][j+4]
        end
        --if #data_tab[i] > 256 then printc("Can not support!") stop() end
        if #data_tab[i] <= 4 then
            iM501.write_iram(addr, value)
        else
            --printc("Start address: "..string.format("0x%08x", addr)..",  Data length: "..string.format("%d", #new_tab))
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {rw_cmd, 08, (#data_tab[i]-4)%256, math.floor((#data_tab[i]-4)/256)})
            local dat = {iM501.CMD.IM_WR, addrbyte4, addrbyte3, addrbyte2, databyte1, databyte2, databyte3, databyte4}
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, dat)
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.IM_WR_BST})
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, new_tab)
            addr = addr + #data_tab[i]
        end
    end
    printc("Burst write IRAM successful!")
end

--*****************
iM501.__help.write_dram_burst =
[==[
Usage: iM501.write_dram_burst(addr, data_tab)
  Burst write DRAM.
  Param: 
    addr:     the start address of DRAM that will been written.
    data_tab: date table will be written. must be x8
  Return: no
]==]
iM501.write_dram_burst = function(addr, data_tab)
    --local data_tab = iM501.code_convert(code_file, "dram")
    local length = (#data_tab-1)*iM501.EMB_DATA_SIZE + #data_tab[#data_tab]
    --printc("Start address: "..string.format("0x%08x", addr)..",  Data length: "..string.format("%d", length))
    local rw_cmd = iM501.CMD.REG_WR_2

    for i = 1, #data_tab do
        --local addrbyte1 = math.floor(addr/(256*256*256))
        local addrbyte2 = (math.floor(addr/(256*256)))%256
        local addrbyte3 = (math.floor(addr/256))%256    
        local addrbyte4 = addr %256
        local new_tab = {}

        local databyte1 = data_tab[i][1]
        local databyte2 = data_tab[i][2]
        for j = 1, #data_tab[i]-2 do
            new_tab[j] = data_tab[i][j+2]
        end
        --if #data_tab[i] > 256 then printc("Can not support!") stop() end
        if #data_tab[i] <= 2 then
            write_iM501_DRAM(addr, value)
        else
            --printc("Start address: "..string.format("0x%08x", addr)..",  Data length: "..string.format("%d", #new_tab))
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {rw_cmd, 08, (#data_tab[i]-2)%256, math.floor((#data_tab[i]-2)/256)})
            local dat = {iM501.CMD.DM_WR, addrbyte4, addrbyte3, addrbyte2, databyte1, databyte2}
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, dat)
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, {iM501.CMD.DM_WR_BST})
            UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, new_tab)
            addr = addr + #data_tab[i]
        end
    end
    printc("Burst write DRAM successful!")
end

--*****************
iM501.__help.code_convert =
[==[
Usage: iM501.code_convert(code_file, code_type)
  convert code file to table for download.
  Param: 
    code_file:  code file, support format: "*.bin", "*.dat", "*.exe".
    code_type:  code type, DRAM or IRAM.
  Return: true or false
]==]
iM501.code_convert = function(code_file, code_type)
    local block_size = iM501.EMB_DATA_SIZE
    printc("\nLoading bat file... "..code_file)    
    file = assert(io.open(code_file, "rb"), "function code_convert() ERROR : data file is not exist! "..code_file)
    local str = file:read("*all")
    local data_tab = {}

    local x = math.floor(string.len(str)/block_size)
    --print(string.len(str))
    local y_end = string.len(str)%block_size

    for i = 1, x do
        data_tab[i] = {}
        for j = 1, block_size do
            data_tab[i][j] = string.byte(str, (i-1)*block_size + j)
        end
    end

    if y_end ~= 0 then
        data_tab[x+1] = {}
        local tab_len = 8
        local len = y_end%tab_len
        local z = y_end - len
        for i = 1, z do
            data_tab[x+1][i] = string.byte(str, x*block_size + i)
            --printc(string.format("0x%X", data_tab[x+1][i]))
        end
 
        if len ~= 0 then
            for i = 1, len do
                data_tab[x+1][z+i] = string.byte(str, x*block_size + y_end - len + i)
            end

            --[[
            for i = 1, len do
                data_tab[x+1][z+len-i+1] = string.byte(str, x*block_size + y_end - len + i)
            end
            for i = 1, len do
                print(data_tab[x+1][z+i])
            end
            ]]
        end
    end
    file:close()
    file = nil
--[[
    bin file format is
    07 06 05 04 03 02 01 00
    15 14 13 12 11 10 09 08
    ......
    so need convert the order of data to
    00 01 02 03 04 05 06 07
    08 09 10 11 12 13 14 15
    ...... Right now, DRAM just surport 2 bytes write.
]]
    if code_type ~= "EFT" then
        for i = 1, #data_tab do
            for j = 1, #data_tab[i] do
                if j%8 == 0 then
                    local a,b,c,d = data_tab[i][j-4],data_tab[i][j-5],data_tab[i][j-6],data_tab[i][j-7]
                    data_tab[i][j-7],data_tab[i][j-6],data_tab[i][j-5],data_tab[i][j-4]=
                      data_tab[i][j-0],data_tab[i][j-1],data_tab[i][j-2],data_tab[i][j-3]
                    data_tab[i][j-3],data_tab[i][j-2],data_tab[i][j-1],data_tab[i][j-0] = a,b,c,d
                end
            end
            --[[
            if code_type == "dram" then
                for j = 1, #data_tab[i] do
                    if j%4 == 0 then
                        local a,b = data_tab[i][j-3],data_tab[i][j-2]
                        data_tab[i][j-3],data_tab[i][j-2]= data_tab[i][j-1],data_tab[i][j-0]
                        data_tab[i][j-1],data_tab[i][j-0] = a,b
                    end
                end
            end
            ]]
        end
    end
    collectgarbage("collect")
    return data_tab
end


--*****************
iM501.__help.load_code =
[==[
Usage: iM501.load_code(start_addr, code_file, code_type, interface)
  Download code to DSP RAM.
  Param: 
    start_addr: the start address of IRAM that will been written.
    code_file:  date file will be written. must be x8
    code_type:  code type, DRAM or IRAM.
    interface:  access interface, 1 or 2, means I2C or SPI. default is DL_PATH.
  Return: no
]==]
iM501.load_code = function (start_addr, code_file, code_type, interface)
    interface = (interface or DL_PATH) or 1
    printc("\n******************** Download RAM code")
    local data_tab = iM501.code_convert(code_file, code_type)

    if DL_PATH == 2 then
        local addr = start_addr
        local wr_cmd = iM501.CMD.SPI_DM_WR
        if code_type == "iram" then
            wr_cmd = iM501.CMD.SPI_IM_WR
        elseif code_type == "dram" then
            wr_cmd = iM501.CMD.SPI_DM_WR
        elseif code_type == "EFT" then
            wr_cmd = iM501.CMD.SPI_DM_WR
        else
            assert(false, "RAM type is wrong!")
        end
        for i = 1, #data_tab do
            local tab_len = #data_tab[i]
            local tab = {wr_cmd, addr%0x10000%0x100, math.floor((addr%0x10000)/0x100), math.floor((addr/0x10000)), math.floor((tab_len/2))%0x100, math.floor((tab_len/2)/0x100)}
            for j=1,tab_len do
                table.insert(tab, data_tab[i][j])
            end 
            --printc("Start address: "..string.format("0x%08x", addr)..", Data length: "..string.format("%d", #data_tab[i]))
            UIF.raw_write(UIF.type.SPI, 0, tab)
            addr = addr + #data_tab[i]
        end
    else
        if iM501.LOAD_METHOD == 1 then
            if code_type == "iram" then
                iM501.write_iram_burst(start_addr, data_tab)
            elseif code_type == "dram" then
                iM501.write_dram_burst(start_addr, data_tab)
            elseif code_type == "EFT" then
                iM501.write_dram_burst(start_addr, data_tab)
            else
                assert(false, "RAM type is wrong!")
            end
        elseif iM501.LOAD_METHOD == 2 then
            local addr = start_addr
            if code_type == "iram" then
                UIF.set_interface(UIF.type.I2C, I2C_SPEED, iM501.ATTRI_LOAD_IRAM)
            elseif code_type == "dram" then
                UIF.set_interface(UIF.type.I2C, I2C_SPEED, iM501.ATTRI_LOAD_DRAM)
            elseif code_type == "EFT" then
                UIF.set_interface(UIF.type.I2C, I2C_SPEED, iM501.ATTRI_LOAD_DRAM)
            else
                assert(false, "RAM type is wrong!")
            end
            for i = 1, #data_tab do
                local tab = {addr%0x10000%0x100, math.floor((addr%0x10000)/0x100), math.floor((addr/0x10000))}
                for j=1,#data_tab[i] do
                    table.insert(tab, data_tab[i][j])
                end 
                --printc("Start address: "..string.format("0x%08x", addr)..", Data length: "..string.format("%d", #data_tab[i]))
                UIF.raw_write_fast(UIF.type.I2C, iM501.I2C_addr, tab, iM501.EMB_DATA_NUM)
                addr = addr + #data_tab[i]
            end
    
            if iM501.EMB_DATA_NUM > 1 then
                UIF.raw_write_fast(UIF.type.I2C, iM501.I2C_addr, nil, iM501.EMB_DATA_NUM, true)
            end
            UIF.set_interface(UIF.type.I2C, I2C_SPEED)
        end
    end

--------------------
    if ( Enable_Load_Code_Check ) then
        printc("Read back for download checking.")
        local addr = start_addr
        for i = 1, #data_tab do
            local tab = {}
            local error = 0
           -- print(code_type,addr, #data_tab[i], interface)
            if code_type == "iram" then
                tab = iM501.read_iram_burst(addr, #data_tab[i], interface)
            elseif code_type == "dram" then
                tab = iM501.read_dram_burst(addr, #data_tab[i], interface)
            elseif code_type == "EFT" then
                tab = iM501.read_dram_burst(addr, #data_tab[i], interface)
            end
           -- print(tab, #tab)
            if tab == false then 
                printc("code download error.")
                COMMUNICATION_ERROR = 1
                break 
            end
            addr = addr + #data_tab[i]
            
            for j = 1, #data_tab[i] do
                if tab[j] ~= data_tab[i][j] then
                    error = 1
                    --[[
                    printc(j, tab[j], data_tab[i][j])
                    printc("read back data:")
                    printc(tab)
                    printc("write data:")
                    printc(data_tab[i])
                    stop()
                   ]]
                    break
                end
            end
            
            if error == 1 then 
                printc("code download error.")
                COMMUNICATION_ERROR = 1
                break 
            end
            printc("Block "..i.." Download Right.")
        end
    end 
-------------------
    collectgarbage("collect")
    printc("******************** Down load code to RAM successful! ^_^\n")  
end

--*****************
iM501.__help.load_vec =
[==[
Usage: iM501.load_vec() 
  Download parameters to DSP.
  Param:  vec_file
  Return: no
]==]
iM501.load_vec = function(vec_file)
    printc("\n******************** Download parameters")
    print(vec_file)
    file = io.open(vec_file, "r") --assert(io.open(vec_file, "r"), "function load_vec() ERROR: Can't find the vec file! Please check it! "..vec_file)
    if file == nil then
        local str = vec_file
        local index = 1
        repeat
            index = string.find(str, "\\")
            str = string.sub(str, (index or 0)+1, -1)
        until index == nil
        vec_file = iM501.vec.default_path..str
        print("use default path.")
        file = io.open(vec_file, "r")
    end
    printc("vec file name is: "..vec_file)
    local i, address, para = 0
    
    for line in file:lines(file) do
        i = i + 1
        local dat_len = 1
        local interface = 1
        local token = line_to_token (line)
        for j = 1, #token do
            if string.find(token[j], "0x") == nil and string.find(token[j], "0X") == nil then  --�����ַ������ǰû��OX�����ϡ�
                token[j] = "0x"..token[j]
            end
        end

        if #token >= 2 then
            address, para = tonumber(token[1]), tonumber(token[2])
            if #token == 3 then dat_len = tonumber(token[3]) end
            if #token == 4 then interface = tonumber(token[4]) end
            if dat_len ~= 1 and dat_len ~= 2 then dat_len = 1 end
            if interface ~= 1 and interface ~= 2 then interface = 1 end
            printc(string.format("0x%0X", address).." = "..string.format("0x%0X", para))
            if address >= 0xffc0000 and  address < 0x10000000 then
                iM501.write_dram(address, para)
            else
                iM501.write_reg(address, para, dat_len, interface)
            end
        elseif #token ~= 0 then 
            printc("LOG_READ_MAP(line "..tostring(i)..", token_num "..tostring(#token).."): "..line)
        end
    end
    file:close()
    file = nil
    collectgarbage("collect")
    printc("******************** Down parameters successful! ^_^\n") 
end
local fileCovertTab = function(code_file)
    printc("\nLoading bat file... "..code_file)
    local file = assert(io.open(code_file, "rb"), "function code_convert() ERROR : data file is not exist! "..code_file)
    local str = file:read("*all")
    local data_tab = {}
    local bin_length = string.len(str)
    printc(" bin_file_length: "..string.format("0x%0x", bin_length)) 
   
    for i=1, bin_length do
        table.insert(data_tab, string.byte(str, i))
    end
    file:close()
    return data_tab
end

iM501.i2c_fast_load_code = function (code_file, randomizeTab)
    printc("\n******************** Fast I2C Download RAM code")
    --local data_tab = iM501.code_convert(code_file, code_type)
    local data_tab = (randomizeTab and #randomizeTab>0) and randomizeTab or fileCovertTab(code_file)

    local block_size = iM501.EMB_DATA_SIZE    
    local p=0
    local burst_len=0
    local bin_length = #data_tab
    local seg_num=1
    local changeFlag = true
    printc(" bin_file_length: "..string.format("0x%0x", bin_length)) 
   
    while p < bin_length do 
        printc(" seg_num: "..string.format("%0d", seg_num))
        seg_num=seg_num+1
    	local length = {}
    	local len = 0  	
    	local address = {}
    	local addr = 0
    	local mem_type = 0
    	length[1]= data_tab[p+7]
    	length[2]= data_tab[p+8]
    	length[3]= data_tab[p+9]
    	length[4]= data_tab[p+10]
    	printc("length[1]: "..string.format("0x%0x", length[1]))      
    	printc("length[2]: "..string.format("0x%0x", length[2]))      
    	printc("length[3]: "..string.format("0x%0x", length[3]))      
    	printc("length[4]: "..string.format("0x%0x", length[4])) 
    	len = length[1] + length[2]*0x100 + length[3]*0x10000 + length[4]*0x1000000
    	printc("len: "..string.format("0x%0x", len)) 
    	
    	address[1]= data_tab[p+1]
    	address[2]= data_tab[p+2]
    	address[3]= data_tab[p+3]
    	address[4]= data_tab[p+4]
    	printc("address[1]: "..string.format("0x%0x", address[1]))   
    	printc("address[2]: "..string.format("0x%0x", address[2]))  
    	printc("address[3]: "..string.format("0x%0x", address[3]))  
    	printc("address[4]: "..string.format("0x%0x", address[4]))  
    	addr =address[1] + address[2]*0x100 + address[3]*0x10000 + address[4]*0x1000000
        printc("addr: "..string.format("0x%0x", addr)) 
        
    	mem_type = data_tab[p+5] + data_tab[p+6]*0x100
        --printc("mem_type: "..string.format("0x%0x", mem_type)) 
        printc("mem_type: "..(mem_type==0 and "IRAM" or "DRAM"))
    	
    	--p = p+ len + 4+ 4 +2 --4+4+2 4: length len 4: addr len 2: iram or dram len
    	p = p+ 4+ 4 +2 --4+4+2 4: length len 4: addr len 2: iram or dram len

        if mem_type == 0x00 then
            UIF.set_interface(UIF.type.I2C, I2C_SPEED, iM501.ATTRI_LOAD_IRAM)
            burst_len=4
            --changeFlag = false
        elseif mem_type == 0x01 then
            UIF.set_interface(UIF.type.I2C, I2C_SPEED, iM501.ATTRI_LOAD_DRAM)
            burst_len=2
            --changeFlag = false
        else
            assert(false, "RAM type is wrong!")
        end
    	
		if(len > burst_len) then 
			local block_num =0
			local block_end =0
			block_num = len / block_size
			block_end = len % block_size
			printc("block_num: "..string.format("0x%0x", block_num))
			printc("block_end: "..string.format("0x%0x", block_end))
			
            for i = 1, block_num do
                local tab = {addr%0x10000%0x100, math.floor((addr%0x10000)/0x100), math.floor((addr/0x10000))}          
                for j=1, block_size do
                    table.insert(tab , data_tab[p+j])
                    --print(unpack( tab ))
                    --io.read()
                end         
                UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, tab,block_size)
                addr = addr + block_size
                p= p + block_size
            end
            tab = {addr%0x10000%0x100, math.floor((addr%0x10000)/0x100), math.floor((addr/0x10000))}
            for j=1,block_end do
                    table.insert(tab, data_tab[p+j])
            end
   			UIF.raw_write(UIF.type.I2C, iM501.I2C_addr, tab, block_end)
   			addr = addr + block_end
            p= p + block_end
		
        else
            UIF.set_interface(UIF.type.I2C, I2C_SPEED)
            --changeFlag = true
			if mem_type == 0x00 then--iram
				local tab=data_tab[p+4]*0x1000000+data_tab[p+3]*0x10000+data_tab[p+2]*0x100+data_tab[p+1]
                iM501.write_iram(addr, tab, 1)
                --io.read()
		    	p= p + 4
		    end
		    if mem_type == 0x01 then--dram
			    local tab= data_tab[p+2]*0x100+data_tab[p+1]
		    	iM501.write_dram(addr, tab, 1)
		    	p= p + 2
		    end
        end
        UIF.set_interface(UIF.type.I2C, I2C_SPEED)
  	end   
end
--------------------


--*****************
iM501.__help.init =
[==[
    Usage: iM501.init() 
    Init DSP, Reset DSP, then download the init vec file, RAM code and run vec file. these files are defined in "config"
    Param: No
    Return: true or false
]==]
iM501.init = function(path501In, micMode, fsMode, fastdowncode)
    printc("\n******************** Init iM501")
    UIF.clear_flag()
    iM501.reset(1)

    path501In = path501In or CODE_VEC_PATH
    
    -- im501 control port select
    if DL_PATH == 2 and SPI_MODE ~= 0 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 8+SPI_MODE, 1, 1)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, SPI_MODE, 1, 1)
    elseif DL_PATH == 1 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 0x4)
    end
    wait(1)
    
    -- to UIF flash, for power down & power save module
    --UIF.load_vec(1, path501In..iM501.vec.power_down_i2c, iM501.vec.power_down_i2c, 1)
    --UIF.load_vec(2, path501In..iM501.vec.power_down_spi, iM501.vec.power_down_spi, 2)
    --UIF.load_vec(3, path501In..iM501.vec.power_up, iM501.vec.power_up, 2)

    -- hold 501
    iM501.write_reg(0, 0x5, 1, 2)
    iM501.write_reg(0x0f, 0x5, 1, 1)

    -- VOS parameter
    --iM501.load_vec(path501In..iM501.vec.init)

    -- load dsp code
    Timer_Record()
    if fastdowncode==3 then
        iM501.i2c_fast_load_code(path501In.."fast_dRAM.bin")
        iM501.i2c_fast_load_code(path501In.."fast_iRAM.bin")
    elseif fastdowncode==2 then
        iM501.i2c_fast_load_code(path501In.."fast.bin")
    elseif fastdowncode==1 or fastdowncode==4 then
        for i = 1, #iM501.code do
           iM501.load_code(iM501.code[i][1], path501In..iM501.code[i][2], iM501.code[i][3])   
        end
    end
    Timer_Record()
    
    --load VEN parameters
    iM501.load_vec(path501In..iM501.vec.run)

    --eft_code
    --Timer_Record()
    --for i = 1, #iM501.eft_code do
    --    iM501.load_code(iM501.eft_code[i][1], path501In..iM501.eft_code[i][2], iM501.eft_code[i][3])   
    --end
    --Timer_Record()
    
    -- start 501
    iM501.write_reg(0, 0x4, 1, 2)
    iM501.write_reg(0xf, 0x4, 1, 1)
    
    -- mode select
    --if "oneMic" == micMode then iM501.write_message(0x19, 0x1) end
    --if "twoMic" == micMode then iM501.write_message(0x19, 0x2) end
 --   if 8000 == fsMode then iM501.write_message(0xc, 0x8) end
  --  if 16000 == fsMode then iM501.write_message(0xc, 0x10) end

    --check DSP status
	--assert(iM501.check_run(), "ERROR: check dsp run failed !")

    wait(1)
    printc("********************\n")

end

function iM501.check_run()
    printc("\nCheck the status of iM501.")
    return true --check_pass_fail() 
end

function iM501.rec_voice_buffe_old( irq_gpio_index )
    local param = 
        {
      	    u1=irq_gpio_index,
      	    u2=SPI_MODE,
      	    u3=SPI_SPEED
      	}
      	
    --clear_rpt()	
    UIF.cmd_write(42, param)

    printc("Start SPI recording, waiting for IRQ trigger...")
    --assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
    --print("Record finished.")
    check_send_ab_command_result("Voice buffer read", 2000 )
end

T_IRQ2CLKON = 1000  --ms, delay time from receive IRQ to turn on PDMCLK
T_NM2PSM = 3000     --8000 ms, delay time from normal to sleep, 0 means always on normal mode after wakeup.
T_SPIR2SPIR = 3000  --ms, delay time from SPI read to next SPI read
C_SPIR = 1           --0repeat SPI read cycles.
GOTO = 3    --1:normal mode  2: psm   3: one shot mode
MODE_AFTER_SPIR = 2  --1
function iM501.rec_voice_buffer_origin( irq_gpio_index, t1, t2, t3, c1, goto, after_spiread )
    local param = 
    {
        u1=irq_gpio_index or iM501.GPIO.IRQ,
        u2=SPI_MODE,
        u3=SPI_SPEED,
        u4=t1 or T_IRQ2CLKON, --T1=2500ms
        u5=t2 or T_NM2PSM,    --T2=2500ms
        u6=t3 or T_SPIR2SPIR, --T3=2500ms
        u7=c1 or C_SPIR,    --C1=2 
        u8=goto or GOTO,    --1:normal mode  2: psm   3: one shot mode
        u9=after_spiread or MODE_AFTER_SPIR,    -- see always_on_acoustic_awareness_Spec 
                       --1:normal mode  2: psm 
        u10=0,   --reserved
        u11=0    --reserved
    }

    UIF.cmd_write(42, param)
    printc("Update One shot setting")
end
--  print(T_IRQ2CLKON, T_NM2PSM, T_SPIR2SPIR, C_SPIR, GOTO, MODE_AFTER_SPIR)
function iM501.enter_PSM_origin()
    local param  = {}
    UIF.cmd_write(45, param)
    --audiobridge.send_command(45, param)
    printc("Enter PSM...")
    --print("Record finished.")
    --iM501.write_message(iM501.message.enter_psm, 0)
    --FM36.adc_clk_onoff(0)
end

function iM501.backto_NM_origin()
--[[
    FM36.adc_clk_onoff(1)
    wait(T_IRQ2CLKON/1000)
    iM501.write_message(iM501.message.back_nm, 0, DL_PATH)
]]
    local param  = {}
    UIF.cmd_write(50, param)
    printc("Enter Normal Mode.")
end
--new modified function to support I2C enter im501 to PSM
--function offer by ruancheng
-- UIF FW need update to  H2.7.4.2:A2.5.4
function iM501.rec_novoice_buffer( irq_gpio_index )
    local param =
        {
            u1=irq_gpio_index or iM501.GPIO.IRQ,
            u2=SPI_MODE,
            u3=SPI_SPEED,
            u4=800, --T1=2500ms
            u5=3000,    --T2=2500ms
            u6=3000, --T3=2500ms
            u7=0,    --C1=0 
            u8=1,    --1:normal mode  2: psm   3: one shot mode
            u9=1,    -- see always_on_acoustic_awareness_Spec 
                      -- 1: 3.a  2: 3.b.i  3:3.b.ii 
            u10=DL_PATH,   --reserved
            u11=0    --reserved
      }
    
        clear_rpt()
        audiobridge.send_command(42, param)
    
        printc("Start SPI recording, waiting for IRQ trigger...")
        assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
        --print("Record finished.")
end
function iM501.rec_voice_buffer( irq_gpio_index )
    local param =
        {
            u1=irq_gpio_index or iM501.GPIO.IRQ,
            u2=SPI_MODE,
            u3=SPI_SPEED,
            u4=500, --T1=2500ms
            u5=3000,    --T2=2500ms
            u6=3000, --T3=2500ms
            u7=0,    --C1=0 
            u8=3,    --1:normal mode  2: psm   3: one shot mode
            u9=1,    -- see always_on_acoustic_awareness_Spec 
                      -- 1: 3.a  2: 3.b.i  3:3.b.ii 
            u10=DL_PATH,   --reserved
            u11=0    --reserved
      }
    
        clear_rpt()
        audiobridge.send_command(42, param)
    
        printc("Start SPI recording, waiting for IRQ trigger...")
        assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
        --print("Record finished.")
end
function iM501.rec_voice_buffer_NM( irq_gpio_index )
    local param =
        {
            u1=irq_gpio_index or iM501.GPIO.IRQ,
            u2=SPI_MODE,
            u3=SPI_SPEED,
            u4=500, --T1=2500ms
            u5=0,  --3000,    --T2=2500ms
            u6=3000, --T3=2500ms
            u7=0,    --C1=0 
            u8=3,    --1:normal mode  2: psm   3: one shot mode
            u9=1,    -- see always_on_acoustic_awareness_Spec 
                      -- 1: 3.a  2: 3.b.i  3:3.b.ii 
            u10=DL_PATH,   --reserved
            u11=0    --reserved
      }
    
        clear_rpt()
        audiobridge.send_command(42, param)
    
        printc("Start SPI recording, waiting for IRQ trigger...")
        assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
        --print("Record finished.")
end
function iM501.rec_voice_buffer_PSM( irq_gpio_index )
    local param =
        {
            u1=irq_gpio_index or iM501.GPIO.IRQ,
            u2=SPI_MODE,
            u3=SPI_SPEED,
            u4=500, --T1=2500ms
            u5=3000,    --T2=2500ms
            u6=3000, --T3=2500ms
            u7=0,    --C1=0 
            u8=3,    --1:normal mode  2: psm   3: one shot mode
            u9=2,    -- see always_on_acoustic_awareness_Spec 
                      -- 1: 3.a  2: 3.b.i  3:3.b.ii 
            u10=DL_PATH,   --reserved
            u11=0    --reserved
      }
    
        clear_rpt()
        audiobridge.send_command(42, param)
    
        printc("Start SPI recording, waiting for IRQ trigger...")
        assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
        --print("Record finished.")
end
function iM501.enter_NORMAL(if_type, speed )
    local param  = {
        u1= if_type,
        u2= speed
    }
    clear_rpt()
    audiobridge.send_command(50, param)
    printc("Enter NORMAL...")
    --assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
    assert(wait_rpt(1011, (10000), "Single read ERROR!"))
end
function iM501.enter_PSM(if_type, speed )
    local param  = {
        u1= if_type,
        u2= speed
    }
    clear_rpt()
    audiobridge.send_command(45, param)
    printc("Enter PSM...")
    --assert(wait_rpt(1011, (1000/10), "Single read ERROR!"))
    assert(wait_rpt(1011, (10000), "Single read ERROR!"))
end

function iM501.enter_Oneshot(t4,c2)
    local param =
    {	
        u1=SPI_MODE,
        u2=SPI_SPEED,
        u3=t4 or T_SPIR2SPIR, --T4=2500ms
        u4= c2 or C_SPIR,    --C2=1 
        u5=0,   --reserved
        u6=0    --reserved
    }
    UIF.cmd_write(51, param)
    printc("Start Read voice buffer.")
end

function iM501.set_pll(mips)
    iM501.write_dram(0x0fffff32, mips )
    iM501.write_dram(0x0fffff34, 0x0001)
    iM501.read_dram(0x0fffff32, 0x2)
    iM501.read_dram(0x0fffff34, 0x2)
end

function iM501.set_ldo(ldo)
    iM501.write_dram(0x0fffff22, ldo) 
end

function iM501.clear_flag()
    print "Now clear pattern flag......"
    iM501.write_dram(iM501.vec.flag_address, 0x5678)
    iM501.write_dram(iM501.vec.flag_address + 2, 0x1234)
    local state, temp = iM501.read_dram(iM501.vec.flag_address)
    if state == false then printc(string.format("Clear FLAG ERROR")) return end
    printc(string.format("FLAG be cleared to 0x%x", temp))
    local test_adr = 0x0FFF0000
    iM501.write_dram(test_adr, 0x5678)     -- 2 bytes
    iM501.write_dram(test_adr + 2, 0x1234)   -- 2 bytes
    local state, temp = iM501.read_dram(test_adr)
    if state == false then printc(string.format("Clear FLAG ERROR")) return end
    printc(string.format("FLAG be cleared to 0x%x", temp))
end

function iM501.run_pattern(pattern, mips, ldo, wait_time)
    printc("\nRun Pattern "..pattern)
    iM501.reset(1)
    if DL_PATH == 2 and SPI_MODE ~= 0 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 8+SPI_MODE, 1, 1)
        iM501.write_reg(iM501.I2C_REG.spi_i2c, SPI_MODE, 1, 1)
    elseif DL_PATH == 1 then
        iM501.write_reg(iM501.I2C_REG.spi_i2c, 0x4)
    end
    wait(0.2)

    ---- turn on DSP clock
    iM501.write_reg(0x00, 0x5, 0x1, 2)   -- SPI host register
    iM501.write_reg(0x0F, 0x5, 0x1, 1)   -- I2C host register
    iM501.clear_flag()
    iM501.set_pll(mips)
    iM501.set_ldo(ldo)
    wait(0.2)
    print(PATTERN_PATH..pattern)
    for i = 1, #iM501.code do
        iM501.load_code(iM501.code[i][1], PATTERN_PATH..pattern.."\\"..iM501.code[i][2], iM501.code[i][3])   
    end

    iM501.write_reg(0x00, 0x4, 0x1, 2)   -- SPI host register
    iM501.write_reg(0x0F, 0x4, 0x1, 1)   -- I2C host register
    sleep(wait_time)
    printc("********************\n")
end

function iM501.JTAG()
    iM501.reset(3)
    local state, value = iM501.read_dram(0xfffff32)
    if state == false or value == 0xffff then return false end
    
    iM501.write_reg(0x0E, 0x08, 1, 1)
    iM501.write_reg(0x0F, 0x04, 1, 1)
end
