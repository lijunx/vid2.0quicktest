--module define
printc("define UIF command bus GUI module.")
GUI.com_path = {}
GUI.com_path.constant = 
{
    dl_path_default = DL_PATH or 2; --1:I2C; 2:SPI
    spi_mode_default = (SPI_MODE or 0)+1; --1:"CPHA,CPOL: 00"
    i2c_speed_default = 400;
    spi_speed_default = 5;
}
GUI.com_path.list = 
{
    spi_mode = iup.list
    {
        "CPHA,CPOL: 00","CPHA,CPOL: 01","CPHA,CPOL: 02","CPHA,CPOL: 03", action_time = false,
        dropdown="YES", visible_items=4, size="80x", value = GUI.com_path.constant.spi_mode_default,
        action=function()
            com_path_to_kernal()
            printc("SPI mode is "..GUI.com_path.list.spi_mode.value-1)
        end
    };
}

GUI.com_path.label = 
{
    bus = iup.label{title="Command Bus:"};
    spi_mode = iup.label{title="SPI mode:"};
    i2c_speed = iup.label{title="I2C Speed(KHz):"};
    spi_speed = iup.label{title="SPI Speed(MHz):"};
}
GUI.com_path.text =
{
    i2c_speed = iup.text{value = GUI.com_path.constant.i2c_speed_default, size="30x", action=function() com_path_to_kernal() end};
    spi_speed = iup.text{value = GUI.com_path.constant.spi_speed_default, size="30x", action=function() com_path_to_kernal() end};
}
GUI.com_path.toggle =
{
    spi = iup.toggle{title="SPI", value=GUI.com_path.constant.dl_path_default == 2 and "ON" or "OFF";};
    i2c = iup.toggle{title="I2C", value=GUI.com_path.constant.dl_path_default == 1 and "ON" or "OFF";};
}
GUI.com_path.radio = 
{
    bus = iup.radio
    {
        iup.hbox{GUI.com_path.toggle.spi,GUI.com_path.toggle.i2c;gap="30",margin="5",alignment = "ACENTER"};
        value=GUI.com_path.toggle.spi;
    };
}
GUI.com_path.button =
{
    --update = iup.button{size="40x", title="Update"};
    --default = iup.button{size="40x", title="Default"};
    --version = iup.button{size="40x", title="Version"};
}

GUI.com_path.local_frame =
{

};
    
GUI.com_path.frame = iup.frame
{
    iup.vbox
    {  
        iup.hbox
        {
            iup.hbox{GUI.com_path.label.bus, GUI.com_path.radio.bus;gap="5",margin="5",alignment="ACENTER"};
            --iup.hbox{GUI.com_path.button.default, GUI.com_path.button.update;gap="20",margin="5",alignment="ACENTER"};
            gap="100",margin="5",alignment="ACENTER"
        };
        iup.hbox
        {
            --iup.hbox{GUI.com_path.label.bus, GUI.com_path.radio.bus;gap="5",margin="5",alignment="ACENTER"};
            iup.hbox{GUI.com_path.label.spi_mode, GUI.com_path.list.spi_mode;gap="5",margin="5",alignment="ACENTER"};
            iup.hbox{GUI.com_path.label.spi_speed, GUI.com_path.text.spi_speed;gap="5",margin="5",alignment="ACENTER"};
            iup.hbox{GUI.com_path.label.i2c_speed, GUI.com_path.text.i2c_speed;gap="5",margin="5",alignment="ACENTER"};
            gap="37",margin="5",alignment="ACENTER"
        };
        gap="10",margin="0x5",alignment="ALEFT"
    };
    title = "Command Bus Setting", 
    size="400x",margin="0x10", alignment = "ACENTER",
}

--function
download_path = DL_PATH

com_path_to_kernal = function()
    if GUI.com_path.text.spi_speed.value+0 > 40 then
        message("ERROR", "Host supports maximum SPI speed is 40MHz")
        GUI.com_path.text.spi_speed.value = 40
    end

    if GUI.com_path.text.i2c_speed.value+0 > 400 then
        message("ERROR", "Host supports maximum I2C speed is 400KHz")
        GUI.com_path.text.i2c_speed.value = 400
    end

    for k, v in pairs(GUI.kernel.command_bus) do
        if GUI.kernel.command_bus[k].name == "spi" then
            GUI.kernel.command_bus[k].active = GUI.com_path.toggle.spi.value
            GUI.kernel.command_bus[k].bclock = (GUI.com_path.text.spi_speed.value + 0) * 1000000
            GUI.kernel.command_bus[k].format = GUI.com_path.list.spi_mode.value - 1
        elseif GUI.kernel.command_bus[k].name == "i2c" then
            GUI.kernel.command_bus[k].active = GUI.com_path.toggle.i2c.value
            GUI.kernel.command_bus[k].bclock = (GUI.com_path.text.i2c_speed.value + 0) * 1000
        end
    end
end

GUI.com_path.toggle.spi.action = function()
    download_path = 2
    GUI.com_path.label.spi_mode.active = "yes"
    GUI.com_path.list.spi_mode.active = "yes"
    GUI.com_path.label.spi_speed.active = "yes"
    GUI.com_path.text.spi_speed.active = "yes"
    GUI.com_path.label.i2c_speed.active = "no"
    GUI.com_path.text.i2c_speed.active = "no"
    com_path_to_kernal()
end

GUI.com_path.toggle.i2c.action = function()
    download_path = 1
    GUI.com_path.label.spi_mode.active = "no"
    GUI.com_path.list.spi_mode.active = "no"
    GUI.com_path.label.spi_speed.active = "no"
    GUI.com_path.text.spi_speed.active = "no"
    GUI.com_path.label.i2c_speed.active = "yes"
    GUI.com_path.text.i2c_speed.active = "yes"
    com_path_to_kernal()
end

GUI.com_path.active = function(active)
    if active == "yes" or active == "YES" then
        GUI.com_path.radio.bus.active = "yes"
        GUI.com_path.list.spi_mode.active = "yes"
        GUI.com_path.text.i2c_speed.active = "yes"
        GUI.com_path.text.spi_speed.active = "yes"
    elseif active == "no" or active == "NO" then
        GUI.com_path.radio.bus.active = "no"
        GUI.com_path.list.spi_mode.active = "no"
        GUI.com_path.text.i2c_speed.active = "no"
        GUI.com_path.text.spi_speed.active = "no"
    end
end

GUI.com_path.active("yes")
--GUI.com_path.button.default.action()
--UIF.init()
