--module define
printc("define iM501 VR Setting module.")
GUI = GUI or {}
im501_system_config=0
--print(string.format("0x%0x", im501_system_config))
----index defined.
-- NM: normal, PSM: power saving mode, 
-- BF: before, HP: higt power ADC
NM_NS_BF_XSD = 0
NM_NS_BF_SD = 1
NM_XSD_EN = 2
NM_PDM_BYPASS = 3
NM_XSD_HPADC = 4
NM_SD_HPADC = 5
NM_SD_EN = 6  ---
NM_XAA_EN = 6
PSM_SD_EN = 7 ---
PSM_XAA_EN = 7
PSM_NS_BF_XSD = 8
PSM_NS_BF_SD = 9
PSM_XSD_EN = 10
PSM_XSD_HPADC = 11
PSM_SD_HPADC = 12
NS_PARA = 13--14
NM_SR = 13   --0: 8K; 1: 16K
MIC_SEL = 16
PDMCLKO = 17--18
NM_NS_EN = 19
PSM_NS_EN = 20
monitor_address = ""
GUI.im501_vr_setting = {}
GUI.im501_vr_setting.constant = 
{
	xsd_default = 1;           --"Hi"
	sd_default = 1;            --2:"zmkm", 1:"hello blue genius"
    MIC_default = 1;
	nm_ns_default = 2;   --1:"Before xSD", 2:"After xSD, before SD", 3:"After SD"	    
    nm_xsd_adc = 2;
    nm_sd_adc = 2;
    psm_ns_default = 2;
    psm_xsd_adc = 2;
    psm_sd_adc = 2;
}
GUI.im501_vr_setting.list = 
{
    xsd = iup.list
    {
        "Hi";
        dropdown="YES", visible_items=1, size="40x", value = GUI.im501_vr_setting.constant.xsd_default, action_time = false,
        action=function()
            printc("xSD voice is"..GUI.im501_vr_setting.list.xsd[GUI.im501_vr_setting.list.xsd.value])
        end
    };
    sd = iup.list
    { 
        "Hello Blue Genie", "Zhi Ma Kai Men", "Ni Hao Xiao Yi";
        dropdown="YES", visible_items=3, size="80x", value = GUI.im501_vr_setting.constant.sd_default, action_time = false,
        action=function()
            printc("SD voice is "..GUI.im501_vr_setting.list.sd[GUI.im501_vr_setting.list.sd.value])
        end
    };
    mic_sel = iup.list
    { 
        "Host MIC", "Third-party MIC";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.MIC_default, action_time = false,
        action=function()
            printc("MIC selection is "..GUI.im501_vr_setting.list.mic_sel[GUI.im501_vr_setting.list.mic_sel.value])
        end
    };
    pdmclko = iup.list
    { 
        "512KHz", "768KHz", "1024KHz";
        dropdown="NO", visible_items=3, size="60x30", value = 3, action_time = false,
        action=function()
            printc("MIC selection is "..GUI.im501_vr_setting.list.pdmclko[GUI.im501_vr_setting.list.pdmclko.value])
        end
    };
    nm_ns = iup.list
    {
        "Before xSD","Before SD";--,"After SD";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.nm_ns_default, action_time = false,
        action=function()
            printc("Normal mode NS position is "..GUI.im501_vr_setting.list.nm_ns[GUI.im501_vr_setting.list.nm_ns.value])
        end
    };
    nm_xsd_adc = iup.list
    {
        "Low power ADC","Normal ADC";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.nm_xsd_adc, action_time = false,
        action=function()
            printc("Normal mode xSD use "..GUI.im501_vr_setting.list.nm_xsd_adc[GUI.im501_vr_setting.list.nm_xsd_adc.value])
        end
    };
    nm_sd_adc = iup.list
    {
        "Low power ADC","Normal ADC";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.nm_sd_adc, action_time = false,
        action=function()
            printc("Normal mode SD use "..GUI.im501_vr_setting.list.nm_sd_adc[GUI.im501_vr_setting.list.nm_sd_adc.value])
        end
    };
    psm_ns = iup.list
    {
        "Before xSD","Before SD";--,"After SD";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.psm_ns_default, action_time = false,
        action=function()
            printc("Power saving mode NS position is "..GUI.im501_vr_setting.list.psm_ns[GUI.im501_vr_setting.list.psm_ns.value])
        end
    };
    psm_xsd_adc = iup.list
    {
        "Low power ADC","Normal ADC";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.psm_xsd_adc, action_time = false,
        action=function()
            printc("Power saving mode xSD use "..GUI.im501_vr_setting.list.psm_xsd_adc[GUI.im501_vr_setting.list.psm_xsd_adc.value])
        end
    };
    psm_sd_adc = iup.list
    {
        "Low power ADC","Normal ADC";
        dropdown="YES", visible_items=2, size="60x", value = GUI.im501_vr_setting.constant.psm_sd_adc, action_time = false,
        action=function()
            printc("Power saving mode SD use "..GUI.im501_vr_setting.list.psm_sd_adc[GUI.im501_vr_setting.list.psm_sd_adc.value])
        end
    };
}

GUI.im501_vr_setting.label = 
{
    --avd = iup.label{title = "??"};
    xsd_keyword = iup.label{title = "Keyword:"};
    sd_keyword = iup.label{title = "Hotword:"};
    mic_sel = iup.label{title = "MIC:"};
    nm_ns_position = iup.label{title = "Position:"};
    psm_ns_position = iup.label{title = "Position:"};
    psap_addr = iup.label{title = "Address:"};
    addr_s1 = iup.label{size="81x",title = "DRAM Address1(0x):"};
    data_s1 = iup.label{title = "Value:"};
    addr_s2 = iup.label{size="81x",title = "DRAM Address2(0x):"};
    data_s2 = iup.label{title = "Value:"};
    time_interval = iup.label{title = "Time Interval(S):"};
}

GUI.im501_vr_setting.text = 
{
    psap_addr = iup.text{value = string.format("0x%0x", iM501.vec.psap), size="60x"};
    spl_addr1 = iup.text{value = "", size="60x"};
    spl_addr2 = iup.text{value = "", size="60x"};
    spl_value = iup.text{value = "", size="60x"};
    time_interval = iup.text{value = "1.5", size="20x"};
}

GUI.im501_vr_setting.time = 
{
    monitor_timer = iup.timer{time=1500, run="NO"}
}

GUI.im501_vr_setting.toggle = 
{
    nm_avd = iup.toggle{title="AVD",value="OFF"};
    nm_xsd = iup.toggle{title="xSD",value="ON"};
    nm_sd  = iup.toggle{title="SD",value="ON"};
    nm_ns  = iup.toggle{title="NS",value="ON"};
    psm_avd = iup.toggle{title="AVD",value="ON"};
    psm_xsd = iup.toggle{title="xSD",value="ON"};
    psm_sd  = iup.toggle{title="SD",value="ON"};
    psm_ns  = iup.toggle{title="NS",value="ON"};
    aa  = iup.toggle{title="AA",value="ON"}; --acoustic awareness
    psap1 = iup.toggle{title="bit0",value="OFF"};
    psap2 = iup.toggle{title="bit1",value="OFF"};
    psap3 = iup.toggle{title="bit2",value="OFF"};
    psap4 = iup.toggle{title="bit3",value="OFF"};
    psap5 = iup.toggle{title="bit4",value="OFF"};
    psap6 = iup.toggle{title="bit5",value="OFF",active="OFF"};
    psap7 = iup.toggle{title="bit6",value="OFF",active="OFF"};
    psap8 = iup.toggle{title="bit7",value="OFF",active="OFF"};
    hex1 = iup.toggle{title="Hex",value="ON"};
    hex2 = iup.toggle{title="Hex",value="ON"};
    dec1 = iup.toggle{title="Dec",value="OFF"};
    dec2 = iup.toggle{title="Dec",value="OFF"};
};

GUI.im501_vr_setting.button =
{
    all_on = iup.button{size="35x", title="All ON"};
    default = iup.button{size="35x", title="Default"};
    ok = iup.button{size="35x", title="Ok"};
    cancel = iup.button{size="35x", title="Cancel"};
    psap1 = iup.button{size="35x", title="1"};
    psap2 = iup.button{size="35x", title="2"};
    psap3 = iup.button{size="35x", title="3"};
    psap4 = iup.button{size="35x", title="4"};
    psap5 = iup.button{size="35x", title="5"};
    monitor1 = iup.button{size="35x", title="Monitor"};
    monitor2 = iup.button{size="35x", title="Monitor"};
    monitor_start = iup.button{size="35x", title="Start"};
    monitor_stop = iup.button{size="35x", title="Stop"};
}

GUI.im501_vr_setting.radio = 
{
    unit_switch = iup.radio
    {
        iup.hbox
        {
            GUI.im501_vr_setting.toggle.hex1, 
            GUI.im501_vr_setting.toggle.dec1, 
            gap="5",margin="5x5",alignment = "ACENTER"
        };
        value=GUI.im501_vr_setting.toggle.hex1;
    };
}

GUI.im501_vr_setting.local_frame = 
{
    global = iup.frame
    {
        iup.hbox
        {
            iup.hbox
            {
            --GUI.im501_vr_setting.toggle.xsd,
            GUI.im501_vr_setting.label.xsd_keyword,
            GUI.im501_vr_setting.list.xsd ;
            gap="5",margin="5",alignment = "ACENTER"
            };
            iup.hbox
            {
            --GUI.im501_vr_setting.toggle.sd,
            GUI.im501_vr_setting.label.sd_keyword,
            GUI.im501_vr_setting.list.sd ;
            gap="5",margin="5",alignment = "ACENTER"
            };
            iup.hbox
            {
            --GUI.im501_vr_setting.toggle.sd,
            GUI.im501_vr_setting.label.mic_sel,
            GUI.im501_vr_setting.list.mic_sel ;
            gap="5",margin="5",alignment = "ACENTER"
            };
            GUI.im501_vr_setting.button.default;
            gap="20",margin="1",alignment = "ACENTER"
        };
        title = "Global Setting",margin="0x5",alignment = "ACENTER",
     };
     normal_mode = iup.frame
     {
        iup.hbox
        { 
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.nm_ns,
                --GUI.im501_vr_setting.label.nm_ns_position,
                GUI.im501_vr_setting.list.nm_ns;
                gap="5",margin="5",alignment = "ACENTER"
            };
            GUI.im501_vr_setting.toggle.nm_avd;
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.nm_xsd,
                GUI.im501_vr_setting.list.nm_xsd_adc;
                gap="5",margin="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.nm_sd,
                GUI.im501_vr_setting.list.nm_sd_adc;
                gap="5",margin="5",alignment = "ACENTER"
            };
            --GUI.im501_vr_setting.button.all_on,
            gap="24",margin="1",alignment = "ACENTER"
        };
        title = "Normal mode Setting",margin="0x5",alignment = "ACENTER",
    };
     power_save_mode = iup.frame
     {
        iup.hbox
        { 
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.psm_ns,
                --GUI.im501_vr_setting.label.psm_ns_position,
                GUI.im501_vr_setting.list.psm_ns;
                gap="5",margin="5",alignment = "ACENTER"
            };
            GUI.im501_vr_setting.toggle.psm_avd;
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.psm_xsd,
                GUI.im501_vr_setting.list.psm_xsd_adc;
                gap="5",margin="5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.psm_sd,
                GUI.im501_vr_setting.list.psm_sd_adc;
                gap="5",margin="5",alignment = "ACENTER"
            };
            --GUI.im501_vr_setting.button.all_on,
            gap="24",margin="1",alignment = "ACENTER"
        };
        title = "Power saving mode Setting",margin="0x5",alignment = "ACENTER",
    };
    psap = iup.frame
    {
        iup.vbox
        { 
            --GUI.im501_vr_setting.text.psap_addr;
            iup.hbox
            {
                GUI.im501_vr_setting.toggle.psap1;
                GUI.im501_vr_setting.toggle.psap2;
                GUI.im501_vr_setting.toggle.psap3;
                GUI.im501_vr_setting.toggle.psap4;
                GUI.im501_vr_setting.toggle.psap5;
                GUI.im501_vr_setting.toggle.psap6;
                GUI.im501_vr_setting.toggle.psap7;
                GUI.im501_vr_setting.toggle.psap8;
                gap="25",margin="5X5",alignment = "ACENTER"
            };
            gap="5",margin="0x5",alignment = "ACENTER";
        };
        title = "User Defined Functions",margin="5x10",alignment = "ACENTER",
    };
    spl = iup.frame
    {
        iup.hbox
        { 
             iup.hbox
            {
                GUI.im501_vr_setting.label.addr_s1;
                GUI.im501_vr_setting.text.spl_addr1;
                GUI.im501_vr_setting.button.monitor1;
                --GUI.im501_vr_setting.radio.unit_switch;
                gap="5",margin="5X5",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.im501_vr_setting.label.addr_s2;
                GUI.im501_vr_setting.text.spl_addr2;
                GUI.im501_vr_setting.button.monitor2;
                --GUI.im501_vr_setting.radio.unit_switch;
                gap="5",margin="5X5",alignment = "ACENTER"
            };
            gap="11",margin="0x5",alignment = "ACENTER";
        };
        title = "User Defined Automatic Output",margin="5x10",alignment = "ACENTER",
    };
}
GUI.im501_vr_setting.dialog = 
{
    pdmclko_sel = iup.dialog
    {
        iup.vbox
        {
            GUI.im501_vr_setting.list.pdmclko;
            iup.hbox
            {
                GUI.im501_vr_setting.button.ok;
                GUI.im501_vr_setting.button.cancel;
                margin="5x5", gap="5"
            };
            margin="10x5", gap="10",alignment = "ACENTER",
       };
       title="Select PDM Output Clock", margin="10x10", gap="10"--,size="100x20"
    };
    monitor = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                iup.hbox
                {
                    GUI.im501_vr_setting.label.data_s1;
                    GUI.im501_vr_setting.text.spl_value;
                    margin="5x5", gap="5",alignment = "ACENTER",
                };
                GUI.im501_vr_setting.radio.unit_switch;
                margin="5x5", gap="20",alignment = "ACENTER",
            };
            --[[
            iup.hbox
            {
                iup.hbox
                {
                    GUI.im501_vr_setting.label.time_interval;
                    GUI.im501_vr_setting.text.time_interval;
                    margin="5x5", gap="5",alignment = "ACENTER",
                };
                iup.hbox
                {
                    GUI.im501_vr_setting.button.monitor_start;
                    GUI.im501_vr_setting.button.monitor_stop;
                    margin="5x5", gap="5",alignment = "ACENTER",
                };
                margin="5x5", gap="20",alignment = "ACENTER",
            };
            ]]
            margin="5x5", gap="5",alignment = "ALEFT",
       };
       title="Monitor", margin="10x10", gap="10"--,size="100x20"
    };
}
GUI.im501_vr_setting.frame = iup.frame
{
    iup.vbox
    {
        GUI.im501_vr_setting.local_frame.global;
        GUI.im501_vr_setting.local_frame.normal_mode;
        GUI.im501_vr_setting.local_frame.power_save_mode;
        GUI.im501_vr_setting.local_frame.psap;
        GUI.im501_vr_setting.local_frame.spl;
        gap="5",margin="10x5",alignment = "ALEFT"
    };
    title = "iM501 Functional Setting", size="400x",margin="0x5",alignment = "ACENTER",
}


--function
function GUI.im501_vr_setting.toggle.nm_avd:action()
    if GUI.im501_vr_setting.toggle.nm_avd.value == "ON" then
        printc("Turn on AVD")
    else
        printc("Turn off AVD")
    end
end
function GUI.im501_vr_setting.toggle.aa:action()
    if GUI.im501_vr_setting.toggle.aa.value == "ON" then
        printc("Turn on acoustic awareness")
    else
        printc("Turn off acoustic awareness")
    end
end
function GUI.im501_vr_setting.toggle.nm_xsd:action()
    if GUI.im501_vr_setting.toggle.nm_xsd.value == "ON" then
        printc("Turn on xSD")
        --GUI.im501_vr_setting.label.xsd_keyword.active = "yes";
        GUI.im501_vr_setting.list.nm_xsd_adc.active = "yes" ;
        local value, index = {1,GUI.im501_vr_setting.list.nm_xsd_adc.value-1}, {NM_XSD_EN, NM_XSD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    else
        printc("Turn off xSD")
        --GUI.im501_vr_setting.label.nm_xsd_keyword.active = "no";
        GUI.im501_vr_setting.list.nm_xsd_adc.active = "no" ;
        local value, index = {0}, {NM_XSD_EN}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end
function GUI.im501_vr_setting.toggle.nm_sd:action()
    if GUI.im501_vr_setting.toggle.nm_sd.value == "ON" then
        printc("Turn on SD")
        --GUI.im501_vr_setting.label.sd_keyword.active = "yes";
        GUI.im501_vr_setting.list.nm_sd_adc.active = "yes" ;
        local value, index = {1,GUI.im501_vr_setting.list.nm_sd_adc.value-1}, {NM_SD_EN, NM_SD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    else
        printc("Turn off SD")
        --GUI.im501_vr_setting.label.nm_sd_keyword.active = "no";
        GUI.im501_vr_setting.list.nm_sd_adc.active = "no" ;
        --local value, index = {0,0}, {NM_SD_EN, NM_SD_HPADC}
        local value, index = {0}, {NM_SD_EN}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end
function GUI.im501_vr_setting.toggle.nm_ns:action()
    if GUI.im501_vr_setting.toggle.nm_ns.value == "ON" then
        printc("Turn on NS")
        GUI.im501_vr_setting.label.nm_ns_position.active = "yes";
        GUI.im501_vr_setting.list.nm_ns.active = "yes" ;
        if GUI.im501_vr_setting.list.nm_ns.value+0 == 1 then
            --local value, index = {1,1,0}, {NM_NS_EN, NM_NS_BF_XSD, NM_NS_BF_SD}
            local value, index = {1,0}, {NM_NS_BF_XSD, NM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        else
            local value, index = {0,1}, {NM_NS_BF_XSD, NM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        end
    else
        printc("Turn off NS")
        GUI.im501_vr_setting.label.nm_ns_position.active = "no";
        GUI.im501_vr_setting.list.nm_ns.active = "no" ;
        local value, index = {0,0}, {NM_NS_BF_XSD, NM_NS_BF_SD}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.im501_vr_setting.toggle.psm_avd:action()
    if GUI.im501_vr_setting.toggle.psm_avd.value == "ON" then
        printc("Turn on PSM AVD")
    else
        printc("Turn off PSM AVD")
    end
end
function GUI.im501_vr_setting.toggle.psm_xsd:action()
    if GUI.im501_vr_setting.toggle.psm_xsd.value == "ON" then
        printc("Turn on PSM xSD")
        --GUI.im501_vr_setting.label.xsd_keyword.active = "yes";
        GUI.im501_vr_setting.list.psm_xsd_adc.active = "yes" ;
        local value, index = {1,GUI.im501_vr_setting.list.psm_xsd_adc.value-1}, {PSM_XSD_EN, PSM_XSD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    else
        printc("Turn off PSM xSD")
        --GUI.im501_vr_setting.label.nm_xsd_keyword.active = "no";
        GUI.im501_vr_setting.list.psm_xsd_adc.active = "no" ;
        local value, index = {0}, {PSM_XSD_EN}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end
function GUI.im501_vr_setting.toggle.psm_sd:action()
    if GUI.im501_vr_setting.toggle.psm_sd.value == "ON" then
        printc("Turn on PSM SD")
        --GUI.im501_vr_setting.label.sd_keyword.active = "yes";
        GUI.im501_vr_setting.list.psm_sd_adc.active = "yes" ;
        local value, index = {1,GUI.im501_vr_setting.list.psm_sd_adc.value-1}, {PSM_SD_EN, PSM_SD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    else
        printc("Turn off PSM SD")
        --GUI.im501_vr_setting.label.nm_sd_keyword.active = "no";
        GUI.im501_vr_setting.list.psm_sd_adc.active = "no" ;
        local value, index = {0}, {PSM_SD_EN}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end
function GUI.im501_vr_setting.toggle.psm_ns:action()
    if GUI.im501_vr_setting.toggle.psm_ns.value == "ON" then
        printc("Turn on PSM NS")
        GUI.im501_vr_setting.label.psm_ns_position.active = "yes";
        GUI.im501_vr_setting.list.psm_ns.active = "yes" ;
        if GUI.im501_vr_setting.list.psm_ns.value+0 == 1 then
            local value, index = {1,0}, {PSM_NS_BF_XSD, PSM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        else
            local value, index = {0,1}, {PSM_NS_BF_XSD, PSM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        end
    else
        printc("Turn off PSM NS")
        GUI.im501_vr_setting.label.psm_ns_position.active = "no";
        GUI.im501_vr_setting.list.psm_ns.active = "no" ;
        local value, index = {0,0}, {PSM_NS_BF_XSD, PSM_NS_BF_SD}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.im501_vr_setting.list.sd:action()
    GUI.im501_vr_setting.list.sd.action_time = not GUI.im501_vr_setting.list.sd.action_time
    if GUI.im501_vr_setting.list.sd.action_time then
        printc("Select SD keyword")
        iM501.write_message(iM501.message.configure_sd, GUI.im501_vr_setting.list.sd.value+0, DL_PATH)
    end
end
function GUI.im501_vr_setting.list.xsd:action()
    GUI.im501_vr_setting.list.xsd.action_time = not GUI.im501_vr_setting.list.xsd.action_time
    if GUI.im501_vr_setting.list.xsd.action_time then
        printc("Select xSD hotword")
    end
end

function GUI.im501_vr_setting.list.mic_sel:action()
    GUI.im501_vr_setting.list.mic_sel.action_time = not GUI.im501_vr_setting.list.mic_sel.action_time
    if GUI.im501_vr_setting.list.mic_sel.action_time then
          printc("Select MIC")
          if GUI.im501_vr_setting.list.mic_sel.value+0 == 2 then
              GUI.im501_vr_setting.dialog.pdmclko_sel:popup()
              local temp = GUI.im501_vr_setting.list.pdmclko.value+0
              local value, index = {},{}
              if temp == 1 then
                  value, index = {1,0,1}, {MIC_SEL,PDMCLKO,PDMCLKO+1}
              elseif temp == 2 then
                  value, index = {1,1,0}, {MIC_SEL,PDMCLKO,PDMCLKO+1}
              else
                  value, index = {1,1,1}, {MIC_SEL,PDMCLKO,PDMCLKO+1}
              end
              im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
          elseif GUI.im501_vr_setting.list.mic_sel.value+0 == 1 then
              local value, index = {0}, {MIC_SEL}
              im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
          end
     end
end

GUI.im501_vr_setting.button.cancel.action = function() 
    GUI.im501_vr_setting.dialog.pdmclko_sel:hide()
end
GUI.im501_vr_setting.button.ok.action = function()
    GUI.im501_vr_setting.dialog.pdmclko_sel:hide()
    local temp = GUI.im501_vr_setting.list.pdmclko.value+0
    local value, index = {},{}
    if temp == 1 then
        value, index = {1,0,1}, {MIC_SEL,PDMCLKO,PDMCLKO+1}
    elseif temp == 2 then
        value, index = {1,1,0}, {MIC_SEL,PDMCLKO,PDMCLKO+1}
    else
        value, index = {1,1,1}, {MIC_SEL,PDMCLKO,PDMCLKO+1}
    end
    im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
end

function GUI.im501_vr_setting.list.nm_xsd_adc:action()
    GUI.im501_vr_setting.list.nm_xsd_adc.action_time = not GUI.im501_vr_setting.list.nm_xsd_adc.action_time
    if GUI.im501_vr_setting.list.nm_xsd_adc.action_time then
        printc("Select xSD power ADC")
        local value, index = {GUI.im501_vr_setting.list.nm_xsd_adc.value-1}, {NM_XSD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.im501_vr_setting.list.nm_sd_adc:action()
    GUI.im501_vr_setting.list.nm_sd_adc.action_time = not GUI.im501_vr_setting.list.nm_sd_adc.action_time
    if GUI.im501_vr_setting.list.nm_sd_adc.action_time then
        printc("Select SD power ADC")
        local value, index = {GUI.im501_vr_setting.list.nm_sd_adc.value-1}, {NM_SD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.im501_vr_setting.list.nm_ns:action()
    GUI.im501_vr_setting.list.nm_ns.action_time = not GUI.im501_vr_setting.list.nm_ns.action_time
    if GUI.im501_vr_setting.list.nm_ns.action_time then
        printc("Select NS Position")
        if GUI.im501_vr_setting.list.nm_ns.value+0 == 1 then
            local value, index = {1,1,0}, {NM_NS_EN, NM_NS_BF_XSD, NM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        else
            local value, index = {1,0,1}, {NM_NS_EN, NM_NS_BF_XSD, NM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        end
    end
end

function GUI.im501_vr_setting.list.psm_xsd_adc:action()
    GUI.im501_vr_setting.list.psm_xsd_adc.action_time = not GUI.im501_vr_setting.list.psm_xsd_adc.action_time
    if GUI.im501_vr_setting.list.psm_xsd_adc.action_time then
        printc("Select xSD power ADC")
        local value, index = {GUI.im501_vr_setting.list.psm_xsd_adc.value-1}, {PSM_XSD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.im501_vr_setting.list.psm_sd_adc:action()
    GUI.im501_vr_setting.list.psm_sd_adc.action_time = not GUI.im501_vr_setting.list.psm_sd_adc.action_time
    if GUI.im501_vr_setting.list.psm_sd_adc.action_time then
        printc("Select SD power ADC")
        local value, index = {GUI.im501_vr_setting.list.psm_sd_adc.value-1}, {PSM_SD_HPADC}
        im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
    end
end

function GUI.im501_vr_setting.list.psm_ns:action()
    GUI.im501_vr_setting.list.psm_ns.action_time = not GUI.im501_vr_setting.list.psm_ns.action_time
    if GUI.im501_vr_setting.list.psm_ns.action_time then
        printc("Select NS Position")
        if GUI.im501_vr_setting.list.psm_ns.value+0 == 1 then
            local value, index = {1,1,0}, {PSM_NS_EN, PSM_NS_BF_XSD, PSM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        else
            local value, index = {1,0,1}, {PSM_NS_EN, PSM_NS_BF_XSD, PSM_NS_BF_SD}
            im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
        end
    end
end

function update_vr_set()
    local tab = {}
    local temp = im501_system_config
    for i = 1, 32 do 
        if temp%2 == 0 then
            table.insert(tab, 0)
        else
            table.insert(tab, 1)
        end
        temp = bit:_rshift(temp, 1)
    end
    --print(unpack(tab))
    GUI.im501_vr_setting.list.mic_sel.value = tab[MIC_SEL+1]+1
    --print(GUI.im501_vr_setting.list.mic_sel.value)
    --GUI.im501_vr_setting.toggle.nm_avd.value = tab[NM_AVD_EN+1] and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.nm_xsd.value = tab[NM_XSD_EN+1] == 1 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.nm_sd.value = tab[NM_SD_EN+1] == 1 and "ON" or "OFF"
    --GUI.im501_vr_setting.toggle.nm_ns.value = tab[NM_NS_EN+1] and "ON" or "OFF"
    GUI.im501_vr_setting.list.nm_xsd_adc.value = tab[NM_XSD_HPADC+1]+1
    GUI.im501_vr_setting.list.nm_sd_adc.value = tab[NM_SD_HPADC+1]+1
    GUI.im501_vr_setting.toggle.nm_ns.value = "ON"
    if tab[NM_NS_BF_XSD+1] == 1 then 
        GUI.im501_vr_setting.list.nm_ns.value = 1
    elseif tab[NM_NS_BF_SD+1] == 1 then 
        GUI.im501_vr_setting.list.nm_ns.value = 2
    else
        GUI.im501_vr_setting.toggle.nm_ns.value = "OFF"
    end
    --GUI.im501_vr_setting.toggle.psm_avd.value = tab[PSM_AVD_EN+1] and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psm_xsd.value = tab[PSM_XSD_EN+1] == 1 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psm_sd.value = tab[PSM_SD_EN+1] == 1 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psm_ns.value = "ON"
    GUI.im501_vr_setting.list.psm_xsd_adc.value = tab[PSM_XSD_HPADC+1]+1
    GUI.im501_vr_setting.list.psm_sd_adc.value = tab[PSM_SD_HPADC+1]+1
    if tab[PSM_NS_BF_XSD+1] == 1 then 
        GUI.im501_vr_setting.list.psm_ns.value = 1
    elseif tab[PSM_NS_BF_SD+1] == 1 then 
        GUI.im501_vr_setting.list.psm_ns.value = 2
    else
        GUI.im501_vr_setting.toggle.psm_ns.value = "OFF"
    end
    GUI.im501_vr_setting.toggle.aa.value = "ON"
    temp = bit:_and(im501_keyword, 0xf)
    if temp == 4 then temp = 3
    elseif temp == 8 then temp = 4
    end
    GUI.im501_vr_setting.list.sd.value = temp
    --print(im501_keyword)

    temp = im501_psap
    GUI.im501_vr_setting.toggle.psap1.value = bit:_and(temp, 0x1) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap2.value = bit:_and(temp, 0x2) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap3.value = bit:_and(temp, 0x4) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap4.value = bit:_and(temp, 0x8) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap5.value = bit:_and(temp, 0x10) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap6.value = bit:_and(temp, 0x20) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap7.value = bit:_and(temp, 0x40) ~= 0 and "ON" or "OFF"
    GUI.im501_vr_setting.toggle.psap8.value = bit:_and(temp, 0x80) ~= 0 and "ON" or "OFF"
end

function GUI.im501_vr_setting.button.all_on:action()
    GUI.im501_vr_setting.toggle.nm_avd.value = "ON"
    GUI.im501_vr_setting.toggle.nm_xsd.value = "ON"
    GUI.im501_vr_setting.toggle.nm_sd.value = "ON"
    GUI.im501_vr_setting.toggle.nm_ns.value = "ON"
    GUI.im501_vr_setting.toggle.aa.value = "ON"
    GUI.im501_vr_setting.toggle.nm_avd:action()
    GUI.im501_vr_setting.toggle.nm_xsd:action()
    GUI.im501_vr_setting.toggle.nm_sd:action()
    GUI.im501_vr_setting.toggle.nm_ns:action()
    GUI.im501_vr_setting.toggle.aa:action()
end
function GUI.im501_vr_setting.button.default:action()
    --GUI.im501_vr_setting.toggle.nm_avd.value = "ON"
    GUI.im501_vr_setting.toggle.nm_xsd.value = "ON"
    GUI.im501_vr_setting.toggle.nm_sd.value = "ON"
    GUI.im501_vr_setting.toggle.nm_ns.value = "ON"
    GUI.im501_vr_setting.toggle.aa.value = "ON"
    GUI.im501_vr_setting.toggle.nm_avd:action()
    GUI.im501_vr_setting.toggle.nm_xsd:action()
    GUI.im501_vr_setting.toggle.nm_sd:action()
    GUI.im501_vr_setting.toggle.nm_ns:action()
    GUI.im501_vr_setting.toggle.aa:action()
    GUI.im501_vr_setting.list.xsd.value = GUI.im501_vr_setting.constant.nm_xsd_default
    GUI.im501_vr_setting.list.sd.value = GUI.im501_vr_setting.constant.nm_sd_default
    GUI.im501_vr_setting.list.nm_ns.value = GUI.im501_vr_setting.constant.nm_ns_default
end
--[[
function GUI.im501_vr_setting.button.dram_r:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end

    local addr = remove_0x(GUI.im501_vr_setting.text.spl_value.value)

    if addr == false then return end
    
    local state, value = iM501.read_dram(tonumber("0x"..addr), 2, DL_PATH)
    if state then
        GUI.im501_vr_setting.text.spl_value.value = string.format("0x%04x", value)
    else
        message("ERROR", value)
    end
end
]]
function GUI.im501_vr_setting.button.monitor1:action()
    local addr = remove_0x(GUI.im501_vr_setting.text.spl_addr1.value)
    if addr == false then return end
    local state, value = iM501.read_dram(tonumber("0x"..addr), 2, DL_PATH)
    if state then
        GUI.im501_vr_setting.text.spl_value.value = string.format("0x%04x", value)
        monitor_address = "0x"..addr
        printc("Monitor DRAM: "..string.format("0x%04x", monitor_address))
        GUI.im501_vr_setting.dialog.monitor.title="Monitor "..string.format("0x%04x", monitor_address)
        GUI.im501_vr_setting.dialog.monitor:show()
        local del = GUI.im501_vr_setting.text.time_interval.value + 0
        GUI.im501_vr_setting.time.monitor_timer.time = del*1000
        GUI.im501_vr_setting.time.monitor_timer.run = "YES"
    else
        message("ERROR", value)
    end
end

function GUI.im501_vr_setting.button.monitor2:action()
    local addr = remove_0x(GUI.im501_vr_setting.text.spl_addr2.value)
    if addr == false then return end
    local state, value = iM501.read_dram(tonumber("0x"..addr), 2, DL_PATH)
    if state then
        GUI.im501_vr_setting.text.spl_value.value = string.format("0x%04x", value)
        monitor_address = "0x"..addr
        printc("Monitor DRAM: "..string.format("0x%04x", monitor_address))
        GUI.im501_vr_setting.dialog.monitor.title="Monitor "..string.format("0x%04x", monitor_address)
        GUI.im501_vr_setting.dialog.monitor:show()
        local del = GUI.im501_vr_setting.text.time_interval.value + 0
        GUI.im501_vr_setting.time.monitor_timer.time = del*1000
        GUI.im501_vr_setting.time.monitor_timer.run = "YES"
    else
        message("ERROR", value)
    end
end

function GUI.im501_vr_setting.time.monitor_timer.action_cb()
    print("Time called")
    --GUI.im501_vr_setting.time.monitor_timer.time = GUI.im501_vr_setting.text.time_interval.value + 0
    local state, value = iM501.read_dram(tonumber(monitor_address), 2, DL_PATH)
        if state then
            if GUI.im501_vr_setting.toggle.hex1.value == "ON" then
                GUI.im501_vr_setting.text.spl_value.value = string.format("0x%04x", value)
            else
                GUI.im501_vr_setting.text.spl_value.value = string.format("%0d", value)
            end
        else
            message("ERROR", value)
        end
end
function GUI.im501_vr_setting.dialog.monitor:close_cb()
    GUI.im501_vr_setting.time.monitor_timer.run = "NO"
    GUI.im501_vr_setting.dialog.monitor:hide()
    return iup.IGNORE
end
--[[
function GUI.im501_vr_setting.button.monitor_start:action()
    local del = GUI.im501_vr_setting.text.time_interval.value + 0
    GUI.im501_vr_setting.time.monitor_timer.time = del*1000
    GUI.im501_vr_setting.time.monitor_timer.run = "YES"
end
function GUI.im501_vr_setting.button.monitor_stop:action()
    GUI.im501_vr_setting.time.monitor_timer.run = "NO"
    --GUI.im501_vr_setting.dialog.monitor:hide()
end
]]

function GUI.im501_vr_setting.toggle.hex1:action()
    local value = tonumber(GUI.im501_vr_setting.text.spl_value.value)
    GUI.im501_vr_setting.text.spl_value.value = string.format("0x%04x", value)
end

function GUI.im501_vr_setting.toggle.dec1:action()
    local value = tonumber(GUI.im501_vr_setting.text.spl_value.value)
    GUI.im501_vr_setting.text.spl_value.value = string.format("%0d", value)
end

function GUI.im501_vr_setting.toggle.psap1:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap1.value == "ON" then
        temp = bit:_or(temp, 1)
    elseif state and GUI.im501_vr_setting.toggle.psap1.value == "OFF" then
        temp = bit:_and(temp, 0xfffe)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
end

function GUI.im501_vr_setting.toggle.psap2:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap2.value == "ON" then
        temp = bit:_or(temp, 2)
    elseif state and GUI.im501_vr_setting.toggle.psap2.value == "OFF" then
        temp = bit:_and(temp, 0xfffd)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
    
end

function GUI.im501_vr_setting.toggle.psap3:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap3.value == "ON" then
        temp = bit:_or(temp, 4)
    elseif state and GUI.im501_vr_setting.toggle.psap3.value == "OFF" then
        temp = bit:_and(temp, 0xfffb)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
    
end

function GUI.im501_vr_setting.toggle.psap4:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap4.value == "ON" then
        temp = bit:_or(temp, 8)
    elseif state and GUI.im501_vr_setting.toggle.psap4.value == "OFF" then
        temp = bit:_and(temp, 0xfff7)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
    
end

function GUI.im501_vr_setting.toggle.psap5:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap5.value == "ON" then
        temp = bit:_or(temp, 16)
    elseif state and GUI.im501_vr_setting.toggle.psap5.value == "OFF" then
        temp = bit:_and(temp, 0xffef)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
    
end

function GUI.im501_vr_setting.toggle.psap6:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap6.value == "ON" then
        temp = bit:_or(temp, 32)
    elseif state and GUI.im501_vr_setting.toggle.psap6.value == "OFF" then
        temp = bit:_and(temp, 0xffdf)
    end
--print( string.format("0x%0x", temp))
    iM501.write_dram(iM501.vec.psap, temp)
    
end

function GUI.im501_vr_setting.toggle.psap7:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap7.value == "ON" then
        temp = bit:_or(temp, 64)
    elseif state and GUI.im501_vr_setting.toggle.psap7.value == "OFF" then
        temp = bit:_and(temp, 0xffbf)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
    
end

function GUI.im501_vr_setting.toggle.psap8:action()
    local state, temp = iM501.read_dram(iM501.vec.psap)
--print( string.format("0x%0x", temp))
    if state and GUI.im501_vr_setting.toggle.psap8.value == "ON" then
        temp = bit:_or(temp, 128)
    elseif state and GUI.im501_vr_setting.toggle.psap8.value == "OFF" then
        temp = bit:_and(temp, 0xff7f)
    end
--print( string.format("0x%0x", temp))
    im501_psap = temp
    iM501.write_dram(iM501.vec.psap, temp)
    
end

GUI.im501_vr_setting.active = function(active)
    if active == "yes" or active == "YES" then
    --[[
        GUI.im501_vr_setting.toggle.nm_avd.value = "ON"
        GUI.im501_vr_setting.toggle.nm_xsd.value = "ON"
        GUI.im501_vr_setting.toggle.nm_sd.value = "ON"
        GUI.im501_vr_setting.toggle.nm_ns.value = "ON"
        GUI.im501_vr_setting.toggle.psm_avd.value = "ON"
        GUI.im501_vr_setting.toggle.psm_xsd.value = "ON"
        GUI.im501_vr_setting.toggle.psm_sd.value = "ON"
        GUI.im501_vr_setting.toggle.psm_ns.value = "ON"
        GUI.im501_vr_setting.toggle.aa.value = "ON"
        ]]
        --update_vr_set()
        --[[
        GUI.im501_vr_setting.toggle.nm_avd:action()
        GUI.im501_vr_setting.toggle.nm_xsd:action()
        GUI.im501_vr_setting.toggle.nm_sd:action()
        GUI.im501_vr_setting.toggle.nm_ns:action()
        GUI.im501_vr_setting.toggle.psm_avd:action()
        GUI.im501_vr_setting.toggle.psm_xsd:action()
        GUI.im501_vr_setting.toggle.psm_sd:action()
        GUI.im501_vr_setting.toggle.psm_ns:action()
        GUI.im501_vr_setting.toggle.aa:action()
        ]]
        GUI.im501_vr_setting.list.nm_xsd_adc.active = "yes"
        --GUI.im501_vr_setting.list.nm_sd_adc.active = "yes"
        GUI.im501_vr_setting.list.nm_ns.active = "yes"
        GUI.im501_vr_setting.list.psm_xsd_adc.active = "yes"
        --GUI.im501_vr_setting.list.psm_sd_adc.active = "yes"
        GUI.im501_vr_setting.list.psm_ns.active = "yes"
        GUI.im501_vr_setting.label.xsd_keyword.active = "yes"
        GUI.im501_vr_setting.list.xsd.active = "yes"
        GUI.im501_vr_setting.label.sd_keyword.active = "yes"
        GUI.im501_vr_setting.list.sd.active = "yes"
        GUI.im501_vr_setting.label.mic_sel.active = "yes"
        GUI.im501_vr_setting.list.mic_sel.active = "yes"
        --GUI.im501_vr_setting.toggle.nm_avd.active = "yes"
        GUI.im501_vr_setting.toggle.nm_xsd.active = "yes"
        --GUI.im501_vr_setting.toggle.nm_sd.active = "yes"
        GUI.im501_vr_setting.toggle.nm_ns.active = "yes"
        --GUI.im501_vr_setting.toggle.psm_avd.active = "yes"
        GUI.im501_vr_setting.toggle.psm_xsd.active = "yes"
        --GUI.im501_vr_setting.toggle.psm_sd.active = "yes"
        GUI.im501_vr_setting.toggle.psm_ns.active = "yes"
        GUI.im501_vr_setting.toggle.aa.active = "yes"
        GUI.im501_vr_setting.button.all_on.active = "yes"
        GUI.im501_vr_setting.button.default.active = "yes"
        GUI.im501_vr_setting.toggle.psap1.active = "yes"
        GUI.im501_vr_setting.toggle.psap2.active = "yes"
        GUI.im501_vr_setting.toggle.psap3.active = "yes"
        GUI.im501_vr_setting.toggle.psap4.active = "yes"
        GUI.im501_vr_setting.toggle.psap5.active = "yes"
        --GUI.im501_vr_setting.toggle.psap6.active = "yes"
        --GUI.im501_vr_setting.toggle.psap7.active = "yes"
        --GUI.im501_vr_setting.toggle.psap8.active = "yes"
    elseif active == "no" or active == "NO" then
    --[[
        GUI.im501_vr_setting.toggle.nm_avd.value = "OFF"
        GUI.im501_vr_setting.toggle.nm_xsd.value = "OFF"
        GUI.im501_vr_setting.toggle.nm_sd.value = "OFF"
        GUI.im501_vr_setting.toggle.nm_ns.value = "OFF"
        GUI.im501_vr_setting.toggle.psm_avd.value = "OFF"
        GUI.im501_vr_setting.toggle.psm_xsd.value = "OFF"
        GUI.im501_vr_setting.toggle.psm_sd.value = "OFF"
        GUI.im501_vr_setting.toggle.psm_ns.value = "OFF"
        GUI.im501_vr_setting.toggle.aa.value = "OFF"
        GUI.im501_vr_setting.toggle.nm_avd:action()
        GUI.im501_vr_setting.toggle.nm_xsd:action()
        GUI.im501_vr_setting.toggle.nm_sd:action()
        GUI.im501_vr_setting.toggle.nm_ns:action()
        GUI.im501_vr_setting.toggle.psm_avd:action()
        GUI.im501_vr_setting.toggle.psm_xsd:action()
        GUI.im501_vr_setting.toggle.psm_sd:action()
        GUI.im501_vr_setting.toggle.psm_ns:action()
        GUI.im501_vr_setting.toggle.aa:action()
        ]]
        GUI.im501_vr_setting.list.nm_xsd_adc.active = "no"
        GUI.im501_vr_setting.list.nm_sd_adc.active = "no"
        GUI.im501_vr_setting.list.nm_ns.active = "no"
        GUI.im501_vr_setting.list.psm_xsd_adc.active = "no"
        GUI.im501_vr_setting.list.psm_sd_adc.active = "no"
        GUI.im501_vr_setting.list.psm_ns.active = "no"
        GUI.im501_vr_setting.label.xsd_keyword.active = "no"
        GUI.im501_vr_setting.list.xsd.active = "no"
        GUI.im501_vr_setting.label.sd_keyword.active = "no"
        GUI.im501_vr_setting.list.sd.active = "no"
        GUI.im501_vr_setting.label.mic_sel.active = "no"
        GUI.im501_vr_setting.list.mic_sel.active = "no"
        GUI.im501_vr_setting.toggle.nm_avd.active = "no"
        GUI.im501_vr_setting.toggle.nm_xsd.active = "no"
        GUI.im501_vr_setting.toggle.nm_sd.active = "no"
        GUI.im501_vr_setting.toggle.nm_ns.active = "no"
        GUI.im501_vr_setting.toggle.psm_avd.active = "no"
        GUI.im501_vr_setting.toggle.psm_xsd.active = "no"
        GUI.im501_vr_setting.toggle.psm_sd.active = "no"
        GUI.im501_vr_setting.toggle.psm_ns.active = "no"
        GUI.im501_vr_setting.toggle.aa.active = "no"
        GUI.im501_vr_setting.button.all_on.active = "no"
        GUI.im501_vr_setting.button.default.active = "no"
        GUI.im501_vr_setting.toggle.psap1.active = "no"
        GUI.im501_vr_setting.toggle.psap2.active = "no"
        GUI.im501_vr_setting.toggle.psap3.active = "no"
        GUI.im501_vr_setting.toggle.psap4.active = "no"
        GUI.im501_vr_setting.toggle.psap5.active = "no"
        --GUI.im501_vr_setting.toggle.psap6.active = "no"
        --GUI.im501_vr_setting.toggle.psap7.active = "no"
        --GUI.im501_vr_setting.toggle.psap8.active = "no"
    end
end

GUI.im501_vr_setting.active("no")

