----If variable has this mark "--**",
----  means you must check or modify it before start test!!!!
----Test Variable
    TESTER = "WJ"           --**
    TEST_PC = "Thinkpad T430"    --**
    OS_TYPE = "windows7"    --**

    --
    SAMPLE_RATE_S = tostring(SAMPLE_RATE / 1000).."k"

    ENGINE_DELAY=150
    SPKOUT_SRC_MAX = SPKOUT_SRC_MAX or 4
    LOUT_SRC_MAX = LOUT_SRC_MAX or 4

    if GUI.audio_path.list.slot_num.value+0 < 5 then
        SPKOUT_SRC_MAX = GUI.audio_path.list.slot_num.value+0
        LOUT_SRC_MAX = 0
    else
        SPKOUT_SRC_MAX = 4
        LOUT_SRC_MAX = GUI.audio_path.list.slot_num.value-4
    end
    --DEBUG = 1 --**

  ----Input node
    AB_MICIN_ENABLE = GUI.ab_setting.toggle.ab_in.value == "ON" and 1 or 0  --**
      MIC_MICS = {
        {r=0, m=0},  --**
        {r=0, m=1},  --**
        {r=0, m=2},  --**
        {r=0, m=3},  --**
        {r=0, m=4},  --**
        {r=0, m=5},  --**
      }

      AB_LIN_ENABLE = 0  --**           -- benchmark
      if AB_MICIN_ENABLE == 1 then
          local mic_num = GUI.ab_setting.list.ab_in.value + 0
          printc("\nAudiobridge input channel number: "..mic_num)
          if mic_num < 6 then
              for i = 1, 6-mic_num do table.remove(MIC_MICS, #MIC_MICS) end
          elseif mic_num > 6 then
              --AB_LIN_ENABLE = 1
	      for i = 7, mic_num do table.insert(MIC_MICS, {r=0, m=mic_num-1}) end
          end
          AB_MICIN = {}--{0,1,2,3,}--4,5,6,7}   --**
          if AB_MICIN_ENABLE == 1 then
              for i = 1, #MIC_MICS do AB_MICIN[i] = i-1 end
          end
          AB_LIN = AB_LIN_ENABLE == 0 and {} or {#MIC_MICS, #MIC_MICS+1}
          print(GUI.ab_setting.toggle.gpio_in.value)
          print(GUI.ab_setting.toggle.gpio_in.active)
          GPIO_REC_BIT_MASK = 0
          GPIO_REC = {}
          --if (GUI.ab_setting.toggle.gpio_in.active == "yes" or GUI.ab_setting.toggle.gpio_in.active == "YES") and GUI.ab_setting.toggle.gpio_in.value == "ON" then
          if GUI.ab_setting.toggle.gpio_in.value == "ON" then
              local gpio_list = GUI.ab_setting.list.gpio_in.value_tab
              --print(gpio_list)
              local gpio_mask = 0 --gpio_list[#gpio_list]
              local gpio_index = {} --gpio_mask == 1 and {#gpio_list+1} or {} --gpio start number is gpio2
              for i = 1, #gpio_list do
                  if gpio_list[i] == 1 then table.insert(gpio_index, i+1) end
                  gpio_mask = bit:_lshift(gpio_mask,1) + gpio_list[#gpio_list - i + 1]
                  --print(gpio_mask)
              end
              GPIO_REC_BIT_MASK = gpio_mask
              GPIO_REC =gpio_index
              printc("GPIO mask: "..string.format("0x%0x", gpio_mask))
              printc(gpio_index)
          end
      end
--------
    AB_SPIIN_ENABLE = GUI.ab_setting.toggle.spi_in.value == "ON" and 1 or 0
      if AB_SPIIN_ENABLE == 1 then
          local spi_list = GUI.ab_setting.list.spi_in.value_tab
          --print(gpio_list)
          printc("\nSPI input channel number: "..#spi_list)
          printc("SPI input list:")
          printc(spi_list)
          local spi_mask = spi_list[#spi_list]
          local channel_num = spi_list[#spi_list]

          for i = 2, #spi_list do
              --if spi_list[#spi_list - i + 1] == 1 then table.insert(SPI_REC, 1, SPI_LIST[#spi_list - i + 1]) end
              spi_mask = bit:_lshift(spi_mask,1) + spi_list[#spi_list - i + 1]
              channel_num = channel_num + spi_list[#spi_list - i + 1]
          end
          SPI_REC_CH_MASK = spi_mask
          --channel_num = SLOT_NUM  ----?
          SPI_IN = channel_num
          printc("\nSPI input channel number: "..channel_num)
          print(unpack(SPI_REC))
          if channel_num < 6 then
              for i = 1, 6-channel_num do table.remove(MIC_MICS, #MIC_MICS) end
          elseif channel_num > 6 then
              for i = 1, channel_num - #MIC_MICS do table.insert(MIC_MICS, {r=0, m=#MIC_MICS+i-1}) end
          end

          DEBUG_INFO_ENABLE = 0
          if (GUI.ab_setting.toggle.debug_info.active == "yes" or GUI.ab_setting.toggle.debug_info.active == "YES") and GUI.ab_setting.toggle.debug_info.value == "ON" then
              if SPI_REC[#SPI_REC] == SPI_LIST[#SPI_LIST] then
                  DEBUG_INFO_SRC_NODE = NODE_SPI_IN
                  DEBUG_INFO_SRC_IDX = #SPI_REC-1
                  DEBUG_INFO_ENABLE = 1
              end
          end

      end
      ---standby

--------
    SOUNDCARD_IN_ENABLE = 0  --**
    if GUI.ab_setting.toggle.sc_in.value == "ON" then
        SOUNDCARD_IN_ENABLE =  1
        INPUT_SOUNDCARD = sc_in_dev_list[GUI.ab_setting.list.sc_in_dev.value+0] --**
        printc("\nRecord Sound Card: "..INPUT_SOUNDCARD)
        SOUNDCARD_IN = SOUNDCARD_IN_ENABLE == 0 and {} or {0,1}
    end
--------
    FILE_ARRAY_ENABLE = 0
    if GUI.ab_setting.toggle.file_in.value == "ON" then
        FILE_ARRAY_ENABLE = 1
        FILE_ARRAY ={}
        NODE_FILE_ARRAY = {}

        INPUT_FILE_ARRAY = GUI.ab_setting.text.file_in.name
       	INPUT_FILE_CHANNEL = GUI.ab_setting.text.file_in.channel_num + 0
       	local channel_num = INPUT_FILE_CHANNEL
       	channel_num = channel_num > 8 and 8 or channel_num
        for i = 1, channel_num do
          	FILE_ARRAY[i] = i-1
          	NODE_FILE_ARRAY[i] = NODE_FILE_ARRAY_S
    	end
    	if( channel_num < 8 )then
    		for i = channel_num +1, 8 do
    			FILE_ARRAY[i] = 0
          		NODE_FILE_ARRAY[i] = NODE_SILENCE
          	end
    	end
        PLAY_MODE = "loop"  --"once", "extend_silence"
        --PLAY_MODE = "once"  --"once", "extend_silence"
        printc("\nInput file: "..INPUT_FILE_ARRAY)
        printc("Channel number: "..channel_num)
    end

  ----Output node
    SOUNDCARD_OUT_ENABLE = 0
    if GUI.ab_setting.toggle.sc_out.value == "ON" then
        SOUNDCARD_OUT_ENABLE = 1
        OUTPUT_SOUNDCARD = sc_out_dev_list[GUI.ab_setting.list.sc_out_dev.value+0] --**

        local sc_out_l_src = list_convert_node(GUI.ab_setting.list.sc_out_l.value+0)
        local sc_out_r_src = list_convert_node(GUI.ab_setting.list.sc_out_r.value+0)
        SOUNDCARD_OUT_SRC = {sc_out_l_src[1], sc_out_r_src[1]}
        printc("\nPlay Sound Card: "..OUTPUT_SOUNDCARD)
        printc(sc_out_l_src[1])
        printc(sc_out_r_src[1])
    end

    FILE_LOUT_ENABLE = 0
  --  print(GUI.ab_setting.list.file_out.len)
    if GUI.ab_setting.toggle.file_out.value == "ON" and GUI.ab_setting.list.file_out.len+0 > 0 then
        FILE_LOUT_ENABLE = 1
        FILE_LOUT_NAME = GUI.ab_setting.text.file_out.value  --**
        FILE_LOUT_SRC, OUTPUT_CH = list_convert_node(GUI.ab_setting.list.file_out)
        
        print(SAMPLE_RATE, SPI_EN, DSP_NAME)
        if SAMPLE_RATE == 16000 and SPI_EN and DSP_NAME == "iM501" then
            if SPI_CHANNEL==nil or SPI_CHANNEL == 6 then
                FILE_LOUT_SRC = 
                {
                    {NODE_AB_MICIN, 0, "SPI0"};
                    {NODE_AB_MICIN, 1, "SPI1"};
                    {NODE_AB_MICIN, 2, "SPI2"};
                    {NODE_AB_MICIN, 3, "SPI3"};
                    {NODE_AB_MICIN, 4, "SPI4"};
                    {NODE_AB_MICIN, 5, "SPI5"};
                    --{NODE_AB_MICIN, 6, "SPI"};
                }
            else
                FILE_LOUT_SRC = 
                {
                    {NODE_AB_MICIN, 0, "SPI0"};
                    {NODE_AB_MICIN, 1, "SPI1"};
                    {NODE_AB_MICIN, 2, "SPI2"};
                    {NODE_AB_MICIN, 3, "SPI3"};
                    {NODE_AB_MICIN, 4, "SPI4"};
                    {NODE_AB_MICIN, 5, "SPI5"};
                    {NODE_AB_MICIN, 6, "SPI6"};
                    {NODE_AB_MICIN, 7, "SPI7"};
                }
            end
        elseif SAMPLE_RATE == 16000 and AUDIO_BUS_TYPE == 1 and DSP_NAME == "iM501" then
            if VB_CHANNEL == nil or VB_CHANNEL == 1 then
                FILE_LOUT_SRC = 
                {
                    {NODE_AB_MICIN, 0, "PDM0_Left"};
                    {NODE_AB_MICIN, 1, "PDM0_Right"};
                    {NODE_AB_MICIN, 2, "PDM1_Left"};
                    {NODE_AB_MICIN, 3, "PDM1_Right"};
                    {NODE_AB_MICIN, 6, "AVD"};
                    {NODE_AB_MICIN, 5, "xSD"};
                    {NODE_AB_MICIN, 4, "IRQ"};
                    {NODE_AB_MICIN, 7, "Voice Buffer"};
                }
            else
                FILE_LOUT_SRC = 
                {
                    {NODE_AB_MICIN, 0, "PDM0_Left"};
                    {NODE_AB_MICIN, 1, "PDM0_Right"};
                    {NODE_AB_MICIN, 2, "PDM1_Left"};
                    {NODE_AB_MICIN, 3, "PDM1_Right"};
                }
                for i = 1, VB_CHANNEL do
                    table.insert(FILE_LOUT_SRC, {NODE_AB_MICIN, 3+i, "Voice Buffer "..tostring(i)})
                end
            end
        end
        printc("\nOutput file: "..FILE_LOUT_NAME)
        for i = 1, #FILE_LOUT_SRC do printc(FILE_LOUT_SRC[i]) end
    end

    AB_SPIOUT_ENABLE = 0
    AB_SPKOUT_ENABLE = 0
    AB_LOUT_ENABLE = 0
    AB_OUT_CHANNEL = 0
    AB_SPKOUT_SRC, AB_LOUT_SRC = {}, {}
  --  print(GUI.ab_setting.list.ab_out.len)
    if GUI.ab_setting.toggle.ab_out.value == "ON" and GUI.ab_setting.list.ab_out.len+0 > 0 then
        AB_SPKOUT_ENABLE = 1
        local temp_tab = list_convert_node(GUI.ab_setting.list.ab_out)
        if #temp_tab < 5 then
            AB_SPKOUT_SRC = temp_tab
            AB_OUT_CHANNEL = #AB_SPKOUT_SRC
        else
            AB_SPKOUT_SRC, AB_LOUT_SRC = {}, {}
            AB_LOUT_ENABLE = 1
            for i = 1, #temp_tab do
                if i<5 then
                    AB_SPKOUT_SRC[i] = temp_tab[i]
                else
                    AB_LOUT_SRC[i-4] = temp_tab[i]
                end
            end
            AB_OUT_CHANNEL =  #AB_SPKOUT_SRC + #AB_LOUT_SRC
            printc("\nAB Lout: ")
            for i = 1, #AB_LOUT_SRC do printc(AB_LOUT_SRC[i]) end
        end
        printc("\nAB Spkout: ")
        for i = 1, #AB_SPKOUT_SRC do printc(AB_SPKOUT_SRC[i]) end
    end

  --  print(GUI.ab_setting.list.spi_out.len)
    if GUI.ab_setting.toggle.spi_out.value == "ON" and GUI.ab_setting.list.spi_out.len+0 > 0 then
        AB_SPKOUT_ENABLE = 1
        AB_SPIOUT_ENABLE = 1
        local temp_tab = list_convert_node(GUI.ab_setting.list.spi_out)

        if #temp_tab < 5 then
            AB_SPKOUT_SRC = temp_tab
            AB_OUT_CHANNEL = #AB_SPKOUT_SRC
        else
            AB_SPKOUT_SRC, AB_LOUT_SRC = {}, {}
            AB_LOUT_ENABLE = 1
            for i = 1, #temp_tab do
                if i<5 then
                    AB_SPKOUT_SRC[i] = temp_tab[i]
                else
                    AB_LOUT_SRC[i-4] = temp_tab[i]
                end
            end
            AB_OUT_CHANNEL =  #AB_SPKOUT_SRC + #AB_LOUT_SRC
            printc("\nAB Lout: ")
            --print(#AB_LOUT_SRC)
            for i = 1, #AB_LOUT_SRC do printc(AB_LOUT_SRC[i]) end
        end
        printc("\nAB Spkout: ")
        --printc(#AB_SPKOUT_SRC)
        for i = 1, #AB_SPKOUT_SRC do printc(AB_SPKOUT_SRC[i]) end
    end

    WAVEFORM_ENABLE = 0
  --  print(GUI.ab_setting.list.waveview.len)
    if GUI.ab_setting.toggle.waveview.value == "ON" and GUI.ab_setting.list.waveview.len+0 > 0 then
        WAVEFORM_ENABLE = 1
        WAVEFORM_SRC = list_convert_node(GUI.ab_setting.list.waveview)

        if SAMPLE_RATE == 16000 and (AUDIO_BUS_TYPE == 1 or SPI_EN) and DSP_NAME == "iM501" then
             WAVEFORM_SRC = FILE_LOUT_SRC
        end

        printc("\nWaveview: ")
        for i = 1, #WAVEFORM_SRC do printc(WAVEFORM_SRC[i]) end
    end

  ----Volume control
    VOLUME_MICIN = 6  --**  -- 0~18dB, 6dB step
    VOLUME_LOUT = 0     --**  -- MAX is 0dB, 1.5dB step.  -37.5
    VOLUME_SPKOUT = 0   --**  -- MAX is 0dB, 1.5dB step.  -1.5
    VOLUME_LIN = 0

    if AB_MICIN_ENABLE ~= 1 and AB_LIN_ENABLE ~=1 and AB_LOUT_ENABLE ~= 1 and AB_SPKOUT_ENABLE ~= 1 and AB_SPIIN_ENABLE ~= 1 and AB_SPIOUT_ENABLE ~= 1 then
        AB_ENABLE = 0
        message("Warning", "No path of UIF been used, Please check setting!")
    else
        AB_ENABLE = 1
    end

    print("\n\n")
