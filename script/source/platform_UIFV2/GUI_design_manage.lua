----first setp
--require(SCRIPT_PATH.."init_GUI")
printc("Define Design Manage GUI module.")
require(SCRIPT_PATH..sub_dirctory.."GUI_design_manage_function")
----Read design folder, get design group and list.
local rfdesign_path = DESIGN_PATH..DSP_NAME.."\\RfDesign\\"
local mydesign_path = DESIGN_PATH..DSP_NAME.."\\MyDesign\\"
local rfdesign = misc.find_names("*.json", rfdesign_path, 1)
local mydesign = misc.find_names("*.json", mydesign_path, 1)
table.sort(rfdesign)
table.sort(mydesign)
print(unpack(rfdesign))
print(unpack(mydesign))

----found out design list
local design_group = get_design_list(rfdesign, mydesign)
group = {}
design = {}
design_path = rfdesign_path
local mydesign_offset = #design_group.rfdesign + 1
for i = 1, #design_group.rfdesign do
    print(design_group.rfdesign[i].name)
    print(unpack(design_group.rfdesign[i].design))
    group[i] = {name = design_group.rfdesign[i].name, type = "rfdesign"}
    design[i] = design_group.rfdesign[i].design
    for j = 1, #design[i] do
        design[i][j] = string.sub(design[i][j], 1, -6)
    end
end
for i = 1, #design_group.mydesign do
    print(design_group.mydesign[i].name)
    print(unpack(design_group.mydesign[i].design))
    table.insert(group, {name = design_group.mydesign[i].name, type = "mydesign"})
    table.insert(design, design_group.mydesign[i].design)
    for j = 1, #design[#design] do
        design[#design][j] = string.sub(design[#design][j], 1, -6)
    end
end

--stop()
--
GUI.design_manage = {}
GUI.design_manage.constant =
{
}

GUI.design_manage.label = 
{
    group = iup.label{size="55x10";title = [==[Design Group:]==]};
    design = iup.label{size="55x20";title = "Reference\n Design:"};
    design_name = iup.label{size="190x8";title = [==[]==]};
    note = iup.label{size="190x16";title = [==[]==]};
    parting_line = iup.label{size="190x5";title = [==[-----------------------------------------------------------]==]};
    import_name = iup.label{size="55x10";title = [==[Design Name:]==]};
    import_path = iup.label{size="55x10";title = [==[Design Path:]==]};
    import_note = iup.label{size="200x10";title = [==[Please browse the folder to select the design]==]};
    rename_note = iup.label{size="150x24";title = [==[Please input a new design name, the name can't contain any of the follow characters: "\/+-:*?<>|="]==]};
    new_name = iup.label{size="55x10";title = [==[New Name:]==]};
    duplicate_group = iup.label{size="55x10";title = [==[Duplicate To:]==]};
    duplicate_name = iup.label{size="55x10";title = [==[Design Name:]==]};
    duplicate_note = iup.label{size="163x16";title = [==[Please select a group to save the selected design, input new design name(s)]==]};
    new_group_note = iup.label{size="160x24";title = [==[Please input a new my design group name, the name can't contain any of the follow characters: "\/+-:*?<>|="]==]};
    new_group_name = iup.label{size="55x10";title = [==[New Name:]==]};  
}

GUI.design_manage.toggle = 
{
    edit = iup.toggle{title="Edit Enable",value="OFF";};
}

GUI.design_manage.list = 
{
    group = iup.list
    {   
        dropdown="YES", visible_items=4, value = 1, len = 0, type="rfdesign", size="130x", active = "yes", action_time = false,
    };
    design = iup.list
    {
        multiple="YES", dropdown="NO", visible_items=8, value = 0, len = 0, size="130x80", active = "yes", action_time = false,
    };
    duplicate_group = iup.list
    {   
        dropdown="YES", visible_items=4, value = 1, len = 0, size="100x", active = "yes", action_time = false,
    };
}

GUI.design_manage.text =
{
    design = iup.text{value = "", size="95x"};
    import_name = iup.text{value = "", size="95x"};
    import_path = iup.text{value = "", size="95x"};
    new_name = iup.text{value = "", size="95x"};
    duplicate_name = iup.text{value = "", size="100x"};
    new_group_name = iup.text{value = "", size="95x"};
}

GUI.design_manage.button =
{
    export = iup.button{size="42x", title="Export", active = "no"};
    import = iup.button{size="42x", title="Import", active = "no"};
    duplicate = iup.button{size="42x", title="Duplicate", active = "no"};
    delete = iup.button{size="42x", title="Delete", active = "no"};
    open = iup.button{size="42x", title="Open", active = "no"};
    back = iup.button{size="42x", title="Back", active = "yes"};
    rename = iup.button{size="42x", title="Rename", active = "no"};
    browse_import = iup.button{size="42x", title="Browse", active = "yes"};
    confirm_import = iup.button{size="42x", title="Confirm", active = "yes"};
    cancel_import = iup.button{size="42x", title="Cancel", active = "yes"};
    confirm_duplicate = iup.button{size="42x", title="Confirm", active = "yes"};
    cancel_duplicate = iup.button{size="42x", title="Cancel", active = "yes"};
    confirm_rename = iup.button{size="42x", title="Confirm", active = "yes"};
    cancel_rename = iup.button{size="42x", title="Cancel", active = "yes"};
    confirm_new_group = iup.button{size="42x", title="Confirm", active = "yes"};
    cancel_new_group = iup.button{size="42x", title="Cancel", active = "yes"};
}

GUI.design_manage.dialog =
{
    main = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {   
                GUI.design_manage.label.group;
                GUI.design_manage.list.group;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.vbox{
                iup.hbox
                {
                    GUI.design_manage.label.design;
                    GUI.design_manage.list.design;
                    margin="0x0", gap="5x0", alignment = "ACENTER",
                };
                iup.vbox
                {
                    GUI.design_manage.label.design_name;
                    GUI.design_manage.label.note;
                    margin="0x0", gap="0x0", alignment = "ACENTER",
                };
                GUI.design_manage.label.parting_line;
                margin="10x5", gap="5x0"--, alignment = "ACENTER",
            };
            iup.vbox
            {   
                iup.hbox
                {
                    GUI.design_manage.button.export;
                    iup.label{title = "|", fgcolor = "220 220 220"};
                    GUI.design_manage.button.duplicate;
                    iup.label{title = "|", fgcolor = "220 220 220"};
                    GUI.design_manage.button.rename;
                    margin="5x0", gap="18", alignment = "ACENTER",
                };
                iup.hbox
                {
                    GUI.design_manage.button.import;
                    iup.label{title = "|", fgcolor = "220 220 220"};
                    GUI.design_manage.button.delete;
                    iup.label{title = "|", fgcolor = "220 220 220"};
                    GUI.design_manage.button.open;
                    --GUI.design_manage.button.back;
                    margin="5x0", gap="18", alignment = "ACENTER",
                };
                margin="4x5", gap="5x0"; alignment = "LCENTER",
            };
        };
        title="Design Manager", margin="5x5", gap="5" --size="450x420", dropdown="YES"
    };
    import = iup.dialog
    {
        iup.vbox
        {
            iup.hbox{GUI.design_manage.label.import_note;margin="10x5", gap="5x0";};
            iup.hbox
            {   
                GUI.design_manage.label.import_name;
                GUI.design_manage.text.import_name;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.hbox
            {   
                GUI.design_manage.label.import_path;
                GUI.design_manage.text.import_path;
                GUI.design_manage.button.browse_import;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.hbox
            {   
                GUI.design_manage.button.confirm_import;
                GUI.design_manage.button.cancel_import;
                margin="50x5", gap="50x0";alignment = "ACENTER";
            };
            margin="10x5", gap="5x0";alignment = "LCENTER";
        };
        title="Import Design", margin="5x5", gap="5"
    };
    rename = iup.dialog
    {
        iup.vbox
        {
            iup.hbox{GUI.design_manage.label.rename_note;margin="10x5", gap="5x0";};
            iup.hbox
            {   
                GUI.design_manage.label.new_name;
                GUI.design_manage.text.new_name;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.hbox
            {   
                GUI.design_manage.button.confirm_rename;
                GUI.design_manage.button.cancel_rename;
                margin="25x5", gap="50x0";alignment = "ACENTER";
            };
            margin="10x5", gap="5x0";alignment = "LCENTER";
        };
        title="Rename Design", margin="5x5", gap="5"
    };
    duplicate = iup.dialog
    {
        iup.vbox
        {
            iup.hbox{GUI.design_manage.label.duplicate_note;margin="10x5", gap="5x0";};
            iup.hbox
            {   
                GUI.design_manage.label.duplicate_group;
                GUI.design_manage.list.duplicate_group;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.hbox
            {   
                GUI.design_manage.label.duplicate_name;
                GUI.design_manage.text.duplicate_name;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.hbox
            {   
                GUI.design_manage.button.confirm_duplicate;
                GUI.design_manage.button.cancel_duplicate;
                margin="30x5", gap="50x0";alignment = "ACENTER";
            };
            margin="10x5", gap="5x0";alignment = "LCENTER";
        };
        title="Duplicate Design", margin="5x5", gap="5"
    };
    new_group = iup.dialog
    {
        iup.vbox
        {
            iup.hbox{GUI.design_manage.label.new_group_note;margin="10x5", gap="5x0";};
            iup.hbox
            {   
                GUI.design_manage.label.new_group_name;
                GUI.design_manage.text.new_group_name;
                margin="10x5", gap="5x0";alignment = "ACENTER";
            };
            iup.hbox
            {   
                GUI.design_manage.button.confirm_new_group;
                GUI.design_manage.button.cancel_new_group;
                margin="30x5", gap="50x0";alignment = "ACENTER";
            };
            margin="10x5", gap="5x0";alignment = "LCENTER";
        };
        title="Duplicate Design", margin="5x5", gap="5"
    };
}

GUI.design_manage.list.group = update_group_list(GUI.design_manage.list.group, group, 1)
GUI.design_manage.list.design = update_design_list(GUI.design_manage.list.design, design[GUI.design_manage.list.group.value+0])
GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[GUI.design_manage.list.group.value+0], 
                                                  get_select_value_from_list(GUI.design_manage.list.design) )
update_button_state(GUI.design_manage.list.group.type, GUI.design_manage.list.design.value)



----------------------------------------------------------------
GUI.design_manage.button.export.action = function()
    printc("Export selected design.")

    local fd=iup.filedlg{dialogtype="SAVE", title="Export Selected Design: "..GUI.design_manage.label.design_name.title, 
                         nochangedir="YES", value=get_select_value_from_list(GUI.design_manage.list.design),
                         filter="*.json", filterinfo="*.json", allownew="YES"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "0") or (status == "1") then -- 0 normal; 1,new; -1, cancel
        if string.find(fd.value, ".json") == nil then
            fd.value = fd.value..".json"
        end
        printc(fd.value)
        print(design_path..GUI.design_manage.label.design_name.title)
        os.execute("copy \""..design_path..GUI.design_manage.label.design_name.title..".json\" \""..fd.value.."\" ".."/Y")
    end

    fd:destroy()
end

----------------------------------------------------------------
GUI.design_manage.button.import.action = function()
    GUI.design_manage.text.import_name.value = ""
    --GUI.design_manage.text.import_path.value = ""
    GUI.design_manage.dialog.import:popup()
end

GUI.design_manage.button.browse_import.action = function()
    printc("Select New Design to Import.")
    local fd=iup.filedlg{dialogtype="OPEN", title="Import a New Design to My design Group: "..GUI.design_manage.list.group[GUI.design_manage.list.group.value+0], 
                         nochangedir="NO", value="", directory=GUI.design_manage.text.import_path.value,
                         filter="*.json", filterinfo="Json File", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot Import file "..fd.value)
        end
    else
        if string.find(fd.value, "json") == nil then
            message("ERROR", "The file of selected should be wrong, please double check!")
        else
            local str = get_filename_from_folder(fd.value)
            fd.directory = string.sub(fd.value, 1, -(string.len(str))-1)
            GUI.design_manage.text.import_name.value = str
            GUI.design_manage.text.import_path.value = fd.directory
            
            os.execute("copy \""..fd.value.."\" "..DESIGN_PATH..DSP_NAME.."\\".." ".."/Y")
            os.execute("ren \""..DESIGN_PATH..DSP_NAME.."\\"..str.."\" \"temp.json\"")
            print(fd.directory, fd.value)
        end
    end
    fd:destroy()
end

GUI.design_manage.button.confirm_import.action = function()
    GUI.design_manage.text.import_name.action(self,13,GUI.design_manage.text.import_name.value)
    local str = GUI.design_manage.text.import_name.value
    local design_name = string.sub(str, 1, -6)

    local tab = design[GUI.design_manage.list.group.value+0]
    local flag = 0
    for i = 1, #tab do
        if design_name == tab[i] then 
            message("Rename", "There is a design with the same name exits in this design group, please rename.")
            flag = 1
            break
        end
    end
    if flag == 0 then
        local path = GUI.design_manage.list.group[GUI.design_manage.list.group.value+0] == "No Group" and design_path
                     or design_path..GUI.design_manage.list.group[GUI.design_manage.list.group.value+0].."\\"
        table.insert(design[GUI.design_manage.list.group.value+0], design_name)
        os.execute("copy "..DESIGN_PATH..DSP_NAME.."\\temp.json".." \""..path.."\" ".."/Y")
        os.execute("ren \""..path.."temp.json\"".." \""..str.."\"")
        os.execute("del \""..DESIGN_PATH..DSP_NAME.."\\temp.json\"")
        GUI.design_manage.list.design = update_design_list(GUI.design_manage.list.design, design[GUI.design_manage.list.group.value+0], 1)
        table.sort(design[GUI.design_manage.list.group.value+0])
        GUI.design_manage.dialog.import:hide()
        printc("Import Successfully.")
    end
end

GUI.design_manage.button.cancel_import.action = function()
    printc("Import cancel.")
    GUI.design_manage.dialog.import:hide()
end

GUI.design_manage.text.import_name.action = function(self,c,new_value)
    if tonumber(c) == 13 then
        if string.find(new_value, ".json") == nil then
            new_value = new_value..".json"
        end
        GUI.design_manage.text.import_name.value = new_value
        printc("New design name: "..GUI.design_manage.text.import_name.value)
    end
end

----------------------------------------------------------------
GUI.design_manage.button.duplicate.action = function(saveas)
    SAVEAS_DESIGN = saveas or 0
    local tab = {}
    for i = mydesign_offset, #group do tab[i - mydesign_offset + 1] = group[i].name end
    table.insert(tab, "New Mydesign Group")
    print(unpack(tab))
    GUI.design_manage.list.duplicate_group = update_normal_list(GUI.design_manage.list.duplicate_group, tab)

    local str = ""
    local select_design = get_select_value_from_list(GUI.design_manage.list.design)
    for i = 1, #select_design do str = str..select_design[i]..";" end
    GUI.design_manage.text.duplicate_name.value = string.sub(str, 1, -2)
    if SAVEAS_DESIGN == 1 then
        GUI.design_manage.label.duplicate_group.title = "Save To: "
        GUI.design_manage.dialog.duplicate.title = "Save As Manage"
    else
        GUI.design_manage.label.duplicate_group.title = "Duplicate To:"
        GUI.design_manage.dialog.duplicate.title = "Duplicate Manage"
    end
    GUI.design_manage.dialog.duplicate:popup()
end

GUI.design_manage.button.confirm_duplicate.action = function()
    local group_name = GUI.design_manage.list.duplicate_group[GUI.design_manage.list.duplicate_group.value+0]
    local source_group = GUI.design_manage.list.group.value+0
    local target_group = GUI.design_manage.list.group.value+0
    local select_design = get_select_value_from_list(GUI.design_manage.list.design)
    local flag = 0

    local str = GUI.design_manage.text.duplicate_name.value
    local index = 1
    local design_tab = {}
    repeat
       index = string.find(str, ";")
        table.insert(design_tab, string.sub(str, 1, (index or 0)-1))
        str = string.sub(str, (index or 0)+1, -1)
    until index == nil
    print(unpack(design_tab))

    if group_name ~= "New Mydesign Group" then
        local tab = design[mydesign_offset+GUI.design_manage.list.duplicate_group.value-1]

        if #select_design ~= #design_tab then
            message("Error", "The number of selected design is not equal the number of new design name.")
            flag = 1
        else
            for i = 1, #tab do
                for j = 1, #design_tab do
                    if design_tab[j] == tab[i] then 
                        message("Rename", "There is a design with the same name exits in this design group: "..design_tab[j]..", please rename.")
                        flag = 1
                        break
                    end
                end
            end
        end
        target_group = mydesign_offset+GUI.design_manage.list.duplicate_group.value-1
    else
        GUI.design_manage.text.new_group_name.value = ""
        GUI.design_manage.dialog.new_group:popup()
        print(GUI.design_manage.text.new_group_name.value)
        local str = string.gsub(GUI.design_manage.text.new_group_name.value, " ", "")
        if str ~= "" then
            table.insert(group, {name = GUI.design_manage.text.new_group_name.value, type = "mydesign"})
            table.insert(design, {})
            GUI.design_manage.list.group = update_group_list(GUI.design_manage.list.group, group, #group)
            os.execute("md \""..mydesign_path..GUI.design_manage.text.new_group_name.value.."\"")
        else
            message("Error", "New group name is invalid.")
            flag = 1
        end
        target_group = GUI.design_manage.list.group.value+0
    end
    
    if flag == 0 then
        local path = GUI.design_manage.list.group[target_group] == "No Group" and mydesign_path
                     or mydesign_path..GUI.design_manage.list.group[target_group].."\\"
        for i = 1, #select_design do 
            table.insert(design[target_group], design_tab[i] or select_design[i])  -- in case #select_design ~= #design_tab
            print(design_path..GUI.design_manage.list.group[source_group].."\\"..select_design[i], design_tab[i])
            local note = ""
            if SAVEAS_DESIGN == 1 then
                os.execute("copy \""..DESIGN_PATH..DSP_NAME.."\\temp.json\"".." \""..path..(design_tab[i] or select_design[i])..".json\"") --" \""..mydesign_path..GUI.design_manage.list.group[target_group].."\\"..tostring(i)..".json\" ".."/Y")
                local bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel= update_audio_global_value()
                note = get_audio_info(bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel)
            else
                os.execute("copy \""..design_path..GUI.design_manage.list.group[source_group].."\\"..select_design[i]..".json\"".." \""..path..(design_tab[i] or select_design[i])..".json\"") --" \""..mydesign_path..GUI.design_manage.list.group[target_group].."\\"..tostring(i)..".json\" ".."/Y")
                --os.execute("ren \""..mydesign_path..GUI.design_manage.list.group[target_group].."\\"..select_design[i]..".json\"".." \""..(design_tab[i] or select_design[i])..".json\"")
                note = load_note_info(design_path..GUI.design_manage.list.group[source_group].."\\", select_design[i])
            end
--print("\n\nadfasdfasdf\n"..note)
            add_note_info(path, design_tab[i] or select_design[i], note)
        end

        GUI.design_manage.label.design.title = "My Design:"
        design_path = mydesign_path

        GUI.design_manage.list.group.value = target_group
        GUI.design_manage.list.group.type = group[target_group].type
        GUI.design_manage.list.design = update_design_list(GUI.design_manage.list.design, design[target_group])
        GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[target_group], 
                                                        get_select_value_from_list(GUI.design_manage.list.design) )
        update_button_state(GUI.design_manage.list.group.type, GUI.design_manage.list.design.value)

        print(GUI.design_manage.list.group.type)
        
        GUI.design_manage.dialog.duplicate:hide()
        printc("Duplicate Successfully.")
    end    
end

GUI.design_manage.button.cancel_duplicate.action = function()
    printc("Duplicate cancel.")
    GUI.design_manage.dialog.duplicate:hide()
end

GUI.design_manage.button.confirm_new_group.action = function()
    printc("Input new group name.")
    local str = string.gsub(GUI.design_manage.text.new_group_name.value, " ", "")
    if str == "" then
        message("Error", "New group name is invalid.")
    else
        GUI.design_manage.dialog.new_group:hide()
    end
end

GUI.design_manage.button.cancel_new_group.action = function()
    GUI.design_manage.dialog.new_group:hide()
    printc("New group name cancel.")
end
----------------------------------------------------------------
GUI.design_manage.button.delete.action = function()
    local action = iup.Alarm("Alarm", "\nYou are going to delete the selected design, are you sure?\n", "Delete", "Cancel")
    if action == 1 then
        local select_group = GUI.design_manage.list.group.value+0
        local group_path = design_path..GUI.design_manage.list.group[select_group].."\\"
        if GUI.design_manage.list.group[select_group] == "No Group" then
            group_path = design_path
        end
    
        local select_design = get_select_value_from_list(GUI.design_manage.list.design)
        local str = GUI.design_manage.list.design.value
        local count = 0
    
        for i = 1, string.len(str) do
            if string.sub(str, i, i) == "+" then
                os.execute("del \""..group_path..design[select_group][i - count]..".json\"")
                --print(group_path.."\\"..design[select_group][i - count]..".json")
                delete_note_info(group_path, design[select_group][i - count])
                table.remove(design[select_group], i - count)
                count = count + 1
            end
        end
    
        if #design[select_group] == 0 then
            if GUI.design_manage.list.group[select_group] ~= "No Group" then 
                os.execute("rd \""..group_path.."\"")
            end
            table.remove(group, select_group)
            table.remove(design, select_group)
            GUI.design_manage.list.group = update_group_list(GUI.design_manage.list.group, group, 1)
            GUI.design_manage.label.design.title = "Reference\n Design:"
            design_path = rfdesign_path
        end
    
        select_group = GUI.design_manage.list.group.value+0
        GUI.design_manage.list.design = update_design_list(GUI.design_manage.list.design, design[select_group])
        GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[select_group], 
                                                        get_select_value_from_list(GUI.design_manage.list.design) )
        update_button_state(GUI.design_manage.list.group.type, GUI.design_manage.list.design.value)
    end
end

----------------------------------------------------------------
GUI.design_manage.button.rename.action = function()
    GUI.design_manage.text.new_name.value = get_select_value_from_list(GUI.design_manage.list.design)[1]
    GUI.design_manage.dialog.rename:popup()
end

GUI.design_manage.button.confirm_rename.action = function()
    local new_name = GUI.design_manage.text.new_name.value
    if string.find(new_name, ".json") ~= nil then
        new_name = string.sub(new_name, 1, -6)
    else
        GUI.design_manage.text.new_name.value = GUI.design_manage.text.new_name.value..".json"
    end

    local select_group = GUI.design_manage.list.group.value+0
    local tab = design[select_group]
    local flag = 0
    for i = 1, #tab do
            print(tab[i], new_name)
        if new_name == tab[i] then 
            message("Rename", "There is a design with the same name exits in this design group, please rename.")
            flag = 1
            break
        end
    end
    
    if flag == 0 then
        local path = GUI.design_manage.list.group[select_group] == "No Group" and design_path
                     or design_path..GUI.design_manage.list.group[select_group].."\\"
        local select_design, index = get_select_value_from_list(GUI.design_manage.list.design)
        print(path..select_design[1], GUI.design_manage.text.new_name.value)
        os.execute("ren \""..path..select_design[1]..".json\" \""..GUI.design_manage.text.new_name.value.."\"")

        local select_value = GUI.design_manage.list.design.value
        modify_note_info(path, design[select_group][index[1]], "", new_name)
        design[select_group][index[1]] = new_name
        GUI.design_manage.list.design[index[1]] = tostring(index[1])..", "..new_name
        GUI.design_manage.list.design.value = select_value
        GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[select_group], 
                                                        get_select_value_from_list(GUI.design_manage.list.design) )
        GUI.design_manage.dialog.rename:hide()
    end
end

GUI.design_manage.button.cancel_rename.action = function()
    GUI.design_manage.dialog.rename:hide()
end

GUI.design_manage.text.new_name.action = function(self,c,new_value)
    if tonumber(c) == 13 then
        GUI.design_manage.text.import_name.value = new_value
        printc("New design name: "..GUI.design_manage.text.import_name.value)
    end
end


----------------------------------------------------------------
GUI.design_manage.button.open.action = function()
    GUI.uif_setting.time._1s.run = "YES"
    GUI.uif_setting.time._1s.action_cb()
    local design_name = get_select_value_from_list(GUI.design_manage.list.design)[1]
    JSON_FILE = design_path..GUI.design_manage.list.group[GUI.design_manage.list.group.value+0].."\\"..design_name..".json"
    print(JSON_FILE)

    os.execute("copy \""..JSON_FILE.."\" "..TEMP_JSON)
    printc("Switch UIF Setting.")
    collectgarbage("collect")
    GUI.kernel = load_jsonfile1(TEMP_JSON)
   -- update_system_setting("OFF")
     update_system_setting("ON", "ON")
    if GUI.design_manage.list.group.type == "mydesign" then
        GUI.uif_setting.toggle.edit.value = "ON"
        --GUI.audio_path.active("yes")
        --GUI.ab_setting.active("yes")
    else
        GUI.uif_setting.toggle.edit.value = "OFF"
        --GUI.audio_path.active("no")
        --GUI.ab_setting.active("no")
    end
    GUI.uif_setting.toggle.edit:action()
    --GUI.uif_setting.label.info.title = [==[    UIF Version  : ]==]..UIF.version..[==[. Script Version: []==]..Version.."].\n"..[==[    Setting Info:]==]
    GUI.uif_setting.dialog.main.title = "Design Operation: "..design_name.."    "

    if HW_USB_CONNECTION_STATE == 1 then
        GUI.uif_setting.dialog.main.title = GUI.uif_setting.dialog.main.title.."    online"
    else
        GUI.uif_setting.dialog.main.title = GUI.uif_setting.dialog.main.title.."    offline"
    end
    
    wait(0.2)
    GUI.uif_setting.button.update.title = "Update Setting"

    if DSP_NAME == "iM501" then
        if AUDIO_BUS_TYPE == 1 then
            local str = "  The format of audio bus is PDM, you should\n    Open: jumper 3-4 of J6, jumper 3-4 of J2, jumper 1-2 and 3-4 of J12\n    Short: 1,3,5,7 of J14.\n If you need AEC echo path, you should \n    Short: jumper 1-2 of J6, jumper 1-2 of J2."
            message("iM501 EVM boad jumper setting", str)
        elseif AUDIO_BUS_TYPE == 2 or AUDIO_BUS_TYPE == 3 then
            local str = "  iM501 EVM boad jumper setting \n\n The format of audio bus is I2S/PCM, you should\n Short: jumper 1-2 and 3-4 of J6, jumper 1-2 and 3-4 of J2, jumper 1-2 and 3-4 of J12.\n Open: 1,3,5,7 of J14."
            message("iM501 EVM boad jumper setting", str)
        end
    end

    GUI.uif_setting.dialog.main:show()
    GUI.design_manage.dialog.main:hide()
    
    --local loop_result = iup.MainLoop()
end


GUI.design_manage.list.group.action = function()
    GUI.design_manage.list.group.action_time = not GUI.design_manage.list.group.action_time
    if GUI.design_manage.list.group.action_time then
        GUI.design_manage.list.design = update_design_list(GUI.design_manage.list.design, design[GUI.design_manage.list.group.value+0])
        local type = group[GUI.design_manage.list.group.value+0].type
        GUI.design_manage.list.group.type = type
        if type == "rfdesign" then
            GUI.design_manage.label.design.title = "Reference\n Design:"
            design_path = rfdesign_path
        elseif type == "mydesign" then
            GUI.design_manage.label.design.title = "My Design:"
            design_path = mydesign_path
        end
        
        GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[GUI.design_manage.list.group.value+0], 
                                                  get_select_value_from_list(GUI.design_manage.list.design) )
        update_button_state(GUI.design_manage.list.group.type, GUI.design_manage.list.design.value)
--print(GUI.design_manage.list.design.value)
        printc("Switch Group")
    end
end

GUI.design_manage.list.design.action = function()
--    GUI.design_manage.list.design.action_time = not GUI.design_manage.list.design.action_time
--    if GUI.design_manage.list.design.action_time then
        update_button_state(GUI.design_manage.list.group.type, GUI.design_manage.list.design.value)
        GUI.design_manage.label.design_name.title, GUI.design_manage.label.note.title = update_design_text(GUI.design_manage.list.group[GUI.design_manage.list.group.value+0], 
                                                  get_select_value_from_list(GUI.design_manage.list.design) )

        --print(GUI.design_manage.list.design.value)
--    end
end

function GUI.design_manage.dialog.main:close_cb()
  iup.ExitLoop()
  GUI.design_manage.dialog.main:hide()
  return iup.IGNORE
end

--GUI.design_manage.dialog.main:show()
--local loop_result = iup.MainLoop()
--stop()
