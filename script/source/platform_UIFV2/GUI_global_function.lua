--****NOTE:in iup library, there is no number value, need use tonumber() or +0.
----
GUI = {}
----Color setting
GUI.color = {
    blue = "0 0 255",
    red = "255 0 0",
    black = "0 0 0",
    green = "25 100 25",
    white = "255 255 255",
    gray = "100 100 100",
    yellow = "255 255 128",
}
GUI.constant = 
{
    switch_on = "ON";
    switch_off = "OFF";
}
--module define
OUTPUT = {
    mic0_bypass = "TDM/PDM s1";
    mic1_bypass = "TDM/PDM s2";
    file_in =  "Wavefile CH1";
}

function remove_0x(value)
    if string.find(value, "0x") ~= nil then
        value = string.gsub(value, "0x", "")
    elseif string.find(value, "0X") ~= nil then
        value = string.gsub(value, "0X", "")
    end
    
    if value == "" then
        message("ERROR", "Nothing input")
        return false
    end

    if string.find(value, "%X") ~= nil then
        message("ERROR", "The input contains illegal characters.")
        return false
    end

    return value
end

function s_2_n(str_value)
    local num_value = {}
    for i = 1, string.len(str_value) do
        if string.sub(str_value, i, i) == "+" then
            num_value[i] = 1
        else
            num_value[i] = 0
        end
    end
--    print(str_value)
    print(unpack(num_value))
    return num_value
end

function n_2_s(num_value)
    local str_value = ""
    for i = 1, #num_value do
        if num_value[i] == 1 then
            str_value = str_value.."+"
        else
            str_value = str_value.."-"
        end
    end
    print(str_value)
--    print(unpack(num_value))
    return str_value
end

function update_list_value_tab(list)
    list.value_tab = {}
    for i = 1, list.len+0 do
        if i <= string.len(list.value) and string.sub(list.value, i, i) == "+" then
            list.value_tab[i] = 1
        else
            list.value_tab[i] = 0
        end
    end
    
    print(unpack(list.value_tab))
    return list
end

--**************************************************--
function unsilence_len(wave_data)
    local time_start, time_end = 0, 0
    local sample_THD = 0.012
    local time_last = wave.get_property(wave_data , "time_last")
    local sample_rate = wave.get_property(wave_data , "sample_rate")

    --print("saaaa")

    local sample_table = wave.get_sample(wave_data, 1, time_last * sample_rate - 1)
    local num = 0
    local flg = 0

    for i = 1, #sample_table do
        if sample_table[i] - sample_THD > 0 or sample_table[i] + sample_THD < 0 then
   	        time_start = (math.floor(i / sample_rate * 1000))
   	        printc("delay time is "..time_start.."ms")
   	        break
	    end
    end
      
	for i = #sample_table-1, 1, -1 do
	    if sample_table[i] - sample_THD > 0 or sample_table[i] + sample_THD < 0 then
	        time_end = (math.floor(i / sample_rate * 1000))
		    printc("delay time is "..time_end.."ms")
		    break
	    end
    end
	--print(time_start, time_end)
	return time_start, time_end
end

function separate_multi_file(str)
    local index = 1
    local directory, file = "", {}
    repeat
        index = string.find(str, "|")
        local before_index = string.sub(str, 1, (index or 0) - 1)
        local after_index = string.sub(str, (index or 0)+1, -1)
        if string.len(before_index) > 0 then
            table.insert(file, before_index)
        end
        str = after_index
        --print(before_index, after_index)
    until index == nil
    if #file > 1 then 
        directory = file[1].."\\"
        table.remove(file, 1) -- the first one is directory.
    else
        index = string.find(file[1], "\\")
        directory = string.sub(file[1], 1, (index or 0) - 1).."\\"  -- in case index == nil
        file[1] = string.sub(file[1], (index or 0)+1, -1)
    end
    --print(directory, #file)
    print(unpack(file))
    return directory, file
end

