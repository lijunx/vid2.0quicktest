FM36 = {I2C_addr = 0xC0}
    ATTRI_FM36_PATH_NORMAL = 0
    ATTRI_FM36_PATH_PWD_BP = 1
FM36.__help = {}
--
--CODE_VEC_PATH = NOAH_PATH.."DSP\\FM36\\"

FM36.vec =      --** vec files. Pls put these files in the path of CODE_VEC_PATH.
      {
        frame_counter  = 0x2306,
	    code_ready     = 0x22fb,
      }

if AUDIO_BUS_TYPE > 1 then
    FM36.vec.init = "init_I2S.vec" 
    FM36.vec.run   = "run_I2S.vec"
    FM36.patch_code =
      {
        {0X3f00, "patch_code_I2S.exe"},
	  }
elseif AUDIO_BUS_TYPE == 1 then
    FM36.vec.init = "init_PDM.vec" 
    FM36.vec.run   = "run_PDM.vec"
    FM36.patch_code =
      {
        {0X3f00, "patch_code_PDM.exe"},
      }
end

FM36.CMD = {
         SYN_0     = 0xFC,
         SYN_1     = 0xF3,
         DM_WR     = 0x3B,
         DM_RD     = 0x37,
         PM_WR     = 0x0D,
         PM_RD     = 0x07,
         CM_WR     = 0x2B,
         CM_RD     = 0x27,
         HOST_WR_1 = 0x68,
         HOST_WR_2 = 0x6A,
         HOST_RD   = 0x60,
         DSP_WR_1  = 0x58,
         DSP_WR_2  = 0x5A,
         DSP_RD    = 0x56,
         TYPE_DM   = 0x00,
         TYPE_PM   = 0x01,
         TYPE_CM   = 0x02,
}
FM36.ROM_ID = 0x600c
--FM36 unlock_mmreg is different from previous dsp
DSP_PM_Type = 1 ;  -- 1: FM36; 0 : others DSP

--------------------------    for FM36-600C on AB03      ------------------------

FM36.patch_code = 
{
    0x003F00,  
    0x3C0065,  
    0x43700A,  
    0x377201,  
    0x17F05E,  
    0x6800A5,  
    0x22629F,  
    0x3C0045,  
    0x43730A,  
    0x377261,  
    0x17F0BE,  
    0x6800A5,  
    0x22629F,  
    0x44000A,  
    0x19E78F,  
    0x81012A,  
    0x877200,  
    0x6800A1,  
    0x977200,  
    0x81013A,  
    0x877220,  
    0x6800A1,  
    0x977220,  
    0x81014A,  
    0x877240,  
    0x6800A1,  
    0x977240,  
    0x81011A,  
    0x19BFCF,  
    0x877210,  
    0x6000A1,  
    0x977210,  
    0x9101DA,  
    0x877230,  
    0x6000A1,  
    0x977230,  
    0x9101EA,  
    0x877250,  
    0x6000A1,  
    0x977250,  
    0x9101FA,  
    0x862450,  
    0x19C27F,  
    0x8A2C72,  
    0x7000AA,  
    0x877260,  
    0x6800A1,  
    0x977260,  
    0x8A2C82,  
    0x7000AA,  
    0x877280,  
    0x6800A1,  
    0x977280,  
    0x810BF1,  
    0x1831DF,  
    0x43F005,  
    0x3C0025,  
    0x3A2D50,  
    0x3B72A2,  
    0x1DC53F,  
    0x43F005,  
    0x19C8CF,  
    0x877270,  
    0x340088,  
    0x6000A1,  
    0x8B72A0,  
    0x7800A2,  
    0x977270,  
    0x877290,  
    0x6000A1,  
    0x8B72B0,  
    0x7800A2,  
    0x977290,  
    0x862550,  
    0x19A30F
};

--parameters for FM36 signal path on AB03
FM36.para_table = 
{
  --patch settings
  {0x3FA0, 0x9E77},
  {0x3FB0, 0x3F00},
  {0x3FA1, 0x9BFB},
  {0x3FB1, 0x3F0E},
  {0x3FA2, 0x9C26},
  {0x3FB2, 0x3F1C},
  {0x3FA3, 0x831C},
  {0x3FB3, 0x3F2A},
  {0x3FA4, 0x9C8B},
  {0x3FB4, 0x3F36},
  {0x3FA5, 0x9A2F},
  {0x3FB5, 0x3F3D},    
  --------------------------------------------------------------
  ----------------/ SP0 Format  ------------------ 
  --reset in Config_SPx_Format()
  --{0x2260, 0x78df},--16bit TDM
  --{0x2260, 0x78ff},--32bit TDM
  --{0x2260, 0x7AFF},--32bit TDM,MSB first,Left alignment,8 slot 
  --{0x2260, 0x7AFD},--32bit TDM,MSB first,Left alignment,6 slot 
  -- {0x2260, 0x78D9}, --16bit I2S,MSB first,Left alignment,2 slot 

  ----------------/ multi-function port  ---------------- 
  {0x2264, 0x01FC}, --use SP0 SP1 PDMI_012 PDMO_012
  
  ------------------  PDM CLOCK SETTING   ----------------/
  {0x2265, 0x0000}, --ADC clock source, 0: ADC =PLL, 2: ADC=DCA, 3: ADC=1/2 DAC 
  --{0x2266, 0x0013}, --3.072Hz
  {0x2266, 0x001B}, --2.048M
  --{0x2266, 0x0033},--1.024
  --ADC
  --{0x2267, 0xBB80}, --3.072Hz
  {0x2267, 0x7d00}, --2.048M --clock to iM401
  --{0x2267, 0x3e80},--1.024
   --DAC
   --{0x226A, 0xBB80}, --3.072Hz
   --{0x226A, 0x7d00}, --2.048M 
  {0x226A, 0x3e80},--1.024   --clock from iM401
   ----------------/ SP1 Format  ------------------
   --reset in Config_SPx_Format()
  --{0x2261, 0x78FF},--32bit TDM,MSB first,Left alignment,8 slot, Left alignment of word inside slot to meet AD1937
  --{0x2260, 0x78D9}, --16bit I2S,MSB first,Left alignment,2 slot 
  --{0x2268, 0xBB80}, 
  {0x2288, 0x0000},
  {0x2289, 0x7FFF},
  {0x2290, 0x7FFF}, 
  {0x22FD, 0x00DE}, --enable ADC/DAC interrupt
  --{0x2274, 0x0001},--mic revert  
  --{0x2303, 0x8000},
  --{0x2304, 0x4000},
  --{0x2305, 0x0000},
  {0x22FC, 0x8000}, --BYPASS ALL SIGNAL PORCESSING
  {0x226E, 0x000C}, --PLL 24.576MHz OSC 
------------------------------------------------------------
  --select data interruption source :  
  -- 0: PDM, 1: SP0, 2: SP1, 3: SP2
  {0x22B2, 0x0001},
  {0x22B3, 0x0001},
  {0x22B4, 0x0001},  
    --additional, input
    ----229A = 0x1 --Aux-in From SP0
    ----229B = 0x0 --Aux-in-L in slot0 
    ----229C = 0x1 --Aux-in-R in slot1
    ------output, aux2 output same as aux1
    ----22C7 = 0x1018 --Aux-in-L
    ----22C8 = 0x1019 --Aux-in-R
    ----22B4 = 1 --SP0
    ----22D5 = 6 --slot6 
    ----22D6 = 7 --slot7  
  --lin source 
  {0x229A, 0x0002}, --Aux-in From SP1
  {0x229B, 0x0008}, --Aux-in-L in SP1 slot0 
  {0x229C, 0x0009}, --Aux-in-R in SP1 slot1
  
  --select output data source slot
  {0x22C1, 0x101A},
  {0x22C2, 0x101B},
  {0x22C3, 0x101C},
  {0x22C4, 0x101D},
  {0x22C5, 0x101E},
  {0x22C6, 0x101F},   
  {0x22C7, 0x1018}, --Aux-in-L
  {0x22C8, 0x1019}, --Aux-in-R
--  {0x22C7, 0x1020}, --default 0
--  {0x22C8, 0x1020}, --default 0
  
  --select data dest slot
  --If lineout is from TX0, offset is 0~7
  --If lineout is from TX1, offset is 8~15
  --If lineout is from TX2, offset is 16~23
  --If offset = 0x8000, means Zero output.
  {0x22D7, 0x0000},
  {0x22D8, 0x0001},
  {0x22D9, 0x0002},
  {0x22DA, 0x0003},
  {0x22DB, 0x0004},
  {0x22DC, 0x0005},  
  {0x22D5, 0x0006}, --slot6 
  {0x22D6, 0x0007}, --slot7
  
  --mic souce
  {0x2282, 0x0000},
  {0x2283, 0x0001},
  {0x2284, 0x0002},
  {0x2285, 0x0003},
  {0x2286, 0x0004},
  {0x2287, 0x0005},
  
  --set PDM out data
  {0x22B9, 0x3F40},
  {0x22BA, 0x3F42},
  {0x22BB, 0x3F44},
  {0x22BC, 0x3F46},
  {0x22BD, 0x3F48},
  {0x22BE, 0x3F4A}, 
  --{0x22B3, 0}, 
  {0x22FA, 0xFF}, 
  {0x22E5, 0x20}, --PDMDAC CLOCK As Input
  {0x22EB, 0x0006}, --Actual MIC number in system.
  --{0x22FB, 0 },  --run flag
  
  --{0x3FCF, 0x020},--PDMDAC CLOCK As Input
--------------------------------
  {0x22F1, 0xD800},
  {0x22EB, 0x0006},
  {0x22C1, 0x1018},
  {0x22C2, 0x1019},
  {0x229A, 0x0002},
  {0x229B, 0x0008},
  {0x229C, 0x0009},
  {0x2260, 0x78DF},
  {0x2261, 0x78DF},
  {0x2268, 0x3E80},
  {0x2269, 0x3E80},
  --{0x22FB, 0 },  --run flag
---------------------------------


  --[[*******************  MIC Volume Control  ****************
  Volume setting [7:4]:
  0x0 ( 0 dB) 0x4 (-48 dB) 0x8 (-96 dB) 0xc (Not allowed)
  0x1 (-12 dB) 0x5 (-60 dB) 0x9 (-108 dB) 0xd (Not allowed)
  0x2 (-24 dB) 0x6 (-72 dB) 0xa (-120 dB) 0xe (+24 dB)
  0x3 (-36 dB) 0x7 (-84 dB) 0xb (-132 dB) 0xf (+12 dB)
  Volume setting [3:0]:
  0x0 ( 0 dB) 0x4 (-3.00 dB) 0x8 (-6.00 dB) 0xc (-9.00 dB)
  0x1 (-0.75 dB) 0x5 (-3.75 dB) 0x9 (-6.75 dB) 0xd (-9.75 dB)
  0x2 (-1.50 dB) 0x6 (-4.50 dB) 0xa (-7.50 dB) 0xe (-10.50 dB)
  0x3 (-2.25 dB) 0x7 (-5.25 dB) 0xb (-8.25 dB) 0xf (-11.25 dB)
  Real volume = Volume for [7:4] + Volume for [3:0]
  **********************************************************]]
  --{0x3FC6, 0x0000}, --MIC0,1 Volume Gain = 0dB
  --{0x3FC7, 0x0000}, --MIC2,3 Volume Gain = 0dB
  --{0x3FC8, 0x0000}, --MIC4,5 Volume Gain = 0dB
  --/**************** MEMS Gain Control **********************/
  --{0x3F91, 0x0000}, --MEMS MIC TYPE, SW default setting as MEMS type
  {0x3F92, 0x0000} --MEMS MIC gain = 0dB
  --{0x3F92, 0x0555} --MEMS MIC gain = 6dB
  --{0x3F92, 0x0AAA} --MEMS MIC gain = 12dB --SW default setting
  --{0x3F92, 0x0FFF} --MEMS MIC gain = 18dB
} ;


--*******************************   Unlock PM  *******************************/ 
--**  this operation must be executed once before Read_PM() after reset DSP **/
function Unlock_PM(dsp_type)
    local unlock_mmreg;
    
    if( dsp_type == 0 ) then      
        unlock_mmreg = 0x3FD6; -- the MREG address is 0x3FEE for FM31/FM32/FM33/FM34 
    else
        unlock_mmreg = 0x3FEE; -- the MREG address is 0x3FEE for FM36 
    end

    FM36.write_dm( unlock_mmreg, 0xECF3); 
    FM36.write_dm( unlock_mmreg, 0x3807);
    FM36.write_dm( unlock_mmreg, 0x79AD);
end

--************************  DM/ PM/ CM Single Write ******************************/
--data protocol: I2C_ADDR, FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.DM_WR, math.floor(dm_addr/256), dm_addr%256, math.floor(dm_val/256), dm_val%256
--raw  data:     --0xc0,   ----------3 bytes reg_addr--------------, ---------------------4 bytes vaule ---------------------------------
function FM36.write_dm(dm_addr, dm_val)
    printc("Write MMR "..string.format("0x%0x", dm_addr).." = "..string.format("0x%0x", dm_val))
    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.DM_WR, math.floor(dm_addr/256), dm_addr%256, math.floor(dm_val/256), dm_val%256};
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat)
end

--data protocol: I2C_ADDR, FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.PM_WR,(dm_addr>>8)&0xFF, dm_addr&0xFF, *(pdata++)^(xor_key>>16), *(pdata++)^(xor_key>>8),*(pdata++)^(xor_key)
--raw  data:     --0xc0,   ----------------------4 bytes reg_addr--------------------, ----------------------------------- 4 bytes vaule ---------------------------------
function FM36.write_pm(pm_addr, data)
    printc("Write MMR "..string.format("0x%0x", pm_addr).." = "..string.format("0x%0x", data))
    local dat = { FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.PM_WR, math.floor(pm_addr/256), pm_addr%256, math.floor(data/65536), math.floor(data/256)%256, data%256};
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat)  
end

--data protocol: I2C_ADDR, FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.CM_WR, math.floor(dm_addr/256), dm_addr%256, math.floor(data/256), data%256
--raw  data:     --0xc0,   ----------3 bytes reg_addr--------------, ---------------------4 bytes vaule ---------------------------------
function FM36.write_cm(cm_addr, data)
    printc("Write MMR "..string.format("0x%0x", cm_addr).." = "..string.format("0x%0x", data))
    local dat = { FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.CM_WR, math.floor(cm_addr/256), cm_addr%256, math.floor(data/256), data%256 };
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat) 
end

--/************************  DM/ PM/ CM Single Read *****************************/
--unsigned char buf[] = { FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.DM_RD,(dm_addr>>8)&0xFF, dm_addr&0xFF};
function FM36.read_dm(dm_addr)
    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.DM_RD, math.floor(dm_addr/256), dm_addr%256}
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat) 

    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x25}
    --UIF.raw_write(I2C, FM36.I2C_addr, dat)
    --local value = UIF.raw_read(I2C, FM36.I2C_addr, 1)
    local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)
    
    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x26}
    --UIF.raw_write(I2C, FM36.I2C_addr, dat)
    --local value1 = UIF.raw_read(I2C, FM36.I2C_addr, 1)
    local value1 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)

    return value1[1]*256 + value[1]
end

--unsigned char buf[] = { FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.PM_RD, (dm_addr>>8)&0xFF, dm_addr&0xFF}; 
function FM36.read_pm(pm_addr)
    Unlock_PM(DSP_PM_Type);    

    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.PM_RD, math.floor(pm_addr/256), pm_addr%256}
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat) 

    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x24}
    --UIF.raw_write(I2C, FM36.I2C_addr, dat)
    --local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1)
    local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)
    
    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x25}
    --UIF.raw_write(I2C, FM36.I2C_addr, dat)
    --local value1 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1)
    local value1 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)

    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x26}
    --UIF.raw_write(I2C, FM36.I2C_addr, dat)
    --local value2 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1)
    local value2 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)

    return value2[1]*65536 + value1[1]*256 + value[1]
end

--unsigned char buf[] = { FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.CM_RD, (dm_addr>>8)&0xFF, dm_addr&0xFF};   
function FM36.read_cm(cm_addr)
    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.CM_RD, math.floor(cm_addr/256), cm_addr%256}
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat) 

    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x25}
    --UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat)
    --local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1)
    local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)
    
    dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, 0x26}
    --UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat)
    --local value1 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1)
    local value1 = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)

    return value1[1]*256 + value[1]
end

--/*********************     DM / PM Burst Read / Write     **********************/
function FM36.write_dm_burst(vec_tab)
    for i=1, #vec_tab do 
        FM36.write_dm(vec_tab[i][1], vec_tab[i][2])
    end
end

function FM36.write_pm_burst(code_tab)
    local addr = code_tab[1]
    for i = 2, #code_tab do
        FM36.write_pm(addr, code_tab[i])
        addr = addr+1
    end
end

--check DSP IDMA control port stauts, wait until not busy
function Check_IDMA()
    local timeout = 100 ;
     
    for i = 0, i< timeout do
        state = HOST_LegacyRead(0x20) ;
    end     
end

function FM36.reset(type)
    type = type or 1
    if type == 1 or type == 3 then
        printc("\nPin reset FM36")
    UIF.set_gpio(UIF.GPIO.FM36_RST,1)
    UIF.set_gpio(UIF.GPIO.FM36_RST,0)
    wait(0.5)
    UIF.set_gpio(UIF.GPIO.FM36_RST,1)
    elseif type == 2 or type == 3 then
        printc("SW reset FM36")
    end
end

function FM36.set_adc_clk(value)
UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.FM36_CODEC)
    value = value or 2
    if value == 1 then
        FM36.write_dm(0x3fd5, 0x3e80)
        FM36.write_dm(0x3fcd, 0x3e80)
        FM36.write_dm(0x3fed, 0x33)
    elseif value == 2 then
        FM36.write_dm(0x3fd5, 0x7d00)
        FM36.write_dm(0x3fcd, 0x3e80)
        FM36.write_dm(0x3fed, 0x1b)
    elseif value == 3 then
        FM36.write_dm(0x3fd5, 0xbb80)
        FM36.write_dm(0x3fcd, 0x3e80)
        FM36.write_dm(0x3fed, 0x13)
    elseif value == 4 then
        FM36.write_dm(0x3fd5, 0xfa00)
        FM36.write_dm(0x3fcd, 0x7d80)
        FM36.write_dm(0x3fed, 0xf)
    end
    FM36.write_dm(0x3fe4, FM36.read_dm(0x3fe4))
  
UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.UIF_S)
    --UIF.set_interface(UIF.type.PDMCLK_SEL, 0, value)
end

function FM36.set_adc_clk_sw(value)
    if value == 1 then
        FM36.write_dm(0x2267, 0x3e80)
        FM36.write_dm(0x226A, 0x3e80)
        FM36.write_dm(0x2266, 0x33)
    elseif value == 2 then
        FM36.write_dm(0x2267, 0x7d00)
        FM36.write_dm(0x226A, 0x3e80)
        FM36.write_dm(0x2266, 0x1b)
    elseif value == 3 then
        FM36.write_dm(0x2267, 0xbb80)
        FM36.write_dm(0x226A, 0x3e80)
        FM36.write_dm(0x2266, 0x13)
    elseif value == 4 then
        FM36.write_dm(0x2267, 0xfa00)
        FM36.write_dm(0x226A, 0x7d80)
        FM36.write_dm(0x2266, 0xf)
    end
end

function FM36.adc_clk_onoff(switch)
    switch = switch or 1
    UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.FM36_CODEC)
    if switch == 0 then
        FM36.write_dm(0x3f96, 0x3F3F)  --mute MIC0-5  
        FM36.write_dm(0x3f90, 0x003F)  --power down MIC0-5
        FM36.write_dm(0x3fCF, 0x0024)  --turn off PDMCLK --0000�]�i�H
    else 
        FM36.write_dm(0x3fCF, 0x0020)  --turn on clk
        FM36.write_dm(0x3f90, 0x0000)  --power up MIC0-5
        FM36.write_dm(0x3f96, 0x003F)  --normal MIC0-5
    end
    UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.UIF_S)
end

function FM36.power_down()
    FM36.write_dm(0x3fef, 0x3000)
end
--/*********************     HOST Register Read / Write     *********************/
function HOST_SingleWrite_1(host_addr, host_val)    
    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_WR_1, math.floor(host_val/256), host_val%256}
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat) 
end

function HOST_SingleWrite_2(host_addr, host_val)
    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_WR_2, math.floor(host_val/256), host_val%256}
    UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat) 
end

function HOST_LegacyRead(host_addr)
    local dat = {FM36.CMD.SYN_0, FM36.CMD.SYN_1, FM36.CMD.HOST_RD, host_addr}
    --UIF.raw_write(UIF.type.I2C, FM36.I2C_addr, dat)
    --local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1)
    local value = UIF.raw_read(UIF.type.I2C, FM36.I2C_addr, 1, dat)
    return value
end

--**********************************************************************
--DMIC 4 steps PGA gain
function DMIC_PGA_Control(gain)
    local data, mute = 0, 0x3F
    local data_tab = {0x0000, 0x0555, 0x0aaa, 0x0fff}
    if gain == 1000 then
        mute = 0x3f3f
    elseif (math.floor(gain/6)+1)<5 then
        data = data_tab[math.floor(gain/6)+1]
    else
        assert(false, "PGA error!")
    end

    if (mute == 0x3F) then
        FM36.write_dm(0x3F92, data) ;
    end    
    FM36.write_dm( 0x3F96, mute ) ;
end


--Setup External Lin data input source
--From : SP0_RX, SP1_RX
function Config_Lin(lin_sp_index, start_slot_index )
    --APP_TRACE_INFO(("\r\nConfig_Lin sp_index = %d, start_slot_index = %d\r\n", lin_sp_index, start_slot_index));
    --Aux-Source
    FM36.write_dm(0x229A, lin_sp_index+1) ;
    --Aux-in-L
    FM36.write_dm(0x229B, bit:_lshift(lin_sp_index,3) + start_slot_index) ;
    --Aux-in-R
    FM36.write_dm(0x229C, bit:_lshift(lin_sp_index,3) + start_slot_index + 1) ;
end

-- Set additional data besides MIC data
-- M0..M5 + X0 + X1
-- Max 8 slots
-- Make sure last 2 slots folowed mic data closely
function Config_SP0_Out(mic_num )
    --APP_TRACE_INFO(("\r\nConf FM36 Mic num = %d\r\n", mic_num));
    FM36.write_dm(0x22EB, mic_num) ;
    --select output data source slot
    FM36.write_dm(0x22C1 + mic_num, 0x1018) ;
    FM36.write_dm(0x22C2 + mic_num, 0x1019) ;
end


function Config_SPx_Format(bit_length )
    local temp=0;

    --APP_TRACE_INFO(("\r\nConf FM36 Bit length = %d\r\n", bit_length));
    if( bit_length == 32 ) then          
        temp = 0x78FF;--32bit TDM, 16bit data, MSB first,Left alignment,8 slot 
    else
        temp = 0x78DF;--16bit TDM,16bit data, MSB first,Left alignment,8 slot 
    end

    FM36.write_dm(0x2260, temp) ; --SP0
    FM36.write_dm(0x2261, temp) ; --SP1
end

function Config_SR( sr )
    --APP_TRACE_INFO(("\r\nConf FM36 SR = %dkHz\r\n", sr));
    sr = sr or 48000      
    --select output data source slot
    FM36.write_dm(0x2268, sr) ; --Output Clock for ADC DPLL, PDM input sampling rate.
    FM36.write_dm(0x2269, sr) ;-- reference Clock for DAC DPLL,PDM output sampling rate.
end

--[[
/*
*********************************************************************************************************
*                                       Init_FM36_AB03()
*
* Description : Initialize FM36 DSP on AB03 board.
* Argument(s) : sr        : sample rate : 8000 ~ 48000 
*               bit_length: 16bit/32bit mode
*               mic_num   : 0~6
*               lin_sp_index  : line in data source: 0 ~ 1
*               start_slot_index: line in data slot: 0 ~ 7
* Return(s)   : NO_ERR :   execute successfully
*               others :   =error code .  
*
* Note(s)     : None.
*********************************************************************************************************
*/
]]



function FM36.init(sr, mic_num, lin_sp_index, start_slot_index, bit_length )
    FM36.reset();  

    --check chip type by rom id 
    assert(FM36.read_cm(0x2FFF) == FM36.ROM_ID, "FM36 check ROMID ERROR!")
    
    --load FM36-600C patch code
    FM36.write_pm_burst(FM36.patch_code)

    --load parameters
    FM36.write_dm_burst(FM36.para_table)
    Config_SP0_Out( mic_num )
    Config_Lin( lin_sp_index, start_slot_index )
    Config_SPx_Format( bit_length )
    Config_SR( sr )
    FM36.write_dm(0x22FB, 0)  --run flag

    --check running status
	local a = FM36.read_dm(0x2306)
	wait(0.2)
	local b = FM36.read_dm(0x2306)
	wait(0.2)
    local c = FM36.read_dm(0x22fb)

	if a == nil or b == nil or a == b or c ~=0x5a5a then 
		printc("FM36 is dead!")
	else
		printc("FM36 is running...")
	end

    FM36.power_down()
end

function FM36.init_fast()
    UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.FM36_CODEC)
    FM36.reset();  

    --check chip type by rom id 
    --assert(FM36.read_cm(0x2FFF) == FM36.ROM_ID, "FM36 check ROMID ERROR!")
    
    --load FM36-600C patch code
    FM36.write_pm_burst(FM36.patch_code)

    --load parameters
    FM36.write_dm_burst(FM36.para_table)
    --FM36.set_adc_clk(PDM_CLK_SEL)
    FM36.set_adc_clk_sw(PDM_ADC_CLK_SEL+PDM_DAC_CLK_SEL*16)
    FM36.write_dm(0x22FB, 0)
    --[[
    Config_SP0_Out( mic_num )
    Config_Lin( lin_sp_index, start_slot_index )
    Config_SPx_Format( bit_length )
    Config_SR( sr )
    FM36.write_dm(0x22FB, 0)  --run flag

    --check running status
	local a = FM36.read_dm(0x2306)
	wait(0.2)
	local b = FM36.read_dm(0x2306)
	wait(0.2)
    local c = FM36.read_dm(0x22fb)

	if a == nil or b == nil or a == b or c ~=0x5a5a then 
		printc("FM36 is dead!")
	else
		printc("FM36 is running...")
	end
]]
    --FM36.power_down()
    UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.UIF_S)
end

function FM36.disable_pdmclk()
    UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.FM36_CODEC)
    FM36.write_dm(0x3fcf, 0x00)
    --UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.UIF_S)
end

function FM36.enable_pdmclk()
    --FM36.init_fast()
    UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.FM36_CODEC)
    FM36.write_dm(0x3fcf, 0x20)
    --UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.UIF_S)
end
