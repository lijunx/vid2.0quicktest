RULER_STATE_DETACHED      =     0x00
RULER_STATE_ATTACHED      =     0x01
RULER_STATE_CONFIGURED    =     0x02
RULER_STATE_SELECTED      =     0x03
RULER_STATE_RUN           =     0x04

AUD_NODE = "e1_"
CLS_NAME = "t2"
NODE_NAME= "t3"
FILE_NAME= "t6"
SRC_DES  = "e4_"
SRC_IDX  = "u5"
SRC_LABEL = "t17"
CHANNUM	 = "u7"
SAMPLERATE="u8"
SAMREFSRC ="u9"
SAMDLL ="t10"
URB_SIZE ="u11"
SAMOUT_DES = "e12_"
SAMOUT_IDX = "u13"
URB_DELAY = "u16"
LOOP_MODE = "t20"
SAMPLE_FMT = "t21"

----SAM Lout table
SAM_LINEOUT  = 0x0000
MIC0_BYPASS  = 0x0001
MIC1_BYPASS  = 0x0002
MIC2_BYPASS  = 0x0003
MIC3_BYPASS  = 0x0004
MIC4_BYPASS  = 0x0005
MIC5_BYPASS  = 0x0006
DBG_ML_SDOA0 = 0x0007
DBG_ML_SDOA1 = 0x0008
DBG_ML_SDOA2 = 0x0009
DBG_ML_SDOA3 = 0x000A
DBG_DOA_INFO01 = 0x000B
DBG_DOA_INFO11 = 0x000C
DBG_DOA_INFO21 = 0x000D
DBG_DOA_INFO31 = 0x000E
DBG_STNS_OUT    = 0x000F
DBG_DOA_TRACK = 0x0010
DBG_OUT_POWER = 0x0011
DBG_PITCH     = 0x0012
DBG_DVFLAG00  = 0X0013

SAM_OUT_TABLE = {
"SAM_LINEOUT",
"MIC0_BYPASS","MIC1_BYPASS","MIC2_BYPASS","MIC3_BYPASS","MIC4_BYPASS","MIC5_BYPASS",
"DBG_ML_SDOA0","DBG_ML_SDOA1","DBG_ML_SDOA2","DBG_ML_SDOA3",
"DBG_DOA_INFO01","DBG_DOA_INFO11","DBG_DOA_INFO21","DBG_DOA_INFO31",
"DBG_STNS_OUT","DBG_DOA_TRACK","DBG_OUT_POWER", "DBG_PITCH","DBG_DVFLAG00"
}
---- node name table
NODE_AB_MICIN = "readmic"
NODE_AB_LIN = "readmic"
NODE_SOUNDCARD_IN = "soundcard_in"
NODE_FILE_MICIN = "file_mic"
NODE_FILE_ARRAY_S = "file_array"
NODE_FILE_LIN = "file_lin"
NODE_SAMUP = "samup"
NODE_SAMDOWN = "samdown"
NODE_SILENCE = "silence"
NODE_SPI_IN = "readspi"  -- note type: "CAudUsbReadNode"  -- for SPI recording.
NODE_SPI_OUT = "writespi"  -- note type: "CAudUsbWriteNode"  -- for SPI play.
NODE_DEG_INFO = "debuginfo"  -- note type: "CDebugInfoDecode"

SPI_REC_CH_MASK = 0
AB_OUT_CHANNEL = 4
if ubt == nil then ubt = {} end
if ubt.__help == nil then ubt.__help = {} end


function dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k,v in pairs(o) do
            if type(k) ~= 'number' then k = '"'..k..'"' end
            s = s .. '['..k..'] = ' .. dump(v) .. ',\n'
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

function print_bridge_status(v)
    printc("\nBridge status return : "..dump(v),0);
    printc("post_status = "..v.u1);
    printc("ruler_status= "..v.u2);
    printc("mic_status  = "..v.u3);
end

function print_bridge_info(v)
    printc("\nBridge info return : "..dump(v),0);
    printc("model     = "..v.t1);
    printc("hw_version= "..v.t2);
    printc("sw_version= "..v.t3);
end

function print_ruler_info(v)
    printc("\nRuler info return : "..dump(v),0)
    printc("model     = "..v.u1)
    printc("hw_version= "..v.e2.t1)
    printc("sw_version= "..v.e2.t2)
    printc("sw_version= "..v.e2.t3)
end

function read_ruler_info(slot)
    local param = {u1 = slot}
    audiobridge.send_command(4, param)
    local ruler_info = wait_rpt(5, 5000)
    wait_rpt(1011, 5000)
    local ruler_info1 = {}
    ruler_info1.ruler_id = ruler_info.u1
    ruler_info1.ruler_type = ruler_info.e2.t1
    ruler_info1.ruler_HW = ruler_info.e2.t2
    ruler_info1.ruler_SW = ruler_info.e2.t3
    ruler_info1.mic_num = ruler_info.e2.u4
    MIC_NUM = ruler_info1.mic_num
    ruler_info1.mic_vendor = ruler_info.e2.e5.t1
    ruler_info1.mic_type = ruler_info.e2.e5.u2
    ruler_info1.mic_model = ruler_info.e2.e5.t3
    return ruler_info1
end
--val = { ["u1"] = 0,["e2"] = { ["t2"] = 1.0,["t3"] = 1.0,["t1"] = R02,["u4"] = 8,["e5"] = { ["u4"] = 7,["u2"] = 1,["t3"] = 1306,["t1"] = 3S,} ,} ,}

function read_bridge_status() -- { ["u3"] = 0,["u2"] = 515,["u1"] = 0,}
    audiobridge.send_command(9)
    local AB_status = wait_rpt(10, 5000)
    wait_rpt(1011, 5000)
    local bridge_status1 = {}
    bridge_status1.post = AB_status.u1
    bridge_status1.ruler = AB_status.u2
    bridge_status1.mic = AB_status.u3
    return bridge_status1
end

function read_bridge_info()  -- { ["t2"] = [HW:V1.0],["t3"] = [FW:H:V0.1.8][FW:A:V2.0],["t1"] = [AB01],}
    audiobridge.send_command(11)
    local bridge_info = wait_rpt(50, 5000)
    wait_rpt(1011, 5000)
    local bridge_info1 = {}
    bridge_info1.model = bridge_info.t1
    bridge_info1.HW = bridge_info.t2
    bridge_info1.SW = bridge_info.t3
    return bridge_info1
end --
----------------------------------------------------------------------
function wait_rpt(expect_id, timeout_ms)
    local id, value
    local counter = timeout_ms/2
    counter = counter <1 and 1 or counter

    repeat
        --print(counter)
		counter = counter -1
    	id, value = audiobridge.get_notify(1)   --2ms  ?
        --print(id, value)
    	if id==nil then
    		if( counter == 0 )then
          		printc("get_notify(): no mesg", 1);
          		return false
          	end
    	elseif id == 10 then
          	print_bridge_status(value)
          	--return true, value
    	elseif id == 50 then
          	print_bridge_info(value)
            --return true, value
        elseif id == 5 then
            print_ruler_info(value)
            --return true, value
        elseif id == 1010 then
        	update_usb_state(value)
    	else
            --printc("Get report, id ="..id.." val = "..dump(value).."---end---", 0);
    	end
    	wait(0.001)
    until id == expect_id
    --printc("\ncounter = "..counter,DEBUG )
    return true, value
end



function clear_rpt()
    repeat
        id, value = audiobridge.get_notify(1)
    until id == nil
end

function get_usb_status()

    local i, v = audiobridge.get_notify(1)
    --print(i, v)
	if i == nil then
    	print("get_notify(): no mesg");
        return false
    elseif i == 1010 then
    	print(true,v.u1)
		return true, v.u1
	else
		printc("Unexpect report id:"..i)
		return false
	end

end




usb_state = {}
usb_state.flag = false
function update_usb_state( d_value )

    print("update_usb_state()...")
    local state, value, state1, value1
--print(usb_state.state, usb_state.value,usb_state.flag)
    if d_value == nil then
        if usb_state.flag == true then
            usb_state.flag = false
            print(usb_state.state, usb_state.value)
			return usb_state.state, usb_state.value
        else
        	state, value = get_usb_status()
        end
	else
		state, value = true, d_value.u1
		usb_state.flag = true
	end

    if state == true then
		if(value == 1) then
			printc("USB Attached.")
			usb_state.state, usb_state.value = true, true
			HW_USB_CONNECTION_STATE = 1
			return true, true
		else
			repeat --fix ABDeamon bug!!
				state,value = get_usb_status()
				print(state,value,state1,value1)
                if state == true then
                    state1, value1 = state, value
                else
    				if( state1 == true and value1 == 1 ) then
    					printc("USB Attached.")
    					usb_state.state, usb_state.value = true, true
    					HW_USB_CONNECTION_STATE = 1
    					return true, true
    				end
                end
			until state==false
			printc("USB Detached.")
			usb_state.state, usb_state.value = true, false
			HW_USB_CONNECTION_STATE = 0
			return true, false
		end
	else
	    usb_state.state = false
		return false, false
    end

end


function wait_rpt_old(expect_id, timeout)
    local id, value
    wait(0.1)
    repeat
    	id, value = audiobridge.get_notify(timeout)
        --print(id, expect_id)
    	if id==nil then
          	printc("Get report failed, return nil\n", 1);
          	return false
    	elseif id == 10 then
          	print_bridge_status(value)
          	return value
    	elseif id == 50 then
          	print_bridge_info(value)
            return value
        elseif id == 5 then
            print_ruler_info(value)
            return value
    	else
            printc("Get report, id ="..id.." val = "..dump(value).."---end---", 0);
    	end
    until id==expect_id
    return true, value
end


function wait_5011_rpt(timeout)
    repeat
        local r, v = wait_rpt(5011, timeout)
        if not r then
            printc("wait 5011 rpt fail");
            return;
        end
        printc("commend 5011 return value is "..v.u14, DEBUG)
    until (v.u14 == 0)
end

function check_send_ab_command_result( str_msg, wait_time_ms )
	local r,v = wait_rpt(1011, wait_time_ms)
	if r == true then
	    if v.u1 ~= 0 then
	        message("ERROR", str_msg.." error["..tostring(v.u1).."]")
	        return false
	    end
	else
	    message("ERROR", str_msg.." error[]")
	    return false
	end
	return true
end

function check_send_pc_command_result( str_msg, wait_time_ms )
	local r,v = wait_rpt(5011, wait_time_ms)
	if r == true then
	    if v.u1 ~= 0 then
	        message("ERROR", str_msg.." error["..tostring(v.u1).."]")
	        return false
	    end
	else
	    message("ERROR", str_msg.." error[]")
	    return false
	end
	return true
end
--------------------------------------------------------------------------
function srcs_array_to_binary(srcs)
    if type(srcs) ~= "table" then
        printc("\nError, expect a table!");
        printc(debug.traceback());
    end

    local type={
        ["file"]=0x0,
        ["mic"] =0x80,
        ["sam_out"]=0x90,
        ["sam_in"]=0x10,
    }

    local r={}
    for i=1,#srcs do
        r[i]= type[srcs[i].t]+srcs[i].idx;
    end

    return r;
end

function mics_to_binary(num)
    local mic_bin = {0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87};
    local r = {};
    for i=1, num do
        r[i] = mic_bin[i];
    end
    return r;
end

function samsoft_src(v)
    local r = {};
    local function find(idx)
        local j;
            for j=1, #r do
            if r[j].idx == idx then
               return true;
            end
        end
    end
    for i=1, #v do
        if v[i].t == "sam_out" and not find(v[i].idx) then
            r[#r+1] = v[i]
        end
    end
    return r
end

function sleep(t)
    if t == 0 then
        os.execute("pause")
    else
        printc("wait time "..t.."s")
        wait(t)
    end
end

function audio_plyrec_configuration(param)
    if CLIENT_MODE == "Client" then
        send_to_server('audio_plyrec_configuration',{param})
    else
        printc("\nconfig audio setting.")
        audiobridge.send_command(5000, param);
        printc(dump(param), 1)--DEBUG)
        wait(0.05)
        wait_5011_rpt(2000);
        --local r, v = wait_rpt(5011, 10000)
        --print(v.u14)
    end
end

--[[
function AB_connect()
    if CLIENT_MODE == "Client" then
        send_to_server('AB_connect')
    else
        audiobridge.start_server()
        if AB_ENABLE == 1 then
            repeat
                local r, v = wait_rpt(1010, 10000)
                if not r then
                    message("Error", "No audio bridge attached");
                    audiobridge.stop_server();
                    --assert(false, "Failed, no audio bridge attached")
                    return;
                    end
            until (v.u1 == 1)
            printc("\nUIF connect successful. ^_^")
        end
        return true
    end
end

function AB_connect_GUI()
    audiobridge.start_server()

    local state = UIF.get_board_ver()
    if not state then  --fix bug: when PC power off, UIF is not power off, then the first time when start server will fail.
        audiobridge.stop_server()
        wait(1)
        audiobridge.start_server()
        state = UIF.get_board_ver()
    end

    if state  then
        printc("\nUIF connect successful. ^_^")
        HW_USB_CONNECTION_STATE = 1
        return true
    else
        printc("\nUIF NOT connected !!!")
        HW_USB_CONNECTION_STATE = 0
        return false
    end
end
]]--

function AB_connect()
    if CLIENT_MODE == "Client" then
        send_to_server('AB_connect')
    else
        audiobridge.start_server()
        if AB_ENABLE == 1 then
            repeat
                local r, v = wait_rpt(1010, 1000)
                if not r then
                    message("Error", "No audio bridge attached");
                    audiobridge.stop_server();
                    --assert(false, "Failed, no audio bridge attached")
                    return;
                    end
            until (v.u1 == 1)
            printc("\nUIF connect successful. ^_^")
        end
        return true
    end
end

function AB_connect_GUI()
    audiobridge.start_server()

    local status, state = UIF.ping()
    if status == false and state == false then  --fix bug: when PC power off, UIF is not power off, then the first time when start server will fail.
        audiobridge.stop_server()
        wait(1)
        audiobridge.start_server()
        UIF.ping()
    end

    if HW_USB_CONNECTION_STATE == 1  then
        printc("\nUIF connect successful. ^_^")
        return true
    else
        printc("\nUIF NOT connected !!!")
        return false
    end
end

function AB_disconnect()
    if CLIENT_MODE == "Client" then
        send_to_server('AB_disconnect')
    else
        printc("\nDisconnect UIF!")
        audiobridge.stop_server()
    end
end

function set_record_configuration(param)  --AB Lin/MICin
--print(AB_ENABLE)

    if AB_ENABLE == 1 then
        if CLIENT_MODE == "Client" then
            param={
                u1=0,
                u2=SAMPLE_RATE,
                u3=#MIC_MICS,
                u4=AB_LIN_ENABLE,
                u5=SAMPLE_BIT_LEN,
                u6=GPIO_REC_BIT_MASK,
                u7=AUDIO_BUS_TYPE,
                --u8=DATE_LATCH == 1 and 0 or 1,
                u8=BCLK_POLARITY,
                u9=ONE_CYCLE_DELAY,
                --u10=FRAM_START,
                u11=M_S == 1 and 0 or 1,
                u12=AB_SPIIN_ENABLE == 1 and SPI_REC_CH_MASK or 0,
                u13=SLOT_NUM or 2,
            }
            send_to_server('set_record_configuration',{param})
        else
            printc("\nconfig recording setting.")
            --print(CLIENT_MODE)
            if CLIENT_MODE ~= "Server" then
                param={
                    u1=0,
                    u2=SAMPLE_RATE,
                    u3=#MIC_MICS,
                    u4=AB_LIN_ENABLE,
                    u5=SAMPLE_BIT_LEN,
                    u6=GPIO_REC_BIT_MASK,
                    u7=AUDIO_BUS_TYPE,
                    --u8=DATE_LATCH == 1 and 0 or 1,
                    u8=BCLK_POLARITY, --
                    u9=ONE_CYCLE_DELAY,
                    --u10=FRAM_START,
                    u11=M_S == 1 and 0 or 1,
                    u12=AB_SPIIN_ENABLE == 1 and SPI_REC_CH_MASK or 0,
                    u13=SLOT_NUM or 2,
                }
            end
            printc(dump(param))
            audiobridge.send_command(1, param)

            check_send_ab_command_result("Set recording configuration", 1000)
            wait(0.1)
        end
    end
end
--      BIT_ORDER = 1     --1: MSB first, 2: LSB first
function set_play_configuration(param)  --AB Lout/SPKout
    if AB_ENABLE == 1 then
        local play_channel=SPKOUT_SRC_MAX + LOUT_SRC_MAX
        if CLIENT_MODE == "Client" then
            param={
                u1=1,
                u2=SAMPLE_RATE,
                u3=AB_OUT_CHANNEL, --play_channel,
                u5=SAMPLE_BIT_LEN,
                u7=AUDIO_BUS_TYPE,
                --u8=DATE_LATCH,
                u8=BCLK_POLARITY,
                u9=ONE_CYCLE_DELAY,
                --u10=FRAM_START,
                u11=M_S == 1 and 0 or 1,
                u12=AB_SPIIN_ENABLE == 1 and SPI_REC_CH_MASK or 0,
                u13=SLOT_NUM or 2,
            }
            send_to_server('set_play_configuration',{param})
        else
            printc("\nconfig play setting.")
            if CLIENT_MODE ~= "Server" then
                param={
                    u1=1,
                    u2=SAMPLE_RATE,
                    u3=AB_OUT_CHANNEL, --play_channel,
                    u5=SAMPLE_BIT_LEN,
                    u7=AUDIO_BUS_TYPE,
                    --u8=DATE_LATCH,
                    u8=BCLK_POLARITY,
                    u9=ONE_CYCLE_DELAY,
                    --u10=FRAM_START,
                    u11=M_S == 1 and 0 or 1,
                    u12=AB_SPIIN_ENABLE == 1 and SPI_REC_CH_MASK or 0,
                    u13=SLOT_NUM or 2,
                }
            end
            printc("param>="..dump(param))
            audiobridge.send_command(1, param)

            check_send_ab_command_result("Set play configuration", 1000)
            wait(0.1)
        end
        --print(AB_OUT_CHANNEL)
    end
end

function update_audio_configuration()  -- update CODEC & FM36 settings
    if AB_ENABLE == 1 then
        if CLIENT_MODE == "Client" then
            send_to_server('update_audio_configuration')
        else
            printc("\nupdate audio setting.")
            audiobridge.send_command(16)
            check_send_ab_command_result("Update audio configuration", 1000)
            wait(0.1)
        end
    end
end

function update_audio_setting()
    set_record_configuration()
    set_play_configuration()
    update_audio_configuration()
    AB_set_volume(VOLUME_MICIN, VOLUME_LOUT, VOLUME_SPKOUT, VOLUME_LIN)
end

--function toggle_MIC()
function toggle_MIC(mics, on_off)
    if AB_MICIN_ENABLE == 1 then
        printc("\ntoggle MIC.")
        on_off = on_off or 1
        for i=1, #mics do
            local param={
            u1=mics[i].r,
            u2=mics[i].m,
            u3=on_off;
            }
        printc(dump(param), DEBUG)
        audiobridge.send_command(8, param)
        check_send_ab_command_result("toggle Ruler", 5000)
        wait(0.05)
        end
    end
end

--function close_MIC()
function close_MIC(ruler_id)
    if AB_ENABLE == 1 then
        printc("\nclose all MIC.")
        audiobridge.send_command(13, {u1=ruler_id})
        check_send_ab_command_result("close all MIC on Ruler", 5000)
        return true
    end
end

PLAY_STATUS = 0
--[[
function start_play_record(delay)
    stop_play_record()

    delay = delay or 1
    local param = update_param()
    --close_MIC(1)
    --toggle_MIC(MIC_MICS)
    audio_plyrec_configuration(param)
    set_record_configuration()
    set_play_configuration()

    printc("\nstart play and record.")
    if AB_ENABLE == 1 then
        local start_type=1
        local record_flag = AB_MICIN_ENABLE == 1 or AB_LIN_ENABLE == 1
        local play_flag = AB_LOUT_ENABLE == 1 or AB_SPKOUT_ENABLE == 1
        if record_flag and play_flag then
        	start_type = 3
        elseif record_flag then
            start_type = 1
        elseif play_flag then
            start_type = 2
        else
            assert(false, "Play and record are both disabled, please check!")
        end

        audiobridge.send_command(5001 )  --Start audio engine
        wait_5011_rpt(10000);
        wait(0.05)

        local param={
            u1=start_type,
        }
        --printc("param>="..dump(param), DEBUG)
        audiobridge.send_command(2, param)
        assert(wait_rpt(1011, 1000), "Start play and record ERROR!")
        wait(0.05)
        if delay == 1 then
            sleep(RECORD_TIME)
        end
    else
        audiobridge.send_command(5001 )  --Start audio engine
        wait_5011_rpt(10000);
        wait(0.05)
        if PLAY_MODE ~= "once" then
            if delay == 1 then
                sleep(RECORD_TIME)
            end
        else
            repeat
                local r, v = wait_rpt(5011, timeout)
                assert(r, "Stop play and record ERROR!")
                printc("\ncommend 5011 return value is "..v.u14)
            until (v.u14 == 11)
        end
    end
    PLAY_STATUS = 1
end

function stop_play_record()
    printc("\nstop play and record.\n")
    if AB_ENABLE == 1 then
        audiobridge.send_command(3)
        assert(wait_rpt(1011, 1000), "Stop play and record ERROR!")
        wait(0.05)
    end
    if PLAY_MODE ~= "once" or AB_ENABLE == 1 then
        --wait(0.1)
        audiobridge.send_command(5002)  --Stop audio engine
        wait_5011_rpt(10000);
        wait(0.05)
    else
        wait(0.05)
    end
    --toggle_MIC(MIC_MICS, 0)
    --AB_disconnect()
    PLAY_STATUS = 0
end
]]


function start_play_record( delay )

    if DSP_NAME == "FM1388" and CLIENT_MODE == "Server" then
    	return
    end

    --stop_play_record()

    delay = delay or 1
    local param = update_param()
    --close_MIC(1)
    --toggle_MIC(MIC_MICS)

    audio_plyrec_configuration(param)
    update_audio_setting()
    --io.read()
    printc("\nstart play and record.")
    --print(AB_ENABLE)
    if AB_ENABLE == 1 then
        local start_type=1
        local record_flag = AB_MICIN_ENABLE == 1 or AB_LIN_ENABLE == 1 or AB_SPIIN_ENABLE == 1
        local play_flag = AB_LOUT_ENABLE == 1 or AB_SPKOUT_ENABLE == 1
        if record_flag and play_flag then
        	start_type = 3
        elseif record_flag then
            start_type = 1
        elseif play_flag then
            start_type = 2
        else
            assert(false, "Play and record are both disabled, please check!")
        end
        if CLIENT_MODE == "Client" then
        	send_to_server('start_play_record_sub1',{start_type})
            if AB_SPIOUT_ENABLE == 1 then
        	    send_to_server('start_spi_play',{SPI_REC_CH_MASK, 48000/SAMPLE_RATE})
            elseif AB_SPIIN_ENABLE == 1 then
        	    send_to_server('start_spi_record',{SPI_REC_CH_MASK})
            end
        else
			start_play_record_sub1(start_type)
            if AB_SPIOUT_ENABLE == 1 then
                start_spi_play(SPI_REC_CH_MASK, 48000/SAMPLE_RATE)
			elseif AB_SPIIN_ENABLE == 1 then
			    start_spi_record(SPI_REC_CH_MASK)
            end
        end
        if delay == 1 then
            sleep(RECORD_TIME)
        end
    else
        if CLIENT_MODE == "Client" then
        	send_to_server('start_play_record_sub2',{PLAY_MODE})
        else
			start_play_record_sub2(PLAY_MODE)
        end
        if PLAY_MODE ~= "once" then  --??????
            if delay == 1 then
                sleep(RECORD_TIME)
            end
        end
    end
    PLAY_STATUS = 1
end

----mask  low bit is channel[0],  high bit is channel[10].1: Enable   0: Disable
function start_spi_record(mask)
    if CLIENT_MODE == "Client" then
		return
	else
        printc("\nStart SPI Recording:")
        mask = mask or 0x03ff
        local param=
        {
             u1=mask,
        }
        audiobridge.send_command(47, param)
        return check_send_ab_command_result("send SPI recording CMD", 1000)
    end
end

--mask  low bit is channel[0],  high bit is channel[10].1: Enable   0: Disable
--timer. spi sample rate=8K ,timer=10ms.(16K,10ms).(24K,8ms).(48K,4ms). 0x01=4ms,0x02=8ms,0x03=10ms
function start_spi_play(mask, timer_tick)
    if CLIENT_MODE == "Client" then
		return
	else
        printc("\nStart SPI Playing:")
        mask = mask or 0x03ff
        timer_tick = timer_tick > 3 and 3 or timer_tick
        local param=
        {
            u1=mask,--record mask
            u2=0x00ff,  --play mask
            u3=timer_tick,
        }
        audiobridge.send_command(49, param)
        return check_send_ab_command_result("send SPI play CMD", 1000)
    end
end

function start_play_record_sub1( start_type )
    if CLIENT_MODE == "Client" then
		return
	else
      ------------------------------------
        audiobridge.send_command(5001 )  --Start audio engine
        wait_5011_rpt(10000);
        wait(0.05)

        local param={
            u1=start_type,
        }
        --printc("param>="..dump(param), DEBUG)
        audiobridge.send_command(2, param)
        check_send_ab_command_result("Start play and record", 1000)
        ----------------------------------
        wait(0.05)
    end

end


function start_play_record_sub2( play_mode )
    if CLIENT_MODE == "Client" then
		return
	else
        audiobridge.send_command(5001 )  --Start audio engine
        wait_5011_rpt(10000);
        wait(0.05)
        if play_mode == "once" then
            repeat
                local r, v = wait_rpt(5011, timeout)
                if r == false then
                    message("ERROR", "Stop play and record ERROR!")
                else
                    printc("\ncommend 5011 return value is "..v.u14)
                end
            until (v.u14 == 11)
        end
    end

end

---------------------------------------------------------------------------
function stop_play_record()

    if DSP_NAME == "FM1388" and CLIENT_MODE == "Server" then
    	return
    end

    printc("\nstop play and record.\n")
    if AB_ENABLE == 1 then

        if CLIENT_MODE == "Client" then
            send_to_server('stop_play_record_sub1')
        else
	    stop_play_record_sub1()
        end
    end

    if PLAY_MODE ~= "once" or AB_ENABLE == 1 then
         if CLIENT_MODE == "Client" then
            send_to_server('stop_play_record_sub2')
        else
	    stop_play_record_sub2()
        end
    end

    PLAY_STATUS = 0

end

function stop_play_record_sub1( )

    if CLIENT_MODE == "Client" then
	    return
    end

    audiobridge.send_command(3)
    check_send_ab_command_result("Stop play and record", 1000)
    wait(0.05)
end

function stop_play_record_sub2( )

    if CLIENT_MODE == "Client" then
	    return
    end

    audiobridge.send_command(5002)  --Stop audio engine
    wait(0.05)
    --assert(wait_rpt(5011, 10000), "Stop play and record ERROR!")
    wait_5011_rpt(10000);
end


local function check_AB_config()
	--[[
    local src_table = {
        {SAMUP_ENABLE, SAM_MICIN_SRC, "samsoft mic_in"},
        {SAMDOWN_ENABLE, SAM_LIN_SRC, "samsoft L_in"},
        {SOUNDCARD_OUT_ENABLE, SOUNDCARD_OUT_SRC, "sound card output"},
        {FILE_LOUT_ENABLE, FILE_LOUT_SRC, "wirte file"},
        {FILE_SPKOUT_ENABLE, FILE_SPKOUT_SRC, "wirte SPK_out file "},
        {AB_LOUT_ENABLE, AB_LOUT_SRC, "audiobridge L_out"},
        {AB_SPKOUT_ENABLE, AB_SPKOUT_SRC, "audiobridge SPK_out"},
        {WAVEFORM_ENABLE, WAVEFORM_SRC, "waveform"},
    }
    for i = 1, #src_table do
        if src_table[i][1] == 1 then
            for j = 1, #src_table[i][2] do
                assert(src_table[i][2][j][2], "The source setting of "..src_table[i][3].." is error! Note "..src_table[i][2][j][1]..", number "..j.." is error!")
            end
        end
    end
    ]]--
end


local function print_AB_config(str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(ABCONFIG_FILE,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end

function AB_config()
    check_AB_config()
    print_AB_config("\n*********************************")
    print_AB_config("Test PC: "..TEST_PC)
    print_AB_config("OS: "..OS_TYPE)

    if AB_MICIN_ENABLE == 1 then
        print_AB_config("\nAB MIC in:")
        for i = 1, #MIC_MICS do print_AB_config("    Ruler:"..MIC_MICS[i].r.."; MIC:"..MIC_MICS[i].m) end
    end

    if AB_LIN_ENABLE == 1 then
        print_AB_config("\nAB Line in enable")
    else
        print_AB_config("\nAB Line in disable")
    end

    if SOUNDCARD_IN_ENABLE == 1 then
       print_AB_config("\nSound card input enable:")
       print_AB_config("    sound name is \""..INPUT_SOUNDCARD.."\"")
    end

    if FILE_MICIN_ENABLE == 1 then
       print_AB_config("\nFile 2 input enable:")
       print_AB_config("    file name is \""..INPUT_FILE_MIC.."\"")
    end

    if FILE_LIN_ENABLE == 1 then
       print_AB_config("\nFile 1 input enable:")
       print_AB_config("    file name is \""..INPUT_FILE_LIN.."\"")
    end

    if FILE_ARRAY_ENABLE == 1 then
       print_AB_config("\nFile array input enable:")
       print_AB_config("    file name is \""..INPUT_FILE_ARRAY.."\"")
    end

    if SAMUP_ENABLE == 1 then
       print_AB_config("\nSAMsoft uplink enable:")
       print_AB_config("    vec is \""..SAM_VEC.."\"")
       print_AB_config("    dll is \""..SAM_DLL.."\"")
       print_AB_config("    input source:")
       for i = 1, #SAM_MICIN_SRC do
           print_AB_config("        "..SAM_MICIN_SRC[i][1]..":"..SAM_MICIN_SRC[i][2])
       end
       print_AB_config("    uplink output:")
       for i = 1, #SAM_LOUT do
           print_AB_config("        "..SAM_LOUT[i]..":"..SAM_OUT_TABLE[SAM_LOUT[i]+1])
       end
    end

    if SAMDOWN_ENABLE == 1 then
       print_AB_config("\nSAMsoft downlink enable:")
       print_AB_config("    vec is \""..SAM_VEC.."\"")
       print_AB_config("    dll is \""..SAM_DLL.."\"")
       print_AB_config("    input source:")
       for i = 1, #SAM_LIN_SRC do
           print_AB_config("        "..SAM_LIN_SRC[i][1]..":"..SAM_LIN_SRC[i][2])
       end
    end

    if SOUNDCARD_OUT_ENABLE == 1 then
       print_AB_config("\nSound card output enable:")
       print_AB_config("    sound name is \""..OUTPUT_SOUNDCARD.."\"")
       print_AB_config("    output source:")
       for i = 1, #SOUNDCARD_OUT_SRC do
           print_AB_config("        "..SOUNDCARD_OUT_SRC[i][1]..":"..SOUNDCARD_OUT_SRC[i][2])
       end
    end

    if FILE_LOUT_ENABLE == 1 then
       print_AB_config("\nWrite file 1 enable:")
       print_AB_config("    file name is \""..FILE_LOUT_NAME.."\"")
       print_AB_config("    output source:")
       for i = 1, #FILE_LOUT_SRC do
           print_AB_config("        "..FILE_LOUT_SRC[i][1]..":"..FILE_LOUT_SRC[i][2])
       end
    end

    if FILE_SPKOUT_ENABLE == 1 then
       print_AB_config("\nWrite file 2 enable:")
       print_AB_config("    file name is \""..FILE_SPKOUT_NAME.."\"")
       print_AB_config("    output source:")
       for i = 1, #FILE_SPKOUT_SRC do
           print_AB_config("        "..FILE_SPKOUT_SRC[i][1]..":"..FILE_SPKOUT_SRC[i][2])
       end
    end

    if AB_LOUT_ENABLE == 1 then
       print_AB_config("\nAB Lout enable:")
       print_AB_config("    output source:")
       for i = 1, #AB_LOUT_SRC do
           print_AB_config("        "..AB_LOUT_SRC[i][1]..":"..AB_LOUT_SRC[i][2])
       end
    end

    if AB_SPKOUT_ENABLE == 1 then
       print_AB_config("\nAB SPKout enable:")
       print_AB_config("    output source:")
       for i = 1, #AB_SPKOUT_SRC do
           print_AB_config("        "..AB_SPKOUT_SRC[i][1]..":"..AB_SPKOUT_SRC[i][2])
       end
    end

    if WAVEFORM_ENABLE == 1 then
       print_AB_config("\nWaveform viewer enable:")
       print_AB_config("    output source:")
       for i = 1, #WAVEFORM_SRC do
           print_AB_config("        "..WAVEFORM_SRC[i][1]..":"..WAVEFORM_SRC[i][2])
       end
    end

    print_AB_config("\n*********************************\n")
end


function update_param()
    param={}
    local node_number = 1

    check_AB_config()

    --silence node
    param[AUD_NODE.."0"] =
     {
        [CLS_NAME]   = "CSilenceNode",
        [NODE_NAME] = NODE_SILENCE,
     }

    --config CAudUsbReadNode
    if AB_LIN_ENABLE == 1 or AB_MICIN_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
         {
            [CLS_NAME]   = "CAudUsbReadNode",
            [NODE_NAME] = NODE_AB_MICIN,
            [SAMPLE_FMT]= SR_LEN,
            [CHANNUM]    = #AB_MICIN + #AB_LIN +#GPIO_REC,
            [SAMPLERATE] = SAMPLE_RATE,
            [URB_SIZE] =30,
         }
        printc("\nCAudUsbReadNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CAudUsbReadNode
    if AB_SPIIN_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
         {
            [CLS_NAME]   = "CAudUsbReadNode",
            [NODE_NAME] = NODE_SPI_IN,
            [SAMPLE_FMT]= SR_LEN,
            [CHANNUM]    = SPI_IN,
            [SAMPLERATE] = SAMPLE_RATE,
            [URB_SIZE] =30,
         }
        printc("\nCAudUsbReadNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CSoundCardReadNode
    if SOUNDCARD_IN_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
         {
            [CLS_NAME]  = "CSoundCardReadNodeV2",
            [NODE_NAME] = NODE_SOUNDCARD_IN,
            [FILE_NAME] = INPUT_SOUNDCARD,
            [CHANNUM]    = #SOUNDCARD_IN,
            [SAMPLERATE] = SAMPLE_RATE,
            [SAMPLE_FMT]= SR_LEN,
         }
        printc("\nCSoundCardReadNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CWavFileReadNode for mic simulation
    if FILE_MICIN_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CWavFileReadNode",
            [NODE_NAME] = NODE_FILE_MICIN,
            [CHANNUM]   = #FILE_MICIN,
            [FILE_NAME] = INPUT_FILE_MIC,
            [LOOP_MODE] = PLAY_MODE,
            [SAMPLE_FMT]= SR_LEN,
          }
        printc("\nCWavFileReadNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end


    if FILE_ARRAY_ENABLE == 1 then
   -- 	for i = 1, #INPUT_FILE_ARRAY do
	        param[AUD_NODE..tostring(node_number)] =
	          {
	            [CLS_NAME]  = "CWavFileReadNode",
	            [NODE_NAME] = NODE_FILE_ARRAY_S,
	            [CHANNUM]   = INPUT_FILE_CHANNEL,
	            [FILE_NAME] = INPUT_FILE_ARRAY,
	            [LOOP_MODE] = PLAY_MODE,
	            [SAMPLE_FMT]= SR_LEN, --"S16_LE"--
	          }
	        printc("\nCWavFileReadNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
	        node_number = node_number + 1
      --  end

    end


    --config CWavFileReadNode for line in
    if FILE_LIN_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CWavFileReadNode",
            [NODE_NAME] = NODE_FILE_LIN,
            [CHANNUM]   = #FILE_LIN,
            [FILE_NAME] = INPUT_FILE_LIN,
            [LOOP_MODE] = PLAY_MODE,
            [SAMPLE_FMT]= SR_LEN,
          }
        printc("\nCWavFileReadNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CDebugInfoDecode for SPKout
    if DEBUG_INFO_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CDebugInfoDecode",
            [NODE_NAME] = NODE_DEG_INFO,
          }
        param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(0)] = {}
        param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(0)][NODE_NAME] = DEBUG_INFO_SRC_NODE
        param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(0)][SRC_IDX] = DEBUG_INFO_SRC_IDX

        printc("\nCDebugInfoDecode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CSAMUpNode
    if SAMUP_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CSAMUpNode",
            [NODE_NAME] = NODE_SAMUP,
            [FILE_NAME]  = SAM_VEC,
            [SAMDLL] = SAM_DLL,
            --[SAMPLE_FMT]= SR_LEN,
          }

        for i = 1, #SAM_MICIN_SRC do
            param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
            param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = SAM_MICIN_SRC[i][1]
            param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = SAM_MICIN_SRC[i][2]
        end

        if SAMUP_REF_ENABLE == 1 then
            for i = 1, #SAMUP_REF_SRC do
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+#SAM_MICIN_SRC)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+#SAM_MICIN_SRC)][NODE_NAME] = SAMUP_REF_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+#SAM_MICIN_SRC)][SRC_IDX] = SAMUP_REF_SRC[i][2]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+#SAM_MICIN_SRC)][SAMREFSRC] = 1
            end
        end

        for i = 1, #SAM_LOUT_SRC do
            if i > 10 then
                param[AUD_NODE..tostring(node_number)][SAMOUT_DES..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SAMOUT_DES..tostring(i-1)][SAMOUT_IDX] = SAM_LOUT_SRC[i]
            else
                param[AUD_NODE..tostring(node_number)][SAMOUT_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SAMOUT_DES..tostring(0)..tostring(i-1)][SAMOUT_IDX] = SAM_LOUT_SRC[i]
            end
        end
        printc("\nCSAMUpNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end


    --config CSAMUpNode
    if SAMDOWN_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CSAMDownNode",
            [NODE_NAME] = NODE_SAMDOWN,
            [FILE_NAME]  = SAM_VEC,
            [SAMDLL] = SAM_DLL,
            --[SAMPLE_FMT]= SR_LEN,
          }

        for i = 1, #SAM_LIN_SRC do
            param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
            param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = SAM_LIN_SRC[i][1]
            param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = SAM_LIN_SRC[i][2]
        end
        for i = 1, #SAM_SPKOUT do
            param[AUD_NODE..tostring(node_number)][SAMOUT_DES..tostring(i-1)] = {}
            param[AUD_NODE..tostring(node_number)][SAMOUT_DES..tostring(i-1)][SAMOUT_IDX] = SAM_SPKOUT[i]
        end
        printc("\nCSAMDownNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CAudUsbWriteNode
    if AB_LOUT_ENABLE == 1 or AB_SPKOUT_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
        {
            [CLS_NAME]  = "CAudUsbWriteNode",
            [URB_SIZE] =30,
            [URB_DELAY] = ENGINE_DELAY,
            [SAMPLE_FMT]= SR_LEN,
        }
        local play_channel = 0

    	if AB_SPKOUT_ENABLE == 1 then
            for i = 1, #AB_SPKOUT_SRC do
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = AB_SPKOUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = AB_SPKOUT_SRC[i][2]
                if AB_SPKOUT_SRC[i][3] ~= nil then
                    param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_LABEL] = AB_SPKOUT_SRC[i][3]
                end
                play_channel = play_channel + 1
                --print(i, AB_SPKOUT_SRC[i][1], AB_SPKOUT_SRC[i][2], play_channel,SRC_DES..tostring(0)..tostring(i-1))
            end
    	else
    	--[[
            for i = 1, 4 do
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = NODE_SILENCE
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = 0
                play_channel = play_channel + 1
            end
            ]]
        end
    	if AB_LOUT_ENABLE == 1 then
            for i = 1,  #AB_LOUT_SRC do
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(play_channel+i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(play_channel+i-1)][NODE_NAME] = AB_LOUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(play_channel+i-1)][SRC_IDX] = AB_LOUT_SRC[i][2]
                if AB_LOUT_SRC[i][3] ~= nil then
                    param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(play_channel+i-1)][SRC_LABEL] = AB_LOUT_SRC[i][3]
                end
                --print(i, AB_LOUT_SRC[i][1], AB_LOUT_SRC[i][2], play_channel,SRC_DES..tostring(0)..tostring(play_channel+i-1))
            end
            play_channel = play_channel + #AB_LOUT_SRC
    	else
    	--[[
            for i = 1, 4 do
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+play_channel)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+play_channel)][NODE_NAME] = NODE_SILENCE
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1+play_channel)][SRC_IDX] = 0
            end
            play_channel = play_channel + 4
            ]]
        end
        AB_OUT_CHANNEL = play_channel
        --print(play_channel)
        printc("\nCAudUsbWriteNode : "..dump(param[AUD_NODE..tostring(node_number)]), DEBUG)
        node_number = node_number + 1
    end

    --config CShareMemSinkNode
    if WAVEFORM_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CShareMemSinkNode",
            [NODE_NAME] = "CShareMemSinkNode",
            [FILE_NAME] = FILE_LOUT_NAME,
            [SAMPLE_FMT]= "S16_LE"--SR_LEN,
          }

        for i = 1, #WAVEFORM_SRC do
            if i > 10 then
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][NODE_NAME] = WAVEFORM_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][SRC_IDX] = WAVEFORM_SRC[i][2]
                if WAVEFORM_SRC[i][3] ~= nil then
                    param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][SRC_LABEL] = WAVEFORM_SRC[i][3]
                end
            else
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = WAVEFORM_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = WAVEFORM_SRC[i][2]
                if WAVEFORM_SRC[i][3] ~= nil then
                    param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_LABEL] = WAVEFORM_SRC[i][3]
                end
            end
        end
        printc("\nCShareMemSinkNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CWavFileWriteNode for Lout
    if FILE_LOUT_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CWavFileWriteNodeV2",
            [FILE_NAME] = FILE_LOUT_NAME,
            [SAMPLE_FMT]= SR_LEN,
          }

        for i = 1, #FILE_LOUT_SRC do
            if i > 10 then
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][NODE_NAME] = FILE_LOUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][SRC_IDX] = FILE_LOUT_SRC[i][2]
                if FILE_LOUT_SRC[i][3] ~= nil then
                    param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][SRC_LABEL] = FILE_LOUT_SRC[i][3]
                end
            else
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = FILE_LOUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = FILE_LOUT_SRC[i][2]
                if FILE_LOUT_SRC[i][3] ~= nil then
                    param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_LABEL] = FILE_LOUT_SRC[i][3]
                end
            end
        end
        printc("\nCWavFileWriteNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    --config CWavFileWriteNode for SPKout
    if FILE_SPKOUT_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CWavFileWriteNodeV2",
            [FILE_NAME] = FILE_SPKOUT_NAME,
            [SAMPLE_FMT]= SR_LEN,
          }

        for i = 1, #FILE_SPKOUT_SRC do
            if i > 10 then
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][NODE_NAME] = FILE_SPKOUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][SRC_IDX] = FILE_SPKOUT_SRC[i][2]
            else
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = FILE_SPKOUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = FILE_SPKOUT_SRC[i][2]
            end
        end
        printc("\nCWavFileWriteNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

    if SOUNDCARD_OUT_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CSoundCardWriteNode",
            [FILE_NAME] = OUTPUT_SOUNDCARD,
            [SAMPLE_FMT]= SR_LEN,
          }

        for i = 1, #SOUNDCARD_OUT_SRC do
            if i > 10 then
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][NODE_NAME] = SOUNDCARD_OUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(i-1)][SRC_IDX] = SOUNDCARD_OUT_SRC[i][2]
            else
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][NODE_NAME] = SOUNDCARD_OUT_SRC[i][1]
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(i-1)][SRC_IDX] = SOUNDCARD_OUT_SRC[i][2]
            end
        end
        printc("\CSoundCardWriteNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    end

--[[
    --config CShareMemSinkNode
    --if WAVEFORM_ENABLE == 1 then
        param[AUD_NODE..tostring(node_number)] =
          {
            [CLS_NAME]  = "CShareMemSinkNode",
            [NODE_NAME] = "CShareMemSinkNode1",
            [SAMPLE_FMT]= SR_LEN,
          }

       -- for i = 1, #WAVEFORM_SRC do
         --   if i > 10 then
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)][NODE_NAME] = NODE_SOUNDCARD_IN
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)][SRC_IDX] = SOUNDCARD_IN[1]
          --  else
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(1)] = {}
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(1)][NODE_NAME] = NODE_SOUNDCARD_IN
                param[AUD_NODE..tostring(node_number)][SRC_DES..tostring(0)..tostring(1)][SRC_IDX] = SOUNDCARD_IN[2]
         --   end
        --end
        printc("\nCShareMemSinkNode : "..dump(param[AUD_NODE..tostring(node_number)]),DEBUG)
        node_number = node_number + 1
    --end

]]
    return param
end


--[[
--set AB volume
VOLUME_MICIN = 6
VOLUME_LOUT = 0
VOLUME_SPKOUT = 0
function AB_set_volume(micin, lout, spkout)
    if CLIENT_MODE == "Client" then
        send_to_server('AB_set_volume',{micin, lout, spkout})
    else
        micin = micin or VOLUME_MICIN
        lout = lout or VOLUME_LOUT
        spkout = spkout or VOLUME_SPKOUT

        param = {
          u1=math.floor(micin),         --MIC gain = 18dB, only 4 step, 0,6,12,18
          u2=math.floor(lout*(-10)),    --LOUT Gain = -37.5dB @ 1.5dB step
          u3=math.floor(spkout*(-10)),  --SPK Gain = -15dB @ 1.5dB step
        }

        --printc("param >= "..dump(param));
        audiobridge.send_command(14, param);
        check_send_ab_command_result("Set volume", 10000)

        VOLUME_MICIN = micin
        VOLUME_LOUT = lout
        VOLUME_SPKOUT = spkout
    end
end
]]--

--set UIF volume
VOLUME_MICIN = 6
VOLUME_LOUT = 0
VOLUME_SPKOUT = 0
VOLUME_LIN = 0
function AB_set_volume(micin, lout, spkout, lin)
    if CLIENT_MODE == "Client" then
        send_to_server('AB_set_volume',{micin, lout, spkout,lin})
    else
        micin = micin or VOLUME_MICIN
        lout = lout or VOLUME_LOUT
        spkout = spkout or VOLUME_SPKOUT
        lin = lin or VOLUME_LIN
        if micin >18 then 
micin = 18 printc("The gain setting of Mic_in is out of range, use MAX 18dB.")
        elseif micin < 0 then micin = 0 printc("The gain setting of Mic_in is out of range, use MIN 0dB.")
        end
        if lout >53 then 
lout = 53 printc("The gain setting of LOUT is out of range, use MAX 53dB.")
        elseif lout < -69.5 then lout = -69.5 printc("The gain setting of LOUT is out of range, use MIN -69.5dB.")
        end
        if spkout >53 then 
spkout = 53 printc("The gain setting of SPKOUT is out of range, use MAX 53dB.")
        elseif spkout < -69.5 then spkout = -69.5 printc("The gain setting of SPKOUT is out of range, use MIN -69.5dB.")
        end
        if lin >67.5 then 
lin = 67.5 printc("The gain setting of LIN is out of range, use MAX 67.5dB.")
        elseif lin < -12 then lin = -12 printc("The gain setting of LIN is out of range, use MIN -12dB.")
        end
        param = {
          u1=math.floor(micin),   --MIC gain = 18dB, only 4 step, 0,6,12,18
          u2=math.floor(lout),    --LOUT gain = -69.5dB ~ 53dB @ 0.5dB step
          u3=math.floor(spkout),  --SPK gain = -69.5dB ~ 53dB @ 0.5dB step
          u4=math.floor(lin)      --LIN gain = -12dB ~ 67.5dB @ 0.5dB step
        }

        printc("param >= "..dump(param));
        audiobridge.send_command(14, param);
        check_send_ab_command_result("Set volume", 5000)

        VOLUME_MICIN = micin
        VOLUME_LOUT = lout
        VOLUME_SPKOUT = spkout
        VOLUME_LIN = lin
    end
end


function dowork()
    start_play_record()
    stop_play_record()
end


