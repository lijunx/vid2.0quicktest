UIF = {
    board_version = "UIF",
    type = 
    {
	    UART       = 9,
        I2C        = 1,
	    SPI        = 2,
	    GPIO       = 3,
	    AUDIO_BUS  = 4,  -- ATTRI: 0:PDM, 1:I2S, 2: TDM, 3: PCM
	    GPIO_I2C   = 5,  -- ATTRI: GPIO_SDA*16+GPIO_SCLK
	    I2C_SWITCH = 6,  -- ATTRI: 1:UIF_M, 2:UIF_S, 3:FM36_CODEC
	    PDMCLK_SEL = 7,  -- ATTRI: 1:1.024M, 2:2.048M, 3:3.072M, 4:4.096M
	    GPIO_CLK   = 8,
        CHIP_TYPE  = 9,   -- ATTRI: iM501:51

        PDM_I2S_PATH = 4,
    },
    GPIO = 
    {
        IO0 = 0,
        IO1 = 1,
        IO2 = 2,
        IO3 = 3,
        IO4 = 4,
        IO5 = 5,
        IO6 = 6,
        IO7 = 7,
        IO8 = 8,
        IO9 = 9,
        --PDMIN_CLK_DIR  = 8,
        --MATER_SLAVE_CTR= 9,
        AUDIO_RST = 10,
        CODEC_RST = 12,
        FM36_RST  = 11,
        I2C_SEL1  = 13,
        I2C_SEL1  = 14,
        I2C_SEL1  = 15,
        BUZZER    = 16,
    },
    attribute =
    {
        I2S = 1,
        PDM = 0,
        UIF_M = 1,
        UIF_S = 2,
        FM36_CODEC = 3,
        CHIP_TYPE_IM501 = 51
    },
    ID = 
    {
        FM1388 = 1388;
        iM501 = 501;
    }
}
UIF.__help = {}
UIFReset = 20
----------------------------------------------------------------------------------------------------------------------------
------------------------  Low Level Functions
----------------------------------------------------------------------------------------------------------------------------
function table_to_string( table_data )

    local str = ''
    if next(table_data) ~= nil then
		str = str..'{'
	    for k,v in pairs(table_data) do
			if(type(k) == 'string') then
				str = str..'[\"'..tostring(k)..'\"]='
			else
				str = str..'['..tostring(k)..']='
			end
			if(type(v) == 'table' ) then
				str = str..table_to_string(v)
			elseif(type(v) == 'string' ) then
                str = str..'\"'..tostring(v)..'\",'
			else
	            str = str..tostring(v)..','
			end
		end
		str = str..'},'
	end

	str = string.gsub( str, [[\]], [[\\]] )

	return str

end
-------------------------------------------------------------------------
function send_to_server( func_name_str, para_table )

	if para_table == nil then para_table = {}  end

	--print(#para_table)
	assert(type(func_name_str)=='string',"\nError:send_to_server func_name_str not a STRING !")
	assert(type(para_table)=='table' ,"\nError:send_to_server para_table not a TABLE !")

    local str = func_name_str..'('
  	for k,v in pairs(para_table) do
		if(type(v) == 'table' ) then
			str = str..table_to_string(v)
	    elseif(type(v) == 'string' ) then
            str = str..'\"'..tostring(v)..'\",'
	    else
	        str = str..tostring(v)..','
		end
	end
	str = str..'>\n'
    str = string.gsub( str, ",}", "}" )
    str = string.gsub( str, ",>", ")" )
	str = string.gsub( str, "(>", ")" )
	printc(str, DEBUG)
	client:send( str )
	client:settimeout(5)

    --data1 = client:receive('*l')
    --print(data)
    -- data1  = client:receive('*l')
    --print(data1)
    --return data

    local state  = false
    local data   = nil
    local endflag= false
    local result = ''

    repeat
      data  = client:receive('*l')
      --print("["..data.."]")
      if data == 'ok'  then
      	state = true
      	endflag  = true
      elseif data==nil then
		state = false
      	endflag  = true
      else
      	result = result..data
      	--print("["..data.."]")
      end
    until( data == nil or endflag == true)
    --print("<"..result..">")
    return state, result

end

--------------------------------------------------------------

function send_to_client( str )
    save_client_mode()
    print(str)
    g_client:send(str .. '\n')
    restore_client_mode()
end

function get_client_number()
    if CLIENT_MODE == "Client" then
        local number = send_to_server('get_client_number')
        print(number)
        return number

    else
        print("\nRead Client Number: "..#socks)
        if CLIENT_MODE == "Server" then
            send_to_client(#socks)
        else
            return  #socks
        end
    end
end
-------------------------------------------------

--*****************
UIF.__help.set_interface =
[==[
Usage: UIF.set_interface(UIF_type, UIF_speed, UIF_attribute)
  set UIF type.
  Param:  UIF_type, UIF_speed, UIF_attribute
    IF_type:
       1: I2C, no attribute setting. speed, 400 means 400KHz.
       2: SPI, no attribute setting. speed, 1000 means 1000KHz.
       3: GPIO, no speed setting. attribute, 1: high, 0:low.
       --4: AUDIO_BUS, select pdm or i2s pathh. no speed setting. attribute: 0:PDM, 1:I2S, 2: TDM, 3: PCM
	   5: GPIO_I2C, which GPIO will be used to I2C(for iM205), speed just surport 200khz.
	      Attribute, GPIO_SDA*16+GPIO_SCLK.
	      If you defined GPIO6 to SDA, GPIO7 to SCLK, attribute = 0x67.
	   6: I2C_SWITCH, select which I2C connect to I2C host bus.
	      no speed setting. Attribute, 1:UIF_M, 2:UIF_S, 3:FM36_CODEC.
	   7: PDMCLK_SEL,  set pdm clk that output by FM36.
	      no UIF_speed setting. Attribute: 1: 1.024M, 2: 2.048M, 3: 3.072M, 4: 4.096M
  Return: true or false
]==]
function UIF.set_interface(UIF_type, UIF_speed, UIF_attribute, debug)
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.set_interface',{UIF_type, UIF_speed, UIF_attribute, debug})
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then return false end
        printc("\nSet type and speed of interface.", debug)
        printc("  Interface type is : "..UIF_type, debug)
        printc("  Interface speed is: "..UIF_speed, debug)
        if UIF_attribute ~= nil then printc("  Interface attribute is: "..UIF_attribute, debug) end
        local param=
        {
            u1=UIF_type,
            u2=UIF_speed,
            u3=UIF_attribute or 0
            }
        audiobridge.send_command(30, param)
        check_send_ab_command_result( "Set interface", 2000 )
    end
end

------------------------------------------------------------

------------------------------------------------------------
global_raw_write_pack = {}
global_raw_write_index= 0
global_emb_pack_num   = 0
function UIF.raw_write_fast(UIF_type, device_addr, data_array, block_num, flag)
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.raw_write_fast',{UIF_type, device_addr, data_array, block_num, flag})
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then return false end
        if( flag ~= true ) then
    	    --printc(data_array, 1)

    		for i = 1, #data_array do
    			global_raw_write_pack[global_raw_write_index + i] = data_array[i]
    	    end

    		global_emb_pack_num = global_emb_pack_num + 1
    		global_raw_write_index = global_raw_write_index+#data_array
    	    if( global_emb_pack_num < block_num ) then
    			return true
    	    end
        	--printc(global_raw_write_pack, DEBUG)
        end
        printc("\nRaw write fast::", DEBUG)

        local param=
        {
            u1=UIF_type,
            u2=device_addr,
            u3=#global_raw_write_pack,
            b4=global_raw_write_pack
        }
        audiobridge.send_command(31, param)
        
        global_raw_write_pack = {}
        global_emb_pack_num   = 0
        global_raw_write_index= 0
    	--io.read()
        return check_send_ab_command_result( "Raw write fast", 2000 )
    end
end
---------------------------------------------------------------

function UIF.raw_write(UIF_type, device_addr, data_array)
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.raw_write',{UIF_type, device_addr, data_array})
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then return false end
        printc("\nRaw write:", DEBUG)
        --printc(data_array, DEBUG)
        local param=
        {
            u1=UIF_type,
            u2=device_addr,
            u3=#data_array,
            b4=data_array
        }
        audiobridge.send_command(31, param)
        return check_send_ab_command_result( "Raw write", 2000 )
    end
end

function UIF.raw_read(UIF_type, device_addr, length, pre_write_data)
    if CLIENT_MODE == "Client" then
        local state,str = send_to_server('UIF.raw_read',{UIF_type, device_addr, length, pre_write_data})
        if state then
            tab= {}
            assert(pcall(loadstring("tab=".."{"..str.."}")))
         end
        --print(unpack(tab))
        return tab
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then return false end
        local param=
        {
            u1=UIF_type,
            u2=device_addr,
            u3=length,
            u4=#pre_write_data,
            b5=pre_write_data
        }
        audiobridge.send_command(32, param)
        local r, v = wait_rpt(31, 10000)
        if r == false then printc("Single read ERROR!") return false, {} end
        printc("Interface type = "..v.u1, DEBUG);
        printc("Device address = "..v.u2, DEBUG);
        printc("Data length    = "..v.u3, DEBUG);
        state = check_send_ab_command_result( "Raw read", 10000 )
        if not state then return false, {} end

        local tab = {}
        for i,k in pairs(v.b4) do  --the table than returned by ABD is start from 0
            tab[i+1] = k
        end
        --printc(tab, DEBUG)
        if CLIENT_MODE == "Server" then
            local data_str = ''
            for i = 1, #tab do
                if i>1 then data_str = data_str..',' end
                data_str = data_str..tostring(tab[i])
            end
            send_to_client( data_str )
        else
            return tab
        end
    end
end

function UIF.flash_write(addr_index, package, vec_name )
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.flash_write',{addr_index, package, vec_name})
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then return false end
        printc("\nflash write.")
        local param=
        {
            u1=addr_index,
            u2=#package,
            b3=package,
            b4=vec_name
        }
        audiobridge.send_command(40, param)
        printc(package, DEBUG)
        return check_send_ab_command_result( "Flash write", 2000 )
    end
end

function UIF.burst_write(UIF_type, device_addr, mem_addr, mem_addr_len, length, bin_data)
    --UIF.usb_con_assert()
    if HW_USB_CONNECTION_STATE ~= 1 then return end
    --printc("\nBurst write.")
    --printc("Memory address = "..mem_addr, DEBUG);
    local mem_addr_h = mem_addr%65536
    local mem_addr_l = math.floor(mem_addr/65536)
    local param=
    {
        u1=UIF_type,
        u2=device_addr,
        u3=mem_addr_l,
        u4=mem_addr_h,
        u5=mem_addr_len,
        u6=length,
        b7=bin_data
    }
    audiobridge.send_command(33, param)
    check_send_ab_command_result( "Burst write", 2000 )
end

function UIF.burst_read(UIF_type, device_addr, mem_addr, mem_addr_len, package_len, read_len, package)
    --UIF.usb_con_assert()
    if HW_USB_CONNECTION_STATE ~= 1 then return end
    --printc("\nBurst read.")
    local mem_addr_h = mem_addr%65536
    local mem_addr_l = math.floor(mem_addr/65536)
    local param=
    {
        u1=UIF_type,
        u2=device_addr,
        u3=mem_addr_l,
        u4=mem_addr_h,
        u5=mem_addr_len,
        u6=package_len,
        u7=read_len,
        u8=package
    }
    audiobridge.send_command(34, param)
    local r, v = wait_rpt(33, 10000)
    if r == false then printc("Burst read ERROR!") return false end
    printc("Interface type = "..v.u1, DEBUG);
    printc("Device address = "..v.u2, DEBUG);
    printc("Memory address = "..(v.u3+v.u4*65536), DEBUG);
    printc("MMR addr len   = "..v.u5, DEBUG);
    printc("Package length = "..v.u6, DEBUG);
    local state = check_send_ab_command_result( "Burst read", 10000 )
    if not state then return false, {} end
    local tab = {}
    for i,k in pairs(v.b7) do  --the table that returned by ABD is start from 0
        tab[i+1] = k
    end
    return tab
end

--*****************
UIF.__help.init =
[==[
Usage: UIF.init()
  Init UIF board. set I2C/SPI speed, set pdm or i2s path.
  Param:  no.
  Return: no.
]==]
function UIF.init()

    if HW_USB_CONNECTION_STATE ~= 1 then return end
    printc("UIF.init()")
    --UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.FM36_CODEC)
    UIF.cmd_write(UIFReset)
    if not UIF.get_board_ver() then return end
    UIF.set_interface(UIF.type.I2C, I2C_SPEED)
    UIF.set_interface(UIF.type.SPI, SPI_SPEED, SPI_MODE)

    UIF.check_SAB()

    if AUDIO_BUS_TYPE == 1 then UIF.set_interface(UIF.type.PDMCLK_SEL, 0, PDM_ADC_CLK_SEL+PDM_DAC_CLK_SEL*16) end
    --set_record_configuration()
    --set_play_configuration()
    --update_audio_configuration()--added for play&rec sync issue
    --local pdm_i2s = AUDIO_BUS == "PDM" and UIF.attribute.PDM or UIF.attribute.I2S
    ----expand : TDM, PCM?????
    --UIF.set_interface(UIF.type.AUDIO_BUS, 0, pdm_i2s)

    AB_set_volume(VOLUME_MICIN, VOLUME_LOUT, VOLUME_SPKOUT, VOLUME_LIN)
    --UIF.set_interface(UIF.type.I2C_SWITCH, 0, UIF.attribute.UIF_S)
end

--*****************
UIF.__help.set_i2c_gpio =
[==[
Usage: UIF.set_i2c_gpio(speed, sda, sclk)
  Set GPIO.
  Param:
    speed: I2C speed.
    sda: which GPIO used to SDA.
    sclk: which GPIO used to SCLK.
  Return: no.
]==]
function UIF.set_i2c_gpio(speed, sda, sclk)
    local attri = sda*16+sclk
    printc("Define GPIO["..sda.."] to SDA")
    printc("Define GPIO["..sclk.."] to SCLK")
    printc("I2C speed is "..speed)
    UIF.set_interface(UIF.type.GPIO_I2C, speed, attri)
end

--*****************
UIF.__help.set_gpio =
[==[
Usage: UIF.set_gpio(gpio_num, value)
  Set GPIO.
  Param:
    gpio_num.
    value: 0: low, 1: higt. default 1.
  Return: no.
]==]
function UIF.set_gpio(gpio_num, value)
    if gpio_num == nil then return end
    value = value or 1
    printc("Set GPIO["..gpio_num.."] = "..value)
    UIF.raw_write(UIF.type.GPIO, gpio_num, {value})
end

--*****************
UIF.__help.I2C_switch =
[==[
Usage: UIF.I2C_switch(status)
  Switch I2C between FM36_CODEC and UIF_S_I2C.
  Param:  status. 2: I2C bus connect to UIF_S_I2C; 3: I2C bus connect to FM36 and codec. default is 3.
  Return: no
]==]
UIF.I2C_switch = function(status)
    status = status or 3
    if status == 3 then
        UIF.set_interface(UIF.type.I2C_SWITCH, 0 , UIF.attribute.FM36_CODEC)
    elseif status == 2 then
        UIF.set_interface(UIF.type.I2C_SWITCH, 0 , UIF.attribute.UIF_S)
    elseif status == 1 then
        UIF.set_interface(UIF.type.I2C_SWITCH, 0 , UIF.attribute.UIF_M)
    end
end
 
function UIF.load_vec(addr_index, vec_file, str_name, interface)
    --if HW_USB_CONNECTION_STATE ~= 1 then return end
    interface = interface or 1
    --local file = assert(io.open(vec_file, "r"), "function UIF.load_vec() ERROR: Can't find the vec file! Please check it! "..vec_file)
    local file = io.open(vec_file, "r") --assert(io.open(vec_file, "r"), "function load_vec() ERROR: Can't find the vec file! Please check it! "..vec_file)
    if file == nil then
        printc("Vec file can't be found. "..vec_file)
        vec_file = iM501.vec.default_path..str_name
        printc("use default path."..vec_file)
        file = io.open(vec_file, "r")
    end
    local i = 0
    printc("\nFile name is: "..vec_file)
    local package = {}

    for line in file:lines(file) do
        i = i + 1
        local token = line_to_token (line)
        for j = 1, #token do
            if string.find(token[j], "0x") == nil and string.find(token[j], "0X") == nil then  --??????????OX????
                token[j] = "0x"..token[j]
            end
        end
        if #token >= 2 then
            local address, para = tonumber(token[1]), tonumber(token[2])
            printc(address, para, interface)
            local dat_len = 1
            if #token == 3 then dat_len = tonumber(token[3]) end
            if dat_len ~= 1 and dat_len ~= 2 then dat_len = 1 end
            local tab = {}
            if dat_len == 2 and DSP_NAME == "iM501" then
                tab = {dat_len+2, iM501.CMD.REG_WR_2, address, math.floor(para/256), para%256}
            elseif dat_len == 1 and DSP_NAME == "iM501" then
                if interface == 1 then
                    tab = {dat_len+2, iM501.CMD.REG_WR_1, address, para%256}
                elseif interface == 2 then  --for spi write
                    tab = {dat_len+2, iM501.CMD.SPI_REG_WR, address, para%256}
                end
            elseif DSP_NAME == "iM401" then
                tab = {3, address, math.floor(para/256), para%256}
                --print("*****AAAAAAA")
            end

            for j = 1, #tab do table.insert(package, tab[j]) end

        elseif #token ~= 0 then
            printc("LOG_READ_MAP(line "..tostring(i)..", token_num "..tostring(#token).."): "..line)
        end
    end

    file:close()
    --print(unpack(package))
    --print(str_name)
    if #package == 0 then package = {0} end
	local package2 = Convert_String_Table(str_name)
--print(unpack(package2))
    UIF.flash_write(addr_index, package, package2)
    --printc("********************\n")
end

function UIF.set_vec_config(vec_index_a, vec_index_b, delay_s, chip_id, irq_pin, enable, turn_off_pdmclk_en, interface)
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.set_vec_config',{vec_index_a, vec_index_b, delay_s, chip_id, irq_pin, enable, turn_off_pdmclk_en, interface})
    else
        if HW_USB_CONNECTION_STATE ~= 1 then return end
        printc("\nset dsp vec configration.")
        local param=
        {
            u1=vec_index_a,
            u2=vec_index_b,
            u3=delay_s,
            u4=chip_id or 51, -- chip type:  41: iM401, 51: iM501
            u5=irq_pin or 0,  -- trigger IRQ GPIO index : 0~ 7
            u6=enable or 0,   -- 0: disable trigger load vec, 1: enable
            u7=turn_off_pdmclk_en or 1,
            u8=interface or 1
        }
        printc("Chip id: "..param.u4)
        printc("IRQ GPIO number: "..param.u5)
        printc("Trigger enable: "..param.u6)
        printc("PDM CLK Turn off enable: "..param.u7)
        printc("Write parameter interface: "..param.u8)
        audiobridge.send_command(41, param)
        printc(package, DEBUG)
        return check_send_ab_command_result( "Set vec config", 2000 )
    end
end

function UIF.session_delay(times, unit) --"	unit��o=1: second.  =2: millisecond. =3: microsecond.
    if HW_USB_CONNECTION_STATE ~= 1 then message("Error", "USB Detached") return end
    printc("\nSession delay.")
    local param = {u1=1}
    printc("Delay unit "..unit)
    audiobridge.send_command(35, param)
    check_send_ab_command_result( "session_delay", 1000 )
    param =
    {
        u1 = unit,
        u2 = times
    }
    audiobridge.send_command(36, param)
    check_send_ab_command_result( "session_delay", 1000 )
    audiobridge.send_command(35, param)
    check_send_ab_command_result( "session_delay", 1000 )
end

--------------------------------------------------------------------

client_mode_save = ""
client_mode_save_flag = false
function save_client_mode( mode )
	client_mode_save = CLIENT_MODE
	CLIENT_MODE = mode
	client_mode_save_flag = true
end

function restore_client_mode()
    if client_mode_save_flag then
        client_mode_save_flag = false
		CLIENT_MODE = client_mode_save
		print(CLIENT_MODE)
	end
end

--------------------------------------------------------------------

function UIF.get_board_ver()
    if CLIENT_MODE == "Client" then
        local state,ver = send_to_server('UIF.get_board_ver')
        print(state, ver)
        if( state == true )  then
            UIF.version = ver
        else
            UIF.version = "N/A"
        end
        printc("UIF version is "..UIF.version)

        return state,UIF.version
    else
        local r, v, w
        if HW_USB_CONNECTION_STATE ~= 1 then
            --message("Error", "USB Detached")
            printc("USB Detached, ".."Read board version failed.")
            UIF.version = "N/A"
            --return false, "N/A"
            r = false
        else
            printc("\nRead UIF board Info...")
    
            --clear_rpt()
            audiobridge.send_command(11)
    
            r, v = wait_rpt(50, 100)

            if(r == true ) then
                r, w = wait_rpt(1011, 1000)
            end

            if( r == true )  then
                UIF.version = v.t1..v.t2..v.t3
            else
                UIF.version = "N/A"
            end
        end
        printc("UIF version is "..UIF.version)

        --print(CLIENT_MODE)
        if CLIENT_MODE == "Server" then
            send_to_client(UIF.version)
        end
        
        return r, UIF.version
    end
end


function UIF.check_SAB()
	if string.find(UIF.version, "SAB")  then
	   printc("SAB connected.")
	   if DSP_NAME == "FM1388" then
           FM1388.GPIO.RST  = 3
           FM1388.GPIO.SEL_PACKAGE  = 1
           FM1388.GPIO.LDO = 0  --SAB
       end
       UIF.board_version = "SAB"
       return true
	elseif string.find(UIF.version, "UIF") then
	    if DSP_NAME == "FM1388" then
            FM1388.GPIO.RST  = 6
            FM1388.GPIO.SEL_PACKAGE  = 3
            FM1388.GPIO.LDO = 1
        end
        UIF.board_version = "UIF"
	else
        UIF.board_version = "N/A"
	end
	return false
end


function UIF.ping0()
    local status_change, usb_state = update_usb_state()
    print( "Ping : ", status_change, usb_state )
    if( status_change == true ) then
	    if ( usb_state == false ) then
	       HW_USB_CONNECTION_STATE = 0
	    else
	       printc("UIF Re-Connected...")
	       HW_USB_CONNECTION_STATE = 1
	       UIF.init()
	    end
    end
end

function UIF.ping()
    local status_change, usb_state = update_usb_state()
    print( "Ping : ", status_change, usb_state )
    if( status_change == true and  usb_state == true ) then
        printc("UIF Re-Connected...")
        UIF.init()
    end
    return status_change, usb_state
end

function UIF.usb_con_assert()

	if(HW_USB_CONNECTION_STATE == 0) then
		print_prompt_err("ERROR: Can't find board ! \rPlease check HW and USB connection !")
	end

end

function check_USB_state()

    if CLIENT_MODE == "Client" then
        local state, value = send_to_server('check_USB_state')
        return tonumber(value)
    else
    --[[
        local res = HW_USB_CONNECTION_STATE == 1
	    if CLIENT_MODE == "Server" then
            send_to_client( tostring(res) )
        else
            return  res
        end
        ]]
        UIF.ping()

	    if CLIENT_MODE == "Server" then
            send_to_client(tostring(HW_USB_CONNECTION_STATE) )
        end
        return  HW_USB_CONNECTION_STATE + 0
	end

end

function UIF.set_DSP_type(type)  --Sent DSP type to server
    type = type or "FM1388"
    if CLIENT_MODE == "Client" then
        DSP_NAME = type
        send_to_server('UIF.set_DSP_type',{type})
    else
        DSP_NAME = type
	    print(DSP_NAME)
	    if DSP_NAME ~= "FM1388" then  --Default DSP settin is FM1388
	        require(SCRIPT_PATH..DSP_NAME.."")
	    end
	end
end
--------------------------------------------------------------------
function get_version (version, keyword)
    local str = version
    local str1 = ""
    local flag = false
    repeat  --get version for 
        local s, e = string.find(str, "%b[]")
        print(s,e)
        if s ~= nil then
            str1 = string.sub(str, s+1,e-1)
            if string.find(str1, keyword) then 
                s, e = string.find(str1, "(%d+).(%d+)")
                if s ~= nil then
                    flag = true
                    version = string.sub(str1, s, e) + 0 
                else 
                    verison = 0
                end
                break 
            end
            str = string.sub(str, e+1, -1)
        else
            str1 = ""
            version = 0
            break
        end
        
    until false

    print(flag, str1, version)
    return flag, str1, version
end

function UIF.Check_FW_Version( Host_FW_Ver, Audio_FW_Ver )
    local ver = UIF.version
    print(ver)
    local _, host_ver_str, host_ver = get_version(ver, "FW:H")
    local _, audio_ver_str, audio_ver = get_version(ver, "FW:A")

    if host_ver_str < Host_FW_Ver or audio_ver_str < Audio_FW_Ver then
        printc("ERROR: UIF Firmware version not match this script for FM1388/iM501 SPI Rec.")
        printc("You should update to: "..Host_FW_Ver.." and "..Audio_FW_Ver.."\n" )
        return false
    end
    return true
end

function UIF.Write_Flash(addr,data_array,pack_num)
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.Write_Flash',{addr,data_array,pack_num})
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then message("Error", "USB Detached") return end
        local param=
        {
            u1=#data_array,
            b2=data_array,
            u3=pack_num,
        }
        audiobridge.send_command(50, param)
        return check_send_ab_command_result( "send Write flash CMD", 1000 )
    end
end

function UIF.Updata_FW(bin_file)
    if HW_USB_CONNECTION_STATE ~= 1 then message("Error", "USB Detached") return end
    local addr=0x00800000
    local data_array={0x00,0x08,0x00,0x00,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}
    local tab = {}

    local data_tab = FM1388.code_convert(bin_file, FM1388.EMB_DATA_SIZE_RAM, 1)
    for i = 1, #data_tab do
        for j=1,#data_tab[i] do
            table.insert(tab, data_tab[i][j])
        end
        printc(tab,1)
		UIF.Write_Flash(addr,tab,#data_tab)
		tab = {}
    end
end

function UIF.cmd_write(cmd, param)
    if CLIENT_MODE == "Client" then
        send_to_server('UIF.cmd_write',{cmd, param})
    else
        --UIF.usb_con_assert()
        if HW_USB_CONNECTION_STATE ~= 1 then return end
        printc("\nCmd write:", DEBUG)
        param = param or {}
        printc(dump(param), DEBUG)
        audiobridge.send_command(cmd, param)
        return check_send_ab_command_result( "Cmd:"..cmd.." write", 2000 )
    end
end

function UIF.Reset()
		param={}
		audiobridge.send_command(20, param)
		check_send_ab_command_result( "uif reset", 1000 )
end

function UIF.clear_flag()
    UIF.cmd_write(52, {})
end

----mask  low bit is channel[0],  high bit is channel[10].1: Enable   0: Disable
function UIF.spi_record()
    if CLIENT_MODE == "Client" then
		return
	else
        printc("\nStart SPI Recording:")
        mask = mask or 0x03ff
        local param=
        {
             u1=SPI_REC_CH_MASK,
             u2=0,--play mask
             u3=0,--play time tick
             u4=2,--irq gpio index
             u5=SPI_MODE,
        	 u6=SPI_SPEED,
        	 u7=chip_id or 501,
        }
        audiobridge.send_command(47, param)
        check_send_ab_command_result("send SPI recording CMD", 6000)
        return true
    end
end