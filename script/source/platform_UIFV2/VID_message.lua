iM501 = iM501 or {}
--**************************************************--
iM501.message =
{
    -- write ((real_command << 2) or 0x01) to interrupt_dsp register.
    configure_vid = 0x1,   -- system config, set system functional.?
    enter_psm = 0x2,       -- 0x80000000
    back_nm = 0x1,
    hw_mic_bypass = 0x3,
    configure_sd = 0x5,    -- attribute: = 1, ni hao xiao E; = 2, zmkm; = 4 hello blue genie
                           -- attribute: = 0, kewword triggered; =1, command; =2 keyword and command
    start_vb_transfer = 0x04,
    terminate_vb_transfer = 0x05,
    spi_record = 0x6,      --0, stop; 1, start
    terminate_spi_record = 0x07,
    start_debug_mode = 0x8,
    end_debug_mode = 0x9,
    configure_mic_input = 0x08, -- bits 0-7: I2C address; 8-11: mic version; 12-13: mic number;
                                -- 14: 1: digital mic, 0: analog mic; 15: 1 active mic.
    access_mic = 0x09,     -- bits 0-7: register value to be written, 8-11: register address
    config_output = 0x0A,  -- 0-3, 1:VEN to PDMO0; 2: MIC0; 3: MIC1; 4: MIC2; 5: ECHO REF. 4-7: For PDMO1
    read_mic = 0x0B,       -- 0-3: register index; 4-7: mic number. check DRAM 0xfffbee4.
    config_mips = 0x0f,    -- 0-7: MIPS
    config_avd = 0x11,     -- 0-7: AVD parameter will be written to reg 6 of smart MIC; 8-11: xAA timer;
                           -- 12-13: AVD clear timer
    config_xaa = 0x12,     -- 0-7: parameter index; 8-23: 16bit parameters


};
iM501.message_return = 
{
    mic_num = 0xfffbf18,
    mic0_type = 0xfffbf1a,   --house smart mic must connect to MIC0.
    mic0_hladc = 0xfffbf1c,
    
    output_type = 0xfffbf1e,
    psm_sr = 0xfffbf2c,
    nm_sr = 0xfffbf2e,

   -- psm_en = 0xfffbf70,
    avd_en = 0xfffbf70,
    psm_xsd_option = 0xfffbf74, --0: disable; 1: xSD after VEN; 2: xSD without VEN
    psm_sd_option = 0xfffbf78,  --0: disable; 1: SD after VEN; 2: SD without VEN
    nm_ven_en = 0xfffbf7a,      
    nm_xsd_option = 0xfffbf7c,
    nm_sd_option = 0xfffbf7e,

    pdmtxo0l = 0xfffbf88,     --0: MIC0; 1:MIC1; 2:MIC2; 3:MIC3; 4:RX_L; 5:RX_R; 6:VEN_OUT; 7: No signal
    pdmtxo0r = 0xfffbf8a,
    pdmtxo1l = 0xfffbf8c,
    pdmtxo1r = 0xfffbf8e,

    nm_output_type = 0xfffbf90,  --0:PDM Bypass; 1:pull low; 2:PDM; 3:TX
    psm_output_type = 0xfffbf92,

    xaa_timer = 0xfffbfa8,    --00: disable; 2:4s; 4:8s; 6:12s; 8:16s; a:20s; c:24s; e:28s; 3:64s; 5:128s; 7:192s; 9:256s; b:320s; d:384s; f:448s
    avd_clear_timer = 0xfffbfaa, --00: disable; 1:2s; 2:4s; 3:8s  (pdmcki=1.024M)

    xaa_spl = 0xfff9fd4,
    xaa_environment = 0xfff9fd0,
	xaa_floor = 0xfff9fd8,
    command_detect = 0xfffbf08,
    command_detecth = 0xfffbf0a,

    spi_channel = 0xfff9fda,
    
    vb_channel = 0xfff9fdc,
    vr_triggered_channel = 0xfff9fde,

    --[[
         0xfffbf08 = 0xmnpq, 
         0xfffbf0a = 0xhjkl
         q= number of commands detected  
         p = first command detected
         n= second command detected
         m= third command detected
         l= fourth command detected.
         k=fifth command detected.
         j= sixth command detected
         h= seventh command detected.
         The maximum number of commands stored is 7.
     ]]

};

--sd_attribute
iM501.sd_attribute = 
{
    option = {"Ni Hao Xiao E", "Zi Ma Kai Men", "Hello Blue Genie"};
    value = {1, 2, 4};
}
iM501.sd_function = 
{
    option = {"Keyword triggered", "Command Triggered", "Keyword and Command Triggered"};
    value = {0, 1, 2};
}
iM501.output_attribute = 
{
    option = {"VEN", "MIC0", "MIC1", "MIC2", "Echo"};
    value = {1,2,3,4,5}
}
iM501.avd_attribute = 
{
    reg6 = {},  --0-7
    xaa_timer =  --8-11
    {
        option =  {"Disable", "4s", "8s", "12s", "16s", "20s", "24s", "28s", "64s", "128s", "192s", "256s", "320s", "384s", "448s"};
        value = {0, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x3, 0x5, 0x7, 0x9, 0xb, 0xd, 0xf};
    };
    avd_clr_timer =  -- 12-13
    {
      {
          option = {"Disable", "2s", "4s", "8s"};
          value = {0,1,2,3};
      };
      {
          option = {"Disable", "1s", "2s", "4s"};
          value = {0,1,2,3};
      };
    };
}

-- im501_system_config = iM501.update_vid_config(im501_system_config, value, index)
--[[
   avd on : iM501.update_vid_config(2,0,0,0,1)
   avd off: iM501.update_vid_config(2,0,0,0,0)
   PSM xSD on : iM501.update_vid_config(1,0,1,0) --disable VEN
   PSM xSD off: iM501.update_vid_config(1,0,0,0)
   PSM  SD on : iM501.update_vid_config(1,1,1,0)
   PSM  SD off: iM501.update_vid_config(1,1,0,0)
   NM  xSD off: iM501.update_vid_config(0,0,1,0)
   NM  xSD off: iM501.update_vid_config(0,0,0,0)
   NM   SD on : iM501.update_vid_config(0,1,1,0)
   NM   SD off: iM501.update_vid_config(0,1,0,0)

]]
function iM501.update_vid_config(mode, detection_type, detection_on_off, ven_on_off, avd_on_off)
    local mode_offset, type_offset, detection_offset, ven_offset, avd_offset = 12, 8, 0, 1, 2
    mode = mode or 0
    detection_type = detection_type or 0
    detection_on_off = detection_on_off or 0
    ven_on_off = ven_on_off or 0
    avd_on_off = avd_on_off or 0
    if type(mode) == "string" then
        if string.upper(mode) == "NM" then mode = 0
        elseif string.upper(mode) == "PSM" then mode = 1
        elseif string.upper(mode) == "AVD" then mode = 2
        else mode = 0
        end
    end

    if type(detection_type) == "string" then
        if string.upper(detection_type) == "XSD" then detection_type = 0
        elseif string.upper(detection_type) == "SD" then detection_type = 1
        else detection_type = 0 
        end
    end

    if ven_on_off == 1 then detection_on_off = 1 end
    
    local value = mode * (2^mode_offset) + detection_type * (2^type_offset)
                  + detection_on_off * (2^detection_offset)
                  + ven_on_off * (2^ven_offset) + avd_on_off * (2^avd_offset)
    print(value)
    iM501.write_message(iM501.message.configure_vid, value, DL_PATH)
    --local _, temp = iM501.read_dram(iM501.vec.system_config_check)
    --if temp == src then print("Update sccussfully.") end
    --printc(string.format("0x%0x", src))
    return true
end

function iM501.check_psm_en()
    local _, psm_en = iM501.read_dram(iM501.message_return.psm_en, 2)
    return psm_en
end

function iM501.check_mic_setting()
    local _, mic_type = iM501.read_dram(iM501.message_return.mic0_type, 2) -- 0: not inhouse, 1:iM205
print(mic_type)
    return mic_type
end

function iM501.check_avd_en()
    local _, avd_en = iM501.read_dram(iM501.message_return.avd_en, 2) -- 0: disable, 1: enable
print(avd_en)
    return avd_en
end

function iM501.check_psm_xsd()
    local _, psm_xsd_en = iM501.read_dram(iM501.message_return.psm_xsd_option, 2) -- 0: disable, 1:enable with VEN, 2: enable without VEN
print(psm_xsd_en)
    return psm_xsd_en
end

function iM501.check_psm_sd()
    local _, psm_sd_en = iM501.read_dram(iM501.message_return.psm_sd_option, 2) -- 0: disable, 1:enable with VEN, 2: enable without VEN
print(psm_sd_en)
    return psm_sd_en
end

function iM501.check_command()
    local state, command = iM501.read_dram(iM501.message_return.command_detect, 4) -- 0: disable, 1:enable with VEN, 2: enable without VEN
    local flag, command_num, command_detect = false, 0, 0
    if state then
        command_num = command%0x10
        if command_num > 7 then
            command_num = 0
            flag = false
        else
            command_detect = math.floor(command/0x10)
            flag = true
        end
    end
print(flag, command, command_num, command_detect)

    return flag, command_num, command_detect
end

function iM501.write_mic_setting(mic_type_new)   --read back, update, write
    --local mic_type  = iM501.check_mic_setting()

    mic_type_new = mic_type_new or 1--mic_type


    local mic_set = 0   --
    iM501.write_message(iM501.message.configure_mic_input, mic_type_new, DL_PATH)
end
