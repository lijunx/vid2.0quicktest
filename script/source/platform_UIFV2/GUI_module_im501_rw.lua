--module define
printc("define iM501 write&read GUI module.")
GUI.im501_rw = {}
GUI.im501_rw.constant = 
{
    spi_reg_addr_default = 1, --0x00
    i2c_reg_addr_default = 8, --0x0f
}
GUI.im501_rw.list = 
{
    spi_reg_addr = iup.list
    {
        "0x00","0x01","0x02";
        dropdown="YES", visible_items=3, value = GUI.im501_rw.constant.spi_reg_addr_default, size="60x", action_time = false,
    };
    i2c_reg_addr = iup.list
    {
        "0x08","0x09","0x0A","0x0B","0x0C","0x0D","0x0E","0x0F","0x10","0x11","0x12"; 
        dropdown="YES", visible_items=6, value = GUI.im501_rw.constant.i2c_reg_addr_default, size="60x", action_time = false,
    };
};

GUI.im501_rw.label = 
{   
    addr_s1 = iup.label{size="80x",title = "IRAM   Address(0x):"};
    data_s1 = iup.label{title = "Value(0x):"};
    data_len1 = iup.label{title = "Len(0x):"};
    addr_s2 = iup.label{size="80x",title = "DRAM  Address(0x):"};
    data_len2 = iup.label{title = "Len(0x):"};
    data_s2 = iup.label{title = "Value(0x):"};
    addr_s3 = iup.label{size="80x",title = "SPI Register(0x):"};
    data_s3 = iup.label{title = "Value(0x):"};
    addr_s4 = iup.label{size="80x",title = "I2C Register(0x):"};
    data_s4 = iup.label{title = "Value(0x):"};
    addr_s5 = iup.label{size="80x",title = "Message Code(0x):"};
    data_s5 = iup.label{title = "Value(0x):"};
    dsp2host = iup.label{size="80x", title= "DSP to Host Value(0x)"};
};
--EFT_FILE = import_gui_default("eft_file")
--EFT_ADDR = import_gui_default("eft_addr")
GUI.im501_rw.text =
{
    iram_addr = iup.text{value = import_gui_default("iram_addr"), size="45x"};
    iram_len  = iup.text{value = import_gui_default("iram_len"),  size="35x"};
    iram_data = iup.text{value = import_gui_default("iram_data"), size="45x"};
    dram_addr = iup.text{value = import_gui_default("dram_addr"), size="45x"};
    dram_len  = iup.text{value = import_gui_default("dram_len"),  size="35x"};
    dram_data = iup.text{value = import_gui_default("dram_data"), size="45x"};
    dsp_cmd   = iup.text{value = import_gui_default("dsp_cmd"), size="60x"};
    cmd_attr  = iup.text{value = import_gui_default("cmd_addr"), size="60x"};
    --spi_reg_addr = iup.text{value = "", size="60x"};
    spi_reg_data = iup.text{value = "", size="60x"};
    --i2c_reg_addr = iup.text{value = "", size="60x"};
    i2c_reg_data = iup.text{value = "", size="60x"};
    dsp2host = iup.text{value = "", size="182x"};
};

GUI.im501_rw.button =
{
    iram_r = iup.button{size="20x", title="R"};
    iram_w = iup.button{size="20x", title="W", active = "no"};
    dram_r = iup.button{size="20x", title="R"};
    dram_w = iup.button{size="20x", title="W"};
    dsp_cmd_r = iup.button{size="20x", title="R", active = "no"};
    dsp_cmd_w = iup.button{size="20x", title="W"};
    spi_reg_r = iup.button{size="20x", title="R"};
    spi_reg_w = iup.button{size="20x", title="W"};
    i2c_reg_r = iup.button{size="20x", title="R"};
    i2c_reg_w = iup.button{size="20x", title="W"};
    dsp2host_r = iup.button{size="20x", title="R"};
};

GUI.im501_rw.frame = iup.frame
{
    iup.vbox
    {
        iup.hbox
        {
            iup.hbox{GUI.im501_rw.label.addr_s1,GUI.im501_rw.text.iram_addr;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_len1,GUI.im501_rw.text.iram_len;gap="0",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_s1,GUI.im501_rw.text.iram_data;gap="0",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.button.iram_r,GUI.im501_rw.button.iram_w;gap="10",margin="0",alignment = "ACENTER"};
            gap="10",margin="25",alignment = "ACENTER"
        },
        iup.hbox
        {
            iup.hbox{GUI.im501_rw.label.addr_s2,GUI.im501_rw.text.dram_addr;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_len2,GUI.im501_rw.text.dram_len;gap="0",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_s2,GUI.im501_rw.text.dram_data;gap="0",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.button.dram_r,GUI.im501_rw.button.dram_w;gap="10",margin="0",alignment = "ACENTER"};
            gap="10",margin="25",alignment = "ACENTER"
        },
        --
        iup.hbox
        {
            iup.hbox{GUI.im501_rw.label.addr_s5,GUI.im501_rw.text.dsp_cmd;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_s5,GUI.im501_rw.text.cmd_attr;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.button.dsp_cmd_r,GUI.im501_rw.button.dsp_cmd_w;gap="10",margin="0",alignment = "ACENTER"};
            gap="32",margin="25",alignment = "ACENTER"
        },
        iup.hbox
        {
            iup.hbox{GUI.im501_rw.label.addr_s3,GUI.im501_rw.list.spi_reg_addr;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_s3,GUI.im501_rw.text.spi_reg_data;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.button.spi_reg_r,GUI.im501_rw.button.spi_reg_w;gap="10",margin="0",alignment = "ACENTER"};
            gap="32",margin="25",alignment = "ACENTER"
        },
        iup.hbox
        {
            iup.hbox{GUI.im501_rw.label.addr_s4,GUI.im501_rw.list.i2c_reg_addr;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.label.data_s4,GUI.im501_rw.text.i2c_reg_data;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.button.i2c_reg_r,GUI.im501_rw.button.i2c_reg_w;gap="10",margin="0",alignment = "ACENTER"};
            gap="32",margin="25",alignment = "ACENTER"
        };
        iup.hbox
        {
            iup.hbox{GUI.im501_rw.label.dsp2host,GUI.im501_rw.text.dsp2host;gap="10",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.im501_rw.button.dsp2host_r;gap="10",margin="0",alignment = "ACENTER"};
            gap="32",margin="25",alignment = "ACENTER"
        };
        gap="5",margin="10"
    },
    title = "iM501 Read and Write", 
    size="400x",margin="0x10",alignment = "ACENTER",
}


local function write_data_to_file(addr, dat, type, append)
    local fileName = type == "iram" and "IRAM_Dump.txt" or "DRAM_Dump.txt"
    local align = type == "iram" and 4 or 2
    append = append or false
    if not append then os.execute("c: > "..fileName) end
    local file = io.open(fileName,"a+")
    local str = ""

    for i = 1, #dat, align do
        local lineData = 0
        local lineData1 = 0
        for j=0, align-1 do
            lineData = lineData + dat[i+align-1-j] * (256^j)
            lineData1 = lineData1 + dat[i+j] * (256^j)
        end
        str = str..string.format("0x%08x", addr).."  "..string.format("0x%0"..(align*2).."x", lineData).."      "..string.format("0x%0"..(align*2).."x", lineData1).."\n"
        addr = addr + align
    end

    file:write(str.."\n")
    file:close()
end

--function
function GUI.im501_rw.button.iram_r:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    local addr = remove_0x(GUI.im501_rw.text.iram_addr.value)

    if addr == false then return end
    addr = tonumber("0x"..addr)
    local len = remove_0x(GUI.im501_rw.text.iram_len.value) or 4
    len = tonumber("0x"..len)
    export_gui_default("iram_addr", string.format("0x%08x", addr))
    export_gui_default("iram_len", string.format("0x%x", len))
    print(string.format("0x%08x", addr), string.format("0x%08x", len))
    if len <= 4 then
        local state, value = iM501.read_iram(addr, DL_PATH)
        if state then
            GUI.im501_rw.text.iram_data.value = string.format("0x%08x", value)
            export_gui_default("iram_data", string.format("0x%08x", value))
        else
            message("ERROR", value)
        end
    elseif len < iM501.EMB_DATA_SIZE then
        local dat = iM501.read_iram_burst(addr, len, DL_PATH)
        if dat then
            write_data_to_file(addr, dat, "iram")
        end
    else
        local loop = math.floor(len / iM501.EMB_DATA_SIZE)
        local rest = len % iM501.EMB_DATA_SIZE
        for i = 1, loop do
            local dat = iM501.read_iram_burst(addr, iM501.EMB_DATA_SIZE, DL_PATH)
            if dat then
                write_data_to_file(addr, dat, "iram", true)
            end
            addr = addr + iM501.EMB_DATA_SIZE
        end
        if rest<=4 then
            local state, value = iM501.read_iram(addr, DL_PATH)
            if state then
                write_data_to_file(addr, value, "iram", true)
            end
        else
            local dat = iM501.read_iram_burst(addr, rest, DL_PATH)
            if dat then
                write_data_to_file(addr, dat, "iram", true)
            end
        end
    end
end

function GUI.im501_rw.button.dram_r:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end

    local addr = remove_0x(GUI.im501_rw.text.dram_addr.value)

    if addr == false then return end
    addr = tonumber("0x"..addr)
    local len = remove_0x(GUI.im501_rw.text.dram_len.value) or 2
    len = tonumber("0x"..len)
    export_gui_default("dram_addr", string.format("0x%08x", addr))
    export_gui_default("dram_len", string.format("0x%x", len))
    print(string.format("0x%08x", addr), string.format("0x%08x", len))
    if len == 2 or len == 4 then
        local state, value = iM501.read_dram(addr, len, DL_PATH)
        if state then
            GUI.im501_rw.text.dram_data.value = string.format("0x%04x", value)
            export_gui_default("dram_data", string.format("0x%08x", value))
        else
            message("ERROR", value)
        end
    elseif len < iM501.EMB_DATA_SIZE then
        local dat = iM501.read_dram_burst(addr, len, DL_PATH)
        if dat then
            write_data_to_file(addr, dat, "dram")
        end
    else
        local loop = math.floor(len / iM501.EMB_DATA_SIZE)
        local rest = len % iM501.EMB_DATA_SIZE
        for i = 1, loop do
            local dat = iM501.read_dram_burst(addr, iM501.EMB_DATA_SIZE, DL_PATH)
            if dat then
                write_data_to_file(addr, dat, "dram", true)
            end
            addr = addr + iM501.EMB_DATA_SIZE
        end

        if rest<=4 then
            local state, value = iM501.read_dram(addr, DL_PATH)
            if state then
                write_data_to_file(addr, value, "dram", true)
            end
        else
            local dat = iM501.read_dram_burst(addr, rest, DL_PATH)
            if dat then
                write_data_to_file(addr, dat, "dram", true)
            end
        end
    end
end

function GUI.im501_rw.button.dram_w:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    local addr = remove_0x(GUI.im501_rw.text.dram_addr.value)
    if addr == false then return end
    local data = remove_0x(GUI.im501_rw.text.dram_data.value)
    if data == false then return end
    
    local state = iM501.write_dram(tonumber("0x"..addr), tonumber("0x"..data), DL_PATH)

    export_gui_default("dram_addr", "0x"..addr)
    export_gui_default("dram_data", "0x"..data)
    if state == false then
        message("ERROR", "Write DRAM Error")
    end
end

function GUI.im501_rw.button.dsp_cmd_w:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    local addr = remove_0x(GUI.im501_rw.text.dsp_cmd.value)
    if addr == false then return end
    local data = remove_0x(GUI.im501_rw.text.cmd_attr.value)
    if data == false then return end
  
    local state = iM501.write_message(tonumber("0x"..addr), tonumber("0x"..data), DL_PATH)
    export_gui_default("dsp_cmd", "0x"..addr)
    export_gui_default("cmd_addr", "0x"..data)
    if state == false then
        message("ERROR", "Write DRAM Error")
    end
end

function GUI.im501_rw.button.spi_reg_r:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr = remove_0x(spi_reg_addr.value)
    local addr = remove_0x(GUI.im501_rw.list.spi_reg_addr[GUI.im501_rw.list.spi_reg_addr.value])
    if addr == false then return end
    local state, value = iM501.read_reg(tonumber("0x"..addr), 1, 2)
    if state then
        GUI.im501_rw.text.spi_reg_data.value = string.format("0x%02x", value)
    else
        message("ERROR", value)
    end
end

function GUI.im501_rw.button.spi_reg_w:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr, data = remove_0x(spi_reg_addr.value), remove_0x(spi_reg_data.value)
    local addr = remove_0x(GUI.im501_rw.list.spi_reg_addr[GUI.im501_rw.list.spi_reg_addr.value])
    if addr == false then return end
    local data = remove_0x(GUI.im501_rw.text.spi_reg_data.value)
    if data == false then return end
  
    local state = iM501.write_reg(tonumber("0x"..addr), tonumber("0x"..data), 1, 2)
    if state == false then
        message("ERROR", "Write SPI Register Error")
    end

    ----this case is user run dsp by rwitten register
    if DL_PATH == 2 and DSP_RUN == 0 and PDMCLK_ONOFF == 1 and tonumber("0x"..addr) == 0 and tonumber("0x"..data) == 4 then
--[[
        normal_mode.active = "yes"
        PSM.active = "yes"
        --
        normal_mode.fgcolor = GUI.color.green
        PSM.fgcolor = GUI.color.black
        avd_on.fgcolor = GUI.color.green
        avd_off.fgcolor = GUI.color.black
        xsd_on.fgcolor = GUI.color.green
        xsd_off.fgcolor = GUI.color.black
        sd_on.fgcolor = GUI.color.green
        sd_off.fgcolor = GUI.color.black
        all_on.fgcolor = GUI.color.black
        RUN.fgcolor = GUI.color.green
        voice_buffer_read.fgcolor = GUI.color.black
        DSP_RUN = 1]]
    end
end

function GUI.im501_rw.button.i2c_reg_r:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr = remove_0x(i2c_reg_addr.value)
    local addr = remove_0x(GUI.im501_rw.list.i2c_reg_addr[GUI.im501_rw.list.i2c_reg_addr.value])
    if addr == false then return end
    local state, value = iM501.read_reg(tonumber("0x"..addr), 1, 1)
    if state then
        GUI.im501_rw.text.i2c_reg_data.value = string.format("0x%02x", value)
    else
        message("ERROR", value)
    end
end

function GUI.im501_rw.button.i2c_reg_w:action()
    if PDMCLK_ONOFF == 0 then
        --message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr, data = remove_0x(i2c_reg_addr.value), remove_0x(i2c_reg_data.value)
    local addr = remove_0x(GUI.im501_rw.list.i2c_reg_addr[GUI.im501_rw.list.i2c_reg_addr.value]) 
    if addr == false then return end
    local data = remove_0x(GUI.im501_rw.text.i2c_reg_data.value)
    if data == false then return end
  
    local state = iM501.write_reg(tonumber("0x"..addr), tonumber("0x"..data), 1, 1)
    if state == false then
        message("ERROR", "Write SPI Register Error")
    end

    ----this case is user run dsp by rwitten register
    if DL_PATH == 1 and DSP_RUN == 0 and PDMCLK_ONOFF == 1 and tonumber("0x"..addr) == 0xf and tonumber("0x"..data) == 4 then
--[[
        normal_mode.active = "yes"
        PSM.active = "yes"
        --
        normal_mode.fgcolor = GUI.color.green
        PSM.fgcolor = GUI.color.black
        avd_on.fgcolor = GUI.color.green
        avd_off.fgcolor = GUI.color.black
        xsd_on.fgcolor = GUI.color.green
        xsd_off.fgcolor = GUI.color.black
        sd_on.fgcolor = GUI.color.green
        sd_off.fgcolor = GUI.color.black
        all_on.fgcolor = GUI.color.black
        RUN.fgcolor = GUI.color.green
        voice_buffer_read.fgcolor = GUI.color.black
        DSP_RUN = 1]]
    end
end

function GUI.im501_rw.button.dsp2host_r:action()
    local addr = 0x0FFFBFF8
    local len = 8
    local state, value = iM501.read_dram(0x0FFFBFF8, 4, DL_PATH)
    local state1, value1 = iM501.read_dram(0x0FFFBFFC, 4, DL_PATH)
    if state and state1 then
        GUI.im501_rw.text.dsp2host.value = string.format("0x%08x", value + value1*(256^4))
    else
        GUI.im501_rw.text.dsp2host.value = "Read Failed"
    end
end

GUI.im501_rw.active = function(active)
    if active == "yes" or active == "YES" then
        GUI.im501_rw.text.iram_addr.active = "yes"
        GUI.im501_rw.text.iram_data.active = "yes"
        GUI.im501_rw.text.dram_addr.active = "yes"
        GUI.im501_rw.text.dram_data.active = "yes"
        GUI.im501_rw.text.dsp_cmd.active = "yes"
        GUI.im501_rw.text.cmd_attr.active = "yes"
        GUI.im501_rw.list.spi_reg_addr.active = "yes"
        GUI.im501_rw.text.spi_reg_data.active = "yes"
        GUI.im501_rw.list.i2c_reg_addr.active = "yes"
        GUI.im501_rw.text.i2c_reg_data.active = "yes"
        GUI.im501_rw.button.iram_r.active = "yes"
        GUI.im501_rw.button.dram_r.active = "yes" 
        GUI.im501_rw.button.dram_w.active = "yes"
        GUI.im501_rw.button.dsp_cmd_w.active = "yes"
        GUI.im501_rw.button.spi_reg_r.active = "yes" 
        GUI.im501_rw.button.spi_reg_w.active = "yes"
        GUI.im501_rw.button.i2c_reg_r.active = "yes" 
        GUI.im501_rw.button.i2c_reg_w.active = "yes"
        GUI.im501_rw.label.addr_s1.active = "yes"
        GUI.im501_rw.label.data_s1.active = "yes"
        GUI.im501_rw.label.addr_s2.active = "yes"
        GUI.im501_rw.label.data_s2.active = "yes"
        GUI.im501_rw.label.addr_s3.active = "yes"
        GUI.im501_rw.label.data_s3.active = "yes"
        GUI.im501_rw.label.addr_s4.active = "yes"
        GUI.im501_rw.label.data_s4.active = "yes"
        GUI.im501_rw.label.addr_s5.active = "yes"
        GUI.im501_rw.label.data_s5.active = "yes"
    elseif active == "no" or active == "NO" then
        GUI.im501_rw.text.iram_addr.active = "no"
        GUI.im501_rw.text.iram_data.active = "no"
        GUI.im501_rw.text.dram_addr.active = "no"
        GUI.im501_rw.text.dram_data.active = "no"
        GUI.im501_rw.text.dsp_cmd.active = "no"
        GUI.im501_rw.text.cmd_attr.active = "no"
        GUI.im501_rw.list.spi_reg_addr.active = "no"
        GUI.im501_rw.text.spi_reg_data.active = "no"
        GUI.im501_rw.list.i2c_reg_addr.active = "no"
        GUI.im501_rw.text.i2c_reg_data.active = "no"
        GUI.im501_rw.button.iram_r.active = "no"
        GUI.im501_rw.button.iram_w.active = "no"
        GUI.im501_rw.button.dram_r.active = "no" 
        GUI.im501_rw.button.dram_w.active = "no"
        GUI.im501_rw.button.dsp_cmd_w.active = "no"
        GUI.im501_rw.button.spi_reg_r.active = "no" 
        GUI.im501_rw.button.spi_reg_w.active = "no"
        GUI.im501_rw.button.i2c_reg_r.active = "no" 
        GUI.im501_rw.button.i2c_reg_w.active = "no"
        GUI.im501_rw.label.addr_s1.active = "no"
        GUI.im501_rw.label.data_s1.active = "no"
        GUI.im501_rw.label.addr_s2.active = "no"
        GUI.im501_rw.label.data_s2.active = "no"
        GUI.im501_rw.label.addr_s3.active = "no"
        GUI.im501_rw.label.data_s3.active = "no"
        GUI.im501_rw.label.addr_s4.active = "no"
        GUI.im501_rw.label.data_s4.active = "no"
        GUI.im501_rw.label.addr_s5.active = "no"
        GUI.im501_rw.label.data_s5.active = "no"
    end
end

GUI.im501_rw.active("NO")
