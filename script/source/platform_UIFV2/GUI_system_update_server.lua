-- pay attention to line 298 line 490 
printc("Define System update and sync server.")
GUI.system_server = {}
GUI.system_server.constant = 
{
    setting_path = CONFIG_PATH..DSP_NAME.."\\",
}
GUI.system_server.button =
{
    update_ab_setting = iup.button{size="90x", title="Update UIF Path",};
    update_audio_path = iup.button{size="90x", title="Update Aduio Bus",};
    update_com_path = iup.button{size="90x", title="Update Command Bus",};
    update = iup.button{size="60x", title="Update",};
    default = iup.button{size="60x", title="Default",};
    version = iup.button{size="60x", title="Version",};
    open = iup.button{size="60x", title="Open",};
    save = iup.button{size="60x", title="Save",};
}

GUI.system_server.toggle =
{
    auto_update = iup.toggle{title="Auto update after mode switch", value="ON";};
}

GUI.system_server.frame = iup.frame
{
  --  iup.vbox
   -- {
    --[[
        iup.hbox  
        {
            GUI.system_server.button.update_ab_setting;
            GUI.system_server.button.update_audio_path;
            GUI.system_server.button.update_com_path;
            gap="79",margin="0",alignment = "ACENTER",
        };
        ]]
        iup.hbox 
        {
            --GUI.system_server.toggle.auto_update;
            GUI.system_server.button.open;
            GUI.system_server.button.save;
            GUI.system_server.button.update;
            --GUI.system_server.button.default;
            GUI.system_server.button.version;
            gap="54",margin="26x10",alignment = "ACENTER",
        };
    --    gap="10",margin="10x10",alignment = "ALEFT"
   -- };
    title = "UIF Setting Control", 
    size="400x",margin="0x10",alignment = "ACENTER",
}

GUI.system_server.button.version.action = function()
    local state, version = UIF.get_board_ver()
    if state == false then
        message("ERROR", version)
    else
        message("System Version", "UIF Version    : "..version.."\n".."Script Version: ["..Version.."]")
    end
end

GUI.system_server.button.default.action = function()
    printc("Use default setting.")
    GUI.kernel = load_jsonfile1(TEMP_JSON)
    update_system_setting(GUI.system_server.toggle.auto_update.value)
end

GUI.system_server.button.update.action = function()
    GUI.system_server.button.update_com_path.action()
    --GUI.system_server.button.update_ab_setting.action()
    GUI.system_server.button.update_audio_path.action()
    GUI.system_server.button.update_ab_setting.action()
    update_audio_setting()
end

GUI.system_server.button.update_ab_setting.action = function()
    dofile(SCRIPT_PATH..sub_dirctory.."GUI_config_ab.lua")
    if WAVEFORM_ENABLE == 0 then end
    if PLAY_STATUS == 1 then start_play_record(0) end
end

function update_audio_global_value()
    local bus_type = GUI.audio_path.constant.pdm
    local sample_rate = 16000--GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0]+0
    local bit_len = 16--GUI.audio_path.list.bit_len[GUI.audio_path.list.bit_len.value+0]+0
    local one_cycle_dely = GUI.audio_path.constant.delay_en
    --local data_latch = GUI.audio_path.constant.rising
    local bclk_polarity = GUI.audio_path.constant.rising
    local fram_start = 4
    local m_s = GUI.audio_path.constant.master
    local bit_order = GUI.audio_path.constant.msb
    local pdm_clk_sel = GUI.audio_path.list.pdm_clk.value+0
    local slot_num = 8
    --print(GUI.audio_path.toggle.pdm.value)
    if GUI.audio_path.toggle.pdm.value == "OFF" then
        if GUI.audio_path.toggle.i2s.value == "ON" then bus_type = GUI.audio_path.constant.i2s
        elseif GUI.audio_path.toggle.pcm.value == "ON" then bus_type = GUI.audio_path.constant.pcm
        elseif GUI.audio_path.toggle.tdm.value == "ON" then bus_type = GUI.audio_path.constant.tdm
        end

        --sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0]+0
        bit_len = GUI.audio_path.list.bit_len[GUI.audio_path.list.bit_len.value+0]+0
        one_cycle_dely = GUI.audio_path.toggle.delay.value == "ON" and GUI.audio_path.constant.delay_en or GUI.audio_path.constant.delay_dis
        --data_latch = GUI.audio_path.toggle.latch_edge_rising.value == "ON" and GUI.audio_path.constant.rising or GUI.audio_path.constant.falling
        bclk_polarity = GUI.audio_path.toggle.bclk_polarity_normal.value == "ON" and GUI.audio_path.constant.rising or GUI.audio_path.constant.falling
        --fram_start = GUI.audio_path.toggle.fram_latch_rising.value == "ON" and 5 or 4
        m_s = GUI.audio_path.toggle.master.value == "ON" and GUI.audio_path.constant.master or GUI.audio_path.constant.slave
        --bit_order = GUI.audio_path.toggle.msb.value == "ON" and GUI.audio_path.constant.msb or GUI.audio_path.constant.lsb
        slot_num = GUI.audio_path.list.slot_num.value + 0
    end
        sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0]+0

---- For SPI play&recording, audio bus need to setting a fixed format.
	if (GUI.ab_setting.toggle.spi_out.value == "ON" and GUI.ab_setting.list.spi_out.len+0 > 0) or GUI.ab_setting.toggle.spi_in.value == "ON" then
	    sample_rate = GUI.ab_setting.list.spi_in.sample_rate
        bit_len = 16
        one_cycle_dely = GUI.audio_path.constant.delay_en
        bclk_polarity = GUI.audio_path.constant.rising
        m_s = GUI.audio_path.constant.master
        slot_num = 8
	end
----

    return bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel, slot_num
end

GUI.system_server.button.update_audio_path.action = function()
    local bus_type, sample_rate, bit_len, m_s, one_cycle_dely, bclk_polarity, fram_start, bit_order, pdm_clk_sel, slot_num = update_audio_global_value()
--    if audio_bus ~= AUDIO_BUS or sample_rate ~= SAMPLE_RATE or bit_len ~= SAMPLE_BIT_LEN or m_s ~= M_S
--      or one_cycle_dely ~= ONE_CYCLE_DELAY or data_latch ~= BCLK_POLARITY or bit_order ~= BIT_ORDER or pdm_clk_sel ~= PDM_ADC_CLK_SEL then
        --AUDIO_BUS = audio_bus
        AUDIO_BUS_TYPE = bus_type
        SAMPLE_RATE = sample_rate
        SAMPLE_BIT_LEN = bit_len
        SR_LEN = "S"..SAMPLE_BIT_LEN.."_LE"
        M_S = m_s
        ONE_CYCLE_DELAY = one_cycle_dely
        BCLK_POLARITY = bclk_polarity
        SLOT_NUM = slot_num
        --DATE_LATCH = data_latch
        --FRAM_START = fram_start
        --BIT_ORDER = bit_order
        PDM_ADC_CLK_SEL = pdm_clk_sel
--    end
    --UIF.init()
    if AUDIO_BUS_TYPE == 1 then UIF.set_interface(UIF.type.PDMCLK_SEL, 0, PDM_ADC_CLK_SEL+PDM_DAC_CLK_SEL*16) end
end

GUI.system_server.button.update_com_path.action = function()
    if GUI.com_path.text.spi_speed.value+0 > 40 then
        message("ERROR", "Host supports maximum SPI speed is 40MHz")
        return
    end
    if GUI.com_path.text.i2c_speed.value+0 > 400 then
        message("ERROR", "Host supports maximum I2C speed is 400KHz")
        return
    end

    printc("Update new setting.")

    if SPI_MODE ~= GUI.com_path.list.spi_mode.value-1 or DL_PATH ~= download_path then
        SPI_MODE = GUI.com_path.list.spi_mode.value-1

        DL_PATH = download_path
        
        if DSP_NAME == "iM501" then
            
            if PDMCLK_ONOFF == 0 then message("Warning", "DSP will back to normal mode because this action needed.") end
            GUI.audio_path.toggle.pdm_clk.value = "ON"
            UIF.set_vec_config(3,DL_PATH,DELAY_POWER_DOWN,51,iM501.GPIO.IRQ,0, PDMCLK_TURNOFF_EN, DL_PATH)
            PDMCLK_ONOFF = 1
            iM501.switch_I2C_SPI(DL_PATH)
        end
    end
    print(GUI.com_path.text.spi_speed.value, GUI.com_path.text.i2c_speed.value, I2C_SPEED, SPI_SPEED)

    if GUI.com_path.text.spi_speed.value+0 > 40 then
        message("ERROR", "Host supports maximum SPI speed is 40MHz")
        GUI.com_path.text.spi_speed.value = 40
    end

    if GUI.com_path.text.i2c_speed.value+0 > 400 then
        message("ERROR", "Host supports maximum I2C speed is 400KHz")
        GUI.com_path.text.i2c_speed.value = 400
    end
    SPI_SPEED = GUI.com_path.text.spi_speed.value*1000 
    I2C_SPEED = GUI.com_path.text.i2c_speed.value+0
    
--[[
    if I2C_SPEED ~= GUI.com_path.text.i2c_speed.value+0 then
        local temp_speed = I2C_SPEED
        I2C_SPEED = GUI.com_path.text.i2c_speed.value+0
        if (SPI_SPEED/1000) * (I2C_SPEED/temp_speed) > 40 then
            message("ERROR", "Host supports maximum SPI speed is 40MHz")
            I2C_SPEED = temp_speed
            GUI.com_path.text.i2c_speed.value = I2C_SPEED
            return
        end
        --UIF.set_interface(UIF.type.I2C, I2C_SPEED)
        GUI.com_path.text.spi_speed.value = (SPI_SPEED/1000) * (I2C_SPEED/temp_speed)
        SPI_SPEED = GUI.com_path.text.spi_speed.value*1000  
        --UIF.set_interface(UIF.type.SPI, SPI_SPEED, SPI_MODE)
    elseif SPI_SPEED ~= GUI.com_path.text.spi_speed.value*1000 then
        local temp_speed = SPI_SPEED
        SPI_SPEED = GUI.com_path.text.spi_speed.value*1000  
        if (I2C_SPEED) * (SPI_SPEED/temp_speed) > 400 then
            message("ERROR", "Host supports maximum I2C speed is 400KHz")
            SPI_SPEED = temp_speed
            GUI.com_path.text.spi_speed.value = SPI_SPEED/1000
            return
        end
        --UIF.set_interface(UIF.type.SPI, SPI_SPEED, SPI_MODE)
        GUI.com_path.text.i2c_speed.value = (I2C_SPEED) * (SPI_SPEED/temp_speed)
        I2C_SPEED = GUI.com_path.text.i2c_speed.value+0
        --UIF.set_interface(UIF.type.I2C, I2C_SPEED)
    end
]]

    UIF.set_interface(UIF.type.I2C, I2C_SPEED)
    UIF.set_interface(UIF.type.SPI, SPI_SPEED, SPI_MODE)

    printc("Code download bus: "..(DL_PATH == 1 and "I2C" or "SPI"))
    if DL_PATH == 1 then
        printc("I2C Speed: "..I2C_SPEED)
    else
        printc("SPI mode: "..SPI_MODE)
        printc("SPI Speed: "..SPI_SPEED)
    end
end

GUI.system_server.button.save.action = function()
    printc("Save UIF Setting.")
    local new_mode = get_filename_from_folder(JSON_FILE)
    --print(new_mode)
    local fd=iup.filedlg{dialogtype="SAVE", title="Save UIF Setting", 
                         nochangedir="YES", directory=GUI.system_server.constant.setting_path,value=new_mode,
                         filter="*.json", filterinfo="*.json", allownew="YES"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "0") or (status == "1") then -- 0 normal; 1,new; -1, cancel
        if string.find(fd.value, ".json") == nil then
            fd.value = fd.value..".json"
        end
        printc(fd.value)
        os.execute("c: > "..fd.value)
        export_jsonfile(fd.value, GUI.kernel)
    end

    fd:destroy()
end

GUI.system_server.button.open.action = function()
    printc("Open UIF Setting.")
    local fd=iup.filedlg{dialogtype="OPEN", title="Open UIF Setting", 
                         nochangedir="YES", directory=GUI.system_server.constant.setting_path,value="",
                         filter="*.json", filterinfo="Json File", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot Import file "..fd.value)
        end
    else
        if string.find(fd.value, "json") == nil then
            message("ERROR", "The file of selected should be wrong, please double check!")
        end
        --GUI.fm1388_mode.text.customer.value = fd.value
        
        local new_mode = get_filename_from_folder(fd.value)
        fd.directory = string.sub(fd.value, 1, -(string.len(new_mode)+1))
        source_path = fd.value
        objective = GUI.system_server.constant.setting_path..new_mode
        if fd.directory ~= GUI.system_server.constant.setting_path then
            --os.execute("copy "..source_path.." "..objective)
            os.execute("copy \""..fd.value.."\" \""..GUI.system_server.constant.setting_path..new_mode.."\"")
        end
        new_mode = string.sub(new_mode, 1, -6)
        printc(fd.directory, new_mode)

        JSON_FILE = GUI.system_server.constant.setting_path..new_mode..".json"
        printc(JSON_FILE)
        os.execute("copy \""..JSON_FILE.."\" "..TEMP_JSON)
        printc("Switch UIF Setting.")
        collectgarbage("collect")
        GUI.kernel = load_jsonfile1(TEMP_JSON)
        update_system_setting("ON", "ON")
    end
    fd:destroy()
end

function update_list_item(list, tab)
   -- printc(list.len, #tab, GUI.ab_setting.constant.ab_in_max)

    if list.type == "ab_out"    --ab out has limit set by slot number
      and #tab > GUI.ab_setting.constant.ab_in_max then
        --message("Warning", "The number of TDM/PDM output out of range, please check.")
        print("The number of TDM/PDM output out of range, please check.")
        for i = 1, #tab - GUI.ab_setting.constant.ab_in_max do
            table.remove(tab)
        end
    end
    list.len = list.len or 0
    for i = 1, list.len+0 do
        list[i] = nil
    end
    for i = 1, #tab do
        list[i] = tab[i]
    end
    list.len = #tab
    print(list.len, list.value_tab) 
    if list.value_tab ~= nil and (#list.value_tab ~= list.len+0) then
    printc("adjust value table length.")
        if #list.value_tab > list.len+0 then
            for i = list.len, #list.value_tab do table.remove(list.value_tab, list.len+1) end
        end
        if #list.value_tab < list.len+0 then
            for i = #list.value_tab, list.len do table.insert(list.value_tab, 0) end
        end
    end
    return list
end

sc_in_dev_list = {}--soundcard_select(2)
sc_out_dev_list = {}--soundcard_select(1)
function update_sc_in_list(list, redo)
    redo = redo or 0
    local temp_list = {}
    for i = 1 , #sc_in_dev_list do
        temp_list[i] = sc_in_dev_list[i]
    end
    sc_in_dev_list = soundcard_select(2)
    if #sc_in_dev_list == #temp_list then
        local temp = 0
        for  i = 1 , #sc_in_dev_list do
            if temp_list[i] ~= sc_in_dev_list[i] then temp = i break end
        end
        if temp == 0 and redo == 0 then return list end
    end
    local temp = list.value+0
    for i = 1 , #temp_list do
        list[i] = nil
    end
    if #sc_in_dev_list == 0 then 
        GUI.ab_setting.toggle.sc_in.value = "OFF"
    else
        for i = 1, #sc_in_dev_list do
            list[i] = sc_in_dev_list[i]
        end
        if #sc_in_dev_list < temp then
           list.value = #sc_in_dev_list
        else
            list.value = temp == 0 and 1 or temp
        end
    end
    list.visible_items=#sc_in_dev_list
    return list
end

function update_sc_out_list(list, redo)
    redo = redo or 0
    local temp_list = {}
    for i = 1 , #sc_out_dev_list do
        temp_list[i] = sc_out_dev_list[i]
    end
    sc_out_dev_list = soundcard_select(1)
    if #sc_out_dev_list == #temp_list then
        local temp = 0
        for  i = 1 , #sc_out_dev_list do
            if temp_list[i] ~= sc_out_dev_list[i] then temp = i break end
        end
        if temp == 0 and redo == 0 then return list end
    end
    local temp = list.value+0
    for i = 1 , #temp_list do
        list[i] = nil
    end
    if #sc_out_dev_list == 0 then 
        GUI.ab_setting.toggle.sc_out.value = "OFF"
        GUI.ab_setting.toggle.sc_out.action()
    else
        for i = 1, #sc_out_dev_list do
            list[i] = sc_out_dev_list[i]
        end
        if #sc_out_dev_list < temp then
            list.value = #sc_out_dev_list
        else
            list.value = temp == 0 and 1 or temp
        end
    end
    list.visible_items=#sc_out_dev_list
    return list
end

local function update_sc_out_value(list_item)
    local position = 1
    for i = 1, #out_node_src do
        if list_item == out_node_src[i] then
            position = i
            break
        end
    end
    return position
end

function sync_io_channel_by_slot()
    --GUI.ab_setting.constant.ab_in_max = GUI.audio_path.list.slot_num.value + 0
--print(GUI.ab_setting.list.ab_out.len,GUI.ab_setting.constant.ab_in_max)
    if GUI.ab_setting.list.ab_out.len+0 > GUI.ab_setting.constant.ab_in_max then
        for i = GUI.ab_setting.constant.ab_in_max + 1, GUI.ab_setting.list.ab_out.len+0 do
            GUI.ab_setting.list.ab_out[i] = nil
        end
    end
    GUI.ab_setting.list.ab_out.len = GUI.ab_setting.constant.ab_in_max

    if GUI.ab_setting.list.ab_in.value > GUI.audio_path.list.slot_num.value then
        GUI.ab_setting.list.ab_in.value = GUI.audio_path.list.slot_num.value
        GUI.ab_setting.list.ab_in.action()
    end
end

message_time = 0
function check_board_function(functional)
    print(UIF.board_version)
    functional = functional or "SPI"
    if UIF.board_version == "N/A" then 
        if message_time == 0 then
            message("Warning", "USB Detached, offline.")
            message_time = 1
        end
        return true
    end
    if functional == "SPI" then
    --[[
        if UIF.board_version ~= "SAB" and (GUI.ab_setting.toggle.spi_in.active == "YES" or GUI.ab_setting.toggle.spi_out.active == "YES") then
            if message_time == 0 then
                message("Warning", "UIF can't support SPI play&recording!")
                message_time = 1
            end
            GUI.ab_setting.toggle.spi_in.value = "OFF"
            GUI.ab_setting.toggle.spi_in.action()
            GUI.ab_setting.toggle.spi_in.active = "NO"
            GUI.ab_setting.toggle.spi_out.value = "OFF"
            GUI.ab_setting.toggle.spi_out.action()
            GUI.ab_setting.toggle.spi_out.active = "NO"
            GUI.ab_setting.button.spi_in.active = "NO"
            GUI.ab_setting.button.spi_out_src_sel.active = "NO"
            return false
        end
        return true
        ]]
        return true
    elseif functional == "AB" then
        if UIF.board_version ~= "UIF" and (GUI.ab_setting.toggle.ab_in.active == "YES" or GUI.ab_setting.toggle.ab_out.active == "YES") then
            if message_time == 0 then
                message("Warning", "SAB can't support I2S/PDM play&recording!")
                message_time = 1
            end
            GUI.ab_setting.toggle.ab_in.value = "OFF"
            GUI.ab_setting.toggle.ab_in.action()
            GUI.ab_setting.toggle.ab_in.active = "NO"
            GUI.ab_setting.label.ab_in.active = "NO"
            GUI.ab_setting.list.ab_in.active = "NO"
            GUI.ab_setting.toggle.gpio_in.active = "NO"
            GUI.ab_setting.button.gpio_in.active = "NO"
            GUI.ab_setting.toggle.ab_out.value = "OFF"
            GUI.ab_setting.toggle.ab_out.action()
            GUI.ab_setting.toggle.ab_out.active = "NO"
            GUI.ab_setting.button.ab_out_src_sel.active = "NO"
            return false
        end
        return true
    end
end
--type: 0xff: check everything;
--      0x10:slot number: ab_in, ab_out, gpio_in; 0x11: ab_in, 0x12: spi_in, 0x13: gpio_in, 0x14: ab_out
--      0x20:sample rate: 0x21: wavefile input_text;
--      
function system_setting_sync(type)
    --print(type)
    printc("\n******* System Setting Sync... *******\n")
    if type == 0xff or type == 0x10 or type == 0x11 then
    --check ab_in
    printc("Check ab_in.")
        if check_board_function("AB")  and GUI.ab_setting.toggle.ab_in.active == "YES" and GUI.ab_setting.toggle.ab_in.value == "ON" then
            if GUI.ab_setting.constant.ab_in_max < GUI.ab_setting.list.ab_in.value+0 then
                --GUI.ab_setting.list.ab_in.value = GUI.ab_setting.constant.ab_in_max
                if type == 0x11 then
                    --message("Warning", "The number of UIF_input and GPIO_record beyond MAX: "..GUI.ab_setting.constant.ab_in_max)
                    print("The number of UIF_input and GPIO_record beyond MAX: "..GUI.ab_setting.constant.ab_in_max)
                end
                --GUI.ab_setting.list.ab_in.action_time = false
                --GUI.ab_setting.list.ab_in.action()
            end
            if GUI.ab_setting.list.ab_in.value+0 > 6 or GUI.ab_setting.list.ab_in.value+0 < 3 then
                GUI.ab_setting.toggle.gpio_in.active = "no"
                GUI.ab_setting.button.gpio_in.active = "no"
                GUI.ab_setting.toggle.gpio_in.value = "OFF"
            else
                GUI.ab_setting.toggle.gpio_in.active = "yes"
                if GUI.ab_setting.toggle.gpio_in.value == "ON" then
                    GUI.ab_setting.button.gpio_in.active = "yes"
                end
            end
        end
	end
	    
    if type == 0xff or type == 0x10 or type == 0x12 then
    --check spi_in
    printc("Check spi_in.")
        if check_board_function("SPI")  and GUI.ab_setting.toggle.spi_in.active == "YES" and GUI.ab_setting.toggle.spi_in.value == "ON" then
            local spi_list = GUI.ab_setting.list.spi_in.value
            local len = GUI.ab_setting.list.spi_in.len + 0
            local select_num = 0
            local num_max = GUI.ab_setting.constant.spi_in_max
            print("spi_list value: "..spi_list)

            for i = 1, len do
                if string.sub(spi_list, i, i) == "+" then
                    if select_num == num_max then
                        if type == 0x12 then message("Warning", "The number of SPI input beyond MAX: "..GUI.ab_setting.constant.spi_in_max) end
                        local str = string.sub(spi_list, 1, i-1)  --clear 
                        for j = i, len do
                            str = str.."-"
                            GUI.ab_setting.list.spi_in.value_tab[j] = 0 --[len-j+1]=0
                        end
                        GUI.ab_setting.list.spi_in.value = str
                        break
                    end
                    select_num = select_num + 1
                end
            end

            --print(spi_list)
            print(unpack(GUI.ab_setting.list.spi_in.value_tab))
        end
        update_debug_toggle_active()
    end
    
    if type == 0xff or type == 0x10 or type == 0x11 or type == 0x13 then
    --check gpio_in
    printc("Check gpio_in.")
        if GUI.ab_setting.toggle.gpio_in.active == "YES" and GUI.ab_setting.toggle.gpio_in.value == "ON" then
            local gpio_list = GUI.ab_setting.list.gpio_in.value
            local len = GUI.ab_setting.list.gpio_in.len + 0
            local select_num = 0
            local num_max = GUI.ab_setting.constant.ab_in_max - GUI.ab_setting.list.ab_in.value
            for i = 1, len do
                if string.sub(gpio_list, i, i) == "+" then
                    if select_num == (num_max < 4 and num_max or 4) then
                        if type == 0x13 then message("Warning", "The number of UIF_input and GPIO_record beyond MAX: "..GUI.ab_setting.constant.ab_in_max) end
                        local str = string.sub(gpio_list, 1, i-1)
                        for j = i, len do
                            str = str.."-"
                            GUI.ab_setting.list.gpio_in.value_tab[j] = 0 --[len-j+1]=0
                        end
                        GUI.ab_setting.list.gpio_in.value = str
                        break
                    end
                    select_num = select_num + 1
                end
            end
        end
    end
    
    if type == 0xff or type == 0x10 or type == 0x14 then
    --check ab_out
    printc("Check ab_out.")
        if GUI.ab_setting.list.ab_out.len+0 > GUI.ab_setting.constant.ab_in_max then
            if type == 0x13 then message("Warning", "The number of UIF's output out of range") end
            for i = GUI.ab_setting.constant.ab_in_max+1, GUI.ab_setting.list.ab_out.len+0 do
                GUI.ab_setting.list.ab_out[i] = nil
            end
            GUI.ab_setting.list.ab_out.len = GUI.ab_setting.constant.ab_in_max
        end
    end
    
    if type == 0xff or type == 0x10 or type == 0x15 then
    --check spi_out
    printc("Check spi_out.")
        if GUI.ab_setting.list.spi_out.len+0 > GUI.ab_setting.constant.spi_out_max then
            if type == 0x15 then message("Warning", "The number of SPI's output out of range") end
            for i = GUI.ab_setting.constant.spi_out_max+1, GUI.ab_setting.list.spi_out.len+0 do
                GUI.ab_setting.list.spi_out[i] = nil
            end
            GUI.ab_setting.list.spi_out.len = GUI.ab_setting.constant.spi_out_max
        end
    end
    
    if type == 0xff or type == 0x10 or type == 0x12 or type == 0x16 then
    --check debug_info
    printc("Check debug_info.")
        if GUI.ab_setting.toggle.debug_info.active == "YES" and GUI.ab_setting.toggle.debug_info.value == "ON" then
            local debug_list1 = GUI.ab_setting.list.debug_info1.value
            local debug_list2 = GUI.ab_setting.list.debug_info2.value
            local len1 = GUI.ab_setting.list.debug_info1.len + 0
            local len2 = GUI.ab_setting.list.debug_info2.len + 0
            local select_num = 0
            local num_max = GUI.ab_setting.constant.debug_info_max
            printc("debug_list1 value: "..debug_list1)

            for i = 1, len1 do
                if string.sub(debug_list1, i, i) == "+" then
                    if select_num == num_max then
                        if type == 0x15 then message("Warning", "The number of Debug info beyond MAX: "..GUI.ab_setting.constant.debug_info_max) end
                        local str = string.sub(debug_list1, 1, i-1)  --clear 
                        for j = i, len1 do
                            str = str.."-"
                            GUI.ab_setting.list.debug_info1.value_tab[len1-j+1]=0
                        end
                        GUI.ab_setting.list.debug_info1.value = str
                        break
                    end
                    select_num = select_num + 1
                end
            end
            --print(spi_list)
            print(unpack(GUI.ab_setting.list.debug_info1.value_tab))
            print("debug_list1 value: "..debug_list1)
            for i = 1, len2 do
                if string.sub(debug_list2, i, i) == "+" then
                    if select_num == num_max then
                        if type == 0x16 then message("Warning", "The number of Debug info beyond MAX: "..GUI.ab_setting.constant.debug_info_max) end
                        local str = string.sub(debug_list2, 1, i-1)  --clear 
                        for j = i, len2 do
                            str = str.."-"
                            GUI.ab_setting.list.debug_info2.value_tab[len2-j+1]=0
                        end
                        GUI.ab_setting.list.debug_info2.value = str
                        break
                    end
                    select_num = select_num + 1
                end
            end

            --print(spi_list)
            print(unpack(GUI.ab_setting.list.debug_info2.value_tab))
        end
        update_debug_toggle_active()
    end
    if type == 0xff or type == 0x20 or type == 0x21 then
    --check wavefile_in
    printc("Check wavefile_in.")
    print(GUI.ab_setting.toggle.file_in.active, GUI.ab_setting.toggle.file_in.value)
        if GUI.ab_setting.toggle.file_in.active == "YES" and GUI.ab_setting.toggle.file_in.value == "ON" then
            local directory, wavefile_tab = separate_multi_file(GUI.ab_setting.text.file_in.value)
            local input_wave_data = {}
            local load_error, wave_sr = 0, 0
            channels_label = {}
            
            for i = 1, #wavefile_tab do
                local wave_file = directory..wavefile_tab[i]
                local wave_data = wave.load(wave_file)
                local channel_num = 0
                if wave_data == nil then
                    message("ERROR", "load wavefile error: "..wave_file..", Use default file.")
                    load_error = 1
                    --break
                else
                  	channel_num = wave.get_property(wave_data, "channel_num")
                    local wave_sr_temp = wave.get_property(wave_data, "sample_rate")
                    --print(wave_sr_temp, GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0])
                    if wave_sr_temp ~= 0+GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] then
                        message("ERROR", "The sample rate of input wavefile is different with system")
                        load_error = 1
                        wave.delete_wavedata(wave_data)
                        --break
                    end
                    wave_sr = wave_sr == 0 and wave_sr_temp or wave_sr
                    if wave_sr ~= wave_sr_temp then
                        message("ERROR","The sample rate of wave array are not same.")
                        load_error = 1
                        wave.delete_wavedata(wave_data)
                        --break
                    end
                end
                if load_error == 1 then
                    wave_data = wave.load(NOAH_PATH.."Vector\\vector_"..GUI.audio_path.constant.sample_rate..".wav") --wave.load(GUI.ab_setting.constant.file_in_default)
                    channel_num = 2
                    table.insert(channels_label, "")
                    table.insert(channels_label, "")
                else
                    --load channel name
                    local label = get_channelname_from_wavefile(wave_file)
                    for j = 1, channel_num do
                        if label[j] ~= nil then
                            local pos = string.find(label[j], "SPI")
                            if pos ~= nil then --"SPI*:"
                                label[j] = string.sub(label[j], string.find(label[j], ":")+1, -1)
                            end
                            table.insert(channels_label, label[j])
                        else
                            table.insert(channels_label, "")
                        end
                    end
        
                    print(unpack(channels_label))
                    --
                end
                
              	local wave_data_array = channel_num == 1 and {wave_data} or wave.n_to_mono(wave_data)
              	for i = 1, channel_num do 
              	    table.insert(input_wave_data, wave_data_array[i])
              	    --wave.delete_wavedata(wave_data_array[i])
              	end
              	wave.delete_wavedata(wave_data)
        	end
    
            local wave_data = #input_wave_data == 1 and input_wave_data[1] or wave.mono_to_n(input_wave_data, 2)
            wave.save("playtmp.wav", wave_data)
            GUI.ab_setting.text.file_in.name = NOAH_PATH.."playtmp.wav"
            if load_error == 1 then
                GUI.ab_setting.text.file_in.value = NOAH_PATH.."Vector\\vector_"..GUI.audio_path.constant.sample_rate..".wav" --GUI.ab_setting.constant.file_in_default
                GUI.ab_setting.text.file_in.name = NOAH_PATH.."Vector\\vector_"..GUI.audio_path.constant.sample_rate..".wav" --GUI.ab_setting.constant.file_in_default
                GUI.ab_setting.text.file_in.channel_num = 2
            else
                GUI.ab_setting.text.file_in.channel_num = wave.get_property(wave_data, "channel_num")
                wave.delete_wavedata(wave_data)
            end

        end
        --print(GUI.audio_path.constant.sample_rate)
        --print(GUI.ab_setting.text.file_in.value)
    end

    if type == 0xff then
        if node_name == "file_out" and sync_waveview_value == "ON" then
            for i = 1, GUI.ab_setting.list.file_out.len+0 do
                GUI.ab_setting.list.file_out[i] = nil
            end
            for i = 1, GUI.ab_setting.list.waveview.len+0 do
                GUI.ab_setting.list.file_out[i] = GUI.ab_setting.list.waveview[i]
            end
            GUI.ab_setting.list.file_out.len = GUI.ab_setting.list.waveview.len
        end
    end
end

function update_ab_setting()
    printc("\n*********** Update AB Setting... ***********\n")
    local node_list = copy_table(GUI.ab_setting.list.node)
    printc("Node number in json file: "..#GUI.kernel.node)
    convert_enable = 0
    for i = 1, #GUI.kernel.node do
        printc(tostring(i)..": "..GUI.kernel.node[i].name..":  "..GUI.kernel.node[i].active)
        node_name = GUI.kernel.node[i].name
        if node_name == "ab_in" then
            GUI.ab_setting.list.ab_in.value = GUI.kernel.node[i].channum
            --GUI.ab_setting.list.ab_in.action_time = false
            --GUI.ab_setting.list.ab_in.action()
        elseif node_name == "gpio_in" then
            if GUI.ab_setting.toggle.ab_in.value == "OFF" then
                GUI.ab_setting.toggle.gpio_in.value = "OFF"
            end
            GUI.ab_setting.list.gpio_in.value = GUI.kernel.node[i].filename
            GUI.ab_setting.list.gpio_in = update_list_value_tab(GUI.ab_setting.list.gpio_in)
        elseif node_name == "spi_in" then
            GUI.ab_setting.list.spi_in.value = GUI.kernel.node[i].filename
            GUI.ab_setting.list.spi_in.sample_rate = GUI.kernel.node[i].sample_rate
            GUI.ab_setting.list.spi_in = update_list_value_tab(GUI.ab_setting.list.spi_in)
        elseif node_name == "debug_info" then 
        --standby
            GUI.ab_setting.list.debug_info1.value = GUI.kernel.node[i].filename1
            GUI.ab_setting.list.debug_info2.value = GUI.kernel.node[i].filename2
            GUI.ab_setting.list.debug_info1 = update_list_value_tab(GUI.ab_setting.list.debug_info1)
            GUI.ab_setting.list.debug_info2 = update_list_value_tab(GUI.ab_setting.list.debug_info2)
            print(GUI.ab_setting.list.debug_info1.value, GUI.ab_setting.list.debug_info2.value)
        elseif node_name == "file_in" then
            local file_in = dir_fill(GUI.kernel.node[i].filename)
            GUI.ab_setting.text.file_in.value = file_in
            GUI.ab_setting.text.file_in.name = file_in
            if GUI.kernel.node[i].active == "ON" then GUI.ab_setting.text.file_in.action(self,13,file_in) end
            file_in =  dir_remove(file_in)
        elseif node_name == "sc_in" then
            local position = 1
            for j = 1, #sc_in_dev_list do
                if GUI.kernel.node[i].filename == sc_in_dev_list[j] then
                    position = j
                    break
                end
            end
            GUI.ab_setting.list.sc_in_dev.value = position
        elseif node_name == "ab_out" then
        elseif node_name == "spi_out" then
        elseif node_name == "file_out" then
            GUI.ab_setting.text.file_out.value = dir_fill(GUI.kernel.node[i].filename)
            sync_waveview_value = GUI.kernel.node[i].samewithwaveview
        elseif node_name == "waveview" then
        elseif node_name == "sc_out" then
            local position = 1
            --print(GUI.kernel.node[i].filename)
            for j = 1, #sc_out_dev_list do
                if GUI.kernel.node[i].filename == sc_out_dev_list[j] then
                    position = j
                    break
                end
            end
            GUI.ab_setting.list.sc_out_dev.value = position
        end
        --update_out_node_src()
        if GUI.kernel.node[i].src_des ~= nil and #GUI.kernel.node[i].src_des > 0 then
            src_des_tab = {}
            for j = 1, #GUI.kernel.node[i].src_des do  --re-order
                for index = 1, #GUI.kernel.node[i].src_des do
                    if j == GUI.kernel.node[i].src_des[index].sId then
                        table.insert(src_des_tab, GUI.kernel.node[i].src_des[index].name)
                        break
                    end
                end
            end
            print(unpack(src_des_tab))
            if node_name == "sc_out" then
                GUI.ab_setting.list.sc_out_l.value = update_sc_out_value(src_des_tab[1])
                GUI.ab_setting.list.sc_out_r.value = update_sc_out_value(src_des_tab[2])
            else
                str = "GUI.ab_setting.list."..node_name.."=update_list_item(GUI.ab_setting.list."..node_name..",src_des_tab)"
                assert(pcall(loadstring(str)))
            end
        end
        
        node_active = GUI.kernel.node[i].active

--print(node_name, GUI.ab_setting.toggle.file_in.value)
        print( assert(pcall(loadstring("GUI.ab_setting.toggle."..node_name..".value=node_active"))))
        assert(pcall(loadstring("GUI.ab_setting.toggle."..node_name..".action()")))
--print(GUI.ab_setting.toggle.file_in.value)
        if i == #GUI.kernel.node then convert_enable = 1 end
    end
    printc("\n*************update end.")
end

function update_audio_bus()
    printc("\n*********** Update Audio Bus... ***********\n")
    GUI.audio_path.toggle.i2s.value = "OFF"
    GUI.audio_path.toggle.tdm.value = "OFF"
    GUI.audio_path.toggle.pcm.value = "OFF"
    GUI.audio_path.toggle.pdm.value = "OFF"

    local position = 0
    for k, v in pairs(GUI.kernel.audio_bus) do
	    if GUI.kernel.audio_bus[k].active == "ON" then
	        bus_name = GUI.kernel.audio_bus[k].name
	        printc(bus_name..": "..GUI.kernel.audio_bus[k].active)
    	    pcall(loadstring("GUI.audio_path.toggle."..bus_name..".value = \"ON\""))
    	    pcall(loadstring("GUI.audio_path.local_frame.detail.title = audio_path_title."..bus_name))
	        if GUI.kernel.audio_bus[k].name ~= "pdm" then
	        --[[
	            for i = 1, GUI.audio_path.list.sample_rate.visible_items+0 do
	                if tonumber(GUI.audio_path.list.sample_rate[i]) == GUI.kernel.audio_bus[k].sample_rate then
	                    GUI.audio_path.list.sample_rate.value = i
  	                    break
	                end
	            end
	            ]]
	            GUI.audio_path.list.slot_num.value = GUI.kernel.audio_bus[k].channum or 8
	            --GUI.ab_setting.constant.ab_in_max = GUI.audio_path.list.slot_num.value + 0
                GUI.audio_path.list.bit_len.value = GUI.kernel.audio_bus[k].format[1].bit_len/16
                GUI.audio_path.toggle.delay.value = GUI.kernel.audio_bus[k].format[1].delay == "enable" and "ON" or "OFF"
                --GUI.audio_path.toggle.latch_edge_rising.value = GUI.kernel.audio_bus[k].format[1].data_latch == "rising" and "ON" or "OFF"
                --GUI.audio_path.toggle.latch_edge_falling.value = GUI.kernel.audio_bus[k].format[1].data_latch == "falling" and "ON" or "OFF"
                GUI.audio_path.toggle.bclk_polarity_normal.value = GUI.kernel.audio_bus[k].format[1].bclk_polarity == "normal" and "ON" or "OFF"
                GUI.audio_path.toggle.bclk_polarity_invert.value = GUI.kernel.audio_bus[k].format[1].bclk_polarity == "invert" and "ON" or "OFF"
                --GUI.audio_path.toggle.fram_latch_rising.value = GUI.kernel.audio_bus[k].format[1].fram_start == "rising" and "ON" or "OFF"
                --GUI.audio_path.toggle.fram_latch_falling.value = GUI.kernel.audio_bus[k].format[1].fram_start == "falling" and "ON" or "OFF"
                GUI.audio_path.toggle.master.value = GUI.kernel.audio_bus[k].m_s == "master" and "ON" or "OFF"
                GUI.audio_path.toggle.slave.value = GUI.kernel.audio_bus[k].m_s == "slave" and "ON" or "OFF"
                --GUI.audio_path.toggle.msb.value = GUI.kernel.audio_bus[k].format[1].bit_order == "msb" and "ON" or "OFF"
                --GUI.audio_path.toggle.lsb.value = GUI.kernel.audio_bus[k].format[1].bit_order == "lsb" and "ON" or "OFF"
--[[
                if GUI.kernel.audio_bus[k].name == "i2s" then
                    GUI.audio_path.list.slot_num.value = 2  --  ????
                end
]]
            else
                --?????????????????

                GUI.audio_path.list.slot_num.value = 8
                --GUI.audio_path.list.sample_rate.value = 2
                GUI.audio_path.list.bit_len.value = 1
                GUI.audio_path.list.pdm_clk.value = (GUI.kernel.audio_bus[k].bclock/1000000)/1.024
                --GUI.kernel.audio_bus[k].channum = 2--GUI.audio_path.list.slot_num.value + 0
            end
            
                print(GUI.audio_path.list.sample_rate.visible_items, GUI.audio_path.list.sample_rate[6], GUI.kernel.audio_bus[k].sample_rate)
                --io.read()
	            for i = 1, GUI.audio_path.list.sample_rate.visible_items+0 do
	                if tonumber(GUI.audio_path.list.sample_rate[i]) == GUI.kernel.audio_bus[k].sample_rate then
	                    GUI.audio_path.list.sample_rate.value = i
  	                    break
	                end
	            end
            
            GUI.ab_setting.constant.ab_in_max = GUI.audio_path.list.slot_num.value + 0
            GUI.audio_path.constant.sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
            position = k
            break
    	end
    end
print(position)
    if position == 0 then 
        GUI.audio_path.toggle.pdm.value = "ON"
        message("ERROR", "Audio bus setting is error, no bus selection, use default, PDM BUS.")
    end

    printc("\n*************update end.")
end

function update_command_bus()
    printc("\n*********** Update Command Bus... ***********\n")
    for k, v in pairs(GUI.kernel.command_bus) do
        if GUI.kernel.command_bus[k].name == "spi" then
            GUI.com_path.toggle.spi.value = GUI.kernel.command_bus[k].active
            GUI.com_path.text.spi_speed.value = GUI.kernel.command_bus[k].bclock/1000000
            GUI.com_path.list.spi_mode.value = GUI.kernel.command_bus[k].format + 1
        elseif GUI.kernel.command_bus[k].name == "i2c" then
            GUI.com_path.toggle.i2c.value = GUI.kernel.command_bus[k].active
            GUI.com_path.text.i2c_speed.value = GUI.kernel.command_bus[k].bclock/1000
        end
    end
    if GUI.com_path.toggle.spi.value == "ON" then
        DL_PATH = 2
        GUI.com_path.toggle.spi.action()
    else
        DL_PATH = 1
        GUI.com_path.toggle.i2c.action()
    end

    printc("\n*************update end.")
end

function update_code_vec()
    printc("\n*********** Update Code and Vec... ***********\n")
    CODE_VEC_PATH = dir_fill(GUI.kernel.code_vec[1].code)
    if DSP_NAME == "FM1388" then
        GUI.fm1388_operation.text.ram_code.value = CODE_VEC_PATH
        GUI.fm1388_operation.text.run_vec.value = dir_fill(GUI.kernel.code_vec[1].vec)
    elseif DSP_NAME == "iM501" then
        GUI.VID.text.ram_code.value = CODE_VEC_PATH
        GUI.VID.text.vec.value = dir_fill(GUI.kernel.code_vec[1].vec)
    elseif DSP_NAME == "iM205" then
        GUI.im205.text.init_vec.value = dir_fill(GUI.kernel.code_vec[1].vec)
    elseif DSP_NAME == "iM401" then
        --GUI.im205.text.init_vec.value = dir_fill(GUI.kernel.code_vec[1].vec)
    end
    
    local str = string.sub(CODE_VEC_PATH, -2)
    local index = 1
    repeat
        index = string.find(str, "\\")
        str = string.sub(str, (index or 0)+1, -1)
    until index == ni
    --print(str)
    SW_VERSION = str
    --print("\n\n*************update end.")
end

function update_system_setting(auto_update, code_update, reset)
    --printc(auto_update, code_update)
    reset = reset or "YES"
    if reset == "YES" then
        UIF.cmd_write(UIFReset)
    end
    auto_update = auto_update or "ON"
    code_update = code_update or "ON"
    update_audio_bus()
    update_ab_setting()
    update_command_bus()
    if code_update == "ON" then
        update_code_vec()
    end

    system_setting_sync(0xff)
    save_ab_setting_active()
    --if GUI.system_server.toggle.auto_update.value == "ON" then
    if auto_update == "ON" then
        printc("Update system setting to UIF...")
        GUI.uif_setting.button.update.action()
    end
end

