function get_design_list(rfdesign, mydesign)
    local design_group = {rfdesign = {}, mydesign = {}} --{name = "", design = {}}
    local rfdesign_group = {}
    for i = 1, #rfdesign do
        local str = string.sub(rfdesign[i], 2, -1)
        rfdesign[i] = str
        local index = 1
        rfdesign_group[i] = {}
        repeat
            index = string.find(str, "\\")
            table.insert(rfdesign_group[i], string.sub(str, 1, (index or 0) - 1))
            str = string.sub(str, (index or 0)+1, -1)
        until index == nil
        print(unpack(rfdesign_group[i]))
    end
    local mydesign_group = {}
    for i = 1, #mydesign do
        local str = string.sub(mydesign[i], 2, -1)
        mydesign[i] = str
        local index = 1
        mydesign_group[i] = {}
        repeat
            index = string.find(str, "\\")
            table.insert(mydesign_group[i], string.sub(str, 1, (index or 0) - 1))
            str = string.sub(str, (index or 0)+1, -1)
        until index == nil
        print(unpack(mydesign_group[i]))
    end
    
    for i = 1, #rfdesign_group do
        if #rfdesign_group[i] == 2 then
            if #design_group.rfdesign < 1 then 
                design_group.rfdesign[1] = 
                {
                    name = rfdesign_group[i][1], 
                    design = {rfdesign_group[i][2]}
                }
            else
                local flag = 1
                for j = 1, #design_group.rfdesign do
                    if rfdesign_group[i][1] == design_group.rfdesign[j].name then
                        flag = 0
                        table.insert(design_group.rfdesign[j].design, rfdesign_group[i][2])
                    end
                end
                if flag == 1 then 
                    table.insert(design_group.rfdesign, {name=rfdesign_group[i][1], design={rfdesign_group[i][2]}})
                end
            end
        end
    end

    local no_group_index = 0
    for i = 1, #mydesign_group do
        if #mydesign_group[i] == 2 then
            if #design_group.mydesign < 1 then 
                design_group.mydesign[1] = 
                {
                    name = mydesign_group[i][1], 
                    design = {mydesign_group[i][2]}
                }
            else
                local flag = 1
                for j = 1, #design_group.mydesign do
                    if mydesign_group[i][1] == design_group.mydesign[j].name then
                        flag = 0
                        table.insert(design_group.mydesign[j].design, mydesign_group[i][2])
                    end
                end
                if flag == 1 then 
                    table.insert(design_group.mydesign, {name=mydesign_group[i][1], design={mydesign_group[i][2]}})
                end
            end
        else
            if #design_group.mydesign < 1 then 
                design_group.mydesign[1] = 
                {
                    name = "No Group", 
                    design = {mydesign_group[i][1]}
                }
                no_group_index = i
            else
                table.insert(design_group.mydesign[no_group_index].design, mydesign_group[i][1])
            end
        end
    end
    --[[
    for i = 1, #design_group.rfdesign do
        print(design_group.rfdesign[i].name)
        print(unpack(design_group.rfdesign[i].design))
    end
    
    for i = 1, #design_group.mydesign do
        print(design_group.mydesign[i].name)
        print(unpack(design_group.mydesign[i].design))
    end
    ]]
    return design_group
end

function add_design()

end

function del_design()

end

function get_select_value_from_list(list)
    local select_value = {}
    local select_index = {}
    local str = list.value
    for i = 1, string.len(str) do
        if string.sub(str, i, i) == "+" then
            local str1 = list[i]
            local index = string.find(str1, ",")
            str1 = string.sub(str1, (index or -1)+2, -1)
            table.insert(select_value, str1)   -- remove index from design list value
            table.insert(select_index, i)
        end
    end
    --print(unpack(select_value))
    --print(unpack(select_index))
    return select_value, select_index
end

function update_group_list(list, group, value)
    value = value or 1
    for i = 1, list.len + 0 do list[i] = nil end
    for i = 1, #group do
        list[i] = group[i].name
    end
    list.len = #group
    list.visible_items = #group
    list.value = value <= #group and value or #group
    list.type = group[list.value+0].type
    list.action_time = false
    return list
end

function update_design_list(list, design, value)
    for i = 1, list.len + 0 do list[i] = nil end
    for i = 1, #design do list[i] = tostring(i)..", "..design[i] end  -- add index for each design
    list.len = #design
    if value == nil then
        value = "+"
    else
        local str = ""
        for i = 1, list.len-value do str = str.."-" end
        for i = list.len-value, list.len do str = str.."+" end
        value = str 
        --print(value)
    end

    list.value = value
    return list
end

function update_normal_list(list, tab, value)
    value = value or 1
    for i = 1, list.len + 0 do list[i] = nil end
    for i = 1, #tab do
        list[i] = tab[i]
    end
    list.len = #tab
    list.visible_items = #tab
    list.value = value <= #tab and value or #tab
    list.action_time = false

    return list
end

function update_design_text(group, design)
    local str = ""
    for i = 1, #design do
        str  = str ..design[i].." ; "
    end
    str = string.sub(str, 1, -4)
    --print(str)

    local note = ""
    if #design == 1 then 
        note = load_note_info((group == "No Group") and design_path or design_path..group.."\\", design[1])
        note = "Note: "..note
    end
    return group.."\\"..str, note
end

function update_button_state(type, value)
    local select_num = 0
    for i = 1, string.len(value) do
       if string.sub(value, i, i) == "+" then select_num = select_num + 1 end
    end

    if select_num == 0 then
        GUI.design_manage.button.export.active = "no"
        GUI.design_manage.button.import.active = "no"
        GUI.design_manage.button.duplicate.active = "no"
        GUI.design_manage.button.delete.active = "no"
        GUI.design_manage.button.rename.active = "no"
        GUI.design_manage.button.open.active = "no"
    elseif select_num == 1 then
        GUI.design_manage.button.export.active = "yes"
        GUI.design_manage.button.duplicate.active = "yes"
        GUI.design_manage.button.open.active = "yes"
        if type == "rfdesign" then
            GUI.design_manage.button.import.active = "no"
            GUI.design_manage.button.delete.active = "no"
            GUI.design_manage.button.rename.active = "no"
        elseif type == "mydesign" then
            GUI.design_manage.button.import.active = "yes"
            GUI.design_manage.button.delete.active = "yes"
            GUI.design_manage.button.rename.active = "yes"
        end
    elseif select_num > 1 then
        GUI.design_manage.button.export.active = "no"
        GUI.design_manage.button.duplicate.active = "yes"
        GUI.design_manage.button.open.active = "no"
        GUI.design_manage.button.rename.active = "no"
        if type == "rfdesign" then
            GUI.design_manage.button.import.active = "no"
            GUI.design_manage.button.delete.active = "no"
        elseif type == "mydesign" then
            GUI.design_manage.button.import.active = "yes"
            GUI.design_manage.button.delete.active = "yes"
        end
    end
end

local function line_to_note(line)
    local len = string.len(line)
    local idx = 0
    
    local pos = string.find(line, ":")
    if pos == nil then return end
    local t = string.sub(line, 1, pos-1)
    local t1 = string.sub(line, pos+1, -1)
    printc(t.."    "..t1)
    return t, t1
end

function load_note_info(path, design)
    local note_file = path.."readme.txt"
    printc("Load note: "..note_file.."   "..design)
    local file = io.open(note_file, "r")
    if file == nil then printc("No note file.") return "" end
    for line in file:lines(file) do
        local design_name, note = line_to_note(line)
        if design_name == nil or note == nil then
            printc("No design note in this line.")
        else
            if design_name == design then file:close() print(note) return note end
        end
    end
    
    file:close()
    printc("No note for this design")
    return ""
end

function add_note_info(path, design, note)
    local note_file = path.."readme.txt"
    printc("Add note: "..note_file)
    local file = io.open(note_file,"a+")
    if file == nil then printc("No note file.") return end
    local str = design..":"..note.."\n" -- may be should to judge the last char if it's "\n"
    printc(str)
    file:write(str)
    file:close()

    return note
end

function delete_note_info(path, design)
    local note_file = path.."readme.txt"
    printc("Delete note: "..note_file)
    local file = io.open(note_file, "r")
    local file_tab = {}
    if file == nil then printc("No note file.") return end
    for line in file:lines(file) do
        local design_name, note = line_to_note(line)
        if design_name == nil then
            printc("No design note in this line.")
        else
            if design_name ~= design then table.insert(file_tab, line) end
        end
    end
    file:close()

    os.execute("c: > "..note_file)
    local file = io.open(note_file,"a+")
    for i = 1, #file_tab do
        file:write(file_tab[i].."\n")
    end
    file:close()
end

function modify_note_info(path, design, note, new_design_name)  
-- Both note and new_design_name may be modified, but one time only one can change.
    new_design_name = new_design_name or design
    local note_file = path.."readme.txt"
    printc("Modify note: "..note_file)
    local file = io.open(note_file, "r")
    local file_tab = {}
    if file == nil then printc("No note file.") return end
    for line in file:lines(file) do
        local design_name, note1 = line_to_note(line)
        if design_name == nil then
            printc("No design note in this line.")
        else
            if design_name == design then
                if new_design_name ~= design then
                    table.insert(file_tab, new_design_name..":"..note1)
                else
                    table.insert(file_tab, new_design_name..":"..note)
                end
            else
                table.insert(file_tab, line)
            end
        end
    end
    file:close()

    os.execute("c: > "..note_file)
    local file = io.open(note_file,"a+")
    for i = 1, #file_tab do
        file:write(file_tab[i].."\n")
    end
    file:close()
end
