--why iup.list action will run twice?
--module define
printc("define VR GUI module.")
GUI.vr = {}
GUI.vr.constant =
{
    ne_path_default = NOAH_PATH.."Vector\\Hi_ZMKM\\";
    ne_path_simulation = NOAH_PATH.."Vector\\mic_bypass\\";
    report_name_default = RECORD_PATH.."VR_FT_FR_report_"..os.date("%m%d%H%M")..".xml";
    noise_sel_default = "+++++++++---";
    empty_thd = -65;
    calibrate_vector = NOAH_PATH.."Vector\\1k_tone.wav";
}
GUI.vr.label = 
{
    cali_sc = iup.label{size="36x20";title="Calibrate  Record"};
    ne_sc = iup.label{size="18x20";title=" NE Play"};
    noise_sc = iup.label{size="22x20";title="Noise Play"};
    ne_path = iup.label{size="35";title=" NE Path"};
    noise_list = iup.label{size="40";title="Noise List"};
    snr = iup.label{size="35";title="SNR"};
    time_unit = iup.label{size="5";title="S"};
    record_time = iup.label{size="50";title="Record Time"};
    cali_step1 = iup.label
    {
       size="";title=
[==[    Calibrate standard microphone use sound level calibrator.
Please put the standard microphone into sound level calibrator,
then turn on calibrator.
]==]
    };
    cali_step2 = iup.label
    {
        size="x20";title=
[==[    Calibrate play device at MRP.
Please put standard microphone at MRP.
]==]
    };
    cali_step3 = iup.label
    {
        size="x20";title=
[==[    Calibrate play device at DUT point.
Please put standard microphone at DUT point.
]==]
    };
    test_cycle = iup.label{size="50";title="Testing..."};
}

GUI.vr.list = 
{
    cali_sc = iup.list
    {
        dropdown="YES", visible_items=#sc_in_dev_list, value = 1, size="95x", action_time = false,
    };
    ne_sc = iup.list
    {
        dropdown="YES", visible_items=#sc_out_dev_list, value = 1, size="95x", action_time = false,
    };
    noise_sc = iup.list
    {
        dropdown="YES", visible_items=#sc_out_dev_list, value = 1, size="95x", action_time = false,
    };
    noise = iup.list
    {
        "NE only","Cafeteria","Fullsize Car 130kmh",
        "Mensa","Outside Traffic Crossroads","Outside Traffic Road",
        "Pub","Train Station","Work_Noise Office Callcener",
        "Car","Music","Car+Music";
        multiple="YES", value = GUI.vr.constant.noise_sel_default,
        len=12,size="120x98",font = "HELVETICA_NORMAL_10", action_time = false,
    };
}
GUI.vr.list.cali_sc = update_sc_in_list(GUI.vr.list.cali_sc, 1)
GUI.vr.list.ne_sc = update_sc_out_list(GUI.vr.list.ne_sc, 1)
GUI.vr.list.noise_sc = update_sc_out_list(GUI.vr.list.noise_sc, 1)
GUI.ab_setting.list.sc_out_dev.value = GUI.vr.list.ne_sc.value
NOISE_PLAY = GUI.vr.list.noise_sc[GUI.vr.list.noise_sc.value]
STD_MIC_RECORD = GUI.vr.list.cali_sc[GUI.vr.list.cali_sc.value]
NE_PLAY = sc_out_dev_list[GUI.ab_setting.list.sc_out_dev.value+0]

NOISE_TABLE1 =    --** --{noise name, SNR}
{
  {"NE_only.wav", 0},
  {"Cafeteria.wav", 15},
  {"Fullsize_Car_130kmh.wav",-20},
  {"Mensa.wav",15},
  {"Outside_Traffic_Crossroads.wav",6},
  {"Outside_Traffic_Road.wav",18},
  {"Pub.wav",21},
  {"Train_Station.wav",10},
  {"Work_Noise_Office_Callcener.wav",18},
  {"Car.wav",0},
  {"Music.wav",0},
  {"Car+Music.wav",0},
}

GUI.vr.text =
{
    test_time = iup.text{value = tostring(RECORD_TIME), size="30x"};
    ne_path = iup.text{value = GUI.vr.constant.ne_path_default, size="120x"};
    noise_db1 = iup.text{value = NOISE_TABLE1[1][2], size="30x10",active="NO"};
    noise_db2 = iup.text{value = NOISE_TABLE1[2][2], size="30x10"};
    noise_db3 = iup.text{value = NOISE_TABLE1[3][2], size="30x10"};
    noise_db4 = iup.text{value = NOISE_TABLE1[4][2], size="30x10"};
    noise_db5 = iup.text{value = NOISE_TABLE1[5][2], size="30x10"};
    noise_db6 = iup.text{value = NOISE_TABLE1[6][2], size="30x10"};
    noise_db7 = iup.text{value = NOISE_TABLE1[7][2], size="30x10"};
    noise_db8 = iup.text{value = NOISE_TABLE1[8][2], size="30x10"};
    noise_db9 = iup.text{value = NOISE_TABLE1[9][2], size="30x10"};
    noise_db10 = iup.text{value = NOISE_TABLE1[10][2], size="30x10"};
    noise_db11 = iup.text{value = NOISE_TABLE1[11][2], size="30x10"};
    noise_db12 = iup.text{value = NOISE_TABLE1[12][2], size="30x10"};
    report_name = iup.text{value = GUI.vr.constant.report_name_default, size="120x"};
}
RECORD_TIME = GUI.vr.text.test_time.value
GUI.vr.button =
{
    calibrate = iup.button{size="50x", title="Calibrate",};
    waveview = iup.button{size="50x", title="Waveview",};
    ne_path = iup.button{size="60x", title="Browse Path",};
    noise_sel = iup.button{size="60x", title="Noise Select",};
    start = iup.button{size="50x", title="Start",};
    stop = iup.button{size="50x", title="Stop", active="OFF"};
    pause = iup.button{size="50x", title="Pause", active="OFF"};
    ok = iup.button{size="50x", title="Ok",};
    cancel = iup.button{size="50x", title="Cancel",};
    report_view = iup.button{size="50x", title="Report",};
    report_name = iup.button{size="50x", title="Browse",};
    cali_step1 = iup.button{size="80x", title="Step 1",};
    cali_step2 = iup.button{size="80x", title="Step 2",};
    cali_step3 = iup.button{size="80x", title="Step 3",};
    cali_ok = iup.button{size="50x", title="Ok",};
    cali_cancel = iup.button{size="50x", title="Cancel",};
}

GUI.vr.toggle = 
{
    record = iup.toggle{title="Enable",value="ON";};
    real_test = iup.toggle{title="Real Test",value="ON";};
    simulation = iup.toggle{title="Simulation",value="OFF";};
}

GUI.vr.radio = 
{
    mode_switch = iup.radio
    {
        iup.hbox{GUI.vr.toggle.real_test,GUI.vr.toggle.simulation};
        value=GUI.vr.toggle.real_test;
    };
}

GUI.vr.local_frame =
{
    sc_sel = iup.frame
    {
        iup.hbox
        {
            iup.hbox{GUI.vr.label.cali_sc,GUI.vr.list.cali_sc;gap="0",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.vr.label.ne_sc,GUI.vr.list.ne_sc;gap="0",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.vr.label.noise_sc,GUI.vr.list.noise_sc;gap="0",margin="0",alignment = "ACENTER"};
            gap="23",margin="10x10",alignment = "ACENTER"
        };
        title = "Sound Card Selection", 
        size="400x",margin="0x10",alignment = "ACENTER",
    };
    vector_set = iup.frame
    {
        iup.hbox
        {
            iup.hbox{GUI.vr.label.ne_path,GUI.vr.text.ne_path,GUI.vr.button.ne_path;gap="15",margin="0",alignment = "ACENTER"};
            iup.hbox{GUI.vr.button.noise_sel;gap="10",margin="0",alignment = "ACENTER"};
            gap="80",margin="30x10",alignment = "ACENTER"
        };
        title = "NE and Noise Wavefile Setting", 
        size="400x",margin="0x10",alignment = "ACENTER",
    };
    test_set = iup.frame
    {
        iup.hbox
        {
            iup.frame
            {
                iup.hbox
                {
                    GUI.vr.toggle.record;
                    GUI.vr.label.record_time,
                    GUI.vr.text.test_time,
                    GUI.vr.label.time_unit;
                    gap="10",margin="5x3",alignment = "ACENTER"
                };
                title = "Record Setting",
            };
            iup.frame
            {
                iup.hbox
                {
                    --GUI.vr.label.report,
                    GUI.vr.text.report_name,
                    GUI.vr.button.report_name;
                    gap="10",margin="5",alignment = "ACENTER"
                };
                title = "Report Setting",
            };
            gap="55",margin="10x10",alignment = "ACENTER"
        };
        title = "Test Setting", 
        size="400x",margin="0x10",alignment = "ACENTER",
    };
    test_process = iup.frame
    {
        iup.vbox
        {
            iup.hbox
            {
                GUI.vr.radio.mode_switch;
                --GUI.vr.button.calibrate;
                --GUI.vr.button.waveview;
                gap="50",margin="20x2",alignment = "ACENTER"
            };
            iup.hbox
            {
                GUI.vr.button.calibrate;
                GUI.vr.button.start;
                --GUI.vr.button.stop;
                --GUI.vr.button.pause;
                GUI.vr.button.waveview;
                GUI.vr.button.report_view;
                gap="60",margin="32",alignment = "ACENTER"
            };
            gap="5",margin="10x5",alignment = "ACENTER"
        };
        title = "VR Test Process", 
        size="400x",margin="0x10",alignment = "ACENTER",
    };
    cali_step1 = iup.frame
    {
        iup.vbox
        {
            GUI.vr.button.cali_step1;
            GUI.vr.label.cali_step1;
            gap="10",margin="10x5",alignment = "ACENTER"
        };
        title = "", 
        size="200x",margin="0x10",alignment = "ACENTER",
    };
    cali_step2 = iup.frame
    {
        iup.vbox
        {
            GUI.vr.button.cali_step2;
            GUI.vr.label.cali_step2;
            gap="10",margin="64x5",alignment = "ACENTER"
        };
        title = "", 
        size="200x",margin="0x10",alignment = "ACENTER",
    };
    cali_step3 = iup.frame
    {
        iup.vbox
        {
            GUI.vr.button.cali_step3;
            GUI.vr.label.cali_step3;
            gap="10",margin="50x5",alignment = "ACENTER"
        };
        title = "", 
        size="200x",margin="0x10",alignment = "ACENTER",
    };
}

GUI.vr.dialog = 
{
    noise_sel = iup.dialog
    {
        iup.vbox
        {
            iup.hbox
            {
                iup.vbox
                {
                    GUI.vr.label.noise_list;
                    GUI.vr.list.noise;
                    margin="0", gap="0"
                };
                iup.vbox
                {
                    GUI.vr.label.snr;
                    GUI.vr.text.noise_db1;
                    GUI.vr.text.noise_db2;
                    GUI.vr.text.noise_db3;
                    GUI.vr.text.noise_db4;
                    GUI.vr.text.noise_db5;
                    GUI.vr.text.noise_db6;
                    GUI.vr.text.noise_db7;
                    GUI.vr.text.noise_db8;
                    GUI.vr.text.noise_db9;
                    GUI.vr.text.noise_db10;
                    GUI.vr.text.noise_db11;
                    GUI.vr.text.noise_db12;
                    margin="0", gap="0"
                };
                margin="10x5", gap="10",alignment = "ACENTER",
            };
            iup.hbox
            {
                GUI.vr.button.ok;
                GUI.vr.button.cancel;
                margin="10x5", gap="10",alignment = "ACENTER",
            };
            margin="10x5", gap="10",alignment = "ACENTER",
        };
        title="Select Noise", margin="10x10", gap="10"--,size="100x20"
    };
    calibrate = iup.dialog
    {
        iup.vbox
        {
            GUI.vr.local_frame.cali_step1;
            GUI.vr.local_frame.cali_step2;
            GUI.vr.local_frame.cali_step3;
            iup.hbox
            {
                GUI.vr.button.cali_ok;
                GUI.vr.button.cali_cancel;
                margin="50x5", gap="50",alignment = "ACENTER",
            };
            margin="10x5", gap="10",alignment = "ACENTER",
        };
        title="Calibration", margin="10x10", gap="10"--,size="100x20"
    };
    test_cycle = iup.dialog
    {
        iup.vbox
        {
            GUI.vr.label.test_cycle;
            GUI.vr.button.stop;
            GUI.vr.button.pause;
            margin="10x5", gap="10",alignment = "ACENTER",
        };
        title="Testing", margin="10x10", gap="10"--,size="100x20"
    };
}
GUI.vr.list.cali_sc.action = function()
    GUI.vr.list.cali_sc = update_sc_in_list(GUI.vr.list.cali_sc)
    STD_MIC_RECORD = GUI.vr.list.cali_sc[GUI.vr.list.cali_sc.value]
    print(STD_MIC_RECORD)
end
GUI.vr.list.ne_sc.action = function()
    GUI.vr.list.ne_sc = update_sc_out_list(GUI.vr.list.ne_sc,1)
    GUI.ab_setting.list.sc_out_dev.value = GUI.vr.list.ne_sc.value
    NE_PLAY = sc_out_dev_list[GUI.ab_setting.list.sc_out_dev.value+0]
    print(GUI.ab_setting.list.sc_out_dev.value)
end
GUI.vr.list.noise_sc.action = function()
    GUI.vr.list.noise_sc = update_sc_out_list(GUI.vr.list.noise_sc,1)
    NOISE_PLAY = GUI.vr.list.noise_sc[GUI.vr.list.noise_sc.value]
    print(NOISE_PLAY)
end

GUI.vr.list.noise.action = function()
    local num_tab = s_2_n(GUI.vr.list.noise.value)
    for i = 2, #num_tab do
        if num_tab[i] == 1 then
            pcall(loadstring("GUI.vr.text.noise_db"..tostring(i)..".active=\"yes\""))
        else
            pcall(loadstring("GUI.vr.text.noise_db"..tostring(i)..".active=\"no\""))
        end
    end
end

GUI.vr.toggle.record.action = function()
    if GUI.vr.toggle.record.value == "ON" then
        GUI.vr.label.record_time.active = "ON"
        GUI.vr.text.test_time.active = "ON"
        GUI.vr.label.time_unit.active = "ON"
    else
        GUI.vr.label.record_time.active = "OFF"
        GUI.vr.text.test_time.active = "OFF"
        GUI.vr.label.time_unit.active = "OFF"
    end
end

GUI.vr.toggle.real_test.action = function()
    if GUI.vr.toggle.real_test.value == "ON" then
        GUI.vr.button.calibrate.active = "ON"
        GUI.vr.text.ne_path.value = GUI.vr.constant.ne_path_default
        realtest_ab_set()
    end 
end
GUI.vr.toggle.simulation.action = function()
    if GUI.vr.toggle.simulation.value == "ON" then
        GUI.vr.button.calibrate.active = "OFF"
        GUI.vr.text.ne_path.value = GUI.vr.constant.ne_path_simulation
        simulation_ab_set()
    end
end
GUI.vr.button.calibrate.action = function()
    calibrate_folder = RECORD_PATH.."calibration"
    os.execute("md "..calibrate_folder)
    calibrate_path = calibrate_folder.."\\"
    GUI.vr.dialog.calibrate:popup()
end
GUI.vr.button.cali_step1.action = function()
    GUI.vr.button.cali_step1.title="running..."
    printc("\nCalibrate standard microphone use sound level calibrator.")
    audio.record(STD_MIC_RECORD, calibrate_path.."STM_calibration.wav")
    wait(3)
    audio.stop_record(STD_MIC_RECORD)
    print("\n******** Standard microphone calibration finished")
    local wave_data = wave.load(calibrate_path.."STM_calibration.wav")
    local power1 = wave.power(get_leftchannel(wave_data))-5
    local power2 = wave.power(get_rightchannel(wave_data))-5
    local Pref = power1 > power2 and power1 or power2 --calibrator output is 94, MRP need 89
    print("STM recording power:"..Pref)
    if Pref < GUI.vr.constant.empty_thd then 
        message("ERROR", "Record nothing, please check and try again.")
        GUI.vr.button.cali_step1.title="Step1"
        return
    end
    print_cali_data(tostring(Pref))
    GUI.vr.button.cali_step1.title="Step1"
end
GUI.vr.button.cali_step2.action = function()
    GUI.vr.button.cali_step2.title="running..."
    print("\nCalibrate play device at MRP.")
    audio.play(NE_PLAY, GUI.vr.constant.calibrate_vector, 0, true)
    audio.record(STD_MIC_RECORD, calibrate_path.."MRP_calibration.wav")
    wait(3)
    audio.stop_record(STD_MIC_RECORD)
    audio.stop_play(NE_PLAY)

    local wave_data = wave.load(calibrate_path.."MRP_calibration.wav")
    local power1 = wave.power(get_leftchannel(wave_data))
    local power2 = wave.power(get_rightchannel(wave_data))
    local Pmrp = power1 > power2 and power1 or power2 
    print("MRP recording power1:"..Pmrp)
    if Pmrp < GUI.vr.constant.empty_thd then 
        message("ERROR", "Record nothing, please check and try again.")
        GUI.vr.button.cali_step2.title="Step2"
        return
    end
    print_cali_data(tostring(Pmrp))
    GUI.vr.button.cali_step2.title="Step2"
end
GUI.vr.button.cali_step3.action = function()
    ----read Pref and Pmrp
    GUI.vr.button.cali_step3.title="running..."
    print("\nCalibrate play device at DUT point.")
    --print("Used the calibration data of last time.")
    local tab, i = {}, 0
    local file = io.open(CALIBRATION_FILE, "r")

    for line in file:lines(file) do
        i = i + 1
        tab[i] = line + 0
    end
    file:close()
    if tab[1] == nil or tab[2] == nil then
        message("ERROR", "You need finish Step1 and Step2 first!")
        GUI.vr.button.cali_step3.title="Step3"
        return
    end
    local Pref = tab[1]
    local Pmrp = tab[2]
    print(Pref,Pmrp)
    local wave_data = wave.load(GUI.vr.constant.calibrate_vector)
    Ps = wave.power(wave_data) + (Pref - Pmrp)
    print("NE power:"..Ps)
    print_cali_data(tostring(Ps))
    
    wave_data = wave.amplify(wave_data, Pref - Pmrp)
    wave.save(NOAH_PATH.."Vector\\temp.wav", wave_data)
    audio.play(NE_PLAY, NOAH_PATH.."Vector\\temp.wav", 0, true)
    audio.record(STD_MIC_RECORD, calibrate_path.."DUT_calibration.wav")
    wait(3)
    audio.stop_record(STD_MIC_RECORD)
    audio.stop_play(NE_PLAY)

    wave_data = wave.load(calibrate_path.."DUT_calibration.wav")
    local power1 = wave.power(get_leftchannel(wave_data))
    local power2 = wave.power(get_rightchannel(wave_data))
    local Pdut = power1 > power2 and power1 or power2 
    print("DUT recording power:"..Pdut)
    if Pdut < GUI.vr.constant.empty_thd then
        message("ERROR", "Record nothing, please check and try again.")
        GUI.vr.button.cali_step3.title="Step3"
        return 
    end
    audio.play(NOISE_PLAY, GUI.vr.constant.calibrate_vector, 0, true)
    audio.record(STD_MIC_RECORD, calibrate_path.."DUT_calibration_noise.wav")
    wait(3)
    audio.stop_record(STD_MIC_RECORD)
    audio.stop_play(NOISE_PLAY)

    wave_data = wave.load(calibrate_path.."DUT_calibration_noise.wav")
    power1 = wave.power(get_leftchannel(wave_data))
    power2 = wave.power(get_rightchannel(wave_data))
    power1 = power1 > power2 and power1 or power2 

    wave_data = wave.load(GUI.vr.constant.calibrate_vector)
    power2= wave.power(wave_data)
    print(power1, power2)
    local Pn = power2 + Pdut - power1
    print("Noise power:"..Pn)
    if Pn < GUI.vr.constant.empty_thd then
        message("ERROR", "Record nothing, please check and try again.")
        GUI.vr.button.cali_step3.title="Step3"
        return
    end
    print_cali_data(tostring(Pn))
    GUI.vr.button.cali_step3.title="Step3"
end

GUI.vr.button.cali_ok.action = function()
    local tab, i = {}, 0
    local file = io.open(CALIBRATION_FILE, "r")
    for line in file:lines(file) do
        i = i + 1
        tab[i] = line + 0
    end
    file:close()
    if tab[1] == nil or tab[2] == nil or tab[3] == nil or tab[4] == nil then
        message("ERROR", "You did not finish calibration.")
        return
    end
    os.execute("copy "..NOAH_PATH..CALIBRATION_FILE.." "..NOAH_PATH..CALIBRATION_BK)
    GUI.vr.dialog.calibrate:hide()
end
GUI.vr.button.cali_cancel.action = function()
    GUI.vr.dialog.calibrate:hide()
end

GUI.vr.button.start.action = function()
  --  if DSP_RUN ~= 1 then message("ERROR", "DSP is not running!") return end
    GUI.vr.dialog.test_cycle:show()
    --GUI.vr.label.test_cycle.title = "Testing..."
    if GUI.vr.toggle.real_test.value == "ON" then
        local tab, i = {}, 0
        local file = io.open(CALIBRATION_BK, "r")
        for line in file:lines(file) do
            i = i + 1
            tab[i] = line + 0
        end
        file:close()
        if tab[3] == nil or tab[4] == nil then
            message("ERROR", "You need calibrate first!")
            return
        end
        Ps = tab[3]
        Pn = tab[4]
        print(Ps,Pn)
    end
    RECORD_TIME = GUI.vr.text.test_time.value
    GUI.vr.label.test_cycle.title = "Testing..."
    for noise_num = 1, #NOISE_TABLE do
        printc("Noise name: "..NOISE_TABLE[noise_num][1])
        str1 = string.sub(NOISE_TABLE[noise_num][1], 1, -5)
        local str = str1.."_"..tostring(NOISE_TABLE[noise_num][2]).."dB"
        pcall(loadstring("NOISE"..tostring(noise_num).."=".."str1"))

        if GUI.vr.toggle.real_test.value == "ON" then
            --SNR calibration
            local wave_data = get_leftchannel(wave.load(NOISE_FILE_PATH..NOISE_TABLE[noise_num][1]))
            if NOISE_TABLE[noise_num][1] ~= "NE_only" or NOISE_TABLE[noise_num][2] ~= 0 then
                local power1 = wave.power(wave_data)
                wave_data = wave.amplify(wave_data, Pn - power1 - NOISE_TABLE[noise_num][2])
            end
            wave.save(NOISE_FILE_PATH.."temp.wav", wave_data)
            wave.delete_wavedata(wave_data)
            --
            if GUI.vr.toggle.record.value == "ON" then
                audio.play(NOISE_PLAY, NOISE_FILE_PATH.."temp.wav", 0, true)
            end
        end
        local output_folder = RECORD_PATH.."Version_"..SW_VERSION.."\\"..str
        local micbypass_folder = NOAH_PATH.."Vector\\mic_bypass\\"..str
        match_string = "*.wav"
        local INPUT_FILE_PATH = GUI.vr.text.ne_path.value
        if GUI.vr.toggle.simulation.value == "ON" then
            output_folder = RECORD_PATH.."Version_"..SW_VERSION.."_simulation\\"..str
            INPUT_FILE_PATH = GUI.vr.text.ne_path.value..str.."\\"
            TEST_CYCLE = 1
        end
        os.execute("md "..output_folder)
        os.execute("md "..micbypass_folder)
        local output_path = output_folder.."\\"
        local micbypass_path = micbypass_folder.."\\"
        FILE_INPUT_TABLE = misc.find_names(match_string, INPUT_FILE_PATH, 0)
    
        if FILE_INPUT_TABLE == nil then 
            message("ERROR","No wavefile in this folder: "..INPUT_FILE_PATH)
            GUI.vr.button.start.title = "Start"
            ----stop play
            return
         end
    
        HOT_WORD = #FILE_INPUT_TABLE
        DATA = {}
        table.insert(DATA, string.sub(NOISE_TABLE[noise_num][1], 1, -5))
        table.insert(DATA, NOISE_TABLE[noise_num][2])
        table.insert(DATA, 89 - NOISE_TABLE[noise_num][2])
    
        --AVD_FT, AVD_FR, xSD_FT, xSD_FR, SD_FT, SD_FR = "","","","","",""
        AVD_FT, AVD_FR, xSD_FT, xSD_FR, SD_FT, SD_FR = {},{},{},{},{},{}
        local vr_data = {} --{avd_ft, avd_fr, xsd_ft, xsd_fr, sd_ft, sd_fr}
        for num = 1, TEST_CYCLE do
            for i = 1, #FILE_INPUT_TABLE do
                GUI.vr.label.test_cycle.title = "Case: "..tostring(i)
                local input_file = string.gsub(FILE_INPUT_TABLE[i], "\\", "")
                --INPUT_FILE_ARRAY = INPUT_FILE_PATH..input_file
                local wave_data = wave.load(INPUT_FILE_PATH..input_file)
                print(INPUT_FILE_PATH..input_file)
                local ts, te = unsilence_len(get_leftchannel(wave_data))
                --print(ts, te)
                wave_data = wave.resample(wave_data, SAMPLE_RATE)
                ts, te = ts+sys_delay, te+sys_delay  --system delay is about 200ms

                GUI.ab_setting.text.file_out.value = output_path..input_file
                Hot_Word = string.sub(input_file, 10, -1)
                GUI.ab_setting.text.file_in.channel_num = 3

                if GUI.vr.toggle.real_test.value == "ON" then
                    local power1 = wave.power(get_leftchannel(wave.cut(wave_data, ts, te)))
                    wave_data = wave.amplify(wave_data, Ps - power1)
                    local time_last = wave.get_property(wave_data , "time_last")
                    local time_add = RECORD_TIME - time_last
                    if time_add > 0 then
                        local new_wave_data = wave.gen_silence(time_add*1000)
                        wave_data = wave.append(wave_data, new_wave_data)
                    else
                        wave_data = wave.cut(wave_data, 0, RECORD_TIME*1000)
                    end
                    GUI.ab_setting.text.file_in.channel_num = 1
                    GUI.ab_setting.text.file_out.value = output_path.."Hot_Word_"..string.sub(input_file, 1, -5).."_cycle_"..tostring(num)..".wav"
                    Hot_Word = input_file
                end
                wave.save(NOAH_PATH.."temp.wav", wave_data)
                GUI.ab_setting.text.file_in.value = NOAH_PATH.."temp.wav"
                local FILE_LOUT_NAME = GUI.ab_setting.text.file_out.value
                pcall(loadstring("Hot_Word"..tostring(i).."=".."Hot_Word")) --string.sub(input_file, 1, -5)))
                dofile(SCRIPT_PATH..sub_dirctory.."GUI_config_ab.lua")
                if GUI.vr.toggle.record.value == "ON" then 
                    AB_config()
                    printc("Record signal is save to file : "..FILE_LOUT_NAME)
                    dowork()
                end
                os.execute("del \""..GUI.ab_setting.text.file_in.value.."\"")
    
                --calculate and judge, then save the data for report.
                --local ts, te, time_last = 1800,3900, 6
                wave_data = wave.load(FILE_LOUT_NAME)
                local wave_data_array = wave.n_to_mono(wave_data)
                
                if num == 1 and GUI.vr.toggle.real_test.value == "ON" then --save test vector
                    local new_wave_data = wave.mono_to_n({wave_data_array[OUTPUT_CH.file_in], wave_data_array[OUTPUT_CH.mic0_bypass], wave_data_array[OUTPUT_CH.mic1_bypass]})
                    wave.save(micbypass_path.."Hot_Word_"..string.sub(input_file, 1, -5)..".wav", new_wave_data)
                    wave.delete_wavedata(new_wave_data)
                end
    
                local threshold = 100  --ms
                local time_last = wave.get_property(wave_data , "time_last")
               -- print(OUTPUT_CH.avd,OUTPUT_CH.xsd,OUTPUT_CH.irq)
                local ts_avd, te_avd = unsilence_len(wave_data_array[OUTPUT_CH.avd])         
                local ts_xsd, te_xsd = unsilence_len(wave_data_array[OUTPUT_CH.xsd])
                local ts_sd, te_sd = unsilence_len(wave_data_array[OUTPUT_CH.irq])
                local fr_ft = 0
                vr_data[i] = vr_data[i] or {}
                for j = 1, 6 do vr_data[i][j] = vr_data[i][j] or 0 end
                ------{AVD FT, AVD FR, xSD FT, xSD FR, SD FT, SD FR}
                --******************** need take care of this part. need to modify.
                if GUI.im501_vr_setting.toggle.nm_avd.value == "ON" or GUI.im501_vr_setting.toggle.psm_avd.value == "ON" then
                    if ts_avd > (time_last*1000-10) or te_avd == 0 then  --no trigger.
                        vr_data[i][2] = vr_data[i][2]+1 --? avd FR
                        if vr_data[i][2] == 1 then table.insert(AVD_FR,Hot_Word) end
                        fr_ft = 1
                    else  --has trigger
                        if ts_avd < ts-threshold or te_avd > te+threshold then  -- happen in wrong position
                            vr_data[i][1] = vr_data[i][1]+1 --  AVD FT
                            if vr_data[i][1] == 1 then table.insert(AVD_FT,Hot_Word) end
                            fr_ft = 1
                            
                            ts_avd,te_avd = unsilence_len(wave.cut(wave_data_array[OUTPUT_CH.avd],ts-threshold,te+threshold))
                            --ts_avd,te_avd = ts_avd+ts-threshold,te_avd+ts-threshold
                            if ts_avd > (te - ts + threshold) or te_avd == 0 then  -- in right position no trigger
                                vr_data[i][2] = vr_data[i][2]+1 --? avd FR
                                if vr_data[i][2] == 1 then table.insert(AVD_FR,Hot_Word) end
                                fr_ft = 1
                            end
                        end
                    end
                end
                if GUI.im501_vr_setting.toggle.nm_xsd.value == "ON" or GUI.im501_vr_setting.toggle.psm_xsd.value == "ON" then
                    if ts_xsd > (time_last*1000-10) or te_xsd == 0 then  --no trigger.
                        vr_data[i][4] = vr_data[i][4]+1 --? xsd FR
                        if vr_data[i][4] == 1 then table.insert(xSD_FR,Hot_Word) end
                        fr_ft = 1
                    else  --has trigger
                        if ts_xsd < ts or te_xsd > (te - 500) then  -- happen in wrong position
                            vr_data[i][3] = vr_data[i][3]+1 --  xsd FT
                            print(xSD_FT,Hot_Word)
                            if vr_data[i][3] == 1 then table.insert(xSD_FT,Hot_Word) end
                            fr_ft = 1
                            
                            ts_xsd,te_xsd = unsilence_len(wave.cut(wave_data_array[OUTPUT_CH.xsd],ts,te-500))
                            if ts_xsd >= (te - ts-500) or te_xsd == 0 then  -- in right position no trigger
                                vr_data[i][4] = vr_data[i][4]+1 --? xsd FR
                                if vr_data[i][4] == 1 then table.insert(xSD_FR,Hot_Word) end
                                fr_ft = 1
                            end
                        end
                    end
                end
                if GUI.im501_vr_setting.toggle.nm_sd.value == "ON" or GUI.im501_vr_setting.toggle.psm_sd.value == "ON" then
                    if ts_sd > (time_last*1000-10) or te_sd == 0 then  --no trigger.
                        vr_data[i][6] = vr_data[i][6]+1 --? sd FR
                        if vr_data[i][6] == 1 then table.insert(SD_FR,Hot_Word) end
                        fr_ft = 1
                    else  --has trigger
                        if te_sd < te-500 or te_sd > te+500 then  -- happen in wrong position
                            vr_data[i][5] = vr_data[i][5]+1 --  SD FT
                            if vr_data[i][5] == 1 then table.insert(SD_FT,Hot_Word) end
                            fr_ft = 1
                            
                            ts_sd,te_sd = unsilence_len(wave.cut(wave_data_array[OUTPUT_CH.irq],te-500,te+500))
                            if ts_sd >= 1000 or te_sd == 0 then  -- in right position no trigger
                                vr_data[i][6] = vr_data[i][6]+1 --? sd FR
                                if vr_data[i][6] == 1 then table.insert(SD_FR,Hot_Word) end
                                fr_ft = 1
                            end
                        end
                    end
                end
            end
        end
        GUI.vr.button.start.title = "Start"
        local vr_data1 = {0,0,0,0,0,0}
    
        for i = 1, #vr_data do
            for j = 1, #vr_data[1] do --calculate all of hot word's FR and FT
                vr_data1[j] = vr_data1[j] + vr_data[i][j]
                -- average each hot word's FR and FT
                vr_data[i][j] = decimal_round(vr_data[i][j]/TEST_CYCLE,2)
            end
        end
        for i = 1, #vr_data1 do  -- average all hot word's FR and FT
            vr_data1[i] = decimal_round(vr_data1[i]/(TEST_CYCLE*#FILE_INPUT_TABLE),2)
        end
    
        table.insert(vr_data, 1, vr_data1)
    
        for i = 1, #vr_data do
            for j = 1, #vr_data[1] do
                table.insert(DATA, vr_data[i][j])--tostring(vr_data[i][j]*100).."%")
            end
        end
        
        pcall(loadstring("DATA"..noise_num.."=".."DATA"))
        pcall(loadstring("AVD_FT"..tostring(noise_num).."=".."AVD_FT"))
        pcall(loadstring("AVD_FR"..tostring(noise_num).."=".."AVD_FR"))
        pcall(loadstring("XSD_FT"..tostring(noise_num).."=".."xSD_FT"))
        pcall(loadstring("XSD_FR"..tostring(noise_num).."=".."xSD_FR"))
        pcall(loadstring("SD_FT"..tostring(noise_num).."=".."SD_FT"))
        pcall(loadstring("SD_FR"..tostring(noise_num).."=".."SD_FR"))
        
        if GUI.vr.toggle.record.value == "ON" and GUI.vr.toggle.real_test.value == "ON" then
            audio.stop_play(NOISE_PLAY)
        end
        os.execute("del \""..NOISE_FILE_PATH.."temp.wav\"")

        report.xml_generate_report(TEMPLATE, GUI.vr.text.report_name.value)
    end
end

GUI.vr.button.stop.action = function()
print("aaaaaaaaaaaaaa")
end
GUI.vr.button.pause.action = function()
print("bbbbbbbbbbbbbb")
end
GUI.vr.button.waveview.action = function()
    local f1 = io.popen(NOAH_PATH.."WaveViewer\\WaveWindow.exe")
end
GUI.vr.button.report_view.action = function()
    local f1 = io.popen(GUI.vr.text.report_name.value)
    if f1 == nil then message("Error","No this file: "..GUI.vr.text.report_name.value) end
end

GUI.vr.button.ne_path.action = function()
    local fd=iup.filedlg{dialogtype="DIR", title="Select Code Directory", 
                         nochangedir="YES", directory=GUI.vr.text.ne_path.value,value="",
                         filter="", filterinfo="File Directory", allownew="NO"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "-1") or (status == "1") then
        if (status == "1") then
             message("ERROR", "Cannot load file "..fd.value)
        end
    else
        GUI.vr.text.ne_path.value = fd.value
        local match_string = "*.wav"
        local ne_file = misc.find_names(match_string, fd.value, 0)
        --print(unpack(bin_file))
        if ne_file == nil or #ne_file == 0 then 
            message("ERROR", "This directory do not include any wavefile.") 
        end
        
        GUI.ab_setting.text.file_in.value = fd.value..ne_file[1]

        --print(GUI.ab_setting.text.file_in.value, str)
        print("Input file: "..GUI.ab_setting.text.file_in.value)
        local wave_data = wave.load(GUI.ab_setting.text.file_in.value)
        if wave_data == nil then
            message("ERROR", "load wavefile error: "..GUI.ab_setting.text.file_in.value)
            GUI.ab_setting.text.file_in.value = GUI.ab_setting.constant.file_in_default
            GUI.ab_setting.text.file_in.channel_num = 2
        else
            GUI.ab_setting.text.file_in.channel_num = wave.get_property(wave_data, "channel_num")
            wave.delete_wavedata(wave_data)
        end
        --print(GUI.ab_setting.text.file_in.channel_num)
    end
    fd:destroy()
    --update_out_node_src()
end

GUI.vr.button.noise_sel.action = function()
    GUI.vr.list.noise.action()
    GUI.vr.dialog.noise_sel:popup()
end

SNR=""
function update_noise_tab()
    local num_tab = s_2_n(GUI.vr.list.noise.value)
    NOISE_TABLE = {}
    for i = 1, #num_tab do
        if num_tab[i] == 1 then
            pcall(loadstring("SNR=GUI.vr.text.noise_db"..tostring(i)..".value"))
            table.insert(NOISE_TABLE, {NOISE_TABLE1[i][1], tonumber(SNR)})
            print(NOISE_TABLE[1][1],NOISE_TABLE[1][2], #NOISE_TABLE)
            --print(NOISE_TABLE[2][1],NOISE_TABLE[2][2])
        end
    end
end
NOISE_TABLE = {}
update_noise_tab()

GUI.vr.button.ok.action = function()
    update_noise_tab()
    GUI.vr.dialog.noise_sel:hide()
end

GUI.vr.button.cancel.action = function()
    GUI.vr.dialog.noise_sel:hide()
end

GUI.vr.button.report_name.action = function()
    local fd=iup.filedlg{dialogtype="SAVE", title="Save wavefile", 
                         nochangedir="YES", directory=RECORD_PATH,value=GUI.vr.text.report_name.value,
                         filter="*.xml", filterinfo="*.xml", allownew="YES"}
    fd:popup(iup.CENTER, iup.CENTER)
    local status = fd.status

    if (status == "0") or (status == "1") then -- 0 normal; 1,new; -1, cancel
        if string.find(fd.value, ".xml") == nil then
            fd.value = fd.value..".xml"
        end
        GUI.vr.text.report_name.value = fd.value

        print(GUI.ab_setting.text.file_out.value)
    end
    
    fd:destroy()
end





--[[
function GUI.vr.button.reg_r:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr = remove_0x(spi_reg_addr.value)
    local addr = remove_0x(GUI.vr.text.reg_addr.value)
    if addr == false then return end
    local state, value = iM205.read_reg(tonumber("0x"..addr))
    if state then
        GUI.vr.text.reg_data.value = string.format("0x%02x", value)
    else
        message("ERROR", value)
    end
end

function GUI.vr.button.reg_w:action()
    if PDMCLK_ONOFF == 0 then
        message("Warning", "DSP may be in idle, access may be fail.")
    end
    --local addr, data = remove_0x(spi_reg_addr.value), remove_0x(spi_reg_data.value)
    local addr = remove_0x(GUI.vr.text.reg_addr.value)
    if addr == false then return end
    local data = remove_0x(GUI.vr.text.reg_data.value)
    if data == false then return end
  
    local state = iM205.write_reg(tonumber("0x"..addr), tonumber("0x"..data))
    if state == false then
        message("ERROR", "Write SPI Register Error")
    end
end
]]
GUI.vr.toggle.action = function()
    if GUI.vr.toggle.value == "ON" then

    else

    end
end

GUI.vr.active = function(active)
    if active == "yes" or active == "YES" then

    elseif active == "no" or active == "NO" then

    end
end

GUI.vr.active("yes")

GUI.vr.toggle.value = "OFF"
GUI.vr.toggle.action()
