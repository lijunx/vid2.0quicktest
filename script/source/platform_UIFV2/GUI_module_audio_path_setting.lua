--module define
printc("define UIF audio bus format GUI module.")
GUI.audio_path = {}

GUI.audio_path.constant = 
{
	bus_default = "I2S";
	m_s = M_S or 1;
	latch_edge_default = DATE_LATCH or 1; --1:rising; 2:falling
	bclk_polarity_default = BCLK_POLARITY or 1; --1:rising; 2:falling
	delay_default = ONE_CYCLE_DELAY or 1; -- 
    slot_num_default = 2; --"1","2","3","4","5","6","7","8"
    bit_len_default = SAMPLE_BIT_LEN/16; --1: "16bit", 2: "32bit"  --, 2: "24bit"
    sample_rate_default = 6; --"8000","16000","22050","32000","44100","48000"
    pdm_clk_default = PDM_ADC_CLK_SEL or 2; -- 1: 1.024M; 2:2.048M; 3:3.072M; 4: 4.096M
    bit_order_default = BIT_ORDER or 1;  --1: MSB first, 2: LSB first
    fram_start_default = FRAM_START or 5;
    pdm = 1;
    i2s = 2;
    pcm = 3;
    tdm = 4;
    rising = 0;
    falling = 1;
    delay_en = 1;
    delay_dis = 0;
    master = 1; -- base on UIF codec.
    slave = 0;
    msb = 1;
    lsb = 0;
    sample_rate = 16000;
}
PDM_ADC_CLK_SEL = 0
audio_path_title = 
{
    pdm = "PDM Setting",
    pcm = "PCM/PCM-TDM Setting",
    tdm = "TDM Setting",
    i2s = "I2S/I2S-TDM Setting",
}

GUI.audio_path.list = 
{
    slot_num = iup.list
    { 
        "1","2","3","4","5","6","7","8"; action_time = false,
        dropdown="YES", visible_items=8, size="30x", value = GUI.audio_path.constant.slot_num_default,
        action=function()
            audio_path_to_kernal()
            system_setting_sync(0x10)
        end
    };
    bit_len = iup.list
    {
        "16", "32",action_time = false,  -- "24",
        dropdown="YES", visible_items=3, size="30x", value = GUI.audio_path.constant.bit_len_default,
        action=function()
            GUI.audio_path.list.bit_len.action_time = not GUI.audio_path.list.bit_len.action_time
            if GUI.audio_path.list.bit_len.action_time then
                local bl = GUI.audio_path.list.bit_len[GUI.audio_path.list.bit_len.value+0]
                printc("Bit length is "..bl)
           --     SAMPLE_BIT_LEN = tonumber(bl)
                audio_path_to_kernal()
                --print(bl)
            --    SR_LEN = "S"..SAMPLE_BIT_LEN.."_LE"
            end
        end
    };
    sample_rate = iup.list
    {
        "8000","16000","24000","32000","48000", "96000", action_time = false,
        dropdown="YES", visible_items=6, size="50x", value = GUI.audio_path.constant.sample_rate_default,
        action=function()
            GUI.audio_path.list.sample_rate.action_time = not GUI.audio_path.list.sample_rate.action_time
            if GUI.audio_path.list.sample_rate.action_time then
                local sr = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0]
                printc("Sample rate is "..sr)
                audio_path_to_kernal()
                system_setting_sync(0x20)
            end
        end
    };
    pdm_clk = iup.list
    { 
        "1.024", "2.048", "3.072", "4.096", action_time = false,
        dropdown="YES", visible_items=4, size="45x", value = GUI.audio_path.constant.pdm_clk_default,
        action=function()
            GUI.audio_path.list.pdm_clk.action_time = not GUI.audio_path.list.pdm_clk.action_time
            if GUI.audio_path.list.pdm_clk.action_time then
                audio_path_to_kernal()
                printc("PDM clock: "..GUI.audio_path.list.pdm_clk[GUI.audio_path.list.pdm_clk.value])
            end
        end
    };
}

GUI.audio_path.label =
{
    bus = iup.label{title="Audio Bus:"};
    m_s = iup.label{title="Master/Slave:"};--,active = "no"};
    sample_rate = iup.label{title="Sample Rate(Hz):"};
    slot_num = iup.label{title="Slot Number:"};
    bit_len = iup.label{title="Bit Length(bit):"};
    delay = iup.label{title="One Cycle Delay:"};
    latch_edge = iup.label{title="Data Latch:"};
    bclk_polarity = iup.label{title="BCLK Polarity:"};
    bit_order = iup.label{title="Bit Order:",active = "no"};
    fram_start = iup.label{title="Fram Start:",active = "no"};
    pdm_clk = iup.label{title="PDM Clock(MHz):"};
}

GUI.audio_path.text = { }

GUI.audio_path.toggle =
{
    i2s = iup.toggle{title="I2S/I2S-TDM",value=GUI.audio_path.constant.bus_default == "I2S" and "ON" or "OFF";};
    tdm = iup.toggle{title="TDM",value=GUI.audio_path.constant.bus_default == "TDM" and "ON" or "OFF";};
    pcm = iup.toggle{title="PCM/PCM-TDM",value=GUI.audio_path.constant.bus_default == "PCM" and "ON" or "OFF";};
    pdm = iup.toggle{title="PDM",value=GUI.audio_path.constant.bus_default == "PDM" and "ON" or "OFF";};
    latch_edge_rising = iup.toggle{title="Rising Edge",value=GUI.audio_path.constant.latch_edge_default == 1 and "ON" or "OFF";};
    latch_edge_falling = iup.toggle{title="Falling Edge",value=GUI.audio_path.constant.latch_edge_default == 2 and "ON" or "OFF";};
    bclk_polarity_normal = iup.toggle{title="Normal",value=GUI.audio_path.constant.bclk_polarity_default == 1 and "ON" or "OFF";};
    bclk_polarity_invert = iup.toggle{title="Invert",value=GUI.audio_path.constant.bclk_polarity_default == 2 and "ON" or "OFF";};
    fram_latch_rising = iup.toggle{title="Rising Edge",value=GUI.audio_path.constant.fram_start_default == 5 and "ON" or "OFF";active = "no"};
    fram_latch_falling = iup.toggle{title="Falling Edge",value=GUI.audio_path.constant.fram_start_default == 4 and "ON" or "OFF";active = "no"};
    msb = iup.toggle{title="MSB First",active="OFF";value =GUI.audio_path.constant.bit_order_default == 1 and "ON" or "OFF"};
    lsb = iup.toggle{title="LSB First",active="OFF";value =GUI.audio_path.constant.bit_order_default == 2 and "ON" or "OFF"};
    delay = iup.toggle{title="One Cycle Delay",value=GUI.audio_path.constant.delay_default == 1 and "ON" or "OFF";};
    master = iup.toggle{title="Master",active="ON";value =GUI.audio_path.constant.m_s == 1 and "ON" or "OFF"};
    slave = iup.toggle{title="Slave",active="ON";value =GUI.audio_path.constant.m_s == 2 and "ON" or "OFF"};
    pdm_clk = iup.toggle{title="On",value="ON";};
}

GUI.audio_path.radio = 
{
    latch_edge = iup.radio
    {
        iup.hbox{GUI.audio_path.toggle.latch_edge_rising,GUI.audio_path.toggle.latch_edge_falling;gap="10",margin="5",alignment = "ACENTER"};
        value=GUI.audio_path.toggle.latch_edge_rising;
    };
    bclk_polarity = iup.radio
    {
        iup.hbox{GUI.audio_path.toggle.bclk_polarity_normal,GUI.audio_path.toggle.bclk_polarity_invert;gap="10",margin="5",alignment = "ACENTER"};
        value=GUI.audio_path.toggle.bclk_polarity_normal;
    };
    m_s = iup.radio
    {
        iup.hbox{GUI.audio_path.toggle.master,GUI.audio_path.toggle.slave;gap="5",margin="5",alignment = "ACENTER"};
        value=GUI.audio_path.toggle.master;
    };
    bit_order = iup.radio
    {
        iup.hbox{GUI.audio_path.toggle.msb,GUI.audio_path.toggle.lsb;gap="5",margin="5",alignment = "ACENTER"};
        value=GUI.audio_path.toggle.msb;
    };
    fram_start = iup.radio
    {
        iup.hbox{GUI.audio_path.toggle.fram_latch_rising,GUI.audio_path.toggle.fram_latch_falling;gap="5",margin="5",alignment = "ACENTER"};
        value=GUI.audio_path.toggle.fram_latch_rising;
    };
    bus = iup.radio
    {   --GUI.audio_path.toggle.tdm,
        iup.hbox{GUI.audio_path.toggle.pdm,GUI.audio_path.toggle.i2s,GUI.audio_path.toggle.pcm,gap="40",margin="5",alignment = "ACENTER"};
        value=GUI.audio_path.toggle.i2s;
    };
}

GUI.audio_path.button =
{
    --update = iup.button{size="40x", title="Update"};
    --default = iup.button{size="40x", title="Default"};
}

GUI.audio_path.local_frame =
{
    detail = iup.frame
    {
        iup.vbox
        {
            iup.hbox
            {
              iup.hbox{GUI.audio_path.label.sample_rate, GUI.audio_path.list.sample_rate;gap="5",margin="5",alignment="ACENTER"};
              iup.hbox{GUI.audio_path.label.slot_num, GUI.audio_path.list.slot_num;gap="5",margin="5",alignment="ACENTER"};
              iup.hbox{GUI.audio_path.label.bit_len, GUI.audio_path.list.bit_len;gap="5",margin="5",alignment="ACENTER"};
              gap="40",margin="5",alignment="ACENTER"
            };
            iup.hbox
            {
              --iup.hbox{GUI.audio_path.toggle.delay;gap="5",margin="5",alignment="ACENTER"};
              --iup.hbox{GUI.audio_path.label.fram_start, GUI.audio_path.radio.fram_start;gap="0",margin="5",alignment="ACENTER"};
              --iup.hbox{GUI.audio_path.label.latch_edge, GUI.audio_path.radio.latch_edge;gap="0",margin="5",alignment="ACENTER"};
              iup.hbox{GUI.audio_path.label.bclk_polarity, GUI.audio_path.radio.bclk_polarity;gap="0",margin="5",alignment="ACENTER"};
              iup.hbox{GUI.audio_path.label.m_s, GUI.audio_path.radio.m_s;gap="0",margin="5",alignment="ACENTER"};
              gap="80",margin="5",alignment="ACENTER"
            };
            iup.hbox
            {
              iup.hbox{GUI.audio_path.toggle.delay;gap="5",margin="5",alignment="ACENTER"};
              --iup.hbox{GUI.audio_path.label.bit_order, GUI.audio_path.radio.bit_order;gap="9",margin="15",alignment="ACENTER"};
              --iup.hbox{GUI.audio_path.label.fram_start, GUI.audio_path.radio.fram_start;gap="0",margin="0",alignment="ACENTER"};
              iup.hbox{GUI.audio_path.label.pdm_clk, GUI.audio_path.list.pdm_clk, gap="5",margin="5",alignment="ACENTER"}; --,GUI.audio_path.toggle.pdm_clk
              gap="140",margin="5",alignment="ACENTER"
            };
            --[[
            iup.hbox
            {
              iup.hbox{GUI.audio_path.label.pdm_clk, GUI.audio_path.list.pdm_clk,GUI.audio_path.toggle.pdm_clk, gap="5",margin="5",alignment="ACENTER"};
              gap="100",margin="5",alignment="ACENTER"
            };
            ]]
            gap="10",margin="0x5",alignment="ALEFT"
        };
        title =  "I2S Setting",
        alignment="ACENTER",--size="120x", 
    };
    pdm = iup.frame
    {
        iup.hbox
        {  
            --GUI.audio_path.toggle.pdm_clk;
            --GUI.audio_path.list.pdm_clk,
            gap="60",margin="45",alignment = "ACENTER"
        };
        title = "PDM Setting", 
        size="120x", alignment="ACENTER",
    }
};

GUI.audio_path.frame = iup.frame
{
    iup.vbox
    {  
        iup.hbox{GUI.audio_path.label.bus, GUI.audio_path.radio.bus;gap="5",margin="5x5",alignment="ACENTER"};
        GUI.audio_path.local_frame.detail;
        gap="5",margin="10x5",alignment="ALEFT"
    };
    title = "Audio Bus Format Setting",
    margin="0x10", alignment = "ACENTER", --size="400x",
}

----function
function audio_bus_switch(bus_type)
    for k, v in pairs(GUI.kernel.audio_bus) do
        GUI.kernel.audio_bus[k].active = "OFF"
        if GUI.kernel.audio_bus[k].name == bus_type then  GUI.kernel.audio_bus[k].active = "ON" end
    end
    update_audio_bus()
    system_setting_sync(0xff)
    active_design_save_button("yes")
end

function audio_path_to_kernal()
    for k, v in pairs(GUI.kernel.audio_bus) do
        if GUI.kernel.audio_bus[k].active == "ON" then
            if GUI.kernel.audio_bus[k].name ~= "pdm" then
                GUI.kernel.audio_bus[k].sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
                GUI.kernel.audio_bus[k].channum = GUI.audio_path.list.slot_num.value + 0
                GUI.kernel.audio_bus[k].m_s = GUI.audio_path.toggle.master.value == "ON" and "master" or "slave"
                GUI.kernel.audio_bus[k].format[1].bit_len = GUI.audio_path.list.bit_len[GUI.audio_path.list.bit_len.value] + 0
                GUI.kernel.audio_bus[k].format[1].delay = GUI.audio_path.toggle.delay.value == "ON" and "enable" or "disable"
                --GUI.kernel.audio_bus[k].format[1].bit_order = GUI.audio_path.toggle.msb.value == "ON" and "msb" or "lsb"
                --GUI.kernel.audio_bus[k].format[1].data_latch = GUI.audio_path.toggle.latch_edge_rising.value == "ON" and "rising" or "falling"
                GUI.kernel.audio_bus[k].format[1].bclk_polarity = GUI.audio_path.toggle.bclk_polarity_normal.value == "ON" and "normal" or "invert"
                --GUI.kernel.audio_bus[k].format[1].fram_start = GUI.audio_path.toggle.fram_latch_rising.value == "ON" and "rising" or "falling"
            else
                -- for PDM format
                GUI.kernel.audio_bus[k].bclock = tonumber(GUI.audio_path.list.pdm_clk[GUI.audio_path.list.pdm_clk.value+0])*1000000
                GUI.kernel.audio_bus[k].sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
                GUI.kernel.audio_bus[k].channum = 8--GUI.audio_path.list.slot_num.value + 0
            end
            GUI.ab_setting.constant.ab_in_max = GUI.audio_path.list.slot_num.value + 0
            GUI.audio_path.constant.sample_rate = GUI.audio_path.list.sample_rate[GUI.audio_path.list.sample_rate.value+0] + 0
        end
    end
    active_design_save_button("yes")
end

GUI.audio_path.toggle.latch_edge_rising.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.latch_edge_falling.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.fram_latch_rising.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.fram_latch_falling.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.bclk_polarity_normal.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.bclk_polarity_invert.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.msb.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.lsb.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.delay.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.master.action = function()
    audio_path_to_kernal()
end
GUI.audio_path.toggle.slave.action = function()
    audio_path_to_kernal()
end

GUI.audio_path.toggle.pdm.action = function()
    GUI.audio_path.local_frame.detail.title = audio_path_title.pdm
    audio_bus_switch("pdm")
    GUI.audio_path.local_frame.detail.active1()
end

GUI.audio_path.toggle.pcm.action = function()
    GUI.audio_path.local_frame.detail.title = audio_path_title.pcm
    audio_bus_switch("pcm")
    GUI.audio_path.local_frame.detail.active1()
end

GUI.audio_path.toggle.tdm.action = function()
    GUI.audio_path.local_frame.detail.title = audio_path_title.tdm
    audio_bus_switch("tdm")
    GUI.audio_path.local_frame.detail.active1()
end

GUI.audio_path.toggle.i2s.action = function()
    GUI.audio_path.local_frame.detail.title = audio_path_title.i2s
    audio_bus_switch("i2s")
    GUI.audio_path.local_frame.detail.active1()
end

GUI.audio_path.active = function(active)
    if active == "yes" or active == "YES" then
        GUI.audio_path.toggle.pdm.active = "yes"
        GUI.audio_path.toggle.pcm.active = "yes"
        GUI.audio_path.toggle.tdm.active = "yes"
        GUI.audio_path.toggle.i2s.active = "yes"
        GUI.audio_path.local_frame.detail.active1("yes")
    elseif active == "no" or active == "NO" then
        GUI.audio_path.toggle.pdm.active = "no"
        GUI.audio_path.toggle.pcm.active = "no"
        GUI.audio_path.toggle.tdm.active = "no"
        GUI.audio_path.toggle.i2s.active = "no"
        GUI.audio_path.local_frame.detail.active1("no")
    end
end

GUI.audio_path.toggle.pdm_clk.action = function()
    if GUI.audio_path.toggle.pdm_clk.value == "ON" then
        GUI.audio_path.list.pdm_clk.active = "yes"
        --FM36.adc_clk_onoff(1)
        if DSP_NAME == "iM501" then
            UIF.set_vec_config(3,DL_PATH,DELAY_POWER_DOWN,51,iM501.GPIO.IRQ,0, PDMCLK_TURNOFF_EN, DL_PATH)
        else
            FM36.adc_clk_onoff(1)
        end
        PDMCLK_ONOFF = 1
    else
        GUI.audio_path.list.pdm_clk.active = "no"
        FM36.adc_clk_onoff(0)
        PDMCLK_ONOFF = 0
    end
end


GUI.audio_path.local_frame.detail.active1 = function(active)
    active = active or "yes"
    if (active == "yes") or (active == "YES") then
        if GUI.audio_path.toggle.pdm.value == "OFF" then
            GUI.audio_path.label.slot_num.active = "yes"
            GUI.audio_path.list.slot_num.active = "yes"
            GUI.audio_path.label.bit_len.active = "yes"
            GUI.audio_path.list.bit_len.active = "yes"
            GUI.audio_path.toggle.delay.active = "yes"
            --GUI.audio_path.label.latch_edge.active = "yes"
            --GUI.audio_path.radio.latch_edge.active = "yes"
            GUI.audio_path.label.bclk_polarity.active = "yes"
            GUI.audio_path.radio.bclk_polarity.active = "yes"
            GUI.audio_path.label.m_s.active = "yes"
            --GUI.audio_path.radio.m_s.active = "yes"
            GUI.audio_path.toggle.master.active = "YES"
            GUI.audio_path.toggle.slave.active = "YES"
            --GUI.audio_path.label.bit_order.active = "yes"
            --GUI.audio_path.radio.bit_order.active = "yes"
            GUI.audio_path.label.sample_rate.active = "yes"
            GUI.audio_path.list.sample_rate.active = "yes"
            GUI.audio_path.toggle.pdm_clk.active  = "no"
            GUI.audio_path.list.pdm_clk.active = "no"
            GUI.audio_path.label.pdm_clk.active = "no"
            if GUI.audio_path.toggle.i2s.value == "ON" then
                --GUI.audio_path.label.slot_num.active = "no"
                --GUI.audio_path.list.slot_num.active = "no"
                GUI.audio_path.toggle.delay.active = "no"
            end
        else
            --GUI.audio_path.label.sample_rate.active = "no"
            --GUI.audio_path.list.sample_rate.active = "no"
            GUI.audio_path.label.slot_num.active = "no"
            GUI.audio_path.list.slot_num.active = "no"
            GUI.audio_path.label.bit_len.active = "no"
            GUI.audio_path.list.bit_len.active = "no"
            GUI.audio_path.toggle.delay.active = "no"
            --GUI.audio_path.label.latch_edge.active = "no"
            --GUI.audio_path.radio.latch_edge.active = "no"
            GUI.audio_path.label.bclk_polarity.active = "no"
            GUI.audio_path.radio.bclk_polarity.active = "no"
            GUI.audio_path.label.m_s.active = "no"
            --GUI.audio_path.radio.m_s.active = "no"
            GUI.audio_path.toggle.master.active = "NO"
            GUI.audio_path.toggle.slave.active = "NO"
            --GUI.audio_path.label.bit_order.active = "no"
            --GUI.audio_path.radio.bit_order.active = "no"
            GUI.audio_path.label.sample_rate.active = "yes"
            GUI.audio_path.list.sample_rate.active = "yes"
            GUI.audio_path.toggle.pdm_clk.active  = "yes"
            if GUI.audio_path.toggle.pdm_clk.value == "ON" then
                GUI.audio_path.list.pdm_clk.active = "yes"
            end
            GUI.audio_path.label.pdm_clk.active = "yes"
        end
    else
        GUI.audio_path.label.sample_rate.active = "no"
        GUI.audio_path.list.sample_rate.active = "no"
        GUI.audio_path.label.slot_num.active = "no"
        GUI.audio_path.list.slot_num.active = "no"
        GUI.audio_path.label.bit_len.active = "no"
        GUI.audio_path.list.bit_len.active = "no"
        GUI.audio_path.toggle.delay.active = "no"
        --GUI.audio_path.label.latch_edge.active = "no"
        --GUI.audio_path.radio.latch_edge.active = "no"
        GUI.audio_path.label.bclk_polarity.active = "no"
        GUI.audio_path.radio.bclk_polarity.active = "no"
        GUI.audio_path.label.m_s.active = "NO"
        --GUI.audio_path.radio.m_s.active = "NO"
        GUI.audio_path.toggle.master.active = "NO"
        GUI.audio_path.toggle.slave.active = "NO"
        --GUI.audio_path.label.bit_order.active = "no"
        --GUI.audio_path.radio.bit_order.active = "no"
        GUI.audio_path.toggle.pdm_clk.active  = "no"
        GUI.audio_path.list.pdm_clk.active = "no"
        GUI.audio_path.label.pdm_clk.active = "no"
    end
end
--[[
GUI.audio_path.local_frame.detail.active1 = function(type)
    type = type or 1
    if GUI.audio_path.toggle.pdm.value == "OFF" then
        if type == 1 then
            if GUI.audio_path.toggle.pcm.value == "ON" then
                GUI.audio_path.label.slot_num.active = "yes"
                GUI.audio_path.list.slot_num.active = "yes"
                GUI.audio_path.toggle.delay.active = "yes"
                GUI.audio_path.label.latch_edge.active = "yes"
                GUI.audio_path.radio.latch_edge.active = "yes"
                GUI.audio_path.label.m_s.active = "yes"
                GUI.audio_path.radio.m_s.active = "yes"
                --GUI.audio_path.label.bit_order.active = "yes"
                --GUI.audio_path.radio.bit_order.active = "yes"
            else
                GUI.audio_path.label.slot_num.active = "no"
                GUI.audio_path.list.slot_num.active = "no"
                GUI.audio_path.toggle.delay.active = "no"
                GUI.audio_path.label.latch_edge.active = "no"
                GUI.audio_path.radio.latch_edge.active = "no"
                GUI.audio_path.label.m_s.active = "no"
                GUI.audio_path.radio.m_s.active = "no"
            end
            
            GUI.audio_path.label.sample_rate.active = "yes"
            GUI.audio_path.list.sample_rate.active = "yes"
            GUI.audio_path.toggle.pdm_clk.active  = "no"
            GUI.audio_path.list.pdm_clk.active = "no"
            GUI.audio_path.label.pdm_clk.active = "no"
        else

        end
    else
        GUI.audio_path.label.sample_rate.active = "no"
        GUI.audio_path.list.sample_rate.active = "no"
        GUI.audio_path.toggle.pdm_clk.active  = "yes"
        if GUI.audio_path.toggle.pdm_clk.value == "ON" then
            GUI.audio_path.list.pdm_clk.active = "yes"
        end
        GUI.audio_path.label.pdm_clk.active = "yes"
        GUI.audio_path.label.slot_num.active = "no"
        GUI.audio_path.list.slot_num.active = "no"
        GUI.audio_path.toggle.delay.active = "no"
        GUI.audio_path.label.latch_edge.active = "no"
        GUI.audio_path.radio.latch_edge.active = "no"
    end
end
]]

--GUI.audio_path.button.default.action()
--GUI.audio_path.local_frame.detail.active1()
--GUI.audio_path.active("yes")

