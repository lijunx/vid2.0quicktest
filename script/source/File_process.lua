file_process_version = "v0.1"
--**************************************************--
--Function List:
--  printc
--  print_debug
--  print_config_info
--  print_stress_info
--  print_error_info
--  clear_PK_file
--  get_file_name
--  upload
--  
--**************************************************--

--**************************************************--
--Function Name: printc
--Description: print and save screen information in screen_info.txt.
--Return Value: no.
--Parameter: str
--  str: string that will be print and save.
--Usage: printc("screen information")
--**************************************************--
Printc_Run_1th_Flag = true
print_file_index    = 0 
function printc(str, display)

    display = display or 1
    local str_printf = str
    if(Printc_Run_1th_Flag == true)  then
        Printc_Run_1th_Flag = false
        str_printf =             "****************                          Log File                       ****************\n"
        str_printf = str_printf.."****************        For Auto Simulation Tool script CMD record.      ****************\n"
        str_printf = str_printf.."****************         A product of SQA Fortemedia Co., Ltd.           ****************\n\n"..str
    end

    if(str_printf == nil) then str_printf = "nil" end
   
    if type(str_printf) == "table" then
        local st = "\n---------------------------------\n"  
        for i = 1, #str_printf do
            if type(str_printf[i]) == "number" then
                st = st..string.format("  %02X", str_printf[i])
                if( i%16  == 0 ) then 
                	st = st.."\n" 
                elseif( i%8 == 0 ) then 
                 	st = st.."\t" 
                end
            else
                st = st..str_printf[i].."  "
            end
        end
        st = st.."\n-----------------------------------\n" 
        str_printf = st
    end

    local length_file = Length_Of_File(PRINT_FILE)
    if( length_file > 1000*1000*500) then   --each log file less than 500MB
        print_file_index = print_file_index + 1
		PRINT_FILE 	= "screen_info"..print_file_index..".txt"
    end
    local FILE = assert( io.open(PRINT_FILE,"a+") )
    assert(FILE:write( str_printf.."\n" ) )
    assert(FILE:close())

    
    local token = {}
    local len = string.len(str_printf)
    local idx = 0
    
    while idx < len do
        local pos = string.find(str_printf, "\n", idx + 1)
        if pos == nil then pos = len end
        local t = string.sub(str_printf, idx + 1, pos)
        table.insert(token, t)
        idx = pos
    end

    for i = 1, #token do
        if string.len(token[i]) > 80 then  -- 80, because the width of noah.exe's window is 80.
            idx = 0
            while idx < 80 do
                local pos = string.find(token[i], "\\",  idx + 1)
                if pos == nil or pos > 80 then break end
                idx = pos
            end
            if idx == 0 then
                while idx < 80 do
                    local pos = string.find(token[i], " ",  idx + 1)
                    if pos == nil or pos > 80 then break end
                    idx = pos
                end
            end
            if idx == 0 then
                if display == 1 then print(token[i]) end
            else
                if display == 1 then 
                    print(string.sub(token[i], 1, idx))
                    print("  "..string.sub(token[i], idx + 1, -1))
                end
            end
        else
            if display == 1 then print(token[i]) end
        end
    end

end

--**************************************************--
--Function Name: print_debug
--Description: print string when debug.
--Return Value: no
--Parameter: str : the string that will be print
--
--**************************************************--
function print_debug(str)
    if (DEBUG_FLAG == 1 or DEBUG_FLAG == true) then
        print(str)
    end
end


--**************************************************--
--Function Name: print_config_info
--Description: save config information in test_condition.txt
--Return Value: no
--Parameter: no 
-- 
--**************************************************--
function print_config_info()
    printc("\n********************")
    printc("Test information: ")
    local FILE = assert( io.open(CONFILE_FILE,"a+") )
    local str_printf = "\n\n********************"
    assert(FILE:write( str_printf.."\n") )
    str_printf = "Start time: "..os.date("%Y/%m/%d %H:%M:%S")
    printc(str_printf)
    assert(FILE:write( str_printf.."\n" ) )
    str_printf = "Test platform is "..TEST_PLATFORM..","
    printc(str_printf)
    assert(FILE:write( str_printf.."\n") )

    str_printf = "Digital interface mode is "..MODE_I2S_CHI..","
    printc(str_printf)
    assert(FILE:write( str_printf.."\n") )
    str_printf = "Chip is "..DSP_M_S..","
    printc(str_printf)
    assert(FILE:write( str_printf.."\n") )
    str_printf = "Test sample rate is "..SAMPLE_RATE..","
    printc(str_printf)
    assert(FILE:write( str_printf.."\n") )
    str_printf = "Tester: "..TESTER
    printc(str_printf)
    assert(FILE:write( str_printf.."\n") )
    str_printf = "********************\n"
    assert(FILE:write( str_printf.."\n") )
    assert(	FILE:close() )
    printc("********************\n\n")
end

--**************************************************--
--Function Name: print_stress_info
--Description: print stress test related information.
--Return Value: no
--Parameter: str.
--
--**************************************************--
function print_stress_info(str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(STRESS_INFO_FILE,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end

--**************************************************--
--Function Name: print_cali_data
--Description: print calibration data.
--Return Value: no
--Parameter: str.
--
--**************************************************--
function print_cali_data(str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(CALIBRATION_FILE,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end
--**************************************************--
--Function Name: print_error_info
--Description: save error information in error_info.txt
--Return Value: no
--Parameter: str, flag
--  str: error information
--  flag: if flag is one and DEBUG_FLAG is one too, the script will stop.
--    default value is 1.
--Usage: print_error_info("function ERROR", 0)
--**************************************************--
function print_error_info(str, flag)
    flag = flag or 1
    if(str == nil) then str = "nil" end
    printc(str.."\n")
    local FILE = assert( io.open(ERROR_FILE,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )

    --alarm(0.5)
    if DEBUG_FLAG == 1 and flag == 1 then
        control.stop()
    end
end

--**************************************************--
--Function Name: print_pesq_info
--Description: save pesq value in PESQ_info.txt.
--Return Value: no
--Parameter: str
--Usage: print_pesq_info("wave pesq value is 4.0")
--**************************************************--
function print_pesq_info(str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(PESQ_INFO_FILE,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end

function print_result(file, str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(file,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end

--**************************************************--
--Function Name: clear_PK_file
--Description: delete .pk file
--Return Value: no
--Parameter: path
--  path: Indicate file path.
--Usage: clear_PK_file("e:\\")
--**************************************************--
function clear_PK_file(path)
    PK_PAHT = path  
	os.execute("del \""..PK_PAHT.."*.pk\"")
end

--**************************************************--
--Function Name: get_file_name
--Description: get the file name list
--Return Value: no
--
--Parameter: file_path, match_string, flag_print
--  file_path: Indicate file path
--  match_string: Indicate the key word
--  flag_print: if flag_print is not nil, it will print file name.
--Usage: get_file_name("e:\\wave_file\\", "*.wav", 1)
--**************************************************--
function get_file_name(file_path, match_string, flag_print)
    match_string = match_string or "*.wav"
    local file = misc.find_names(match_string, file_path, 0)
    if file == nil then printc("no file name matched") end
    for i = 1, #file do
        file[i] = string.gsub(file[i], "\\", "")
    end
    if flag_print == nil then return file end
    
    printc("{")
    for i = 1, #file do
        printc("\""..file[i].."\",")
    end
    printc("}")
end

--**************************************************--
--Function Name: upload
--Description: upload test result to SQA server.
--Return Value: no
--Parameter: source_folder, object_file
--  source_folder: Indicate source folder
--  object_file: Indicate object_file
--Usage: upload(RESULT_PATH, "\\\\192.168.50.200\\Share Doc\\upload\\WJ\\")
--**************************************************--
function upload(source_folder, object_file)
    printc("\nUpload test result.")
    os.execute("md \""..UPLOAD_FOLDER.."\"")
    local upload_folder = UPLOAD_FOLDER.."\\"..object_file
    os.execute("md \""..upload_folder.."\"")
    printc("uploading object path: "..upload_folder)
    printc("uploading source path: "..source_folder)
    printc("uploading start...".."\n".."  it needs a few minutes, please wait!")
    os.execute("xcopy \""..source_folder.."\" \""..upload_folder.."\" ".."/s/e")
    printc("upload finished!\n")
end

--------------------------------------------------------------------------------
-- Function Description: Print "PASS" or "FAIL" pattern
-- @param   : none
-- @return  : none
--
-- Usage: Print_Result()
--------------------------------------------------------------------------------
function Print_Result()

    local pre = " -- -- -- -- -- --  "
    if(TOTAL_TEST_RESULT) then
		printc("\n\n********  PASS  ********\n\n")
		printc(pre.."XXXXXX    XX     XXXXX   XXXXX")
		printc(pre.." X    X    X    X     X X     X")
		printc(pre.." X    X    X    X       X      ")
		printc(pre.." X    X   X X   X       X      ")
		printc(pre.." XXXXX    X X    XXXXX   XXXXX ")
		printc(pre.." X       X   X        X       X")
		printc(pre.." X       XXXXX        X       X")
		printc(pre.." X       X   X  X     X X     X")
		printc(pre.."XXXX    XXX XXX  XXXXX   XXXXX")
	else
		printc("\n\n********  FAIL  ********\n\n")
		printc(pre.."XXXXXXX   XX     XXXXX  XXXXX  ")
		printc(pre.." X    X    X       X      X  ")
		printc(pre.." X         X       X      X  ")
		printc(pre.." X  X     X X      X      X    ")
		printc(pre.." XXXX     X X      X      X    ")
		printc(pre.." X  X    X   X     X      X    ")
		printc(pre.." X       XXXXX     X      X    ")
		printc(pre.." X       X   X     X      X   X")
		printc(pre.."XXXX    XXX XXX  XXXXX  XXXXXX")
	end

end

function Print_Result_UI()

    local pre = " -- -- -- -- -- --  "
    if(TOTAL_TEST_RESULT) then
		--printc("\n\n********    PASS    ********\n\n")
		printc(pre.."XXXXXX        XX         XXXXX       XXXXX")
		printc(pre.."  X        X        X        X          X  X          X")
		printc(pre.."  X        X        X        X              X      ")
		printc(pre.."  X        X      X  X      X              X      ")
		printc(pre.."  XXXXX        X  X        XXXXX      XXXXX ")
		printc(pre.."  X              X      X                X              X")
		printc(pre.."  X              XXXXX                X              X")
		printc(pre.."  X              X      X    X          X  X          X")
		printc(pre.."XXXX        XXX  XXX    XXXXX      XXXXX")
	else
		--printc("\n\n********    FAIL    ********\n\n")
		printc(pre.."XXXXXXX      XX          XXXXX    XXXXX  ")
		printc(pre.."  X        X        X              X           X  ")
		printc(pre.."  X                  X              X            X  ")
		printc(pre.."  X    X          X  X            X            X    ")
		printc(pre.."  XXXX          X  X            X            X    ")
		printc(pre.."  X    X        X      X          X            X    ")
		printc(pre.."  X              XXXXX          X            X    ")
		printc(pre.."  X              X      X          X            X      X")
		printc(pre.."XXXX        XXX  XXX    XXXXX    XXXXXX")
	end

end

---------------------------------------------------------------------------------
-- Function Description: Get the time spend from last time this subroutine called
-- @param:   none
-- @return  : string "minute : second"
--
-- Usage: a = Timer_Record()
---------------------------------------------------------------------------------

Timer_Index        = 0
Timer      ={}
Timer[Timer_Index] = os.time()
function Timer_Record(title)
    title = title or ""
	local cost_t
	local str_time_cost
	Timer_Index  =  Timer_Index + 1
	Timer[Timer_Index]  =  os.time()
	cost_t  =  Timer[Timer_Index] - Timer[Timer_Index - 1]
	str_time_cost = string.format("%d : %d", cost_t/60, cost_t%60)
	print_time_cost(string.format("\n**************"..title.." Time cost:%4d Minutes %2d Seconds. **************\n", cost_t/60,cost_t%60))
	--Status_Bar()
	return str_time_cost

end
function print_time_cost(str)
    if(str == nil) then str = "nil" end
    local FILE = assert( io.open(TIME_COST_FILE,"a+") )
    assert(FILE:write( str.."\n" ) )
    assert(	FILE:close() )
end

function Length_Of_File( filename )

	local fh = assert(io.open(filename, "rb"))
	local len = assert(fh:seek("end"))
	fh:close()
	return len
  
end
---------------------------------------------------------------------------------
-- Function Description: Return to GUI to prompt a box
-- @param:   str      : the string need to display
--           msg_type : "error"   -- prompt error msg, default
--                      "info"    -- prompt info  msg
-- @return  : none
--
-- Usage:  print_prompt_err("error detected")
--         print_prompt_err("error detected", "error")
--         print_prompt_err("test finished!","info")
---------------------------------------------------------------------------------
function print_prompt_err(str, msg_type)

    if( CLIENT_MODE ~= "Server" ) then
        --printc(str)
        msg_type = (msg_type == "info") and "pass:" or "fail:"
        str      =  msg_type..str
        error(str,0)
    else
    	printc(str)
    end
end



function message(title, str)
    --iup.Alarm(title, str, "OK")
    iup.Message(title, str)
    printc(title..": "..str)
end

function dir_fill(path)
    if string.find(path, ":\\") == nil then
        path = NOAH_PATH..path
    end
   -- print(path)
    return path
end

function dir_remove(path)
--print("aaaa"..path)
    local i, j = string.find(path, NOAH_PATH)
    if i ~= nil then
        path = string.sub(path, j+1, -1)
    end
--    print("bbbb"..path)
    return path
end

function get_filename_from_folder(folder)
    local str = folder
    local index = 1
    repeat
        index = string.find(str, "\\")
        str = string.sub(str, (index or 0)+1, -1)
    until index == nil
    return str
end

function get_channelname_from_wavefile(wavefile)
print(wavefile)
    local file = io.open(wavefile, "rb")
    local str = file:read("*all")
    local data_tab = {}
    local len = string.len(str)
    --print(str, len)
     
    local start_chunk = 0
    local chunk_found = 0
    local chunk = {}
    
    for i = len - 1280, len do
        data_tab[i] = string.byte(str, i)
        if chunk_found == 1 then
            --print(data_tab[i])
            if data_tab[i] > 0x20 then
                table.insert(chunk, string.char(data_tab[i]))
                start_chunk = 1
            elseif start_chunk == 1 and data_tab[i] == 0 then break end 
        end
    
        if i > 4 and chunk_found == 0 and (string.char(data_tab[i]) == "B" or string.char(data_tab[i]) == "X") then
            local str1 = string.char(data_tab[i-3])..string.char(data_tab[i-2])..string.char(data_tab[i-1])..string.char(data_tab[i])
            --local str1 = string.gsub(str, i-4, i)
            if str1 == "CHLB" or str1 == "FMEX" then chunk_found = 1 end
        end
    end

    file:close()
    print(#chunk)
    if #chunk > 0 then
        str = ""
        for i = 1, #chunk do
             str = str..chunk[i]
        end
        print(str)

        os.execute("c: > ".."channel_name.json")
        local file = io.open("channel_name.json","a+")
        file:write(str)
        file:close()

        local channel_name = load_jsonfile1("channel_name.json")
        return channel_name.channels_label
    else
        return {}
    end
end

function import_gui_default(item)
    GUI_TEXT_DEFAULT = GUI_TEXT_DEFAULT or "gui_default.txt"
    local file = io.open(GUI_TEXT_DEFAULT, "r")
    if file == nil then
        GUI_TEXT_DEFAULT = "gui_default.txt"  --Save test process
        os.execute("c: > "..GUI_TEXT_DEFAULT)
        printc("No gui_default.txt")
        return ""
    end

    local text = ""
    for line in file:lines(file) do
        local i,j = string.find(line, ":")
        if i ~= nil then
            local str = string.sub(line, 1, i-1)
            print(str, string.sub(line, i+1, -1))
            if str == item then text = string.sub(line, i+1, -1) break end
        end
    end

    file:close()
    return text
end

function export_gui_default(item, text)
    local file = io.open(GUI_TEXT_DEFAULT,"r")
    local file_line = {}
    if file == nil then 
        file_line = {item..":"..text}
        GUI_TEXT_DEFAULT = "gui_default.txt" 
    else
        for line in file:lines(file) do
            if string.len(line)>0 then table.insert(file_line, line) end
        end
        file:close()
        if #file_line < 1 then file_line = {item..":"..text} end
    end
    
    local temp_file = "temp.txt"
    os.execute("c: > "..temp_file)
    file = io.open(temp_file,"a+")

    local add_flag = 1
    for i = 1, #file_line do 
        print(file_line[i])
        local j = string.find(file_line[i], ":")
        if j ~= nil then
            local str = string.sub(file_line[i], 1, j-1)
            print(str, string.sub(file_line[i], j+1, -1))
            if str == item then 
                file_line[i] = item..":"..text
                add_flag = 0
            end
        end
        file:write(file_line[i].."\n")
    end
    if add_flag == 1 then file:write(item..":"..text.."\n") end
    
    file:close()
    
    os.execute("copy "..temp_file.." "..GUI_TEXT_DEFAULT)--.." ".."/s/e")
    os.execute("del "..temp_file)
end

--**************************************************--
--Function Name: getfiles
--Description: get files in root path.
--Return Value: files array.
--Parameter: 
--   ext_name: expanded-name,
--   indir:    true: search in subdirectory. false: only root path.
--   whole_path: true: return whold path. false: return file name.
--
--**************************************************--
function getfiles(rootpath, exp_name, indir, whole_path, pathes)
    pathes = pathes or {}
    for entry in lfs.dir(rootpath) do
        if entry ~= '.' and entry ~= '..' then
            local path = rootpath .. '\\' .. entry
            local attr = lfs.attributes(path)
            --print(attr.mode, path)
            assert(type(attr) == 'table')
            
            if attr.mode == 'directory' then
                if indir then
                    getfiles(path, exp_name, indir, whole_path, pathes)
                end
            else
                if string.find(string.lower(entry), string.lower(exp_name)) ~= nil then
                    if whole_path then
                        table.insert(pathes, path)
                    else
                        table.insert(pathes, entry)
                    end
                end
            end
        end
    end
    return pathes
end

function getdirs(rootpath, whole_path, pathes)
    pathes = pathes or {}
    for entry in lfs.dir(rootpath) do
        if entry ~= '.' and entry ~= '..' then
            local path = rootpath .. '\\' .. entry
            local attr = lfs.attributes(path)
            --print(attr.mode, path)
            if type(attr) == 'table' then
                if attr.mode == 'directory' then
                    if whole_path then
                        table.insert(pathes, path)
                    else
                        table.insert(pathes, entry)
                    end
                end
            end
        end
    end
    return pathes
end

