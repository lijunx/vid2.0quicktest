dofile(SCRIPT_PATH.."Arithmetic.lua")
dofile(SCRIPT_PATH.."File_process.lua")
dofile(SCRIPT_PATH.."Json_function.lua")
dofile(SCRIPT_PATH.."Check.lua")

-- connectionMode: 1--I2S, 2--PDM
function intial_platform(dutName, samplerate, platform, connectionMode,fastdowncode)
    local connectionModeStr = "I2S"
    if 2 == connectionMode and 1==fastdowncode then connectionModeStr = "PDM_SPI_2048" end
    if 2 == connectionMode and 2==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    if 2 == connectionMode and 3==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    if 2 == connectionMode and 4==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    if 3 == connectionMode and 1==fastdowncode then connectionModeStr = "PDM_SPI_1024" end
    if 3 == connectionMode and 2==fastdowncode then connectionModeStr = "PDM_I2C_1024" end
    if 3 == connectionMode and 3==fastdowncode then connectionModeStr = "PDM_I2C_1024" end
    if 3 == connectionMode and 4==fastdowncode then connectionModeStr = "PDM_I2C_1024" end
    --- 4 , Mclk 24M means Real PDMCLK 2000K
    if 4 == connectionMode and 1==fastdowncode then connectionModeStr = "PDM_SPI_2048" end
    if 4 == connectionMode and 2==fastdowncode then connectionModeStr = "PDM_I2C_2048" end   
    if 4 == connectionMode and 3==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    if 4 == connectionMode and 4==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    --- 5 ,PDMCLK3072k  Mclk 26M means Real PDMCLK 3250K
    if 5 == connectionMode and 1==fastdowncode then connectionModeStr = "PDM_SPI_3250" end
    if 5 == connectionMode and 2==fastdowncode then connectionModeStr = "PDM_I2C_3250" end
    if 5 == connectionMode and 3==fastdowncode then connectionModeStr = "PDM_I2C_3250" end
    if 5 == connectionMode and 4==fastdowncode then connectionModeStr = "PDM_I2C_3250" end
    --- 6 , Mclk 28.8M means Real PDMCLK 1200K
    if 6 == connectionMode and 1==fastdowncode then connectionModeStr = "PDM_SPI_1024" end
    if 6 == connectionMode and 2==fastdowncode then connectionModeStr = "PDM_I2C_1024" end
    if 6 == connectionMode and 3==fastdowncode then connectionModeStr = "PDM_I2C_1024" end
    if 6 == connectionMode and 4==fastdowncode then connectionModeStr = "PDM_I2C_1024" end
    --- 7 , Mclk 28.8M means Real PDMCLK 2400K
    if 7 == connectionMode and 1==fastdowncode then connectionModeStr = "PDM_SPI_2048" end
    if 7 == connectionMode and 2==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    if 7 == connectionMode and 3==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    if 7 == connectionMode and 4==fastdowncode then connectionModeStr = "PDM_I2C_2048" end
    

    JSON_FILE = CONFIG_PATH..dutName.."\\"..samplerate.."_" ..connectionModeStr..".json"
    TEMP_JSON = CONFIG_PATH.."temp.json"
    print("connectionModeStr  is... "..connectionModeStr)

     
    --Init platform
    sub_dirctory = "platform_"..platform.."\\"
    dofile(SCRIPT_PATH..sub_dirctory.."Config_platform.lua")
    dofile(SCRIPT_PATH..sub_dirctory.."Init_platform.lua")
end