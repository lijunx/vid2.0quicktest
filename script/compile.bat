cls
REM ------------------------------------------------------------------------------------
REM - VID.bat
REM - by JunWang
REM ------------------------------------------------------------------------------------
set CODE_HOME=%cd%
REM ------------------------------------------------------------------------------------
REM Platform library: compile
REM ------------------------------------------------------------------------------------
set exception=%cd%\source\platform_UIFV2\GUI_config_ab.lua

 	cd source
 	@echo compile files
  for /f "delims=" %%e in ('dir /b /s') do (
 	if /i %%e neq %exception% (
		luac -o "%%~dpne.ndl" "%%~dpne.lua" 2>>nul
			del /q "%%~dpne.lua" 2>>nul
			) else @echo %%e
	)
