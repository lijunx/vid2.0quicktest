
# Configuration文件说明

包含四大类：
1. Audio bridge通路设置:  node
2. Audio Bus Format 设置:  audio_bus
3. Command Bus Format 设置：command_bus
4. Code 和Vec路径设置：code_vec

## 1 Audio bridge通路设置: node
### 1.1 CAudUsbReadNode 从UIF读取MIC,LINE-IN和GPIO数据
- name: “ab_in”,
- active: “ON” or “OFF”,
- sample_rate:  AB上来的数据的采样率
- channum: AB上来的数据里面有多少路数据，UI负责根据LINE-IN是否打开，GPIO是否打开，已经MIC开关数，来计算这个通道数，并告诉音频系统

### 1.2 CWavFileReadNode 读取Wav文件节点
- name: “file_in”,
- active: “ON” or “OFF”,
- filename:  读取的波形文件路径和名称

### 1.3 CSoundCardReadNode 声卡读取节点
- name: “sc_in”,
- active: “ON” or “OFF”,
- sample_rate:  数据的采样率,
- channum: 声卡录音的通道数,
- filename: 录音声卡的名称

### 1.4 CAudUsbWriteNode 往AB板子发送音频数据。
- name: “ab_out”,
- active: “ON” or “OFF”,
- src_des:表示数据来源

### 1.5 CWavFileWriteNode 写Wav文件节点
- name: “file_out”,
- active: “ON” or “OFF”,
- filename: 输出波形文件路径和名称,
- src_des:表示数据来源

### 1.6 CShareMemSinkNode 共享内存节点，把数据循环写到共享内存中，供客户端如波形显示等来取。
- name: “waveview”,
- active: “ON” or “OFF”,
- src_des: 表示数据来源

### 1.7 CsoundCardWriteNode 写Wav文件节点
- name: “sc_out”,
- active: “ON” or “OFF”,
- filename: 播放声卡的名称,
- src_des: 表示数据来源

### 1.8 src_des: 数据来源
- sId_: Index
- name: 源节点的名称。
```
    ab_in: TDM/PDM s1; TDM/PDM s2……TDM/PDM s8
    gpio_in: GPIO2
    file_in: Wavefile CH1;  Wavefiel CH2……
    sc_in: Soundcard L; Soundcard R
```

## 2 Audio Bus Format 设置：audio_bus
包含四中BUS： I2S, TDM, PCM, PDM
每种BUS的描述：
- name: BUS 的名称
- active: “ON” or “OFF”
- sample_rate: bus的采样率
- channum: bus 的通道数
- m_s: bus master/slave
- format:
```
    delay: one cycle delay.  enable/disable
    data_letch: 数据锁存.  rising/falling
    bit_len: 总线每个通道的bit长度
    bit_order: MSB/LSB
```

## 3 Command Bus Format 设置；command_bus
UIF与DSP之间的命令总线设置：包含SPI 和I2C
- name: BUS 的名称
- active: “ON” or “OFF”
- sample_rate: bus的采样率
- format: 

## 4 Code 和Vec路径设置。code_vec
DSP code和vec的路劲
- code:  code and vec存放的路径,
- vec:  写入DSP的vec文件
