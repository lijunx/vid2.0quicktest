@echo off
set service_name=NoahCtrlSvr
set service_displayname=Automaticall Run Noah When System Power On

::set bin path
set prog_path=%cd%\NoahCtrlSvr.exe

::set start mode auto:demand:disabled:
set strt=auto

echo                    Install NoahCtrlSvr 
echo =========================================================== 

echo try to stop and delete old service
::delet old service if it exists
net stop %service_name% 2>nul
sc delete %service_name% 2>nul

::prepare startup.lua
echo log("DBSvr", "start DB server. Result is:", tostring(dbsvr.start_server())) >startup.lua
echo log("DBSvr", "startup finish") >>startup.lua

echo create service
sc create %service_name% binPath= "%prog_path%" start= auto DisplayName= "%service_displayname%"

echo start service
::start service
net start %service_name% 
if NOT %errorlevel% == 0 goto error

echo ==========================Install Complete============================
goto end

:error
echo !!!!!!!!!!!!!!!!!!!!!!!   install failed     !!!!!!!!!!!!!!!!!!!!!!!!!
echo -------------------------     exit     -------------------------------

:end
pause